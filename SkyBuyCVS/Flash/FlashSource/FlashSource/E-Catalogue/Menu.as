﻿import mx.transitions.Tween;
import mx.transitions.easing.*;
class Menu extends MovieClip {
	private var startX:Number;
	private var startY:Number;
	private var tmp:Number;
	private var noOfItemsDisplayed:Number;
	private var noOfItems:Number;
	private var menuItemHeight:Number;
	private var menuSeperatorHeight:Number;
	private var menuFooterHeight:Number;
	private var menuItemAndSepHeight:Number;
	private var menuHolderHeight:Number;
	private var menuContainer:MovieClip;
	private var mc:MovieClip;
	private var menuFooter:MovieClip;
	private var menuItemContainer:MovieClip;
	private var currentY:Number;
	public var prevTimerInterval:Number;
	public var nextTimerInterval:Number;
	private var count:Number;
    public var visible:Boolean;
	private var isPrevTransStart:Boolean;
	private var isNextTransStart:Boolean;
	private function init():Void {
		startX = 0;
		startY = 0;
		currentY = startY;
		noOfItemsDisplayed = 10;
		menuItemHeight = 22;
		menuSeperatorHeight = 2;
		menuFooterHeight = 22;
		menuItemAndSepHeight = menuItemHeight+menuSeperatorHeight;
		menuHolderHeight = menuItemAndSepHeight*noOfItemsDisplayed;
		menuContainer = this;
		mc = menuContainer.attachMovie("MenuContainer", "menuContainer", menuContainer.getNextHighestDepth());
		menuContainer.attachMovie("mask","mask_mc",menuContainer.getNextHighestDepth());
		mc.setMask(menuContainer.mask_mc);
		menuContainer.mask_mc._height = menuItemAndSepHeight*noOfItemsDisplayed+menuFooterHeight;
		menuItemContainer = mc.createEmptyMovieClip("MenuItemContainer", mc.getNextHighestDepth());
		mc.attachMovie("mask","menuItemContainerMask",mc.getNextHighestDepth());
		menuItemContainer.setMask(mc.menuItemContainerMask);
		menuFooter = mc.attachMovie("bottom", "menuFooter", mc.getNextHighestDepth());
		menuFooter.next._visible = false;
		menuFooter.prev._visible = false;
		menuFooter.next.onRelease = nextItemOnReleaseHandler;
		menuFooter.next.onRollOver = function(){
			
			if(this._parent.next._alpha==100){
				this._parent._parent._parent.nextTimerInterval=setInterval(this._parent._parent._parent.nextItemOnRollOverHandler,350,this);
				this._parent.next.gotoAndStop(2);
				this._parent.prev.gotoAndStop(1);
			}
		};
		menuFooter.next.onRollOut=function(){
			this._parent.next.gotoAndStop(1);
			clearInterval(this._parent._parent._parent.nextTimerInterval);
		};
		menuFooter.prev.onRelease = prevItemOnReleaseHandler;
		menuFooter.prev.onRollOver = function(){
			if(this._parent.prev._alpha==100){
			this._parent._parent._parent.prevTimerInterval=setInterval(this._parent._parent._parent.prevItemOnRollOverHandler,350,this);
			this._parent.prev.gotoAndStop(2);
			}
		};
		menuFooter.prev.onRollOut=function(){
			this._parent.prev.gotoAndStop(1);
			clearInterval(this._parent._parent._parent.prevTimerInterval);
		};
		menuFooter._x = startX;
		count = 0;
		visible=false;
		isPrevTransStart=false;
		isNextTransStart=false;
	}
	private function setPostions():Void {
		if (count>getNoOfItemsDisplayed()) {
			mc.menuItemContainerMask._height = menuHolderHeight;
			mc.container._height = menuHolderHeight;
			menuFooter.next._visible = true;
			menuFooter.prev._visible = true;
			menuFooter.next._alpha = 100;
			menuFooter.prev._alpha = 50;
			menuFooter._y = menuHolderHeight;
		} else {
			menuFooter._y = currentY;
			mc.container._height = (count+1)*menuItemHeight;
			mc.menuItemContainerMask._height = (count+1)*menuItemHeight;
		}
		mc._y = (menuHolderHeight+menuFooterHeight)*-1;
	}
	private function nextItemOnRollOverHandler(mClip:MovieClip):Void {
		clearInterval(mClip._parent._parent._parent.prevTimerInterval);
		if(mClip._parent._parent._parent.isNextTransStart==false and mClip._parent._parent._parent.isPrevTransStart==false){
			var mc:MovieClip = mClip._parent._parent._parent;
			if (mc.menuItemContainer._y>((mc.menuItemAndSepHeight*mc.count-mc.menuHolderHeight)*-1)) {
				mClip._parent.prev._alpha = 100;
			} else {
				mClip._parent.prev._alpha = 100;
				mClip._parent.next._alpha = 50;
				mClip._parent.next.gotoAndStop(1);
				clearInterval(mClip._parent._parent._parent.nextTimerInterval);
			}
			if (mClip._parent.next._alpha == 100) {
				
				var tx:Tween=new Tween(mc.menuItemContainer,"_y",Regular.easeIn,mc.menuItemContainer._y,(mc.menuItemContainer._y- mc.menuItemAndSepHeight),.8,true);
				tx.start();
				mClip._parent._parent._parent.isNextTransStart=true;
				tx.onMotionFinished=function(obj:MovieClip){
					mClip._parent._parent._parent.isNextTransStart=false;
					
					delete obj;
				};
				
				//mc.menuItemContainer._y -= mc.menuItemAndSepHeight;
				
			}
			
		}
	}
	private function nextItemOnReleaseHandler():Void {
		clearInterval(this._parent._parent._parent.prevTimerInterval);
		if(this._parent._parent._parent.isNextTransStart==false and this._parent._parent._parent.isPrevTransStart==false){
			var mc:MovieClip = this._parent._parent._parent;
			if (mc.menuItemContainer._y>((mc.menuItemAndSepHeight*mc.count-mc.menuHolderHeight)*-1)) {
				this._parent.next._alpha = 100;
			} else {
				this._parent.prev._alpha = 100;
				this._alpha = 50;
				clearInterval(this._parent._parent._parent.nextTimerInterval);
			}
			if (this._alpha == 100) {
				mc.menuItemContainer._y -= mc.menuItemAndSepHeight;
				this._parent.prev.gotoAndStop(2);
			}
			
		}
	}
	private function prevItemOnRollOverHandler(mClip:MovieClip):Void {
		clearInterval(mClip._parent._parent._parent.nextTimerInterval);
		if(mClip._parent._parent._parent.isNextTransStart==false and mClip._parent._parent._parent.isPrevTransStart==false){
			var mc:MovieClip = mClip._parent._parent._parent;
			    
				if ((mc.menuItemContainer._y)<0) {
					mClip._parent.next._alpha = 100;
					mClip.next.gotoAndStop(1);
				} else {
					trace("Finished "+mClip._parent.prev);
					mClip._parent.next._alpha = 100;
					mClip._parent.prev._alpha = 50;
					mClip._parent.prev.gotoAndStop(1);
					 clearInterval(mClip._parent._parent._parent.prevTimerInterval);
				}
				if (mClip._parent.prev._alpha == 100) {
					var tx:Tween=new Tween(mc.menuItemContainer,"_y",Regular.easeIn,mc.menuItemContainer._y,(mc.menuItemContainer._y+ mc.menuItemAndSepHeight),.8,true);
					tx.start();
					mClip._parent._parent._parent.isPrevTransStart=true;
					tx.onMotionFinished=function(obj:MovieClip){
						mClip._parent._parent._parent.isPrevTransStart=false;
						delete obj;
					};
					//mc.menuItemContainer._y += mc.menuItemAndSepHeight;
				}
		}
				
	}
	private function prevItemOnReleaseHandler():Void {
		clearInterval(this._parent._parent._parent.nextTimerInterval);
		if(this._parent._parent._parent.isNextTransStart==false and this._parent._parent._parent.isPrevTransStart==false){
			var mc:MovieClip = this._parent._parent._parent;
			if ((mc.menuItemContainer._y)<0) {
				this._parent.next._alpha = 100;
			} else {
				this._parent.next._alpha = 100;
				this._alpha = 50;
				 clearInterval(this._parent._parent._parent.prevTimerInterval);
			}
			if (this._alpha == 100) {
				mc.menuItemContainer._y += mc.menuItemAndSepHeight;
			}
			
		}
	}
	/*"left"--The paragraph is left-aligned. 
	"center"--The paragraph is centered. 
	"right"--The paragraph is right-aligned. 
	"justify"--The paragraph is justified*/
	public function Menu() {
		init();
	}
	
	public function addItem(label:String,id:Number,categoryId:Number, fn:Function, align:String, indent:Number, bold:Boolean, italic:Boolean, color:Number, bgColor:Number, rollOverColor:Number, rollOutColor:Number, bgRollOverColor:Number, bgRollOutColor:Number):Void {
		var txtFmt:TextFormat = new TextFormat();
		if (indent != null and indent != undefined) {
			txtFmt.indent = indent;
		}
		if (bold != null and bold != undefined) {
			txtFmt.bold = bold;
		}
		if (italic != null and italic != undefined) {
			txtFmt.italic = italic;
		}
		if (align != null and align != undefined and (align == "left" || align == "right" || align == "center" || align == "justify")) {
			txtFmt.align = align;
		}
		if (color != null and color != undefined) {
			txtFmt.color = color;
		}
		
		var menuItem:MovieClip = menuItemContainer.attachMovie("MenuItem", "MenuIem"+count, menuItemContainer.getNextHighestDepth());
		menuItem._x = startX;
		menuItem._y = currentY;
		menuItem.menulbl.text = label;
		menuItem.label=label;
		menuItem.id=id;
		menuItem.categoryId=categoryId;
		if (bgColor != null and bgColor != undefined) {
			menuItem.bgColor = bgColor;
		}
		if (rollOutColor != null and rollOutColor != undefined) {
			menuItem.rollOutColor = rollOutColor;
			txtFmt.color=rollOutColor;
		}
		if (rollOverColor != null and rollOverColor != undefined) {
			menuItem.rollOverColor = rollOverColor;
		}
		if (bgRollOutColor != null and bgRollOutColor != undefined) {
			menuItem.bgRollOutColor = bgRollOutColor;
		}
		if (bgRollOverColor != null and bgRollOverColor != undefined) {
			menuItem.bgRollOverColor = bgRollOverColor;
		}
		menuItem.menulbl.setTextFormat(txtFmt);
		menuItem.beginFill(bgColor,100);
			menuItem.moveTo(2,2);
			menuItem.lineTo(menuItem._width-2,2);
			menuItem.lineTo(menuItem._width-2,menuItem._height);
			menuItem.lineTo(2,menuItem._height);
			menuItem.lineTo(2,2);
		menuItem.style = txtFmt;
		menuItem.onRelease =function(){ 
		fn(this);
		this._parent._parent._parent.hideMenu();
		};
		menuItem.onRollOver = function() {
			var txtFmt:TextFormat = this.style;
			txtFmt.color = this.rollOverColor;
			this.menulbl.setTextFormat(txtFmt);
			
			this.beginFill(this.bgRollOverColor,100);
			this.moveTo(2,2);
			this.lineTo(this._width-2,2);
			this.lineTo(this._width-2,this._height);
			this.lineTo(2,this._height);
			this.lineTo(2,2);
			
			
		};
		menuItem.onRollOut = function() {
			var txtFmt:TextFormat = this.style;
			txtFmt.color = this.rollOutColor;
			this.menulbl.setTextFormat(txtFmt);
			this.beginFill(this._bgRollOutColor,100);
			this.moveTo(2,2);
			this.lineTo(this._width-2,2);
			this.lineTo(this._width-2,this._height);
			this.lineTo(2,this._height);
			this.lineTo(2,2);
			if(this._xmouse<-1 or this._xmouse>this._width)
			  	this._parent._parent._parent.hideMenu();
			
			
		};
		var separator:MovieClip = menuItemContainer.attachMovie("separator", "separator"+count, menuItemContainer.getNextHighestDepth());
		separator._x = startX+8;
		separator._y = currentY+menuItemHeight;
		currentY += menuItemAndSepHeight;
		count++;
		setPostions();


	}
	public function showMenu():Void {
		trace("show Menu : "+this._name );
		if(visible==false){
		if (count>noOfItemsDisplayed) {
			menuFooter.next._visible = true;
			menuFooter.prev._visible = true;
			menuFooter.next._alpha = 100;
			menuFooter.prev._alpha = 50;
		}
		menuItemContainer._y = 0;
		var tx:Tween = new Tween(mc, "_y", Regular.easeOut, ((menuHolderHeight+menuFooterHeight)*-1), 0, 1.8, true);
		tx.start();
		tx.onMotionFinished=function(obj:MovieClip){
			delete obj;
		};
		visible=true;
		}
	}

	public function hideMenu():Void {
		if(visible==true){
		var tx:Tween = new Tween(mc, "_y", Regular.easeOut, 0, ((menuHolderHeight+menuFooterHeight)*-1), 1.5, true);
		tx.start();
		tx.onMotionFinished=function(obj:MovieClip){
			delete obj;
		};
		visible=false;
		}
	}
	public function setNoOfItemsDisplayed(value:Number):Void {
		this.noOfItemsDisplayed = value;
		menuHolderHeight = menuItemAndSepHeight*this.noOfItemsDisplayed;
		menuContainer.mask_mc._height = menuItemAndSepHeight*noOfItemsDisplayed+menuFooterHeight;
	}
	public function getNoOfItemsDisplayed():Number {
		return this.noOfItemsDisplayed;
	}
	public function getNoOfItemsAdded(){
		return count;
	}
}