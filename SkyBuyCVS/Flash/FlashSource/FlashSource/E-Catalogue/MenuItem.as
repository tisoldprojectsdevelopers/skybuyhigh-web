﻿class MenuItem extends MovieClip {
	private var _label:String;
	private var _id:Number;
	private var _categoryId:Number;
	private var _rollOverColor:Number;
	private var _rollOutColor:Number;
	private var _bgRollOverColor:Number;
	private var _bgRollOutColor:Number;
	private var _bgColor:Number;
	private var _style:TextFormat;
	public function MenuItem() {
		/*this.rollOverColor = 0xEDEDED;
		this.rollOutColor = 0xEDEDED;*/
		style = new TextFormat();
	}
	public function set label(_label:String):Void {
		this._label = _label;
	}
	public function get label():String {
		return this._label;
	}
	public function set categoryId(_categoryId:Number):Void {
		this._categoryId = _categoryId;
	}
	public function get categoryId():Number {
		return this._categoryId;
	}
	public function set id(_id:Number):Void {
		this._id = _id;
	}
	public function get id():Number {
		return this._id;
	}
	public function set style(_style:TextFormat):Void {
		this._style = _style;
	}
	public function get style():TextFormat {
		return this._style;
	}
	public function set rollOverColor(_rollOverColor:Number):Void {
		this._rollOverColor = _rollOverColor;
	}
	public function get rollOverColor():Number {
		return this._rollOverColor;
	}
	public function set rollOutColor(_rollOutColor:Number):Void {
		this._rollOutColor = _rollOutColor;
	}
	public function get rollOutColor():Number {
		return this._rollOutColor;
	}
	
	public function set bgRollOverColor(_bgRollOverColor:Number):Void {
		this._bgRollOverColor = _bgRollOverColor;
	}
	public function get bgRollOverColor():Number {
		return this._bgRollOverColor;
	}
	public function set bgRollOutColor(_bgRollOutColor:Number):Void {
		this._bgRollOutColor = _bgRollOutColor;
	}
	public function get bgRollOutColor():Number {
		return this._bgRollOutColor;
	}
	public function set bgColor(_bgColor:Number):Void {
		this._bgColor = _bgColor;
	}
	public function get bgColor():Number {
		return this._bgColor;
	}
	/*public function onRollOver() {
	trace("RollOver");
	this.beginFill(this._rollOverColor,100);
	this.moveTo(0,0);
	this.lineTo(this._width,0);
	this.lineTo(this._width,this._height);
	this.lineTo(0,this._height);
	this.lineTo(0,0);
	}*/
	/*public function onRollOut() {
	trace("RollOut");
	this.beginFill(this._rollOutColor,20);
	this.moveTo(0,0);
	this.lineTo(this._width,0);
	this.lineTo(this._width,this._height);
	this.lineTo(0,this._height);
	this.lineTo(0,0);
	}*/
}