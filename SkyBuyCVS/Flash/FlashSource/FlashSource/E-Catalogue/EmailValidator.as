﻿class EmailValidator extends String {
	/*
	Requires:	ActionScript v. 2.0
	Date:		April 25, 2006
	Contact:	admin@flashmx.us
	Created by:	Nikolay Shishenkov
	==============================
	Idea: this class has 1 goal to allow you to verify email address string by basic email characteristics
	it is based on the email standards information that can be found here:
	http://www.remote.org/jochen/mail/info/chars.html
	==============================
	Usage: 
	//=> |START| copy from this line
	//={ Boolean => email_validator.validate(String); }=
	//Import class - in this example the class is located ubder the folder "classes" (relative to the project's FLA location)
	import classes.EmailValidator;
	//Create the validator object:
	var email_validator	= new EmailValidator();
	trace(email_validator.validate("admin@flashmx.us"));		// will return true
	trace(email_validator.validate("admin#@flashmx.us"));		// will return false - because the string contains invalid character(#)
	trace(email_validator.validate("admin@flashmx.tooLong"));	// will return false - because the domain name is invalid
	//... basicly it will return true ONLY if the email is complaint to the standard email address requirements
	//= final line to copy =| END |
	*/
	var ASCII_ALLOWED:Array	= [[38,39],[42,43],[45,57],[61,61],[63,63],[65,90],[94,95],[97,123],[125,126]];
	//The list of all allowed ASCII Chars ranges for standard email address
	//(including some special chars that are allowed in some cases such as {'}` etc.)
	var i:Number, j:Number, email_arr:Array, after_arr:Array, str_len:Number;
	//
	function validate(inputString:String):Boolean{
		//
		if(inputString.length<1)return false;
		//@
		email_arr	= inputString.split("@");
		if(email_arr.length!=2) return false; //	Only one @ is allowed
		//.
		after_arr	= email_arr[1].split(".");
		if(after_arr.length<2) return false;
		if(after_arr[after_arr.length-1].length<2 || after_arr[after_arr.length-1].length>4)
			return false; //	Only one . is allowed and after the . you can have between 2 and 4 chars
		//
		if(!ValidateString(email_arr[0].toString())||!ValidateString(email_arr[1].toString()))
			return false;	// the email contains invalid characters
		//
		return true;	// this is a valid email
	}
	//
	function ValidateString(_str:String):Boolean{
		str_len	= _str.length;
		//
		for(i=0;i<str_len;i++)
			if(!CheckUsedCharacter(_str.charCodeAt(i)))
				return false; // the strin contains atleast one invalid chr
		//
		return true;	// all chars are good
	}
	//
	function CheckUsedCharacter(_chr:Number):Boolean{
		for(j=0;j<ASCII_ALLOWED.length;j++)
			if(_chr>=ASCII_ALLOWED[j][0] && _chr<=ASCII_ALLOWED[j][1])
				return true;	// the caracter falls into one of the allowed areas
		//
		return false;	// The used character is not allowed.
	}
}