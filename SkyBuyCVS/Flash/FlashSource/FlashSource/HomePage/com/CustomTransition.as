﻿package com
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class CustomTransition extends EventDispatcher
	{
		private var proMc:MovieClip;
		private var type:String;
		private var begin:Number;
		private var end:Number;
		private var tftimer:Timer;
		public function CustomTransition(aTarget:MovieClip, aType:String, aFrom:Number, aTo:Number) {
			proMc = aTarget;
			type = aType;
			begin = aFrom;
			end = aTo;
			switch (type) {
				case 'alpha':
				proMc.alpha = begin;
				break;
				//
				case 'y':
				proMc.y = aFrom 
				break;
			}
			tftimer = new Timer(20);
			tftimer.addEventListener(TimerEvent.TIMER, applyEffects)
			tftimer.start();
		}
		private function applyEffects(evt:TimerEvent) {
			var incOrDec:int = begin > end  ? -1: 1;
			switch (type) {
				case 'alpha':
				proMc.alpha = proMc.alpha + (incOrDec * 0.05);
				if (proMc.alpha >= end && incOrDec == 1) {
					tftimer.stop();
					evt.stopImmediatePropagation();
					dispatchEvent(new Event(Event.COMPLETE));
					
				}else if (proMc.alpha <= end && incOrDec == -1) {
					tftimer.stop();
					evt.stopImmediatePropagation();
					dispatchEvent(new Event(Event.COMPLETE));
				}
				break;
				//
				case 'y':
				proMc.y = proMc.y + (incOrDec * 1);
				if (proMc.y >=end) {
					tftimer.stop();
					evt.stopImmediatePropagation();
					dispatchEvent(new Event(Event.COMPLETE));
				}
				break;
			}
		}
	}
}