﻿package com
{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.*;
	import flash.text.TextFormat;
	import flash.net.URLVariables;
	import flash.display.SimpleButton;
	import com.MyLocale;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Contactus extends MovieClip  
	{
		var txtFormat:TextFormat
		var nxml:XML = MyLocale.getInstance().nxml;
		var msgSend:String 
		
		public function Contactus() {
			mcCancle.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCancle);
			mcSend.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSend);
			msg.btnOk.addEventListener(MouseEvent.MOUSE_DOWN, hdlrMsgok);
		}
		private function hdlrCancle(evt:MouseEvent):void {
			trace(nxml.menu.children()[5].@reqURL, '>>>>>>>>>>>>>>>>>>>>>>>>>>')
			MovieClip(parent.parent).loadMovies('swf', 'swf/home', 0);
		}
		private function hdlrSend(evt:MouseEvent):void {
			txtFormat = new TextFormat();
			txtFormat.color = 0x003366;
			txtFormat.bold = true;
			msgSend = 'Failed';
			if ((Trim(form.txtContent.text).length > 0 || Trim(form.txtExperience.text).length > 0) && Trim(form.txtEmail.text).length > 0 && Trim(form.txtName.text).length > 0) {
				var variables:URLVariables = new URLVariables();
				variables.action = 'send';
				variables.name = form.txtName.text;
				variables.email = form.txtEmail.text;;
				variables.content = form.txtContent.text;
				variables.experience = form.txtExperience.text;
				variables.orderno = form.txtOrderno.text;
				//
				try {
					var request:URLRequest = new URLRequest(nxml.menu.children()[5].@reqURL)
					var ldr:URLLoader = new URLLoader();
					ldr.dataFormat = URLLoaderDataFormat.VARIABLES;
					request.data = variables;
					request.method = URLRequestMethod.POST;
					//
					ldr.addEventListener(Event.COMPLETE, handleComplete);  
					ldr.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				
					ldr.load(request)
				}catch (err:Error) {
					trace('URL Not loaded')
				}
				
				//
				
			}else {
				 msg.visible = true;
				 msg.msg.text = "Please enter a  name, valid e-mail address and comments ";
				 txtFormat.color = 0xFF0000;
				 txtFormat.bold = false;
				 msg.msg.setTextFormat(txtFormat)
			}
		}
		//
		private function handleComplete(event:Event):void {  
			var loader:URLLoader = URLLoader(event.target);  
			if (Trim(loader.data.output) == 'sent') {
				 msg.visible = true;
				 msg.msg.text = "Thank You.";
				 txtFormat.color = 0x003300;
				 txtFormat.bold = false;
				 msg.msg.setTextFormat(txtFormat);
				 msgSend = 'success'
			}else{
				 msg.visible = true;
				 msg.msg.text = "Please enter a valid e-mail address";
				 txtFormat.color = 0xFF0000;
				 txtFormat.bold = false;
				 msg.msg.setTextFormat(txtFormat);
			}
		}  
		private function onIOError(event:IOErrorEvent):void {  
			trace("Error loading URL.");  
		}  
		//
		private function hdlrMsgok(evt:MouseEvent):void {
			if (msgSend == 'success') {
				MovieClip(parent.parent).loadMovies('swf', 'swf/home', 0);
			}
			msg.visible = false;
		}
		//
		function Trim(str:String):String{
			for(var i = 0; str.charCodeAt(i) < 33; i++);
			for(var j = str.length-1; str.charCodeAt(j) < 33; j--);
			return str.substring(i, j+1);
		}
	}
}