﻿package com
{
	import flash.display.MovieClip;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.*;
	import flash.events.*;
	/**
	 * ...
	 * @author ... Karthikeyan
	 * @date 16/09/09
	 */
	public class Content extends MovieClip
	{
		public var num:int ;
		public var id:String;
		private var myLocal:MyLocale;
		private var myString:String;
		private var notesScroller:MovieClip;
		public function Content() {
			myLocal = MyLocale.getInstance();
			addEventListener(Event.ADDED_TO_STAGE, hdlrAddetoStage);
		}
		private function hdlrAddetoStage(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrAddetoStage);
			myString = myLocal.getContentData(id);
			formatTexts(myString);
		}
		//Format texts
		private function formatTexts(aStr:String):void {
			var supStartExpression:RegExp = new RegExp("<sup>", "g");
			var supEndExpression:RegExp = new RegExp("</sup>", "g");
			aStr = aStr.replace(supStartExpression, "<font face=\"GG Superscript\" size=\"16\">");
			aStr = aStr.replace(supEndExpression, "</font>");
			assignText(aStr);
		}
		//Assign texts to Text Box
		private function assignText(aStr:String):void {
			txt_header.text = myLocal.getContentHeader(id)
			sNode.tf.embedFonts = true;
			sNode.tf.autoSize = TextFieldAutoSize.LEFT;			
			sNode.tf.embedFonts = true;
			sNode.tf.htmlText = aStr;
			assignScroll();
		}
		//Assign Scroll mc
		private function assignScroll() {
			notesScroller=new ScrollMc(sNode,300);
			notesScroller.x=72;
			notesScroller.y=220;
			notesScroller.name="notesScroller";
			notesScroller.refreshScroll();
			addChild(notesScroller);
			notesScroller.refreshScroll();
			addEventListener(TextEvent.LINK, linkHandler);
		}
		//init Links
		private function linkHandler(str:TextEvent):void {
			if (str.text == 'skybuyhigh') {
				MovieClip(parent.parent).showBackBtn('movieclip', '/' + id + '/', 1);
				SWFAddress.setValue('/contact_us/');
			} else if (str.text =='demo') {
				navigateToURL('http://www.skybuyhigh.com/service/generateCatalogue.do');
			} else if (str.text == 'program') {
				MovieClip(parent.parent).showBackBtn('movieclip', '/' + id + '/', 1);
				SWFAddress.setValue('/programs/operators_brokers/');
				
			}else if (str.text == 'link1') {
				navigateToURL('http://www.nbaa.org/2009');
			
			}else if (str.text == 'link2') {
				navigateToURL('http://www.prweb.com/releases/revolution_air/charter_flights/prweb2319424.htm');
			} 
		}
		//
		private function navigateToURL(url:String):void {
			SWFAddress.href(url, '_blank');
			//navigateURL(new URLRequest(url), '_blank');
		}
	}
}