﻿package com
{
	import flash.display.MovieClip;
	import flash.display.LoaderInfo;
	import flash.display.SimpleButton;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.display.Loader;
	import flash.text.TextField
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	import fl.transitions.Tween;
	import fl.transitions.easing.*
	//
	import com.google.analytics.AnalyticsTracker;
	import com.google.analytics.GATracker;
	//
	import com.SWFAddress;
	import com.SWFAddressEvent;
	//
	import com.Menu;
	/**
	 * ...
	 * * @author Karthikeyan.J.R
	 * * @Date 01/09/09
	 */
	public class  Index extends MovieClip
	{
		public var toc:Menu;
		private var btnBack:BackBtn;
		private var sel:int
		private var ldr:Loader;
		public var allMenus:Array
		//Object added to Stage
		public function Initialize():void {
			mcMenu.gotoAndPlay('ply');
			mcMenu.addEventListener(Event.COMPLETE, addMenuBut);
			//
		}
		//Adding Title Buttons
		private function addMenuBut(evt:Event):void {
			allMenus = new Array();
			toc = new Menu();
			toc.addEventListener('menuCreated', menuCreated);
			mcMenu.addChild(toc);
		}
		//
		private function menuCreated(evt:Event):void {
			SWFAddress.addEventListener(SWFAddressEvent.CHANGE, handleSWFAddress);
		}
		public function loadMovies(aType:String, aUrl:String, aSel:int, aBack:Boolean = false, _name:String = null):void {
			var url:URLRequest; 
			if ((_name == '/demo_catalogue/' && aType == 'uri')) {
				trackers.trackPageview(aUrl);
			}else if (_name != '/page_not_found/') {
				trackers.trackPageview('#'+_name);
			}
			if (ldr) {
				if (ldr.hasEventListener(Event.COMPLETE)) {
					ldr.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, hdlrProgress);
					ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, hdlrComplete);
					ldr.close()
				}
			}
			if (aSel < 6) {
				if (aType != 'url') {
					toc.reset()
					toc.setFormat(toc.arr_level1[aSel], 0xb72b2e);
					toc.arr_level1[aSel].base.gotoAndStop(3);
				}
			}
			if (aType == 'movieclip') {
				unloadMov();
				try {
					trace(_name.substr(1, _name.length -2))
					var aMc:MovieClip 
					if (_name != '/contact_us/' && _name != '/page_not_found/') {
						aMc  = new Content();
						(aMc).id  = _name.substr(1, _name.length -2);
					}else {
						var Cls:Class = getDefinitionByName(aUrl) as Class;
						aMc = new  Cls () as MovieClip;
					}
					mcLoader.addChild(aMc);
					mcLoader.alpha = 0;
					var twn:CustomTransition = new CustomTransition(mcLoader, 'alpha', 0, 1);
				}catch (err:Error) {
					trace('Error');
				}
			}else if (aType == 'url') {
				try {
					url = new URLRequest(aUrl);
					navigateToURL(url, '_blank');
				}catch (err:Error){
					trace('Error');
				}
			}else if (aType == 'swf') {
				if (aUrl) {
					unloadMov() ;
					url = new URLRequest(aUrl+'.swf');
					ldr = new Loader();
					mcLoader.alpha = 0;
					progressBar.visible = true;
					progressBar.bar.scaleX = 0;
					ldr.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, hdlrProgress);
					ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrComplete);
					try {
						ldr.load(url);
						mcLoader.addChild(ldr);
					}catch (err:Error) {
						trace('Error')
					}
				}
			}
		}
		//
		private function hdlrProgress(evt:ProgressEvent):void {
			var per:Number = evt.bytesLoaded/evt.bytesTotal ;
			progressBar.pc_txt.text = Math.round(per *100) +' %';
			MovieClip(progressBar.bar).scaleX = per; 
		}
		//
		private function hdlrComplete(evt:Event):void {
			trace('Completeeee')
			progressBar.visible = false;
			if (ldr) {
				var twn:CustomTransition = new CustomTransition(mcLoader, 'alpha', 0, 1);
			}
			
		}
		//
		public function unloadMov() {
			if (mcLoader.numChildren > 0 ) {
				if (ldr) {
					ldr = null;
				}
				progressBar.visible = false;
				mcLoader.removeChildAt(0); 
			}
		}
		//Show Back button
		public function showBackBtn(aType:String, aURL:String, aSel:int):void {
			removeBackBtn();
			btnBack = new BackBtn();
			btnBack.x = 975;
			btnBack.y = 550;
			btnBack.type = aType;
			btnBack.uri = aURL;
			btnBack.sel = aSel;
			btnBack.visible = true;
			btnBack.buttonMode = true;
			btnBack.gotoAndStop(1);
			btnBack.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackPress);
			this.addChild(btnBack);
		}
		private function hdlrBackPress(evt:MouseEvent):void {
			SWFAddress.setValue(evt.target.uri);
			btnBack.removeEventListener(MouseEvent.MOUSE_DOWN, hdlrBackPress);
			removeBackBtn();
		}
		//Remove Back Btn
		private function removeBackBtn() {
			if (btnBack) {
				if (this.contains(btnBack)) {
					this.removeChild(btnBack);
					btnBack = null;
				}
			}
		}
		//Triming the String
		public function Trim(str:String){
			for (var i = 0; str.charCodeAt(i) < 33; i++) 
			for (var j = str.length-1; str.charCodeAt(j) < 33; j--) 
			return str.substring(i,j + 1);
		}
		//Init Links
		public function initLinks():void {
			for (var i = 1 ; i < 5; i++ ) {
				footer_mc['link_' + i].focusRect = false;
				footer_mc['link_' + i].addEventListener(MouseEvent.MOUSE_DOWN, goLinkPlace);
			}
		}
		private function goLinkPlace(evt:MouseEvent):void {
			var uriArr:Array = new Array(0, 'airline_login', 'vendor_login', 'customer_login', 'face_book');
			try {
				var uris:String = uriArr[evt.target.name.split('_')[1]];
				SWFAddress.href(MyLocale.getInstance().getLinks(uris), '_blank');
				//navigateToURL(new URLRequest(MyLocale.getInstance().getLinks(uris)), '_blank');
			}catch (err:Error) { 
				trace('Error In url')
			}
		}
		//
		public function removeBackMc(aBool:Boolean):void {
			if (!aBool) {
				removeBackBtn();
			}
		}
		//Setting the URL Paths Google Anylatics
		public function handleSWFAddress(e:SWFAddressEvent):Boolean {
			var mc:MovieClip;
			for (var i = 0; i < allMenus.length; i++) {
				if (e.value == '/') {
					mc = allMenus[0] as MovieClip;
					loadMovies(mc.type, mc.uri, mc.id, mc.back, mc.name);
					return true 
				}else {
					mc = allMenus[i] as MovieClip
				}
				try {
					if (mc.name == e.value) {
						loadMovies(mc.type, mc.uri, mc.id, mc.back, mc.name);
						return true;
					}
				}catch (err:Error) {
					throw(err);
				}
			}
			loadMovies('movieclip', 'Page_not_found', 7, false, '/page_not_found/')
			return false;
		}
		// Pusing submenus in to Array
		public function pushAllMenus(aMc:MovieClip):Boolean {
			for each( var mc in allMenus) {
				if (mc.name == aMc.name ) {
					return false;
				}
			}
			allMenus.push(aMc);
			return true;
		}
	}
}