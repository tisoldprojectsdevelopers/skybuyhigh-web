﻿package com
{
	import flash.events.*
	import flash.net.*;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class MyLocale extends EventDispatcher
	{
		public static var _instance:MyLocale;
		public var nxml:XML;
		public static function getInstance():MyLocale {
			if (_instance == null) {
				_instance = new MyLocale();
				return _instance;
			}
			return _instance
		}
		public function getContent() :void {
			var ldr:URLLoader = new URLLoader();
			var req:URLRequest = new URLRequest('xml/Toc.xml');
			ldr.load(req);
			ldr.addEventListener(Event.COMPLETE, hdlrXMLLoaded);
		}
		//XML Loader Complete handler
		private function hdlrXMLLoaded(evt:Event):void {
			nxml =  new XML(evt.target.data);
			dispatchEvent(new Event('XMLLOADED'));
		}
		//Get contents 
		public function getContentData(id:String):String {
			var str:String;
			if (nxml) {
				str = nxml.content.contents.(@id==id)[0] as XML
			}
			return String(str) +''
		}
		//Get content headers
		public function getContentHeader(id:String):String {
			var str:String;
			if (nxml) {
				str = nxml.content.contents.(@id==id).@header 
			}
			return String(str) +''
		}
		//Get bottom Links
		public function getLinks(id:String):String {
			var str:String;
			if (nxml) {
				str = nxml.links.link.(@id==id)[0] as XML
			}
			return String(str) +''
		}
		//Get Tracker Id
		public function getTrackerId():String {
			var str:String
			if (nxml) {
				str = nxml.tracker[0] as XML
			}
			return String(str) +'';
		}
	
	}
}