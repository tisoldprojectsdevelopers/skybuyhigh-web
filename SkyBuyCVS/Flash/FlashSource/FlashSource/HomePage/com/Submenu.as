﻿package com
{
	import fl.motion.Tweenables;
	import fl.transitions.TweenEvent;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.text.*;
	
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class  Submenu extends MovieClip
	{
		private var target:MovieClip;
		private var subMenu:SubmenuBtn;
		private var subMenuCont:MovieClip
		private var menu_arr:Array
		private var tweenSt:Tween;
		private var tweenEnd:Tween
		var tfFormat:TextFormat;
		var showMenu:Boolean
		public function Submenu(aTarget:MovieClip , aBool:Boolean)
		{
			addEventListener(Event.ADDED_TO_STAGE, subMenuAdded);
			target = aTarget;
			showMenu = aBool;
		}
		//Sub Menu added to stage
		private function subMenuAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, subMenuAdded);
			//
			createSubMenu();
		}
		private function createSubMenu() {
			var len:int = target.xmlObj.children().length();
			var yy:Number = 0;
			subMenuCont = new MovieClip();
			menu_arr = new Array();
			for (var i = 0; i < len; i++) {
				subMenu = new SubmenuBtn();
				subMenu.x = -5
				subMenu.sText = target.xmlObj.sub_menu[i].@txt;
				subMenu._txt.text = target.xmlObj.sub_menu[i].@txt
				subMenu.base.width = target.base.width;
				subMenu._txt.width = target.base.width - 22;
				subMenu.deepLink = target.name  + target.xmlObj.sub_menu[i].@txt.toLowerCase()+'/';
				
				subMenu.y = yy;
				subMenu.xmlObj = target.xmlObj.sub_menu[i];
				//Menu Properties for load Movie
				subMenu.type = target.xmlObj.sub_menu[i].@type;
				subMenu.uri =  target.xmlObj.sub_menu[i].@typeId
				subMenu.id = target.id
				subMenu.back = false;
				subMenu.name = target.name + target.xmlObj.sub_menu[i].@broDisName.toLowerCase() + '/';
				//
				var div:Divider = new Divider();
				div.mouseEnabled = false;
				div.width = subMenu.base.width;
				div.x = 3;
				div.y = subMenu.height;
				
				subMenu.addChild(div);
				subMenu.gotoAndStop(1);
				
				menu_arr.push(subMenu);
				
				subMenuCont.addChild(subMenu); 
				yy = subMenu.y + subMenu.height;
				MovieClip(parent.parent.parent.parent).pushAllMenus(subMenu);
			}
			if (showMenu) {
				addChild(subMenuCont);
				createMask();
				//subMenuCont.y = - subMenuCont.height;
				tweenSt = new Tween(subMenuCont, 'y', Strong.easeIn, - subMenuCont.height, target.base.y,  0.2, true);
				tweenSt.addEventListener("motionFinish", tweenStartCom);
				tweenSt.start();	
			}
			
		}
		//
		private function tweenStartCom(evt:Event):void {
			for each(var mc in menu_arr) {
				mc.addEventListener(MouseEvent.MOUSE_DOWN, fnSubmenuDown);
				mc.addEventListener(MouseEvent.ROLL_OVER, fnRollOver);
				mc.addEventListener(MouseEvent.ROLL_OUT, fnRollOut);
			}
		}
		//
		private function fnRollOver(evt:MouseEvent):void {
			evt.target.base.gotoAndStop(2)
			setFormat(evt.target as MovieClip, 0xb72b2e);
		}
		private function fnRollOut(evt:MouseEvent):void {
			evt.target.base.gotoAndStop(1)
			setFormat(evt.target as MovieClip, 0x5D5D5D);
		}
		private function fnSubmenuDown(evt:MouseEvent):void {
			SWFAddress.setValue(evt.target.name);
			MovieClip(parent.parent.parent.parent).removeBackMc(evt.target.back)
			//MovieClip(parent.parent.parent.parent).loadMovies(evt.target.type, evt.target.uri, evt.target.id,  evt.target.back, evt.target.name);
			killMe();
		}
		private function createMask() {
			var sp:Sprite = new Sprite();
			sp.graphics.beginFill(0x00ff00, 0.5);
			sp.graphics.drawRect(0, 0, this.width, this.height);
			sp.graphics.endFill();
			addChild(sp); 
			subMenuCont.mask = sp;
		}
		public function killMe() {
			tweenEnd = new Tween(subMenuCont, 'y', Strong.easeIn,  target.base.y, - subMenuCont.height, 0.2, true);
			tweenEnd.addEventListener("motionFinish", tweenComp);
			tweenEnd.start();
		}
		private function tweenComp(evt:Event):void {
			try {
				tweenEnd.stop();
				evt.target.removeEventListener("motionFinish", tweenComp);
				//target.base.gotoAndStop(1);
				target.open = false;
				parent.removeChild(this);
			}catch (e:Error) {
				trace(e)
			}
		}
		//
		private function setFormat(target:MovieClip, col:int):void {
			var tf:TextFormat = new TextFormat();
			tf.color = col;
			target._txt.setTextFormat(tf);
		}
	}
	
}