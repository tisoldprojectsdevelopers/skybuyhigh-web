﻿package com
{
	import fl.transitions.TweenEvent;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent
	import fl.transitions.easing.*;
	import flash.text.TextFieldAutoSize;
	import flash.events.MouseEvent;
	import com.MyLocale;
	import flash.text.TextFormat;
	//
	import com.SWFAddress;
	import com.SWFAddressEvent;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class  Menu extends MovieClip
	{
		public var nxml:XML;
		private var len:int;
		private var count:int
		private var xx:Number = 0;
		public var subMenuArr:Array
		private var subMenu:Submenu
		public var arr_level1:Array
		public function Menu () {
			addEventListener(Event.ADDED_TO_STAGE, hdlrMenuAdded);
		}
		//Checks Menu Added
		private function hdlrMenuAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrMenuAdded);
			loadXML();
		}
		//XML Loader Complete handler
		private function loadXML():void {
			nxml = MyLocale.getInstance().nxml;
			len = nxml.menu.children().length();
			arr_level1 = new Array();
			createLevel1();
			subMenuArr = new Array();
		}
		//Creating First Level Script
		private function createLevel1() {
			for (var i = 0; i < len; i++ ) {
				var mcMenu:Menubtn = new Menubtn();
				count = i;
				mcMenu.id = count;
				mcMenu.base.width = nxml.menu.menus[count].@width;
				mcMenu.sText = nxml.menu.menus[count].@txt.toUpperCase();
				mcMenu._txt.text = nxml.menu.menus[count].@txt.toUpperCase();
				mcMenu.xmlObj =  nxml.menu.menus[count];
				mcMenu._txt.width = mcMenu.base.width;
				addChild(mcMenu);
				mcMenu.deepLink = '/' + nxml.menu.menus[count].@broDisName.toLowerCase() + '/';
				mcMenu.x = xx;
				//Menu Properties for load Movie
				mcMenu.type =  nxml.menu.menus[count].@type
				mcMenu.uri =  nxml.menu.menus[count].@typeId
				mcMenu.id = nxml.menu.menus[count].@id;
				mcMenu.back = false;
				mcMenu.name = '/'+nxml.menu.menus[count].@broDisName.toLowerCase()+'/' ;
		
				// Positioning the Menu
				mcMenu.pos = xx;
				xx = mcMenu.x + mcMenu.width;
				//
				if (i<len -1) {
					var sep:Seprator = new Seprator();
					sep.x = xx;
					sep.mouseEnabled = false;
					addChild(sep);
				}
				arr_level1.push(mcMenu);
				MovieClip(parent.parent).pushAllMenus(MovieClip(mcMenu));
			}
			for each(var mc in arr_level1) {
				addListener(mc);
			}
			var twn:CustomTransition = new CustomTransition(MovieClip(parent.parent).footer_mc, 'alpha', 0, 1);
			twn.addEventListener(Event.COMPLETE, hdlrFootterCom);
			dispatchEvent(new Event('menuCreated'));
			//Static 
			addSubMenu(arr_level1[2], false);
			removeSubMenu(arr_level1[2]);
		}
		//
		private function hdlrFootterCom(evt:Event):void {
			MovieClip(parent.parent).initLinks();
		}
		//Adding Listeners
		public function addListener(mc:MovieClip):void {
			mc.buttonMode = true;
			MovieClip(mc).focusRect = false;
			mc.addEventListener(MouseEvent.MOUSE_DOWN, hdlrMouseDown);
			mc.addEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
			mc.addEventListener(MouseEvent.ROLL_OUT, hdlrRollOut);
		}
		//MenuBtn level_1 Down Handler
		private function hdlrMouseDown(evt:MouseEvent):void {
			if (evt.target.type != 'submenu') {
				MovieClip(parent.parent).removeBackMc(evt.target.back);
				SWFAddress.setValue(evt.target.name);
			}	
		}
		//MenuBtn level_1 Roll_over Handler
		private function hdlrRollOver(evt:MouseEvent):void {
			if (MovieClip(evt.target.base).currentFrame != 3) {
				MovieClip(evt.target.base).gotoAndStop(2);
				setFormat(evt.target as MovieClip, 0xb72b2e);
			}
			if (evt.target.type == 'submenu') {
				addSubMenu(evt.target as MovieClip, true);
				evt.target.removeEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
				evt.target.removeEventListener(MouseEvent.MOUSE_DOWN, hdlrMouseDown);
			}
			
		}
		//MenuBtn level_1 Roll_out Handler
		private function hdlrRollOut(evt:MouseEvent):void {
			if (MovieClip(evt.target.base).currentFrame != 3) {
				MovieClip(evt.target.base).gotoAndStop(1);
				setFormat(evt.target as MovieClip, 0x5D5D5D);
			}
			SWFAddress.resetStatus();
			if (evt.target.type == 'submenu') {
				removeSubMenu(evt.target as MovieClip);
				evt.target.addEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
			}
		}
		//Reset Level_1 butotns
		public function reset() {
			for each (var mc in arr_level1) {
				MovieClip(mc).base.gotoAndStop(1);
				setFormat(MovieClip(mc), 0x5D5D5D);
			}
		}
		//AddSubmenu
		private function addSubMenu(mc:MovieClip, aBool:Boolean):void {
			if (!mc.open) {
				subMenu = new Submenu(mc, aBool);
				subMenu.name = mc.name; 
				subMenu.y = mc.height - 1;
				mc.addChild(subMenu);
				mc.open = true;
			}
		}
		//RemoveSubmenu
		private function removeSubMenu(mc:MovieClip):void {
			if (subMenu) {
				if (mc.contains(subMenu)) {
					subMenu.killMe();
				}
			}
		}
		//
		public function setFormat(target:MovieClip, col:int):void {
			var tf:TextFormat = new TextFormat();
			tf.color = col;
			target._txt.setTextFormat(tf);
		}
		
	}
}