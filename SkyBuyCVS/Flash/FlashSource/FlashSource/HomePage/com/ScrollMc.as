﻿package com{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	import flash.system.ApplicationDomain;
	import flash.geom.Rectangle;
	//import tdi.loader.LoaderTdi;

	public dynamic class ScrollMc extends MovieClip {;

	public var contentMc:MovieClip;
	var maskMc:MovieClip;

	//variabl for loading assets
	//var myLoader:LoaderTdi;
	var thisClass:MovieClip;

	public static  const SCROLLED="hdlrScrolled";

	//variables for importing assets
	var upArrow:Class;
	var upArrowMc:MovieClip;
	var downArrow:Class;
	var downArrowMc:MovieClip;
	var knob:Class;
	var knobMc:MovieClip;
	var scrollBar:Class;
	var scrollBarMc:MovieClip;
	var container:ScrollMc;
	var mcHeight:Number;
	var mcWidth:Number;
	var scrollHeight:Number;
	var knobMcX:Number;
	var scrollcontentMc:MovieClip;
	var delta:Number;

	public var temp_target;

	public function ScrollMc(mc:MovieClip,Height:Number) {
		init();
		container=this;
		container.x=mc.x;
		container.y=mc.y;
		mcHeight=Height;
		mcWidth=mc.width;
		scrollcontentMc = mc;
		container.addChild(mc);
		mc.x=mc.y=0;
		importassets();
		/*
		myLoader=new LoaderTdi("scroll.swf",thisClass);
		myLoader.addEventListener(LoaderTdi.COMPLETE,importassets);
		myLoader.x=-100;
		myLoader.y=-100;
		myLoader.alpha = 0;
		scrollcontentMc.addChild(myLoader);
		*/
	}
	public function get actualHeight():Number {
		return mcHeight;
	}
	public function get tempTarget():* {
		return temp_target;
	}
	function init() {
		thisClass=this;
		this.addEventListener(MouseEvent.MOUSE_WHEEL, scrollContent);
	}
	function importassets() {
		//var temp=ApplicationDomain.currentDomain;
		var xpos=container.width;
		//scrollBar=temp.getDefinition("scrollBar")  as  Class;
		scrollBarMc= new ScrollscrollBar();
		scrollBarMc.name = "scrollBarMc";
		container.addChild(scrollBarMc);
		scrollBarMc.mouseChildren=false;
		//scrollBarMc.mouseEnabled=false;



		//knob=temp.getDefinition("knob")  as  Class;
		knobMc=new Scrollknob();
		knobMc.name = "knobMc";
		knobMc.mouseChildren=false;

		//downArrow=temp.getDefinition("downArrow")  as  Class;
		downArrowMc=new ScrolldownArrow();
		downArrowMc.name = "downArrowMc";
		downArrowMc.buttonMode = true;
		container.addChild(downArrowMc);

		//upArrow=temp.getDefinition("upArrow")  as  Class;
		upArrowMc=new ScrollupArrow();
		upArrowMc.name ="upArrowMc";
		upArrowMc.buttonMode = true;
		container.addChild(upArrowMc);

		downArrowMc.mouseChildren=false;
		upArrowMc.mouseChildren=false;


		if (mcHeight < (upArrowMc.height*2)) {
			mcHeight=upArrowMc.height*2 ;
			downArrowMc.x=upArrowMc.x=xpos +5;
			scrollHeight = 0;
		} else {
			mcHeight=mcHeight ;
			container.addChild(knobMc);
			scrollBarMc.x=downArrowMc.x=upArrowMc.x=xpos+5;
			scrollHeight=mcHeight - upArrowMc.height - downArrowMc.height;

			scrollBarMc.height=scrollHeight;
			//knobMc.y=
			scrollBarMc.y=upArrowMc.height;
			scrollBarMc.y= upArrowMc.height;
		}
		downArrowMc.y=scrollHeight + upArrowMc.height;

		createMask(0,0,mcWidth+5,mcHeight);
		maskMc.name = "maskMc";
		container.addChild(maskMc);
		scrollcontentMc.mask = maskMc;
		initializeScroll();
		refreshScroll();
		setChildIndex(knobMc,this.numChildren-1);
	}
	public function refreshScroll(pSkip:Boolean=false) {
		//Line added by for centering the knob at scroll
		knobMc.x = knobMcX;//scrollBarMc.x + (scrollBarMc.width/2) - (knobMc.width/2)
		scrollcontentMc.y=0
		//
		if (scrollcontentMc.height <= mcHeight) {
			upArrowMc.mouseEnabled=false;
			downArrowMc.mouseEnabled=false;
			downArrowMc.gotoAndStop(3);
			upArrowMc.gotoAndStop(3);
			upArrowMc.visible = downArrowMc.visible =scrollBarMc.visible =knobMc.visible =true;
			//knobMc.height = (1-((scrollcontentMc.height- mcHeight)/scrollcontentMc.height))*scrollHeight;
			knobMc.visible =false;
			scrollcontentMc.y =0;
			//Added by Mani
			scrollBarMc.visible=false;
			downArrowMc.visible=false;
			upArrowMc.visible=false;
			//*end

		} else {
			if (downArrowMc.currentFrame!=2) {
				downArrowMc.gotoAndStop(1);
			}
			if (upArrowMc.currentFrame!=2) {
				upArrowMc.gotoAndStop(1);
			}
			//upArrowMc.mouseChildren=true;
			upArrowMc.mouseEnabled=true;
			//downArrowMc.mouseChildren=true;
			downArrowMc.mouseEnabled=true;

			upArrowMc.visible = downArrowMc.visible =scrollBarMc.visible =knobMc.visible =true;
			//knobMc.height = (1-((scrollcontentMc.height- mcHeight)/scrollcontentMc.height))*scrollHeight;
			//trace(upArrowMc.visible,upArrowMc.x)
			//scrollTarget(this);
		}
		knobMcX=Math.round(scrollBarMc.x + (scrollBarMc.width/2) - (knobMc.width/2));
		knobMc.x = knobMcX
		var ratio = ((scrollHeight- knobMc.height)/(scrollcontentMc.height-mcHeight));
		knobMc.y =(ratio*Math.abs(scrollcontentMc.y))+upArrowMc.height;
	}
	public function initializeScroll() {
		if (scrollHeight!=0) {
			knobMc.buttonMode=true;
			knobMc.intY = knobMc.y;
			//Line added by for centering the knob at scroll
			knobMcX=Math.round(scrollBarMc.x + (scrollBarMc.width/2) - (knobMc.width/2));
			knobMc.x = knobMcX;
			///
			//knobMc.height = (1-((scrollcontentMc.height- mcHeight)/scrollcontentMc.height))*scrollHeight;
			this.addEventListener(MouseEvent.MOUSE_DOWN, knobInit);
			knobMc.addEventListener(MouseEvent.MOUSE_UP, knobUp);
			//knobMc.addEventListener(MouseEvent.MOUSE_DOWN, knobInit);
		}
		//upArrowMc.buttonMode = downArrowMc.buttonMode =true;
		upArrowMc.mouseChildren=false;
		downArrowMc.mouseChildren=false;
		upArrowMc.addEventListener(MouseEvent.CLICK, scrollContent);
		downArrowMc.addEventListener(MouseEvent.CLICK, scrollContent);
		scrollBarMc.addEventListener(MouseEvent.MOUSE_DOWN,scrollBarPress);
	}
	function scrollBarPress(evt:MouseEvent) {
		knobMc.y = container.mouseY-knobMc.height/2;
		if (scrollBarMc.y>knobMc.y) {
			knobMc.y = scrollBarMc.y;
		} else if (Math.round(scrollHeight-knobMc.height+knobMc.intY)<knobMc.y) {
			knobMc.y = Math.round(scrollHeight-knobMc.height+knobMc.intY);
		}
		knobMc.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_DOWN));
		this.stage.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_MOVE));
		knobMc.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));
	}
	function knobUp(event:MouseEvent) {
		dispatchEvent(new Event(SCROLLED));
	}
	function knobInit(event:MouseEvent) {
		var target =event.target;
		temp_target= target;
		if (target == knobMc || target == scrollBarMc) {
			target=knobMc;
			temp_target= target;
			this.stage.addEventListener(MouseEvent.MOUSE_UP, endScroll);
			this.stage.addEventListener(MouseEvent.MOUSE_MOVE, scrollContent);
			knobMc.startDrag(false,new Rectangle(knobMcX,target.intY,0,Math.round(scrollHeight-target.height)));
		} else {
			
		}
	}

	function scrollContent(event:MouseEvent) {
		var eventTarget;
		var target =event.target;

		if (event.type == "mouseWheel") {
			if (upArrowMc.mouseEnabled) {
				eventTarget = event.currentTarget;
				eventTarget.delta = event.delta;
				scrollTarget(eventTarget);
			}
		} else {
			eventTarget = event.target;
			scrollTarget(eventTarget);
		}
	}
	public function scrollTarget(target,fromMyPos:uint=0) {
		var eventTarget = target;
		var tempVal;
		var ratio;
		if (eventTarget == this) {
			var contentHeight = scrollcontentMc.height - mcHeight;
			tempVal = scrollcontentMc.y;
			if (eventTarget.delta > 0) {
				tempVal+=15;
				tempVal=(tempVal>0)?0:tempVal;
			} else if (eventTarget.delta < 0) {
				tempVal-=15;
				tempVal=(tempVal<=-contentHeight)?-contentHeight:tempVal;
			}
			scrollcontentMc.y=tempVal;
			//knobMc.height = (1-((scrollcontentMc.height- mcHeight)/scrollcontentMc.height))*scrollHeight;
			//if (scrollHeight!=0) {
			ratio = ((scrollHeight- knobMc.height)/(scrollcontentMc.height-mcHeight));
			knobMc.y =(ratio*Math.abs(scrollcontentMc.y))+upArrowMc.height;

			//}
		}
		//trace("sCROLL TARGET CALLED",scrollHeight)
		if ((scrollHeight!=0) && (fromMyPos==0)) {
			tempVal = knobMc.y;
			if (eventTarget == upArrowMc) {
				tempVal-=scrollHeight/10;
				knobMc.y =(tempVal < knobMc.intY )?knobMc.intY:tempVal;
			} else if (eventTarget == downArrowMc) {
				tempVal+=scrollHeight/10;
				knobMc.y =(tempVal > Math.round(scrollHeight-knobMc.height+knobMc.intY))?Math.round(scrollHeight-knobMc.height+knobMc.intY):tempVal;
			}
			ratio = ((scrollcontentMc.height-mcHeight)/(knobMc.height - scrollHeight));
			scrollcontentMc.y=ratio*(knobMc.y - knobMc.intY);
		} else {
			contentHeight = scrollcontentMc.height - mcHeight;
			tempVal = scrollcontentMc.y;
			if (eventTarget == upArrowMc) {
				tempVal+=15;
				tempVal=(tempVal>0)?0:tempVal;
			} else if (eventTarget == downArrowMc) {
				tempVal-=15;
				tempVal=(tempVal<=-contentHeight)?-contentHeight:tempVal;
			} else if (fromMyPos>0) {
				if (contentHeight>0) {
					tempVal = -contentHeight;
				}
			}
			scrollcontentMc.y=tempVal;
			if (scrollHeight!=0) {
				ratio = ((scrollHeight- knobMc.height)/(scrollcontentMc.height-mcHeight));
				knobMc.y =(ratio*Math.abs(scrollcontentMc.y))+upArrowMc.height;
			}
		}
	}
	public function endScroll(event:MouseEvent) {
		knobMc.stopDrag();
		this.stage.removeEventListener(MouseEvent.MOUSE_UP,endScroll);
		this.stage.removeEventListener(MouseEvent.MOUSE_MOVE,scrollContent);
		knobMc.removeEventListener(MouseEvent.MOUSE_MOVE,scrollContent);
	}
	function createMask(X:Number,Y:Number,Width:Number,Height:Number) {
		maskMc=new MovieClip  ;
		maskMc.graphics.lineStyle(1,0x006666);
		maskMc.graphics.beginFill(0x006666);
		maskMc.graphics.drawRect(X,Y,Width,Height);
		maskMc.graphics.endFill();
	}
}
}