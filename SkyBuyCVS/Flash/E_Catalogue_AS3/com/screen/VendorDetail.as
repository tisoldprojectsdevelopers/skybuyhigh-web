﻿package com.screen 
{
	
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.utils.CustomTween;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class VendorDetail extends MovieClip
	{
		
		public function VendorDetail() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			this.alpha = 0;
			var tween:CustomTween = new CustomTween(this, 'alpha', 0, 1, 0.3, true);
			tween.start();
			closewindow.addEventListener(MouseEvent.MOUSE_DOWN, killMe);
		}
		public function LoadVendorDetails(aUrl:String):void {
			loader.LoadImage(aUrl);
			this.setChildIndex(closewindow, this.numChildren - 1);
		}
		private function killMe(evt:MouseEvent):void {
			closewindow.removeEventListener(MouseEvent.MOUSE_DOWN, killMe);
			var tween:CustomTween = new CustomTween(this, 'alpha',1, 0, 0.3, true);
			tween.addEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenComplete);
			tween.start();
		}
		private function hdlrTweenComplete(evt:Event):void {
			evt.target.removeEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenComplete);
			parent.removeChild(this);
			delete (this);
		}
	}
}