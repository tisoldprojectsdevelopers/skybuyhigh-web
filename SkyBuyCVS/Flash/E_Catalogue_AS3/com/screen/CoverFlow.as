﻿package com.screen{
	import com.document.Main;
	import com.utils.ImageLoader;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import com.mylocale.Myxml;
	import flash.display.Shape;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class CoverFlow extends MovieClip {
		private var count:int;
		private var tween:Tween;
		private var currentImgId:int = -1;
		//
		private var container:MovieClip;
		private var subContainer:MovieClip;
		public var disObjArr:Array;
		private var imgContArr:Array;
		private var isMove:Boolean = true;
		//
		public var currSeqId:int;
		public var currOwnerId:String;
		//
		public var currentItem:Object;
		//
		private var noVendorPro:ProductNotFound;
		//
		private var refObj:Object;
		public function CoverFlow() {
			addEventListener(Event.ADDED_TO_STAGE, coverFlowAddedToStage);
			
		}
		private function coverFlowAddedToStage(evt:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE, coverFlowAddedToStage);
			//
			container = new MovieClip();
			subContainer = new MovieClip();
			subContainer.x = 330;
			container.addChild(subContainer);
			addChild(container);
			//
			left.buttonMode = true;
			right.buttonMode = true;
			//
			left.focusRect = false;
			right.focusRect = false;
			//
			left.addEventListener(MouseEvent.MOUSE_DOWN, showPrevImage);
			right.addEventListener(MouseEvent.MOUSE_DOWN, showNextImage);
			//Mask the main Container
			var sh:Shape = createMask();
			addChild(sh);
			container.mask = sh;
			bName.mask = sh;
			titleName.mask = sh;
			left.mask = sh;
			right.mask = sh;
			sbhPrice.mask = sh;
			this.setChildIndex(sh, this.numChildren - 1);
		}
		//Short based on Selected vendor
		private function extractObjWithId(aId:int, aOwner:String):Array {
			var seqArr:Array = Myxml.getInstance().getCatalogueObjxml;
			var tmpArr:Array = new Array();
			for each (var obj in seqArr) {
				if (aOwner != null) {
					if (obj.SeqId == aId && obj.OwnerId == aOwner) {
						obj.SequenceId = tmpArr.length;
						tmpArr.push(obj);
					}
				} else {
					if (obj.SeqId == aId) {
						obj.SequenceId = tmpArr.length;
						tmpArr.push(obj);
					}
				}
			}
			return tmpArr;
		}
		public function initialize(aObj:Object = null):int {
			showSubContainer(false);
			//subContainer.visible = false;
			if (noVendorPro && this.contains(noVendorPro)) {
				this.removeChild(noVendorPro);
			}
			
			addDummyLoading();
			currentImgId = -1;
			isMove = true;
			if (aObj != null) {
				if (currOwnerId  ==  aObj.OwnerId && currSeqId == aObj.CategoryId) {
					//trace('Same Vendor or All products')
					var imgId:int;
					for (var aa in disObjArr) {
						if (disObjArr[aa].ProdId ==  aObj.ProdId) {
							currentImgId  = aa;
							moveObject(currentImgId);
							return 0;
						}
					}
					imgId = disObjArr.length < 3 ? 0:1;
					currentImgId = imgId;
					moveObject(currentImgId);
					return 0;
				}
			}
			initLoading();
			count = 0;
			subContainer.x = 0;
			disObjArr = new Array();
			imgContArr = new Array();
			var ownerId:String;
			var seqId:int;
			if (aObj != null) {
				refObj = aObj;
				currOwnerId = ownerId = aObj.OwnerId;
				currSeqId = seqId = aObj.CategoryId;
			} else {
				refObj = null;
				currOwnerId = ownerId = null;
				currSeqId = seqId = 1;
			}
			//
			disObjArr = extractObjWithId(seqId, ownerId);
			loadImages();
			return 0;
		}
		//Load Images
		private function loadImages():int {
			var xx:Number = disObjArr.length < 3 ? 50 : 50;
			var yy:Number = 120;
			if (disObjArr.length > 0) {
				for (var i in disObjArr) {
					var imgContainer:LoaderCoverFlow = new LoaderCoverFlow();
					imgContainer.x = xx;
					imgContainer.y = yy;
					imgContainer.id = disObjArr[i].SequenceId;
					xx = xx + 330;
					imgContainer.xx = xx;
					imgContainer.focusRect = false;
					imgContainer.addEventListener('loadComplete', hdrlImageLoaded);
					try {
						imgContainer.LoadImage(disObjArr[i].CoverFlowImg, true);
					} catch (err:Error) {
						trace('Error in Cover Image Loading');
					}
					imgContainer.buttonMode = true;
					imgContainer.mouseChildren = false;
					imgContainer.obj = disObjArr[i];
					imgContainer.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCenterImage);
					imgContainer.addEventListener(MouseEvent.ROLL_OVER, hdlrCenterRollOver);
					imgContainer.addEventListener(MouseEvent.ROLL_OUT, hdlrCenterRollOut);
					subContainer.addChild(imgContainer);
					//Add the View more details button with in the image container and properties
					var viewMore:ViewMoreDetails_1 = new ViewMoreDetails_1();
					viewMore.name = 'viewMore';
					viewMore.x = 33;
					viewMore.y = 220;
					viewMore.visible = false;
					imgContainer.addChild(viewMore);
					imgContArr.push(imgContainer);
				}
				var imgId:int = disObjArr.length < 3 ? 0:1;
				currentImgId = imgId;
				if (isMove) {
					if (refObj != null) {
						if (refObj.ProdId != undefined) {
							for (var aa in disObjArr) {
								if (disObjArr[aa].ProdId == refObj.ProdId) {
									moveObject(aa);;
									return 1
								}
							}
						}
					}
				}
				moveObject(currentImgId);
			} else {
				noVendorPro = new ProductNotFound();
				this.addChild(noVendorPro);
				return 1
			}
			return 0;
		}
		//Show and Hide Container
		public function showSubContainer(aBool:Boolean):void {
			subContainer.visible = aBool;
			bName.visible = aBool;
			titleName.visible = aBool;
			sbhPrice.visible = aBool;
			left.visible = aBool;
			right.visible = aBool;
		}
		//Move the products call from the Production details screen;
		public function moveProducts(aObj:Object):void {
			if (currOwnerId == null) {
				if (currSeqId != aObj.CategoryId) {
					var obj:Object = new Object();
					obj.OwnerId = null;
					obj.CategoryId = aObj.CategoryId;
					initialize(obj);
				}
			}
			for (var aa in imgContArr) {
				if (imgContArr[aa].obj.ProdId == aObj.ProdId) {
					currentImgId = aa;
					isMove = false;
					moveObject(currentImgId);
					break;
				}
			}
		}
		//Stop Loading Process and remove the child object with in the container
		private function initLoading():void {
			if (imgContArr) {
				if (imgContArr.length > 0) {
					for each (var mc in imgContArr) {
						subContainer.removeChild(mc);
					}
				}
			}
		}
		//Image Load complete handler
		private function hdrlImageLoaded(evt:Event):int {
			count++;
			evt.target.removeEventListener('loadComplete', hdrlImageLoaded);
			if (count == disObjArr.length) {
				container.visible = true;
				/*var imgId:int = disObjArr.length < 3 ? 0:1;
				currentImgId = imgId;
				if (isMove) {
					if (refObj != null) {
						if (refObj.ProdId != undefined) {
							for (var aa in disObjArr) {
								if (disObjArr[aa].ProdId == refObj.ProdId) {
									moveObject(aa);;
									return 1
								}
							}
						}
						
					}
					moveObject(currentImgId);
				}*/
			}
			return 0;
		}
		//Center the image
		private function hdlrCenterImage(evt:MouseEvent):void {
			if (evt.target.id == currentImgId) {
				dispatchEvent(new Event('itemSelected'));
			}
			moveObject(evt.currentTarget.id);
		}
		//
		private function moveObject(aId:int = 0):void {
			currentImgId = aId;
			currentItem = imgContArr[aId].obj as Object;
			if (tween) {
				tween.stop();
			}
			changeText(aId);
			tween = new Tween(subContainer, 'x', Strong.easeOut, subContainer.x, -((aId * 330)-330)   , 1, true);
			tween.start();
		}
		//
		private function changeText(aId:int):void {
			try {
				trace('Text Assigning');
				bName.htmlText = disObjArr[aId].ProdBrand != null ? disObjArr[aId].ProdBrand:'';
				titleName.htmlText = disObjArr[aId].ProdTitle != null ? disObjArr[aId].ProdTitle:'';
				if (disObjArr[aId].Adv.toLowerCase() != 'y') {
					sbhPrice.htmlText  = disObjArr[aId].SkybuyPrice != null ? disObjArr[aId].SkybuyPrice:'';
				} else {
					sbhPrice.text = '';
				}
			} catch (err:Error) {
				trace('Error in Text assigning');
			}
		}
		//
		private function showPrevImage(evt:MouseEvent):void {
			if (currentImgId != 0) {
				currentImgId = currentImgId -1;
				moveObject(currentImgId);
			}
		}
		//
		private function showNextImage(evt:MouseEvent):void {
			try {
				if (currentImgId < disObjArr.length - 1) {
					currentImgId = currentImgId + 1;
					moveObject(currentImgId);
				}
			} catch (err:Error) {
				trace('Cover flow - show next image', err);
			}
		}
		var loop:Boolean = false;
		//Navigating the next next Images from Product detail streen
		public function showNextImageFromProductDet(evt:Event):void {
			try {
				currentImgId = loop ? currentImgId :evt.currentTarget.currentProduct.SequenceId;
				currentImgId = currentImgId < disObjArr.length - 1 ? (loop = true, currentImgId++): -1;
				currentImgId++;
				//trace(currentImgId, 'currentImgId', disObjArr.length, 'disObjArr.length', loop, 'loop');
				if (disObjArr[currentImgId].Adv.toUpperCase()=='Y') {
					showNextImageFromProductDet(evt);
				} else {
					currentItem = imgContArr[currentImgId].obj as Object;
					moveObject(currentImgId);
				}
				/*
				 * currentImgId = evt.currentTarget.currentProduct.SequenceId;
				currentImgId = currentImgId < disObjArr.length - 1 ? currentImgId++: -1;
				currentImgId++;
				trace(currentImgId, 'currentImgId', disObjArr.length, 'disObjArr.length');
				if (disObjArr[currentImgId].Adv.toUpperCase()=='Y') {
				showNextImageFromProductDet(evt);
				}else {
				currentItem = imgContArr[currentImgId].obj as Object;
				moveObject(currentImgId);
				}
				 * */
				loop = false;
			} catch (err:Error) {
				trace('Cover flow - show Next Image From ProductDet', err);
			}
		}
		private function hdlrCenterRollOver(evt:Event):void {
			if (evt.target.id == currentImgId) {
				evt.target.getChildByName('viewMore').visible = true;
			}
		}
		//
		private function hdlrCenterRollOut(evt:MouseEvent):void {
			if (evt.target.id == currentImgId) {
				MovieClip(evt.target).getChildByName('viewMore').visible = false;
			}
		}
		//Didplay Dummy Loading
		public function addDummyLoading():void {
			showSubContainer(false);
			var dm:DummyLoading = new DummyLoading();
			dm.addEventListener('Initialized', hdlrInitialized);
			addChild(dm);
		}
		private function hdlrInitialized(evt:Event):void {
			showSubContainer(true);
			//subContainer.visible = true;
		}
		//Create mask sprite
		private function createMask():Shape {
			var sh:Shape = new Shape();
			sh.graphics.beginFill(0x004000, 1);
			sh.graphics.drawRect(50.9, 108.0, 909.2, 417.3);
			sh.graphics.endFill();
			return sh;
		}
	}
}