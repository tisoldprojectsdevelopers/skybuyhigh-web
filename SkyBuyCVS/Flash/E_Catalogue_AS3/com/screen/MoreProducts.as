﻿package com.screen 
{
	import com.utils.ViewMoreContainer;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.geom.Point;
	import com.mylocale.Myxml;
	import com.utils.CustomTrans;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class MoreProducts extends MovieClip
	{
		public var isOpen:Boolean = false;
		private var currentOwnerId:String;
		private var currentSeqId:String;
		private var productArr:Array;
		private var contHolder:MovieClip;
		private var mainContainer:MovieClip;
		private var msk:Sprite;
		private var _height:Number;
		public var selectedProIs:Object;
		//
		private var currentPos:int = 0;
		private var isNavNext:Boolean = true;
		private var viewProArr:Array;
		//
		public function MoreProducts() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				open.buttonMode = true;
				open.focusRect = false;
				open.addEventListener(MouseEvent.MOUSE_DOWN, hdlrOpenBar);
			}
			mainContainer = new MovieClip();
			mainContainer.x = 45.9;
			mainContainer.y = 10;
			this.addChild(mainContainer);
		}
		public function hdlrOpenBar(evt:MouseEvent):void {
			var tween:CustomTrans;
			var pt:Point = new Point(this.x, this.y);
			open.removeEventListener(MouseEvent.MOUSE_DOWN, hdlrOpenBar);
			if (isOpen) {
				tween = new CustomTrans(this, 'x', 913.5 - this.width + 60, 913.5, 1);
				tween.frameRate = 30;
				tween.addEventListener(CustomTrans.TWEEN_COMPLETE, hdlrMotionFinish);
				tween.startTween();
			} else {
				tween = new CustomTrans(this, 'x', 913.5 , 913.5 - this.width +61, 1);
				tween.frameRate = 30;
				tween.addEventListener(CustomTrans.TWEEN_COMPLETE, hdlrMotionFinish);
				tween.startTween();
			}
			isOpen =  !isOpen;
		}
		//Handler Tween Complete
		private function hdlrMotionFinish(evt:Event):void {
			evt.currentTarget.removeEventListener(CustomTrans.TWEEN_COMPLETE, hdlrMotionFinish);
			open.addEventListener(MouseEvent.MOUSE_DOWN, hdlrOpenBar);
		}
		//
		public function disSameVendorPro(aObj:Object):Boolean {
			//trace('Calling Function ---- ',  aObj.SeqId,  aObj.OwnerId)
			if (currentOwnerId == null) {
				currentOwnerId = aObj.OwnerId; 
				currentSeqId = aObj.SeqId;
				productArr = extractObjWithId(int(currentSeqId), currentOwnerId);
			} else if( currentOwnerId != aObj.OwnerId || currentSeqId != aObj.SeqId) {
				currentOwnerId = aObj.OwnerId; 
				currentSeqId = aObj.SeqId;
				productArr = extractObjWithId(int(currentSeqId), currentOwnerId);
			}else {
				return false
			}
			for (var aa in productArr) {
				if (productArr[aa].Adv.toLowerCase() == 'y') {
					productArr.splice(aa, 1);
				}
			}
			showSideThumb();
			return true;
		}
		/*
		 * aId - SeqId aOwner:ownerId;
		 * 
		 */
		private function extractObjWithId(aId:int, aOwner:String):Array {
			var seqArr:Array = Myxml.getInstance().getCatalogueObjxml;
			var tmpArr:Array = new Array();
			for each(var obj in seqArr) {
				if (aOwner != null) {
					if (obj.SeqId == aId && obj.OwnerId == aOwner) {
						tmpArr.push(obj);
					}
				}else {
					if (obj.SeqId == aId) {
						tmpArr.push(obj);
					}
				}
			}
			return tmpArr;
		}
		//
		private function showSideThumb():void {
			viewProArr = new Array();
			if (productArr.length > 0) {
				for (var pro in productArr) {
					var viewCont:Viewcontainer = new Viewcontainer();
					viewCont.productObj = productArr[pro];
					viewCont.addEventListener('thumbDown', hdlrObjSelected);
					viewCont.loader.LoadImage(viewCont.productObj.MainThumbImage, true);
					viewProArr.push(viewCont);
				}
			}
			if (productArr.length <= 5) {
				displayImages(viewProArr);
				removeListener();
			}else {
				addListener();
				navigate(displayImages, true);
				isNavNext = true;
			}
			
		}
		//Display selected product detail image
		private function hdlrObjSelected(evt:MouseEvent):void {
			if (isOpen) {
				hdlrOpenBar(evt);
			}
			selectedProIs = evt.target.productObj;
			dispatchEvent(new Event('showSelctedPro'));
		}
		//
		private function navigate(aFunction:Function, isNext:Boolean= true):void {
			var tmp:Array = new Array();
			if (isNext) {
				if (!isNavNext ) {
					if ( currentPos + 5 > viewProArr.length) {
						currentPos = (5-(viewProArr.length -currentPos) );
					} else {
						currentPos = currentPos + 5;
					}
				}
				isNavNext = true;
				if (currentPos+5 < viewProArr.length) {
					tmp = tmp.concat(viewProArr.slice(currentPos, currentPos+5));
					currentPos = currentPos + 5;
				} else {
					var tmpId:int = viewProArr.length-currentPos;
					tmp = tmp.concat(viewProArr.slice(currentPos, currentPos+tmpId));
					currentPos = 5 - tmpId;
					tmp = tmp.concat(viewProArr.slice(0, currentPos));
				}
			} else {
				if (isNavNext) {
					if(currentPos - 5  < 0){
						currentPos = viewProArr.length  - (5 - currentPos);
					}else{
						currentPos = currentPos - 5;
					}
				}
				isNavNext = false;
				if (currentPos - 5 >= 0 ) {
					tmp = tmp.concat(viewProArr.slice(currentPos-5,currentPos));
					currentPos = currentPos - 5;
				} else {
					var tmpId2:int = 5-currentPos;
					tmp = tmp.concat(viewProArr.slice(viewProArr.length - tmpId2, viewProArr.length));
					currentPos = 5 - tmpId2;
					tmp = tmp.concat(viewProArr.slice(0, currentPos));
					currentPos = viewProArr.length - tmpId2;
				}
			}
			aFunction(tmp);
		}
		//
		private function displayImages(_array:Array):void {
			removeCont();
			contHolder = new MovieClip();
			mainContainer.addChild(contHolder);
			var yy:Number  = 0;
			for (var aa in _array) {
				 _array[aa].y = yy;
				 yy = _array[aa].y + _array[aa].height;
				 contHolder.addChild(_array[aa]);
			}
			_height = 5 * _array[aa].height;
			setMask(_array[aa].width,_height);
		}
		//
		private function addListener():void {
			next.enabled = true;
			next.alpha = 1;
			prev.enabled = true;
			prev.alpha = 1; 
			next.focusRect = false;
			prev.focusRect = false;
			next.addEventListener(MouseEvent.MOUSE_DOWN, nxtHandler);
			prev.addEventListener(MouseEvent.MOUSE_DOWN, prevHandler);
		}
		private function removeListener():void {
			next.enabled = false
			next.alpha = 0.5;
			prev.enabled = false;
			prev.alpha = 0.5;
			next.removeEventListener(MouseEvent.MOUSE_DOWN, nxtHandler);
			prev.removeEventListener(MouseEvent.MOUSE_DOWN, prevHandler);
		}
		//
		private function nxtHandler(evt:MouseEvent):void {
			navigate(displayImages, true);
		}
		private function prevHandler(evt:MouseEvent):void{
			navigate(displayImages, false);
		}
		//setMask for the container holder
		private function setMask(_width:Number, _height:Number):void {
			msk = new Sprite();
			msk.graphics.beginFill(0x808080, 0.5);
			msk.graphics.drawRect(0, 0, _width,400);
			msk.graphics.endFill();
			mainContainer.addChild(msk);
			msk.x = contHolder.x;
			msk.y = contHolder.y;
			contHolder.mask = msk;
		}
		//Remove the childrens from the contHolder
		private function removeCont():void {
			if (contHolder && contHolder.numChildren > 0) {
				mainContainer.removeChild(contHolder);
				mainContainer.removeChild(msk);
			}
		}
		//Close the Panel
		public function closeBar(evt:MouseEvent):void {
			if (isOpen) {
				hdlrOpenBar(evt);
			}
		}
	}
}