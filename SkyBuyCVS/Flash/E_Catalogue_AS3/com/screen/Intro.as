﻿package com.screen 
{
	import com.mylocale.Myxml;
	import com.utils.CustomTween;
	import fl.transitions.TweenEvent;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.*;
	import flash.net.URLRequest;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.text.TextField
	//Custom Classes
	import com.document.Main;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Intro extends MovieClip
	{
		
		private var logo:MovieClip;
		public var thamArr:Array;
		private var lt:XML 
		private var noOfThub:int;
		private var count:int;
		private var count1:int
		private var xx:Number = -286.0;
		private	var yy:Number = -4.7;
		//
		public var currCoverObj:Object
		//
		public function Intro() 
		{
			addEventListener(Event.ADDED_TO_STAGE, introAdded);
		}
		private function introAdded(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, introAdded);
			thamArr = new Array();
			lt  = Myxml.getInstance().coverflow_images();
			noOfThub = lt.children().length();
			showThumb();
			showNetJetLogo();
			btnShop.focusRect = false;
			btnShop.addEventListener(MouseEvent.MOUSE_DOWN, hdlrGotoShop);
		}
		private function showNetJetLogo():void
		{
			try {
				logo = new MovieClip();
				if (!Main.thisClass.isItWeb) {
					var logoPath:String = Myxml.getInstance().categories_path('AirLogoIntroPath');
					var ldr:Loader = new Loader();
					ldr.load(new URLRequest(logoPath));
					ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, netJetLogoLoaded);
					logo.addChild(ldr);
				}
			}catch (err:Error) {
				trace(err);
			}
			
		}
		//
		private function netJetLogoLoaded(event:Event):void {
			event.currentTarget.removeEventListener(Event.COMPLETE, netJetLogoLoaded);
			logoNetJet.addChild(logo);
			logo.x =-181.9;
			logo.y =-60.3;
			var tX:Tween = new Tween(logo,"scaleX",Regular.easeIn,1.2,1,0.3,true);
			var tY:Tween = new Tween(logo,"scaleY",Regular.easeIn,1.2,1,0.3,true);
		}
		//
		private function showThumb():void {
			var thb:ThumbContainer = new ThumbContainer();
			thb.obj = Myxml.getInstance().getxmlCatalogueObj(lt.children()[count].@SeqId, lt.children()[count].@ProdId);
			thb.alpha = 0;
			thb.focusRect = false;
			var ldr:Loader = new Loader();
			var url:String = Myxml.getInstance().Trim(String(lt.children()[count].@ImgURL));
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrThumbLoaded);
			try {
				ldr.load(new URLRequest(url));
			}catch (err:Error) {
				trace("Error is", err);
			}
			ldr.x = -80;
			ldr.y = -81;
			//
			thb.addChild(ldr);
			//
			thamArr[count/*Number(lt.children()[count].@coverSeqId) - 1*/]  = thb;
			//
			thumb.addChild(thb);
		}
		//Complete Handler imageLoaded
		private function hdlrThumbLoaded(event:Event):void
		{
			event.currentTarget.removeEventListener(Event.COMPLETE, hdlrThumbLoaded);
			count++;
			if (count == noOfThub) {
				count1 = 0;
				showThumbTran();
			}else {
				showThumb();
			}
		}
		//MouseDown Handler - thumb Down
		private function hdlThubDown(event:MouseEvent):void
		{
			currCoverObj = event.currentTarget.obj;
			dispatchEvent(new Event('imagedown', true));
		}
		//Adding Transitions to Thumbnails
		private function showThumbTran():void
		{
			thamArr[count1].x = xx;
			thamArr[count1].y = yy;
			xx = thamArr[count1].x + thamArr[count1].width -20;
			var tween:CustomTween = new CustomTween(thamArr[count1], 'alpha', 0, 1, 0.7, true);
			tween.addEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenFinish);
			tween.start();
		}
		//Tween Complete Handler
		private function hdlrTweenFinish(evt:Event):void
		{	
			evt.currentTarget.removeEventListener(TweenEvent.MOTION_FINISH, hdlrTweenFinish)
			count1++;
			if (count1 < noOfThub) {
				showThumbTran();
			}else if (count1 == noOfThub) {
				for each(var aa in thamArr) {
					aa.buttonMode = true;
					aa.addEventListener(MouseEvent.MOUSE_DOWN, hdlThubDown);
				}
			}
		}
		//Goto shop page Handler
		private function hdlrGotoShop(event:MouseEvent):void
		{
			dispatchEvent(new MouseEvent('ClicktoGo'));
		}
		//Delete the Screen
		public function killMe():void {
			try {
				parent.removeChild(this);
				delete (this);
			}catch (err:Error) {
				trace('Error in Kill Intro Scrren', err);
			}
		}
	}

}