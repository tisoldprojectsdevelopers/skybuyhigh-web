﻿package com.screen 
{
	import com.document.Main;
	import com.document.WebServiceParam;
	import fl.managers.FocusManager;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent
	import flash.display.SimpleButton;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.system.fscommand;
	import flash.display.AVM1Movie;
	//
	import com.utils.EmailValidator;
	import com.mylocale.Myxml;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class PrivateJet extends MovieClip
	{
		private var product:Object;
		private var isCorrectDet:Boolean;
		private var fm:FocusManager;
		public function PrivateJet() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrAddedToStage);
		}
		//
		private function hdlrAddedToStage(evt:Event):void {
			Main.thisClass.showCloseBtn(false);
			removeEventListener(Event.ADDED_TO_STAGE, hdlrAddedToStage);
			msg.visible = false;
			//
			fm = new FocusManager(this);
			fm.setFocus(contact_me.txtName);
			//
			contact_me.txtTailNo.maxChars = 32;
			contact_me.txtName.maxChars = 64;
			contact_me.txtMyPhone.maxChars = 10;
			contact_me.txtMyPhone.restrict = '0-9';
			
			//
			contact_me.mcReqName.alpha=0;
			contact_me.mcReqTailNo.alpha=0;
			contact_me.invalidEmail.alpha=0;
			contact_me.invalidPhone.alpha = 0;
			var tf:TextFormat = new TextFormat();
			tf.size = 12;
			tf.font = 'Ariel'
			contact_me.txtName.setStyle("textFormat", tf);
			contact_me.txtMyPhone.setStyle("textFormat", tf);
			contact_me.txtName.setStyle("textFormat", tf);
			
		}
		//
		public function Initialize(aObj:Object):void {
			product = aObj;
			loadSwf(); 
			//
			contact_me.btnContact.focusRect = false;
			contact_me.backtogallery.focusRect = false;
			msg.btnOk.focusRect = false;
			//
			contact_me.btnContact.addEventListener(MouseEvent.MOUSE_DOWN, sendContactDet);
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				contact_me.backtogallery.addEventListener(MouseEvent.MOUSE_DOWN, killMe);
			}
			msg.btnOk.addEventListener(MouseEvent.MOUSE_DOWN, closeMsgBox);
		}
		//
		private function closeMsgBox(evt:MouseEvent):void {
			msg.visible = false;
			if (!isCorrectDet) {
				//contact_me.txtName.text="";
				//contact_me.txtMyEmail.text="";
				//contact_me.txtMyPhone.text = "";
			}
		}
		//
		private function loadSwf():void {
			mcLoading.visible = true;
			var ldr:Loader = new Loader();
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrSwfLoaded);
			try {
				ldr.load(new URLRequest(product.SwfPath));
			}catch (err:Error) {
				trace('Loading Error in Privatjet swf', err);
			}
			mcCont.addChild(ldr);
		}
		//
		private function hdlrSwfLoaded(evt:Event):void {
			evt.currentTarget.removeEventListener(Event.COMPLETE, hdlrSwfLoaded);
			mcLoading.visible = false;
		}
		//
		private function sendContactDet(evt:MouseEvent):void {
			isCorrectDet = true;
			var _validator = new EmailValidator();
			var isValid:Boolean = true;
			//
			var isPhoneNoValid:Boolean;
			var isEmailValid:Boolean;
			var txtFormat:TextFormat=new TextFormat();
			txtFormat.color=0x003300;
			txtFormat.bold = true;
			
			if (Myxml.getInstance().Trim(contact_me.txtName.text).length < 1) {
				contact_me.mcReqName.alpha = 1;
				isValid = false;
			}else {
				contact_me.mcReqName.alpha = 0;
			}
			//
			if (Myxml.getInstance().Trim(contact_me.txtTailNo.text).length < 1) {
				contact_me.mcReqTailNo.alpha = 1;
				isValid = false;
			}else {
				contact_me.mcReqTailNo.alpha = 0;
			}
			//
			if (Myxml.getInstance().Trim(contact_me.txtMyPhone.text).length > 0 && Myxml.getInstance().Trim(contact_me.txtMyPhone.text).length != 10) {
				contact_me.invalidPhone.alpha = 1;
				isValid = false
			} else {
				contact_me.invalidPhone.alpha = 0;
			}
			//
			if (Myxml.getInstance().Trim(contact_me.txtMyEmail.text).length > 0 && _validator.validate(contact_me.txtMyEmail.text) == false) {
				contact_me.invalidEmail.alpha = 1;
				isValid = false;
			} else {
				contact_me.invalidEmail.alpha = 0;
			}
			if (Myxml.getInstance().Trim(contact_me.txtMyPhone.text).length < 1 && _validator.validate(contact_me.txtMyEmail.text) == false) {
				isValid = false;
			} 
			//
			if (isValid) {
				if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO' && Main.thisClass.isItWeb) {
					showRightMsgBox()
				}else if(Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB' && Main.thisClass.isItWeb){
					sendDetails();
				} else if (Main.thisClass.isItWeb == false) {
					sendDetails();
				}
			}else {
				showWrongMsgBox();
			}
		}
		public function deviceMessage(aStatus:String):void {
			if (aStatus == 'Failed') {
				showFailedMsgBox();
			}if (aStatus == 'Success') {
				showRightMsgBox()
			}
		}
		private function showRightMsgBox():void {
			contact_me.btnContact.addEventListener(MouseEvent.MOUSE_DOWN, sendContactDet);
			contact_me.invalidEmail.alpha = 0;
			contact_me.invalidPhone.alpha = 0;
			var txtFormat:TextFormat = new TextFormat();
			msg.msg.text = "Thank you for your interest. We will get in touch with you soon.";
			msg.msg.setTextFormat(txtFormat);
			msg.visible = true;
			msg.isValid.text="1";
		}
		private function showWrongMsgBox():void {
			isCorrectDet = false;
			var txtFormat:TextFormat = new TextFormat();
			msg.msg.text = "Please provide us with your name, tail number, phone number and/or e-mail for us to contact you.";
			txtFormat.color=0xFF0000;
			txtFormat.bold=false;
			msg.isValid.text="0";
			msg.msg.setTextFormat(txtFormat);
			msg.visible = true;
		}
		//
		private function showFailedMsgBox():void {
			isCorrectDet = false;
			var txtFormat:TextFormat = new TextFormat();
			msg.msg.text = "Your order failed. please report the following error to SkyBuy Admin";
			txtFormat.color=0xFF0000;
			txtFormat.bold=false;
			msg.isValid.text="0";
			msg.msg.setTextFormat(txtFormat);
			msg.visible = true;
		}
		private function sendDetails():void {
			contact_me.btnContact.removeEventListener(MouseEvent.MOUSE_DOWN, sendContactDet);
			var prodDetailsXML:String="";
			prodDetailsXML=prodDetailsXML+"<Product>";
			prodDetailsXML=prodDetailsXML+"<VendorId>"+product.OwnerId+"</VendorId>";
			prodDetailsXML=prodDetailsXML+"<CategoryId>"+product.CateId+"</CategoryId>";
			prodDetailsXML=prodDetailsXML+"<ProductName>"+ product.ProdTitle+"</ProductName>";
			prodDetailsXML=prodDetailsXML+"<ProductId>"+product.ProdId+"</ProductId>";
			prodDetailsXML=prodDetailsXML+"<ProductCode>"+product.ProdCode+"</ProductCode>";
			prodDetailsXML=prodDetailsXML+"<ContactName>"+contact_me.txtName.text+"</ContactName>";
			prodDetailsXML=prodDetailsXML+"<ContactEmail>"+contact_me.txtMyEmail.text+"</ContactEmail>";
			prodDetailsXML = prodDetailsXML + "<ContactPhone>" + contact_me.txtMyPhone.text + "</ContactPhone>";
			prodDetailsXML = prodDetailsXML + '<FlightNo>' + contact_me.txtTailNo.text + '</FlightNo>';
			prodDetailsXML = prodDetailsXML + "</Product>";
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB' && Main.thisClass.isItWeb == true) {
				var webservice:WebServiceParam = new WebServiceParam();
				webservice.sendArilinePacDet(prodDetailsXML, product.TITLE);
				webservice.addEventListener('Success', messageSend);
			}else if(Main.thisClass.isItWeb == false){
				fscommand("ContactDetails", prodDetailsXML);
			}
		}
		//
		private function messageSend(evt:Event):void {
			var successMsg:String  = evt.currentTarget.successMsg;
			if (successMsg == 'OPS') {
				showRightMsgBox();
			}else {
				showFailedMsgBox();
			}	
		}
		//
		private function showCoverFlowScreen():void {
			var obj:Object = new Object();
			obj.ProdId =product.ProdId;
			obj.OwnerId = product.OwnerId;
			obj.CategoryId =product.SeqId;
			Main.thisClass.shoppingScreen.coverFlow.addDummyLoading();
			Main.thisClass.shoppingScreen.coverFlow.moveProducts(obj);
		}
		//Kill me
		private function killMe(evt:MouseEvent):void {
			fm.deactivate();
			showCoverFlowScreen();
			dispatchEvent(new Event(Event.REMOVED));
			Main.thisClass.showCloseBtn(true);
			parent.removeChild(this);
		}
	}
}