﻿package com.screen 
{
	import fl.containers.ScrollPane;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	//Custom Classes
	import com.document.PurchaseDetails;
	import com.utils.Product;
	import com.mylocale.Myxml;
	import com.document.Main;
	import com.screen.CreditCardDetails;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ShoppingCart extends MovieClip
	{
		public static const REMOVE_PRODUCT:String = 'remove_product';
		public var prodgrandtot:ProdGrandTotal
		//
		private var purDetailIns:PurchaseDetails;
		private var container:MovieClip;
		private var count:int;
		private var yy:Number = 0;
		private var productItems:Array;
		private var removeProduct:Product;
		public var removeProductObj:Object;
		//
		private var bottLine:Sprite
		private var graySh:Sprite; 
		private var termCondi:MovieClip
		
		//
		private var myPan:ScrollPane;
		public var creditCard:CreditCardDetails;
		public function ShoppingCart() {
			alert.visible = false;
			alert.addEventListener('ok', hdlrDeleteConfirm);
			alert.addEventListener('cancel', hdlrDeleteCancle);
			//
			btnShop.focusRect = false;
			backToHome.focusRect = false;
			mcCheckout.focusRect = false;
			mcCheckout1.focusRect = false;
			//
			btnShop.addEventListener(MouseEvent.MOUSE_DOWN, hdlrContinueShop);
			backToHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrShowIntro);
			mcCheckout.addEventListener(MouseEvent.MOUSE_DOWN, hdlrShowCardDetailScreen);
			mcCheckout1.addEventListener(MouseEvent.MOUSE_DOWN, hdlrShowCardDetailScreen);
		}
		//Reference - purchaseDetails instance 
		public function Init(aIns:PurchaseDetails):void {
			purDetailIns = aIns;
		}
		//Initialize the shopping cart
		public function showBuyProduct():void {
			productItems = new Array();
			yy = 0;
			count = 0 ;
			container = new MovieClip();
			purDetailIns.clubProducts();
			displayProducts();
		}
		//Create no of products and the details
		private function displayProducts():void {
			for (var count in purDetailIns.purchaseDet) {
				var pro:Product = new Product();
				pro.Initialize(purDetailIns.purchaseDet[count]);
				pro.addEventListener(Product.ITEM_POLICY,hdlrShowReturnPolicy);
				pro.addEventListener(Product.ITEM_REMOVE, hdlrRemoveItem);
				pro.addEventListener(Product.ITEMCOUNT_CHANGED, hdlrQuantitychanged);
				productItems.push(pro);
			}
			var tf:Timer = new Timer(100);
			tf.addEventListener(TimerEvent.TIMER, hdlrProdLoaded);
			tf.start();
		}
		private function hdlrProdLoaded(evt:TimerEvent):void {
			evt.currentTarget.stop();
			for each(var obj:Product in productItems) {
				container.addChild(obj);
				obj.thisparentX = 0
				obj.thisparentY = yy;
				obj.x = 0;
				obj.y = yy;
				yy = obj.y +obj._height;
			}
			drawGrawBar();
			addGrandTotal();
			termsAndConditions();
			myPan = new ScrollPane();
			myPan.source = container;
			myPan.move(53, 105.9);
			myPan.width = 906;
			myPan.height = 350;
			myPan.horizontalScrollPolicy = 'off';
			panCont.addChild(myPan);
			purDetailIns.grandTotal();
		}
		//Draw a bottom Graw bar
		private function drawGrawBar():void {
			graySh = new Sprite();
			graySh.graphics.beginFill(0xF2F2F2, 1);
			graySh.graphics.drawRect(2, 2, 890, 40); 
			graySh.x = 0;
			graySh.y = yy;
			yy = graySh.y + graySh.height;
			graySh.graphics.endFill();
			//
			bottLine = new Sprite();
			bottLine.graphics.lineStyle(1, 0x999999, 0.7);
			bottLine.graphics.moveTo(2, yy+2);
			bottLine.graphics.lineTo(890, yy+2);
			container.addChild(graySh);
			container.addChild(bottLine);
		}
		//AddProduct grandtotal
		private function addGrandTotal():void {
			prodgrandtot = new ProdGrandTotal();
			prodgrandtot.x = 642;
			prodgrandtot.y = 8;
			graySh.addChild(prodgrandtot);
		}
		//Skybuy Terms and Conditions;
		private function termsAndConditions():void {
			termCondi = new MovieClip();
			var _txt:TextField = new TextField();
			_txt.width = 880;
			_txt.autoSize = TextFieldAutoSize.LEFT;
			_txt.embedFonts = true;
			_txt.multiline = true;
			_txt.wordWrap = true;
			//
			var str1:String = "<font color='#003366' size='14' face='Arial'><br><b>Return Policy</b><br><br></font>";
			var str2:String = Myxml.getInstance().getTerms('RTP');
			var str3:String = "<font color='#003366' size='14' face='Arial'><br><br><b>Disclaimer</b></font><br>";
			var str4:String = Myxml.getInstance().getTerms('DIS');
			//
			var tf:TextFormat = new TextFormat();
			tf.align = TextFormatAlign.JUSTIFY;
			_txt.defaultTextFormat = tf;
			//
			_txt.htmlText = Myxml.getInstance().getFormatedText(str1 + str2 + str3 + str4);
			termCondi.addChild(_txt);
			termCondi.x = 10;
			termCondi.y = yy;
			container.addChild(termCondi);
		}
		//Update the quantity changed to the puchase details class
		private function hdlrQuantitychanged(evt:Event):void {
			purDetailIns.updateProduct(evt.currentTarget.product);
		}
		//MouseDown handler to show the return policy
		private function hdlrShowReturnPolicy(evt:MouseEvent):void {
			var rtPoli:ReturnPolicyPopup = new ReturnPolicyPopup();
			rtPoli.showText(evt.currentTarget.product.reffObj.ReturnPolicy);
			rtPoli.showHeader(Myxml.getInstance().getFormatedText(evt.currentTarget.returnPolicyHeader));
			rtPoli.x = 250;
			rtPoli.y = 100;
			this.addChild(rtPoli);
		}
		//Whether Confirme the From the message box
		private function hdlrDeleteConfirm(evt:MouseEvent):void {
			dispatchEvent(new MouseEvent('remove_product'));
			removeFromArray(removeProduct);
			purDetailIns.grandTotal();
			alert.visible = false;
		}
		//Whether Cancel the From the message box
		private function hdlrDeleteCancle(evt:MouseEvent):void {
			alert.visible = false;
		}
		//Confirm to remove the Product for the scrollpan
		private function hdlrRemoveItem(evt:Event):void {
			removeProductObj = evt.currentTarget.product;
			removeProduct = evt.currentTarget as Product;
			alert.visible = true;
		}
		//Remove the product for the product array
		private function removeFromArray(aRemovePro:Product):int {
			for (var pro in productItems) {
				if (productItems[pro] == aRemovePro) {
					productItems.splice(pro, 1);
					container.removeChild(aRemovePro);
					arrangeItems();
					if (productItems.length < 1) {
						Main.thisClass.showIntroPage(true);
						Main.thisClass.shoppingScreen.showCoverFlowScreen(true);
						Main.thisClass.shoppingScreen.productDet.initText();
						stage.removeChild(this);
					}
					return 1;
				}
			}
			return 0;
		}
		//Rearrange the scroll pan when the product is removed from the scroll pan
		private function arrangeItems():void {
			yy = 0;
			for (var pro in productItems) {
				productItems[pro].x = 0;
				productItems[pro].y = yy;
				yy = productItems[pro].y +productItems[pro]._height;
			}
			container.removeChild(graySh);
			container.removeChild(bottLine)
			container.removeChild(termCondi);
			drawGrawBar();
			addGrandTotal();
			termsAndConditions();
			myPan.update();
		}
		//Kill me the Shopping Cart
		public function hdlrContinueShop(evt:MouseEvent):void {
			Main.thisClass.shoppingScreen.hidePersonalShoper();
			Main.thisClass.shoppingScreen.productDet.initText();
			Main.thisClass.shoppingScreen.showCoverFlowScreen(true);
			killMe();
		}
		//
		public function killMe():void {
			stage.removeChild(this);
			delete this;
		}
		//When we click the Top home button to show the intro screen;
		private function hdlrShowIntro(evt:MouseEvent):void {
			hdlrContinueShop(evt);
			Main.thisClass.shoppingScreen.hidePersonalShoper();
			Main.thisClass.showIntroPage(true);
		}
		//When we click the checkout button to show next level
		private function hdlrShowCardDetailScreen(evt:MouseEvent):void {
			var allAreActive:Boolean = true;
			for each (var aa in productItems) {
				if(aa.isActive == false) {
					allAreActive = false;
				}
			}
			if (allAreActive) {
				for each (var bb in productItems) {
					bb.hideDateIns();
				}
				creditCard = new CreditCardDetails();
				addChild(creditCard);
			}
		}
	}
}