﻿package com.screen 
{
	import com.document.Main;
	import com.document.WebServiceCall;
	import com.mylocale.Myxml;
	import fl.managers.FocusManager;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.Socket;
	import flash.system.fscommand;
	import flash.display.SimpleButton;
	import flash.text.TextField;
	import com.document.WebServiceParam;
	import flash.display.SimpleButton;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class GetFlightNo extends MovieClip
	{
		public var mailId :String
		public var orderconfirmScr:OrderConfirm
		
		//
		private var fm:FocusManager;
		public function GetFlightNo() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			thanks.gotoAndStop(1);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(MouseEvent.MOUSE_DOWN, hdlrObjAdded);
			//
			btnHome.focusRect = false;
			placeOrder.focusRect = false;
			btnCancel.focusRect = false;
			mcTailReq.focusRect = false;
			btnBack.focusRect = false;
			mcTailReq.visible = false;
			dummyMC.focusRect = false;
			dummyMC.visible = false;
			
			//
			btnBack.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackToShoppingPage);
			btnHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackHome);
			btnCancel.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCancel);
			//btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSendSuggestion);
			placeOrder.addEventListener(MouseEvent.MOUSE_DOWN, fnPlaceOrder);
			//
			txtTailNo.maxChars = 32;
			custFB1.maxChars = 1024;
			custFB2.maxChars = 1024;
			fm = new FocusManager(this);
			fm.setFocus(txtTailNo);
			//
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
				txtTailNo.text = Myxml.getInstance().getCustomerInfo('TailNo');
			}
		}
		//Placing Ordet to Device
		private function fnPlaceOrder(evt:MouseEvent):void {
			if (Myxml.getInstance().Trim(txtTailNo.text).length > 0) {
				dummyMC.visible = true;
				mcTailReq.visible = false;
				if (Myxml.getInstance().Trim(custFB1.text).length > 0 || Myxml.getInstance().Trim(custFB2.text).length > 0 ) {
					
					thanks.gotoAndPlay(3);
					thanks.addEventListener('AnimEnd', hdlrAninComplete);
				}else {
					if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
						shoOrederConformationScreen();
					}else {
						placeDeviceOrder();
					}
				}
			}else {
				mcTailReq.visible = true;
			}
		}
		//
		private function hdlrAninComplete(evt:Event):void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
				shoOrederConformationScreen();
			}else {
				placeDeviceOrder();
			}
			
		}
		//Place device Order
		private function placeDeviceOrder():void {
			fm.setFocus(compDymmy);
			var tf:Timer = new Timer(500);
			tf.addEventListener(TimerEvent.TIMER, hdlrTimerComplete);
			tf.start();
			
		}
		//
		private function hdlrTimerComplete(evt:TimerEvent):void {
			evt.currentTarget.stop();
			var str:String = Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.shippingDet.custDetailsXML;
			var joinStr:String = '<FlightNo>' + txtTailNo.text + '</FlightNo><NewServicesFeedback>' + custFB1.text + '</NewServicesFeedback><Suggestion>' + custFB2.text + '</Suggestion>';
			str = str.replace('&TailNo&', joinStr);
			if (Main.thisClass.isItWeb && Myxml.getInstance().getCatalogueType().toUpperCase() == "WEB") {
				trace(str);
				var soapmsg:WebServiceParam = new WebServiceParam(); 
				soapmsg.addEventListener(WebServiceParam.SUCCESS, hdlrSOAPMsgSended)
				soapmsg.sendPerchaseDet(Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.ccDetailsXML, str, custFB1.text, custFB2.text);
			
			}else {
				fscommand("CustDetails", str);
				fscommand("ShoppingBag", Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.shippingDet.strShoppingBagXML);
				fscommand("CreditCardDetails", Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.ccDetailsXML);
			}
		}
		//
		private function hdlrSOAPMsgSended(evt:Event):void {
			Main.thisClass.isValidOrder = evt.currentTarget.successMsg;
			/*if (evt.currentTarget.successMsg == 'OPS') {
				Main.thisClass.isValidOrder = "Success";
			}else if (evt.currentTarget.successMsg == 'OPF') {
				Main.thisClass.isValidOrder = "Failed";
			}*/
			shoOrederConformationScreen();
		}
		//Button Submit  Cancel
		private function hdlrCancel(evt:MouseEvent):void {
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.shoppingScreen.productDet.initText();
			Main.thisClass.shoppingScreen.no_of_items.text = '( 0 item )';
			Main.thisClass.shoppingScreen.productDet.purchaseDetail.purchaseDet = new Array();
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//MosueDown handler when the home button clicks
		private function hdlrBackHome(evt:MouseEvent):void {
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//
		public function shoOrederConformationScreen():void {
			Main.thisClass.shoppingScreen.productDet.purchaseDetail.purchaseDet = new Array();
			Main.thisClass.shoppingScreen.no_of_items.text = '( 0 item )';
			orderconfirmScr = new OrderConfirm();
			orderconfirmScr.mailId = Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.shippingDet.emailId;
			addChild(orderconfirmScr);
			dummyMC.visible = false;
		}
		//Mouse Down Handler submit
		/*private function hdlrSendSuggestion(evt:MouseEvent):void {
			if (Myxml.getInstance().Trim(custFB1.text).length > 0 || Myxml.getInstance().Trim(custFB2.text).length > 0) {
				//;
				if (Main.thisClass.isItWeb == false) {
					fscommand("Feedback_1",custFB1.text);
					fscommand("Feedback_2",custFB2.text);
				}
			}
		}*/
		//
		private function hdlrBackToShoppingPage(evt:MouseEvent):void {
			dummyMC.visible = false;
			killMe();
		}
		//Kill me
		private function killMe():void {
			fm.deactivate();
			parent.removeChild(this);
		}
		
	}

}