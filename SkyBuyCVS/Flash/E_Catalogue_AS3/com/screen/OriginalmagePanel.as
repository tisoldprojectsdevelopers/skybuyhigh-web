﻿package com.screen 
{
	import fl.containers.ScrollPane;
	import com.utils.CustomTween;
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class OriginalmagePanel extends MovieClip
	{
		private var loading:Loading 
		public function OriginalmagePanel() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjedAdded);
		}
		//
		private function hdlrObjedAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjedAdded);
			this.alpha = 0;
			var tween:CustomTween = new CustomTween(this, 'alpha', 0, 1, 0.3, true);
			tween.start(); 
			closewindow.focusRect= false;
			closewindow.addEventListener(MouseEvent.MOUSE_DOWN, killMe);
		}
		//Load the externl image 
		public function LoadImage(aUrl:String = null):void {
			var myScrollPan:ScrollPane = new ScrollPane();
			myScrollPan.move(11, 5);
			myScrollPan.setSize(1005, 562);
			myScrollPan.addEventListener(ProgressEvent.PROGRESS, hdlrProgressEvent);
			myScrollPan.addEventListener(Event.COMPLETE, hdlrLoadComplete);
			myScrollPan.load(new URLRequest(aUrl));
			this.addChild(myScrollPan);
			//Loading mc
			loading = new Loading();
			this.addChild(loading);
			loading.x = this.width / 2;
			loading.y = this.height / 2;
			//
			this.setChildIndex(closewindow, this.numChildren - 1);
		}
		//Image Loaded Complete
		private function hdlrLoadComplete(evt:Event):void {
			this.removeChild(loading);
		}
		//Progress Event
		private function hdlrProgressEvent(evt:ProgressEvent):void {
			
		}
		// Remove the window from the stage
		private function killMe(evt:MouseEvent):void {
			closewindow.removeEventListener(MouseEvent.MOUSE_DOWN, killMe);
			var tween:CustomTween = new CustomTween(this, 'alpha',1, 0, 0.3, true);
			tween.addEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenComplete);
			tween.start();
		}
		//Tween Complete
		private function hdlrTweenComplete(evt:Event):void {
			parent.removeChild(this);
			delete (this);
		}
		
	}

}