﻿package com.screen{
	import com.document.Main;
	import com.document.PurchaseDetails;
	import com.mylocale.Myxml;
	import com.utils.CustomTween;
	import fl.controls.ComboBox;
	import fl.data.DataProvider;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import com.utils.ScrollMc;
	import com.utils.calendarSkin;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;

	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ProductDetails extends MovieClip {

		private var loading:McLoding;
		//
		public var currentProduct:Object;
		private var descScroller:MovieClip;
		private var descScroller1:MovieClip;
		public var viewMorepro:MoreProducts;
		public var purchaseDetail:PurchaseDetails;
		public var shoppingCart:ShoppingCart;
		private var cbColor:ComboBox;
		private var cbSize:ComboBox;
		private var datePicker:calendarSkin;
		private var isDateShow:Boolean;
		private var isActiveCart:Boolean;
		private var tf:TextFormat;
		public function ProductDetails() {
			//
			focusRect = false;
			backtogallery.focusRect = false;
			nextPage.focusRect = false;
			mcZoom.focusRect = false;
			//
			loading = new McLoding();
			loading.x = 230;
			loading.y = 215;
			loading.visible = false;
			proudImg.addChild(loading);
			mcZoom.enabled = true;
			mcZoom.alpha = 1;
			//
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				backtogallery.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackToCoverFlow);
				nextPage.addEventListener(MouseEvent.MOUSE_DOWN, showNextProduct);
				if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'CATALOGUE_PREVIEW') {
					oAddToBag.enabled = false;
				}
			} else {
				backtogallery.enabled = false;
				nextPage.enabled = false;
				oAddToBag.enabled = false;
			}
			mcZoom.addEventListener(MouseEvent.MOUSE_DOWN, hdlrShowZoomedImage);
			type_1.mcQuantity.inQuantity.addEventListener(Event.CHANGE, hdlrQuantiyChange);
			type_2.mcQuantity.inQuantity.addEventListener(Event.CHANGE, hdlrQuantiyChange2);
			//Data Listneres
			datePicker = new calendarSkin();
			datePicker.Construct();
			datePicker.addEventListener(calendarSkin.DATE_CHANGE, dateChange);
			type_2.dateChoser.addEventListener(MouseEvent.MOUSE_DOWN, showDataIns);
			datePicker.x = type_2.dateChoser.x;
			datePicker.y = type_2.dateChoser.y;
			type_2.addChild(datePicker);
			datePicker.showCalander(isDateShow);
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO' || Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB' || Myxml.getInstance().getCatalogueType() == 'device' || Main.thisClass.isItWeb == false) {
				oAddToBag.focusRect = false;
				oAddToBag.addEventListener(MouseEvent.MOUSE_DOWN, hdlrAddToBag);
				purchaseDetail = new PurchaseDetails(this as MovieClip);
			}
			viewMorepro = new MoreProducts();
			viewMorepro.x = 913.5;
			viewMorepro.y = 108.5;
			viewMorepro.addEventListener('showSelctedPro', hdlrViewSeletedPro);
			viewMorepro.focusRect = false;
			this.addChild(viewMorepro);
			//
			//this.setChildIndex(mcWhiteBase, this.numChildren - 1);
			this.setChildIndex(mcZoom, this.numChildren - 1);
			this.setChildIndex(backtogallery, this.numChildren - 1);
			this.setChildIndex(nextPage, this.numChildren - 1);
			this.setChildIndex(viewMorepro, this.numChildren - 1);
			//
			tf = new TextFormat();
			tf.size = 14;
		}
		//
		private function showDataIns(evt:MouseEvent):void {
			isDateShow = !isDateShow;
			datePicker.showCalander(isDateShow);
		}
		//
		private function dateChange(evt:Event):void {
			isDateShow = false;
			isActiveCart = false;
			datePicker.showCalander(isDateShow);
			type_2.txtDate.text = evt.currentTarget.currentDay + ' ' + String(evt.currentTarget.currentMonth).substr(0, 3) + ' ' + evt.currentTarget.currentyear;
			type_2.reqTravelDt.visible = true;
			var givDate:Date = new Date(Number(evt.currentTarget.currentyear), Number(evt.currentTarget.currentMonthNumber), Number(evt.currentTarget.currentDay), 00,00,00,00);
			var curDate:Date = new Date();
			curDate.setHours(00);
			curDate.setMilliseconds(00);
			curDate.setSeconds(00);
			curDate.setMinutes(00);
			if (curDate.valueOf() <= givDate.valueOf()) {
				type_2.reqTravelDt.visible = false;
				type_2.invalidTravelDt.visible = false;
				isActiveCart = true;
			} else {
				isActiveCart = false;
				type_2.reqTravelDt.visible = false;
				type_2.invalidTravelDt.visible = true;
			}
		}
		//
		private function hdlrAddToBag(evt:MouseEvent ):int {
			viewMorepro.closeBar(evt);
			if (currentProduct.SeqId == '5') {
				type_2.reqTravelDt.visible = false;
				isDateShow = false;
				datePicker.showCalander(isDateShow );
				if (type_2.txtDate.length == 0) {
					type_2.reqTravelDt.visible = true;
					isActiveCart = false;
					return 0;
					//to break the function
				}
			} else {
				isActiveCart = true;
			}
			if (isActiveCart) {
				var colorInd:int = cbColor ? cbColor.selectedIndex : -1;
				var sizeInd:int = cbSize ? cbSize.selectedIndex : -1;
				if (currentProduct.SeqId == '5') {
					purchaseDetail.addProduct(currentProduct, int(type_2.mcQuantity.inQuantity.text), colorInd, sizeInd, type_2.txtDate.text);
				} else {
					purchaseDetail.addProduct(currentProduct, int(type_1.mcQuantity.inQuantity.text), colorInd, sizeInd, type_2.txtDate.text);
				}
				var giftBag:GiftBag = new GiftBag();
				giftBag.x = 501.0;
				giftBag.y = 141.0;
				stage.addChild(giftBag);
			}
			return 1;
		}
		//Display product details select from the view more details bar
		private function hdlrViewSeletedPro(evt:Event):void {
			initText();
			displayProduct(evt.target.selectedProIs);
		}
		//Mouse Handler to show cover flow page
		private function hdlrBackToCoverFlow(evt:MouseEvent):void {
			viewMorepro.closeBar(evt);
			dispatchEvent(new Event('showCoverFlow'));

		}
		//Mouse Handler to show next Product
		private function showNextProduct(evt:MouseEvent):void {
			PageTurn.gotoAndPlay(2);
			initText();
			dispatchEvent(new Event('showNextImage'));
		}
		//
		public function initText():void {
			type_1.mcQuantity.inQuantity.text = '1';
			type_2.mcQuantity.inQuantity.text = '1';
			type_2.txtDate.text = '';
		}
		//
		/* This is the main function to show all objects and texts
		 * Initializing all the methods
		 * @param passing a obj from coverflow class
		 */
		public function displayProduct(aObj:Object = null):void {
			if (aObj != null) {
				currentProduct = aObj;
				this.visible = true;
				Main.thisClass.shoppingScreen.hidePersonalShoper()
				if (currentProduct.Adv.toUpperCase() == 'Y') {
					Main.thisClass.shoppingScreen.showProdDet(currentProduct);
				} else {
					viewMorepro.disSameVendorPro(currentProduct);
					loadProductImg();
					if (aObj.SeqId == '5') {
						type_1.alpha = 0;
						type_1.x = -550;
						type_2.alpha = 1;
						datePicker.visible = false;
						isDateShow = false;
						type_2.x = 59.5;
						assignTestType_2();
					} else {
						type_1.alpha = 1;
						type_1.x = 59.5;
						type_2.alpha = 0;
						type_2.x = -550;
						assignTestType_1();
					}
				}
			}
		}
		/* Load the selected Image */
		private function loadProductImg():void {
			loading.visible = true;
			proudImg.loader.LoadImage(currentProduct.MainImage, true);
			proudImg.loader.addEventListener('loadComplete', hdlrLoaderComplete);
			proudImg.loader.alpha = 0;
		}
		//Handler Image Complete
		private function hdlrLoaderComplete(evt:Event):void {
			loading.visible = false;
			//evt.target.width = 465;
			//evt.target.height = 387//434;
			var tween:CustomTween = new CustomTween(proudImg.loader, 'alpha', 0, 1, 0.3, true);
			tween.addEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenComplete);
			tween.start();
		}
		private function hdlrTweenComplete(evt:Event):void {
			evt.currentTarget.removeEventListener(CustomTween.TWEEN_COMPLETE, hdlrTweenComplete);
		}
		/*
		 * Reset display Objects 
		*/
		private function reset():void {
			type_1.mcColor.visible = currentProduct.ProdColor == null ? false:true;
			type_1.mcSize.visible = currentProduct.ProdSize == null ? false:true;
		}
		//Assign Text 
		private function assignTestType_2():void {
			try {
				datePicker.Construct();
				type_2.reqTravelDt.visible = false;
				type_2.invalidTravelDt.visible = false;
				type_2.mcQuantity.inQuantity.restrict = '0-9';
				type_2.txtDate.text = '';
				//
				type_2.dtTitle.htmlText = String(currentProduct.ProdTitle) != null ? String(currentProduct.ProdTitle):'';
				type_2.mcDesSd.mcDesc.dtShortDesc.multiline = true;
				type_2.mcDesSd.mcDesc.dtShortDesc.autoSize = TextFieldAutoSize.LEFT;
				type_2.mcDesSd.mcDesc.dtShortDesc.htmlText = String(currentProduct.ProdTitle) != null ? String(currentProduct.ProdSD):'';
				type_2.mcInStorePrice.txtInstorePrice.htmlText = currentProduct.ProdPrice;
				type_2.mcSkybuyPrice.dtSkyBuyPrice.htmlText = currentProduct.SkybuyPrice;
				if (currentProduct.ProdPrice != null) {
					type_2.mcInStorePrice.visible = Number(Myxml.getInstance().formatAsDigit(currentProduct.ProdPrice)) <= Number(Myxml.getInstance().formatAsDigit(currentProduct.SkybuyPrice)) ? false:true;
				}else {
					type_2.mcInStorePrice.visible = false;
				}
				
				InitDescScroller1();
			} catch (err:Error) {
				trace('Error in text assigning type 2', err);
			}
		}
		//Assign Text 
		private function assignTestType_1():void {
			try {
				reset();
				type_1.dtBrand.htmlText = String(currentProduct.ProdBrand) != null ? String(currentProduct.ProdBrand):'';
				type_1.dtTitle.htmlText = String(currentProduct.ProdTitle) != null ? String(currentProduct.ProdTitle):'';
				type_1.mcDesSd.mcDesc.dtShortDesc.multiline = true;
				type_1.mcDesSd.mcDesc.dtShortDesc.autoSize = TextFieldAutoSize.LEFT;
				type_1.mcDesSd.mcDesc.dtShortDesc.htmlText = String(currentProduct.ProdTitle) != null ? String(currentProduct.ProdSD):'';
				type_1.mcInStorePrice.txtInstorePrice.htmlText = String(currentProduct.ProdPrice) != null ? String(currentProduct.ProdPrice):''; 
				type_1.mcSkybuyPrice.dtSkyBuyPrice.htmlText = currentProduct.SkybuyPrice;
				if (currentProduct.ProdPrice != null) {
					type_1.mcInStorePrice.visible = Number(Myxml.getInstance().formatAsDigit(currentProduct.ProdPrice)) <= Number(Myxml.getInstance().formatAsDigit(currentProduct.SkybuyPrice)) ? false:true;
				}else {
					type_1.mcInStorePrice.visible  = false;
				}
				if (type_1.mcColor.visible) {
					if (cbColor) {
						cbColor.close();
						type_1.mcColor.removeChild(cbColor);
					}
					cbColor = new ComboBox();
					cbColor.setStyle("textFormat", tf);
					cbColor.width = 143.2;
					cbColor.height = 24;
					cbColor.x = 145.3;
					cbColor.y = 2.4;
					for each (var aa in currentProduct.ProdColor.split(',')) {
						cbColor.addItem( { label:Myxml.getInstance().Trim(aa)} );
					}
					type_1.mcColor.addChild(cbColor);
					cbColor.addEventListener(Event.CHANGE, hdlrColorChange);
					cbColor.setChildIndex(cbColor.getChildAt(0), 0);
					currentProduct.ProdColorIndex = 0;
				}
				if (type_1.mcSize.visible) {
					if (cbSize) {
						cbSize.close();
						type_1.mcSize.removeChild(cbSize);
					}
					cbSize = new ComboBox();
					cbSize.setStyle("textFormat", tf);
					cbSize.width = 143.2;
					cbSize.height = 24;
					cbSize.x = 145.3;
					cbSize.y = 2.6;
					type_1.mcSize.addChild(cbSize);
					for each (var bb in currentProduct.ProdSize.split(',')) {
						cbSize.addItem({label:Myxml.getInstance().Trim(bb)});
					}
					if (!type_1.mcColor.visible) {
						type_1.mcSize.y = 309.6;
					} else {
						type_1.mcSize.y = 343.1;
					}
					cbSize.addEventListener(Event.CHANGE, hdlrSizeChange);
					currentProduct.ProdSizeIndex = 0;
				}
				InitDescScroller();
			} catch (err:Error) {
				trace('Error in Product assign Text',err);
			}
		}
		//Change Event Product Color Index
		private function hdlrColorChange(evt:Event):void {
			currentProduct.ProdColorIndex = evt.target.selectedIndex;
		}
		//Change Event Product Size Index
		private function hdlrSizeChange(evt:Event):void {
			currentProduct.ProdSizeIndex = evt.target.selectedIndex;
		}
		//Show the selected in Zoom Size
		private function hdlrShowZoomedImage(evt:MouseEvent):void {
			var zoomPanel:ZoomPanel = new ZoomPanel();
			zoomPanel.Initialize(currentProduct);
			stage.addChild(zoomPanel);
			viewMorepro.closeBar(evt);
		}
		//Adding Scroll to Short Description.
		private function InitDescScroller():void {
			if (descScroller) {
				type_1.mcDesSd.removeChild(descScroller);
			}
			type_1.mcDesSd.mcDesc.y = 0;
			descScroller = new ScrollMc(type_1.mcDesSd.mcDesc, 104);
			descScroller.refreshScroll();
			type_1.mcDesSd.addChild(descScroller);
			descScroller.refreshScroll();
		}
		//
		//Adding Scroll to Short Description.
		private function InitDescScroller1():void {
			if (descScroller1) {
				type_2.mcDesSd.removeChild(descScroller1);
			}
			type_2.mcDesSd.mcDesc.y = 0;
			descScroller1 = new ScrollMc(type_2.mcDesSd.mcDesc, 104);
			descScroller1.refreshScroll();
			type_2.mcDesSd.addChild(descScroller1);
			descScroller1.refreshScroll();
		}
		//
		public function showMyCollection():void {
			if (purchaseDetail.productDet.length > 0) {
				//Refering the purchase details class to shopping cart;
				if (isProductsCheckOut()) {
					var checkout:SpecialProducts = new SpecialProducts();
					checkout.init(purchaseDetail);
					checkout.addEventListener('checkout', hdlrCheckOutShowed);
					stage.addChild(checkout);
				} else {
					showShoppingCartScreen();
				}
			} else {
				var noItems:NoItems = new NoItems();
				noItems.x = 613.6;
				noItems.y = 103.3;
				stage.addChild(noItems);
			}
		}
		//
		private function hdlrCheckOutShowed(evt:Event):void {
			showShoppingCartScreen();
		}
		//
		private function showShoppingCartScreen():void {
			shoppingCart = new ShoppingCart();
			shoppingCart.Init(purchaseDetail as PurchaseDetails);
			shoppingCart.addEventListener('remove_product', removePuchasedProd);
			shoppingCart.showBuyProduct();
			stage.addChild(shoppingCart);
			stage.swapChildren(stage.getChildByName('watermark'), shoppingCart);
		}
		//
		private function hdlrQuantiyChange(evt:Event):void {
			if (int(type_1.mcQuantity.inQuantity.text) == 0 || type_1.mcQuantity.inQuantity.text == '') {
				type_1.mcQuantity.inQuantity.text = '1';
			}
		}
		//
		private function hdlrQuantiyChange2(evt:Event):void {
			if (int(type_2.mcQuantity.inQuantity.text) == 0 || type_2.mcQuantity.inQuantity.text == '') {
				type_2.mcQuantity.inQuantity.text = '1';
			}
		}
		//
		private function removePuchasedProd(evt:MouseEvent):void {
			purchaseDetail.removeProduct(evt.currentTarget.removeProductObj as Object);
		}
		//--------------------------------------------------------------------
		//                        Check out product
		//------------------------------------------------------------------
		var tmpCheckOut:Array;
		private function extractObjWithId():Array {
			tmpCheckOut = new Array();
			var seqArr:Array = Myxml.getInstance().getCatalogueObjxml;
			var tmpArr:Array = new Array();
			for each (var obj in seqArr) {
				if (obj.SeqId == 6) {
					tmpArr.push(obj);
				}
			}
			return tmpArr;
		}
		//
		private function avoideDuplicateObj(aPro:Object):Boolean {
			for each (var aa in Main.thisClass.shoppingScreen.productDet.purchaseDetail.productDet) {
				if (aa.reffObj.OwnerId == aPro.OwnerId && aa.reffObj.ProdId == aPro.ProdId) {
					return false;
				}
			}
			tmpCheckOut.push(aPro);
			return true;
		}
		//
		private function isProductsCheckOut():Boolean {
			for each (var aa in extractObjWithId()) {
				avoideDuplicateObj(aa);
			}
			return tmpCheckOut.length > 0?true:false;
		}

	}
}