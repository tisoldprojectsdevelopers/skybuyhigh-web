﻿package com.screen{
	import com.mylocale.Myxml;
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	//Custom Classes
	import com.document.Main;
	import com.menu.Event.MenuEvent;
	import com.menu.Menu;
	import com.screen.*;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Shopping extends MovieClip {
		public static  var insShopping:*;
		//
		private var noOfMenuBtn:int = 5;
		private var drDownMenu:Menu;
		private var targetMenu:MovieClip;
		//
		public var coverFlow:CoverFlow;
		public var productDet:ProductDetails;
		public var priJet:PrivateJet;
		//
		private var tween:Tween;
		public var tmp:Array;
		public function Shopping() {
			focusRect = false;
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			//
			tmp = [{_id:1, name:'women', arrPos:'135'},{_id:2, name:'men', arrPos:'285'} , {_id:4,name:'gift', arrPos:'442'}, {_id:5, name:'packages',  arrPos:'615'}, {_id:3, name:'personal',  arrPos:'810'}];
			insShopping = this;
			//
			backToHome.focusRect = false;
			myCollection.focusRect = false;
			//
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW' && Myxml.getInstance().getCatalogueType().toUpperCase() != 'CATALOGUE_PREVIEW') {
				
				myCollection.addEventListener(MouseEvent.MOUSE_DOWN, hdlrShowShoppingCart);
				if (!Main.thisClass.isItWeb) {
					loadBottomAirlineLogo();
				}
			}else {
				myCollection.enabled = false
			}
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'PRODUCT_PREVIEW') {
				backToHome.enabled = false;
			}else {
				backToHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrGotoIntro);
				loadPerShopperScreen();
				addMenuListener();
			}
			initialize();
		}
		//
		private function loadPerShopperScreen():void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				if (!Main.thisClass.isItWeb) {
					Main.thisClass.personalShopper = 'personal_shopper.swf'
				}
				try {
					var ldr:Loader = new Loader();
					ldr.load(new URLRequest(Main.thisClass.personalShopper));
					mcPersonalShop.addChild(ldr);
					mcPersonalShop.visible = false;
				}catch (err:Error) {
					trace("Error in Personal Shopper Loading");
					//throw new Error('Error in Personal Shopper Loading')
				}
			}
		}
		//
		public function hidePersonalShoper():void {
			mcPersonalShop.visible = false;
		}
		//Handler back to Intro page
		private function hdlrGotoIntro(evt:MouseEvent):void {
			productDet.viewMorepro.closeBar(evt);
			dispatchEvent(new MouseEvent('Intro'));
		}
		//Activate the Listeners for Menu buttons
		private function addMenuListener():void {
			for (var i = 0; i < tmp.length; i++) {
				this['menuBtn_' + tmp[i].name].id = tmp[i]._id;
				this['menuBtn_' + tmp[i].name].pos = tmp[i].arrPos;
				this['menuBtn_' + tmp[i].name].gotoAndStop(1);
				this['menuBtn_' + tmp[i].name].focusRect = false;
				this['menuBtn_' + tmp[i].name].buttonMode = true;
				this['menuBtn_' + tmp[i].name].addEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
				this['menuBtn_' + tmp[i].name].addEventListener(MouseEvent.ROLL_OUT, hdlrRollOut);
				if (tmp[i].name == 'packages') {
					this['menuBtn_' + tmp[i].name].addEventListener(MouseEvent.MOUSE_DOWN, hdlrMenuPackagesDown);
				}
				if (tmp[i].name == 'personal') {
					this['menuBtn_' + tmp[i].name].addEventListener(MouseEvent.MOUSE_DOWN, hdlrMenuMouseDown);
				}
			}
		}
		//setActive Window
		public function setActivePos(aSepId:int):void {
			for (var a in tmp) {
				if (tmp[a]._id == aSepId) {
					active.x = tmp[a].arrPos;
				}
			}
		}
		//Menu Button Down for Pacakges
		private function hdlrMenuMouseDown(evt:MouseEvent):void {
			if (productDet.viewMorepro.isOpen) {
				productDet.viewMorepro.closeBar(evt);
			}
			setActivePos(evt.target.id);
			mcPersonalShop.visible = true;
			this.setChildIndex(mcPersonalShop, this.numChildren - 1);
		}
		//Handler RollOver
		private function hdlrRollOver(evt:MouseEvent):void {
			evt.target.gotoAndStop(2);
			this.setChildIndex(evt.currentTarget as DisplayObject, this.numChildren - 1);
			var menuType:String= evt.target.name.split('_')[1];
			try {
				if (menuType != 'packages' &&  menuType != 'personal') {
					drDownMenu = new Menu();
					drDownMenu.y = evt.currentTarget.height;
					drDownMenu.addMenu(menuType, evt.target.id, true);
					evt.currentTarget.addChild(drDownMenu);
					drDownMenu.addEventListener('menuItemSelect', hdlrMenuItemSeleted);
				}
			} catch (err:Error) {
				trace('Error in Menu Creation', err);
			}
		}
		//Handler RollOut
		private function hdlrRollOut(evt:MouseEvent):void {
			var menuType:String= evt.target.name.split('_')[1];
			if (menuType != 'packages' &&  menuType != 'personal') {
				removeMenuIns(evt.target as MovieClip);
			}
			evt.target.gotoAndStop(1);
		}
		//Remove menu instance
		private function removeMenuIns(aTarget:MovieClip):void {
			if (drDownMenu) {
				try {
					if (aTarget.contains(drDownMenu)) {
						drDownMenu.killMe();
					}
				} catch (e:Error) {
					trace('Error in dropdown rollout');
				}
			}
		}
		//MouseDown Airline packages
		private function hdlrMenuPackagesDown(evt:MouseEvent):void {
			Main.thisClass.fromIntroPage = false;
			if (productDet.viewMorepro.isOpen) {
				productDet.viewMorepro.closeBar(evt);
			}
			mcPersonalShop.visible = false;
			setActivePos(evt.target.id);
			var noOfmenuItems:int = Myxml.getInstance().getLength('5');
			showCoverFlowScreen(true);
			var obj:Object = { OwnerId:null, OwnerName:'All', CategoryId:5, ProductId:noOfmenuItems };
			coverFlow.initialize(obj);
		}
		/*
		 * Getting the selted menu Item Properties or menu button properties
		 */
		private function hdlrMenuItemSeleted(evt:MouseEvent):void {
			coverFlow.initialize(evt.currentTarget.currentObj as Object);
			setActivePos(evt.target.parent.id);
			if (productDet.viewMorepro.isOpen) {
				productDet.viewMorepro.closeBar(evt);
			}
			try {
				mcPersonalShop.visible = false;
				drDownMenu.killMe();
			} catch (err:Error) {
				trace('Menu Item Down ---- ', evt.currentTarget.currentObj);
			}
			Main.thisClass.fromIntroPage = false;
			showCoverFlowScreen(true);
		}
		/* Load a Bottom Air line logo in Shopping Screen */
		private function loadBottomAirlineLogo():void {
			if (!Main.thisClass.isItWeb) {
				var ldr:Loader = new Loader();
				ldr.load(new URLRequest(Myxml.getInstance().categories_path('AirLogoPath')));
				ldr.scaleX = ldr.scaleY = 0.85;
				logos.logo.addChild(ldr);
			}
		}
		/* Creating Instance of Coverflow screen and Product detail screen*/
		private function initialize():void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				coverFlow = new CoverFlow();
				mcCoverFlow.addChild(coverFlow);
				coverFlow.addEventListener('itemSelected', showProduct);
			}
			//
			productDet = new ProductDetails();
			productDet.addEventListener('showCoverFlow', hdlrShowCoverFlow);
			productDet.addEventListener('showNextImage', hdlrShowNextProScreenImg);
			addChild(productDet);
			productDet.mask = mcMask;
			showProductDetailScreen(false);
			resetIndex();
		}
		// Mouse Down Change next product screen Image
		private function hdlrShowNextProScreenImg(evt:Event):void {
			coverFlow.showNextImageFromProductDet(evt);
			productDet.displayProduct(coverFlow.currentItem);
		}
		// Mouse Down Handler showing the cover flow images from Product details
		private function hdlrShowCoverFlow(evt:Event):void {
			showCoverFlowScreen(true);
			var obj:Object = new Object();
			obj.ProdId = productDet.currentProduct.ProdId;
			obj.OwnerId = Main.thisClass.fromIntroPage ? null : coverFlow.currOwnerId;
			obj.CategoryId = productDet.currentProduct.SeqId;
			coverFlow.addDummyLoading();
			coverFlow.initialize(obj);
		}
		/* Show and Hide Coverflow screen 
		 * @Param - Boolean value indication show or hide */
		public function showCoverFlowScreen(isVisible:Boolean):void {
			productDet.visible = !isVisible;
			coverFlow.visible = isVisible;
			doTween(coverFlow as MovieClip);
		}
		/* Show and Hide Product detail screen 
		 * @Param - Boolean value indication show or hide*/
		public function showProductDetailScreen(isVisible:Boolean):void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				coverFlow.visible = !isVisible;
			}
			productDet.visible = isVisible;
			addDummyLoading();
			doTween(productDet as MovieClip);
		}
		/* Display the selected Cover flow Product */
		private function showProduct(evt:Event):void {
			showProdDet(evt.target.currentItem as Object);

		}
		//Showing Advertisement Packates
		public function showProdDet(aObj:Object):void {
			if (aObj.Adv.toUpperCase() == 'Y') {
				if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
					coverFlow.visible = false;
				}
				productDet.visible = false;
				priJet = new PrivateJet();
				priJet.addEventListener(Event.REMOVED, hdlrShowCoverFlowScreen);
				priJet.Initialize(aObj);
				addChild(priJet);
			} else {
				showProductDetailScreen(true);
				productDet.displayProduct(aObj);
			}
		}
		//Show Coverflow Screen from PrivatJet
		private function hdlrShowCoverFlowScreen(evt:Event):void {
			evt.currentTarget.removeEventListener(Event.REMOVED, hdlrShowCoverFlowScreen);
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				coverFlow.visible = true;
			}
		}
		/*Tween Object*/
		private function doTween(aMovie:MovieClip):void {
			if (tween) {
				tween.stop();
			}
			tween = new Tween(aMovie, 'alpha', Strong.easeOut, 0, 1, 0.3, true);
		}
		/*Show Shopping Cart */
		private function hdlrShowShoppingCart(evt:MouseEvent):void {
			productDet.viewMorepro.closeBar(evt);
			productDet.showMyCollection();
		}
		//Reset the index of menu buttons
		private function resetIndex():void {
			var tmp:Array = [ { _id:1, name:'women', arrPos:'135' }, { _id:2, name:'men', arrPos:'285' } , { _id:4, name:'gift', arrPos:'442' }, { _id:5, name:'packages',  arrPos:'615' }, { _id:3, name:'personal',  arrPos:'810' } ];
			for (var i = 0; i < tmp.length; i++) {
				this.setChildIndex(this['menuBtn_' + tmp[i].name], this.numChildren - 1);
			}
		}//show Dummy Loading
		private function addDummyLoading():void {
			var dm:DummyLoading = new DummyLoading();
			addChild(dm);
		}
	}
}