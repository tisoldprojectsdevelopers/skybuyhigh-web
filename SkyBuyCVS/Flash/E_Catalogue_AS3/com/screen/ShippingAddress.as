﻿package com.screen 
{
	import com.mylocale.StateCode;
	import fl.controls.CheckBox;
	import fl.controls.ComboBox;
	import fl.data.DataProvider;
	import fl.managers.FocusManager;
	import fl.motion.ITween;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.*;
	import flash.utils.SetIntervalTimer;
	import flash.utils.Timer;
	import fl.controls.CheckBox;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fl.controls.TextInput;
	//
	import com.utils.EmailValidator;
	import com.document.Main
	import com.mylocale.Myxml;
	import flash.system.fscommand;
	import com.document.WebServiceParam;
	import flash.external.ExternalInterface;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ShippingAddress extends MovieClip
	{
		public var custDetailsXML:String;
		public var strShoppingBagXML:String;
		
		//
		public var tailNo:GetFlightNo;
		
		public var orderconfirm:*;
		
		public var emailId:String;
		private var tfTimer:Timer;
		
		private var serviceSuggestion:String = '';
		private var suggestion:String = '';
		
		private var isBillState:Boolean;
		private var isShipState:Boolean;
		var fm:FocusManager 
		public function ShippingAddress() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			focusRect = false;
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			//
			dummyMC.visible = false;
			btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSubmit);
			btnCancel.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCancel);
			btnBack.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackToCreditcardDet);
			btnHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackHome);
			chkAddress.addEventListener(Event.CHANGE, hdlrCheckBox);
			//
			initializecomboBox();
			//if () {
				
			//}
			//loading.visible = false; 
			insSpecial.visible = false; 
			cusSuggestion.visible = false;
			//
			cusSuggestion.focusRect = false;
			spl.focusRect = false;
			//
			cusSuggestion.btnSubmit.focusRect = false;
			cusSuggestion.btnClose.focusRect = false;
			//
			insSpecial.btnClose.focusRect = false;
			insSpecial.btnSubmit.focusRect = false;
			//
			insSpecial.btnClose.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCloseSpecialIns);
			insSpecial.btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSubmitSpecialIns);
			spl.addEventListener(MouseEvent.MOUSE_DOWN, openPopup);
			//Assigh the text for special text boxes
			sendSuggestion.visible = Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB'  ? false:false;
			sendSuggestion.addEventListener(MouseEvent.MOUSE_DOWN, showSuggestionBox);
			sendSuggestion.buttonMode = true;
			//
			mcStateDisable.visible = false;
			setDefalutValue(); 
		}
		//Open Special Instruction Popup
		private function openPopup(evt:MouseEvent):void {
			insSpecial.visible = true;
			insSpecial.splInstruction.text = "";
			insSpecial.personalInformation.text = "";
			try {
				if(Main.thisClass.splInstruction != null){
					if (Myxml.getInstance().Trim(Main.thisClass.splInstruction).length > 0) {
						insSpecial.splInstruction.text = Main.thisClass.splInstruction;
					}
				}
				if(insSpecial.personalInformation != null){
					if (Myxml.getInstance().Trim(insSpecial.personalInformation).length > 0) {
						insSpecial.personalInformation.text = Main.thisClass.personalInformation;
					}
				}
			}catch (err:Error) {
				trace('Erro in Special Instruction', err);
			}
		}
		//Close the special instruction window and send the instruction
		private function hdlrSubmitSpecialIns(evt:MouseEvent):void {
			Main.thisClass.splInstruction = insSpecial.splInstruction.text;
			Main.thisClass.personalInformation = insSpecial.personalInformation.text;
			insSpecial.visible = false; ;
		}
		//Assign Special Suggestio
		private function showSuggestionBox(evt:MouseEvent):void {
			cusSuggestion._servicetxt.text = "";
			cusSuggestion._suggestionTxt.text = "";
			if (serviceSuggestion.length > 0 ) {
				cusSuggestion._servicetxt.text = serviceSuggestion;
			}
			if (suggestion.length > 0 ) {
				cusSuggestion._suggestionTxt.text = suggestion;
			}
			cusSuggestion.visible = true;
			
			
			cusSuggestion.btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSubmitFeedBack);
			cusSuggestion.btnClose.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCloseSuggestionBox);
		}
		private function hdlrCloseSuggestionBox(evt:MouseEvent):void {
			cusSuggestion.visible = false;
		}
		private function hdlrSubmitFeedBack(evt:MouseEvent):void {
			serviceSuggestion = Myxml.getInstance().Trim(cusSuggestion._servicetxt.text).length > 0 ? Myxml.getInstance().Trim(cusSuggestion._servicetxt.text):'';
			suggestion = Myxml.getInstance().Trim(cusSuggestion._suggestionTxt.text).length > 0 ? Myxml.getInstance().Trim(cusSuggestion._suggestionTxt.text):'';
			cusSuggestion.visible = false;
		}
		//Close the special instruction window
		private function hdlrCloseSpecialIns(evt:MouseEvent):void {
			insSpecial.visible = false; 
		}
		//Initialize the Combo box and Tab index;
		private function initializecomboBox():void {
			//
			chkAddress.focusEnabled= false;
			chkAddress.focusRect= false;
			chkAddress.tabEnabled = false;
			//
			
			var tf:TextFormat = new TextFormat();
			tf.size = 12;
			tf.font = 'Arial'
			//
			var stCode:StateCode = new StateCode();
			
			var biDp:DataProvider = new DataProvider(stCode.stateCode);
			var shDp:DataProvider = new DataProvider(stCode.stateCode);
			
			oBillingState.dataProvider = biDp;
			oBillingState.sortItemsOn("label", "ASC"); 
			
			//		
			oBillingState.textField.setStyle("textFormat", tf);
			oShippingState.dataProvider = shDp;
			oShippingState.textField.setStyle("textFormat", tf);
			//
			fm = new FocusManager(this);
			fm.setFocus(oBillingFName);
			//Setting the Tab indees
			oBillingFName.tabIndex = 0;
			dummyMC.tabIndex = 20;
			
			oBillingFName.maxChars = 64;
			oBillingFName.setStyle("textFormat", tf);
			//
			oBillingLName.maxChars = 64;
			oBillingLName.setStyle("textFormat", tf);
			//
			oBillingAddr1.maxChars = 128;
			oBillingAddr1.setStyle("textFormat", tf);
			//
			oBillingAddr2.maxChars = 128;
			oBillingAddr2.setStyle("textFormat", tf);
			//
			oBillingCity.maxChars = 64;
			oBillingCity.setStyle("textFormat", tf);
			//
			oBillingState.addEventListener(Event.OPEN, hdlrBillStateOpen);
			oBillingState.addEventListener(Event.CLOSE, hdlrBillStateClose);
			oBillingState.setStyle("textFormat", tf);
			//
			oBillingZip.maxChars = 9;
			oBillingZip.restrict = '0-9'
			oBillingZip.setStyle("textFormat", tf);
			//
			billingPhoneNumber.restrict = '0-9'
			billingPhoneNumber.maxChars = 10;
			billingPhoneNumber.setStyle("textFormat", tf);
			//
			oBillingEmail.setStyle("textFormat", tf);
			//
			oShippingFName.setStyle("textFormat", tf);
			//
			oShippingLName.setStyle("textFormat", tf);
			//
			oShippingAddr1.setStyle("textFormat", tf);
			//
			oShippingAddr2.setStyle("textFormat", tf);
			//
			oShippingCity.setStyle("textFormat", tf);
			//
			oShippingState.addEventListener(Event.OPEN, hdlrShipStateOpen);
			oShippingState.addEventListener(Event.CLOSE, hdlrShipStateClose);
			//
			oShippingState.setStyle("textFormat", tf);
			oShippingState.sortItemsOn("label", "ASC"); 
			//
			oShippingZip.maxChars = 9;
			oShippingZip.restrict = '0-9'
			oShippingZip.setStyle("textFormat", tf);
			//
			shippingPhoneNumber.restrict = '0-9'
			shippingPhoneNumber.maxChars = 10;
			shippingPhoneNumber.setStyle("textFormat", tf);
			//
			oShippingEmail.setStyle("textFormat", tf);
			//
			oBillingFName.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingLName.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingAddr1.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingAddr2.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingCity.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingState.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingZip.addEventListener(Event.CHANGE, handlerTextChange);
			billingPhoneNumber.addEventListener(Event.CHANGE, handlerTextChange);
			oBillingEmail.addEventListener(Event.CHANGE, handlerTextChange);
		}
		//Handler Text change
		private function handlerTextChange(evt:Event):void {
			if (chkAddress.selected) {
				switch(evt.currentTarget.name) {
					case 'oBillingFName':
					oShippingFName.text = evt.currentTarget.text;
					break;
					
					case 'oBillingLName':
					oShippingLName.text = evt.currentTarget.text;
					break;
					
					case 'oBillingAddr1':
					oShippingAddr1.text = evt.currentTarget.text;
					break;
					
					case 'oBillingAddr2':
					oShippingAddr2.text = evt.currentTarget.text;
					break;
					
					case 'oBillingCity':
					oShippingCity.text = evt.currentTarget.text;
					break;
					
					case 'oBillingState':
					oShippingState.selectedIndex = evt.currentTarget.selectedIndex;
					break;
					
					case 'oBillingZip':
					oShippingZip.text = evt.currentTarget.text;
					break;
					
					case 'billingPhoneNumber':
					shippingPhoneNumber.text = evt.currentTarget.text;
					break;
					
					case 'oBillingEmail':
					oShippingEmail.text = evt.currentTarget.text;
					break;
				}
			}
		}
		// Close the Combo
		private function closeComboBox():void {
			if (isBillState) { oBillingState.close(); isBillState = false; }
			if (isShipState) { oShippingState.close(); isShipState = false; }
		}
		//
		private function hdlrSubmit(evt:MouseEvent):void {
			closeComboBox();
			fm.deactivate();
			dummyMC.focusRect = false;
			fm.setFocus(dummyMC);
			var sameAsBillAddr:Boolean;
			validateRequiredFields();
			validateFields();
			if (validateRequiredFields() && validateFields()) {
				custDetailsXML = "<CustDetails>";
				if (chkAddress.selected) {
					custDetailsXML = custDetailsXML+"<SameAsShippingAddr>true</SameAsShippingAddr>";
				} else {
					custDetailsXML = custDetailsXML+"<SameAsShippingAddr>false</SameAsShippingAddr>";
				}
				emailId = oBillingEmail.text;
				var shoAdd2:String = oShippingAddr2.length > 0 ? oShippingAddr2.text :'';
				custDetailsXML = custDetailsXML+"<ShippingFname><![CDATA["+oShippingFName.text+"]]></ShippingFname>";
				custDetailsXML = custDetailsXML+"<ShippingLName><![CDATA["+oShippingLName.text+"]]></ShippingLName>";
				custDetailsXML = custDetailsXML+"<ShippingAddr1><![CDATA["+oShippingAddr1.text+"]]></ShippingAddr1>";
				custDetailsXML = custDetailsXML+"<ShippingAddr2><![CDATA["+shoAdd2+"]]></ShippingAddr2>";
				custDetailsXML = custDetailsXML+"<ShippingState><![CDATA["+oShippingState.value+"]]></ShippingState>";
				custDetailsXML = custDetailsXML+"<ShippingCity><![CDATA["+oShippingCity.text+"]]></ShippingCity>";
				var zip:String = oShippingZip.text;
				var varStr1:String = "";
				var varStr2:String = "";
				if (zip.charAt(5) == "-") {
					varStr1 = zip.substring(0, 5);
					varStr2 = zip.substring(6, zip.length);
					zip = varStr1+varStr2;
				}
				custDetailsXML = custDetailsXML+"<ShippingZip>"+zip+"</ShippingZip>";
				custDetailsXML = custDetailsXML+"<ShippingEmail><![CDATA["+oShippingEmail.text+"]]></ShippingEmail>";
				custDetailsXML = custDetailsXML + "<ShippingPhone>" + shippingPhoneNumber.text + "</ShippingPhone>";
				//
				var billAdd2:String = oBillingAddr2.length > 0 ? oBillingAddr2.text :'';
				custDetailsXML = custDetailsXML+"<BillingFName><![CDATA["+oBillingFName.text+"]]></BillingFName>";
				custDetailsXML = custDetailsXML+"<BillingLName><![CDATA["+oBillingLName.text+"]]></BillingLName>";
				custDetailsXML = custDetailsXML+"<BillingAddr1><![CDATA["+oBillingAddr1.text+"]]></BillingAddr1>";
				custDetailsXML = custDetailsXML+"<BillingAddr2><![CDATA["+billAdd2+"]]></BillingAddr2>";
				custDetailsXML = custDetailsXML+"<BillingState><![CDATA["+oBillingState.value+"]]></BillingState>";
				custDetailsXML = custDetailsXML+"<BillingCity><![CDATA["+oBillingCity.text+"]]></BillingCity>";
				zip = oBillingZip.text;
				varStr1 = "";
				varStr2 = "";
				if (zip.charAt(5) == "-") {
					varStr1 = zip.substring(0, 5);
					varStr2 = zip.substring(6, zip.length);
					zip = varStr1+varStr2;
				}
				var splInstruction:String = Myxml.getInstance().Trim(insSpecial.splInstruction.text).length > 0 ? insSpecial.splInstruction.text:'';
				var personalInformation:String = Myxml.getInstance().Trim(insSpecial.personalInformation.text).length > 0 ?insSpecial.personalInformation.text:'';
				custDetailsXML = custDetailsXML+"<BillingZip>"+zip+"</BillingZip>";
				custDetailsXML = custDetailsXML+"<BillingEmail><![CDATA["+oBillingEmail.text+"]]></BillingEmail>";
				custDetailsXML = custDetailsXML+"<BillingPhone>"+billingPhoneNumber.text+"</BillingPhone>";
				custDetailsXML = custDetailsXML+"<SpecialInstruction><![CDATA["+splInstruction+"]]></SpecialInstruction>";
				custDetailsXML = custDetailsXML+"<PersonalInformation><![CDATA["+personalInformation+"]]></PersonalInformation>";
				custDetailsXML = custDetailsXML + "&TailNo&";
				custDetailsXML = custDetailsXML+"</CustDetails>";
				//trace("_root.custDetailsXML"+_root.custDetailsXML);
				if (chkAddress.selected) {
					sameAsBillAddr = true;
				} else {
					sameAsBillAddr = false;
				}
				strShoppingBagXML = "<ShoppingBag>";
				var ShoppingBag:Array = Main.thisClass.shoppingScreen.productDet.purchaseDetail.productDet;
					for (var i=0; i<ShoppingBag.length; i++) {
						var vendorid:String = ShoppingBag[i].reffObj.OwnerId.length !=  null ? ShoppingBag[i].reffObj.OwnerId:'';
						var categoryid:String = ShoppingBag[i].reffObj.CateId.length !=  null ? ShoppingBag[i].reffObj.CateId:'';
						var productname:String = ShoppingBag[i].reffObj.ProdTitle !=  null ? ShoppingBag[i].reffObj.ProdTitle:'';
						var productid:String = ShoppingBag[i].reffObj.ProdId != null ? ShoppingBag[i].reffObj.ProdId:'';
						var productcode:String = ShoppingBag[i].reffObj.ProdCode != null ? ShoppingBag[i].reffObj.ProdCode:'';
						var productsd:String = ShoppingBag[i].reffObj.ProdSD != null ? ShoppingBag[i].reffObj.ProdSD:'';
						var skybuyprice:String = ShoppingBag[i].reffObj.SkybuyPrice != null ? ShoppingBag[i].reffObj.SkybuyPrice:'';
						var vendorprice:String = ShoppingBag[i].reffObj.ProdPrice != null ? ShoppingBag[i].reffObj.ProdPrice:''; 
						var quantity:String = ShoppingBag[i].Quantity != null ? ShoppingBag[i].Quantity:'';
					strShoppingBagXML = strShoppingBagXML+"<Product>";
					strShoppingBagXML = strShoppingBagXML+"<VendorId>"+vendorid+"</VendorId>";
					strShoppingBagXML = strShoppingBagXML+"<CategoryId>"+categoryid+"</CategoryId>";
					strShoppingBagXML = strShoppingBagXML+"<ProductStatus>"+'A'+"</ProductStatus>";
					strShoppingBagXML = strShoppingBagXML+"<ProductName><![CDATA["+productname+"]]></ProductName>";
					strShoppingBagXML = strShoppingBagXML+"<ProductId>"+productid+"</ProductId>";
					strShoppingBagXML = strShoppingBagXML+"<ProductCode><![CDATA["+productcode+"]]></ProductCode>";
					strShoppingBagXML = strShoppingBagXML+"<ProductSD><![CDATA["+productsd+"]]></ProductSD>";
					strShoppingBagXML = strShoppingBagXML+"<SkyBuyPrice>"+skybuyprice+"</SkyBuyPrice>";
					strShoppingBagXML = strShoppingBagXML+"<VendorPrice>"+vendorprice+"</VendorPrice>";
					strShoppingBagXML = strShoppingBagXML +"<Quantity>" + quantity + "</Quantity>";
					var color:String = '';
					var size:String = '';
					if (ShoppingBag[i].reffObj.ProdColor != null) {
						color = ShoppingBag[i].reffObj.ProdColor.split(',')[ShoppingBag[i].ProdColorIndex] == undefined ? "" : Myxml.getInstance().Trim(ShoppingBag[i].reffObj.ProdColor.split(',')[ShoppingBag[i].ProdColorIndex]);
					}
					if (ShoppingBag[i].reffObj.ProdSize != null) {
						 size = ShoppingBag[i].reffObj.ProdSize.split(',')[ShoppingBag[i].ProdSizeIndex] == undefined ? "" :Myxml.getInstance().Trim(ShoppingBag[i].reffObj.ProdSize.split(',')[ShoppingBag[i].ProdSizeIndex]);
					}
					strShoppingBagXML = strShoppingBagXML+"<Color><![CDATA["+color+"]]></Color>";
					strShoppingBagXML = strShoppingBagXML +"<Size><![CDATA["+ size +"]]></Size>";
					
					var strTravelDt:String = "";
					if (ShoppingBag[i].reffObj.SeqId == "5") {
						var monthArr:Array = [0, 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
						var day:int ;
						var month:int;
						for (var a in monthArr) {
							if (monthArr[a] == ShoppingBag[i].ProdDate.split(' ')[1].toLowerCase()) {
								month = a;
								break;
							}
						}
						var dayStr:String = int(ShoppingBag[i].ProdDate.split(' ')[0]) < 10 ? '0' + String(ShoppingBag[i].ProdDate.split(' ')[0]):String(ShoppingBag[i].ProdDate.split(' ')[0]);
						var monthStr:String  = month < 10 ? '0' + String(month) :String(month);
						strTravelDt = monthStr + "/" + dayStr + "/" + ShoppingBag[i].ProdDate.split(' ')[2];
						trace('----------'+strTravelDt)
					}
					strShoppingBagXML = strShoppingBagXML+"<TravelDate>"+strTravelDt+"</TravelDate>";
					strShoppingBagXML = strShoppingBagXML+"</Product>";	
				}
				strShoppingBagXML = strShoppingBagXML + "</ShoppingBag>";
				//
				tfTimer = new Timer(10, 0);
				tfTimer.addEventListener(TimerEvent.TIMER, hdlrTimerComplete);
				tfTimer.start();
				//
				
				//loading.visible = true;
				//loading.play();
				
				if (Myxml.getInstance().getCatalogueType().toUpperCase() == "DEMO" || Myxml.getInstance().getCatalogueType().toUpperCase() == "WEB" || Main.thisClass.isItWeb == false) {
					var dt:Date = new Date();
					var date:Number = dt.getDate();
					var tmp:String = "";
					if (date<=9) {
						tmp = "0"+date;
					} else {
						tmp = ""+date;
					}
					var monthSend:Number = dt.getMonth()+1;
					var tmp1:String = "";
					if (monthSend<=9) {
						tmp1 = "0"+monthSend;
					} else {
						tmp1 = ""+monthSend;
					}
					var tmp2:String = ""+dt.getFullYear();
					tmp2 = tmp2.substring(tmp2.length-2, tmp2.length);
					Main.thisClass.vOrderNumber = Myxml.getInstance().Trim(billingPhoneNumber.text) + "-" + tmp1 + tmp + tmp2 + "-" + dt.getHours() + "" + dt.getSeconds() + "" + dt.getMilliseconds();
					if (Myxml.getInstance().getCatalogueType().toUpperCase() == "DEMO") {
						Main.thisClass.isValidOrder = "Success";
					}
				}
			}
		}
		//
		private function hdlrTimerComplete(evt:TimerEvent):void {
			tfTimer.stop();
			sendDetailsToServer();
		}
		//Send Information to Server
		private function sendDetailsToServer():void {
			tailNo = new GetFlightNo();
			addChild(tailNo);
			/*if (Myxml.getInstance().getCatalogueType().toUpperCase() == "WEB" && Main.thisClass.isItWeb) {
				dummyMC.visible = true;
				var tf:Timer = new Timer(500);
				tf.addEventListener(TimerEvent.TIMER, sendSOAPMessage);
				tf.start();
				
			}else if (Main.thisClass.isItWeb == false || Myxml.getInstance().getCatalogueType() == 'device') {
				tailNo = new GetFlightNo();
				addChild(tailNo);
				fscommand("ShoppingBag", strShoppingBagXML);
				fscommand("CustDetails", custDetailsXML);
				fscommand("CreditCardDetails", Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.ccDetailsXML);
			}else if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
				tailNo = new GetFlightNo();
				addChild(tailNo);
			}*/
		}
		//
		/*private function sendSOAPMessage(evt:TimerEvent):void {
			evt.currentTarget.stop();
			trace('Webservice---------------------------------------------');
			var soapmsg:WebServiceParam = new WebServiceParam(); 
			soapmsg.addEventListener(WebServiceParam.SUCCESS, hdlrSOAPMsgSended)
			soapmsg.sendPerchaseDet(Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.ccDetailsXML, custDetailsXML, serviceSuggestion, suggestion);
		}
		//
		private function hdlrSOAPMsgSended(evt:Event):void {
			Main.thisClass.isValidOrder = evt.currentTarget.successMsg;
			ExternalInterface.call("textFunction", Main.thisClass.isValidOrder);
			
			shoOrederConformationScreen();
		}*/
		public function shoOrederConformationScreen():void {
			if (Main.thisClass.isItWeb && Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB') 
			{
				orderconfirm = new OrderConfirm();
				orderconfirm.mailId = emailId;
				addChild(orderconfirm);
				dummyMC.visible = false;
			}
			
		}
		//MosueDown handler when the home button clicks
		private function hdlrBackHome(evt:MouseEvent):void {
			//killMe();
			closeComboBox();
			Main.thisClass.shoppingScreen.productDet.initText();
			Main.thisClass.shoppingScreen.productDet.shoppingCart.killMe();
			Main.thisClass.showIntroPage(true);
			
		}
		//MouseDown handler when the Cancel button clicks
		private function hdlrCancel(evt:MouseEvent):void {
			closeComboBox();
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.shoppingScreen.productDet.initText();
			Main.thisClass.shoppingScreen.no_of_items.text = '( 0 item )';
			Main.thisClass.shoppingScreen.productDet.purchaseDetail.purchaseDet = new Array();
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//Back to show shopping cart screen
		private function hdlrBackToCreditcardDet(evt:MouseEvent):void {
			Main.thisClass.shoppingScreen.productDet.shoppingCart.creditCard.initComponents();
			killMe();
		}
		//Kill me
		private function killMe():void {
			closeComboBox();
			parent.removeChild(this);
		}
		//
		private	function validateFields():Boolean {
			var isValid:Boolean = true;
			var varTmp:String = "";
			var varTmpLength:Number = 0;
			var _validator:EmailValidator = new EmailValidator();

			if (Myxml.getInstance().Trim(shippingPhoneNumber.text).length != 10 && Myxml.getInstance().Trim(shippingPhoneNumber.text).length>0) {
				invalidShippingPh.alpha = 1;
				shippingPhoneReq.alpha = 0;
				isValid = false;
			} else {
				invalidShippingPh.alpha = 0;
			}
			if (Myxml.getInstance().Trim(billingPhoneNumber.text).length != 10 && Myxml.getInstance().Trim(billingPhoneNumber.text).length > 0) {
				invalidBillingPh.alpha = 1;
				billingPhoneReq.alpha = 0;
				if (chkAddress.selected) {
					invalidShippingPh.alpha = 1;
					shippingPhoneReq.alpha = 0;
				}
				isValid = false;
			} else {
				invalidBillingPh.alpha = 0;
				if (chkAddress.selected) {
					invalidShippingPh.alpha = 0;
				}
			}
			if (Myxml.getInstance().Trim(oShippingEmail.text).length>0 && _validator.validate(oShippingEmail.text) == false) {
				invalidShippEmail.alpha = 1;
				isValid = false;
			} else {
				invalidShippEmail.alpha = 0;
			}
			if (Myxml.getInstance().Trim(oBillingEmail.text).length>0 && _validator.validate(oBillingEmail.text) == false) {
				invalidBillEmail.alpha = 1;
				if (chkAddress.selected) {
					invalidShippEmail.alpha = 1;
				}
				isValid = false;

			} else {
				invalidBillEmail.alpha = 0;
				if (chkAddress.selected) {
					invalidShippEmail.alpha = 0;
				}
			}
			varTmp = Myxml.getInstance().Trim(oShippingZip.text);
			varTmpLength = varTmp.length;
			if (varTmp.charAt(5) == "-") {
				if (Myxml.getInstance().Trim(oShippingZip.text).length>0 && Myxml.getInstance().Trim(oShippingZip.text).length != 10) {
					invalidShippZip.alpha = 1;
					isValid = false;
				} else {
					invalidShippZip.alpha = 0;
					invalidBillZip.alpha = 0;
				}
			} else {
				if (Myxml.getInstance().Trim(oShippingZip.text).length>0 && Myxml.getInstance().Trim(oShippingZip.text).length != 9) {
					if (Myxml.getInstance().Trim(oShippingZip.text).length != 5) {
						invalidShippZip.alpha = 1;
						isValid = false;
					} else {
						invalidShippZip.alpha = 0;
						invalidBillZip.alpha = 0;
					}
				} else {
					invalidShippZip.alpha = 0;
					invalidBillZip.alpha = 0;
				}
			}
			if (varTmp.length == 10 || varTmp.length == 9) {
				if (varTmp.indexOf("-", 0)>=0) {
					if (varTmp.indexOf("-", 0) != 5 && varTmp.lastIndexOf("-", 0) != 5) {
						invalidShippZip.alpha = 1;
						isValid = false;
					}
				}
			}
			varTmp = Myxml.getInstance().Trim(oBillingZip.text);
			varTmpLength = varTmp.length;
			if (varTmp.charAt(5) == "-") {
				if (Myxml.getInstance().Trim(oBillingZip.text).length>0 && Myxml.getInstance().Trim(oBillingZip.text).length != 10) {
					invalidBillZip.alpha = 1;
					if (chkAddress.selected) {
						invalidShippZip.alpha = 1;
					}
					isValid = false;
				} else {
					invalidBillZip.alpha = 0;
					if (chkAddress.selected) {
						invalidShippZip.alpha = 0;
					}
				}
			} else {
				if (Myxml.getInstance().Trim(oBillingZip.text).length>0 && Myxml.getInstance().Trim(oBillingZip.text).length != 9) {
					if (Myxml.getInstance().Trim(oBillingZip.text).length != 5) {
						invalidBillZip.alpha = 1;
						if (chkAddress.selected) {
							invalidShippZip.alpha = 1;
						}
						isValid = false;
					} else {
						invalidBillZip.alpha = 0;
						if (chkAddress.selected) {
							invalidShippZip.alpha = 0;
						}
					}
				} else {
					invalidBillZip.alpha = 0;
					if (chkAddress.selected) {
						invalidShippZip.alpha = 0;
					}
				}
			}
			if (varTmp.length == 10 || varTmp.length == 9) {
				if (varTmp.indexOf("-", 0)>=0) {
					if (varTmp.indexOf("-", 0) != 5 && varTmp.lastIndexOf("-", 0) != 5) {
						invalidBillZip.alpha = 1;
						if (chkAddress.selected) {
							invalidShippZip.alpha = 1;
						}
						isValid = false;
					}
				}
			}
			return isValid;
		}
		private function validateRequiredFields():Boolean {
			var isValid:Boolean = true;
			if (Myxml.getInstance().Trim(oShippingFName.text).length>0) {
				sfnameReq.alpha = 0;
			} else {
				isValid = false;
				sfnameReq.alpha = 1;
			}
			if (Myxml.getInstance().Trim(oShippingLName.text).length>0) {
				slnameReq.alpha = 0;
			} else {
				slnameReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oShippingAddr1.text).length>0) {
				saddr1Req.alpha = 0;
			} else {
				saddr1Req.alpha = 1;
				isValid = false;
			}
			if (oShippingState.selectedIndex >0) {
				sstateReq.alpha = 0;
			} else {
				sstateReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oShippingCity.text).length>0) {
				scityReq.alpha = 0;
			} else {
				scityReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oShippingZip.text).length>0) {
				szipReq.alpha = 0;
			} else {
				invalidShippZip.alpha = 0;
				szipReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oShippingEmail.text).length>0) {
				semailReq.alpha = 0;

			} else {
				invalidShippEmail.alpha = 0;
				semailReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingFName.text).length>0) {
				bfnameReq.alpha = 0;
			} else {
				bfnameReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingLName.text).length>0) {
				blnameReq.alpha = 0;
			} else {
				blnameReq.alpha = 1;
				isValid = false;
			}
			
			if (Myxml.getInstance().Trim(oBillingAddr1.text).length>0) {
				baddr1Req.alpha = 0;
			} else {
				baddr1Req.alpha = 1;
			}
			if (oBillingState.selectedIndex >0) {
				bstateReq.alpha = 0;
			} else {
				bstateReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingCity.text).length>0) {
				bcityReq.alpha = 0;
			} else {
				bcityReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingZip.text).length>0) {
				bzipReq.alpha = 0;
			} else {
				invalidBillZip.alpha = 0;
				bzipReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingEmail.text).length>0) {
				bemailReq.alpha = 0;

			} else {
				invalidBillEmail.alpha = 0;
				bemailReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(billingPhoneNumber.text).length>0) {
				billingPhoneReq.alpha = 0;
			} else {
				billingPhoneReq.alpha = 1;
				invalidBillingPh.alpha = 0;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(shippingPhoneNumber.text).length>0) {
				shippingPhoneReq.alpha = 0;
			} else {
				shippingPhoneReq.alpha = 1;
				invalidShippingPh.alpha = 0;
				isValid = false;
			}
			return isValid;
		}
		private function validateBillingRequiredFields():Boolean {
			var isValid:Boolean = true;
			if (Myxml.getInstance().Trim(oBillingFName.text).length>0) {
				bfnameReq.alpha = 0;
			} else {
				bfnameReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingLName.text).length>0) {
				blnameReq.alpha = 0;
			} else {
				blnameReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingAddr1.text).length>0) {
				baddr1Req.alpha = 0;
			} else {
				baddr1Req.alpha = 1;
			}
			if (Myxml.getInstance().Trim(oBillingState.value).length>0) {
				bstateReq.alpha = 0;
			} else {
				bstateReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingCity.text).length>0) {
				bcityReq.alpha = 0;
			} else {
				bcityReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingZip.text).length>0) {
				bzipReq.alpha = 0;
			} else {
				invalidBillZip.alpha = 0;
				bzipReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(oBillingEmail.text).length>0) {
				bemailReq.alpha = 0;

			} else {
				bemailReq.alpha = 1;
				isValid = false;
			}
			if (Myxml.getInstance().Trim(billingPhoneNumber.text).length>0) {
				billingPhoneReq.alpha = 0;
			} else {
				billingPhoneReq.alpha = 1;
				invalidBillingPh.alpha = 0;
				isValid = false;
			}
			return isValid;
		}
		private function setDefalutValue():void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') { 
				if (Myxml.getInstance().getCustomerInfoXML()) {
					oBillingFName.text = Myxml.getInstance().getCustomerInfo('BillingFName');
					oBillingLName.text = Myxml.getInstance().getCustomerInfo('BillingLName');
					oBillingAddr1.text = Myxml.getInstance().getCustomerInfo('BillingAddr1');
					oBillingAddr2.text = Myxml.getInstance().getCustomerInfo('BillingAddr2');
					oBillingCity.text = Myxml.getInstance().getCustomerInfo('BillingCity');
					oBillingState.text = Myxml.getInstance().getCustomerInfo('BillingState');
					oBillingZip.text = Myxml.getInstance().getCustomerInfo('BillingZip');
					oBillingEmail.text = Myxml.getInstance().getCustomerInfo('BillingEmail'); 
					billingPhoneNumber.text = Myxml.getInstance().getCustomerInfo('BillingPhoneNumber');
					oShippingFName.text = Myxml.getInstance().getCustomerInfo('ShippingFName');
					oShippingLName.text = Myxml.getInstance().getCustomerInfo('ShippingLName');
					oShippingAddr1.text = Myxml.getInstance().getCustomerInfo('ShippingAddr1');
					oShippingAddr2.text = Myxml.getInstance().getCustomerInfo('ShippingAddr2');
					oShippingCity.text = Myxml.getInstance().getCustomerInfo('ShippingCity');
					oShippingState.text = Myxml.getInstance().getCustomerInfo('ShippingState');
					oShippingZip.text = Myxml.getInstance().getCustomerInfo('ShippingZip');
					oShippingEmail.text = Myxml.getInstance().getCustomerInfo('ShippingEmail');
					shippingPhoneNumber.text = Myxml.getInstance().getCustomerInfo('ShippingPhoneNumber');
					oShippingState.selectedIndex = int(Myxml.getInstance().getCustomerInfo('ShippingStateIndex'));
					oBillingState.selectedIndex = int(Myxml.getInstance().getCustomerInfo('BillingStateIndex'));
				}
			}
		}
		//Selecting the checkBox
		private function hdlrCheckBox(evt:Event):void {
			comboBoxChangeEvent();
		}
		//
		private function comboBoxChangeEvent():void {
			if (chkAddress.selected) {
				validateBillingRequiredFields();
				if(validateBillingRequiredFields()){
					oShippingFName.text=oBillingFName.text;
					oShippingLName.text=oBillingLName.text;
					oShippingAddr1.text=oBillingAddr1.text;
					oShippingAddr2.text=oBillingAddr2.text;
					oShippingState.selectedIndex=oBillingState.selectedIndex;
					oShippingCity.text=oBillingCity.text;
					oShippingZip.text=oBillingZip.text;
					oShippingEmail.text=oBillingEmail.text;
					shippingPhoneNumber.text=billingPhoneNumber.text;
					oShippingFName.editable=false;
					oShippingLName.editable=false;
					oShippingAddr1.editable=false;
					oShippingAddr2.editable=false;
					//oShippingState.enabled = false;
					mcStateDisable.visible = true;
					oShippingCity.editable=false;
					oShippingZip.editable=false;
					oShippingEmail.editable=false;
					shippingPhoneNumber.editable=false;
				}else{
	   				chkAddress.selected=false;
				}
			 }
			else {
				oShippingFName.text="";
				oShippingLName.text="";
				oShippingAddr1.text="";
				oShippingAddr2.text="";
				oShippingState.selectedIndex=0;
				oShippingCity.text="";
				oShippingZip.text="";
				oShippingEmail.text="";
				oShippingFName.editable=true;
				oShippingLName.editable=true;
				oShippingAddr1.editable=true;
				oShippingAddr2.editable=true;
				//oShippingState.enabled = true;
				mcStateDisable.visible = false;
				oShippingCity.editable=true;
				oShippingZip.editable=true;
				oShippingEmail.editable=true;
				shippingPhoneNumber.editable=true;
				shippingPhoneNumber.text="";
			}
		}
		//Billing State Combobox open and Close Handler
		private function hdlrBillStateOpen(evt:Event):void {
			isBillState = true;
		}
		private function hdlrBillStateClose(evt:Event):void {
			isBillState = false;
		}
		//Shipping State Combobox open and Close Handler
		private function hdlrShipStateOpen(evt:Event):void {
			isShipState = true;
		}
		private function hdlrShipStateClose(evt:Event):void {
			isShipState = false;
		}
	}
}