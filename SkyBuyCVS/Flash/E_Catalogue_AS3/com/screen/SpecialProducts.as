﻿package com.screen 
{
	import fl.controls.ComboBox;
	import fl.data.DataProvider;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import fl.transitions.TweenEvent;
	import flash.events.MouseEvent;
	import flash.display.SimpleButton;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.net.URLRequest;
	
	import com.mylocale.Myxml;
	import com.document.Main;
	import com.document.PurchaseDetails;
	import com.utils.CustomTrans;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class SpecialProducts extends MovieClip 
	{
		private var purDetailIns:PurchaseDetails;
		private var LoadingResource:Loading;
		private var specialProductArr:Array;
		private var currentItemNo:int;
		private var currentItem:Object;
		private var swfLoader:Loader ;
		private var gloxx:Number = 146.3;
		private var gloyy:Number = 107.1;
		private var isFirstTime:Boolean = true;
		
		//
		private var cbSize:ComboBox;
		private var cbColor:ComboBox;
		
		public function SpecialProducts() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			specialProductArr = new Array();
			initialize();
		}
		//
		public function init(aIns:PurchaseDetails):void {
			purDetailIns = aIns;
		}
		//
		private function initialize():void {
			for each(var aa in extractObjWithId()) {
				avoideDuplicateObj(aa);
			}
			if (specialProductArr.length > 1) {
				mcCont.prev.visible = true;
				mcCont.next.visible = true;
			}else {
				mcCont.prev.visible = false;
				mcCont.next.visible = false;
			}
			if (specialProductArr.length > 0) {
				currentItem = specialProductArr[currentItemNo];
				mcCont.mcColor.visible = false;
				mcCont.mcSize.visible = false;
			
				mcCont.next.buttonMode = true;
				mcCont.prev.buttonMode = true;
				//
				mcCont.next.addEventListener(MouseEvent.MOUSE_DOWN, showNextProduct);
				mcCont.checkout.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCheckout);
				mcCont.prev.addEventListener(MouseEvent.MOUSE_DOWN, showPrevProduct);
				mcCont.addToMyCollection.addEventListener(MouseEvent.MOUSE_DOWN, buyProduct);
				//
				mcCont.mcQuantity.inQuantity.maxChars = "2";
				mcCont.mcQuantity.inQuantity.restrict = "0-9";
				//
				LoadingResource = new Loading();
				LoadingResource.x = 354.4+gloxx;
				LoadingResource.y = 46.8+gloyy;
				LoadingResource.visible = false;
				addChild(LoadingResource);
				initCompoPos();
				showPopup();
				//showProduct(currentItemNo);
			}
			
		}
		//
		private function avoideDuplicateObj(aPro:Object):Boolean {
			for each (var aa in Main.thisClass.shoppingScreen.productDet.purchaseDetail.productDet) {
				if (aa.reffObj.OwnerId == aPro.OwnerId && aa.reffObj.ProdId == aPro.ProdId) {
					return false;
				}
			}
			specialProductArr.push(aPro);
			return true;
		}
		//
		private function initCompoPos():void {
			removeComboBox();
			if (currentItem.ProdColor != null) {
				var arColor:Array = currentItem.ProdColor.split(',');
				if (arColor.length >0) {
					mcCont.mcColor.visible = true;
					mcCont.mcSize.y = 166.7 + gloyy;
					
				} else {
					mcCont.mcColor.visible = false;
					mcCont.mcSize.y = 212.4 + gloyy;
					
				}
			}else {
				mcCont.mcColor.visible = false;
			}
			if (currentItem.ProdSize != null) {
				var arSize:Array = currentItem.ProdSize.split(',');
				if (arSize.length>0) {
					mcCont.mcSize.visible = true;
				} else {
					mcCont.mcSize.visible = false;
				}
			}else {
				mcCont.mcSize.visible = false;
			}
			if (mcCont.mcSize.visible == false && mcCont.mcColor.visible == false) {
				mcCont.addToMyCollection.x = 363.4 + gloxx;
			} else {
				mcCont.addToMyCollection.x = 632.6 + gloxx;
			}
			mcCont.mcSkybuyPrice.dtSkyBuyPrice.text = Myxml.getInstance().formatAsDollars(currentItem.SkybuyPrice);
			mcCont.mcQuantity.inQuantity.text = "1";
			if (mcCont.mcColor.visible) {
				if (!isFirstTime) {
					cbColor = new ComboBox();
					stage.addChild(cbColor);
					cbColor.x = mcCont.mcColor.x + 52
					cbColor.y = mcCont.mcColor.y;
					var dpCol:DataProvider = new DataProvider();
					
					var maxColorWidth:Number = arColor[0].length;
					for (var i:int = 0; i<arColor.length; i++) {
						if (arColor[i].length>maxColorWidth) {
							maxColorWidth = arColor[i].length;
						}
						dpCol.addItem({label:Myxml.getInstance().Trim(arColor[i])});
					}
					maxColorWidth = maxColorWidth*8+20;
					if (maxColorWidth<143) {
						maxColorWidth = 143.2;
					}
					if (maxColorWidth>190) {
						maxColorWidth = 190;
					}
					cbColor.dataProvider = dpCol;
					cbColor.setSize(maxColorWidth,22);
				}
			}
			if (mcCont.mcSize.visible) {
				if (!isFirstTime) {
					cbSize = new ComboBox();
					stage.addChild(cbSize);
					cbSize.x = mcCont.mcSize.x + 52
					cbSize.y = mcCont.mcSize.y;
					var dpSize:DataProvider = new DataProvider();
					
					var maxSizeWidth:Number = arSize[0].length;
					for (var j:int = 0; j<arSize.length; j++) {
						if (arSize[j].length>maxSizeWidth) {
							maxSizeWidth = arSize[j].length;
						}
						dpSize.addItem({label:Myxml.getInstance().Trim(arSize[j])});
					}
					maxSizeWidth = maxSizeWidth*12+20;
					if (maxSizeWidth<143) {
						maxSizeWidth = 143.2;
					}
					if (maxSizeWidth>190) {
						maxSizeWidth = 190;
					}
					cbSize.dataProvider = dpSize;
					cbSize.setSize(maxSizeWidth, 22);
				}
			}
			var tmpProdPrice:Number = Number(currentItem.ProdPrice.split('$')[1]);
			var tmpSkyBuyPrice:Number = Number(currentItem.SkybuyPrice.split('$')[1]);
			if (tmpProdPrice<=tmpSkyBuyPrice) {
				mcCont.mcInStorePrice.visible = false;
				if (mcCont.mcSize.visible == false && mcCont.mcColor.visible == false) {
					mcCont.mcSkybuyPrice.x = 284.1 + gloxx;
					mcCont.mcSkybuyPrice.y = 166.5 + gloyy;
				} else {
					mcCont.mcSkybuyPrice.x = 7.4 + gloxx;
					mcCont.mcSkybuyPrice.y = 213.1 + gloyy;
				}
			} else {
				mcCont.mcSkybuyPrice.x = 7.4 + gloxx;
				mcCont.mcSkybuyPrice.y = 213.1 + gloyy;
				//mcSkybuyPrice._x = 290.6;
				mcCont.mcInStorePrice.visible = true;
				mcCont.mcInStorePrice.txtInstorePrice.text = Myxml.getInstance().formatAsDollars(currentItem.ProdPrice);
			}
		}
		//
		private function showProduct(aId:int):void {
			currentItem = specialProductArr[aId];
			initCompoPos();
			try {
				if (swfLoader) {
					mcCont.productDetails.removeChild(swfLoader);
				}
				swfLoader = new Loader();
				swfLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrSwfLoaded);
				swfLoader.load(new URLRequest(currentItem.SwfPath));	
				
			}catch (err:Error) {
				trace('Erro in Check out swf Loading------', err);
			}
			
			mcCont.productDetails.addChild(swfLoader);
			LoadingResource.visible = true;
		}
		//
		private function removeComboBox():void {
			if (cbSize) {
				if (stage.contains(cbSize)) {
					cbSize.close();
					stage.removeChild(cbSize);
				}
			}
			if (cbColor) {
				if (stage.contains(cbColor)) {
					cbColor.close();
					stage.removeChild(cbColor);
				}
			}
		}
		//Extract totoal objects
		private function extractObjWithId():Array {
			var seqArr:Array = Myxml.getInstance().getCatalogueObjxml;
			var tmpArr:Array = new Array();
			for each(var obj in seqArr) {
				if (obj.SeqId == 6) {
					tmpArr.push(obj);
				}
			}
			return tmpArr;
		}
		
		//Swf Load Complete
		private function hdlrSwfLoaded(evt:Event):void {
			evt.currentTarget.addEventListener(Event.COMPLETE, hdlrSwfLoaded);
			LoadingResource.visible = false;
		}
		//Show Popup
		private function showPopup():void {
			/*var tween:Tween = new Tween(mcCont, 'y', Strong.easeOut, -301, 0, 0.5, true);
			tween.addEventListener(TweenEvent.MOTION_FINISH, hdlrStartComplete);
			tween.start();*/
			var tween:CustomTrans = new CustomTrans(mcCont, 'y', -301, 0, 1);
			tween.frameRate = stage.frameRate;
			tween.addEventListener(CustomTrans.TWEEN_COMPLETE, hdlrStartComplete);
			tween.startTween();
		}
		//
		private function hdlrStartComplete(evt:Event):void {
			evt.currentTarget.removeEventListener(CustomTrans.TWEEN_COMPLETE, hdlrStartComplete);
			//evt.currentTarget.removeEventListener(TweenEvent.MOTION_FINISH, hdlrStartComplete);
			if (isFirstTime) {
				if (specialProductArr.length > 0) {
					isFirstTime = false;
					showProduct(currentItemNo);
				}
			}
		}
		//Hide popup;
		private function hidePopup():void {
			removeComboBox();
			/*var tween:Tween = new Tween(mcCont, 'y', Strong.easeOut, 0, -301, 0.5, true);
			tween.addEventListener(TweenEvent.MOTION_FINISH, hdlrTweenComplete);
			tween.start();*/
			var tween:CustomTrans = new CustomTrans(mcCont, 'y', 0, -301, 1);
			tween.frameRate = stage.frameRate;
			tween.addEventListener(CustomTrans.TWEEN_COMPLETE, hdlrTweenComplete);
			tween.startTween();
			
		}
		//Tween Complete
		private function hdlrTweenComplete(evt:Event):void {
			evt.currentTarget.removeEventListener(CustomTrans.TWEEN_COMPLETE, hdlrTweenComplete);
			dispatchEvent(new Event('checkout'));
			killMe();
		}
		//Show next Product
		private function showNextProduct(evt:MouseEvent):void {
			if (currentItemNo < specialProductArr.length - 1) {
				currentItemNo++;
				showProduct(currentItemNo);
			}
			
		}
		//Show Previous Product
		private function showPrevProduct(evt:MouseEvent):void {
			if (currentItemNo  > 0 ) {
				currentItemNo--;
				showProduct(currentItemNo);
			}
		}
		//Check out mouse down handler
		private function hdlrCheckout(evt:MouseEvent):void {
			hidePopup();
		}
		//Buy the product
		private function buyProduct(evt:MouseEvent):void {
			var giftBag:GiftBag = new GiftBag();
			giftBag.x = 501.0;
			giftBag.y = 141.0;
			stage.addChild(giftBag)
			var aColor:int = mcCont.mcColor.visible ? cbColor.selectedIndex: 0;
			var aSize:int =mcCont. mcSize.visible ? cbSize.selectedIndex: 0;
			purDetailIns.addProduct(currentItem, int(mcCont.mcQuantity.inQuantity.text), aColor, aSize);
		}
		//Remove movieclip
		private function killMe():void {
			parent.removeChild(this);
			delete(this);
		}
	}

}