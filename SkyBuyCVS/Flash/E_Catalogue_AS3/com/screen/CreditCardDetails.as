﻿package com.screen 
{
	import com.document.Main;
	import com.mylocale.Myxml;
	import com.utils.CreditcardValidation;
	import fl.managers.FocusManager;
	import fl.transitions.Tween;
	import fl.transitions.easing.*
	import flash.text.TextField;
	
	import fl.controls.ComboBox;
	import fl.data.DataProvider;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class CreditCardDetails extends MovieClip
	{
		public var shippingDet:ShippingAddress;
		public var ccDetailsXML:String;
		//
		private var tf:TextFormat;
		//Credid card Details
		private var cardDetail:DataProvider = new DataProvider(); 
		private var monthDetail:DataProvider = new DataProvider();
		//
		private var isYearOpen:Boolean;
		private var isMonthOpen:Boolean;
		private var isCardOpen:Boolean;
		//
		public function CreditCardDetails() 
		{
			focusRect = false;
			addEventListener(Event.ADDED_TO_STAGE, objAddedToStage);
			//
			
		}
		//Init the components
		public function initComponents():void {
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'DEMO') {
				Name.text = "";
				CardType.selectedIndex = 0;
				CardNumber.text = "";
				cvv.text  = "";
				expMonth.selectedIndex = 0;
				expYear.selectedIndex = 0;
			}
		}
		//
		private function objAddedToStage(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, objAddedToStage);
			//
			cardDetail.addItem( { label:'-------- Select Card Type  --------', data:'' } );
			cardDetail.addItem({ label:'American Express', data:'AC', cardType:"AMEX"});
			cardDetail.addItem({ label:'Master Card', data:'MC', cardType:"MASTERCARD" });
			cardDetail.addItem( { label:'Visa Card', data:'VC', cardType:"VISA"});
			//
			monthDetail.addItem({ label:'Select', data:'0' });
			monthDetail.addItem({ label:'Jan', data:'01' });
			monthDetail.addItem({ label:'Feb', data:'02' });
			monthDetail.addItem({ label:'Mar', data:'03' });
			monthDetail.addItem({ label:'Apr', data:'04' });
			monthDetail.addItem({ label:'May', data:'05' });
			monthDetail.addItem({ label:'Jun', data:'06' });
			monthDetail.addItem({ label:'Jul', data:'07' });
			monthDetail.addItem({ label:'Aug', data:'08' });
			monthDetail.addItem({ label:'Sep', data:'09' });
			monthDetail.addItem({ label:'Oct', data:'10' });
			monthDetail.addItem({ label:'Nov', data:'11' });
			monthDetail.addItem( { label:'Dec', data:'12' } );
			//
			Name.tabIndex  = 0;
			CardType.tabIndex  = 1;
			CardNumber.tabIndex  = 2;
			cvv.tabIndex  = 3;
			expMonth.tabIndex  = 4;
			expYear.tabIndex  = 5;
			//
			CardNumber.restrict = '0-9'
			CardNumber.maxChars = 16;
			//
			cvv.restrict = '0-9'
			cvv.maxChars = 4;
			//
			cvvinfo.visible = false;
			tf = new TextFormat();
			tf.size = 14;
			tf.font = 'Arial';
			//
			btnCancel.focusRect = false;
			btnBack.focusRect = false;
			btnHome.focusRect = false;
			btnCvv.focusRect = false;
			cvvinfo.focusRect = false;
			btnSubmit.focusRect = false;
			//
			btnCancel.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCancel);
			btnBack.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackToShoppingCart);
			btnHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackHome);
			btnCvv.addEventListener(MouseEvent.MOUSE_DOWN, showCVVPopup);
			cvvinfo.btnClose.addEventListener(MouseEvent.MOUSE_DOWN, closePopup);
			btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSubmit);
			//
			hideWarnings(true);
		}
		//MouseDown handler submit button clicks
		private function hdlrSubmit(evt:MouseEvent):void {
			closeComboBox();
			hideRedWarnings(false);
			
			validateRequiredFields();
			validateInvaliedFields()
			if (validateRequiredFields() && validateInvaliedFields() ) {
				//if (isValid) {
					invalidCN.visible= false;
					invalidExp.visible = false;
					ccDetailsXML = "<CCDetails>";
					ccDetailsXML = ccDetailsXML+"<Name><![CDATA["+Name.text+"]]></Name>";
					ccDetailsXML = ccDetailsXML+"<CardType>"+CardType.value+"</CardType>";
					ccDetailsXML = ccDetailsXML+"<CardNumber>"+CardNumber.text+"</CardNumber>";
					ccDetailsXML = ccDetailsXML+"<CVV>"+cvv.text+"</CVV>";
					ccDetailsXML = ccDetailsXML+"<ExpDate>"+expMonth.value+"/"+expYear.value+"</ExpDate>";
					ccDetailsXML = ccDetailsXML+"<BankCode></BankCode>";
					ccDetailsXML = ccDetailsXML + "</CCDetails>";
					shippingDet = new ShippingAddress();
					addChild(shippingDet);
				}
			//}
		}
		//MosueDown handler when the home button clicks
		private function hdlrBackHome(evt:MouseEvent):void {
			closeComboBox();
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//MouseDown handler when the Cancel button clicks
		private function hdlrCancel(evt:MouseEvent):void {
			closeComboBox();
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.shoppingScreen.productDet.initText();
			Main.thisClass.shoppingScreen.no_of_items.text = '( 0 item )';
			Main.thisClass.shoppingScreen.productDet.purchaseDetail.purchaseDet = new Array();
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//Back to show shopping cart screen
		private function hdlrBackToShoppingCart(evt:MouseEvent):void {
			closeComboBox();
			killMe();
		}
		//Hiding the warnings and assign the texts to the text boxes
		private function hideWarnings(aBool:Boolean):void {
			if (aBool) {
				hideRedWarnings(!aBool)
				//
				var crYear:Date = new Date();
				var fromYear:int = crYear.getFullYear();
				expYear.addItem({label:'Select'});
				for (var i = fromYear; i <= fromYear+10; i++) {
					expYear.addItem({label:String(i)});
				}
				expYear.textField.setStyle("textFormat", tf);
				expYear.addEventListener(Event.OPEN, expYearOpen);
				expYear.addEventListener(Event.CLOSE, expYearClose);
				//
				Name.tabIndex = 0;
				var fm:FocusManager = new FocusManager(this);
				fm.setFocus(Name);
				Name.setStyle("textFormat", tf);
				//
				CardType.dataProvider = cardDetail;
				CardType.addEventListener(Event.OPEN, cardTypeOpen);
				CardType.addEventListener(Event.CLOSE, cardTypeClose);
				CardType.textField.setStyle("textFormat", tf);
				//
				CardNumber.setStyle("textFormat", tf);
				//
				cvv.setStyle("textFormat", tf);
				//
				expMonth.dataProvider = monthDetail;
				expMonth.textField.setStyle("textFormat", tf);
				//
				expMonth.addEventListener(Event.OPEN, expMonthOpen);
				expMonth.addEventListener(Event.CLOSE, expMonthClose);
				//
				if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
					Name.text = Myxml.getInstance().getCustomerInfo('CardHolderName');
					CardType.selectedIndex = 1;
					CardNumber.text = Myxml.getInstance().getCustomerInfo('CardNumber');
					cvv.text = Myxml.getInstance().getCustomerInfo('CVV');
					expMonth.selectedIndex = int(Myxml.getInstance().getCustomerInfo('ExpMonthIndex'));
					expYear.selectedIndex = int(Myxml.getInstance().getCustomerInfo('ExpYearIndex'));
				}
			}else {
				invalidCN.visible = aBool;
				cvvInvalid.visible = aBool;
				invalidExp.visible = aBool;
			}
		}
		private function closeComboBox():void {
			try {
				if (isCardOpen) { CardType.close(); removeChild(CardType); isCardOpen = false; }
				if (isMonthOpen) { expMonth.close(); removeChild(expMonth);isMonthOpen = false; }
				if (isYearOpen) { expYear.close(); removeChild(expYear);isYearOpen = false; }
			}catch (err:Error) {
				trace('Error in Closing the Combobox in  Creditcard Details Screen', err);
			}
		}
		//
		private function hideRedWarnings(aBool:Boolean):void {
			ccNameReq.visible = aBool;
			ccTypeReq.visible = aBool;
			ccNumberReq.visible = aBool;
			cvvReq.visible = aBool;
			ccExpReq.visible = aBool;
			//
			invalidCN.visible = aBool;
			cvvInvalid.visible = aBool;
			invalidExp.visible = aBool;
		}
		//
		function validateRequiredFields():Boolean {
			var isValid:Boolean = true;
			if (Myxml.getInstance().Trim(Name.text).length>0) {
				ccNameReq.visible = false;
			} else {
				isValid = false;
				ccNameReq.visible = true;
			}
			if (Myxml.getInstance().Trim(CardType.value).length>0) {
				ccTypeReq.visible = false;
			} else {
				isValid = false;
				ccTypeReq.visible = true;
			}
			if (Myxml.getInstance().Trim(CardNumber.text).length>0) {
				ccNumberReq.visible = false;
			} else {
				isValid = false;
				ccNumberReq.visible = true;
			}
			if (Myxml.getInstance().Trim(cvv.text).length>0) {
				cvvReq.visible = false;
			} else {
				isValid = false;
				cvvReq.visible = true;
			}
			if (expMonth.selectedIndex != 0 && expYear.selectedIndex != 0) {
				ccExpReq.visible = false;
			} else {
				isValid = false;
				ccExpReq.visible = true;
			}
			return isValid;
		}
		//
		private function validateInvaliedFields():Boolean {
			var isValid:Boolean = true;
			var ccValidator:CreditcardValidation = new CreditcardValidation();
			var isValidCCNumber:Boolean;
			var now:Date = new Date();
			var nowMonth:Number = now.getMonth()+1;
			var nowYear:Number = now.getFullYear();
			var month:Number;
			var year:Number;
			isValidCCNumber = ccValidator.isValidCreditCardNumber(CardType.selectedItem.cardType, CardNumber.text);
			year = parseInt(expYear.value);
			month = parseInt(expMonth.value);
			if (year > 0 && month > 0) {
				ccExpReq.visible = false;
				if ((nowYear>year) || ((nowYear == year) && (nowMonth>month)) || isCardValidityExpired(month, year) == false) {
					invalidExp.visible = true;
					isValid = false;
				} else {
					invalidExp.visible = false;
				}
			}else {
				ccExpReq.visible = true;
			}
			
			if (!isValidCCNumber) {
				invalidCN.visible = true;
				isValid = false;
			} else {
				invalidCN.visible = false;
			}
			if (Myxml.getInstance().Trim(cvv.text).length > 0 && Myxml.getInstance().Trim(cvv.text).length<3) {
				cvvInvalid.visible = true;
				isValid = false;
			} else {
				cvvInvalid.visible = false;
			}
			return isValid;
		}
		private function isCardValidityExpired(month:Number, year:Number):Boolean {
			var today:Date = new Date();
			var todayYear = today.getFullYear();
			var todayMonth = today.getMonth()+1;
			if (year == todayYear) {
				if (month-todayMonth>1) {
					return true;
				} else if (month-todayMonth<0) {
					return false;
				} else {
					return false;
				}
			} else if (year-todayYear == 1) {
				month += 12;
				if (month-todayMonth>1) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		}
		//Show the CVV popup
		private function showCVVPopup(evt:MouseEvent):void {
			cvvinfo.visible = true;
		}
		//Close the instruction CVV popup
		private function closePopup(evt:MouseEvent):void {
			cvvinfo.visible = false;
		}
		//Card Combobox Open and Close Handler
		private function cardTypeOpen(evt:Event):void {
			isCardOpen = true;
			
		}
		private function cardTypeClose(evt:Event):void {
			isCardOpen = false;
		}
		//Month Combobox Open and Close Handler
		private function expMonthOpen(evt:Event):void {
			isMonthOpen= true;
		}
		private function expMonthClose(evt:Event):void {
			isMonthOpen = false;
		}
		//Year Combobox Open and Close Handler
		private function expYearOpen(evt:Event):void {
			isYearOpen = true;
		}
		private function expYearClose(evt:Event):void {
			isYearOpen = false;
		}
		//Kill me
		private function killMe():void {
			parent.removeChild(this);
		}
	}
}