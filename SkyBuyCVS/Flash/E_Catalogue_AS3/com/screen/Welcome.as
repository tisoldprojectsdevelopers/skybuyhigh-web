﻿package com.screen 
{
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.URLRequest;
	import flash.display.SimpleButton;
	import flash.external.ExternalInterface;
	//Custom Classes
	import com.document.Main;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Welcome extends MovieClip
	{
		private var loading:Loading;
		var ldr:Loader;
		var swfPath:String;
		public function Welcome( ) 
		{
			focusRect = false;
			addEventListener(Event.ADDED_TO_STAGE, hdlrScreenAdded);
		}
		public function setSwfPath(aPath:String):void {
			swfPath = aPath;
		}
		private function hdlrScreenAdded(event:Event):void
		{
			btnShop.focusRect = false;
			backToHome.focusRect = false;
			//
			removeEventListener(Event.ADDED_TO_STAGE, hdlrScreenAdded);
			btnShop.addEventListener(MouseEvent.MOUSE_DOWN, hdlrGotoShop);
			backToHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrGotoIntro);
			loadWelcomePage();
		}
		private function loadWelcomePage():void
		{	
			if (swfPath != null) {
				loading = new Loading();
				loading.x = stage.stageWidth / 2; 
				loading.y = stage.stageHeight / 2;
				this.addChild(loading);
				//
				ldr = new Loader();
				ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrLoadComplete);
				try {
					ExternalInterface.call('showAlert', swfPath);
					ldr.load(new URLRequest(swfPath));
				}catch (error:Error) {
					throw new Error('Error in Welcome page Loading')
					trace('Error Loading in welcome page')
				}
				ldr.alpha = 0;
				ldr.x = 40;
				ldr.y = 80;
				this.addChild(ldr);
			}
		}
		//Tween Completed
		private function hdlrLoadComplete(event:Event):void
		{
			if (loading) {
				this.removeChild(loading);
			}
			var tween:Tween = new Tween(ldr, 'alpha', Strong.easeOut, 0, 1, 0.3, true);
			tween.start();
		}
		/*CloseBtn Handler
		private function hdlrClose(evt:MouseEvent):void
		{
			dispatchEvent(new Event('Quit'))
		}*/
		//Goto shop page Handler
		private function hdlrGotoShop(event:MouseEvent):void
		{
			dispatchEvent(new Event('ShowShopping'))
			Main.thisClass.shoppingScreen.showCoverFlowScreen(true);
		}
		//Goto Intro page
		private function hdlrGotoIntro(event:MouseEvent):void
		{
		
			dispatchEvent(new Event('Intro'))
		}
	}

}