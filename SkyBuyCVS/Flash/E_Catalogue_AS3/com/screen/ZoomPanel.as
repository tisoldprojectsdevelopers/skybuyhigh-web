﻿package com.screen 
{
	import com.utils.ScrollMc;
	import flash.display.*
	import flash.events.*;
	import flash.text.*
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import com.mylocale.Myxml;
	import com.utils.ImageLoader;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ZoomPanel extends MovieClip
	{
		private var currentObj:Object;
		private var curImgOriginalUrl:String;
		private var zoomIn:Boolean = false;
		private var tfFormat:TextFormat
		private var descScroller1:ScrollMc
		public function ZoomPanel() 
		{
			addEventListener(Event.ADDED_TO_STAGE, panelAddedtoStage);
		}
		// Panel added to stage.
		private function panelAddedtoStage(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, panelAddedtoStage);
			//
			closewindow.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCloseWindow);
			//
			clktoseeorgimg.gotoAndStop(1);
			clktoseeorgimg.buttonMode = true;
			clktoseeorgimg.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSeeOrgImg);
			clktoseeorgimg.addEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
			clktoseeorgimg.addEventListener(MouseEvent.ROLL_OUT, hdlrRollOut);
			//
		}
		//Get current object from the product detail class
		public function Initialize(aObj:Object):void {
			currentObj = aObj;
			InitializeVendorbtn();
			showThumbImages();
			assignText(aObj);
			imageInit(true);
			//Load medView image and large view image
			curImgOriginalUrl = currentObj.MainOrgImage;0x000000
			LoadMedViewImage(currentObj.MainMedImage);
			LoadLargetImage(currentObj.MainLargeImage);
			//
			med_ImageView.mcClickZoom.visible = false;
			med_ImageView.buttonMode = false;
			//
			
			med_ImageView.mouseChildren = false;
		}
		//Initialize the vendor link button
		private function InitializeVendorbtn():void {
			var tempStr:String = Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'name');
			var tempUrl:String = Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'aboutMeURL');
			if (tempStr != 'null' && tempUrl.length > 0) {
				tfFormat = new TextFormat();
				aboutVendorLink.visible = true;
				aboutVendorLink.buttonMode = true;
				aboutVendorLink.mouseChildren = false;
				aboutVendorLink.vendorName.autoSize = TextFieldAutoSize.LEFT;
				aboutVendorLink.vendorName.htmlText = 'About '+Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'name');
				aboutVendorLink.targetURL = Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'aboutMeURL');
				//EventHandler
				aboutVendorLink.addEventListener(MouseEvent.MOUSE_DOWN, hdlrVendor);
				aboutVendorLink.addEventListener(MouseEvent.ROLL_OVER, hdlrVendorRollOver);
				aboutVendorLink.addEventListener(MouseEvent.ROLL_OUT, hdlrVendorRollOut);
			}else {
				aboutVendorLink.visible = false;
			}
		}
		//Show related Thumb buttons
		private function showThumbImages():void {
			var tmp:Array = new Array(
			['MainThumbImage', 'MainMedImage', 'MainLargeImage', 'MainOrgImage'], 
			['ViewOneThumbImage', 'ViewOneMedImage', 'ViewOneLargeImage', 'ViewOneOrgImage'], 
			['ViewTwoThumbImage', 'ViewTwoMedImage', 'ViewTwoLargeImage', 'ViewTwoOrgImage'], 
			['ViewThreeThumbImage', 'ViewThreeMedImage', 'ViewThreeLargeImage', 'ViewThreeOrgImage'] 
			)
			for (var i = 1; i <= 4; i++) {
				this['view_' + i].visible = currentObj[tmp[i - 1][0]] != null ? true:false;
				if (this['view_' + i].visible) {
					this['view_' + i].buttonMode = true;
					this['view_' + i].mosueChildren = true;
					this['view_' + i].loader.LoadImage(currentObj[tmp[i - 1][0]]);
					this['view_' + i].med = currentObj[tmp[i - 1][1]]
					this['view_' + i].large = currentObj[tmp[i - 1][2]]
					this['view_' + i].orignal = currentObj[tmp[i - 1][3]]
					this['view_' + i].addEventListener(MouseEvent.MOUSE_DOWN, hdlrThumbDown);
				}
			}
		}
		//Assigning the texts to the text Boxed
		private function assignText(aObj:Object):void {
			try {
				oBrand.htmlText = aObj.ProdBrand;
				oTitle.htmlText = aObj.ProdTitle;
				setProductDec(aObj.ProdLD);
				//oZoomDesc.htmlText = aObj.ProdLD;
			}catch (err:Error) {
				trace('Error in Text Assigning - Zoom Panel');
			}
		}
		//Assigning the Product Description
		private function setProductDec(aText:String):void {
			TextField(mcLongDec.oZoomDesc).wordWrap = true;
			TextField(mcLongDec.oZoomDesc).multiline = true;
			TextField(mcLongDec.oZoomDesc).autoSize = TextFieldAutoSize.LEFT;
			mcLongDec.oZoomDesc.htmlText = aText;
			descScroller1 = new ScrollMc(mcLongDec, 321);
			descScroller1.refreshScroll();
			addChild(descScroller1);
		}
		//vendor link Rollover
		private function hdlrVendorRollOver(evt:MouseEvent):void {
			tfFormat.color = 0x0000FF;
			aboutVendorLink.vendorName.htmlText = 'About '+Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'name');
			aboutVendorLink.vendorName.setTextFormat(tfFormat);
			//aboutVendorLink.vendorName.defaultTextFormat = tfFormat;
			
		}
		//vendor link Rollout
		private function hdlrVendorRollOut(evt:MouseEvent):void {
			tfFormat.color = 0x000000;
			aboutVendorLink.vendorName.htmlText = 'About '+Myxml.getInstance().getVendorDetails(currentObj.OwnerId, 'name');
			aboutVendorLink.vendorName.setTextFormat(tfFormat);
		}
		//Load vendor detail swf
		private function hdlrVendor(evt:MouseEvent):void {
			var vendorDetail:VendorDetail = new VendorDetail();
			vendorDetail.LoadVendorDetails(evt.target.targetURL);
			stage.addChild(vendorDetail);
		}
		//Original Image Link RollOver
		private function hdlrRollOver(evt:MouseEvent) {
			evt.currentTarget.gotoAndStop(2);
		}
		//Original Image Link RollOur
		private function hdlrRollOut(evt:MouseEvent):void {
			evt.currentTarget.gotoAndStop(1);
		}
		//Original Image Link down handler
		private function hdlrSeeOrgImg(evt:MouseEvent):void {
			var seeOriginal:OriginalmagePanel = new OriginalmagePanel();
			seeOriginal.LoadImage(curImgOriginalUrl);
			stage.addChild(seeOriginal);
		}
		//Load Med size Image
		private function LoadMedViewImage(aUrl:String):void {
			if (MovieClip(med_ImageView).hasEventListener(MouseEvent.MOUSE_DOWN)) {
				med_ImageView.removeEventListener(MouseEvent.MOUSE_DOWN, ZoomInOut);
			}
			this.med_ImageView.medImage.loader.LoadImage(aUrl); 
			
		}
		//Load Larger size Image
		private function LoadLargetImage(aUrl:String):void {
			if (MovieClip(med_ImageView).hasEventListener(MouseEvent.MOUSE_DOWN)) {
				med_ImageView.removeEventListener(MouseEvent.MOUSE_DOWN, ZoomInOut);
			}
			this.med_ImageView.largeImage.loader.LoadImage(aUrl); 
			this.med_ImageView.largeImage.loader.addEventListener('loadComplete', medImgLoaded);
		}
		//Load complete handler med Image
		private function medImgLoaded(evt:Event):void {
			med_ImageView.mcClickZoom.visible = true;
			med_ImageView.buttonMode = true;
			med_ImageView.addEventListener(MouseEvent.MOUSE_DOWN, ZoomInOut);
		}
		//Mouse down on thumbmail button
		private function hdlrThumbDown(evt:MouseEvent):void {
			imageInit(true);
			LoadMedViewImage(evt.currentTarget.med);
			LoadLargetImage(evt.currentTarget.large);
			curImgOriginalUrl = evt.currentTarget.orignal;
		}
		//Zooming the large image and zoom out the Large image
		private function ZoomInOut(evt:MouseEvent):void {
			var tween1:Tween; 
			var tween2:Tween; 
			zoomIn = !zoomIn;
			imageInit(!zoomIn);
			if (zoomIn) {
				setXYPos(evt.target.mouseX, evt.target.mouseY);
				tween1 = new Tween(this.med_ImageView.largeImage, 'scaleX', Strong.easeOut, 0.5, 1, 0.5, true);
				tween2 = new Tween(this.med_ImageView.largeImage, 'scaleY', Strong.easeOut, 0.5, 1, 0.5, true);
				tween1.start();
				tween2.start();
			}else if (!zoomIn) {
				tween1 = new Tween(this.med_ImageView.largeImage, 'scaleX', Strong.easeOut, 1, 0.5, 0.5, true);
				tween2 = new Tween(this.med_ImageView.largeImage, 'scaleY', Strong.easeOut, 1, 0.5, 0.5, true);
				tween1.start();
				tween2.start();
				med_ImageView.removeEventListener(MouseEvent.MOUSE_MOVE, ZoomOnMouseMove);
			}
		}
		//Mouse Move on Zoomed Object;
		private function ZoomOnMouseMove(evt:MouseEvent) {
			var largerImg :MovieClip = this.med_ImageView.largeImage;
			var container:MovieClip = this.med_ImageView;
			var containerWidth:Number = container.width;
			var containerHeight:Number = container.height;
			var largerImageWidth:Number = largerImg.width;
			var largerImageHeight:Number = largerImg.height;
			var ratioWidth:Number = largerImageWidth / containerWidth;
			var ratioHeight:Number = largerImageHeight / containerHeight;
			largerImg.x  = -evt.target.mouseX * ratioWidth;
			largerImg.y  = -evt.target.mouseY * ratioHeight;
		}
		//Init the Large and med images visibility
		private function imageInit(aBool = true):void {
			this.med_ImageView.largeImage.visible = !aBool;
			this.med_ImageView.medImage.visible = aBool;
			if (aBool) {
				this.med_ImageView.largeImage.scaleX = this.med_ImageView.largeImage.scaleY = 0.5;
			}
		}
		//
		private function setXYPos(ax:Number, ay:Number):void {
			var largerImg :MovieClip = this.med_ImageView.largeImage;
			var container:MovieClip = this.med_ImageView;
			var containerWidth:Number = container.width;
			var containerHeight:Number = container.height;
			var largerImageWidth:Number = largerImg.width;
			var largerImageHeight:Number = largerImg.height;
			var ratioWidth:Number = largerImageWidth / containerWidth;
			var ratioHeight:Number = largerImageHeight / containerHeight;
			var xx:Number = -ax * ratioWidth;
			var yy:Number = -ay * ratioWidth
			var tween1:Tween = new Tween(this.med_ImageView.largeImage, 'x', Strong.easeOut, 0, xx, 0.5, true);
			var tween2:Tween = new Tween(this.med_ImageView.largeImage, 'y', Strong.easeOut, 0, yy, 0.5	, true);
			tween1.start();
			tween2.start();
			med_ImageView.addEventListener(MouseEvent.MOUSE_MOVE, ZoomOnMouseMove);
		}
		//Remove the zoom panel from the stage 
		private function hdlrCloseWindow(evt:MouseEvent) {
			parent.removeChild(this);
		}
	}
}