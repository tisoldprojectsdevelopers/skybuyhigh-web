﻿package com.screen 
{
	import com.document.Main;
	import com.document.WebServiceCall;
	import com.mylocale.Myxml;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.Socket;
	import flash.system.fscommand;
	import flash.display.SimpleButton;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class OrderConfirm extends MovieClip
	{
		public var mailId :String
		
		public function OrderConfirm() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(MouseEvent.MOUSE_DOWN, hdlrObjAdded);
			//
			btnHome.focusRect = false;
			//btnSubmit.focusRect = false;
			btnDone.focusRect = false;
			//
			btnHome.addEventListener(MouseEvent.MOUSE_DOWN, hdlrBackHome);
			//btnSubmit.addEventListener(MouseEvent.MOUSE_DOWN, hdlrSendSuggestion);
			btnDone.addEventListener(MouseEvent.MOUSE_DOWN, hdlrDone);
			//
			mcOrderFailed.visible = false;
			success.visible = false;
			orderNumber.visible = false;
			//
			if (Main.thisClass.isItWeb== false){
				if (Main.thisClass.isValidOrder == "Failed") {
					mcOrderFailed.visible = true;
					mcOrderFailed.orderFailed.text = 'Your order failed. your credit card will not charged.please report the following error to SkyBuy Admin';
				}
				if (Main.thisClass.isValidOrder == 'Success') {
					success.visible = true;
					orderNumber.visible = true;
					success.spl_inst.htmlText = 'Please note the order confirmation number which will also be e-mailed to ' + mailId;
					orderNumber.text = Main.thisClass.vOrderNumber;
				}
			}else if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'WEB') {
				if (Main.thisClass.isValidOrder == 'OPS' || Main.thisClass.isValidOrder == 'OAP') {
					success.visible = true;
					orderNumber.visible = true;
					success.spl_inst.htmlText = 'Please note the order confirmation number which will also be e-mailed to ' + mailId;
					orderNumber.text = Main.thisClass.vOrderNumber;
				}else {
					mcOrderFailed.visible = true;
					var str:String;
					var tmpString:String = 'SBH_ERR_' + Main.thisClass.isValidOrder ;
					str = Myxml.getInstance().webServiceDet('Error_Code');
					str = str.replace(':;', tmpString);
					mcOrderFailed.orderFailed.text =  str;
				}
			}else if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'DEMO') {
				if (Main.thisClass.isValidOrder == "Failed") {
					mcOrderFailed.visible = true;
					mcOrderFailed.orderFailed.text = 'Your order failed. your credit card will not charged.please report the following error to SkyBuy Admin';
				}
				if (Main.thisClass.isValidOrder == 'Success') {
					success.visible = true;
					orderNumber.visible = true;
					success.spl_inst.htmlText = 'Please note the order confirmation number which will also be e-mailed to ' + mailId;
					orderNumber.text = Main.thisClass.vOrderNumber;
				}
			}
		}
		//MosueDown handler when the home button clicks
		private function hdlrBackHome(evt:MouseEvent):void {
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		/*Mouse Down Handler submit
		private function hdlrSendSuggestion(evt:MouseEvent):void {
			if (Myxml.getInstance().Trim(custFB1.text).length > 0 || Myxml.getInstance().Trim(custFB2.text).length > 0) {
				//if (thanks.currentFrame == 1) {
					thanks.gotoAndPlay(3);
				//}
				if (Main.thisClass.isItWeb == false) {
					fscommand("Feedback_1",custFB1.text);
					fscommand("Feedback_2",custFB2.text);
				}else if(Myxml.getInstance().getCatalogueType() == 'live') {
					var str:String = '<customerFeedback><CustTransId>' + Main.thisClass.vOrderNumber + '</CustTransId><NewServicesFeedback>' + Myxml.getInstance().Trim(custFB1.text) + '</NewServicesFeedback><Suggestion>'+ Myxml.getInstance().Trim(custFB1.text) + '</Suggestion></customerFeedback>';
					var webService:WebServiceCall = new WebServiceCall();
					webService.sendCustomSuggestion(str);
				}
				
			}
			
		}*/
		//MouseDown Handler Done
		private function hdlrDone(evt:MouseEvent):void {
			
			Main.thisClass.splInstruction = '';
			Main.thisClass.personalInformation = '';
			Main.thisClass.isValidOrder = '';
			Main.thisClass.vOrderNumber = '';
			Main.thisClass.shoppingScreen.productDet.purchaseDetail.purchaseDet = new Array();
			Main.thisClass.shoppingScreen.no_of_items.text = '( 0 item )';
			
			Main.thisClass.shoppingScreen.productDet.shoppingCart.hdlrContinueShop(evt);
			Main.thisClass.showIntroPage(true);
			killMe();
		}
		//Kill me
		private function killMe():void {
			parent.removeChild(this);
		}
	}

}