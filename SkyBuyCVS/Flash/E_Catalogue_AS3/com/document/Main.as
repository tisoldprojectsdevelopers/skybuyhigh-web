﻿package com.document
{
	import adobe.utils.CustomActions;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.fscommand;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.text.TextField;
	import flash.display.SimpleButton;
	import flash.external.ExternalInterface;
	
	//Custom Classes
	import com.mylocale.Myxml;
	import com.screen.*;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Main extends MovieClip
	{
		//Reference of this class
		public static var thisClass:Main;
		//Declearing the Cataloge Version
		public var version:String = "1.0.2.0";
		//Screens Instance
		public var introScreen:Intro;
		public var welcomeScreen:Welcome;
		public var shoppingScreen:Shopping;
		
		
		public var watermark:WaterMarkDemo;
		public var appMask:Sprite
		//
		public var splInstruction:String
		public var personalInformation:String;
		//
		public var isValidOrder:String;
		public var ordFailed:String;
		public var vOrderNumber:String;
		public var airLineOrderStatus:String;
		//
		var myXml:Myxml;
		public var fileName:String;
		public var coverImageXml:String;
		public var aboutVendorXML:String;
		public var customInfoXML:String;
		public var isItWeb:Boolean;
		public var welcomePath:String;
		public var personalShopper:String;
		//
		public var fromIntroPage:Boolean;
		public function Main() 
		{
			thisClass = this;
			initialize();
		}
		//Initializing the Methods
		private function initialize():void {
			focusRect = false;
			hideContextMenu();
		}
		public function whichTypeCatalogue() {
			showCloseBtn(false);
			if (!isItWeb ) {
				isItWeb = Boolean(this.loaderInfo.parameters.isItWeb);
			}
			isItWeb = true;
			if (isItWeb) {
				/*fileName = String(this.loaderInfo.parameters.fileName);
				coverImageXml = String(this.loaderInfo.parameters.coverImageXml);
				aboutVendorXML = String(this.loaderInfo.parameters.aboutVendorXML);
				customInfoXML = String(this.loaderInfo.parameters.customInfoXML);*/
				fileName = 'eCatalogue_test.xml';
				coverImageXml = 'eCatalogue_cover.xml';
				aboutVendorXML = 'eCatalogue_partner.xml';
				customInfoXML = 'custInfo.xml';
				welcomePath = 'welcomepage.swf'
				personalShopper = 'personal_shopper.swf'
				/*fileName = 'http://qa.skybuyhigh.com/static/eCatalogue_Admin.xml'
				coverImageXml = 'http://qa.skybuyhigh.com/static/eCatalogue_cover_Admin.xml'
				aboutVendorXML = 'http://qa.skybuyhigh.com/static/eCatalogue_partner_Admin.xml'*/
				loadXMLs();
			}else {
				/*fileName = 'eCatalogue.xml';
				coverImageXml = 'eCatalogue_cover.xml';
				aboutVendorXML = 'eCatalogue_partner.xml';
				customInfoXML = 'custInfo.xml';	*/
				loadXMLs();
			}
		}
		public function loadXMLs():void {
			myXml = Myxml.getInstance();
			myXml.getContent(fileName, coverImageXml, aboutVendorXML);
			myXml.addEventListener(Myxml.XML_LOADED, InitScreens);
			
		}
		//Hide the flash context menu 
		private function hideContextMenu():void{
			var cntMenu:ContextMenu = new ContextMenu();
			cntMenu.hideBuiltInItems();
			this.contextMenu = cntMenu;
			if (isItWeb == false) {
				fscommand("fullscreen", "true");
				fscommand("allowscale", "false");
			}
			
		}
		//
		private function showWatermark():void {
			var watermark:WaterMarkDemo = new WaterMarkDemo(); 
			watermark.mouseEnabled = false;
			watermark.name = 'watermark';
			stage.addChild(watermark);
			watermark.visible = Myxml.getInstance().getCatalogueType().toUpperCase() == "DEMO" ? true:false
		}
		//
		public function InitScreens(evt:Event):void {
			dummyLoading.visible = false;
			//Masking the Screens
			appMask = new Sprite();
			appMask.graphics.beginFill(0x804040, 1);
			appMask.graphics.drawRect(0, 0, 1024, 576);
			appMask.graphics.endFill();
			addChild(appMask);
			//Load the addtional xml for demo and webservice
			if (Myxml.getInstance().getCatalogueType().toUpperCase()=='DEMO') {
				myXml.loadCustomInfo(customInfoXML);
			} else if (Myxml.getInstance().getCatalogueType().toUpperCase()=='WEB') {
				myXml.WebserviceXML('webServiceData.xml');
			}
			//Intializing the Screens
			
			shoppingScreen = new Shopping();
			
			this.addChild(shoppingScreen);
			if (Myxml.getInstance().getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
				if (!isItWeb) {
					welcomePath = 'welcomepage.swf'
				}
				welcomeScreen = new Welcome();
				welcomeScreen.setSwfPath(welcomePath);
				welcomeScreen.addEventListener('ShowShopping', goShoppingPage1);
				welcomeScreen.addEventListener('Intro', goIntroPage);
				this.addChild(welcomeScreen);
				//
				shoppingScreen.addEventListener('Intro', goIntroPage);
				//
				createIntroScreen();
				introScreen.mask = appMask;
				welcomeScreen.mask = appMask;
				showIntroPage(true);
			}
			//
			if (Myxml.getInstance().getCatalogueType().toUpperCase() == 'PRODUCT_PREVIEW') {
				shoppingScreen.visible = true;
				var preObj:Object = extractPreviewCatalogueObj();
				shoppingScreen.setActivePos(preObj.SeqId);
				shoppingScreen.productDet.displayProduct(preObj);
			}
			//
			shoppingScreen.mask = appMask;
			if (!isItWeb) {
				showCloseBtn(true);
				btnClose.addEventListener(MouseEvent.MOUSE_DOWN, quitApplication);
				this.setChildIndex(btnClose, this.numChildren - 1);
			}
			showWatermark();
		}
		//Preview Catalogue 
		private function extractPreviewCatalogueObj():Object{
			var seqArr:Array = Myxml.getInstance().getCatalogueObjxml;
			var tmpObject:Object;
			//
			var tmpSeqId:String = Myxml.getInstance().categories_path('activeCategory');
			var tmpOwnerId:String = Myxml.getInstance().categories_path('activeOwnerId');
			var tmpProductId:String = Myxml.getInstance().categories_path('activeProductId');
			//
			for (var aa in seqArr) {
				if (seqArr[aa].SeqId == tmpSeqId && seqArr[aa].OwnerId == tmpOwnerId && seqArr[aa].ProdId == tmpProductId ) {
					tmpObject = seqArr[aa];
				}
			}
				return tmpObject;
		}
		//Create Intro Screen
		private function createIntroScreen():void {
			if (introScreen) {
				introScreen.killMe();
			}
			introScreen = new Intro();
			introScreen.addEventListener('imagedown', goShoppingPage);
			introScreen.addEventListener('ClicktoGo', gotWelcomePage);
			introScreen.addEventListener('Quit', quitApplication);
			this.addChild(introScreen);
		}
		public function showIntroPage(aBool:Boolean):void {
			createIntroScreen();
			shoppingScreen.visible = !aBool;
			welcomeScreen.visible = !aBool;
		}
		//
		private function goIntroPage(evt:Event) :void {
			showIntroPage(true);
		}
		//
		private function goShoppingPage1(evt:Event):void {
			fromIntroPage = true;
			showShopingPage(true);
			shoppingScreen.hidePersonalShoper();
			shoppingScreen.coverFlow.initialize();
			shoppingScreen.setActivePos(1);
		}
		//
		private function goShoppingPage(evt:Event):void {
			fromIntroPage = true;
			showShopingPage(true);
			shoppingScreen.showProductDetailScreen(true);
			//
			var obj:Object = new Object();
			obj.CategoryId = evt.target.currCoverObj.SeqId 
			obj.OwnerId = null;
			//
			shoppingScreen.setActivePos(int(evt.target.currCoverObj.SeqId));
			shoppingScreen.coverFlow.moveProducts(obj);
			shoppingScreen.productDet.displayProduct(evt.target.currCoverObj as Object);
		}
		//
		public function showShopingPage(aBool:Boolean):void{
			introScreen.visible = !aBool;
			shoppingScreen.visible = aBool;
			welcomeScreen.visible = !aBool;
		}
		//
		private function gotWelcomePage(evt:MouseEvent):void {
			showWelcomePage(true);
		}
		//
		public function showWelcomePage(aBool:Boolean):void {
			introScreen.visible = !aBool;
			shoppingScreen.visible = !aBool;
			welcomeScreen.visible = aBool;
		}
		//
		public function quitApplication(evt:MouseEvent):void {
			fscommand('quit');
		}
		//
		public function showOrderConformation():void {
			shoppingScreen.productDet.shoppingCart.creditCard.shippingDet.tailNo.shoOrederConformationScreen();
		}
		//
		public function showAirlinePackagePopup():void {
			shoppingScreen.priJet.deviceMessage(airLineOrderStatus);
		}
		//
		public function showCloseBtn(aBool:Boolean):void {
			btnClose.visible = aBool;
			if (isItWeb) {
				btnClose.visible = false;
			}
		}
		//
		public function showMsg(aMsg:String):void {
			var alt:Message = new Message();
			alt.Show(String(aMsg));
			stage.addChild(alt);
		}
	}

}