﻿package com.document 
{
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	import com.screen.CreditCardDetails;
	import com.screen.ShippingAddress;
	import com.document.WebServiceCall;
	import fl.accessibility.DataGridAccImpl;
	import flash.events.*;
	import com.crypto.rsa.RSAKey;
	import com.util.Hex;
	import com.util.der.PEM;
	import com.util.Base64;
	import com.math.BigInteger;
	import flash.utils.ByteArray;
	import com.mylocale.Myxml;

	public class WebServiceParam extends EventDispatcher
	{
		public static const SUCCESS:String = 'Success';
		public var successMsg:String;
		
		var refCreditCardXML:XML;
		var refShippingCarDetXML:XML;
		//
		private var CustomerID:String;
		private var CustTransId:String;
		//
		private var CustBillFname:String;
		private var CustBillLname:String;
		private var CustBillAddr1:String;
		private var CustBillAddr2:String;
		private var CustBillCity:String;
		private var CustBillState:String;
		private var CustBillZip:String;
		private var CustBillPhone:String;
		private var CustBillEmail:String;
		//
		private var CustShipFname:String;
		private var CustShipLname:String;
		private var CustShipAddr1:String;
		private var CustShipAddr2:String;
		private var CustShipCity:String;
		private var CustShipState:String;
		private var CustShipZip:String;
		private var CustShipPhone:String;
		private var CustShipEmail:String;
		//
		private var SpecialInstruction:String;
		private var PersonalInformation:String;
		//
		private var PaymentName:String;
		private var PaymentType:String;
		private var PaymentNumber:String;
		private var PaymentCode:String;
		private var LFD:String;
		private var PaymentExpDt:String;
		//
		private var NewServicesFeedback:String;
		private var Suggestion:String;
		//
		private var AirId:String;
		private var AirName:String;
		private var DeviceId:String;
		private var DeviceCode:String;
		private var DeviceKey:String;
		private var CheckSumRefID:String;
		private var OrderDt:String;
		private var CreatedBy:String;
		private var FlightNo:String;
		private var Attempt:String;
		//
		private var DeliveryStatus:String;
		private var OrderRefId:String;
		private var MacAddr:String;
		private var ProcessorId:String;
		//
		private var OrderDetails:String;
		private var TotalOrderItems:String;
		private var TotalPayment:String;
		//
		public function WebServiceParam() 
		{
			
		}
		public function sendPerchaseDet(aCardXml:String, aShippingXml:String, aServiceFeedback:String, aSuggestion:String):void {
			refCreditCardXML = new XML(aCardXml);;
			refShippingCarDetXML = new XML(aShippingXml);
			assignParams(aServiceFeedback, aSuggestion);
			//trace(refCreditCardXML, 'refCreditCardXML--------------------------');
			//trace(refShippingCarDetXML, 'refShippingCarDetXML--------------------------');
			
			constructString();
			
		}
		private function assignParams(aServiceFeedback:String, aSuggestion:String):void {
			CustomerID = '';
			CustTransId = getEncryptedText(Main.thisClass.vOrderNumber);
			
			//
			CustBillFname = Myxml.getInstance().Trim(refShippingCarDetXML.child('BillingFName')[0]).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingFName')[0]):'';
			CustBillLname = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingLName')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingLName')[0]):'';
			CustBillAddr1 = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingAddr1')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingAddr1')[0]):'';
			CustBillAddr2 = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingAddr2')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingAddr2')[0]):'';
			CustBillCity = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingCity')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingCity')[0]):'';
			CustBillState = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingState')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingState')[0]):'';
			CustBillZip = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingZip')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingZip')[0]):'';
			CustBillPhone = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingPhone')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingPhone')[0]):'';
			CustBillEmail = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('BillingEmail')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('BillingEmail')[0]):''
			//
			CustShipFname = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingFname')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingFname')[0]):'';
			CustShipLname = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingLName')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingLName')[0]):'';
			CustShipAddr1 = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingAddr1')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingAddr1')[0]):'';
			CustShipAddr2 = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingAddr2')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingAddr2')[0]):'';
			CustShipCity = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingCity')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingCity')[0]):'';
			CustShipState = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingState')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingState')[0]):'';
			CustShipZip = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingZip')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingZip')[0]):'';
			CustShipPhone = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingPhone')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingPhone')[0]):'';
			CustShipEmail = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('ShippingEmail')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('ShippingEmail')[0]):'';
			//
			SpecialInstruction = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('SpecialInstruction')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('SpecialInstruction')[0]):'';
			PersonalInformation = Myxml.getInstance().Trim(getEncryptedText(refShippingCarDetXML.child('PersonalInformation')[0])).length > 0 ? getEncryptedText(refShippingCarDetXML.child('PersonalInformation')[0]):'';
			//
			PaymentName = Myxml.getInstance().Trim(getEncryptedText(refCreditCardXML.child('Name')[0])) .length > 0 ? getEncryptedText(refCreditCardXML.child('Name')[0]):'';
			PaymentType = Myxml.getInstance().Trim(getEncryptedText(refCreditCardXML.child('CardType')[0])) .length > 0 ? getEncryptedText(refCreditCardXML.child('CardType')[0]):'';
			PaymentNumber = Myxml.getInstance().Trim(getEncryptedText(refCreditCardXML.child('CardNumber')[0])).length > 0 ? getEncryptedText(refCreditCardXML.child('CardNumber')[0]):'';
			LFD = Myxml.getInstance().Trim(getEncryptedText(setLfd(refCreditCardXML.child('CardNumber')[0])));
			PaymentCode = Myxml.getInstance().Trim(getEncryptedText(refCreditCardXML.child('CVV')[0])) .length > 0 ?getEncryptedText(refCreditCardXML.child('CVV')[0]):'';
			PaymentExpDt = Myxml.getInstance().Trim(getEncryptedText(refCreditCardXML.child('ExpDate')[0])) .length > 0 ? getEncryptedText(refCreditCardXML.child('ExpDate')[0]):'';
			//
			NewServicesFeedback = aServiceFeedback;
			Suggestion = aSuggestion;
			//
			AirId = Myxml.getInstance().webServiceDet('AirId');
			AirName = Myxml.getInstance().webServiceDet('AirName');
			DeviceId = Myxml.getInstance().webServiceDet('DeviceId');
			DeviceCode = Myxml.getInstance().webServiceDet('DeviceCode');
			DeviceKey = Myxml.getInstance().webServiceDet('DeviceKey');
			/* -----------------------------------------------------------
			 * 	Referance Check Sum Id 
			 * ------------------------------------------------------------ */
			CheckSumRefID = '5';
			//
			var now:Date = new Date();
			var tmpDate:String = now.getDate() < 10 ? "0" + String(now.getDate()):String(now.getDate());
			var tmpMonth:String = Number(now.getMonth())+1 < 10 ? "0" + String(Number(now.getMonth())+1):String(Number(now.getMonth())+1);
			OrderDt = tmpMonth +'/' + tmpDate + '/' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes() + ':' + now.getSeconds();
			
			CreatedBy = OrderDt;
			
			FlightNo = refShippingCarDetXML.child('FlightNo')[0];
			
			Attempt = '';
			//
			DeliveryStatus = '';
			OrderRefId = '';
			MacAddr = Myxml.getInstance().webServiceDet('MacAddr');
			ProcessorId = Myxml.getInstance().webServiceDet('ProcessorId');
			//
			
			OrderDetails = purchaseOrdDetails();
			TotalOrderItems = Main.thisClass.shoppingScreen.productDet.purchaseDetail.productDet.length.toString();// Quantity.toString();
			TotalPayment = formatAsDigit(Main.thisClass.shoppingScreen.productDet.purchaseDetail.totAmount.toString());
		}
		//Construnction SOAP String
		private function constructString():void {
			var detString:String = '<?xml version="1.0" encoding="utf-8" ?><OrderRecordSet originatedFrom="'+Myxml.getInstance().getCatalogueType().toUpperCase()+'"><CustomerID>' + CustomerID +'</CustomerID><CustTransId>' + CustTransId + '</CustTransId>'+
						'<CustBillFname>'+ CustBillFname +'</CustBillFname><CustBillLname>'+CustBillLname+'</CustBillLname><CustBillAddr1>'+CustBillAddr1+'</CustBillAddr1>'+
						'<CustBillAddr2>'+CustBillAddr2+'</CustBillAddr2><CustBillCity>'+CustBillCity+'</CustBillCity><CustBillState>'+CustBillState+'</CustBillState>'+
						'<CustBillZip>'+CustBillZip+'</CustBillZip><CustBillPhone>'+CustBillPhone+'</CustBillPhone><CustBillEmail>'+CustBillEmail+'</CustBillEmail>'+
						'<CustShipFname>'+CustShipFname+'</CustShipFname><CustShipLname>'+CustShipLname+'</CustShipLname><CustShipAddr1>'+CustShipAddr1+'</CustShipAddr1>'+
						'<CustShipAddr2>'+CustShipAddr2+'</CustShipAddr2><CustShipCity>'+CustShipCity+'</CustShipCity><CustShipState>'+CustShipState+'</CustShipState>'+
						'<CustShipZip>'+CustShipZip+'</CustShipZip><CustShipPhone>'+CustShipPhone+'</CustShipPhone><CustShipEmail>'+CustShipEmail+'</CustShipEmail>'+
						'<SpecialInstruction>' + SpecialInstruction + '</SpecialInstruction><PersonalInformation>' + PersonalInformation + '</PersonalInformation>'+
						'<PaymentName>'+PaymentName+'</PaymentName><PaymentType>'+PaymentType+'</PaymentType><PaymentNumber>'+PaymentNumber+'</PaymentNumber>'+
						'<PaymentCode>'+PaymentCode+'</PaymentCode><LFD>'+LFD+'</LFD><PaymentExpDt>'+PaymentExpDt+'</PaymentExpDt>'+
						'<NewServicesFeedback>' + NewServicesFeedback + '</NewServicesFeedback><Suggestion>' + Suggestion + '</Suggestion>'+
						'<AirId>' + AirId + '</AirId><AirName>' + AirName + '</AirName><DeviceId>' + DeviceId + '</DeviceId><DeviceCode>' + DeviceCode + '</DeviceCode><DeviceKey>' + DeviceKey + '</DeviceKey>'+
						'<CheckSumRefID>' + CheckSumRefID + '</CheckSumRefID><OrderDt>'+OrderDt+'</OrderDt><CreatedBy>' + CreatedBy + '</CreatedBy><FlightNo>' + FlightNo + '</FlightNo><Attempt>' + Attempt + '</Attempt>'+ 
						'<DeliveryStatus>' + DeliveryStatus + '</DeliveryStatus><OrderRefId>' + OrderRefId + '</OrderRefId><MacAddr>' + MacAddr + '</MacAddr><ProcessorId>' + ProcessorId +'</ProcessorId>' + 
						OrderDetails + '<TotalOrderItems>' + TotalOrderItems + '</TotalOrderItems><TotalPayment>' + TotalPayment + '</TotalPayment></OrderRecordSet>';
						
			sendSOAPMsg('Prod_Det', detString);
		}
		//
		public function sendArilinePacDet(aAirlineStr:String, aProdName:String):void {
			var xml:XML = new XML(aAirlineStr);
			//
			var id:String = '1';
			var OwnerId:String = xml.child('VendorId')[0];
			var CateId:String = xml.child('CategoryId')[0];
			var ProdCode:String = xml.child('ProductCode')[0];
			var ProdId:String = xml.child('ProductId')[0];
			var ProdName:String = aProdName;//xml.child('ProductName')[0];
			var CustomerName:String = xml.child('ContactName')[0];
			var ContactPhone:String = xml.child('ContactPhone')[0];
			var ContactEmail:String = xml.child('ContactEmail')[0];
			
			var DeviceKey:String = Myxml.getInstance().webServiceDet('DeviceKey');
			var DeviceId:String = Myxml.getInstance().webServiceDet('DeviceId');
			var DeviceCode:String = Myxml.getInstance().webServiceDet('DeviceCode');
			var AirId:String = Myxml.getInstance().webServiceDet('AirId');
			var AirName:String = Myxml.getInstance().webServiceDet('AirName');
			
			var FlightNo:String = Myxml.getInstance().webServiceDet('FlightNo');
			//
			var now:Date = new Date();
			var tmpDate:String = now.getDate() < 10 ? "0" + String(now.getDate()):String(now.getDate());
			var tmpMonth:String = Number(now.getMonth())+1 < 10 ? "0" + String(Number(now.getMonth())+1):String(Number(now.getMonth())+1);
			var OrderDt:String = tmpMonth +'/' + tmpDate + '/' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes() + ':' + now.getSeconds();
			//
			var MacAddr:String = Myxml.getInstance().webServiceDet('MacAddr');
			var ProcessorId:String = Myxml.getInstance().webServiceDet('ProcessorId');
			
			var str:String ='<?xml version="1.0" encoding="utf-8" ?><AirlineRecordSet originatedFrom="'+Myxml.getInstance().getCatalogueType().toUpperCase()+'"><Item id="'+id+'"><OwnerId>'+OwnerId+'</OwnerId><CateId>'+CateId+'</CateId><ProdCode>'+ProdCode+'</ProdCode>'+
							'<ProdId>'+ProdId+'</ProdId><ProdName>'+ProdName+'</ProdName><CustomerName>'+CustomerName+'</CustomerName><ContactPhone>'+ContactPhone+'</ContactPhone><ContactEmail>'+ContactEmail+'</ContactEmail>'+
							'<DeviceKey>'+DeviceKey+'</DeviceKey><DeviceId>'+DeviceId+'</DeviceId><DeviceCode>'+DeviceCode+'</DeviceCode><AirId>'+AirId+'</AirId><AirName>'+AirName+'</AirName><FlightNo>'+FlightNo+'</FlightNo>'+
							'<OrderDt>' + OrderDt + '</OrderDt><MacAddr>' + MacAddr + '</MacAddr><ProcessorId>' + ProcessorId + '</ProcessorId></Item></AirlineRecordSet>';
			sendSOAPMsg('Air_Det', str);		
		}
		//sending LFD Digits with xxxxxx
		private function setLfd(aStr:String) {
			if (aStr.length > 4) {
				var sMask:String = '';
				for (var i = 1; i<=aStr.length-4; i++) {
					sMask = sMask + "X";
					if (i % 4 == 0 && i != aStr.length-4) {
						sMask = sMask + "-";
					}
				}
			}
			var tmp:String = sMask+ "-" + aStr.substring(aStr.length - 4, aStr.length);
			return tmp;
		}
		//Text Encription Function 
		private function getEncryptedText(plainText:String ):String {
			var encData:String ="";
			try {
				var N:String ="00bf973867611156fbb2d3435cf39e5e88ffe364f04cd74b712f63773a0ef6d7f79137d1846ed99744673ad669304293a24b0f1222f4e4db0a93f0901924ef2ac37173cf83c4f1ea86eb62a54709ada60ea0c79ad8f0c387444b50fe77e97b5111bc5b12230bc255c373f1208deef828fcf8a0c6a4bb9c5e9f9054316cec2ef69aa5c7fa9ba04810710a6430423c80ba091fea732d1d0a92ab3135948bb01194348852cbd3a8158ea2032e931f160f8c5bce86e8cf2aa5bb4320823139cb1de62d368f1a22b1ce7da5de158336c0cedce89be54d15ea57d8659a83d425a75855a9936eaa3d95854af071286133b7900c253d514400779ba1bfacf8e34510cb7aa7";
				var E:String = "10001";
				var rsa:RSAKey = RSAKey.parsePublicKey(N,E);
				var src:ByteArray = Hex.toArray(Hex.fromString(plainText));
				var dst:ByteArray = new ByteArray;
				var dst2:ByteArray = new ByteArray;
				rsa.encrypt(src, dst, src.length);
				encData = Base64.encodeByteArray(dst); 
			}catch (err:Error) {
				trace('Error in EnCription -', err, plainText);
			}
			return encData;
		}
		//
		private function sendSOAPMsg(aType:String, aProdDet:String):void {
			var ws:WebServiceCall = new WebServiceCall();
			try {
				ws.sendRequest(aType, aProdDet);
				ws.addEventListener(Event.COMPLETE, messageSend);
			}catch (err:Error){
				trace('Error in Send Webservice request', err);
			}
		}
		//SOAP Message Send Conformation
		private function messageSend(evt:Event):void {
			successMsg = evt.currentTarget.status;
			dispatchEvent(new Event('Success'));
		}
		//
		private function purchaseOrdDetails():String {
			var strShoppingBagXML:String = "<Items>";
			var ShoppingBag:Array = Main.thisClass.shoppingScreen.productDet.purchaseDetail.productDet;
				for (var i=0; i<ShoppingBag.length; i++) {
					var vendorid:String = ShoppingBag[i].reffObj.OwnerId.length !=  null ? ShoppingBag[i].reffObj.OwnerId:'';
					var categoryid:String = ShoppingBag[i].reffObj.CateId.length !=  null ? ShoppingBag[i].reffObj.CateId:'';
					var productname:String = ShoppingBag[i].reffObj.ProdTitle !=  null ? ShoppingBag[i].reffObj.ProdTitle:'';
					var productid:String = ShoppingBag[i].reffObj.ProdId != null ? ShoppingBag[i].reffObj.ProdId:'';
					var productcode:String = ShoppingBag[i].reffObj.ProdCode != null ? ShoppingBag[i].reffObj.ProdCode:'';
					var skybuyprice:String = ShoppingBag[i].reffObj.SkybuyPrice != null ? ShoppingBag[i].reffObj.SkybuyPrice:'';
					var vendorprice:String = ShoppingBag[i].reffObj.ProdPrice != null ? ShoppingBag[i].reffObj.ProdPrice:''; 
					var quantity:String = ShoppingBag[i].Quantity != null ? ShoppingBag[i].Quantity:'';
				strShoppingBagXML = strShoppingBagXML + "<Item>";
				strShoppingBagXML = strShoppingBagXML + "<OrderSeqNo>"+i+1+"</OrderSeqNo>" ;
				strShoppingBagXML = strShoppingBagXML+"<OwnerId>"+replaceAnd(vendorid)+"</OwnerId>";
				strShoppingBagXML = strShoppingBagXML + "<CateId>" + replaceAnd(categoryid) + "</CateId>";
				strShoppingBagXML = strShoppingBagXML + "<ProdCode>"+ replaceAnd(productcode) +"</ProdCode>";
				strShoppingBagXML = strShoppingBagXML + "<ProdId>" + replaceAnd(productid) + "</ProdId>";
				strShoppingBagXML = strShoppingBagXML + "<ProdName></ProdName>";
				strShoppingBagXML = strShoppingBagXML+"<SbhPrice>"+replaceAnd(formatAsDigit(skybuyprice))+"</SbhPrice>";
				strShoppingBagXML = strShoppingBagXML+"<VendPrice>"+replaceAnd(formatAsDigit(vendorprice))+"</VendPrice>";
				strShoppingBagXML = strShoppingBagXML +"<Qty>" + quantity + "</Qty>";
					var color:String = '';
					var size:String = '';
					if (ShoppingBag[i].reffObj.ProdColor != null) {
						color = ShoppingBag[i].reffObj.ProdColor.split(',')[ShoppingBag[i].ProdColorIndex] == undefined ? "" : Myxml.getInstance().Trim(ShoppingBag[i].reffObj.ProdColor.split(',')[ShoppingBag[i].ProdColorIndex]);
					}
					if (ShoppingBag[i].reffObj.ProdSize != null) {
						 size = ShoppingBag[i].reffObj.ProdSize.split(',')[ShoppingBag[i].ProdSizeIndex] == undefined ? "" :Myxml.getInstance().Trim(ShoppingBag[i].reffObj.ProdSize.split(',')[ShoppingBag[i].ProdSizeIndex]);
					}
					strShoppingBagXML = strShoppingBagXML+"<ProdColor>"+replaceAnd(color)+"</ProdColor>";
					strShoppingBagXML = strShoppingBagXML + "<ProdSize>"+ replaceAnd(size) +"</ProdSize>";
					
					var strTravelDt:String = "";
					if (ShoppingBag[i].reffObj.SeqId == "5") {
						var monthArr:Array = [0, 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
						var day:int ;
						var month:int;
						for (var a in monthArr) {
							if (monthArr[a] == ShoppingBag[i].ProdDate.split(' ')[1].toLowerCase()) {
								month = a;
								break;
							}
						}
						var dayStr:String = int(ShoppingBag[i].ProdDate.split(' ')[0]) < 10 ? '0' + String(ShoppingBag[i].ProdDate.split(' ')[0]):String(ShoppingBag[i].ProdDate.split(' ')[0]);
						var monthStr:String  = month < 10 ? '0' + String(month) :String(month);
						strTravelDt = monthStr+"/"+dayStr+"/"+ShoppingBag[i].ProdDate.split(' ')[2];
					}
					strShoppingBagXML = strShoppingBagXML+"<TravelDate>"+replaceAnd(strTravelDt)+"</TravelDate>";
					strShoppingBagXML = strShoppingBagXML+"</Item>";		
			}
			strShoppingBagXML = strShoppingBagXML + "</Items>";
			return strShoppingBagXML; 
		}
		private function formatAsDigit(amount:String):String {
			return Myxml.getInstance().formatAsDigit(amount);
		}
		private function replaceAnd(aStr:String= null):String {
			var myPattern:String = '&amp;';
			if (aStr != null) {
				var str:String;
				str = aStr.replace("&", myPattern);
			}
			return str;
		}
	}
}