﻿package com.document{

	import com.services.WebService;
	import flash.events.*;
	
	public  class  WebServiceCall extends EventDispatcher{
		private var __ws:WebService;
		private var __initTime:Number;
		private var __enc:String;
		private var __type:String;
		private var __response:String ;
		
		public var status:String;
		
		private var __url:String = "http://www.skybuyhigh.com/service/device/PlaceOrderPort?wsdl";
		//private var __url:String = "http://thapovan17:8080/TestService/HelloWorldPort?wsdl";
		private var __urlSugg:String = "http://www.skybuyhigh.com/service/device/UpdateCustomerInfoPort?wsdl";
		
		public function WebServiceCall(){
		    __ws= new WebService();
		};
		/*----------------------------------------------------------------------
		 * Param 1: Package detail type - Airdet or prod det
		 * Parm  2: String - prod detail or Airline package details
		 * 
		  -----------------------------------------------------------------------*/
		public function sendRequest(aType:String, enc:String=null):void{
			__enc = enc;
			__type = aType;
			__ws.addEventListener(Event.CONNECT, connected);
			__ws.connect(__url);
			__ws.cacheResults = true;
			
		}
		/*-----------------------------------------------------------------------
		 * Param 1 : Sending Customer Suggestion with Id
		 * 
		  ----------------------------------------------------------------------- */
		public function sendCustomSuggestion(aString:String):void {
			__enc = aString;
			__ws.addEventListener(Event.CONNECT, connectedSuggestion);
			__ws.connect(__urlSugg);
			__ws.cacheResults = true;
		}
		//
		private function connected(evt:Event):void{
			var arg0:String=  __type;
			var arg1:String =__enc;
			//__ws.SayHello(orderPlaced,arg0,arg1);
			__ws.placeOrder(orderPlaced,arg0,arg1);
		}
		//
		private function connectedSuggestion(evt:Event):void {
			var arg0:String=  __enc;
			__ws.updateCustomerFeedBack(suggestionSended,arg0);
		}
		//
		private function suggestionSended(serviceRespone:XML):void {
			trace(serviceRespone);
		}
		private	function orderPlaced(serviceRespone:XML):void {
			//if (__type == 'Prod_Det') {
				status  = (serviceRespone.elements()[0].elements()[0].elements()[0]).toString();
				trace(status, 'status');
				dispatchEvent(new Event(Event.COMPLETE));
			//}
			trace(serviceRespone);
		}
	}
}