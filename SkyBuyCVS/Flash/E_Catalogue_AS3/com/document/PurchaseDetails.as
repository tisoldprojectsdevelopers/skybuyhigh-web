﻿package com.document 
{
	import com.mylocale.Myxml;
	import flash.display.MovieClip;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class PurchaseDetails
	{
		public var purchaseDet:Array;
		private var target:MovieClip;
		
		public function PurchaseDetails(aTarget:MovieClip) 
		{
			target = aTarget;
			purchaseDet = new Array();
		}
		//Adding a product in puchase bag
		public function addProduct(aObj:Object, aCount:int = 1, aColor:int = -1, aSize:int = -1, aDate:String=null ):int {
			var reffObj:Object = new Object(); 
			reffObj.reffObj = aObj;
			reffObj.Quantity = aCount;
			reffObj.ProdColorIndex = aColor; 
			reffObj.ProdSizeIndex = aSize;
			reffObj.ProdDate = aDate;
			for each(var det in purchaseDet) {
				if (det.reffObj.ProdId == aObj.ProdId && det.reffObj.OwnerId == aObj.OwnerId && det.ProdColorIndex  == aColor && det.ProdSizeIndex  == aSize && det.ProdDate == aDate)
				{
					det.Quantity += aCount;
					recountTotal();
					return 0;
				}
			}
			purchaseDet.push(reffObj);
			recountTotal();
			return 1;
		}
		//Removing a product in puchase bag
		public function removeProduct(aObj:Object):int {
			for (var obj in purchaseDet) {
				if (purchaseDet[obj] == aObj) {
					purchaseDet.splice(obj, 1);
					recountTotal();
					grandTotal();
					return 1;
				}
			}
			return 0;
		}
		//Update a product in puchase bag
		public function updateProduct(aObj:Object):int {
			for (var obj in purchaseDet) {
				if (purchaseDet[obj]== aObj) {
					purchaseDet[obj].Quantity = int(aObj.Quantity);
					recountTotal();
					grandTotal();
					return 1;
				}
			}
			return 0;
		}
		//Count total no of products in puchase bag
		private function recountTotal():void {
			var totalNo:int;
			for each(var obj in purchaseDet) {
				totalNo = totalNo + obj.Quantity;
			}
			try {
				Main.thisClass.shoppingScreen.no_of_items.text = '( ' + String(totalNo) + ' items )';
			}catch(err:Error) {
				trace('Error in recoutTotal',err);
			}
		}
		//Calculate gradTotal
		public function grandTotal():void {
			var grandTot:Number = 0;
			for each(var obj in purchaseDet) {
				grandTot = grandTot + (obj.Quantity * obj.reffObj.PrizeTotal);
			}
			try {
				Main.thisClass.shoppingScreen.productDet.shoppingCart.prodgrandtot._txt.text = Myxml.getInstance().formatAsDollars('$' + String(grandTot));
			}catch(err:Error) {
				trace('Error in recoutTotal',err);
			}
		}
		//Club the Same product with same color and Size and Date
		public function clubProducts():void {
			for (var i in purchaseDet) {
				/*trace(purchaseDet[i].reffObj.ProdId ,
						purchaseDet[i].reffObj.OwnerId ,
						purchaseDet[i].ProdColorIndex ,
						purchaseDet[i].ProdSizeIndex ,
						purchaseDet[i].ProdDate , '----------------------------')*/
				for (var j in purchaseDet) {
					if (i != j) {
						var source:Object = purchaseDet[i];
						var compare:Object = purchaseDet[j];
						if (
						source.reffObj.ProdId == compare.reffObj.ProdId &&
						source.reffObj.OwnerId == compare.reffObj.OwnerId &&
						source.ProdColorIndex == compare.ProdColorIndex &&
						source.ProdSizeIndex == compare.ProdSizeIndex &&
						source.ProdDate == compare.ProdDate
						) {
							source.Quantity += compare.Quantity;
							purchaseDet.splice(j, 1);
							trace(purchaseDet.length, compare.reffObj.ProdId)
							break;
						}
					}
				}
			}
		}
		//Return the products contain array
		public function get productDet():Array {
			return purchaseDet;
		}
		//
		public function get Quantity():int {
			var totalNo:int;
			for each(var obj in purchaseDet) {
				totalNo = totalNo + obj.Quantity;
			}
			return totalNo;
		}
		//
		public function get totAmount():String{
			var grandTot:Number = 0;
			for each(var obj in purchaseDet) {
				grandTot = grandTot + (obj.Quantity * obj.reffObj.PrizeTotal);
			}
			return String(grandTot);
		}
	}
}