﻿
package com.services{
	
	import com.services.WSDL;
	import com.services.WSMethod;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import com.services.WSProxy;
	
	public dynamic class  WebService extends EventDispatcher{
		private var __wsdl:WSDL;
		private var __availableMethods:Array;
		private var __proxy:WSProxy;
		
		public function WebService(){
			__proxy = WSProxy.getInstance();
		};
			
		private function wsdlComplete(methods:Array):void{
			__availableMethods = methods;
			var a:Number;
			for(a=0;a<__availableMethods.length;a++){
				var method:WSMethod = new WSMethod();
				 this[__availableMethods[a].name] = method.createMethod(__availableMethods[a].name, __availableMethods[a].param, __availableMethods[a].targetNS, __availableMethods[a].servicePath, __availableMethods[a].soapAction);
			}
			dispatchEvent(new Event(Event.CONNECT));
		}
		public function connect(wsdl:String):void{
			__wsdl = new WSDL(wsdl);
			__wsdl.getWSDL(wsdlComplete);
		}
		
		public function clearCache():void{
			__proxy.clearCache();
		}
		
		public function get availableMethods():Array{
			return __availableMethods;
		}
		
		public function set cacheResults(cache:Boolean):void{
			__proxy.cacheResults = cache;
		}
	}
	
}