

package com.services {
	
	import com.services.WSCache;

	public class WSCache {
		
		private static var __instance:WSCache;
		private var __cache:Array;
		
		public function WSCache(){
			__cache = new Array();
		};
		
		public static function getInstance():WSCache{
			if(__instance == null){
				__instance = new WSCache();
			}
			return __instance;
		}
		
		public function checkCache(target:String, req:XML):Boolean{
			var a:Number;
			for(a=0;a<__cache.length;a++){
				if(__cache[a].service == target && String(__cache[a].request) == String(req)){
					return true;
				}
			}
			return false;
		}
		
		public function getCachedResult(target:String, req:XML):XML{
			var a:Number;
			for(a=0;a<__cache.length;a++){
				if(__cache[a].service == target && String(__cache[a].request) == String(req)){
					return __cache[a].response;
				}
			}
			return null;
		}
		
		public function setCachedResult(target:String, req:XML, resp:XML):void{
			if(!checkCache(target, req)){
				__cache.push({service: target, request: req, response: resp});
			}
		}
		
		public function flushCache():Boolean{
			__cache = new Array();
			return true;
		}
	}
	
}