

package com.services{
	
	import com.services.WSProxy;

	public dynamic class WSMethod {
		
		private var __action:String;
		private var __servicePath:String;
		private var __methodName:String;
		private var __params:Array;
		private var __targetNamespace:String;
		private var __proxy:WSProxy;
		
		public function WSMethod(){
			__proxy = WSProxy.getInstance();
		};
		
		private function myMethod(loaded:Function, ...args):void{
			var params:String = new String();
			var a:Number;
			for(a=0;a<__params.length;a++){
				var argument:String = "";
				if(args[a] != undefined){
					argument = args[a];
				}
				params += '<arg' + a + '><![CDATA['+ argument + ']]></arg' +a + '>';
			}
	
			var soapRequest:String = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:PlaceOrderServiceSvc="' + __targetNamespace +'">';
			soapRequest += '<soap:Body>';
			soapRequest += '<PlaceOrderServiceSvc:' + __methodName + '>';
			soapRequest += params;
			soapRequest += '</PlaceOrderServiceSvc:' + __methodName + '>';
			soapRequest += '</soap:Body>';
			soapRequest += '</soap:Envelope>';
			
			var request:XML = new XML(soapRequest);
			
			/**
			 * PUT SENDING ACTION HERE
			 */
			__proxy.callMethod(loaded, request, __servicePath, __action);
		}
		
		public function createMethod(name:String, param:Array, ns:String, service:String, action:String):Function{
		
			__servicePath = service;
			__methodName = name;
			__params = param;
			__targetNamespace = ns;
			__action = action;
			
			return myMethod;
		}
		
	}
	
}