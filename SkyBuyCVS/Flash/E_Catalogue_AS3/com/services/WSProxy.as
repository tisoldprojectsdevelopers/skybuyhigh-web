
package com.services {
	
	import flash.errors.IOError;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import com.services.WSProxy;
	import com.services.WSCache;
	
	public class WSProxy {
		
		private static var __instance:WSProxy;
		private var __cache:WSCache;
		private var __urlLoader:URLLoader;
		private var __completeEvent:Function;
		private var __callQueue:Array = new Array();
		private var __busyOnCall:Boolean = false;
		private var __cacheResults:Boolean = false;
		private var __request:XML;
		private var __servicePath:String;
		
		public function WSProxy(){
			__cache = WSCache.getInstance();
		}
		
		public static function getInstance():WSProxy{
			if(__instance == null){
				__instance = new WSProxy();
			}
			return __instance;
		}
		
		private function callService(onLoad:Function, request:XML, servicePath:String, action:String):void{
			__request = request;
			__servicePath = servicePath;
			__completeEvent = onLoad;
			if(__cacheResults){	
				if(__cache.checkCache(servicePath, request)){
					var response:XML = __cache.getCachedResult(servicePath, request);
					__completeEvent(response);
					return;
				}
			}
			var urlRequest:URLRequest = new URLRequest();
			urlRequest.contentType = "text/xml; charset=utf-8";
			urlRequest.method = "POST";
			urlRequest.url = servicePath;
			var soapAction:URLRequestHeader = new URLRequestHeader("SOAPAction", action);
			urlRequest.requestHeaders.push(soapAction);
			urlRequest.data = request;
			__urlLoader = new URLLoader();
			__urlLoader.addEventListener(Event.COMPLETE, onComplete);
			__urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, hdlrSecurityError);
			__urlLoader.addEventListener(IOErrorEvent.IO_ERROR, hdlrIOError);
			__urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, hdlrHttpStatus);
			__urlLoader.load(urlRequest);
			
		}
		private function hdlrSecurityError(err:SecurityError):void {
			trace('SecurityError', err);
		}
		private function hdlrIOError(err:IOErrorEvent):void {
			trace('IOError', err.text)
		}
		private function hdlrHttpStatus(stat:HTTPStatusEvent):void {
			trace(stat);
		}
		
		private function onComplete(evt:Event):void{
			var response:XML = new XML(__urlLoader.data);
			__completeEvent(response);
			if(__cacheResults){
				__cache.setCachedResult(__servicePath, __request, response);
			}
			__busyOnCall = false;
			if(__callQueue.length > 0){
				callService(__callQueue[0].load, __callQueue[0].req, __callQueue[0].path, __callQueue[0].soapAction);
				__callQueue.splice(0,1);
			}
		}
		
		public function callMethod(onLoad:Function, request:XML, servicePath:String, action:String):void{
			if(!__busyOnCall){
				__busyOnCall = true;
				callService(onLoad, request, servicePath, action);
			} else {
				__callQueue.push({load: onLoad, req: request, path: servicePath, soapAction: action});
			}
		}
		
		public function set cacheResults(cache:Boolean):void{
			__cacheResults = cache;
		}
		
		public function clearCache():void{
			__cache.flushCache();
		}
		
	}
	
}