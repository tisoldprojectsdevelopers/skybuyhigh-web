﻿package com.mylocale 
{
	import com.document.Main;
	import flash.events.*;
	import flash.net.*;
	import flash.errors.IOError;
	import flash.system.Capabilities;
	

	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Myxml extends EventDispatcher
	{
		public static const  XML_LOADED:String = 'xmlloaded';
		//Singleton instance 
		public static var _myxml:Myxml;
		//XML Declaration
		protected var xmlCatalogue:XML;
		protected var coverFlow:XML;
		protected var vendorXml:XML;
		protected var xmlContent:XML
		protected var webService:XML;
		//
		private var allAreLoaded:int
		private var noOfxmls:int
		//Hold all the vendor details from the XML;
		private var vendorDetails:Array;
		private var menus:Array;
		private var infostruc:Array;
		private var filesArr:Array;
		
		//Return the Instance - singleton Class;
		public static function getInstance():Myxml{
			if (!_myxml) {
				_myxml = new Myxml();
			}
			return _myxml;
	
		}
		/* Assigning all xml datas to the xml Object.
		 * xmlFile1 - determines the Catalogue xml
		 * xmlFile2 - determines the eCatalogue_cover xml
		 * xmlFile3 - determines the eCatalogue_partner xml
		 * xmlFile4 - determines the custInfo xml
		 * 
		 */
		public function getContent(xmlFile1:String = null, xmlFile2:String = null, xmlFile3:String = null):void	{
			
			filesArr = new Array();
			try 
			{
				for ( var i in arguments) {
					if (arguments[i] != 'undefined' && arguments[i] != null) {
						filesArr.push(arguments[i]);
					}
				}
				noOfxmls = filesArr.length;
				if (noOfxmls  > 0) {
					loadXML(filesArr[0]);
				}
			}
			catch (err:Error)
			{
				throw new Error('Error In xml Loading')
			}
			
		}
		//Load the external xml files
		private function loadXML(aFileName:String):void {
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			loader.addEventListener(Event.COMPLETE, completeHandler);
			try {
				loader.load(new URLRequest(escape(aFileName)));
			} catch (ioError:IOError) { 
				throw new Error(ioError + 'ioError in xml Loading');
				trace(ioError + 'ioError in xml Loading');
			} catch (securityError:SecurityError) {
				throw new Error(securityError + 'securityError in xml Loading');
				trace(securityError + 'securityError in Xml Loading');
			} catch (error:Error) {
				throw new Error(error + 'Error in xml Loading');
				trace(error +'Error in XML Loading');
			}
		}
		//Complete Handler ECatalogue
		private function completeHandler(event:Event):void {
			var loader:URLLoader = event.currentTarget as URLLoader;
			if (allAreLoaded == 0 ) {
				xmlCatalogue = new XML(loader.data);
			}
			else if (allAreLoaded == 1) {
				coverFlow = new XML(loader.data);
			}
			else if (allAreLoaded == 2) {
				vendorXml = new XML(loader.data);
			}
			allAreLoaded++;
			if (allAreLoaded == noOfxmls) {
				if (getCatalogueType().toUpperCase() != 'PRODUCT_PREVIEW') {
					createVendorArr();
				}
				getCatalogueXMLToObj();
				dispatchEvent(new Event('xmlloaded'));
			}else {
				loadXML(filesArr[allAreLoaded]);
			}
		}
		//Load xml Info XML
		public function loadCustomInfo(aString:String):void {
			var urlLdr:URLLoader = new URLLoader();
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			urlLdr.addEventListener(Event.COMPLETE, hdlrCustomComplete);
			try {
				urlLdr.load(new URLRequest(aString));
			}catch (err:Error) {
				trace('Error in Loading Custom info xml');
			}
		}
		//Load complte custom info xml;
		private function hdlrCustomComplete(evt:Event):void {
			xmlContent = new XML(evt.currentTarget.data);
		}
		//Load xml Webservice XML
		public function WebserviceXML(aString:String):void {
			var urlLdr:URLLoader = new URLLoader();
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			urlLdr.addEventListener(Event.COMPLETE, hdlrWebserviceLoaded);
			try {
				urlLdr.load(new URLRequest(aString));
			}catch (err:Error) {
				trace('Error in Loading Web service xml');
			}
		}
		//
		private function hdlrWebserviceLoaded(evt:Event):void {
			webService = new XML(evt.currentTarget.data);
		}
		//Trim 
		public function Trim(aStr:String):String {
			var str:String = ltrim(aStr);
			str = rtrim(str);
			return str;
		}
		//Left Trim
		private function ltrim(matter):String {
			if ((matter.length>1) || (matter.length == 1 && matter.charCodeAt(0)>32 && matter.charCodeAt(0)<255)) {
				var i = 0;
				while (i<matter.length && (matter.charCodeAt(i)<=32 || matter.charCodeAt(i)>=255)) {
					i++;
				}
				matter = matter.substring(i);
			} else {
				matter = "";
			}
			return matter;
		}
		//Right Trim
		private function rtrim(matter):String {
			var i;
			if ((matter.length>1) || (matter.length == 1 && matter.charCodeAt(0)>32 && matter.charCodeAt(0)<255)) {
				i = matter.length-1;
			   while (i>=0 && (matter.charCodeAt(i)<=32 || matter.charCodeAt(i)>=255)) {
					i--;
				}
				matter = matter.substring(0, i+1);
			} else {
				matter = "";
			}
			return matter;
		}
		//Input output Error
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace('IO-Error in XML Loading');
		}
		//SecurityError
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace('Security-Error in XML Loading');
		}
		/*
		*  Returns Categories node attributes values - First Node in ECatalogue XML
		*/
		public function categories_path(aStr:String):String
		{
			return (xmlCatalogue ? String(Trim(xmlCatalogue.@[aStr])):'');
		}
		/*
		*  Return the introduction page xmlList
		*/
		public function coverflow_images():XML {
			var lt:XML;
			if (coverFlow) {
				lt = coverFlow
			}
			return lt;
		}
		/*
		 * Convert Vendor xml datas to an Array
		*/
		private function createVendorArr():void {
			vendorDetails = new Array();
			var len:int = vendorXml.children().length();
			for (var i = 0; i < len; i++ ) {
				var id:Number = vendorXml.children()[i].@id ? Number(Trim(vendorXml.children()[i].@id)):0;
				var aboutMeURL:String  = vendorXml.children()[i].@aboutMeURL ? Trim(vendorXml.children()[i].@aboutMeURL):"";
				var vendorName:String  = vendorXml.children()[i].@name ? Trim(vendorXml.children()[i].@name):"";
				var vendorType:String  = vendorXml.children()[i].@type ? Trim(vendorXml.children()[i].@type):"";
				var gen_id:String;
				
				if (id != 0 && vendorType != '') {
					if (vendorType == "vendor") {
						gen_id = String(id)+"_vendor";
					} else {
						gen_id = String(id)+"_airline";
					}
					vendorDetails[gen_id] = {Id:id, AboutMeURL:aboutMeURL, VendorName:vendorName, VendorType:vendorType};
				}
			}
			createMenuArr();
		}
		/*
		 * Generate array for menus 
		 */
		private function createMenuArr():void {
			var ar:Array;
			menus = new Array();
			for (var i=0; i< xmlCatalogue.children().length(); i++) {
				var seqId:Number = xmlCatalogue.children()[i].@seqId ? Number(xmlCatalogue.children()[i].@seqId) : 0;
				var node:String = "";
				if (seqId == 1) {
					node = "women";
				} else if (seqId == 2) {
					node = "men";
				} else if (seqId == 3) {
					node = "personal";
				}else if (seqId == 4) {
					node = "gift";
				}else if(seqId == 5) {
					node = "packages";
				}else if (seqId == 6) {
					node = "specialProducts";
				}
				ar = new Array();
				for (var j = 0; j < xmlCatalogue.children()[i].children().length(); j++) {
					var id:Number = xmlCatalogue.children()[i].children()[j].@OwnerId;
					if (vendorDetails[String(xmlCatalogue.children()[i].children()[j].@OwnerId) + "_vendor"]) {
						var ownerName:String = vendorDetails[String(xmlCatalogue.children()[i].children()[j].@OwnerId) + "_vendor"].VendorName;
						var ownerId:Number = Number(xmlCatalogue.children()[i].children()[j].@OwnerId)
					}
					if (ownerName != null && ownerId != 0) {
						if (isAlreadyExist(ar, ownerId) != true) {
							ar.push({OwnerId:ownerId, OwnerName:ownerName, CategoryId:seqId, ProductId:j});
						}
					}
				}
				ar.sortOn(["OwnerName"]);
				menus[node] = ar;
			}
		}
		//find the owener already exists
		private function isAlreadyExist(arVendorDetails:Array, id:Number):Boolean {
			var isExist:Boolean = false;
			for (var i = 0; i<arVendorDetails.length; i++) {
				if (arVendorDetails[i].OwnerId == id) {
					isExist = true;
					break;
				}
			}
			return isExist;
		}
		//Retrun the menu Objects - men, women, gift
		public function getMenuObject(aType:String):Array {
			var menuItem:Array
			if (menus[aType]) {
				menuItem = menus[aType];
			}
			return menuItem;
		}
		//Return an XML Catalog xml
		public function get getCatalogueXML():XML {
			var _xml:XML
			if (xmlCatalogue) {
				_xml = xmlCatalogue;
			}
			return _xml;
		}
		//
		private function getCatalogueXMLToObj():void {
			infostruc = new Array();
			var cataPro:CatalogProperties = new CatalogProperties();
			infostruc = cataPro.getArray().slice(0, cataPro.getArray().length);
		}
		//
		public function get getCatalogueObjxml():Array {
			return infostruc;
		}
		//Return and vendorxml attribut values
		public function getVendorDetails(aId:String, aString:String):String {
			var str:String
			if (vendorXml) {
				str = vendorXml.Owner.(@id == aId).@[aString];
			}
			return ''+str;
		}
		// Return a obj from xmlCatalogue for perticular seqId and OwnerId;
		public function getxmlCatalogueObj(aSeqId:int, aProductId:int):Object {
			var obj:Object;
			if (infostruc) {
				for each(var property in infostruc) {
					if (property.ProdId == aProductId && property.SeqId == aSeqId) {
						obj = property;
						break;
					}
				}
			}
			return obj;
		}
		//
		public function getTerms(aString):String {
			var str:XML;
			if (xmlCatalogue) {
				str = xmlCatalogue.Messages.Message.(@type == aString)[0] as XML;
			}
			return String(str);
		}
		//Return the skybuyhigh superscript font embed texts
		public function getFormatedText(aStr:String):String {
			var supStartExpression:RegExp = new RegExp("<sup>", "g");
			var supEndExpression:RegExp = new RegExp("</sup>", "g");
			aStr = aStr.replace(supStartExpression, "<font face=\"GG Superscript\" >");
			aStr = aStr.replace(supEndExpression, "</font>");
			return aStr;
		}
		//
		public function formatAsDollars(amt:String):String {
			var amount_array:Array;
			if (amt.indexOf("$", 0) != -1) {
				amt = amt.substring(amt.indexOf("$", 0)+1, amt.length);
			}
			amount_array=amt.split(",");
			amt="";
			for (var i:Number=0; i<amount_array.length; i++) {
				amt=amt+amount_array[i];
			}
			var amount = parseFloat(amt);
			if (isNaN(amount)) {
				return "$0.00";
			}
			// round the amount to the nearest 100th             
			amount = Math.round(amount*100)/100;

			// convert the number to a string
			var amount_str:String = String(amount);

			// split the string by the decimal point, separating the
			// whole dollar value from the cents. Dollars are in
			// amount_array[0], cents in amount_array[1]
			amount_array = amount_str.split(".");

			// if there are no cents, add them using "00"
			if (amount_array[1] == undefined) {
				amount_array[1] = "00";
			}
			// if the cents are too short, add necessary "0"             
			if (amount_array[1].length == 1) {
				amount_array[1] += "0";
			}
			// add the dollars portion of the amount to an             
			// array in sections of 3 to separate with commas
			var dollar_array:Array = new Array();
			var start:Number;
			var end:Number = amount_array[0].length;
			while (end>0) {
				start = Math.max(end-3, 0);
				dollar_array.unshift(amount_array[0].slice(start, end));
				end = start;
			}
			// assign dollar value back in amount_array with
			// the a comma delimited value from dollar_array
			amount_array[0] = dollar_array.join(",");

			// finally construct the return string joining
			// dollars with cents in amount_array
			return "$" + amount_array.join(".");
		}
		//Retrun $format to Number format
		public function formatAsDigit(amount:String):String {
			var digit:String = "";
			if (amount.indexOf("$", 0) != -1) {
				amount = amount.substring(amount.indexOf("$", 0)+1, amount.length);
			}
			var amount_array = amount.split(",");
			for (var k = 0; k<amount_array.length; k++) {
				digit = digit+amount_array[k];
			}
			return digit;
		}
		// Retrun the shipping address details information
		public function getCustomerInfoXML():Boolean {
			var isDefined:Boolean
			if (xmlContent) {
				isDefined = true;
			}
			return isDefined
		}
		//
		public function getCustomerInfo(aStr:String):String {
			var xml:XML;
			if (xmlContent) {
				xml = xmlContent[aStr][0] as XML;
			}
			return String(xml);
		}
		//Return the xmlCatalog given seqId length
		public function getLength(aString:String):int {
			var num:int
			if (xmlCatalogue) {
				num = xmlCatalogue.Category.(@seqId == aString).children().length();
			}
			return num;
		}
		//Check whether the is Democatalog or not
		/*public function get isDemoCatalouge():Boolean {
			var aBool:Boolean;
			if (xmlCatalogue) {
				if (String(xmlCatalogue.@isDemonstrationCatalogue).length > 0) {
					aBool = xmlCatalogue.@isDemonstrationCatalogue.toLowerCase() == 'y' ? true:false;
				}
			}
			return aBool;
		}*/
		//
		public function webServiceDet(aString:String):String {
			var str:String;
			if (webService) {
				str = webService.child(aString)[0];
			}
			return str;
		}
		//
		public function getCatalogueType():String {
			var str:String = '';
			if (xmlCatalogue) {
				if (String(xmlCatalogue.@eCatalogueType).length > 0) {
					str = xmlCatalogue.@eCatalogueType.toLowerCase(); 
				}
			}
			return str;
		}
	}
}