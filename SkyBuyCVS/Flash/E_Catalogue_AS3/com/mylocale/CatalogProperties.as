package com.mylocale {
	import flash.xml.XMLNode;
	
	public class CatalogProperties 
	{
		private var _xmlIns:Myxml;
		private var _xml:XML;
		//Hold all the information in contain in catalog xml
		private var infostruc:Array;
		
		public function CatalogProperties() 
		{
			_xmlIns = Myxml.getInstance();
		}
		public function getArray():Array {			
			infostruc = new Array();
			_xml = _xmlIns.getCatalogueXML;
			var catLen:int = _xml.Category.length();
			for (var i = 0; i < catLen; i++) {
				var len:int = _xml.Category[i].children().length();
				for (var j = 0; j < len; j++) {
					try {
						var Obj:Object = new Object();
						Obj.OwnerId = getAttValue(_xml.Category[i].children()[j], 'OwnerId');
						Obj.Adv = getAttValue(_xml.Category[i].children()[j], 'Adv'); 
						Obj.SwfPath = getAttValue(_xml.Category[i].children()[j], 'SwfPath');
						Obj.CateId = getAttValue(_xml.Category[i], 'id');
						Obj.SeqId = getAttValue(_xml.Category[i], 'seqId');
						Obj.ProdId = getNodeValue(_xml.Category[i].children()[j], 'ProductId'); 
						Obj.ProdColor = getNodeValue(_xml.Category[i].children()[j], 'color'); 
						Obj.ProdSize = getNodeValue(_xml.Category[i].children()[j], 'size'); 
						Obj.ProdCode = getNodeValue(_xml.Category[i].children()[j], 'ProductCode');
						Obj.ProdBrand = getNodeValue(_xml.Category[i].children()[j], 'BRAND');
						Obj.ProdTitle =  getNodeValue(_xml.Category[i].children()[j], 'TITLE');
						Obj.ProdSD = getNodeValue(_xml.Category[i].children()[j], 'ShortDiscription');
						Obj.ProdLD = getNodeValue(_xml.Category[i].children()[j], 'LongDiscription');
						Obj.ProdPrice = getNodeValue(_xml.Category[i].children()[j], 'Price');
						Obj.SkybuyPrice = getNodeValue(_xml.Category[i].children()[j], 'SkyBuyPrice'); 
						Obj.CoverFlowImg = getAttValue1(_xml.Category[i].children()[j].MainImageURL, 'CoverFlowImgPath');  
						
						Obj.MainImage = getNodeValue(_xml.Category[i].children()[j], 'MainImageURL');
						Obj.MainImageCaption = getAttValue1(_xml.Category[i].children()[j].MainImageURL, 'Caption');
						Obj.MainOrgImage = getAttValue1(_xml.Category[i].children()[j].MainImageURL, 'OriginalImgPath'); 
						Obj.MainThumbImage = getNodeValue(_xml.Category[i].children()[j], 'MainThumbURL'); 
						Obj.MainMedImage = getNodeValue(_xml.Category[i].children()[j], 'MainMedView');  
						Obj.MainLargeImage = getNodeValue(_xml.Category[i].children()[j], 'MainLargeView'); 
						
						Obj.ViewOneImage = getNodeValue(_xml.Category[i].children()[j], 'ViewOneImageURL');  
						Obj.ViewOneImageCaption = getAttValue1(_xml.Category[i].children()[j].ViewOneImageURL, 'Caption'); 
						Obj.ViewOneOrgImage = getAttValue1(_xml.Category[i].children()[j].ViewOneImageURL, 'OriginalImgPath');  
						Obj.ViewOneThumbImage = getNodeValue(_xml.Category[i].children()[j], 'ViewOneThumbURL') 
						Obj.ViewOneMedImage = getNodeValue(_xml.Category[i].children()[j], 'ViewOneMedView'); 
						Obj.ViewOneLargeImage = getNodeValue(_xml.Category[i].children()[j], 'ViewOneLargeView');
						
						Obj.ViewTwoImage = getNodeValue(_xml.Category[i].children()[j], 'ViewTwoImageURL')
						Obj.ViewTwoImageCaption = getAttValue1(_xml.Category[i].children()[j].ViewTwoImageURL, 'Caption') 
						Obj.ViewTwoOrgImage = getAttValue1(_xml.Category[i].children()[j].ViewTwoImageURL, 'OriginalImgPath');
						Obj.ViewTwoThumbImage = getNodeValue(_xml.Category[i].children()[j], 'ViewTwoThumbURL');
						Obj.ViewTwoMedImage = getNodeValue(_xml.Category[i].children()[j], 'ViewTwoMedView'); 
						Obj.ViewTwoLargeImage = getNodeValue(_xml.Category[i].children()[j], 'ViewTwoLargeView');
						//
						Obj.ViewThreeImage = getNodeValue(_xml.Category[i].children()[j], 'ViewThreeImageURL');
						Obj.ViewThreeImageCaption = getAttValue1(_xml.Category[i].children()[j].ViewThreeImageURL, 'Caption');
						Obj.ViewThreeOrgImage = getAttValue1(_xml.Category[i].children()[j].ViewThreeImageURL, 'OriginalImgPath');
						Obj.ViewThreeThumbImage = getNodeValue(_xml.Category[i].children()[j], 'ViewThreeThumbURL')
						Obj.ViewThreeMedImage = getNodeValue(_xml.Category[i].children()[j], 'ViewThreeMedView');
						Obj.ViewThreeLargeImage = getNodeValue(_xml.Category[i].children()[j], 'ViewThreeLargeView');
						Obj.loaded = false, 
						
						Obj.ReturnPolicy = getNodeValue(_xml.Category[i].children()[j], 'ReturnPolicy');
						
						Obj.Instruction = getNodeValue(_xml.Category[i].children()[j], 'Instruction');
						Obj.ValidFrom = getNodeValue(_xml.Category[i].children()[j], 'ValidFrom');
						Obj.ValidTo = getNodeValue(_xml.Category[i].children()[j], 'ValidTo');
						//
						/*Obj.ProdColorIndex = null;
						Obj.ProdSizeIndex = null;
						Obj.ProdDate = null;*/
						Obj.Quantity = 0;
						Obj.PrizeTotal =  Number(Obj.SkybuyPrice.split('$')[1]);
					
						//
						infostruc.push(Obj);
					}
					catch (err:Error) {
						trace('Error in XML catalog variable assigning', i, j);
					}
				}
			}
			return infostruc;
		}
		//Return the Node value; 
		private function getNodeValue(aNode:XML, nodeName:String):String {
			var str:String
			if (aNode) {
				str = String(aNode[nodeName][0]);
				str = str.length > 0 ? _xmlIns.Trim(str): null;
			}
			//trace(str , ' -- getNodeValue');
			return str;
		}
		//Return the attribute value
		private function getAttValue(aNode:XML, attName:String):String {
			var str:String
			if (aNode) {
				str = String(aNode.@[attName]);
				str = str.length > 0 ? _xmlIns.Trim(str): null;
			}
			//trace(str , ' -- getAttValue');
			return str;
		}
		//
		private function getAttValue1(aXmlList:*, attName:String):String {
			var str:String
			if (aXmlList) {
				str = String(aXmlList.@[attName]);
				str = str.length > 0 ? _xmlIns.Trim(str): null;
			}
			//trace(str , ' -- getAttValue1');
			return str;
		}
	}
}