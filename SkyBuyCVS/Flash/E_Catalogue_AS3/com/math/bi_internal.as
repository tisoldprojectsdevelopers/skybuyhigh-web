/**
 * bi_internal
 * 
 * A namespace. w00t.
 * Copyright (c) 2007 Henri Torgemane
 * 
 * See LICENSE.txt for full license information.
 */
package com.math {
	public namespace bi_internal = "";
}