﻿package com.menu 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.Event;
	//Custom Classes
	import com.menu.Event.MenuEvent;
	
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class MenuItem extends MovieClip
	{
		public var currentObj:Object;
		public var _txt:String;
		private var bgRect:Sprite;
		private var rollOverRect:Sprite
		//MenuItem Properties
		private var rollOverColor:Number = 0xFFFFFF;
		private var rollOutColor:Number = 0x003366;
		//
		private var bgRollOverColor:Number = 0x003366;
		private var bgRollOutColor:Number = 0xf5f5f5;
		//
		private var bgColor:Number = 0xf5f5f5;
		//
		private var tf:TextFormat
		//
		
		//
		public function MenuItem() 
		{
			buttonMode = true;
			mouseChildren = false;
			addEventListener(Event.ADDED_TO_STAGE, menuItemAdded);
			
		}
		private function menuItemAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, menuItemAdded);
			//
			drawBGRect();
			drawRollOverRect();
			
			formatTextFiels();
			menulbl.text = _txt;
			//
			addEventListener(MouseEvent.MOUSE_DOWN, hdlrMouseDown);
			addEventListener(MouseEvent.ROLL_OVER, hdlrRollOver);
			addEventListener(MouseEvent.ROLL_OUT, hdlrRollOut);
		}
		//MenuItem Bg Color;
		private function drawBGRect():void 
		{	
			bgRect = new Sprite();
			bgRect.graphics.beginFill(bgColor, 1);
			bgRect.graphics.drawRect( 2, 0, this.width-3, this.height);
			bgRect.graphics.endFill();
			addChild(bgRect);
			this.swapChildren(menulbl, bgRect);
		}
		//Menu Item MouseDown
		private function hdlrMouseDown(evt:MouseEvent):void {
			trace('Menu Item Down');
			dispatchEvent(new MenuEvent(MenuEvent.MENU_ITEM_SELECT, false, false, currentObj));
		}
		//Menu Item RollOver color
		private function drawRollOverRect():void {
			rollOverRect = new Sprite();
			rollOverRect.graphics.beginFill(bgRollOverColor, 1);
			rollOverRect.graphics.drawRect(2.5, 0.5, this.width-5, this.height-0.5);
			rollOverRect.graphics.endFill();
			addChild(rollOverRect);
			rollOverRect.visible = false;
			this.swapChildren(menulbl, rollOverRect);
		}
		//Menu Item Text Formater
		private function formatTextFiels():void {
			tf = new TextFormat();
			tf.color = rollOutColor;
			tf.indent = 10;
			tf.italic = false;
			tf.align = TextFormatAlign.LEFT;
			menulbl.defaultTextFormat = tf ;
		}
		//RollOver Handler
		private function hdlrRollOver(evt:MouseEvent):void {
			tf.color = rollOverColor;
			menulbl.defaultTextFormat = tf ;
			menulbl.text = _txt;
			rollOverRect.visible = true;
		}
		//RollOver Handler
		private function hdlrRollOut(evt:MouseEvent):void {
			rollOverRect.visible = false;
			tf.color = rollOutColor;
			menulbl.defaultTextFormat = tf ;
			menulbl.text = _txt;
			
		}
		
	}

}