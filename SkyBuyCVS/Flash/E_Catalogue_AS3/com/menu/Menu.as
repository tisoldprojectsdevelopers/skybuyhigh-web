﻿package com.menu 
{
	import com.menu.Event.MenuEvent;
	import com.utils.CustomTrans;
	import fl.transitions.TweenEvent;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.transitions.Tween;
	import fl.transitions.easing.*
	import flash.utils.Timer;
	//Custom Class
	import com.mylocale.Myxml;
	
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Menu extends MovieClip
	{
		private var dropDown:MovieClip;
		private var menuCont:MenuContainer 
		private var footer:Footer;
		//
		private var menuItemArr:Array;
		//
		private var noOfDisplayItems:int = 5;
		private var menuItemHeight:Number;
		private var currMenuItemY:Number;
		private var menuHeight:Number;
		//
		private var tween:Tween;
		private var tween1:Tween;
		private var isTween:Boolean;
		private var isTween1:Boolean;
		//
		public var currentObj:Object
		//
		public function Menu() {
			
		}
		//
		public function addMenu(aType:String, seqId:int, needAll:Boolean):void {
			var needAllMenuItem:Boolean = needAll;
			try {
				menuItemArr = new Array();
				menuItemArr = Myxml.getInstance().getMenuObject(aType).slice(0, Myxml.getInstance().getMenuObject(aType).length);
				var noOfmenuItems:int = menuItemArr.length;
				needAllMenuItem = noOfmenuItems <= 1 ? false:true; 
				noOfDisplayItems  = noOfmenuItems <= noOfDisplayItems ? noOfmenuItems:noOfDisplayItems;
				if (needAllMenuItem) {
					var obj:Object = {OwnerId:null, OwnerName:'All', CategoryId:seqId, ProductId:noOfmenuItems}
					menuItemArr.unshift(obj);
				}
				createMenu();
				setMenuItemMask();
				attachFooter();
				setDropDownMask();
				showDropDownTween();
				if (noOfmenuItems <= noOfDisplayItems ) {
					footer.next.mouseEnabled = false;
					footer.prev.mouseEnabled = false;
					footer.next.alpha = 0.5;
					footer.prev.alpha = 0.5;
				}
				
			}
			catch (err:Error)
			{
				throw err;
			}
			
		}
		//Create Menu
		private function createMenu():void {
			var yy = 2;
			dropDown = new MovieClip();
			menuCont = new MenuContainer();
			dropDown.addChild(menuCont);
			addChild(dropDown);
			for (var i in menuItemArr) {
				var menuItem:MenuItem = new MenuItem();
				menuItem.currentObj = menuItemArr[i]
				menuItem._txt = menuItemArr[i].OwnerName;
				menuItem.buttonMode = true;
				menuItem.addEventListener(MenuEvent.MENU_ITEM_SELECT, hdlrMenuItemSelect);
				menuItem.y = yy;
				yy = menuItem.y + menuItem.height+3;
				//
				var seprator:Separator = new Separator();
				seprator.x = 2;
				seprator.y = menuItem.height;
				menuItem.addChild(seprator);
				//
				menuCont.addChild(menuItem);
			}
			menuItemHeight = menuItem.height + 3;
			menuCont.container.height = yy+3;					
		}
		//Attach the scrollong Arrow
		private function attachFooter():void {
			footer = new Footer();
			footer.y = menuItemHeight * noOfDisplayItems +3;
			footer.prev.buttonMode = true;
			footer.next.buttonMode = true;
			footer.prev.alpha = 0.75;
			footer.prev.addEventListener(MouseEvent.ROLL_OVER, hdlrScrolPrevOver);
			footer.next.addEventListener(MouseEvent.ROLL_OVER, hdlrScrolNextOver);
			footer.prev.addEventListener(MouseEvent.ROLL_OUT, hdlrScrolPrevOut);
			footer.next.addEventListener(MouseEvent.ROLL_OUT, hdlrScrolNextOut);
			dropDown.addChild(footer);
			dropDown.swapChildren(menuCont, footer);
			menuHeight = footer.y + footer.height;
		}
		//
		private function hdlrMenuItemSelect(evt:Event):void {
			currentObj = evt.currentTarget.currentObj;
			dispatchEvent(new MouseEvent('menuItemSelect'));
			
		}
		//show number of Items in menu
		private function getMaskObj(_wid:Number,_hei:Number):Sprite{
			var mcMask:Sprite = new Sprite();
			mcMask.graphics.beginFill(0x000000, 1);
			mcMask.graphics.drawRect(0, 0, _wid, _hei); 
			mcMask.graphics.endFill();
			return mcMask;
		}
		//
		private function setMenuItemMask():void {
			var _menuItemMask:Sprite = getMaskObj(this.width, menuItemHeight * noOfDisplayItems +2);
			dropDown.addChild(_menuItemMask);
			menuCont.mask = _menuItemMask;
		}
		//
		private function setDropDownMask():void {
			var _dropDownMask:Sprite = getMaskObj(this.width, menuHeight);
			addChild(_dropDownMask);
			dropDown.mask = _dropDownMask;
		}
		//Footer Previous buton down handler
		private function hdlrScrolPrevOver(evt:MouseEvent):void {
			evt.target.gotoAndStop(2);
			isTween1 = true;
			showPrevMenuItem();	
		}
		//Footer Next button down handler
		private function hdlrScrolNextOver(evt:MouseEvent):void {
			evt.target.gotoAndStop(2);
			isTween = true;
			showNextMenuItem();
		}
		//Previous btn rollOut handler
		private function hdlrScrolPrevOut(evt:MouseEvent):void {
			evt.target.gotoAndStop(1);
			isTween1 = false;
		}
		//Next btn rollOut handler
		private function hdlrScrolNextOut(evt:MouseEvent):void {
			evt.target.gotoAndStop(1);
			isTween = false;
		}
		//show Drop down tween
		private function showDropDownTween():void {
			dropDown.y = - menuHeight;
			var tween:CustomTrans = new CustomTrans(dropDown, 'y', dropDown.y, 0, 3);
			tween.frameRate = 30
			tween.startTween();
			/*var tween:Tween = new Tween(dropDown, 'y', Strong.easeOut, dropDown.y, 0, 1.8, true);
			tween.start();*/
		}
		//Tween Next item
		private function showNextMenuItem():void {
			var aFrom:Number = menuCont.y;
			var aTo:Number = menuCont.y - menuItemHeight;
			if (menuCont.y + menuCont.height - menuItemHeight > footer.y && isTween) {
				tween = new Tween(menuCont, 'y', Strong.easeOut, aFrom, aTo, 0.8, true);
				tween.addEventListener(TweenEvent.MOTION_FINISH, hdlrNextTweenComplete);
				tween.start();
				footer.prev.mouseEnabled = false;
			}
		}
		//Tween Previous item
		private function showPrevMenuItem():void {
			try {
				var aFrom:Number = menuCont.y;
				var aTo:Number =  menuCont.y + menuItemHeight;
				aTo = aTo > 0 ? 0:aTo;
				if (menuCont.y < 0 && isTween1) {
					tween1 = new Tween(menuCont, 'y', Strong.easeOut, aFrom, aTo, 0.8, true);
					tween1.addEventListener(TweenEvent.MOTION_FINISH, hdlrPrevTweenComplete);
					tween1.start();
					footer.next.mouseEnabled = false;
				}else {
					//tween1.removeEventListener(TweenEvent.MOTION_FINISH, hdlrPrevTweenComplete)
				}
			}catch (error:Error) {
				trace('Error in Previous next Item');
			}
		}
		//Tween complete - Next handler
		private function hdlrNextTweenComplete(evt:TweenEvent):void {
			footer.prev.mouseEnabled = true;
			if (isTween) {
				showNextMenuItem();
			}else {
				tween.removeEventListener(TweenEvent.MOTION_FINISH, hdlrNextTweenComplete);
			}
		}
		//Tween complete - Previous handler
		private function hdlrPrevTweenComplete(evt:TweenEvent):void {
			footer.next.mouseEnabled = true;
			if (isTween1) {
				showPrevMenuItem();
			}else {
				tween1.removeEventListener(TweenEvent.MOTION_FINISH, hdlrPrevTweenComplete);
			}
		}
		//Kill the Menu
		public function killMe():void {
			isTween = false;
			isTween1 = false;
			if (parent.contains(this)) {
				parent.removeChild(this);
			}
		}
	}

}