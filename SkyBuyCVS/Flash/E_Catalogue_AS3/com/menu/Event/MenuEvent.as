﻿package com.menu.Event 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class MenuEvent extends Event
	{
		public static const MENU_ITEM_SELECT:String = 'menuItemSelect';
		public var currentMenuObj:Object;
		public function MenuEvent(type:String, bubble:Boolean, cancelable:Boolean, obj:Object) 
		{
			this.currentMenuObj = obj;
			super(type, bubble, cancelable);
		}
		
	}

}