﻿package com.utils 
{
	import fl.controls.Button;
	import flash.events.Event;
	import flash.display.SimpleButton;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Close extends Button
	{
		
		public function Close() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		
	}

}