﻿package com.utils 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.SimpleButton;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class DeletePopup extends MovieClip
	{
	
		public function DeletePopup() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			btnOk.addEventListener(MouseEvent.MOUSE_DOWN, hdlrOk);
			btnCancel.addEventListener(MouseEvent.MOUSE_DOWN, hdlrCancel);
		}
		private function hdlrOk(evt:MouseEvent):void {
			dispatchEvent(new MouseEvent('ok'));
		}
		private function hdlrCancel(evt:MouseEvent):void {
			dispatchEvent(new MouseEvent('cancel'));
		}
	}

}