﻿package com.utils 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ViewMoreContainer extends MovieClip
	{
		public var productObj:Object;
		public function ViewMoreContainer() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			//
			buttonMode = true;
			mouseChildren = false;
			addEventListener(MouseEvent.MOUSE_DOWN, hdlrMouseDown);
			addEventListener(MouseEvent.ROLL_OVER, hdlrMouseOver);
			addEventListener(MouseEvent.ROLL_OUT, hdlrMouseOut);
		}
		
		//
		private function hdlrMouseDown(evt:MouseEvent):void {
			dispatchEvent(new MouseEvent('thumbDown', true, false) );
		}
		//
		private function hdlrMouseOver(evt:MouseEvent):void {
			evt.currentTarget.gotoAndPlay('open');
		}
		//
		private function hdlrMouseOut(evt:MouseEvent):void {
			evt.currentTarget.gotoAndPlay('close');
		}
		
		
	}

}