﻿package com.utils 
{
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.Event
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class CustomTween extends EventDispatcher
	{
		private var target:MovieClip;
		private var type:String;
		private var from:Number;
		private var to:Number;
		//
		private var tfTimer:Timer
		//
		public static const TWEEN_COMPLETE:String = 'tweenComplete';
		
		public function CustomTween(aTarget:MovieClip, aType:String, aFrom:Number, aTo:Number, aSec:Number, bSec:Boolean= false) 
		{
			target = aTarget;
			type = aType;
			from = aFrom;
			to = aTo;
			var time:Number = bSec ? aSec * 50 : 1;
			tfTimer = new Timer(time);
			tfTimer.addEventListener(TimerEvent.TIMER, hdlrStartTimer);
		}
		public function start():void {
			tfTimer.start();
		}
		public function stop():void {
			tfTimer.stop();
		}
		private function hdlrStartTimer(evt:TimerEvent):void {
			switch(type) {
				case 'alpha':
					doAlphaTrans();
					break;
				case 'x':
					doXaxisTrans();
					break;
				case 'y':
					doYaxisTrans();
					break;
				
			}
		}
		private function doAlphaTrans():void {
			var incOrDec:int = from > to  ? -1: 1;
			target.alpha = target.alpha + (incOrDec * 0.2);
			if (target.alpha >= to && incOrDec == 1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}else if (target.alpha <= to && incOrDec == -1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}
		}
		private function doXaxisTrans():void  {
			var incOrDec:int = from > to  ? -1: 1;
			target.x = target.x + (incOrDec * 20);
			if (target.x >= to && incOrDec == 1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}else if (target.x <= to && incOrDec == -1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}
		}
		private function doYaxisTrans():void  {
			var incOrDec:int = from > to  ? -1: 1;
			target.y = target.y + (incOrDec * 20);
			if (target.y >= to && incOrDec == 1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}else if (target.y <= to && incOrDec == -1) {
				stop();
				dispatchEvent(new Event('tweenComplete'));
			}
		}
	}
}