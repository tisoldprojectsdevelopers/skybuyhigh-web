﻿package com.utils 
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.utils.ScrollMc;
	import flash.text.TextFieldAutoSize
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ReturnPolicyPopup extends MovieClip
	{
		private var str:String;
		private var descScroller1:ScrollMc;
		public function ReturnPolicyPopup() 
		{
			addEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
		}
		private function hdlrObjAdded(evt:Event ):void {
			removeEventListener(Event.ADDED_TO_STAGE, hdlrObjAdded);
			btnClose.addEventListener(MouseEvent.MOUSE_DOWN, hdlrClose);
		}
		public function showHeader(aStr:String):void {
			rtLable.htmlText = aStr;
		}
		public function showText (aStr:String ):void {
			str = aStr;
			displayText();
		}
		private function displayText():void {
			TextField(myText._txt).wordWrap = true;
			TextField(myText._txt).multiline = true;
			TextField(myText._txt).autoSize = TextFieldAutoSize.LEFT;
			myText._txt.htmlText = str;
			descScroller1 = new ScrollMc(myText, 321);
			descScroller1.refreshScroll();
			addChild(descScroller1);
		}
		private function hdlrClose(evt:MouseEvent):void {
			parent.removeChild(this);
		}
	}
}