﻿package com.utils 
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.errors.IOError
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class ImageLoader extends MovieClip
	{
		private var loading:McLoding;
		public var container:MovieClip;
		private var url:String
		private var ldr:Loader;
		private var maskSp:Shape
		//
		var _width:Number;
		var _height:Number;
		var resize:Boolean;
		/*
		 *  @aPath : image url
		 * */
		public function ImageLoading() {
			
		}
		public function LoadImage(aPath:String, aResize:Boolean = false):void {
			//Assigning Arguments
			url = aPath;
			resize = aResize;
			//Geting current Container Size
			this._height = height;
			this._width = width;
			//Setting mask
			if (resize) {
				setMask();
			}
			//
			killLoader();
			container = new MovieClip();
			this.addChild(container);
			//
			ldr = new Loader();
			ldr.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, hdlrIOError);
			ldr.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, hdlrProgress);
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, hdlrLoadComplete);
			//
			try {
				ldr.load(new URLRequest(aPath));
			}catch (ioError:IOError) {
				trace(ioError + 'Error in Image Loading');
			} catch (securityError:SecurityError) {
				trace(securityError + 'Error in Image Loading');
			} catch (error:Error) {
				trace(error + 'Error in Image Loading');
			}
			container.addChild(ldr);
			//
			loading = new McLoding();
			loading.x = this.width / 2 ;
			loading.y = this.height / 2;
			this.addChild(loading);
			setChildIndex(loading, this.numChildren - 1);
			//
		}
		//
		private function hdlrIOError(evt:IOErrorEvent):void {
			trace('Error in file url', url);
		}
		//Handler Loading progress
		private function hdlrProgress(evt:ProgressEvent):void {
			
		}
		//Image Load Complete
		private function hdlrLoadComplete(evt:Event):void {
			if (loading) {
				this.removeChild(loading);
				loading = null;
			}
			if (resize) {
				fitImage();
				setChildIndex(maskSp, this.numChildren - 1);
			}
			
			
			dispatchEvent(new Event('loadComplete'));
		}
		//
		private function setMask():void {
			if (maskSp) {
				removeChild(maskSp);
			}
			maskSp = new Shape();
			maskSp.graphics.beginFill(0x400000, 1);
			maskSp.graphics.drawRect(0, 0, width, height);
			maskSp.graphics.endFill();
			addChild(maskSp);
			mask = maskSp;
		}
		//StopLoading
		public function stopLoading():void {
			if (loading) {
				ldr.unload();
			}
		}
		//
		public function killLoader():void {
			try {
				if (loading) {
					if (this.contains(loading)) {
						removeChild(loading);
					}
				}
				if (container) {
					if (this.contains(container)) {
						removeChild(container);
					}
				}
			}catch (err:Error) {
				trace('Error in Kill Loader', err);
			}
		}
		//
		private function fitImage():void {
			if (container.width > this._width || container.height > this._height) {
				while (container.width > this._width || container.height > this._height) {
					container.scaleX -= 0.01;
					container.scaleY -= 0.01;
				}
			}
		}
		//
		
	}

}