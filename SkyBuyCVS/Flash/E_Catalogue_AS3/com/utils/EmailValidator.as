﻿package com.utils 
{
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class EmailValidator
	{
		private var ASCII_ALLOWED:Array	= [[38,39],[42,43],[45,57],[61,61],[63,63],[65,90],[94,95],[97,123],[125,126]];
		private var i:Number;
		private var j:Number;
		private var email_arr:Array;
		private var after_arr:Array;
		private var str_len:Number;
		
		public function validate(inputString:String):Boolean{
			//
			if(inputString.length<1)return false;
			//@
			email_arr	= inputString.split("@");
			if(email_arr.length!=2) return false; //	Only one @ is allowed
			//
			after_arr	= email_arr[1].split(".");
			if(after_arr.length<2) return false;
			if(after_arr[after_arr.length-1].length<2 || after_arr[after_arr.length-1].length>4)
				return false; //Only one . is allowed and after the . you can have between 2 and 4 chars
			//
			if(!ValidateString(email_arr[0].toString())||!ValidateString(email_arr[1].toString()))
				return false;	//the email contains invalid characters
			//
			return true;	//this is a valid email
		}
		//
		public function ValidateString(_str:String):Boolean{
			str_len	= _str.length;
			//
			for(i=0;i<str_len;i++)
				if(!CheckUsedCharacter(_str.charCodeAt(i)))
					return false;// the strin contains atleast one invalid chr
			//
			return true;// all chars are good
		}
		//
		public function CheckUsedCharacter(_chr:Number):Boolean {
			for(j=0;j<ASCII_ALLOWED.length;j++)
				if(_chr>=ASCII_ALLOWED[j][0] && _chr<=ASCII_ALLOWED[j][1])
					return true;	// the caracter falls into one of the allowed areas
			//
			return false;	// The used character is not allowed.
		}
	}
}