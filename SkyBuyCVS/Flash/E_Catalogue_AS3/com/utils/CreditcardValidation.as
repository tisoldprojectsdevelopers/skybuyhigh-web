﻿package com.utils 
{
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class CreditcardValidation
	{
		public function isValidExpDate(month:Number,year:Number):Boolean {
			var result:Boolean = true;
			var now:Date = new Date();
			var nowMonth:Number = now.getMonth() + 1;
			var nowYear:Number = now.getFullYear();
			if ((nowYear > year) || ((nowYear == year ) && (nowMonth > month))){
				result = false;
			} 
			return result;
	    }
		/*
		* checks for valid credit card format using the Luhn check and known digit
		* about various cards
		* @ccType a string indicating the entered credit card name
		* @ccNum a string indicating the credit card number being used.
		*/
		public function isValidCreditCardNumber(ccType:String,ccNum:String):Boolean {
			var result:Boolean = true;
			if (ccNum.length>0){
				if (isNaN(Number(ccNum))){
					result = false;
				}
				if (result){
					if (!luhnCheck(ccNum) || !validateCCNum(ccType,ccNum)){
						result = false;
					}
				}
			}
			return result;
		}
		//
		private function luhnCheck(str:String){
			var result:Boolean = true;
			var sum:Number = 0;
			var mul:Number = 1;
			var strLen:Number = str.length;
			for (var i:Number = 0; i < strLen; i++){
				var digit:String = str.substring(strLen-i-1,strLen-i);
				var tproduct:Number = parseInt(digit ,10)*mul;
				if (tproduct >= 10){
					sum += (tproduct % 10) + 1;
				} else {
					sum += tproduct;
				}
				if (mul == 1){
					mul++;
				} else {
					mul--;
				}
			}
			if ((sum % 10) != 0){
				result = false;
			}
			return result;
		}
		private function validateCCNum(cardType:String,cardNum:String):Boolean{
			var result:Boolean = false;
			cardType = cardType.toUpperCase();
			var cardLen:Number = cardNum.length;
			var firstdig:String = cardNum.substring(0,1);
			var seconddig:String = cardNum.substring(1,2);
			var first4digs:String = cardNum.substring(0, 4);
			var validNums:String; 
			switch (cardType){
				case "VISA":
					result = ((cardLen == 16) || (cardLen == 13)) && (firstdig == "4");
					break;
				case "AMEX":
					validNums = "47";
					result = (cardLen == 15) && (firstdig == "3") && (validNums.indexOf(seconddig)>=0);
				break;
			case "MASTERCARD":
				validNums = "12345";
				result = (cardLen == 16) && (firstdig == "5") && (validNums.indexOf(seconddig)>=0);
				break;
			case "DISCOVER":
				result = (cardLen == 16) && (first4digs == "6011");
				break;
			case "DINERS":
				validNums = "068";
				result = (cardLen == 14) && (firstdig == "3") && (validNums.indexOf(seconddig)>=0);
				break;
			}
			return result;
		}
	}
}