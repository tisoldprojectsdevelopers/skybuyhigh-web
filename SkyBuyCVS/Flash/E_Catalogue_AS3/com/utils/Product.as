﻿package com.utils 
{
	import fl.controls.ComboBox;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent
	import flash.events.TextEvent;
	import flash.geom.Point;
	
	import flash.text.*;
	import com.mylocale.Myxml;
	import com.utils.calendarSkin;
	/**
	 * ...
	 * @author Karthikeyan.J.R
	 */
	public class Product extends MovieClip
	{
		public static const ITEM_ADDED:String = 'added';
		public static const ITEM_POLICY:String = 'policy';
		public static const ITEMCOUNT_CHANGED:String = 'itemcount_changed';
		public static const ITEM_REMOVE:String = 'remove_item' ;
		//
		public var product:Object;
		public var thisparentX:Number
		public var thisparentY:Number
		public var returnPolicyHeader:String;
		//
		private var thumb:* 
		//
		private var prodid:ProdId;
		private var prodname:ProdName;
		private var prodtitle:ProdTitle;
		private var prodes:ProdDes;
		private var prodcol:ProductColor;
		private var prodsize:ProdSize;
		private var prodpolicy:ProdPolicy;
		private var prodprize:ProdPrize;
		private var prodqty:ProdQuantity;
		private var prodtot:ProdTotal;
		private var prodremove:ProdRemove;
		private var proddate:ProdDate;
		private var datapicker:calendarSkin;
		//
		private var yy:Number = 0;
		public var _height:Number = 0;
		private var tf:TextFormat
		//
		var lastNoQuantiy:int = 1;
		public var isActive:Boolean = true;
	
		public function Product() 
		{
			
		}
		
		public function hideDateIns():void {
			try {
				if (datapicker) {
					if (parent.parent.contains(datapicker)) {
						datapicker.killMe();
					}
				}
				
			}catch (err:Error) {
				trace('hideDateIns', err);
			}
		}
		public function Initialize(aObj:Object):void {
			tf = new TextFormat();
			tf.size = 14;
			product = aObj;
			if (product != null) {
				arrangeproductId();
				loadThumbImg();
				if (product.reffObj.SeqId == '5') {
					assingTextType1();
				}else {
					assignText();
				}
				drawLines();
				assignQty();
				assignTot();
				addRemove();
			}
		}
		//Load the thumb image
		private function loadThumbImg():void {
			thumb = new Loader_Thumb();
			thumb.width = 121;
			thumb.height = 113;
			if (product.reffObj.MainThumbImage != null) {
				thumb.LoadImage(product.reffObj.MainThumbImage, true);
			}else  {
				var tmp:Shape = new Shape();
				tmp.graphics.beginFill(0x000000, 0);
				tmp.graphics.drawRect(0, 0, 121, 113);
				tmp.graphics.endFill();
				thumb.addChild(tmp);
			}
			thumb.x = 5;
			thumb.y = 5;
			addChild(thumb)
		}
		//Arrange Product Id
		private function arrangeproductId():void {
			prodid = new ProdId();
			prodid._txt.htmlText = product.reffObj.ProdCode != null  ? product.reffObj.ProdCode:'';
			//Assining Product id
			prodid.x = 5;
			prodid.y = 118+2;
			addChild(prodid);
		}
		//Initiate Texts
		private function assignText():void {
			prodname = new ProdName();
			prodtitle = new ProdTitle();
			prodes = new ProdDes();
			prodes._txt.autoSize = TextFieldAutoSize.LEFT;
			prodcol = new ProductColor();
			prodsize = new ProdSize();
			prodpolicy = new ProdPolicy();
			prodprize = new ProdPrize();
			returnPolicyHeader = 'Return Policy';
			try {
				prodname._txt.htmlText = product.reffObj.ProdBrand != null ? product.reffObj.ProdBrand:'';
				prodtitle._txt.htmlText = product.reffObj.ProdTitle != null ? product.reffObj.ProdTitle:'';
				prodes._txt.htmlText = product.reffObj.ProdSD != null  ? product.reffObj.ProdSD:'';
				prodpolicy._txt.htmlText = 'Click here to view Return Policy';
				prodprize._txt.htmlText = product.reffObj.SkybuyPrice != null ? product.reffObj.SkybuyPrice:'';
			}catch (err:Error) {
				trace('Error in Product Text Assining ');
			}
			//Assining Product name
			prodname.x = 145;
			prodname.y = 5;
			addChild(prodname);
			//Assigning Product title
			prodtitle.x = 145;
			prodtitle.y = prodtitle.y + prodtitle.height + 5;
			addChild(prodtitle);
			//Assigning Product description
			prodes.x = 145;
			prodes.y = prodtitle.y + prodtitle.y +5;
			addChild(prodes);
			//Creat ComboBox Color Combobox
			yy = prodes.y + prodes.height;
			if (product.reffObj.ProdColor != null) {
				var cbColor:ComboBox = new ComboBox();
				cbColor.setStyle("textFormat", tf);
				for (var obj in product.reffObj.ProdColor.split(',')) {
					cbColor.addItem( { label:Myxml.getInstance().Trim(product.reffObj.ProdColor.split(',')[obj]) } );
				}
				cbColor.addEventListener(Event.CHANGE, hdlrColorChange);
				cbColor.width = 150;
				cbColor.x = 45;
				cbColor.y = -2;
				prodcol.addChild(cbColor);
				addChild(prodcol);
				prodcol.x = 145;
				prodcol.y = yy  + 20;
				yy = prodcol.y + 25; 
				cbColor.selectedIndex = product.ProdColorIndex;
			}
			if (product.reffObj.ProdSize != null) {
				var cbSize:ComboBox = new ComboBox();
				cbSize.setStyle("textFormat", tf);
				for (var obj1 in product.reffObj.ProdSize.split(',')) {
					cbSize.addItem( { label:Myxml.getInstance().Trim(product.reffObj.ProdSize.split(',')[obj1] )} );
				}
				cbSize.addEventListener(Event.CHANGE, hdlrSizeChange);
				cbSize.width = 150;
				cbSize.x = 45;
				cbSize.y = -2;
				prodsize.x = 145;
				prodsize.y = yy + 10;
				yy = prodsize.y + 25
				prodsize.addChild(cbSize);
				addChild(prodsize);
				cbSize.selectedIndex =  product.ProdSizeIndex;
			}
			
			//Assigning Product prize
			yy = yy + 30;
			_height = yy > prodid.y + prodid.height + 2 ? yy : prodid.y + prodid.height + 2;
			prodprize.x = 542;
			prodprize.y = _height / 2 - 20;
			addChild(prodprize);
			//
			if (product.reffObj.ReturnPolicy != null) {
				prodpolicy.buttonMode = true;
				prodpolicy.mouseChildren = false;
				prodpolicy.addEventListener(MouseEvent.MOUSE_DOWN, hdlrshowPolicyMsg);
				prodpolicy.x = prodname.x;
				prodpolicy.y = _height-prodpolicy.height + 2;
				addChild(prodpolicy);
			}
		}
		//-----------------------------------------------------------
		//Airline Advertisment package
		//-----------------------------------------------------------
		private function assingTextType1():void {
			prodtitle = new ProdTitle();
			prodes = new ProdDes();
			prodes._txt.autoSize = TextFieldAutoSize.LEFT;
			prodpolicy = new ProdPolicy();
			prodprize = new ProdPrize();
			proddate = new ProdDate();
			returnPolicyHeader = 'Restrictions/Validity/Cancellation';
			try {
				prodtitle._txt.htmlText = product.reffObj.ProdTitle;
				prodes._txt.htmlText = product.reffObj.ProdSD;
				prodprize._txt.htmlText = product.reffObj.SkybuyPrice;
				prodpolicy._txt.text = 'Click here to view Restriction / Validity / Cancellation';
				proddate._txt.text = product.ProdDate;
			}catch (err:Error) {
				trace(err);
			}
			//Assining Product title
			prodtitle.x = 145;
			prodtitle.y = 5;
			addChild(prodtitle);
			//Assigning Product title
			prodes.x = prodtitle.x;
			prodes.y = prodtitle.y + prodtitle.height + 5;
			addChild(prodes);
			//Assigning Product Date
			proddate.x = prodes.x ;
			proddate.y = prodes.y + prodes.height + 20;
			proddate.dateChoser.addEventListener(MouseEvent.MOUSE_DOWN, showDatePicker);
			proddate.addEventListener(Event.ADDED_TO_STAGE, hdlrProdDateAdded);
			addChild(proddate);
			
			//
			yy = proddate.y + proddate.height +30;
			//
			_height = yy > prodid.y + prodid.height + 2 ? yy : prodid.y + prodid.height + 2;
			//Assigning Product Prize
			prodprize.x = 542;
			prodprize.y = _height / 2 - 10;
			
			if (product.reffObj.ReturnPolicy != null) {
				prodpolicy.buttonMode = true;
				prodpolicy.mouseChildren = false;
				prodpolicy.addEventListener(MouseEvent.MOUSE_DOWN, hdlrshowPolicyMsg);
				prodpolicy.x = prodtitle.x;
				prodpolicy.y = _height-proddate.height - 2;
				addChild(prodpolicy);
			}
			addChild(prodprize);
		}
		//
		private function hdlrProdDateAdded(evt:Event):void {
			proddate.removeEventListener(Event.ADDED_TO_STAGE, hdlrProdDateAdded);
			validateDateOnLoad();
		}
		//Show and Hide date Picker
		private function showDatePicker(evt:MouseEvent):void {
			datapicker = new calendarSkin();
			datapicker.Construct();
			datapicker.addEventListener(calendarSkin.DATE_CHANGE, hdlrDateChange);
			datapicker.addEventListener(MouseEvent.ROLL_OUT, hdlrDateRollOut);
			datapicker.x = thisparentX+proddate.x + proddate.dateChoser.x;
			datapicker.y = thisparentY+proddate.y + proddate.dateChoser.y;
			parent.parent.addChild(datapicker);
		}
		//
		private function hdlrDateRollOut(evt:MouseEvent):void {
			try {
				if (parent.parent.contains(datapicker)) {
					datapicker.killMe();
				}
			}catch (err:Error) {
				trace('Error in RollOut', err);
			}
		}
		//Change the data - data Picker
		private function hdlrDateChange(evt:Event):void { 
			var givDate:Date = new Date(Number(evt.currentTarget.currentyear), Number(evt.currentTarget.currentMonthNumber), Number(evt.currentTarget.currentDay), 00,00,00,00);
			var curDate:Date = new Date();
			curDate.setHours(00);
			curDate.setMilliseconds(00);
			curDate.setSeconds(00);
			curDate.setMinutes(00);
			proddate._txt.text = String(evt.currentTarget.currentDay) + ' ' + String(evt.currentTarget.currentMonth).substr(0, 3) + ' ' + String(evt.currentTarget.currentyear);
			product.ProdDate = proddate._txt.text;
			if (curDate.valueOf() <= givDate.valueOf()) {
				isActive = true;
				proddate.invalidTravelDt.visible = false;
			}else {
				isActive = false;
				proddate.invalidTravelDt.visible = true;
			}
			try {
				datapicker.killMe();
			}catch (err:Error) {
				trace('Error in Date changer', err);
			}
		}
		//Validate the current date on Loading
		private function validateDateOnLoad():void {
			var tmp:String = product.ProdDate.split(' ')[1];
			var monthArr:Array = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
			var monthIndex:Number = 0;
			for (var aa in monthArr) {
				if (monthArr[aa].toLowerCase() == tmp.toLowerCase()) {
					monthIndex = aa;
					break;
				}
			}
			var year:Number = Number(product.ProdDate.split(' ')[2]);
			var dt:Number = Number(product.ProdDate.split(' ')[0]);
			var givDate:Date = new Date(year, monthIndex, dt, 00,00,00,00);
			var curDate:Date = new Date();
			curDate.setHours(00);
			curDate.setMilliseconds(00);
			curDate.setSeconds(00);
			curDate.setMinutes(00);
			if (curDate.valueOf() <= givDate.valueOf()) {
				isActive = true;
				proddate.invalidTravelDt.visible = false;
			}else {
				isActive = false;
				proddate.invalidTravelDt.visible = true;
				trace(proddate.invalidTravelDt.visible);
			}
		}
		//Combobox Color change - Chage Event
		private function hdlrColorChange(evt:Event):void {
			product.ProdColorIndex = evt.target.selectedIndex;
		}
		//Combobox Size change - Chage Event
		private function hdlrSizeChange(evt:Event):void {
			product.ProdSizeIndex = evt.target.selectedIndex;
		}
		//Draw border and center lines
		private function drawLines():void {
			var tmp:Array = new Array(134, 541, 631.5, 699.5, 797.2)
			for (var i in tmp) {
				var line1:Shape = new Shape();
				line1.graphics.lineStyle(1, 0x999999, 0.7); 
				line1.graphics.moveTo(tmp[i], 4);
				line1.graphics.lineTo(tmp[i], _height-4 );
				addChild(line1)
			}
			var line2:Shape = new Shape();
			line2.graphics.lineStyle(1, 0x999999, 0.7); 
			line2.graphics.moveTo(0, _height);
			line2.graphics.lineTo(890, _height);
			addChild(line2);
			dispatchEvent(new Event('added', true, true));
		}
		//Add Quantity Text box
		private function assignQty():void {
			prodqty = new ProdQuantity();
			prodqty._txt.border = true;
			prodqty._txt.borderColor = 0xE0E0E0;
			lastNoQuantiy = Number(product.Quantity);
			prodqty._txt.htmlText = product.Quantity;
			prodqty._txt.addEventListener(Event.CHANGE, quantityChange);
			prodqty._txt.addEventListener(Event.MOUSE_LEAVE, quantityChange);
			prodqty.mcUpdate.buttonMode = true;
			prodqty.mcUpdate.addEventListener(MouseEvent.MOUSE_DOWN, quantityChange);
			prodqty._txt.restrict = '0-9';
			prodqty._txt.maxChars = 2;
			prodqty.x = 632.5;
			prodqty.y = _height / 2 - 30;
			addChild(prodqty);
		}
		//Quantiy onChange Event 
		private function quantityChange(evt:Event):void {
			if (prodqty._txt.length > 0) {
				product.Quantity = int(prodqty._txt.text) == 0 ? product.Quantity : int(prodqty._txt.text); 
				lastNoQuantiy = int(product.Quantity);
				prodtot._txt.htmlText = '$' + int(product.reffObj.SkybuyPrice.split('$')[1]) * product.Quantity;
				prodtot._txt.htmlText = Myxml.getInstance().formatAsDollars(prodtot._txt.text);
			}else {
				prodqty._txt.htmlText = String(lastNoQuantiy);
			}
			if (Number(prodqty._txt.text)== 0) {
				prodqty._txt.htmlText = String(lastNoQuantiy);
			}
			dispatchEvent(new Event('itemcount_changed'));
		}
		
		//Add Total Text box
		private function assignTot():void {
			prodtot = new ProdTotal();
			prodtot.x = 700.5
			prodtot.y = _height / 2 - 20; 
			prodtot._txt.htmlText = '$' + Number(product.reffObj.SkybuyPrice.split('$')[1]) * product.Quantity;
			prodtot._txt.htmlText = Myxml.getInstance().formatAsDollars(prodtot._txt.text);
			addChild(prodtot);
		}
		private function addRemove():void {
			prodremove = new ProdRemove();
			prodremove.x = 798.2
			prodremove.y = _height / 2 - 30;
			prodremove.addEventListener(MouseEvent.MOUSE_DOWN, removeProduct);
			prodremove.buttonMode = true;
			addChild(prodremove);
		}
		//MouseDown Handler viwe Policy file details
		private function hdlrshowPolicyMsg(evt:MouseEvent):void {
			dispatchEvent(new MouseEvent('policy'));
		}
		//Dispatch the Event when the product was removed
		private function removeProduct(evt:MouseEvent):void {
			dispatchEvent(new Event('remove_item'));
		}
		//Dispatch the Event when the product count changed
		private function itemCountChanged():void {
			dispatchEvent(new Event('itemcount_changed'));
		}
		
		
	}

}
