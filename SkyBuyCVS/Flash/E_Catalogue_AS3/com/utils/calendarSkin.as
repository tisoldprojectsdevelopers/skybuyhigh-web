﻿//=======================================================================
//	[FREE AS3 DATE PICKER V2]
//	AUTHOR 			: NIDIN P VINAYAKAN
//	SCRIPT VERSION 	: AS3
//	DATE 			: 2009 AUGUST
//
//=======================================================================
package com.utils {
 	import flash.display.*;
	import flash.geom.*;
	import flash.text.*;
	import flash.events.*;
	import flash.filters.ColorMatrixFilter;
 
	public  class calendarSkin extends MovieClip {
 
		public static const DATE_CHANGE:String = 'data_change';
		//
		public var currentDay:String;
		public var currentMonth:String;
		public var currentYear:String;
		public var currentMonthNumber:Number;
		//
		public var calendar_mc:Sprite = new Sprite();
		public var inited:Boolean	=	false;
		public var cellArray		:Array;
		public var isToday			:Boolean	=	false;
		public var Days				:Array;
		public var Months			:Array;
		public var DaysinMonth		:Array;
		public var prevDate			:*;
		public var today			:Date;
		public var todaysday		:*;
		public var currentyear		:*;
		public var currentmonth		:*;
		public var currentDateLabel:TextField;
		public var selectedDate		:*
		public var day_bg			:MovieClip;
		public var day_bg_color		:MovieClip;
		public var hit				:Sprite;
		public var day_txt			:TextField;
		//*********************************************************************
		//				 					COLOR VARIABLES
		//*********************************************************************
		public var backgroundColor			:Array		=	[0xFFFFFF,0xFFFFFF];
		public var backgroundStrokeColor	:*			=	0xC0C0C0;
		public var labelColor				:*			=	0x000000;
		public var buttonColor				:*			=	0x000000;
		public var DesabledCellColor		:*			=	0x999999;
		public var EnabledCellColor			:*			=	0xffffff;
		public var TodayCellColor			:*			=	0xACACAC;
		public var mouseOverCellColor		:*			=	0xC1FFC2;
		public var entryTextColor			:*			=	0x000000;
 
		public function calendarSkin() {
 
			super();
		}
		public function Construct() {
 
			addChild(calendar_mc);
			/*trace("*************************************************************");
			trace("#			[FREE AS3 DATE PICKER COMPONENT V2]				#");
			trace("#			AUTHOR 			: NIDIN P VINAYAKAN				#");
			trace("#			SCRIPT VERSION 	: AS3							#");
			trace("#			DATE 			: 2009 AUGUST					#");
			trace("#			WEBSITE 		: www.infogroupindia.com		#");
			trace("*************************************************************");*/
//==================================================================
//							DRAW CALENDAR BACKGROUND
//==================================================================
			var type					:String 	= 	GradientType.RADIAL;
			var colorArray				:Array 		= 	backgroundColor;
			var alphaArray				:Array 		=	[1,1];
			var ratioArray				:Array		=	[0, 255];
			var colorMatrix				:Matrix		=	new Matrix();
			var spreadMethod			:String 	= 	SpreadMethod.PAD;
			var interpolationMethod		:String		=	InterpolationMethod.LINEAR_RGB;
			var focalPointRatio			:Number		=	0;
			var bg						:Sprite 	= 	new Sprite();
			var bgStrokeColor			:*			=	backgroundStrokeColor;
			var bgStrokeThickness		:Number		=	1;
			var bgWidth					:Number		=	165;
			var bgHeight				:Number		=	178;
 
			colorMatrix.createGradientBox(bgWidth,bgHeight,0,0,0);
 
			bg.name 	= 	"background";
 
			bg.graphics.lineStyle(bgStrokeThickness,bgStrokeColor);
			bg.graphics.beginGradientFill(
			  type,
			  colorArray,
			  alphaArray,
			  ratioArray,
			  colorMatrix,
			  spreadMethod,
			  interpolationMethod,
			  focalPointRatio
			  );
 
			bg.graphics.drawRect(0,0,165,178);
			bg.graphics.endFill();
 
			calendar_mc.addChild(bg);
 
//=====================================================================
//							MAKE CALENDAR LABELS
//=====================================================================
				currentDateLabel		 	= 	new TextField();
				currentDateLabel.name 		= 	"currentDateLabel";
				currentDateLabel.autoSize	=	TextFieldAutoSize.CENTER;
				currentDateLabel.selectable =	false;
				currentDateLabel.width		=	66;
				currentDateLabel.y			=	6;
 
			var format:TextFormat 	= 	new TextFormat();
				format.font			=	"Tahoma";
				format.color		=	labelColor;
				format.size			=	11;
				format.bold			=	true;
 
			currentDateLabel.defaultTextFormat	=	format;
			currentDateLabel.text				=	"";
 
				format.letterSpacing 			=	14;
				var weekdisplay:Array			=	["MTWTFSS","SMTWTFS"]
				var weekname:TextField 			= 	new TextField();
				weekname.selectable				=	false;
				weekname.defaultTextFormat		=	format;
				weekname.text					=	weekdisplay[1];
				weekname.width					=	165;
				weekname.x						=	11;
				weekname.y 						=	23;
 
			calendar_mc.addChild(currentDateLabel);
			calendar_mc.addChild(weekname);
 
//=======================================================================
//							MAKE MONTH CHANGER BUTTONS
//=======================================================================
			var nextBtn:Sprite 	= 	makeBtn(90);
				nextBtn.name 	= 	"NextButton";
				nextBtn.x 		= 	160;
				nextBtn.y 		= 	11;
			var prevBtn:Sprite 	= 	makeBtn(270);
				prevBtn.name 	= 	"PrevButton";
				prevBtn.x 		= 	5;
				prevBtn.y 		=	18;
 
				nextBtn.buttonMode 	= 	true;
				prevBtn.buttonMode	=	true;
 
			nextBtn.addEventListener(MouseEvent.CLICK, clickHandler, false, 0, true);
			prevBtn.addEventListener(MouseEvent.CLICK, clickHandler, false, 0, true);
			//
			calendar_mc.addChild(nextBtn);
			calendar_mc.addChild(prevBtn);
			
//=================================================================
//							MAKE CALENDAR ENTRIES
//=================================================================
			Days			 = 	new Array();
			Months			 = 	new Array();
			DaysinMonth		 =	[31,28,31,30,31,30,31,31,30,31,30,31];
			prevDate		 =	undefined;
			today			 = 	new Date();
			todaysday		 =	today.getDay();
			currentyear		 =	today.getFullYear();
			currentmonth	 =	today.getMonth();
 
			Days.push("Monday");
			Days.push("Tuesday");
			Days.push("Wednesday");
			Days.push("Thursday");
			Days.push("Friday");
			Days.push("Saturday");
			Days.push("Sunday");
			Months.push("January");
			Months.push("February");
			Months.push("March");
			Months.push("April");
			Months.push("May");
			Months.push("June");
			Months.push("July");
			Months.push("August");
			Months.push("September");
			Months.push("October");
			Months.push("November");
			Months.push("December");
 
			currentDateLabel.text	=	Months[currentmonth]+" - "+currentyear;
 
			ConstructCalendar();
			/*-----------------------------------------------------------
			 * Construnct Dummy Label for RollOut Listener in Product Display
			 * Make -100, 0, 100, 21
			 *----------------------------------------------------------*/
			var sh:Shape = new Shape();
			sh.graphics.beginFill(0x400040, 0);
			sh.graphics.drawRect( -100, 0, 100, 21);
			sh.graphics.endFill();
			addChild(sh);
			
 
		}
		public function clickHandler(e:MouseEvent){
			switch (e.target.name) {
				case "PrevButton" :
					{
						changeMonth(-1);
						break;
					}
				case "NextButton" :
					{
						changeMonth(1);
						break;
					}
			}
			return;
 
		}
//===============================================================
//							MONTH CHANGER FUNCTION
//===============================================================
		public function changeMonth(monthNum:Number):void {
			if (monthNum!=1) {
				if (currentmonth > 0) {
					currentmonth = (currentmonth - 1);
				} else {
					changeYear(-1);
				}
			} else {
				if (currentmonth < 11) {
 					currentmonth = currentmonth + 1;
				} else {
 					changeYear(1);
				} 
			}
 			ConstructCalendar();
 			return;
		} 
		//=============================================================== 
		//							YEAR CHANGER FUNCTION 
		//===============================================================	 		
		public function changeYear(yearNum:Number):void { 			
			currentyear = currentyear + yearNum; 			
			if (yearNum != 1) { 
				currentmonth = 11;
			} else { 
				currentmonth = 0; 
			} 
			var yearDev:* = currentyear / 4;
 			yearDev = yearDev.toString();
 			var yearDevLength:* = yearDev.split(".").length;
 			if (yearDevLength != 1) {
				DaysinMonth[1] = 28; 
			} else { 
				DaysinMonth[1] = 29; 
			}
 			return; 		
		}
		//================================================================ 
		//							CALENDAR CONSTRUCTOR 
		//================================================================	 		
		public function ConstructCalendar() { 			
			if (inited) {
 				removeEntry();
			}
			var dateBox		:MovieClip; 
			cellArray				= 	new Array();
 			var xpos		:Number		=	5;
 			var ypos		:Number		=	40;
 			var weekCount	:Number		=	0;
 			var endDate		:* 			=	new Date(currentyear, currentmonth, 0);
			var endDay		:*			=	endDate.getDay()+1;
 			var locDate		:*			=	new Date(); 
			isToday						=	false;
			inited 						= 	true;
			if ((locDate.getMonth() == currentmonth) && (locDate.getFullYear() == currentyear)) {
 				isToday		=	true; 			
			} 			
			//****************************************************
 			//		CONSTRUCT FIRST SET OF DESABLED CELLS 
			//**************************************************** 
			endDay = endDay > 6 ? 0:endDay;
			if (endDay > 0) {
				var inc_1:Number = 0;
				while (inc_1 < endDay) {
					dateBox 		= 	Construct_Date_Element(DesabledCellColor,inc_1,false);
					dateBox.id 		= 	DesabledCellColor;
					dateBox.isToday	=	false;
					dateBox.name	=	"D"+inc_1;
					dateBox.x		=	xpos+2;
					dateBox.y		=	ypos + 2;
					dateBox.alpha = 0;
					cellArray.push(dateBox);
 
					calendar_mc.addChild(dateBox);
 
					if (weekCount == 6) {
						weekCount = 0;
						ypos += 22;
						xpos = 5;
					} else {
						weekCount++;
						xpos += 22;
					}
					inc_1++;
				}
			}
			//****************************************************
			//		CONSTRUCT DATE ENTRY CELLS
			//****************************************************
			var entryNum:* 			= 	1;
			currentDateLabel.text	=	Months[currentmonth]+" - "+currentyear;
			var restNum:* = endDay;
			while (restNum < 42) {
				if (entryNum <= DaysinMonth[currentmonth]) {
					if (locDate.getDate()== entryNum && isToday == true) {
						dateBox 		= 	Construct_Date_Element(TodayCellColor,entryNum,true);
						dateBox.id 		= 	TodayCellColor;
						dateBox.hitted	=	false;
						dateBox.serial	=	restNum;
						dateBox.date	=	new Date(currentyear,currentmonth,entryNum);
						dateBox.isToday	=	true;
					}else{
						dateBox 		= 	Construct_Date_Element(EnabledCellColor,entryNum,true);
						dateBox.id 		= 	EnabledCellColor;
						dateBox.hitted	=	false;
						dateBox.serial	=	restNum;
						dateBox.date	=	new Date(currentyear,currentmonth,entryNum);
						dateBox.isToday	=	false;
					}
				} else {
			//****************************************************
			//		CONSTRUCT SECOND SET OF DESABLED DATE CELLS
			//****************************************************
					dateBox 		= 	Construct_Date_Element(DesabledCellColor,entryNum,false);
					dateBox.id 		= 	DesabledCellColor;
					dateBox.isToday	=	false;
					dateBox.alpha = 0;
				}
				dateBox.name	=	"D"+(restNum + inc_1);
				dateBox.x		=	xpos+2;
				dateBox.y		=	ypos+2;
				cellArray.push(dateBox);
 
				calendar_mc.addChild(dateBox);
 
				if (weekCount == 6) {
					weekCount = 0;
					ypos += 22;
					xpos = 5;
				} else {
					weekCount++;
					xpos += 22;
				}
				restNum++;
				entryNum++;
			}
		}
		public function removeEntry():void{
			for(var i=0;i<42;i++){
				calendar_mc.removeChild(cellArray[i]);
			}
			cellArray = [];
		}
//=======================================================================
//		DATE CELL CONSTRUCTOR FUNCTION [RETURNS MOVIECLIP]
//=======================================================================
		public function Construct_Date_Element(color:*,day:*,isEntry:Boolean):MovieClip {
				day_bg				 	= 	new MovieClip();
				hit						= 	new Sprite();
				day_bg_color			=   new MovieClip();
				day_txt					=	new TextField();
			var cellColor	:*			=	color;
			var cellWidth	:Number		=	20;
			var cellHeight	:Number		=	20;
 
				day_bg.name	 	= 	"bg";
				day_txt.name 	= 	"txt";
				
				
			//day_bg.graphics.beginFill(cellColor,1);
			//day_bg.graphics.drawRect(0,0,cellWidth,cellHeight);
			//day_bg.graphics.endFill();
			
			day_bg_color.name = 'day_bg_color'
			day_bg_color.graphics.beginFill(TodayCellColor,1);
			day_bg_color.graphics.drawRect(0,0,cellWidth,cellHeight);
			day_bg_color.graphics.endFill();
			
			day_bg_color.visible = TodayCellColor == color ? true:false;
			
			hit.graphics.beginFill(0xC1FFC2,1);
			hit.graphics.drawRect(0,0,cellWidth,cellHeight);
			hit.graphics.endFill();
			hit.visible = false;
			
			
 
			day_txt.autoSize 		= 	TextFieldAutoSize.CENTER;
			day_txt.selectable 		= 	false;
			day_txt.width 			= 	20;
			day_txt.x 				=	8;
			day_txt.y 				=	2;
 
			var format:TextFormat 			=	new TextFormat();
			format.font 				=	"Tahoma";
			format.color 				=	entryTextColor;
			format.size 				=	10;
			format.bold 				=	false;
			day_txt.defaultTextFormat 	=	format;
			
 
			if(isEntry){
				day_txt.text 				=	day;
				hit.name 	 				= 	"hit";
				day_bg.addEventListener(MouseEvent.MOUSE_DOWN, sendDate);
				day_bg.addEventListener(MouseEvent.ROLL_OVER, roll_over);
				day_bg.addEventListener(MouseEvent.ROLL_OUT, roll_out);
				day_bg.mouseChildren = false
				day_bg.buttonMode = true;
			}
			
 
			
			
			day_bg.addChild(hit);
			day_bg.addChild(day_bg_color);
			
			day_bg.addChild(day_txt);
 
			return (day_bg);
		}
//============================================================
//							CELL ROLL OVER
//============================================================
		private function roll_over(evt:MouseEvent):void {
			evt.currentTarget.getChildByName('hit').visible = true;
		}
//============================================================
//							CELL ROLL OUT
//============================================================
		private function roll_out(evt:MouseEvent):void {
			evt.currentTarget.getChildByName('hit').visible = false;
		}
//============================================================
//							CELL MOUSE DOWN
//============================================================

		private function sendDate(evt:MouseEvent):void {
			currentDay = evt.currentTarget.getChildByName('txt').text;
			currentMonth = String(Months[currentmonth]);
			currentYear = String(currentyear);
			currentMonthNumber = currentmonth;
			resetSelectedDate();
			evt.currentTarget.getChildByName('day_bg_color').visible = true;
			dispatchEvent(new Event('data_change'));
		}
//============================================================
//							RESET THE SELECTED CELL
//============================================================
		private function resetSelectedDate():void {
			for each( var aa in cellArray) {
				if (aa.getChildByName('day_bg_color')!= null) {
					aa.getChildByName('day_bg_color').visible = false;	
				}
			}
		}
		//
//============================================================
//							CELL COLOR CHANGER
//============================================================
		public function changeColor(mc,color){
 
			mc.graphics.clear();
			mc.graphics.beginFill(color);
			mc.graphics.drawRect(0,0,20,20);
			mc.graphics.endFill();
 
		}
//============================================================
//							CELL MOUSE DOWN
//============================================================
		public function showCalander(aBool:Boolean):void {
			this.visible = aBool;
		}
//=============================================================
//							BUTTON GRAPHICS CONSTRUCTOR
//=============================================================

		public function makeBtn(arg2){
			var triangleHeight:uint=6;
			var triangleShape:Sprite = new Sprite();
			triangleShape.graphics.lineStyle(1,buttonColor);
			triangleShape.graphics.beginFill(buttonColor);
			triangleShape.graphics.moveTo(triangleHeight/2, 5);
			triangleShape.graphics.lineTo(triangleHeight, triangleHeight+5);
			triangleShape.graphics.lineTo(0, triangleHeight+5);
			triangleShape.graphics.lineTo(triangleHeight/2, 5);
			triangleShape.rotation = arg2;
			return(triangleShape);
		}
		//
		public function killMe():void {
			if (parent.contains(this)) {
				parent.removeChild(this);
			}
		}
	}
}
