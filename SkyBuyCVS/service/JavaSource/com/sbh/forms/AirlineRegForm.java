package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class AirlineRegForm extends ActionForm implements Serializable{
	
	private String airId;
	private String airName;
	private String airContactName;
	private String airContactName2;
	private String custServicePhoneno;
	private String custServiceEmail;
	private String custServicePhonenoext;
	private String airFirstName;
	private String airLastName;
	private String airAddr1;
	private String airAddr2;
	private String airEmail;
	private String airPhone1;
	private String airPhoneExt1;
	private String airPhone2;
	private String airPhoneExt2;
	private String airFax;
	private String airCity;
	private String airState;
	private String airZip;
	private String airCommPct;
	private String airCountry;
	private String airPassword;
	private String airUserId;
	private String airComments;
	private String airReturnPolicy;
	private String airEmail2;
	private String airMobile1;
	private String airMobile2;
	private String airStatus;
	private String chargeType;
	
	
	private FormFile airlineLogo;
	private String airlineLogoPath;
	private String airlineLogoType;
	private String airlineLogoIntroPath;

	private FormFile aboutMe;
	private String aboutMePath;
	private String aboutMeType;
	
	private String returnAddr1;
	private String returnAddr2;
	private String returnCity;
	private String returnState;
	private String returnCountry;
	private String returnZip;
	private String returnDays;	
	private String poPct;
	/**
	 * @return the poPct
	 */
	public String getPoPct() {
		return poPct;
	}
	/**
	 * @param poPct the poPct to set
	 */
	public void setPoPct(String poPct) {
		this.poPct = poPct;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the airContactName
	 */
	public String getAirContactName() {
		return airContactName;
	}
	/**
	 * @param airContactName the airContactName to set
	 */
	public void setAirContactName(String airContactName) {
		this.airContactName = airContactName;
	}
	/**
	 * @return the airContactName2
	 */
	public String getAirContactName2() {
		return airContactName2;
	}
	/**
	 * @param airContactName2 the airContactName2 to set
	 */
	public void setAirContactName2(String airContactName2) {
		this.airContactName2 = airContactName2;
	}
	/**
	 * @return the custServicePhoneno
	 */
	public String getCustServicePhoneno() {
		return custServicePhoneno;
	}
	/**
	 * @param custServicePhoneno the custServicePhoneno to set
	 */
	public void setCustServicePhoneno(String custServicePhoneno) {
		this.custServicePhoneno = custServicePhoneno;
	}
	/**
	 * @return the custServiceEmail
	 */
	public String getCustServiceEmail() {
		return custServiceEmail;
	}
	/**
	 * @param custServiceEmail the custServiceEmail to set
	 */
	public void setCustServiceEmail(String custServiceEmail) {
		this.custServiceEmail = custServiceEmail;
	}
	/**
	 * @return the custServicePhonenoext
	 */
	public String getCustServicePhonenoext() {
		return custServicePhonenoext;
	}
	/**
	 * @param custServicePhonenoext the custServicePhonenoext to set
	 */
	public void setCustServicePhonenoext(String custServicePhonenoext) {
		this.custServicePhonenoext = custServicePhonenoext;
	}
	/**
	 * @return the airFirstName
	 */
	public String getAirFirstName() {
		return airFirstName;
	}
	/**
	 * @param airFirstName the airFirstName to set
	 */
	public void setAirFirstName(String airFirstName) {
		this.airFirstName = airFirstName;
	}
	/**
	 * @return the airLastName
	 */
	public String getAirLastName() {
		return airLastName;
	}
	/**
	 * @param airLastName the airLastName to set
	 */
	public void setAirLastName(String airLastName) {
		this.airLastName = airLastName;
	}
	/**
	 * @return the airAddr1
	 */
	public String getAirAddr1() {
		return airAddr1;
	}
	/**
	 * @param airAddr1 the airAddr1 to set
	 */
	public void setAirAddr1(String airAddr1) {
		this.airAddr1 = airAddr1;
	}
	/**
	 * @return the airAddr2
	 */
	public String getAirAddr2() {
		return airAddr2;
	}
	/**
	 * @param airAddr2 the airAddr2 to set
	 */
	public void setAirAddr2(String airAddr2) {
		this.airAddr2 = airAddr2;
	}
	/**
	 * @return the airEmail
	 */
	public String getAirEmail() {
		return airEmail;
	}
	/**
	 * @param airEmail the airEmail to set
	 */
	public void setAirEmail(String airEmail) {
		this.airEmail = airEmail;
	}
	/**
	 * @return the airPhone1
	 */
	public String getAirPhone1() {
		return airPhone1;
	}
	/**
	 * @param airPhone1 the airPhone1 to set
	 */
	public void setAirPhone1(String airPhone1) {
		this.airPhone1 = airPhone1;
	}
	/**
	 * @return the airPhoneExt1
	 */
	public String getAirPhoneExt1() {
		return airPhoneExt1;
	}
	/**
	 * @param airPhoneExt1 the airPhoneExt1 to set
	 */
	public void setAirPhoneExt1(String airPhoneExt1) {
		this.airPhoneExt1 = airPhoneExt1;
	}
	/**
	 * @return the airPhone2
	 */
	public String getAirPhone2() {
		return airPhone2;
	}
	/**
	 * @param airPhone2 the airPhone2 to set
	 */
	public void setAirPhone2(String airPhone2) {
		this.airPhone2 = airPhone2;
	}
	/**
	 * @return the airPhoneExt2
	 */
	public String getAirPhoneExt2() {
		return airPhoneExt2;
	}
	/**
	 * @param airPhoneExt2 the airPhoneExt2 to set
	 */
	public void setAirPhoneExt2(String airPhoneExt2) {
		this.airPhoneExt2 = airPhoneExt2;
	}
	/**
	 * @return the airFax
	 */
	public String getAirFax() {
		return airFax;
	}
	/**
	 * @param airFax the airFax to set
	 */
	public void setAirFax(String airFax) {
		this.airFax = airFax;
	}
	/**
	 * @return the airCity
	 */
	public String getAirCity() {
		return airCity;
	}
	/**
	 * @param airCity the airCity to set
	 */
	public void setAirCity(String airCity) {
		this.airCity = airCity;
	}
	/**
	 * @return the airState
	 */
	public String getAirState() {
		return airState;
	}
	/**
	 * @param airState the airState to set
	 */
	public void setAirState(String airState) {
		this.airState = airState;
	}
	/**
	 * @return the airZip
	 */
	public String getAirZip() {
		return airZip;
	}
	/**
	 * @param airZip the airZip to set
	 */
	public void setAirZip(String airZip) {
		this.airZip = airZip;
	}
	/**
	 * @return the airCountry
	 */
	public String getAirCountry() {
		return airCountry;
	}
	/**
	 * @param airCountry the airCountry to set
	 */
	public void setAirCountry(String airCountry) {
		this.airCountry = airCountry;
	}
	/**
	 * @return the airPassword
	 */
	public String getAirPassword() {
		return airPassword;
	}
	/**
	 * @param airPassword the airPassword to set
	 */
	public void setAirPassword(String airPassword) {
		this.airPassword = airPassword;
	}
	/**
	 * @return the airUserId
	 */
	public String getAirUserId() {
		return airUserId;
	}
	/**
	 * @param airUserId the airUserId to set
	 */
	public void setAirUserId(String airUserId) {
		this.airUserId = airUserId;
	}
	/**
	 * @return the airComments
	 */
	public String getAirComments() {
		return airComments;
	}
	/**
	 * @param airComments the airComments to set
	 */
	public void setAirComments(String airComments) {
		this.airComments = airComments;
	}
	/**
	 * @return the airReturnPolicy
	 */
	public String getAirReturnPolicy() {
		return airReturnPolicy;
	}
	/**
	 * @param airReturnPolicy the airReturnPolicy to set
	 */
	public void setAirReturnPolicy(String airReturnPolicy) {
		this.airReturnPolicy = airReturnPolicy;
	}
	/**
	 * @return the airEmail2
	 */
	public String getAirEmail2() {
		return airEmail2;
	}
	/**
	 * @param airEmail2 the airEmail2 to set
	 */
	public void setAirEmail2(String airEmail2) {
		this.airEmail2 = airEmail2;
	}
	/**
	 * @return the airMobile1
	 */
	public String getAirMobile1() {
		return airMobile1;
	}
	/**
	 * @param airMobile1 the airMobile1 to set
	 */
	public void setAirMobile1(String airMobile1) {
		this.airMobile1 = airMobile1;
	}
	/**
	 * @return the airMobile2
	 */
	public String getAirMobile2() {
		return airMobile2;
	}
	/**
	 * @param airMobile2 the airMobile2 to set
	 */
	public void setAirMobile2(String airMobile2) {
		this.airMobile2 = airMobile2;
	}
	/**
	 * @return the airStatus
	 */
	public String getAirStatus() {
		return airStatus;
	}
	/**
	 * @param airStatus the airStatus to set
	 */
	public void setAirStatus(String airStatus) {
		this.airStatus = airStatus;
	}
	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}
	/**
	 * @param chargeType the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	/**
	 * @return the returnAddr1
	 */
	public String getReturnAddr1() {
		return returnAddr1;
	}
	/**
	 * @param returnAddr1 the returnAddr1 to set
	 */
	public void setReturnAddr1(String returnAddr1) {
		this.returnAddr1 = returnAddr1;
	}
	/**
	 * @return the returnAddr2
	 */
	public String getReturnAddr2() {
		return returnAddr2;
	}
	/**
	 * @param returnAddr2 the returnAddr2 to set
	 */
	public void setReturnAddr2(String returnAddr2) {
		this.returnAddr2 = returnAddr2;
	}
	/**
	 * @return the returnCity
	 */
	public String getReturnCity() {
		return returnCity;
	}
	/**
	 * @param returnCity the returnCity to set
	 */
	public void setReturnCity(String returnCity) {
		this.returnCity = returnCity;
	}
	/**
	 * @return the returnState
	 */
	public String getReturnState() {
		return returnState;
	}
	/**
	 * @param returnState the returnState to set
	 */
	public void setReturnState(String returnState) {
		this.returnState = returnState;
	}
	/**
	 * @return the returnCountry
	 */
	public String getReturnCountry() {
		return returnCountry;
	}
	/**
	 * @param returnCountry the returnCountry to set
	 */
	public void setReturnCountry(String returnCountry) {
		this.returnCountry = returnCountry;
	}
	/**
	 * @return the returnZip
	 */
	public String getReturnZip() {
		return returnZip;
	}
	/**
	 * @param returnZip the returnZip to set
	 */
	public void setReturnZip(String returnZip) {
		this.returnZip = returnZip;
	}
	/**
	 * @return the returnDays
	 */
	public String getReturnDays() {
		return returnDays;
	}
	/**
	 * @param returnDays the returnDays to set
	 */
	public void setReturnDays(String returnDays) {
		this.returnDays = returnDays;
	}
	/**
	 * @return the airCommPct
	 */
	public String getAirCommPct() {
		return airCommPct;
	}
	/**
	 * @param airCommPct the airCommPct to set
	 */
	public void setAirCommPct(String airCommPct) {
		this.airCommPct = airCommPct;
	}
	/**
	 * @return the airlineLogo
	 */
	public FormFile getAirlineLogo() {
		return airlineLogo;
	}
	/**
	 * @param airlineLogo the airlineLogo to set
	 */
	public void setAirlineLogo(FormFile airlineLogo) {
		this.airlineLogo = airlineLogo;
	}
	/**
	 * @return the airlineLogoPath
	 */
	public String getAirlineLogoPath() {
		return airlineLogoPath;
	}
	/**
	 * @param airlineLogoPath the airlineLogoPath to set
	 */
	public void setAirlineLogoPath(String airlineLogoPath) {
		this.airlineLogoPath = airlineLogoPath;
	}
	/**
	 * @return the airlineLogoType
	 */
	public String getAirlineLogoType() {
		return airlineLogoType;
	}
	/**
	 * @param airlineLogoType the airlineLogoType to set
	 */
	public void setAirlineLogoType(String airlineLogoType) {
		this.airlineLogoType = airlineLogoType;
	}
	/**
	 * @return the airlineLogoIntroPath
	 */
	public String getAirlineLogoIntroPath() {
		return airlineLogoIntroPath;
	}
	/**
	 * @param airlineLogoIntroPath the airlineLogoIntroPath to set
	 */
	public void setAirlineLogoIntroPath(String airlineLogoIntroPath) {
		this.airlineLogoIntroPath = airlineLogoIntroPath;
	}
	/**
	 * @return the aboutMe
	 */
	public FormFile getAboutMe() {
		return aboutMe;
	}
	/**
	 * @param aboutMe the aboutMe to set
	 */
	public void setAboutMe(FormFile aboutMe) {
		this.aboutMe = aboutMe;
	}
	/**
	 * @return the aboutMePath
	 */
	public String getAboutMePath() {
		return aboutMePath;
	}
	/**
	 * @param aboutMePath the aboutMePath to set
	 */
	public void setAboutMePath(String aboutMePath) {
		this.aboutMePath = aboutMePath;
	}
	/**
	 * @return the aboutMeType
	 */
	public String getAboutMeType() {
		return aboutMeType;
	}
	/**
	 * @param aboutMeType the aboutMeType to set
	 */
	public void setAboutMeType(String aboutMeType) {
		this.aboutMeType = aboutMeType;
	}
	

}
