package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class UploadSkyBuyCatalogueForm extends ActionForm implements Serializable{
	
	/**
	 * Serial Version User Id.
	 */
	private static final long serialVersionUID = 1L;
	
	private String reasonForUpload;
	private String version;
	private String uploadType;
	private FormFile cataloguePath;
	private FormFile installationPath;
//	private FormFile welcomePagePath;
	private String userName;
	private String password;
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the reasonForUpload
	 */
	public String getReasonForUpload() {
		return reasonForUpload;
	}
	/**
	 * @param reasonForUpload the reasonForUpload to set
	 */
	public void setReasonForUpload(String reasonForUpload) {
		this.reasonForUpload = reasonForUpload;
	}
	/**
	 * @return the cataloguePath
	 */
	public FormFile getCataloguePath() {
		return cataloguePath;
	}
	/**
	 * @param cataloguePath the cataloguePath to set
	 */
	public void setCataloguePath(FormFile cataloguePath) {
		this.cataloguePath = cataloguePath;
	}
	/**
	 * @return the welcomePagePath
	 *//*
	public FormFile getWelcomePagePath() {
		return welcomePagePath;
	}
	*//**
	 * @param welcomePagePath the welcomePagePath to set
	 *//*
	public void setWelcomePagePath(FormFile welcomePagePath) {
		this.welcomePagePath = welcomePagePath;
	}*/
	/**
	 * @return the installationPath
	 */
	public FormFile getInstallationPath() {
		return installationPath;
	}
	/**
	 * @param installationPath the installationPath to set
	 */
	public void setInstallationPath(FormFile installationPath) {
		this.installationPath = installationPath;
	}
	/**
	 * @return the uploadType
	 */
	public String getUploadType() {
		return uploadType;
	}
	/**
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
}
