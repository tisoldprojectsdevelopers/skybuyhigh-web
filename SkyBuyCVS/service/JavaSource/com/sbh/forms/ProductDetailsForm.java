package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ProductDetailsForm extends ActionForm implements Serializable{
	
	/**
	 * Serial Version User Id
	 */
	private static final long serialVersionUID = 1L;
	private String prodId;
	private String prodCode;
	private String prodTitle;
	private String inShopStatus;
	private String sbhProdStatus;
	private String brandName;
	private String vendPrice;
	private String cateId;
	private String ownerId;
	private String shortDesc = null;
	private String longDesc;
	private String sbhPrice;
	private String imgPath;	
	private String imgType;	
	private FormFile prodImgPath;	
	private String 	categoryDate;
	private String  sbhComment;
	/*private String validFromDate;
	private String validToDate;*/
	private String instructions;
	private String poPct;
	private String preAuthCC;
	private String color;
	private String size;
	private FormFile uploadMainImagePath;
	private String 	 uploadMainImgCap;
	private String 	 uploadMainImgType;
	private FormFile uploadView1ImagePath;
	private String   uploadView1ImgCap;
	private String 	 uploadView1ImgType;
	private FormFile uploadView2ImagePath;
	private String   uploadView2ImgCap;
	private String 	 uploadView2ImgType;
	private FormFile uploadView3ImagePath;
	private String   uploadView3ImgCap;
	private String 	 uploadView3ImgType;
	
	private String deleteView1Img;
	private String deleteView2Img;
	private String deleteView3Img;
	
	private FormFile advtMainImgPath;
	private FormFile advertisementPath;
	private String   advertisementType;
	
	private String fullDescription;
	private String shortDescription;
	
	private FormFile specialProdPath;
	private String   specialProdType;
	
	/**
	 * @return the specialProdPath
	 */
	public FormFile getSpecialProdPath() {
		return specialProdPath;
	}
	/**
	 * @param specialProdPath the specialProdPath to set
	 */
	public void setSpecialProdPath(FormFile specialProdPath) {
		this.specialProdPath = specialProdPath;
	}
	/**
	 * @return the specialProdType
	 */
	public String getSpecialProdType() {
		return specialProdType;
	}
	/**
	 * @param specialProdType the specialProdType to set
	 */
	public void setSpecialProdType(String specialProdType) {
		this.specialProdType = specialProdType;
	}
	/**
	 * @return the fullDescription
	 */
	public String getFullDescription() {
		return fullDescription;
	}
	/**
	 * @param fullDescription the fullDescription to set
	 */
	public void setFullDescription(String fullDescription) {
		this.fullDescription = fullDescription;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return prodId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return vendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return cateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	/**
	 * @return the shortDesc
	 */
	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * @param shortDesc the shortDesc to set
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	/**
	 * @return the longDesc
	 */
	public String getLongDesc() {
		return longDesc;
	}
	/**
	 * @param longDesc the longDesc to set
	 */
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return sbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}	
	/**
	 * @param categoryDate the categoryDate to set
	 */
	public void setCategoryDate(String categoryDate) {
		this.categoryDate = categoryDate;
	}
	/**
	 * @return the categoryDate
	 */
	public String getCategoryDate() {
		return categoryDate;
	}
	/**
	 * @return the prodTitle
	 */
	public String getProdTitle() {
		return prodTitle;
	}
	/**
	 * @param prodTitle the prodTitle to set
	 */
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}	
	/**
	 * @return the prodImgPath
	 */
	public FormFile getProdImgPath() {
		return prodImgPath;
	}
	/**
	 * @param prodImgPath the prodImgPath to set
	 */
	public void setProdImgPath(FormFile prodImgPath) {
		this.prodImgPath = prodImgPath;
	}
	/**
	 * @return the imgPath
	 */
	public String getImgPath() {
		return imgPath;
	}
	/**
	 * @param imgPath the imgPath to set
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	/**
	 * @return the imgType
	 */
	public String getImgType() {
		return imgType;
	}
	/**
	 * @param imgType the imgType to set
	 */
	public void setImgType(String imgType) {
		this.imgType = imgType;
	}
	/**
	 * @return the inShopStatus
	 */
	public String getInShopStatus() {
		return inShopStatus;
	}
	/**
	 * @param inShopStatus the inShopStatus to set
	 */
	public void setInShopStatus(String inShopStatus) {
		this.inShopStatus = inShopStatus;
	}
	/**
	 * @return the sbhProdStatus
	 */
	public String getSbhProdStatus() {
		return sbhProdStatus;
	}
	/**
	 * @param sbhProdStatus the sbhProdStatus to set
	 */
	public void setSbhProdStatus(String sbhProdStatus) {
		this.sbhProdStatus = sbhProdStatus;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the uploadMainImagePath
	 */
	public FormFile getUploadMainImagePath() {
		return uploadMainImagePath;
	}
	/**
	 * @param uploadMainImagePath the uploadMainImagePath to set
	 */
	public void setUploadMainImagePath(FormFile uploadMainImagePath) {
		this.uploadMainImagePath = uploadMainImagePath;
	}
	/**
	 * @return the uploadMainImgCap
	 */
	public String getUploadMainImgCap() {
		return uploadMainImgCap;
	}
	/**
	 * @param uploadMainImgCap the uploadMainImgCap to set
	 */
	public void setUploadMainImgCap(String uploadMainImgCap) {
		this.uploadMainImgCap = uploadMainImgCap;
	}
	/**
	 * @return the uploadMainImgType
	 */
	public String getUploadMainImgType() {
		return uploadMainImgType;
	}
	/**
	 * @param uploadMainImgType the uploadMainImgType to set
	 */
	public void setUploadMainImgType(String uploadMainImgType) {
		this.uploadMainImgType = uploadMainImgType;
	}
	/**
	 * @return the uploadView1ImagePath
	 */
	public FormFile getUploadView1ImagePath() {
		return uploadView1ImagePath;
	}
	/**
	 * @param uploadView1ImagePath the uploadView1ImagePath to set
	 */
	public void setUploadView1ImagePath(FormFile uploadView1ImagePath) {
		this.uploadView1ImagePath = uploadView1ImagePath;
	}
	/**
	 * @return the uploadView1ImgCap
	 */
	public String getUploadView1ImgCap() {
		return uploadView1ImgCap;
	}
	/**
	 * @param uploadView1ImgCap the uploadView1ImgCap to set
	 */
	public void setUploadView1ImgCap(String uploadView1ImgCap) {
		this.uploadView1ImgCap = uploadView1ImgCap;
	}
	/**
	 * @return the uploadView1ImgType
	 */
	public String getUploadView1ImgType() {
		return uploadView1ImgType;
	}
	/**
	 * @param uploadView1ImgType the uploadView1ImgType to set
	 */
	public void setUploadView1ImgType(String uploadView1ImgType) {
		this.uploadView1ImgType = uploadView1ImgType;
	}
	/**
	 * @return the uploadView2ImagePath
	 */
	public FormFile getUploadView2ImagePath() {
		return uploadView2ImagePath;
	}
	/**
	 * @param uploadView2ImagePath the uploadView2ImagePath to set
	 */
	public void setUploadView2ImagePath(FormFile uploadView2ImagePath) {
		this.uploadView2ImagePath = uploadView2ImagePath;
	}
	/**
	 * @return the uploadView2ImgCap
	 */
	public String getUploadView2ImgCap() {
		return uploadView2ImgCap;
	}
	/**
	 * @param uploadView2ImgCap the uploadView2ImgCap to set
	 */
	public void setUploadView2ImgCap(String uploadView2ImgCap) {
		this.uploadView2ImgCap = uploadView2ImgCap;
	}
	/**
	 * @return the uploadView2ImgType
	 */
	public String getUploadView2ImgType() {
		return uploadView2ImgType;
	}
	/**
	 * @param uploadView2ImgType the uploadView2ImgType to set
	 */
	public void setUploadView2ImgType(String uploadView2ImgType) {
		this.uploadView2ImgType = uploadView2ImgType;
	}
	/**
	 * @return the uploadView3ImagePath
	 */
	public FormFile getUploadView3ImagePath() {
		return uploadView3ImagePath;
	}
	/**
	 * @param uploadView3ImagePath the uploadView3ImagePath to set
	 */
	public void setUploadView3ImagePath(FormFile uploadView3ImagePath) {
		this.uploadView3ImagePath = uploadView3ImagePath;
	}
	/**
	 * @return the uploadView3ImgCap
	 */
	public String getUploadView3ImgCap() {
		return uploadView3ImgCap;
	}
	/**
	 * @param uploadView3ImgCap the uploadView3ImgCap to set
	 */
	public void setUploadView3ImgCap(String uploadView3ImgCap) {
		this.uploadView3ImgCap = uploadView3ImgCap;
	}
	/**
	 * @return the uploadView3ImgType
	 */
	public String getUploadView3ImgType() {
		return uploadView3ImgType;
	}
	/**
	 * @param uploadView3ImgType the uploadView3ImgType to set
	 */
	public void setUploadView3ImgType(String uploadView3ImgType) {
		this.uploadView3ImgType = uploadView3ImgType;
	}
	/**
	 * @return the deleteView1Img
	 */
	public String getDeleteView1Img() {
		return deleteView1Img;
	}
	/**
	 * @param deleteView1Img the deleteView1Img to set
	 */
	public void setDeleteView1Img(String deleteView1Img) {
		this.deleteView1Img = deleteView1Img;
	}
	/**
	 * @return the deleteView2Img
	 */
	public String getDeleteView2Img() {
		return deleteView2Img;
	}
	/**
	 * @param deleteView2Img the deleteView2Img to set
	 */
	public void setDeleteView2Img(String deleteView2Img) {
		this.deleteView2Img = deleteView2Img;
	}
	/**
	 * @return the deleteView3Img
	 */
	public String getDeleteView3Img() {
		return deleteView3Img;
	}
	/**
	 * @param deleteView3Img the deleteView3Img to set
	 */
	public void setDeleteView3Img(String deleteView3Img) {
		this.deleteView3Img = deleteView3Img;
	}
	/**
	 * @return the sbhComment
	 */
	public String getSbhComment() {
		return sbhComment;
	}
	/**
	 * @param sbhComment the sbhComment to set
	 */
	public void setSbhComment(String sbhComment) {
		this.sbhComment = sbhComment;
	}	
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	/*public String getValidFromDate() {
		return validFromDate;
	}
	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}
	public String getValidToDate() {
		return validToDate;
	}
	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}*/
	/**
	 * @return the poPct
	 */
	public String getPoPct() {
		return poPct;
	}
	/**
	 * @param poPct the poPct to set
	 */
	public void setPoPct(String poPct) {
		this.poPct = poPct;
	}
	/**
	 * @return the preAuthCC
	 */
	public String getPreAuthCC() {
		return preAuthCC;
	}
	/**
	 * @param preAuthCC the preAuthCC to set
	 */
	public void setPreAuthCC(String preAuthCC) {
		this.preAuthCC = preAuthCC;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the advtMainImgPath
	 */
	public FormFile getAdvtMainImgPath() {
		return advtMainImgPath;
	}
	/**
	 * @param advtMainImgPath the advtMainImgPath to set
	 */
	public void setAdvtMainImgPath(FormFile advtMainImgPath) {
		this.advtMainImgPath = advtMainImgPath;
	}
	/**
	 * @return the advertisementPath
	 */
	public FormFile getAdvertisementPath() {
		return advertisementPath;
	}
	/**
	 * @param advertisementPath the advertisementPath to set
	 */
	public void setAdvertisementPath(FormFile advertisementPath) {
		this.advertisementPath = advertisementPath;
	}
	/**
	 * @return the advertisementType
	 */
	public String getAdvertisementType() {
		return advertisementType;
	}
	/**
	 * @param advertisementType the advertisementType to set
	 */
	public void setAdvertisementType(String advertisementType) {
		this.advertisementType = advertisementType;
	}
			

}
