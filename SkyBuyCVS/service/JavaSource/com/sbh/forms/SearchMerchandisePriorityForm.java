
package com.sbh.forms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;


import com.sbh.vo.ProductDetailsVO;

public class SearchMerchandisePriorityForm extends ActionForm implements Serializable{
	private ArrayList  recordSet;
	private String searchBy;
	private String searchValue;
	private String category;
	//private String effectiveDate;
	//private String prodStatus;
	private String coverflowStatus;
	//private String uploadFromDate;
	//private String uploadToDate;
	
	
	public  SearchMerchandisePriorityForm(){
		recordSet=new ArrayList();
		/*DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    java.util.Date date = new java.util.Date();
        String sCurrentDate = dateFormat.format(date);
        // System.out.println("Current Date: " + sCurrentDate); */  
        //effectiveDate=sCurrentDate;
        searchBy="All";
        //prodStatus="All";
        category="All";
        //uploadToDate = sCurrentDate; 
        //sbhStatus="ALL";
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public String getSearchValue() {
		return searchValue;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory() {
		return category;
	}
	
	public void setRecordSet(ArrayList recordSet){
		if(recordSet!=null){
		this.recordSet=recordSet;
		}
	}
	
	public ArrayList getRecordSet( ) {
		return recordSet;
	}
	public ProductDetailsVO getMerchandizeDetails(int index) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}

		return (ProductDetailsVO)recordSet.get(index);
	}
	public void setMerchandizeDetails(int index, ProductDetailsVO merchandizeDetails) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}
		recordSet.set(index, merchandizeDetails);


	}

	public void reset( ) {
		recordSet.clear( );

	}
	
	public String getCoverflowStatus() {
		return coverflowStatus;
	}
	public void setCoverflowStatus(String coverflowStatus) {
		this.coverflowStatus = coverflowStatus;
	}
	

}
