package com.sbh.forms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.struts.action.ActionForm;

public class SearchCatalogueForm extends ActionForm implements Serializable{
	
	private String category="";
	//private String catalogueDate="";
	private String prodStatus="";
	private String sbhProdStatus="";
	private String uploadFromDate="";
	private String uploadToDate="";
	private String searchBy = "";
	private String searchValue = "";
	
	public  SearchCatalogueForm(){
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    java.util.Date date = new java.util.Date();
        String sCurrentDate = dateFormat.format(date);
        // System.out.println("Current Date: " + sCurrentDate);   
        prodStatus ="ALL";
        sbhProdStatus="ALL";
        category="ALL";
        uploadFromDate="";
        uploadToDate = sCurrentDate;
        searchBy = "ALL";
        searchValue = "";
       
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the catalogueDate
	 *//*
	public String getCatalogueDate() {
		return catalogueDate;
	}
	*//**
	 * @param catalogueDate the catalogueDate to set
	 *//*
	public void setCatalogueDate(String catalogueDate) {
		this.catalogueDate = catalogueDate;
	}*/
	public void setProdStatus(String prodStatus) {
		this.prodStatus = prodStatus;
	}
	public String getProdStatus() {
		return prodStatus;
	}
	/**
	 * @return the sbhProdStatus
	 */
	public String getSbhProdStatus() {
		return sbhProdStatus;
	}
	/**
	 * @param sbhProdStatus the sbhProdStatus to set
	 */
	public void setSbhProdStatus(String sbhProdStatus) {
		this.sbhProdStatus = sbhProdStatus;
	}
	/**
	 * @return the uploadFromDate
	 */
	public String getUploadFromDate() {
		return uploadFromDate;
	}
	/**
	 * @param uploadFromDate the uploadFromDate to set
	 */
	public void setUploadFromDate(String uploadFromDate) {
		this.uploadFromDate = uploadFromDate;
	}
	/**
	 * @return the uploadToDate
	 */
	public String getUploadToDate() {
		return uploadToDate;
	}
	/**
	 * @param uploadToDate the uploadToDate to set
	 */
	public void setUploadToDate(String uploadToDate) {
		this.uploadToDate = uploadToDate;
	}
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	
	

}
