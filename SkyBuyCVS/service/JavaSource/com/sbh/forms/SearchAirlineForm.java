package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;

public class SearchAirlineForm extends ActionForm implements Serializable{
	
	private String airlineSearchBy;
	private String airlineSearchValue;
	/**
	 * @return the airlineSearchBy
	 */
	public String getAirlineSearchBy() {
		return airlineSearchBy;
	}
	/**
	 * @param airlineSearchBy the airlineSearchBy to set
	 */
	public void setAirlineSearchBy(String airlineSearchBy) {
		this.airlineSearchBy = airlineSearchBy;
	}
	/**
	 * @return the airlineSearchValue
	 */
	public String getAirlineSearchValue() {
		return airlineSearchValue;
	}
	/**
	 * @param airlineSearchValue the airlineSearchValue to set
	 */
	public void setAirlineSearchValue(String airlineSearchValue) {
		this.airlineSearchValue = airlineSearchValue;
	}

}
