/**
 * 
 * @author Thapovan
 * @version Version-Number 1.0
 * @created_On 07/NOV/2008
 */
package com.sbh.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Transaction Id is an auto-generated id, it's generated when the customer 
 * placed an order, if that customer like to view the details of the order. 
 * The customer uses the transaction Id as login id, at that time this form
 * holds the value for the inputed transaction id of that customer.
 * 
 * @author Thapovan
 * @version Version-Number 1.0
 * @created_On 07/NOV/2008
 * @updated_By
 * @updated_On
 */
public class CustomerLoginForm extends ActionForm {
	
	private static final long serialVersionUID = 1L;
	private String transactionId = null;
	private String emailId = null;
	private String reasonForReturn = null;
	private String returnQuantity = null;
	private String totalQuantity = null;
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	} 
	
	/**
	 * @return the reasonForReturn
	 */
	public String getReasonForReturn() {
		return reasonForReturn;
	}

	/**
	 * @param reasonForReturn the reasonForReturn to set
	 */
	public void setReasonForReturn(String reasonForReturn) {
		this.reasonForReturn = reasonForReturn;
	}
	
	/**
	 * @return the returnQuantity
	 */
	public String getReturnQuantity() {
		return returnQuantity;
	}

	/**
	 * @param returnQuantity the returnQuantity to set
	 */
	public void setReturnQuantity(String returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	
	/**
	 * @return the totalQuantity
	 */
	public String getTotalQuantity() {
		return totalQuantity;
	}

	/**
	 * @param totalQuantity the totalQuantity to set
	 */
	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.emailId=null;
		this.reasonForReturn=null;
		this.returnQuantity = null;
	}
}
