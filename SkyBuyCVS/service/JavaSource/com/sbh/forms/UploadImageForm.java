package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class UploadImageForm extends ActionForm implements Serializable{
	
	private FormFile mainImgPath;
	private String 	 mainImgCap;
	private String 	 mainImgType;
	private FormFile view1ImgPath;
	private String   view1ImgCap;
	private String 	 view1ImgType;
	private FormFile view2ImgPath;
	private String   view2ImgCap;
	private String 	 view2ImgType;
	private FormFile view3ImgPath;
	private String   view3ImgCap;
	private String 	 view3ImgType;
	/**
	 * @return the mainImgPath
	 */
	public FormFile getMainImgPath() {
		return mainImgPath;
	}
	/**
	 * @param mainImgPath the mainImgPath to set
	 */
	public void setMainImgPath(FormFile mainImgPath) {
		this.mainImgPath = mainImgPath;
	}
	/**
	 * @return the mainImgCap
	 */
	public String getMainImgCap() {
		return mainImgCap;
	}
	/**
	 * @param mainImgCap the mainImgCap to set
	 */
	public void setMainImgCap(String mainImgCap) {
		this.mainImgCap = mainImgCap;
	}
	/**
	 * @return the view1ImgPath
	 */
	public FormFile getView1ImgPath() {
		return view1ImgPath;
	}
	/**
	 * @param view1ImgPath the view1ImgPath to set
	 */
	public void setView1ImgPath(FormFile view1ImgPath) {
		this.view1ImgPath = view1ImgPath;
	}
	/**
	 * @return the view1ImgCap
	 */
	public String getView1ImgCap() {
		return view1ImgCap;
	}
	/**
	 * @param view1ImgCap the view1ImgCap to set
	 */
	public void setView1ImgCap(String view1ImgCap) {
		this.view1ImgCap = view1ImgCap;
	}
	/**
	 * @return the view2ImgPath
	 */
	public FormFile getView2ImgPath() {
		return view2ImgPath;
	}
	/**
	 * @param view2ImgPath the view2ImgPath to set
	 */
	public void setView2ImgPath(FormFile view2ImgPath) {
		this.view2ImgPath = view2ImgPath;
	}
	/**
	 * @return the view2ImgCap
	 */
	public String getView2ImgCap() {
		return view2ImgCap;
	}
	/**
	 * @param view2ImgCap the view2ImgCap to set
	 */
	public void setView2ImgCap(String view2ImgCap) {
		this.view2ImgCap = view2ImgCap;
	}
	/**
	 * @return the view3ImgPath
	 */
	public FormFile getView3ImgPath() {
		return view3ImgPath;
	}
	/**
	 * @param view3ImgPath the view3ImgPath to set
	 */
	public void setView3ImgPath(FormFile view3ImgPath) {
		this.view3ImgPath = view3ImgPath;
	}
	/**
	 * @return the view3ImgCap
	 */
	public String getView3ImgCap() {
		return view3ImgCap;
	}
	/**
	 * @param view3ImgCap the view3ImgCap to set
	 */
	public void setView3ImgCap(String view3ImgCap) {
		this.view3ImgCap = view3ImgCap;
	}
	/**
	 * @return the mainImgType
	 */
	public String getMainImgType() {
		return mainImgType;
	}
	/**
	 * @param mainImgType the mainImgType to set
	 */
	public void setMainImgType(String mainImgType) {
		this.mainImgType = mainImgType;
	}
	/**
	 * @return the view1ImgType
	 */
	public String getView1ImgType() {
		return view1ImgType;
	}
	/**
	 * @param view1ImgType the view1ImgType to set
	 */
	public void setView1ImgType(String view1ImgType) {
		this.view1ImgType = view1ImgType;
	}
	/**
	 * @return the view2ImgType
	 */
	public String getView2ImgType() {
		return view2ImgType;
	}
	/**
	 * @param view2ImgType the view2ImgType to set
	 */
	public void setView2ImgType(String view2ImgType) {
		this.view2ImgType = view2ImgType;
	}
	/**
	 * @return the view3ImgType
	 */
	public String getView3ImgType() {
		return view3ImgType;
	}
	/**
	 * @param view3ImgType the view3ImgType to set
	 */
	public void setView3ImgType(String view3ImgType) {
		this.view3ImgType = view3ImgType;
	}
	
	
}
