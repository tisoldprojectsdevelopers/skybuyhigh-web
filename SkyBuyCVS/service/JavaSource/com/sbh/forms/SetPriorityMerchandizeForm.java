package com.sbh.forms;

import java.io.Serializable;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.sbh.vo.ProductDetailsVO;

public class SetPriorityMerchandizeForm extends ActionForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ProductDetailsVO>  recordSet;
	private String searchBy;
	private String searchValue;
	private String category="All";
	private String prodStatus="All";
	private String sbhStatus="All";
	
	/**
	 * @return the prodStatus
	 */
	public String getProdStatus() {
		return prodStatus;
	}
	/**
	 * @param prodStatus the prodStatus to set
	 */
	public void setProdStatus(String prodStatus) {
		this.prodStatus = prodStatus;
	}
	/**
	 * @return the sbhStatus
	 */
	public String getSbhStatus() {
		return sbhStatus;
	}
	/**
	 * @param sbhStatus the sbhStatus to set
	 */
	public void setSbhStatus(String sbhStatus) {
		this.sbhStatus = sbhStatus;
	}
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the recordSet
	 */
	public List<ProductDetailsVO> getRecordSet() {
		return recordSet;
	}
	/**
	 * @param recordSet the recordSet to set
	 */
	public void setRecordSet(List<ProductDetailsVO> recordSet) {
		this.recordSet = recordSet;
	}
	
	public ProductDetailsVO getMerchandizeDetails(int index) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}

		return (ProductDetailsVO)recordSet.get(index);
	}
	public void setMerchandizeDetails(int index, ProductDetailsVO merchandizeDetails) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}
		recordSet.set(index, merchandizeDetails);
	}
}
