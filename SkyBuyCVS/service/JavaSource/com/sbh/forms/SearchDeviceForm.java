package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;

public class SearchDeviceForm extends ActionForm implements Serializable{
	
	private String deviceSearchBy;
	private String deviceSearchValue;
	/**
	 * @return the deviceSearchBy
	 */
	public String getDeviceSearchBy() {
		return deviceSearchBy;
	}
	/**
	 * @param deviceSearchBy the deviceSearchBy to set
	 */
	public void setDeviceSearchBy(String deviceSearchBy) {
		this.deviceSearchBy = deviceSearchBy;
	}
	/**
	 * @return the deviceSearchValue
	 */
	public String getDeviceSearchValue() {
		return deviceSearchValue;
	}
	/**
	 * @param deviceSearchValue the deviceSearchValue to set
	 */
	public void setDeviceSearchValue(String deviceSearchValue) {
		this.deviceSearchValue = deviceSearchValue;
	}
	
	

}
