/**
 * 
 */
package com.sbh.forms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Thapovan
 *
 */
public class OrderReturnForm extends ActionForm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String searchBy ;
	private String searchValue;
	private String category;
	private String orderFromDate;
	private String orderToDate;
	private String orderStatus;
	private String rmaNumber;
	private String returnStatus;
	private String orderItemId;
	private String returnComments;
	private String chargeBackAmount;
	private String adminComments;
	private String custReturnQuantity;
	private String returnQuantity;
	private String authoriseSignatory;
	
	
	/**
	 * Constructor to set the values initially during the page load.
	 */
	public  OrderReturnForm(){
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    java.util.Date date = new java.util.Date();
        String sCurrentDate = dateFormat.format(date);
        // System.out.println("Current Date: " + sCurrentDate);   
        searchBy ="ALL";
        searchValue="";
        category="ALL";
        orderFromDate="";
        orderToDate = sCurrentDate;
        orderStatus = "ALL";
	}

	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}

	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}

	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the orderFromDate
	 */
	public String getOrderFromDate() {
		return orderFromDate;
	}

	/**
	 * @param orderFromDate the orderFromDate to set
	 */
	public void setOrderFromDate(String orderFromDate) {
		this.orderFromDate = orderFromDate;
	}

	/**
	 * @return the orderToDate
	 */
	public String getOrderToDate() {
		return orderToDate;
	}

	/**
	 * @param orderToDate the orderToDate to set
	 */
	public void setOrderToDate(String orderToDate) {
		this.orderToDate = orderToDate;
	}

	/**
	 * @return the rmaNumber
	 */
	public String getRmaNumber() {
		return rmaNumber;
	}

	/**
	 * @param rmaNumber the rmaNumber to set
	 */
	public void setRmaNumber(String rmaNumber) {
		this.rmaNumber = rmaNumber;
	}

	/**
	 * @return the returnStatus
	 */
	public String getReturnStatus() {
		return returnStatus;
	}

	/**
	 * @param returnStatus the returnStatus to set
	 */
	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}
	
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}

	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}

	/**
	 * @return the returnComments
	 */
	public String getReturnComments() {
		return returnComments;
	}

	/**
	 * @param returnComments the returnComments to set
	 */
	public void setReturnComments(String returnComments) {
		this.returnComments = returnComments;
	}
	
	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return orderStatus;
	}

	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	/**
	 * @return the chargeBackAmount
	 */
	public String getChargeBackAmount() {
		return chargeBackAmount;
	}

	/**
	 * @param chargeBackAmount the chargeBackAmount to set
	 */
	public void setChargeBackAmount(String chargeBackAmount) {
		this.chargeBackAmount = chargeBackAmount;
	}

	/**
	 * @return the adminComments
	 */
	public String getAdminComments() {
		return adminComments;
	}

	/**
	 * @param adminComments the adminComments to set
	 */
	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}
	
	/**
	 * @return the custReturnQuantity
	 */
	public String getCustReturnQuantity() {
		return custReturnQuantity;
	}

	/**
	 * @param custReturnQuantity the custReturnQuantity to set
	 */
	public void setCustReturnQuantity(String custReturnQuantity) {
		this.custReturnQuantity = custReturnQuantity;
	}

	/**
	 * @return the returnQuantity
	 */
	public String getReturnQuantity() {
		return returnQuantity;
	}

	/**
	 * @param returnQuantity the returnQuantity to set
	 */
	public void setReturnQuantity(String returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	
	/**
	 * This is to reset all the values, when the page loads.
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.rmaNumber = null;
		this.returnStatus = null;
		this.returnComments = null;
		this.adminComments = null;
		this.returnQuantity = null;
		this.custReturnQuantity = null;
	}

	/**
	 * @return the authoriseSignatory
	 */
	public String getAuthoriseSignatory() {
		return authoriseSignatory;
	}

	/**
	 * @param authoriseSignatory the authoriseSignatory to set
	 */
	public void setAuthoriseSignatory(String authoriseSignatory) {
		this.authoriseSignatory = authoriseSignatory;
	}


}
