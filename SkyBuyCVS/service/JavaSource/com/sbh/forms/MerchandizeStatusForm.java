package com.sbh.forms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.sbh.vo.MerchandizeStatus;

public class MerchandizeStatusForm extends ActionForm implements Serializable {
	private List  recordSet;

	public  MerchandizeStatusForm(){
		recordSet=new ArrayList();
		recordSet.add(new MerchandizeStatus());
	}
	public List getRecordSet( ) {
		return recordSet;
	}
	public MerchandizeStatus getMerchandizeStatus(int index) {
		if(recordSet.size()<=index){
			recordSet.add(new MerchandizeStatus());
		}

		return (MerchandizeStatus)recordSet.get(index);
	}
	public void setMerchandizeStatus(int index, MerchandizeStatus merchandizeStatus) {
		if(recordSet.size()<=index){
			recordSet.add(new MerchandizeStatus());
		}
		recordSet.set(index, merchandizeStatus);


	}

	public void reset( ) {
		recordSet.clear( );

	}


}
