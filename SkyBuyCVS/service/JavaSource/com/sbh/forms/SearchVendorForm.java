package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;

public class SearchVendorForm extends ActionForm implements Serializable{
	
	private String vendorSearchBy="";
	private String vendorSearchValue="";
	private String category="";
	private String categoryDate="";
	
	
	/**
	 * @param vendorSearchBy the vendorSearchBy to set
	 */
	public void setVendorSearchBy(String vendorSearchBy) {
		this.vendorSearchBy = vendorSearchBy;
	}
	/**
	 * @return the vendorSearchBy
	 */
	public String getVendorSearchBy() {
		return vendorSearchBy;
	}
	/**
	 * @param vendorSearchValue the vendorSearchValue to set
	 */
	public void setVendorSearchValue(String vendorSearchValue) {
		this.vendorSearchValue = vendorSearchValue;
	}
	/**
	 * @return the vendorSearchValue
	 */
	public String getVendorSearchValue() {
		return vendorSearchValue;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param categoryDate the categoryDate to set
	 */
	public void setCategoryDate(String categoryDate) {
		this.categoryDate = categoryDate;
	}
	/**
	 * @return the categoryDate
	 */
	public String getCategoryDate() {
		return categoryDate;
	}

}
