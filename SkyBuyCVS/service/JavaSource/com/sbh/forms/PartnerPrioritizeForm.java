package com.sbh.forms;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.sbh.vo.PartnerVO;
import com.sbh.vo.ProductDetailsVO;

public class PartnerPrioritizeForm extends ActionForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 20091119L;
	private List<PartnerVO>  recordSet;
	private String searchBy;
	private String searchValue;
	private String status;
	
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	
	/**
	 * @return the recordSet
	 */
	public List<PartnerVO> getRecordSet() {
		return recordSet;
	}
	/**
	 * @param recordSet the recordSet to set
	 */
	public void setRecordSet(List<PartnerVO> recordSet) {
		this.recordSet = recordSet;
	}
	
	
	public PartnerVO getPartnerDetails(int index) {
		if(recordSet.size()<=index){
			recordSet.add(new PartnerVO());
		}

		return (PartnerVO)recordSet.get(index);
	}
	public void setPartnerDetails(int index, PartnerVO oPartnerDetails) {
		if(recordSet.size()<=index){
			recordSet.add(new PartnerVO());
		}
		recordSet.set(index, oPartnerDetails);
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
