package com.sbh.forms;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class OrderItemForm extends ActionForm implements Serializable{
	
	/**
	 * Serial Version User Id
	 */
	private static final long serialVersionUID = 1L;
	
	private String orderItemStatus;
	private String prodCode;
	private String cateId;
	private String qty;
	private String vendPrice;
	private String sbhPrice;
	private String orderCreatedDt;
	private String custFirstName;
	private String custLastName;
	private String custId;
	private String custEmail;
	private String custPhone;
	private String custTransId;
	private String airName;
	private String FlightNo;
	private String orderId;
	private String orderItemId;
	private String custAddress1;
	private String custAddress2;
	private String custCity;
	private String custState;
	private String custCountry;
	private String custZip;
	private String custShipFirstName;
	private String custShipLastName;
	private String custShipAddress1;
	private String custShipAddress2;
	private String custShipCity;
	private String custShipState;
	private String custShipCountry;
	private String custShipZip;
	private String custShipEmail;
	private String custShipPhone;
	private String vendorName;
	private String prodId;
	private String ownerId;
	private String ownerType;
	private String itemName;
	private String brandName;
	private String mainImgPath;
	private String mainImgType;
	private String mainImgCaption;
	private String cardType;
	private String creditCardNo;
	private String creditCardHolderName;
	private String expMonth;
	private String expYear;
	private String orderTrackingDetail;
	private String rejectReason;
	private String travelDate;
	private String cvv;
	private String cvvStatus;
	private String reasonForFailure;
	private String reasonForReject;
	private String shipmentStatus;
	private String color;
	private String size;
	private String creditCardLFD;//Credit Card's Last Four digits.
	private String expDate;
	/**
	 * @return the expDate
	 */
	public String getExpDate() {
		return expDate;
	}
	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the rejectReason
	 */
	public String getRejectReason() {
		return rejectReason;
	}
	/**
	 * @param rejectReason the rejectReason to set
	 */
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	/**
	 * @return the trackingDetail
	 */
	public String getOrderTrackingDetail() {
		return orderTrackingDetail;
	}
	/**
	 * @param trackingDetail the trackingDetail to set
	 */
	public void setOrderTrackingDetail(String orderTrackingDetail) {
		this.orderTrackingDetail = orderTrackingDetail;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the creditCardNo
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}
	/**
	 * @param creditCardNo the creditCardNo to set
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}
	/**
	 * @return the creditCardHolderName
	 */
	public String getCreditCardHolderName() {
		return creditCardHolderName;
	}
	/**
	 * @param creditCardHolderName the creditCardHolderName to set
	 */
	public void setCreditCardHolderName(String creditCardHolderName) {
		this.creditCardHolderName = creditCardHolderName;
	}
	/**
	 * @return the expMonth
	 */
	public String getExpMonth() {
		return expMonth;
	}
	/**
	 * @param expMonth the expMonth to set
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}
	/**
	 * @return the expYear
	 */
	public String getExpYear() {
		return expYear;
	}
	/**
	 * @param expYear the expYear to set
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}	
	public String getCateId() {
		return cateId;
	}
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getVendPrice() {
		return vendPrice;
	}
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	public String getSbhPrice() {
		return sbhPrice;
	}
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	public String getOrderCreatedDt() {
		return orderCreatedDt;
	}
	public void setOrderCreatedDt(String orderCreatedDt) {
		this.orderCreatedDt = orderCreatedDt;
	}
	public String getOrderItemStatus() {
		return orderItemStatus;
	}
	public void setOrderItemStatus(String orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}
	
	/**
	 * @return the custEmail
	 */
	public String getCustEmail() {
		return custEmail;
	}
	/**
	 * @param custEmail the custEmail to set
	 */
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return FlightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		FlightNo = flightNo;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the custZip
	 */
	public String getCustZip() {
		return custZip;
	}
	/**
	 * @param custZip the custZip to set
	 */
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}
	
	/**
	 * @return the custCity
	 */
	public String getCustCity() {
		return custCity;
	}
	/**
	 * @param custCity the custCity to set
	 */
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	/**
	 * @return the custState
	 */
	public String getCustState() {
		return custState;
	}
	/**
	 * @param custState the custState to set
	 */
	public void setCustState(String custState) {
		this.custState = custState;
	}
	/**
	 * @return the custCountry
	 */
	public String getCustCountry() {
		return custCountry;
	}
	/**
	 * @param custCountry the custCountry to set
	 */
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	/**
	 * @return the custShipCity
	 */
	public String getCustShipCity() {
		return custShipCity;
	}
	/**
	 * @param custShipCity the custShipCity to set
	 */
	public void setCustShipCity(String custShipCity) {
		this.custShipCity = custShipCity;
	}
	/**
	 * @return the custShipState
	 */
	public String getCustShipState() {
		return custShipState;
	}
	/**
	 * @param custShipState the custShipState to set
	 */
	public void setCustShipState(String custShipState) {
		this.custShipState = custShipState;
	}
	/**
	 * @return the custShipCountry
	 */
	public String getCustShipCountry() {
		return custShipCountry;
	}
	/**
	 * @param custShipCountry the custShipCountry to set
	 */
	public void setCustShipCountry(String custShipCountry) {
		this.custShipCountry = custShipCountry;
	}
	/**
	 * @return the custShipZip
	 */
	public String getCustShipZip() {
		return custShipZip;
	}
	/**
	 * @param custShipZip the custShipZip to set
	 */
	public void setCustShipZip(String custShipZip) {
		this.custShipZip = custShipZip;
	}
	/**
	 * @return the custShipEmail
	 */
	public String getCustShipEmail() {
		return custShipEmail;
	}
	/**
	 * @param custShipEmail the custShipEmail to set
	 */
	public void setCustShipEmail(String custShipEmail) {
		this.custShipEmail = custShipEmail;
	}
	/**
	 * @return the custShipPhone
	 */
	public String getCustShipPhone() {
		return custShipPhone;
	}
	/**
	 * @param custShipPhone the custShipPhone to set
	 */
	public void setCustShipPhone(String custShipPhone) {
		this.custShipPhone = custShipPhone;
	}	
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the custFirstName
	 */
	public String getCustFirstName() {
		return custFirstName;
	}
	/**
	 * @param custFirstName the custFirstName to set
	 */
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	/**
	 * @return the custLastName
	 */
	public String getCustLastName() {
		return custLastName;
	}
	/**
	 * @param custLastName the custLastName to set
	 */
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	/**
	 * @return the custShipFirstName
	 */
	public String getCustShipFirstName() {
		return custShipFirstName;
	}
	/**
	 * @param custShipFirstName the custShipFirstName to set
	 */
	public void setCustShipFirstName(String custShipFirstName) {
		this.custShipFirstName = custShipFirstName;
	}
	/**
	 * @return the custShipLastName
	 */
	public String getCustShipLastName() {
		return custShipLastName;
	}
	/**
	 * @param custShipLastName the custShipLastName to set
	 */
	public void setCustShipLastName(String custShipLastName) {
		this.custShipLastName = custShipLastName;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return prodId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the mainImgPath
	 */
	public String getMainImgPath() {
		return mainImgPath;
	}
	/**
	 * @param mainImgPath the mainImgPath to set
	 */
	public void setMainImgPath(String mainImgPath) {
		this.mainImgPath = mainImgPath;
	}
	/**
	 * @return the mainImgType
	 */
	public String getMainImgType() {
		return mainImgType;
	}
	/**
	 * @param mainImgType the mainImgType to set
	 */
	public void setMainImgType(String mainImgType) {
		this.mainImgType = mainImgType;
	}
	/**
	 * @return the mainImgCaption
	 */
	public String getMainImgCaption() {
		return mainImgCaption;
	}
	/**
	 * @param mainImgCaption the mainImgCaption to set
	 */
	public void setMainImgCaption(String mainImgCaption) {
		this.mainImgCaption = mainImgCaption;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the custAddress1
	 */
	public String getCustAddress1() {
		return custAddress1;
	}
	/**
	 * @param custAddress1 the custAddress1 to set
	 */
	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}
	/**
	 * @return the custShipAddress1
	 */
	public String getCustShipAddress1() {
		return custShipAddress1;
	}
	/**
	 * @param custShipAddress1 the custShipAddress1 to set
	 */
	public void setCustShipAddress1(String custShipAddress1) {
		this.custShipAddress1 = custShipAddress1;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.orderTrackingDetail = null;
	}
	/**
	 * @return the travelDate
	 */
	public String getTravelDate() {
		return travelDate;
	}
	/**
	 * @param travelDate the travelDate to set
	 */
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	/**
	 * @return the custAddress2
	 */
	public String getCustAddress2() {
		return custAddress2;
	}
	/**
	 * @param custAddress2 the custAddress2 to set
	 */
	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}
	/**
	 * @return the custShipAddress2
	 */
	public String getCustShipAddress2() {
		return custShipAddress2;
	}
	/**
	 * @param custShipAddress2 the custShipAddress2 to set
	 */
	public void setCustShipAddress2(String custShipAddress2) {
		this.custShipAddress2 = custShipAddress2;
	}
	/**
	 * @return the reasonForFailure
	 */
	public String getReasonForFailure() {
		return reasonForFailure;
	}
	/**
	 * @param reasonForFailure the reasonForFailure to set
	 */
	public void setReasonForFailure(String reasonForFailure) {
		this.reasonForFailure = reasonForFailure;
	}
	/**
	 * @return the reasonForReject
	 */
	public String getReasonForReject() {
		return reasonForReject;
	}
	/**
	 * @param reasonForReject the reasonForReject to set
	 */
	public void setReasonForReject(String reasonForReject) {
		this.reasonForReject = reasonForReject;
	}
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	/**
	 * @return the cvvStatus
	 */
	public String getCvvStatus() {
		return cvvStatus;
	}
	/**
	 * @param cvvStatus the cvvStatus to set
	 */
	public void setCvvStatus(String cvvStatus) {
		this.cvvStatus = cvvStatus;
	}
	/**
	 * @return the shipmentStatus
	 */
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	/**
	 * @param shipmentStatus the shipmentStatus to set
	 */
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	/**
	 * @return the creditCardLFD
	 */
	public String getCreditCardLFD() {
		return creditCardLFD;
	}
	/**
	 * @param creditCardLFD the creditCardLFD to set
	 */
	public void setCreditCardLFD(String creditCardLFD) {
		this.creditCardLFD = creditCardLFD;
	}

}