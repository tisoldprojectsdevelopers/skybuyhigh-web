package com.sbh.forms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class SearchOrderForm extends ActionForm implements Serializable{
	
	
	private String searchBy ;
	private String searchValue;
	private String category;
	private String orderFromDate;
	private String orderToDate;
	private String orderStatus;
	
	
	public  SearchOrderForm(){
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    java.util.Date date = new java.util.Date();
        String sCurrentDate = dateFormat.format(date);
        // System.out.println("Current Date: " + sCurrentDate);   
        searchBy ="ALL";
        searchValue="";
        category="ALL";
        orderFromDate="";
        orderToDate = sCurrentDate;
        orderStatus ="ALL";
	}
	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the searchValue
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * @param searchValue the searchValue to set
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return orderStatus;
	}
	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * @return the orderFromDate
	 */
	public String getOrderFromDate() {
		return orderFromDate;
	}
	/**
	 * @param orderFromDate the orderFromDate to set
	 */
	public void setOrderFromDate(String orderFromDate) {
		this.orderFromDate = orderFromDate;
	}
	/**
	 * @return the orderToDate
	 */
	public String getOrderToDate() {
		return orderToDate;
	}
	/**
	 * @param orderToDate the orderToDate to set
	 */
	public void setOrderToDate(String orderToDate) {
		this.orderToDate = orderToDate;
	}

}
