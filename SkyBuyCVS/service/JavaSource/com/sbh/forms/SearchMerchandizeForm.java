
package com.sbh.forms;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;


import com.sbh.vo.ProductDetailsVO;

public class SearchMerchandizeForm extends ActionForm implements Serializable{
	/**
	 * Serial Version User Id.
	 */
	private static final long serialVersionUID = 1L;
	private List<ProductDetailsVO>  recordSet;
	private String searchBy;
	private String searchValue;
	private String category;
	private String effectiveDate;
	private String prodStatus;
	private String sbhStatus;
	private String productType;
	private String uploadFromDate;
	private String uploadToDate;
	
	
	public  SearchMerchandizeForm(){
		recordSet=new ArrayList<ProductDetailsVO>();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	    java.util.Date date = new java.util.Date();
        String sCurrentDate = dateFormat.format(date);
        // System.out.println("Current Date: " + sCurrentDate);   
        effectiveDate=sCurrentDate;
        searchBy="All";
        prodStatus="All";
        category="All";
        uploadToDate = sCurrentDate; 
        sbhStatus="ALL";
        productType="ALL";
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public String getSearchValue() {
		return searchValue;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory() {
		return category;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setProdStatus(String prodStatus) {
		this.prodStatus = prodStatus;
	}
	public String getProdStatus() {
		return prodStatus;
	}
	
	public void setRecordSet(List<ProductDetailsVO> recordSet){
		if(recordSet!=null){
		this.recordSet=recordSet;
		}
	}
	
	public List<ProductDetailsVO> getRecordSet( ) {
		return recordSet;
	}
	public ProductDetailsVO getMerchandizeDetails(int index) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}

		return (ProductDetailsVO)recordSet.get(index);
	}
	public void setMerchandizeDetails(int index, ProductDetailsVO merchandizeDetails) {
		if(recordSet.size()<=index){
			recordSet.add(new ProductDetailsVO());
		}
		recordSet.set(index, merchandizeDetails);
	}

	public void reset( ) {
		recordSet.clear( );

	}
	/**
	 * @return the sbhStatus
	 */
	public String getSbhStatus() {
		return sbhStatus;
	}
	/**
	 * @param sbhStatus the sbhStatus to set
	 */
	public void setSbhStatus(String sbhStatus) {
		this.sbhStatus = sbhStatus;
	}
	/**
	 * @return the uploadFromDate
	 */
	public String getUploadFromDate() {
		return uploadFromDate;
	}
	/**
	 * @param uploadFromDate the uploadFromDate to set
	 */
	public void setUploadFromDate(String uploadFromDate) {
		this.uploadFromDate = uploadFromDate;
	}
	/**
	 * @return the uploadToDate
	 */
	public String getUploadToDate() {
		return uploadToDate;
	}
	/**
	 * @param uploadToDate the uploadToDate to set
	 */
	public void setUploadToDate(String uploadToDate) {
		this.uploadToDate = uploadToDate;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	

}
