package com.sbh.forms;

import java.io.Serializable;

import org.apache.struts.action.ActionForm;

public class DeviceRegForm extends ActionForm implements Serializable{
	
	/**
	 * Serial Version User Id
	 */
	private static final long serialVersionUID = 1L;
	private String deviceId;
	private String airCode;
	private String airId;
	private String deviceType;
	private String deviceSerialNo;
	private String macAddr;
	private String macDesc;
	private String devicePassword;
	private String deviceModel;
	private String processorId;
	private String status;
	private String isActive;
	private String productKey;
	private String deviceCode;
	private String productNo;
	private String adminVersion;
	private String catalogueVersion;
	
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	
	
	public String getProductKey() {
		return productKey;
	}
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * @return the deviceSerialNo
	 */
	public String getDeviceSerialNo() {
		return deviceSerialNo;
	}
	/**
	 * @param deviceSerialNo the deviceSerialNo to set
	 */
	public void setDeviceSerialNo(String deviceSerialNo) {
		this.deviceSerialNo = deviceSerialNo;
	}	
	
	
	/**
	 * @return the devicePassword
	 */
	public String getDevicePassword() {
		return devicePassword;
	}
	/**
	 * @param devicePassword the devicePassword to set
	 */
	public void setDevicePassword(String devicePassword) {
		this.devicePassword = devicePassword;
	}
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	/**
	 * @return the airCode
	 */
	public String getAirCode() {
		return airCode;
	}
	/**
	 * @param airCode the airCode to set
	 */
	public void setAirCode(String airCode) {
		this.airCode = airCode;
	}
	/**
	 * @return the macAddr
	 */
	public String getMacAddr() {
		return macAddr;
	}
	/**
	 * @param macAddr the macAddr to set
	 */
	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}
	/**
	 * @return the macDesc
	 */
	public String getMacDesc() {
		return macDesc;
	}
	/**
	 * @param macDesc the macDesc to set
	 */
	public void setMacDesc(String macDesc) {
		this.macDesc = macDesc;
	}
	/**
	 * @return the processorId
	 */
	public String getProcessorId() {
		return processorId;
	}
	/**
	 * @param processorId the processorId to set
	 */
	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the productNo
	 */
	public String getProductNo() {
		return productNo;
	}
	/**
	 * @param productNo the productNo to set
	 */
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	/**
	 * @return the adminVersion
	 */
	public String getAdminVersion() {
		return adminVersion;
	}
	/**
	 * @param adminVersion the adminVersion to set
	 */
	public void setAdminVersion(String adminVersion) {
		this.adminVersion = adminVersion;
	}
	/**
	 * @return the catalogueVersion
	 */
	public String getCatalogueVersion() {
		return catalogueVersion;
	}
	/**
	 * @param catalogueVersion the catalogueVersion to set
	 */
	public void setCatalogueVersion(String catalogueVersion) {
		this.catalogueVersion = catalogueVersion;
	}
	

}
