	package com.sbh.webservice;

	import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.LogManager;
	import org.apache.log4j.Logger;
import org.jvnet.jax_ws_commons.thread_scope.ThreadScope;

import com.sbh.client.dao.PlaceOrderDAO;

@javax.jws.WebService(
targetNamespace = 
	"http://webservice.sbh.com/"
,
serviceName = 
	"PlaceOrderService"
, 
portName =
	"device/PlaceOrderPort"
	,wsdlLocation = "WEB-INF/wsdl/PlaceOrderService.wsdl"
)


@ThreadScope
public class PlaceOrderDelegate {
	@Resource
	private WebServiceContext ctx;
	com.sbh.webservice.PlaceOrder placeOrder = new com.sbh.webservice.PlaceOrder();

			public String placeOrder(String p_sOrderType, String p_sOrderDetails)  {
				String sIPAddress = null;
				MessageContext mc = ctx.getMessageContext();
				HttpServletRequest request = (javax.servlet.http.HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST);
				sIPAddress = request.getRemoteHost();
				sIPAddress = sIPAddress == null?"":sIPAddress.trim();
			return placeOrder.placeOrder(p_sOrderType,p_sOrderDetails,sIPAddress);
		}
	
}