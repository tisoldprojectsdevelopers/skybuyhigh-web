package com.sbh.webservice;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.client.dao.PlaceOrderDAO;
public class PlaceOrder {
	private static Logger logger = LogManager.getLogger(PlaceOrder.class);

	public String placeOrder(String p_sOrderType,String p_sOrderDetails,String p_sIpAddress) {
		logger.info("PlaceOrder::placeOrder:ENTER");
		
		String sPlaceOrderDetails = "";
		try {
			if(p_sOrderDetails != null && p_sOrderDetails.trim().length() > 0) {
				sPlaceOrderDetails = PlaceOrderDAO.placeOrderDetails(p_sOrderType, p_sOrderDetails, p_sIpAddress);
			}
		}catch(Exception e) {
			return e.getMessage();
		}
		
		logger.info("PlaceOrder::placeOrder:EXIT");
		return sPlaceOrderDetails;
	}
}
