package com.sbh.webservice;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.jvnet.jax_ws_commons.thread_scope.ThreadScope;

@javax.jws.WebService(targetNamespace = "http://webservice.sbh.com/", serviceName = "UpdateCustomerInfoService", portName = "device/UpdateCustomerInfoPort", wsdlLocation = "WEB-INF/wsdl/UpdateCustomerInfoService.wsdl")
@ThreadScope
public class UpdateCustomerInfoDelegate {
	@Resource
	private WebServiceContext ctx;
	com.sbh.webservice.UpdateCustomerInfo updateCustomerInfo = new com.sbh.webservice.UpdateCustomerInfo();

	public String updateCustomerFeedBack(String p_sOrderDetails)throws Exception {
		String sIPAddress = null;
		MessageContext mc = ctx.getMessageContext();
		HttpServletRequest request = (javax.servlet.http.HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST);
		sIPAddress = request.getRemoteHost();
		sIPAddress = sIPAddress == null?"":sIPAddress.trim();
		return updateCustomerInfo.updateCustomerFeedBack(p_sOrderDetails,sIPAddress);
	}

}