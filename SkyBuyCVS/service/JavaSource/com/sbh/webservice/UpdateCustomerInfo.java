package com.sbh.webservice;

import java.net.InetAddress;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.client.dao.PlaceOrderDAO;
import com.sbh.contants.SkyBuyContants;
import com.sbh.dao.CatalogueDAO;

public class UpdateCustomerInfo {
	private static Logger logger = LogManager.getLogger(UpdateCustomerInfo.class);
	
	public  String updateCustomerFeedBack(String p_sOrderDetails,String p_sIPAddress)throws Exception{
	logger.info("UpdateCustomerInfo::updateCustomerFeedBack:ENTER");
		
	String sRemoteHostName = InetAddress.getLocalHost().getHostName();
	String sPlaceOrderDetails=null;
	int iIsUpdate;
	try{
		if(p_sOrderDetails != null && p_sOrderDetails.trim().length() > 0) {
			iIsUpdate =	PlaceOrderDAO.updateOrderDetails(p_sOrderDetails,sRemoteHostName, p_sIPAddress);
			if(iIsUpdate>0){
				sPlaceOrderDetails=SkyBuyContants.ORDER_PLACED;
				}
			else{
				sPlaceOrderDetails=SkyBuyContants.ORDER_FAILED;
			}
			
		}
	}catch(Exception e){
		logger.error("UpdateCustomerInfo::updateCustomerFeedBack:Exception: "+e.getMessage());
	}
	logger.info("UpdateCustomerInfo::updateCustomerFeedBack:EXIT");
	return sPlaceOrderDetails;
}
}

