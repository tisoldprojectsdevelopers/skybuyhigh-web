
package com.sbh.payment.txn;

import java.io.*;
import java.security.KeyStore;
import java.util.*;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.sbh.dao.PaymentDAO;
import com.sbh.vo.CCStoreInfoVO;
import com.sbh.vo.CustomerOrderInfo;
import com.sbh.vo.OrderItemDetails;
import com.sbh.vo.PaymentResponse;

import lp.order.LPOrderFactory;
import lp.order.LPOrderPart;


public class JLinkPointCreditCardTxn {



	public JLinkPointCreditCardTxn(String _clientCertPath, String _configfile,
			String _host, String _password, int _port)
	{
		clientCertPath = _clientCertPath;
		password = _password;
		host = _host;
		configfile = _configfile;
		port = _port;
	}

	public JLinkPointCreditCardTxn()
	{
		try{
			CCStoreInfoVO oCCStoreInfoVO = PaymentDAO.getCCStoreInfo();
			if(oCCStoreInfoVO != null){
				clientCertPath = oCCStoreInfoVO.getClientCertificatePath();
				password = oCCStoreInfoVO.getPassword();
				host = oCCStoreInfoVO.getHost();
				configfile =oCCStoreInfoVO.getStoreNumber();
				if(oCCStoreInfoVO.getPort()!=null && !oCCStoreInfoVO.getPort().equalsIgnoreCase(""))
					port = Integer.parseInt(oCCStoreInfoVO.getPort());
				else
					port = 0;
			}
			/*ResourceBundle oResourceBundle= ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources")	;
			clientCertPath = oResourceBundle.getString("ClientCertificatePath");;
			password = oResourceBundle.getString("Password");
			host =oResourceBundle.getString("Host");
			configfile =oResourceBundle.getString("ConfigFile"); 
			port = Integer.parseInt((String)oResourceBundle.getString("Port"));*/
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	public PaymentResponse process(CustomerOrderInfo p_oCustomerInfo)
	{

		PaymentResponse oPaymentResponse=null;
		String sResponse = "";
		String sOrderDetails=null;
		boolean ret=false;
		try{
			if(p_oCustomerInfo.getOrderType().equalsIgnoreCase("PREAUTH")){
				sOrderDetails=getPreAuthOrderXML(p_oCustomerInfo);
				sOrderDetails=sOrderDetails==null?"":sOrderDetails.trim();								
			}else if(p_oCustomerInfo.getOrderType().equalsIgnoreCase("POSTAUTH")){
				sOrderDetails=getPostAuthOrderXML(p_oCustomerInfo);
				sOrderDetails=sOrderDetails==null?"":sOrderDetails.trim();
			}else if(p_oCustomerInfo.getOrderType().equalsIgnoreCase("SALE")){
				sOrderDetails=getSaleOrderXML(p_oCustomerInfo);
				sOrderDetails=sOrderDetails==null?"":sOrderDetails.trim();								
			}
			else if(p_oCustomerInfo.getOrderType().equalsIgnoreCase("CREDIT")){
				sOrderDetails=getReturnOrderXML(p_oCustomerInfo);
				sOrderDetails=sOrderDetails==null?"":sOrderDetails.trim();
			}
			 
			sResponse = send(sOrderDetails);
			sResponse = sResponse ==null?"":sResponse.trim();


		}
		catch (Exception e)
		{
			e.printStackTrace();
			sResponse = "<r_error>"+e.toString()+"</r_error>";
		}

		oPaymentResponse=parseResponse(sResponse);
		oPaymentResponse.setOutBoundMsg(sOrderDetails);	
		oPaymentResponse.setInBoundMsg(sResponse);	

		return oPaymentResponse;
	}

	private String getPreAuthOrderXML(CustomerOrderInfo p_oCustomerInfo){
		OrderItemDetails oOrderItemDetails=null;
		// Create an empty order
		LPOrderPart  order = LPOrderFactory.createOrderPart("order");

		// Create a part we will use to build the order
		LPOrderPart  op = LPOrderFactory.createOrderPart();

		// Build 'orderoptions'
		// For a test, set result to GOOD, DECLINE, or DUPLICATE
		// op.put("result","GOOD");
		op.put("ordertype",p_oCustomerInfo.getOrderType());
		// add 'orderoptions to order
		order.addPart("orderoptions", op );


		// Build 'merchantinfo'
		op.clear();
		op.put("configfile",p_oCustomerInfo.getStoreNumber());
		// add 'merchantinfo to order
		order.addPart("merchantinfo", op );

		// Build 'transactiondetails'
		op.clear();
		//op.put("oid",p_oCustomerInfo.getPaymentTxnRefId());
		op.put("ponumber",p_oCustomerInfo.getOrderConfirmationNo());
		// If the purchase is tax exempt,
		// pass a value of Y for taxexempt
		op.put("taxexempt",p_oCustomerInfo.getTaxexempt());

		op.put("ip",p_oCustomerInfo.getIpAddress());


		order.addPart("transactiondetails", op );


		// Build 'creditcard'
		op.clear();
		op.put("cardnumber",p_oCustomerInfo.getCardNumber());
		op.put("cardexpmonth",p_oCustomerInfo.getCardExpMonth());
		op.put("cardexpyear",p_oCustomerInfo.getCardExpYear());
		if(p_oCustomerInfo.getCvv()!=null && !p_oCustomerInfo.getCvv().equalsIgnoreCase(""))
			op.put("cvmvalue",p_oCustomerInfo.getCvv());
		
		// add 'creditcard to order
		order.addPart("creditcard", op );

		/*// Build 'billing'
			op.clear();
			op.put("addrnum",p_oCustomerInfo.getAddrNum());
			op.put("zip",p_oCustomerInfo.getZip());
			// add 'billing to order
			order.addPart("billing", op );*/

		// Build 'payment'
		op.clear();
		op.put("chargetotal",p_oCustomerInfo.getChargeTotal());
		// add 'payment to order
		order.addPart("payment", op );
		
		op.clear();
		op.put("zip",p_oCustomerInfo.getZip());
		op.put("addrnum",p_oCustomerInfo.getAddrNum());

		op.put("name",p_oCustomerInfo.getBillingCustName());
		op.put("address1",p_oCustomerInfo.getBillingAddr1());
		op.put("address2",p_oCustomerInfo.getBillingAddr2());
		op.put("city",p_oCustomerInfo.getBillingCity());
		op.put("state",p_oCustomerInfo.getBillingState());
		op.put("country",p_oCustomerInfo.getBillingCountry());
		op.put("phone",p_oCustomerInfo.getBillingPhone());
		op.put("email",p_oCustomerInfo.getBillingEmail());
		// add 'billing to order
		order.addPart("billing", op );


		// build shipping
		op.clear();
		op.put("name",p_oCustomerInfo.getShippingCustName());
		op.put("address1",p_oCustomerInfo.getShippingAddr1());
		op.put("address2",p_oCustomerInfo.getShippingAddr2());
		op.put("city",p_oCustomerInfo.getShippingCity());
		op.put("state",p_oCustomerInfo.getShippingState());
		op.put("country",p_oCustomerInfo.getShippingCountry());
		op.put("zip",p_oCustomerInfo.getShippingZip());
		order.addPart("shipping",op);
		
		// Special Instructions to shipper.
		op.clear();
		op.put("comments", p_oCustomerInfo.getCustSpecialInstruction());
		order.addPart("notes",op);
		
		oOrderItemDetails=p_oCustomerInfo.getOrderItemDetails();
		// Create some parts we use to build order itmes
		if(oOrderItemDetails!=null){
			LPOrderPart items = LPOrderFactory.createOrderPart();
			LPOrderPart item = LPOrderFactory.createOrderPart();
			LPOrderPart options = LPOrderFactory.createOrderPart();

			// build and build 'item1'
			item.put("id",oOrderItemDetails.getId());
			item.put("description",oOrderItemDetails.getDescription());
			item.put("quantity",oOrderItemDetails.getQuantity());
			item.put("price",oOrderItemDetails.getPrice());
			// add 'item' to 'items' collection
			items.addPart("item", item, 1 );
			// add 'items' to order
			order.addPart("items", items );
		}
		return order.toXML();
	}
	/*private  String getPostAuthOrderXML(CustomerOrderInfo p_oCustomerInfo){
		// Create an empty order
		LPOrderPart  order = LPOrderFactory.createOrderPart("order");

		// Create a part we use to build the order
		LPOrderPart  op = LPOrderFactory.createOrderPart();

		// Build 'orderoptions'
		// For a test, set result to GOOD, DECLINE, or DUPLICATE
		//op.put("result","GOOD");
		op.put("ordertype",p_oCustomerInfo.getOrderType());
		// add 'orderoptions to order
		order.addPart("orderoptions", op );


		// Build 'merchantinfo'
		op.clear();
		op.put("configfile",p_oCustomerInfo.getStoreNumber());
		// add 'merchantinfo to order
		order.addPart("merchantinfo", op );


		// Build 'creditcard'
		op.clear();
		op.put("cardnumber",p_oCustomerInfo.getCardNumber());
		op.put("cardexpmonth",p_oCustomerInfo.getCardExpMonth());
		op.put("cardexpyear",p_oCustomerInfo.getCardExpYear());
		// add 'creditcard to order
		order.addPart("creditcard", op );


		// Build 'payment'
		op.clear();
		op.put("chargetotal",p_oCustomerInfo.getChargeTotal());
		// add 'payment to order
		order.addPart("payment", op );

		// Add oid
		op.clear();
		op.put("oid",p_oCustomerInfo.getOrderId());
		// add 'transactiondetails to order
		order.addPart("transactiondetails", op );


		return order.toXML();
	}
	 */

	protected String getPostAuthOrderXML(CustomerOrderInfo p_oCustomerInfo) {
		OrderItemDetails oOrderItemDetails=null;
		// Create an empty order
		LPOrderPart      order = LPOrderFactory.createOrderPart("order");

		// Create a part we will use to build the order
		LPOrderPart  op = LPOrderFactory.createOrderPart();

		// Build 'orderoptions'
		op.put("ordertype",p_oCustomerInfo.getOrderType());
		// live transaction
		//op.put("result","LIVE");
		// add 'orderoptions to order
		order.addPart("orderoptions", op );

		// Build 'merchantinfo'
		op.clear();
		op.put("configfile",p_oCustomerInfo.getStoreNumber());
		// add 'merchantinfo to order
		order.addPart("merchantinfo", op );


		// Build 'transactiondetails'
		op.clear();
		op.put("oid",p_oCustomerInfo.getPaymentTxnRefId());
		op.put("ponumber",p_oCustomerInfo.getOrderConfirmationNo());
		// If the purchase is tax exempt,
		// pass a value of Y for taxexempt
		op.put("taxexempt",p_oCustomerInfo.getTaxexempt());

		op.put("ip",p_oCustomerInfo.getIpAddress());

		order.addPart("transactiondetails", op );

		// Build 'billing'
		// Required for AVS. If not provided,
		// transactions will downgrade.
		op.clear();
		op.put("zip",p_oCustomerInfo.getZip());
		op.put("addrnum",p_oCustomerInfo.getAddrNum());

		op.put("name",p_oCustomerInfo.getBillingCustName());
		op.put("address1",p_oCustomerInfo.getBillingAddr1());
		op.put("address2",p_oCustomerInfo.getBillingAddr2());
		op.put("city",p_oCustomerInfo.getBillingCity());
		op.put("state",p_oCustomerInfo.getBillingState());
		op.put("country",p_oCustomerInfo.getBillingCountry());
		op.put("phone",p_oCustomerInfo.getBillingPhone());
		op.put("email",p_oCustomerInfo.getBillingEmail());
		// add 'billing to order
		order.addPart("billing", op );


		// build shipping
		op.clear();
		op.put("name",p_oCustomerInfo.getShippingCustName());
		op.put("address1",p_oCustomerInfo.getShippingAddr1());
		op.put("address2",p_oCustomerInfo.getShippingAddr2());
		op.put("city",p_oCustomerInfo.getShippingCity());
		op.put("state",p_oCustomerInfo.getShippingState());
		op.put("country",p_oCustomerInfo.getShippingCountry());
		op.put("zip",p_oCustomerInfo.getShippingZip());
		order.addPart("shipping",op);
		
		// Special Instructions to shipper.
		op.clear();
		op.put("comments", p_oCustomerInfo.getCustSpecialInstruction());
		order.addPart("notes",op);
		
		// Build 'creditcard'
		op.clear();
		op.put("cardnumber",p_oCustomerInfo.getCardNumber());
		op.put("cardexpmonth",p_oCustomerInfo.getCardExpMonth());
		op.put("cardexpyear",p_oCustomerInfo.getCardExpYear());
		/* op.put("cvmvalue","123");
        op.put("cvmindicator","not_provided");*/
		// add 'creditcard to order
		order.addPart("creditcard", op );

		// Build 'payment'
		op.clear();
		/* op.put("subtotal","12.99");
      op.put("tax","0.34");
      op.put("shipping","1.45");
      op.put("vattax","0.00");*/
		op.put("chargetotal",p_oCustomerInfo.getChargeTotal());
		// add 'payment to order
		order.addPart("payment", op );

		// add notes to order
		/*op.clear();
     op.put("referred","Saw ad on Web site.");
      op.put("comments","Repeat customer. Ship immediately.");
      order.addPart("notes",op);*/

		oOrderItemDetails=p_oCustomerInfo.getOrderItemDetails();
		// Create some parts we use to build order itmes
		if(oOrderItemDetails!=null){
			LPOrderPart items = LPOrderFactory.createOrderPart();
			LPOrderPart item = LPOrderFactory.createOrderPart();
			LPOrderPart options = LPOrderFactory.createOrderPart();

			// build and build 'item1'
			item.put("id",oOrderItemDetails.getId());
			item.put("description",oOrderItemDetails.getDescription());
			item.put("quantity",oOrderItemDetails.getQuantity());
			item.put("price",oOrderItemDetails.getPrice());
			// add 'item' to 'items' collection
			items.addPart("item", item, 1 );
			// add 'items' to order
			order.addPart("items", items );
		}
		return  order.toXML();
	}
	
	protected String getSaleOrderXML(CustomerOrderInfo p_oCustomerInfo) {
		OrderItemDetails oOrderItemDetails=null;
		// Create an empty order
		LPOrderPart      order = LPOrderFactory.createOrderPart("order");

		// Create a part we will use to build the order
		LPOrderPart  op = LPOrderFactory.createOrderPart();

		// Build 'orderoptions'
		op.put("ordertype",p_oCustomerInfo.getOrderType());
		// live transaction
		//op.put("result","LIVE");
		// add 'orderoptions to order
		order.addPart("orderoptions", op );

		// Build 'merchantinfo'
		op.clear();
		op.put("configfile",p_oCustomerInfo.getStoreNumber());
		// add 'merchantinfo to order
		order.addPart("merchantinfo", op );


		// Build 'transactiondetails'
		op.clear();
		//op.put("oid",p_oCustomerInfo.getPaymentTxnRefId());
		op.put("ponumber",p_oCustomerInfo.getOrderConfirmationNo());
		// If the purchase is tax exempt,
		// pass a value of Y for taxexempt
		op.put("taxexempt",p_oCustomerInfo.getTaxexempt());

		op.put("ip",p_oCustomerInfo.getIpAddress());

		order.addPart("transactiondetails", op );

		// Build 'billing'
		// Required for AVS. If not provided,
		// transactions will downgrade.
		op.clear();
		op.put("zip",p_oCustomerInfo.getZip());
		op.put("addrnum",p_oCustomerInfo.getAddrNum());

		op.put("name",p_oCustomerInfo.getBillingCustName());
		op.put("address1",p_oCustomerInfo.getBillingAddr1());
		op.put("address2",p_oCustomerInfo.getBillingAddr2());
		op.put("city",p_oCustomerInfo.getBillingCity());
		op.put("state",p_oCustomerInfo.getBillingState());
		op.put("country",p_oCustomerInfo.getBillingCountry());
		op.put("phone",p_oCustomerInfo.getBillingPhone());
		op.put("email",p_oCustomerInfo.getBillingEmail());
		// add 'billing to order
		order.addPart("billing", op );


		// build shipping
		op.clear();
		op.put("name",p_oCustomerInfo.getShippingCustName());
		op.put("address1",p_oCustomerInfo.getShippingAddr1());
		op.put("address2",p_oCustomerInfo.getShippingAddr2());
		op.put("city",p_oCustomerInfo.getShippingCity());
		op.put("state",p_oCustomerInfo.getShippingState());
		op.put("country",p_oCustomerInfo.getShippingCountry());
		op.put("zip",p_oCustomerInfo.getShippingZip());
		order.addPart("shipping",op);
		
		// Special Instructions to shipper.
		op.clear();
		op.put("comments", p_oCustomerInfo.getCustSpecialInstruction());
		order.addPart("notes",op);
		
		// Build 'creditcard'
		op.clear();
		op.put("cardnumber",p_oCustomerInfo.getCardNumber());
		op.put("cardexpmonth",p_oCustomerInfo.getCardExpMonth());
		op.put("cardexpyear",p_oCustomerInfo.getCardExpYear());

		if(p_oCustomerInfo.getCvv()!=null && !p_oCustomerInfo.getCvv().equalsIgnoreCase(""))
			op.put("cvmvalue",p_oCustomerInfo.getCvv());

		/* op.put("cvmvalue","123");
        op.put("cvmindicator","not_provided");*/
		// add 'creditcard to order
		order.addPart("creditcard", op );

		// Build 'payment'
		op.clear();
		/* op.put("subtotal","12.99");
      op.put("tax","0.34");
      op.put("shipping","1.45");
      op.put("vattax","0.00");*/
		op.put("chargetotal",p_oCustomerInfo.getChargeTotal());
		// add 'payment to order
		order.addPart("payment", op );

		// add notes to order
		/*op.clear();
     op.put("referred","Saw ad on Web site.");
      op.put("comments","Repeat customer. Ship immediately.");
      order.addPart("notes",op);*/

		oOrderItemDetails=p_oCustomerInfo.getOrderItemDetails();
		// Create some parts we use to build order itmes
		if(oOrderItemDetails!=null){
			LPOrderPart items = LPOrderFactory.createOrderPart();
			LPOrderPart item = LPOrderFactory.createOrderPart();
			LPOrderPart options = LPOrderFactory.createOrderPart();

			// build and build 'item1'
			item.put("id",oOrderItemDetails.getId());
			item.put("description",oOrderItemDetails.getDescription());
			item.put("quantity",oOrderItemDetails.getQuantity());
			item.put("price",oOrderItemDetails.getPrice());
			// add 'item' to 'items' collection
			items.addPart("item", item, 1 );
			// add 'items' to order
			order.addPart("items", items );
		}
		return  order.toXML();
	}
	protected String getReturnOrderXML(CustomerOrderInfo p_oCustomerInfo) {
		OrderItemDetails oOrderItemDetails=null;
		// Create an empty order
		LPOrderPart      order = LPOrderFactory.createOrderPart("order");

		// Create a part we will use to build the order
		LPOrderPart  op = LPOrderFactory.createOrderPart();

		// Build 'orderoptions'
		op.put("ordertype",p_oCustomerInfo.getOrderType());
		// live transaction
		//op.put("result","LIVE");
		// add 'orderoptions to order
		order.addPart("orderoptions", op );

		// Build 'merchantinfo'
		op.clear();
		op.put("configfile",p_oCustomerInfo.getStoreNumber());
		// add 'merchantinfo to order
		order.addPart("merchantinfo", op );


		// Build 'transactiondetails'
		op.clear();
		op.put("oid",p_oCustomerInfo.getPaymentTxnRefId());
		
	    op.put("ip",p_oCustomerInfo.getIpAddress());

		order.addPart("transactiondetails", op );

	    // Build 'creditcard'
		op.clear();
		op.put("cardnumber",p_oCustomerInfo.getCardNumber());
		op.put("cardexpmonth",p_oCustomerInfo.getCardExpMonth());
		op.put("cardexpyear",p_oCustomerInfo.getCardExpYear());
		order.addPart("creditcard", op );

	
		op.clear();
	    op.put("chargetotal",p_oCustomerInfo.getChargeTotal());
	    order.addPart("payment", op );

		return  order.toXML();
	}
	protected PaymentResponse parseResponse( String rsp )
	{
		String R_Time=null;
		String R_Ref=null;
		String R_Approved=null;
		String R_Code=null;
		String R_Authresr=null;
		String R_Error=null;
		String R_OrderNum=null;
		String R_Message=null;
		String R_Score=null;
		String R_TDate=null;
		String R_AVS=null;
		String R_FraudCode=null;
		String R_ESD=null;
		String R_Tax=null;
		String R_Shipping=null;
		String R_Error_Msg=null;
		String R_Error_Code=null;

		PaymentResponse oPaymentResponse=new PaymentResponse();
		R_Time = parseTag("r_time", rsp);
		R_Time=R_Time==null?"":R_Time.trim();
		oPaymentResponse.setR_Time(R_Time);

		R_Ref = parseTag("r_ref", rsp);
		R_Ref=R_Ref==null?"":R_Ref.trim();
		oPaymentResponse.setR_Ref(R_Ref);

		R_Approved = parseTag("r_approved", rsp);
		R_Approved=R_Approved==null?"":R_Approved.trim();
		oPaymentResponse.setR_Approved(R_Approved);

		R_Code = parseTag("r_code", rsp);
		R_Code=R_Code==null?"":R_Code.trim();
		oPaymentResponse.setR_Code(R_Code);

		R_Authresr = parseTag("r_authresronse", rsp);
		R_Authresr=R_Authresr==null?"":R_Authresr.trim();
		oPaymentResponse.setR_Authresr(R_Authresr);

		R_Error = parseTag("r_error", rsp);
		R_Error=R_Error==null?"":R_Error.trim();
		StringTokenizer oStringTokenizer=new StringTokenizer(R_Error,":");

		if(oStringTokenizer.hasMoreTokens()){
			R_Error_Code=oStringTokenizer.nextToken();
		}
		R_Error_Msg="";
		while(oStringTokenizer.hasMoreTokens()){
			R_Error_Msg=R_Error_Msg+oStringTokenizer.nextToken();
			if(oStringTokenizer.hasMoreTokens()){
				R_Error_Msg=R_Error_Msg+" : "; 
			
			}
		}
		R_Error_Msg=R_Error_Msg==null?"":R_Error_Msg.trim();
		R_Error_Code=R_Error_Code==null?"":R_Error_Code.trim();
		oPaymentResponse.setR_Error_Msg(R_Error_Msg);
		oPaymentResponse.setR_Error_Code(R_Error_Code);
		oPaymentResponse.setR_Error(R_Error);

		R_OrderNum = parseTag("r_ordernum", rsp);
		R_OrderNum=R_OrderNum==null?"":R_OrderNum.trim();
		oPaymentResponse.setR_OrderNum(R_OrderNum);

		R_Message = parseTag("r_message", rsp);
		R_Message=R_Message==null?"":R_Message.trim();
		oPaymentResponse.setR_Message(R_Message);

		R_Score = parseTag("r_score", rsp);
		R_Score=R_Score==null?"":R_Score.trim();
		oPaymentResponse.setR_Score(R_Score);

		R_TDate = parseTag("r_tdate", rsp);
		R_TDate=R_TDate==null?"":R_TDate.trim();
		oPaymentResponse.setR_TDate(R_TDate);

		R_AVS = parseTag("r_avs", rsp);
		R_AVS=R_AVS==null?"":R_AVS.trim();
		oPaymentResponse.setR_AVS(R_AVS);

		R_Tax = parseTag("r_tax", rsp);
		R_Tax=R_Tax==null?"":R_Tax.trim();
		oPaymentResponse.setR_Tax(R_Tax);

		R_Shipping = parseTag("r_shipping", rsp);
		R_Shipping=R_Shipping==null?"":R_Shipping.trim();
		oPaymentResponse.setR_Shipping(R_Shipping);

		R_FraudCode = parseTag("r_fraudCode", rsp);
		R_FraudCode=R_FraudCode==null?"":R_FraudCode.trim();
		oPaymentResponse.setR_FraudCode(R_FraudCode);

		R_ESD = parseTag("esd", rsp);
		R_ESD=R_ESD==null?"":R_ESD.trim();
		oPaymentResponse.setR_ESD(R_ESD);

		return oPaymentResponse;
	}

	protected String parseTag(String tag, String rsp )
	{   
		StringBuffer sb = new StringBuffer(256);
		sb.append('<' + tag + '>');
		int len = sb.length();
		int idxSt=-1,idxEnd=-1;
		if( -1 == (idxSt = rsp.indexOf(sb.toString())))
		{ return ""; }
		idxSt += len;
		sb.setLength(0);
		sb.append("</" + tag + '>');
		if( -1 == (idxEnd = rsp.indexOf(sb.toString(),idxSt)))
		{ return ""; }
		return rsp.substring(idxSt,idxEnd);
	}
	private String validate()
	{
		String sRet = null;
		if(clientCertPath == null ||clientCertPath.length() == 0)
			sRet = "Client certificate file path has not been specified.";
		if(password == null || password.length() == 0)
			sRet = "Password has not been specified.";
		if(host == null || host.length() == 0)
			sRet = "Host has not been specified.";
		if(port == 0)
			sRet = "Port number has not been specified.";
		if(sRet != null)
			sRet = "<r_error>" + sRet + "</r_error>";
		return sRet;
	}
	private String send(String sXml)
	throws Exception
	{
		String sRet = validate();
		if(sRet != null)
			return sRet;
		BufferedReader in = null;
		PrintWriter out = null;
		SSLSocket socket = null;
		try
		{
			SSLSocketFactory factory = null;
			try
			{
				SSLContext ctx = SSLContext.getInstance("TLS");
				KeyStore ks = KeyStore.getInstance("JKS");
				ks.load(null, null);
				KeyStore ks2 = KeyStore.getInstance("PKCS12", "SunJSSE");
				FileInputStream fin = new FileInputStream(clientCertPath);
				ks2.load(fin, password.toCharArray());
				KeyManagerFactory kmf = KeyManagerFactory.getInstance("SUNX509");

				kmf.init(ks2, password.toCharArray());


				fin.close();
				ctx.init(kmf.getKeyManagers(), null, null);
				factory = ctx.getSocketFactory();
			}
			catch(Exception e)
			{
				throw e;
			}
			socket = (SSLSocket)factory.createSocket(host, port);
			socket.startHandshake();
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
			out.println(sXml);
			out.println();
			out.flush();
			if(out.checkError())
			{
				try
				{
					out.close();
				}
				catch(Exception e1) { }
				try
				{
					socket.close();
				}
				catch(Exception e1) { }
				return "<r_error>SSLSocketClient: java.io.PrintWriter error</r_error>";
			}
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String sResp = "";
			String s;
			while((s = in.readLine()) != null) 
				if(s != null)
					sResp = sResp + s;
			in.close();
			out.close();
			socket.close();
			in = null;
			out = null;
			socket = null;
			return sResp;
		}
		catch(Exception e)
		{
			try
			{
				if(in != null)
					in.close();
			}
			catch(Exception e1) {
				// System.out.println("Socket-in "+e);
			}
			try
			{
				if(out != null)
					out.close();
			}
			catch(Exception e1) { 
				// System.out.println("Socket-out "+e);
			}
			try
			{
				if(socket != null)
					socket.close();
			}
			catch(Exception e1) { 
				// System.out.println("Socket "+e);
			}
			throw e;
		}
	}

	protected String clientCertPath="";
	protected String password="";
	protected String host="";
	protected String configfile="";
	protected int    port=0;

	// response holders


}