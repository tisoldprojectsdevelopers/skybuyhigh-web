package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.*;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;

import com.sbh.dao.AirlineDAO;
import com.sbh.dao.DeviceRegDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.forms.DeviceRegForm;
import com.sbh.forms.SearchDeviceForm;
import com.sbh.util.Utils;
import com.sbh.vo.AirlineRegVO;
import com.sbh.vo.AirlineVO;
import com.sbh.vo.DeviceAttendanceVO;
import com.sbh.vo.DeviceDetailsVO;
import com.sbh.vo.DeviceRegVO;
import com.sbh.vo.KeyVO;
import com.sbh.vo.LoginVO;

public class DeviceRegAction extends DispatchAction{
	private static Logger logger = LogManager.getLogger(AirlineRegAction.class);
	public ActionForward downloadProduct(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::downloadProduct:ENTER");
		String sFrdKey="deviceRegSuccess";	

		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();	
		try{
			logger.info("DeviceRegAction::deviceReg:ENTER");

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sAirId =  request.getParameter("airId");
			sAirId = sAirId ==null?"":sAirId.trim();

			String sMacAddress =  request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			String sMacDesc =  request.getParameter("macDesc");
			sMacDesc = sMacDesc ==null?"":sMacDesc.trim();

			String sProcessorId =  request.getParameter("processorId");
			sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

			boolean bStatus = DeviceRegDAO.validateDevice(sProductKey, sAirId, sMacAddress, "DP");

			if(bStatus){			
				DeviceRegDAO.updateMacAddress(sProductKey,sMacAddress,sMacDesc,sProcessorId);		
				// System.out.println("TRUE");					
				pw.println("TRUE");
				pw.flush();
			}else{
				pw.println(INVALID_AIRLINE_OR_PRODUCT_KEY);
				pw.flush();
			}

			logger.info("DeviceRegAction::downloadProduct:Exit");
		}catch(Exception e){		
			pw.println(PRODUCT_DOWNLOAD_PROCESS_FAILED);
			pw.flush();		
			e.printStackTrace();
			logger.error("DeviceRegAction::downloadProduct:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}


	public ActionForward deviceStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::deviceStatus:ENTER");
		String sFrdKey="deviceRegSuccess";	

		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();	
		try{
			// System.out.println("deviceStatus:Enter");	

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sMacAddress =  request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			ArrayList alDeviceStatus = DeviceRegDAO.getDeviceStatus(sProductKey,sMacAddress);

			String sDeviceStatus =(String)alDeviceStatus.get(0);
			String sAirlineStatus =(String)alDeviceStatus.get(1);

			// System.out.println("sProductKey :"+sProductKey);
			// System.out.println("sMacAddress :"+sMacAddress);
			if(sDeviceStatus!=null && (sDeviceStatus.trim().length() == 0)|| sDeviceStatus.equalsIgnoreCase("N")|| sDeviceStatus.equalsIgnoreCase("NA") ){
				pw.println(DEVICE_NOT_REGISTERED);
				pw.flush();
			}
			else if((sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("AL")) ){
				if(sAirlineStatus.equalsIgnoreCase("I")){
					pw.println(INACTIVE_AIRLINE);
					pw.flush();
				}else{
					pw.println(DEVICE_IS_ALLOCATED);
					pw.flush();
				}
			}
			else if(sDeviceStatus.equalsIgnoreCase("D")){
				pw.println(DEVICE_DELETED);
				pw.flush();
			}
			else if(sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("A")){
				if(sAirlineStatus.equalsIgnoreCase("I")){
					pw.println(INACTIVE_AIRLINE);
					pw.flush();
				}else{
					pw.println(DEVICE_ALREADY_ACTIVATED);
					pw.flush();
				}
			}
			else if(sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("I")){
				pw.println(DEVICE_INACTIVE);
				pw.flush();
			}

			logger.info("DeviceRegAction::deviceStatus:EXIT");
		}catch(Exception e){		
			pw.println(CHECKING_DEVICE_STATUS_FAILED);
			pw.flush();		
			e.printStackTrace();
			logger.error("DeviceRegAction::deviceStatus:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward addNewMacAddress(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::addNewMacAddress:ENTER");
		String sFrdKey="deviceRegSuccess";	
		ArrayList alDeviceStatus = null;
		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();	
		try{
			// System.out.println("addNewMacAddress:Enter");	

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sMacAddress =  request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();
			
			String sNewMacAddress =  request.getParameter("newMacAddr");
			sNewMacAddress = sNewMacAddress ==null?"":sNewMacAddress.trim();
			
			


			//ArrayList alDeviceStatus = DeviceRegDAO.getDeviceStatus(sProductKey,sMacAddress,sNewMacAddress);
			
			alDeviceStatus = DeviceRegDAO.updateNewMacAddress(sProductKey,sMacAddress,sNewMacAddress);
 
			
			//response.setHeader("AirlineDetails",sAirId+";"+sAirName+";"+sAirEmail+";"+iDeviceId);
			
			String sStatus = (String)alDeviceStatus.get(0);
			sStatus = sStatus == null?"":sStatus.trim();
			String sDeviceStatus =(String)alDeviceStatus.get(1);
			sDeviceStatus = sDeviceStatus == null?"":sDeviceStatus.trim();
			String sAirlineStatus =(String)alDeviceStatus.get(2);
			sAirlineStatus = sAirlineStatus == null?"":sAirlineStatus.trim();
			
			// System.out.println("sProductKey :"+sProductKey);
			// System.out.println("sMacAddress :"+sMacAddress);
			
			response.setHeader("status",sStatus);
			if(sDeviceStatus!=null && (sDeviceStatus.trim().length() == 0)|| sDeviceStatus.equalsIgnoreCase("N")|| sDeviceStatus.equalsIgnoreCase("NA") ){
				pw.println(DEVICE_NOT_REGISTERED);
				pw.flush();
			}
			else if((sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("AL")) ){
				if(sAirlineStatus.equalsIgnoreCase("I")){
					pw.println(INACTIVE_AIRLINE);
					pw.flush();
				}else{
					pw.println(DEVICE_IS_ALLOCATED);
					pw.flush();
				}
			}
			else if(sDeviceStatus.equalsIgnoreCase("D")){
				pw.println(DEVICE_DELETED);
				pw.flush();
			}
			else if(sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("A")){
				if(sAirlineStatus.equalsIgnoreCase("I")){
					pw.println(INACTIVE_AIRLINE);
					pw.flush();
				}else{
					pw.println(DEVICE_ALREADY_ACTIVATED);
					pw.flush();
				}
			}
			else if(sDeviceStatus!=null && sDeviceStatus.equalsIgnoreCase("I")){
				pw.println(DEVICE_INACTIVE);
				pw.flush();
			}

			logger.info("DeviceRegAction::addNewMacAddress:EXIT");
		}catch(Exception e){		
			response.setHeader("status","failed");
			pw.println(CHECKING_DEVICE_STATUS_FAILED);
			pw.flush();		
			e.printStackTrace();
			logger.error("DeviceRegAction::addNewMacAddress:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}

	public ActionForward checkProdKeyAndRegDevice(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::checkProdKeyAndRegDevice:ENTER");
		String sFrdKey="deviceRegSuccess";	

		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();
		String sProductKey=null,sProcessorId = null,sMacAddress = null,sMacDesc = null;
		try{
			// System.out.println("checkProdKeyAndRegDevice:Enter");	

			sProductKey=request.getParameter("productKey");	
			sProductKey = sProductKey == null?"":sProductKey.trim();

			sProcessorId=request.getParameter("processorId");	
			sProcessorId = sProcessorId == null?"":sProcessorId.trim();

			sMacAddress=request.getParameter("macAddress");	
			sMacAddress = sMacAddress == null?"":sMacAddress.trim();

			sMacDesc=request.getParameter("macDesc");	
			sMacDesc = sMacDesc == null?"":sMacDesc.trim();

			String sDeviceStatus = DeviceRegDAO.checkProdKey(sProductKey);

			if(sDeviceStatus.equalsIgnoreCase("A") || sDeviceStatus.equalsIgnoreCase("N")){
				// System.out.println("TRUE");	
				DeviceRegDAO.updateDeviceDetails(sProductKey,sProcessorId,sMacAddress,sMacDesc);
				pw.println("TRUE");
				pw.flush();
			}else{
				// System.out.println("FALSE");
				//response.setHeader("FALSE",sProductKey);
				//response.setHeader("AirlineDetails",sAirId+";"+sAirName+";"+sAirEmail+";"+iDeviceId);
				pw.println("FALSE");
				pw.flush();
			}

			// System.out.println("checkProdKeyAndRegDevice:Exit");
			logger.info("DeviceRegAction::checkProdKeyAndRegDevice:EXIT");
		}catch(Exception e){		
			pw.println(FALSE_DEVICE_IS_NOT_REGISTERED);
			response.setHeader("FailureReason",e.getMessage());
			pw.flush();
			e.printStackTrace();
			logger.error("DeviceRegAction::checkProdKeyAndRegDevice:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}

	public ActionForward deviceReg(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::deviceReg:ENTER");
		String sFrdKey="deviceRegSuccess";	

		String sAirUserId = null, sMacAddr=null;
		String sProcessorId = null,sDeviceCode = null;
		String sDeviceType= null,sDeviceModel=null,sDeviceSerialNumber = null;
		ArrayList alDeviceRegStatus  =  new ArrayList();
		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();
		AirlineRegVO oAirlineRegVO =null;
		String sAirName="",sAirEmail="",sProductKey=null,sActiveMacAddr ="";
		try{
			// System.out.println("deviceReg:Enter");	

			
			String sAirId =  request.getParameter("airId");
			sAirId = sAirId ==null?"":sAirId.trim();

			sMacAddr=request.getParameter("macAddr");	
			sMacAddr = sMacAddr == null?"":sMacAddr.trim();
			
			sActiveMacAddr=request.getParameter("activeMacAddr");	
			sActiveMacAddr = sActiveMacAddr == null?"":sActiveMacAddr.trim();

			sProcessorId=request.getParameter("processorId");	
			sProcessorId = sProcessorId == null?"":sProcessorId.trim();

			sProductKey=request.getParameter("productKey");	
			sProductKey = sProductKey == null?"":sProductKey.trim();	

			// System.out.println("Air ID :"+sAirId);
			// System.out.println("Mac Addr :"+sMacAddr);
			// System.out.println("Product Key :"+sProductKey);
			// System.out.println("sActiveMacAddr :"+sActiveMacAddr);
			
			InetAddress oInetAddress = InetAddress.getLocalHost();
			boolean bStatus = DeviceRegDAO.validateDevice(sProductKey, sAirId, sActiveMacAddr, "DR");
			if(bStatus){

				alDeviceRegStatus = DeviceRegDAO.checkDeviceRegDetails(sAirId,sMacAddr,sProductKey,oInetAddress.getHostName());


				int iDeviceId =(Integer)alDeviceRegStatus.get(0);			
				sDeviceType = (String)alDeviceRegStatus.get(1);
				sDeviceModel = (String)alDeviceRegStatus.get(2);
				sDeviceSerialNumber = (String)alDeviceRegStatus.get(3);
				sDeviceCode = (String)alDeviceRegStatus.get(4);
				// System.out.println("iDeviceId"+iDeviceId);	

				if(iDeviceId>0){
					oAirlineRegVO = AirlineDAO.getAirlineDetailsById(sAirId,null);
					if(oAirlineRegVO!=null){
						sAirId =oAirlineRegVO.getAirId();
						sAirName =oAirlineRegVO.getAirName();
						sAirEmail =oAirlineRegVO.getAirEmail();	
						sAirUserId = oAirlineRegVO.getAirUserId();
					}
					response.setHeader("AirlineDetails",sAirId+";"+sAirName+";"+sAirEmail+";"+iDeviceId +";"+sDeviceModel+";"+sDeviceType+";"+sDeviceSerialNumber+";"+sDeviceCode);

					//insert device log details
					DeviceRegDAO.insertDeviceLogDetails(sAirId,iDeviceId+"","","Device Registered", 0,sActiveMacAddr,"",oInetAddress.getHostName(),"CLIENT");				

					pw.println("true-"+sMacAddr+sAirUserId);
					pw.flush();
				}else {
					pw.println(DEVICE_ALREADY_REGISTERED);
					pw.flush();
				}
			}else{
				pw.println(INVALID_AIRLINE_OR_PRODUCT_KEY);
				pw.flush();
			}

			// System.out.println("deviceReg:Exit");
			logger.info("DeviceRegAction::deviceReg:EXIT");
		}catch(Exception e){		
			pw.println(FALSE_DEVICE_IS_NOT_REGISTERED);
			response.setHeader("FailureReason",e.getMessage());
			pw.flush();
			e.printStackTrace();
			logger.error("DeviceRegAction::deviceReg:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}
	public ActionForward initAddDeviceReg(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("DeviceRegAction::initAddDeviceReg:ENTER");    	  
		String sFwrdKey="initDeviceRegSuccess";     
		List<AirlineVO> alAirCodeList = null;
		try{	
			HttpSession oSession = request.getSession(true);
			oSession.removeAttribute("deviceDetails");
			alAirCodeList = DeviceRegDAO.getAirlineCode();
			if(alAirCodeList!= null)
				request.setAttribute("airCodeList",alAirCodeList );

			request.setAttribute("mode","deviceAdd" );
		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("DeviceRegAction::initAddDeviceReg:Exception "+e.getMessage());
		}
		logger.info("DeviceRegAction::initAddDeviceReg:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward addDeviceRegDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::addDeviceRegDetails:ENTER");
		String sFrdKey="addDeviceRegSuccess";	
		HttpSession oSession=request.getSession(true); 
		List<AirlineVO> alAirCodeList = null;
		String sProdKey = null;
		boolean bIsCodeExist = false;
		DeviceRegVO oDeviceRegVO =null;
		LoginVO oLoginVO = null;
		String sURI=null,sPath=null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");	

			DeviceRegForm oDeviceRegForm = (DeviceRegForm)form;
			bIsCodeExist = DeviceRegDAO.isDeviceIdExist(oDeviceRegForm.getDeviceCode().trim());
			if(!bIsCodeExist){
				sProdKey = DeviceRegDAO.getProductKey(oDeviceRegForm);
				KeyVO oKeyVO= OrderDAO.encryptData(oDeviceRegForm.getDevicePassword(), "SERVER", "");
				oDeviceRegForm.setDevicePassword(oKeyVO.getEncryptedData());
				int iDeviceId = DeviceRegDAO.addDeviceDetails(oDeviceRegForm,sProdKey,oLoginVO.getUserId(),oKeyVO.getRefId());
				if(iDeviceId!=0){
					if(!oDeviceRegForm.getAirId().equalsIgnoreCase(""))
						DeviceRegDAO.addAirDeviceDetails(oDeviceRegForm.getAirId(), iDeviceId,oLoginVO.getUserId());
	
				}
				//Insert Device Audit details
				String sInsertedDeviceDetails = DeviceRegDAO.getInsertedDeviceDetails(oDeviceRegForm);
				DeviceRegDAO.insertDeviceAuditDetails(iDeviceId+"", oDeviceRegForm.getAirId(), sProdKey,sInsertedDeviceDetails,null,null,"DEVICE ADDED",oLoginVO.getUserId());
	
	
				oDeviceRegVO = DeviceRegDAO.getDeviceDetailsById(iDeviceId+"");
				if(oDeviceRegVO!=null){
					BeanUtils.copyProperties(oDeviceRegForm,oDeviceRegVO);
					request.setAttribute("deviceDetails",oDeviceRegVO);			
					request.setAttribute("mode","deviceAdd" );
				}
				alAirCodeList = DeviceRegDAO.getAirlineCode();
				if(alAirCodeList!= null)
					request.setAttribute("airCodeList",alAirCodeList );

			}else{
				alAirCodeList = DeviceRegDAO.getAirlineCode();
				if(alAirCodeList!= null)
					request.setAttribute("airCodeList",alAirCodeList );

				request.setAttribute("mode","deviceAdd" );
				request.setAttribute("error","Device Id "+ oDeviceRegForm.getDeviceCode()+" is already exist.");
				sFrdKey="initDeviceRegSuccess";  
			}
			logger.info("DeviceRegAction::addDeviceRegDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::addDeviceRegDetails:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}
	public ActionForward initSearchDevice(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("DeviceRegAction::initSearchDevice:ENTER");    	  
		String sFwrdKey="initSearchDeviceSuccess";   	

		try{		

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("DeviceRegAction::initSearchDevice:Exception "+e.getMessage());
		}
		logger.info("DeviceRegAction::initSearchDevice:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward searchDevice(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::searchDevice:ENTER");
		String sFrdKey="searchDeviceSuccess";
		ArrayList alDeviceInfo = null;	
		SearchDeviceForm oSearchDeviceForm=(SearchDeviceForm)form; 	
		HttpSession oSession=request.getSession(true);		
		LoginVO oLoginVO = null;
		String sSearchBy=null,sSearchValue = null;
		String sAirId = null,sURI=null,sPath=null;
		try{
			if(oSearchDeviceForm!=null){
				sSearchBy = oSearchDeviceForm.getDeviceSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchDeviceForm.setDeviceSearchBy(sSearchBy);

				sSearchValue = oSearchDeviceForm.getDeviceSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchDeviceForm.setDeviceSearchValue(sSearchValue);			

			}		
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");	

			if(oLoginVO!=null && sPath.equalsIgnoreCase("airline")){				
				sAirId = oLoginVO.getRefId();
			}
			alDeviceInfo = DeviceRegDAO.getDeviceDetails(oSearchDeviceForm,sAirId);
			if(alDeviceInfo!=null && alDeviceInfo.size()>0){
				request.setAttribute("deviceInfo", alDeviceInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  
			logger.info("DeviceRegAction::searchDevice:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::searchDevice:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward editDeviceDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::editDeviceDetails:ENTER");
		String sFrdKey="editDeviceSuccess";
		String sSourceOfEdit = null;
		String sMaskedDevicePassword = null;
		DeviceRegVO oDeviceRegVO = null;	
		DeviceRegForm oDeviceRegForm = (DeviceRegForm)form; 		
		List<AirlineVO> alAirCodeList = null;
		HttpSession oSession=request.getSession(true);		
		try{ 
			String sDeviceId=request.getParameter("deviceId");			

			oDeviceRegVO = DeviceRegDAO.getDeviceDetailsById(sDeviceId);
			sSourceOfEdit = request.getParameter("SourceOfEdit");
			
			if(oDeviceRegVO!=null){
				BeanUtils.copyProperties(oDeviceRegForm,oDeviceRegVO); 
				oSession.setAttribute("deviceDetails",oDeviceRegVO);
				request.setAttribute("mode","deviceEdit" );
			} 
			sMaskedDevicePassword = Utils.getDevicePassword(oDeviceRegVO.getDevicePassword());
			request.setAttribute("maskedDP", sMaskedDevicePassword);
			alAirCodeList = DeviceRegDAO.getAirlineCode();
			if(alAirCodeList!= null)
				request.setAttribute("airCodeList",alAirCodeList );
			
			request.setAttribute("SourceOfEdit", sSourceOfEdit);
			
			logger.info("DeviceRegAction::editDeviceDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::editDeviceDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward updateDeviceDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::updateDeviceDetails:ENTER");
		String sFrdKey="updateDeviceSuccess";		
		DeviceRegForm oDeviceRegForm = (DeviceRegForm)form; 					
		List<AirlineVO> alAirCodeList = null;
		DeviceRegVO oDeviceRegVO =null;
		DeviceRegVO oldDeviceRegVO =null;
		Map<String, List<DeviceDetailsVO>> mDeviceDetailsHistory = null;
		HttpSession oSession=request.getSession(true);
		LoginVO oLoginVO = null;
		String sURI=null,sPath=null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase("vendor"))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			//Insert Device Audit details
			oldDeviceRegVO = (DeviceRegVO)oSession.getAttribute("deviceDetails");
			KeyVO oKeyVO= OrderDAO.encryptData(oDeviceRegForm.getDevicePassword(), "SERVER", "");
			oDeviceRegForm.setDevicePassword(oKeyVO.getEncryptedData());
			DeviceRegDAO.updateDeviceDetails(oDeviceRegForm,oKeyVO.getRefId(),oLoginVO.getUserId());
			//if(!oDeviceRegVO.getAirId().equalsIgnoreCase(oDeviceRegForm.getAirId())){
			if(!oDeviceRegForm.getAirId().equalsIgnoreCase("")){
				//if(oDeviceRegForm.getStatus().equalsIgnoreCase("AL")){
				DeviceRegDAO.updateAirDeviceDetails(oDeviceRegForm.getAirId(), Integer.parseInt(oDeviceRegForm.getDeviceId()),oDeviceRegForm.getStatus(),oLoginVO.getUserId());
				/*}else{
						DeviceRegDAO.addAirDeviceDetails(oDeviceRegForm.getAirId(), Integer.parseInt(oDeviceRegForm.getDeviceId()));
					}*/
				//}
			}
			
			DeviceRegDAO.getUpdatedDeviceDetails(oDeviceRegForm, oldDeviceRegVO,oLoginVO.getUserId());
			
			alAirCodeList = DeviceRegDAO.getAirlineCode();
			if(alAirCodeList!= null)
				request.setAttribute("airCodeList",alAirCodeList );

			oDeviceRegVO = DeviceRegDAO.getDeviceDetailsById(oDeviceRegForm.getDeviceId());
			if(("N".equalsIgnoreCase(oldDeviceRegVO.getStatus()) || "NA".equalsIgnoreCase(oldDeviceRegVO.getStatus())) && "AL".equalsIgnoreCase(oDeviceRegForm.getStatus())) {
				DeviceRegDAO.insertDeviceLogDetails(oDeviceRegForm.getAirId(),oDeviceRegForm.getDeviceId(),"","Device Swapped", 0,oDeviceRegVO.getMacAddr(),"",oLoginVO.getUserId(),"");
			}
			mDeviceDetailsHistory = DeviceRegDAO.getDeviceHistoryById(oDeviceRegForm.getDeviceId());
			
			if(oDeviceRegVO!=null){
				BeanUtils.copyProperties(oDeviceRegForm,oDeviceRegVO); 
				request.setAttribute("deviceDetails",oDeviceRegVO);
				request.setAttribute("mode","deviceEdit" );
				request.setAttribute("DeviceRegDetails", mDeviceDetailsHistory.get("DeviceRegDetails"));
				request.setAttribute("DeviceAllocateDetails", mDeviceDetailsHistory.get("DeviceAllocateDetails"));
				request.setAttribute("CatalogueDownloadDetails", mDeviceDetailsHistory.get("CatalogueDownloadDetails"));
				request.setAttribute("OrderPlacedDetails", mDeviceDetailsHistory.get("OrderPlacedDetails"));
				request.setAttribute("PackageDownloadDetails", mDeviceDetailsHistory.get("PackageDownloadDetails"));
			} 
			
			logger.info("DeviceRegAction::updateDeviceDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::updateDeviceDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward viewDeviceDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::viewDeviceDetails:ENTER");
		String sFrdKey="updateDeviceSuccess";
		DeviceRegVO oDeviceRegVO = null;	
		DeviceRegForm oDeviceRegForm = (DeviceRegForm)form;
		Map<String, List<DeviceDetailsVO>> mDeviceDetailsHistory = null;
		List<AirlineVO> alAirCodeList = null;
		try{
			String sDeviceId=request.getParameter("deviceId");			

			oDeviceRegVO = DeviceRegDAO.getDeviceDetailsById(sDeviceId);
			mDeviceDetailsHistory = DeviceRegDAO.getDeviceHistoryById(oDeviceRegForm.getDeviceId());
			if(oDeviceRegVO!=null){
				BeanUtils.copyProperties(oDeviceRegForm,oDeviceRegVO); 
				request.setAttribute("deviceDetails",oDeviceRegVO);
				if(mDeviceDetailsHistory != null && !mDeviceDetailsHistory.isEmpty()) {
					request.setAttribute("DeviceRegDetails", mDeviceDetailsHistory.get("DeviceRegDetails"));
					request.setAttribute("DeviceAllocateDetails", mDeviceDetailsHistory.get("DeviceAllocateDetails"));
					request.setAttribute("CatalogueDownloadDetails", mDeviceDetailsHistory.get("CatalogueDownloadDetails"));
					request.setAttribute("OrderPlacedDetails", mDeviceDetailsHistory.get("OrderPlacedDetails"));
					request.setAttribute("PackageDownloadDetails", mDeviceDetailsHistory.get("PackageDownloadDetails"));
				}
			} 
			alAirCodeList = DeviceRegDAO.getAirlineCode();
			if(alAirCodeList!= null)
				request.setAttribute("airCodeList",alAirCodeList );
			logger.info("DeviceRegAction::viewDeviceDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::viewDeviceDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward deleteDeviceDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("DeviceRegAction::deleteDeviceDetails:ENTER");
		String sFrdKey="deleteDeviceSuccess",sURI=null,sPath=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		try{	

			
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase("vendor"))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			String sDeviceId =  request.getParameter("deviceId");
			sDeviceId = sDeviceId == null?"":sDeviceId.trim();

			String sAirId =  request.getParameter("airId");
			sAirId = sAirId == null?"0":sAirId.trim();

			DeviceRegDAO.deleteDevice(sDeviceId,oLoginVO.getUserId());

			//Insert Device Audit details
			DeviceRegDAO.insertDeviceAuditDetails(sDeviceId,sAirId,"","",null,null,"DEVICE DELETED",oLoginVO.getUserId());

			logger.info("DeviceRegAction::deleteDeviceDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("DeviceRegAction::deleteDeviceDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	/* For Device Attendance */
	
	public ActionForward initSearchDeviceAttendance(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("DeviceRegAction::initSearchDeviceAttendance:ENTRY");
		String sForwardKey="success";
		
		try {
			DynaActionForm oForm = (DynaActionForm)form;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oForm.set("toDate", sCurrentDate);
			
		}catch(Exception e){		
				sForwardKey=("failure");
				e.printStackTrace();
				logger.error("DeviceRegAction::initSearchDeviceAttendance:EXCEPTION "+e.getMessage());
		}
		
		logger.info("DeviceRegAction::initSearchDeviceAttendance:EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward searchDeviceAttendance(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("DeviceRegAction::searchDeviceAttendance:ENTRY");
		String sForwardKey="success";
		List<DeviceAttendanceVO> alDeviceAttendance = null;
		String sSearchType = null, sSearchValue = null, sActivity = null, sFromDate = null, sToDate = null;
		try {
			DynaActionForm oForm = (DynaActionForm)form;
			
			sSearchType = oForm.getString("searchType");
			sSearchType = sSearchType==null?"":sSearchType.trim();
			
			sSearchValue = oForm.getString("searchValue");
			sSearchValue = sSearchValue==null?"":sSearchValue.trim();
			
			sActivity = oForm.getString("activity");
			sActivity = sActivity==null?"":sActivity.trim();
			
			sFromDate = oForm.getString("fromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();
			
			sToDate = oForm.getString("toDate");
			sToDate = sToDate==null?"":sToDate.trim();
			
			
			alDeviceAttendance = DeviceRegDAO.getDeviceAttendance(sSearchType, sSearchValue, sActivity, sFromDate, sToDate);
			
			if(alDeviceAttendance !=null && alDeviceAttendance.size()>0)
				request.setAttribute("AttendanceInfo", alDeviceAttendance);
			else
				request.setAttribute("NoRecords", "No Records Found");
			
		}catch(Exception e){		
				sForwardKey=("failure");
				e.printStackTrace();
				logger.error("DeviceRegAction::searchDeviceAttendance:EXCEPTION "+e.getMessage());
		}
		
		logger.info("DeviceRegAction::searchDeviceAttendance:EXIT");
		return mapping.findForward(sForwardKey);
	}

/* To check server alive */
	
	public ActionForward isAlive(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
	throws Exception {
		logger.info("DeviceRegAction::isAlive::ENTER");

		String sFwrdKey = null;
		final String sStatus = "TRUE";
		
		try {
			
			response.setHeader("STATUS",sStatus);
			
			logger.info("DeviceRegAction::isAlive:Status "+sStatus);
		}catch(Exception e){
			sFwrdKey = "failure";
			e.printStackTrace();
			logger.error("DeviceRegAction::isAlive:Exception "+e.getMessage());
		}
		logger.info("DeviceRegAction::isAlive:EXIT");
		return mapping.findForward(sFwrdKey);
	}

}
