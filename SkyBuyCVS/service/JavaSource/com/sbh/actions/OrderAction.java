package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.ADMIN_LOGIN_INFO;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.AIRLINE_CATAGEORY_NAME;
import static com.sbh.contants.SkyBuyContants.AIRLINE_LOGIN_INFO;
import static com.sbh.contants.SkyBuyContants.COMPLETED_ORDERS;
import static com.sbh.contants.SkyBuyContants.FULLY_AUTOMATED;
import static com.sbh.contants.SkyBuyContants.INCOMPLETE_ORDERS;
import static com.sbh.contants.SkyBuyContants.NO_ORDERITEMID_PARAMETER;
import static com.sbh.contants.SkyBuyContants.NO_RECORDS;
import static com.sbh.contants.SkyBuyContants.NO_RECORDS_FOUND;
import static com.sbh.contants.SkyBuyContants.NO_RECORDS_KEY;
import static com.sbh.contants.SkyBuyContants.ORDER_ALREADY_CANCELED;
import static com.sbh.contants.SkyBuyContants.ORDER_ALREADY_COMPLETED;
import static com.sbh.contants.SkyBuyContants.ORDER_ALREADY_PROCESSED;
import static com.sbh.contants.SkyBuyContants.ORDER_ALREADY_REJECTED;
import static com.sbh.contants.SkyBuyContants.ORDER_CANCELED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_COMPLETED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_FAILED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_IN_PROCESS;
import static com.sbh.contants.SkyBuyContants.ORDER_PENDING_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_REJECTED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_TOBE_CHARGED_STATUS;
import static com.sbh.contants.SkyBuyContants.PAYMENT_APPROVED;
import static com.sbh.contants.SkyBuyContants.PENDING_ORDERS;
import static com.sbh.contants.SkyBuyContants.PENDING_ORDERS_INFO;
import static com.sbh.contants.SkyBuyContants.PRE_AUTHORIZATION_NO;
import static com.sbh.contants.SkyBuyContants.PRE_AUTHORIZATION_YES;
import static com.sbh.contants.SkyBuyContants.SEMI_AUTOMATED;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.contants.SkyBuyContants.VENDOR_LOGIN_INFO;
import static com.sbh.dao.OrderDAO.addOrderTrackingDetails;
import static com.sbh.dao.OrderDAO.decryptInputData;
import static com.sbh.dao.OrderDAO.encryptInputData;
import static com.sbh.dao.OrderDAO.getCreditCardInformation;
import static com.sbh.dao.OrderDAO.getCustOrderItemDetails;
import static com.sbh.dao.OrderDAO.getOrderItemStatus;
import static com.sbh.dao.OrderDAO.getOrderTrackingDetail;
import static com.sbh.dao.OrderDAO.getPayTxnRefId;
import static com.sbh.dao.OrderDAO.getRejectReason;
import static com.sbh.dao.OrderDAO.getReturnOrderItemDetail;
import static com.sbh.dao.OrderDAO.getReturnOrderItemDetails;
import static com.sbh.dao.OrderDAO.getSearchOrderToReturn;
import static com.sbh.dao.OrderDAO.getTxnFailureDetails;
import static com.sbh.dao.OrderDAO.getVendorOrderItemDetails;
import static com.sbh.dao.OrderDAO.insertOrderAuditDetails;
import static com.sbh.dao.OrderDAO.isOrderItemDetailChanged;
import static com.sbh.dao.OrderDAO.sendChargeBackFailureEmail;
import static com.sbh.dao.OrderDAO.sendOrderCancelFailureEmail;
import static com.sbh.dao.OrderDAO.sendOrderChargeBackRejectEmail;
import static com.sbh.dao.OrderDAO.sendOrderChargeBackSuccessEmailToCustomer;
import static com.sbh.dao.OrderDAO.sendOrderChargeBackSuccessEmailToVendor;
import static com.sbh.dao.OrderDAO.sendOrderItemsFailureEmailToVendor;
import static com.sbh.dao.OrderDAO.sendOrderItemsRejectionEmail;
import static com.sbh.dao.OrderDAO.sendOrderItemsSuccessEmail;
import static com.sbh.dao.OrderDAO.sendRMAApprovedEmailToAdmin;
import static com.sbh.dao.OrderDAO.sendRMAApprovedEmailToCustomer;
import static com.sbh.dao.OrderDAO.sendRMAApprovedEmailToVendor;
import static com.sbh.dao.OrderDAO.sendRMARejectedEmailToAdmin;
import static com.sbh.dao.OrderDAO.sendRMARejectedEmailToCustomer;
import static com.sbh.dao.OrderDAO.sendRMARejectedEmailToVendor;
import static com.sbh.dao.OrderDAO.sendTrackingDetailsEmailToCustomer;
import static com.sbh.dao.OrderDAO.sendTrackingDetailsEmailToVendor;
import static com.sbh.dao.OrderDAO.sendUpdateReturnOrderDetail;
import static com.sbh.dao.OrderDAO.updateCreditCardDetails;
import static com.sbh.dao.OrderDAO.updateCustOrderItemDetails;
import static com.sbh.dao.OrderDAO.updateOrderItemStatus;
import static com.sbh.dao.OrderDAO.updateOrderItemStatusAndReturnStatus;
import static com.sbh.dao.OrderDAO.updateRMANumberStatus;
import static com.sbh.dao.OrderDAO.updateRejectReason;
import static com.sbh.dao.OrderDAO.updateReturnDetail;
import static com.sbh.dao.PaymentDAO.creditAuthorizationToRepay;
import static com.sbh.dao.PaymentDAO.postAuthorizePayment;
import static com.sbh.dao.PaymentDAO.preAuthorizePayment;
import static com.sbh.dao.PaymentDAO.salePayment;
import static com.sbh.util.CCUtils.getMaskCCNo;
import static com.sbh.util.CCUtils.validCC;
import static com.sbh.util.CustomerUtils.createBlob;
import static com.sbh.util.Utils.convertToCategoryName;
import static com.sbh.util.Utils.dateStampConversion;
import static com.sbh.util.Utils.getRandomNo;

import java.awt.Dimension;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;
/*import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.HTMLRenderContext;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
*/import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;

import com.sbh.dao.CustomerDAO;
import com.sbh.dao.EmailDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.dao.PaymentDAO;
import com.sbh.email.Email;
import com.sbh.forms.OrderItemForm;
import com.sbh.forms.OrderReturnForm;
import com.sbh.forms.SearchOrderForm;
import com.sbh.util.CCUtils;
import com.sbh.util.Utils;
import com.sbh.util.VendorUtils;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CreditCardVO;
import com.sbh.vo.CustPayTxnVO;
import com.sbh.vo.CustomerSuggestionsVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.ItemDetailsVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.OrderReportVO;
import com.sbh.vo.OrderReturnDetailsVO;
import com.sbh.vo.OrderTrackingDetailsVO;
import com.sbh.vo.PaymentInformationVO;
import com.sbh.vo.PaymentResponse;
import com.sbh.vo.ProductDetailsVO;

public class OrderAction extends DispatchAction
{
	private static Logger logger = LogManager.getLogger(OrderAction.class);
	public ActionForward getOrderStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::getOrderStatus:ENTER");
		String sFrdKey="orderStatusSuccess",sURI=null,sPath=null;
		ArrayList alOrderStatusInfo =null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			alOrderStatusInfo = OrderDAO.getOrderStatus(oLoginVO);
			if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
				request.setAttribute(PENDING_ORDERS, alOrderStatusInfo.get(0));
				request.setAttribute(INCOMPLETE_ORDERS, alOrderStatusInfo.get(1));
				request.setAttribute(COMPLETED_ORDERS, alOrderStatusInfo.get(2));
			}
			logger.info("OrderAction::getOrderStatus:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::getOrderStatus:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward getOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::getOrderDetails:ENTER");
		String sFrdKey="AdminOrderDetailsSuccess";
		ArrayList alOrderStatusInfo =null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		String sOwnerType=null,sURI=null,sPath=null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}		
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorOrderDetailsSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineOrderDetailsSuccess";  
			}
			String sOrderStatus =  request.getParameter("OrderStatus");
			sOrderStatus = sOrderStatus == null?"":sOrderStatus.trim();

			alOrderStatusInfo = OrderDAO.getOrderDetails(sOrderStatus,oLoginVO);
			if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
				request.setAttribute("orderDetails",alOrderStatusInfo);
			}
			oSession.setAttribute("OrderStatus",sOrderStatus);

			logger.info("OrderAction::getOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::getOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward getOrderItemDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::getOrderItemDetails:ENTER");
		String sFrdKey="AdminOrderItemDetailsSuccess";
		List<OrderItemDetailsVO> alOrderItemDetails =null;
		LoginVO oLoginVO =null;
		HttpSession oSession = request.getSession();
		String sOwnerType=null,sURI=null,sPath=null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorOrderItemDetailsSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineOrderItemDetailsSuccess";  
			}
			String sOrderId =  request.getParameter("OrderId");
			sOrderId = sOrderId == null?"":sOrderId.trim();

			alOrderItemDetails = OrderDAO.getOrderItemDetails(sOrderId,oLoginVO);
			if(alOrderItemDetails!=null && alOrderItemDetails.size()>0){
				request.setAttribute("orderItemsDetails",alOrderItemDetails);
			}

			logger.info("OrderAction::getOrderItemDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::getOrderItemDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward initSearchOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initSearchOrder:ENTER");    	  
		String sFwrdKey="initAdminSearchOrderSuccess",sURI=null,sPath=null;    	
		HttpSession oSession = request.getSession(true);
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		LoginVO oLoginVO =null;
		String sOwnerType =null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFwrdKey="initVendorSearchOrderSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFwrdKey="initAirlineSearchOrderSuccess";  
			}

			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchOrderForm");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oSearchOrderForm.setOrderToDate(sCurrentDate);

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initSearchOrder:Exception "+e.getMessage());
		}
		logger.info("OrderAction::initSearchOrder:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward searchOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::searchOrder:ENTER");
		String sFrdKey="AdminSearchOrderSuccess";
		List<OrderItemDetailsVO> alOrderItemDetails = null;	
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sSearchBy=null,sSearchValue = null , sCategory =null,sOrderFromDate = null,sOrderToDate=null;
		String sOwnerId=null,sOwnerType=null,sURI=null,sPath=null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId ==null?"":sOwnerId.trim();
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			String sOrderStatus =  request.getParameter("OrderStatus");
			sOrderStatus = sOrderStatus == null?"":sOrderStatus.trim();

			String sItemCode =  request.getParameter("ItemCode");
			sItemCode = sItemCode == null?"":sItemCode.trim();

			if(oSearchOrderForm!=null){	
				sSearchBy=oSearchOrderForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchOrderForm.setSearchBy(sSearchBy);

				sSearchValue=oSearchOrderForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchOrderForm.setSearchValue(sSearchValue);

				sCategory = oSearchOrderForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchOrderForm.setCategory(sCategory);

				sOrderFromDate = oSearchOrderForm.getOrderFromDate();
				sOrderFromDate=sOrderFromDate==null?"":sOrderFromDate.trim();
				oSearchOrderForm.setOrderFromDate(sOrderFromDate);	

				sOrderToDate = oSearchOrderForm.getOrderToDate();
				sOrderToDate=sOrderToDate==null?"":sOrderToDate.trim();
				oSearchOrderForm.setOrderToDate(sOrderToDate);	

				if(sOrderStatus.trim().length()>0 && sOrderStatus.equalsIgnoreCase("ALLORDERS")){
					oSearchOrderForm.setOrderStatus("ALL");
					oSearchOrderForm.setSearchBy("ALL");
					oSearchOrderForm.setSearchValue("");
					oSearchOrderForm.setCategory("ALL");	
					oSearchOrderForm.setOrderFromDate("");	

				}else if (sOrderStatus.trim().length()>0){
					oSearchOrderForm.setOrderStatus(sOrderStatus);
					oSearchOrderForm.setSearchBy("ALL");
					oSearchOrderForm.setSearchValue("");
					oSearchOrderForm.setCategory("ALL");	
				}
				if(sItemCode.trim().length()>0){
					oSearchOrderForm.setSearchBy("ITEMCODE");
					oSearchOrderForm.setSearchValue(sItemCode);
				}
			}
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorSearchOrderSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineSearchOrderSuccess";  
			}

			alOrderItemDetails =  OrderDAO.getSearchOrder(oSearchOrderForm,sOwnerType,sOwnerId);
			if(alOrderItemDetails!=null && alOrderItemDetails.size()>0){
				request.setAttribute("orderItemsInfo", alOrderItemDetails);
			}else{
				request.setAttribute(NO_RECORDS_KEY,NO_RECORDS);
			} 			

			logger.info("OrderAction::searchOrder:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::searchOrder:EXCEPTION "+e.getMessage());
		}	
		saveToken(request);
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward editOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::editOrderDetails:ENTER");
		String sFrdKey="AdminEditItemDetailsSuccess";
		OrderItemDetailsVO oOrderItemDetailsVO = null;	
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null,sCateFolderName = null;
		HttpSession oSession = request.getSession();
		OrderItemForm oOrderItemForm=(OrderItemForm)form; 		
		String sOwnerType = null,sURI=null,sPath=null;
		LoginVO oLoginVO =null;

		String placeOrder=request.getParameter("PlaceOrder");

		String sOrderItemId = request.getParameter("OrderItemId");
		String sIsViewOrCharge = request.getParameter("ViewOrder");
		ArrayList alAllColors = null,alAllSizes = null;

		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			oOrderItemDetailsVO = OrderDAO.getCustOrderItemDetails(sOrderItemId);

			if(oLoginVO!= null){				
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}

			if(sOwnerType.equalsIgnoreCase(ADMIN)) {
				if("CHARGE".equalsIgnoreCase(sIsViewOrCharge)) {
					sFrdKey = "AdminChargeItemDetailsSuccess";
				}else if("VIEW".equalsIgnoreCase(sIsViewOrCharge) && ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())){
					sFrdKey = "AdminViewItemDetailsSuccess";
				}
			}
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorEditItemDetailsSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineEditItemDetailsSuccess";  
			}


			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(AIRLINE.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
			}
			if(placeOrder != null) {
				if("placeorder".equalsIgnoreCase(placeOrder)) {
					oSession.setAttribute("PlaceOrder", "YES");
				}else {
					oSession.setAttribute("PlaceOrder", "NO");
				}
			}
			if(oOrderItemDetailsVO!=null){		

				alAllColors = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllColors());
				alAllColors = alAllColors==null?new ArrayList():alAllColors;
				if(alAllColors!=null && alAllColors.size()>0)
					request.setAttribute("allColors",alAllColors);

				alAllSizes = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllSizes());
				alAllSizes = alAllSizes==null?new ArrayList():alAllSizes;

				if(alAllSizes!=null && alAllSizes.size()>0)
					request.setAttribute("allSizes",alAllSizes);

				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				//// System.out.println("custOrderItemDetails:main img path:"+oOrderItemDetailsVO.getMainImgPath());
				oOrderItemDetailsVO.setCvv("");

				BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO); 
				request.setAttribute("custOrderItemDetails",oOrderItemDetailsVO);
				request.setAttribute("LoginType", sOwnerType);
				request.setAttribute("SourceOfEdit", placeOrder);
				request.setAttribute("mode","Edit" );

			} 
			oSession.setAttribute("OrderDetailsUnchanged", oOrderItemDetailsVO);

			logger.info("OrderAction::editOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::editOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}
	public ActionForward updateOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::updateOrderDetails:ENTER");
		String sFrdKey="AdminUpdateItemDetailsSuccess";	
		OrderItemForm oOrderItemForm=(OrderItemForm)form; 	
		LoginVO oLoginVO = null;
		HttpSession oSession=request.getSession(true);
		String sOwnerType= null,sURI=null,sPath=null;
		String isCreditCardChanged = (String)oSession.getAttribute("ISCREDITCARDCHANGED");
		OrderItemDetailsVO oOrderItemDetailsVO = (OrderItemDetailsVO)oSession.getAttribute("OrderDetailsUnchanged");
		boolean isOrderItemChanged;
		String sModifiedFields = "";
		String orderStatus;
		ArrayList<CategoryVO> alCategory =null;
		String sCateFolderName = "",sImageViewPath = "";
		try{		
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			sImageViewPath = System.getProperty("ImageViewPath").trim();
			sImageViewPath = sImageViewPath==null?"":sImageViewPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){				
				sOwnerType = oLoginVO.getUserId();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			} 

			sModifiedFields = OrderDAO.modifiedOrderDetails(oOrderItemDetailsVO,oOrderItemForm);
			if(!"".equalsIgnoreCase(sModifiedFields)){
				insertOrderAuditDetails(oOrderItemForm.getOrderItemId(), oOrderItemForm.getOrderId(), oOrderItemForm.getCustTransId(), sModifiedFields, oLoginVO.getUserId(), null, oLoginVO.getUserId(), "");
			} 

			if("NO".equalsIgnoreCase(isCreditCardChanged) || isCreditCardChanged==null) {
				isOrderItemChanged = isOrderItemDetailChanged(oOrderItemDetailsVO,oOrderItemForm);
				if(isOrderItemChanged) {
					isCreditCardChanged="YES";
				}else {
					isCreditCardChanged="NO";
				}
			}
			if(oOrderItemForm.getCvv() != null && oOrderItemForm.getCvv().trim().length() < 5)
				oOrderItemForm.setCvv(encryptInputData(oOrderItemForm.getCvv(),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));

			if(oOrderItemForm.getCreditCardNo() != null && oOrderItemForm.getCreditCardNo().trim().length()>1 
					&& oOrderItemForm.getCreditCardNo().trim().length()<20) {
				if(CCUtils.validCC(oOrderItemForm.getCreditCardNo(),oOrderItemForm.getCardType())) {
					oOrderItemForm.setCreditCardLFD(getMaskCCNo(oOrderItemForm.getCreditCardNo()));
					oOrderItemForm.setCreditCardNo(encryptInputData(oOrderItemForm.getCreditCardNo(),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));
				}
			}else {
				oOrderItemForm.setCreditCardNo("");
			}
			if(!"".equalsIgnoreCase(sModifiedFields)){
				updateCustOrderItemDetails(oOrderItemForm,sOwnerType,isCreditCardChanged);
			}
			orderStatus = getOrderItemStatus(oOrderItemForm.getOrderItemId());
			oOrderItemForm.setOrderItemStatus(orderStatus);
			oOrderItemDetailsVO = OrderDAO.getCustOrderItemDetails(oOrderItemForm.getOrderItemId()); 

			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
			}

			oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());


			request.setAttribute("mode","Update" );
			request.setAttribute("custOrderItemDetails",oOrderItemDetailsVO);
			logger.info("OrderAction::updateOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::updateOrderDetails:EXCEPTION "+e.getMessage());
		}	
		logger.info("OrderAction::updateOrderDetails:EXIT");
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward getMostPopularMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::getMostPopularMerchandize:ENTER");
		String sFrdKey="AdminMerchandizeSuccess";
		List<ProductDetailsVO> alProductInfo =null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		String sOwnerType=null,sURI=null,sPath=null;
		ArrayList<CategoryVO> alCategory = null;
		String sScheme = null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}		
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorMerchandizeSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineMerchandizeSuccess";  
			}

			sScheme = request.getScheme();
			alProductInfo = OrderDAO.getMostPopularMerchandizeDetails(alCategory,oLoginVO,sScheme);
			if(alProductInfo!=null && alProductInfo.size()>0){
				request.setAttribute("popualrMerchandizeInfo",alProductInfo);
			}


			logger.info("OrderAction::getMostPopularMerchandize:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::getMostPopularMerchandize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	/*public ActionForward initPlaceOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::initPlaceOrder:ENTER");
		String sFrdKey="initPlaceOrderSuccess",sURI=null,sPath=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		ArrayList alOrderStatusInfo =null;

		SearchOrderForm oSearchOrderForm = null;
		if(form != null) {
			oSearchOrderForm = (SearchOrderForm)form;
		}else {
			oSearchOrderForm = new SearchOrderForm();
		}

		oSearchOrderForm.setOrderStatus(ORDER_PENDING_STATUS);
		oSearchOrderForm.setCategory(oSearchOrderForm.getCategory()==null?"":oSearchOrderForm.getCategory().trim());
		oSearchOrderForm.setOrderFromDate(oSearchOrderForm.getOrderFromDate()==null?"":oSearchOrderForm.getOrderFromDate().trim());
		oSearchOrderForm.setOrderToDate(oSearchOrderForm.getOrderToDate()==null?"":oSearchOrderForm.getOrderToDate().trim());
		oSearchOrderForm.setSearchBy(oSearchOrderForm.getSearchBy()==null?"":oSearchOrderForm.getSearchBy().trim());
		oSearchOrderForm.setSearchValue(oSearchOrderForm.getSearchValue()==null?"":oSearchOrderForm.getSearchValue().trim());

		List<OrderItemDetailsVO> orderItemsInfo = null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			alOrderStatusInfo = OrderDAO.getOrderStatus(oLoginVO);
			orderItemsInfo = OrderDAO.getSearchOrder(oSearchOrderForm, "", "");
			if(orderItemsInfo.size()==0) {
				request.setAttribute(NO_RECORDS_KEY, NO_RECORD);
			}
			if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
				request.setAttribute(PENDING_ORDERS_INFO, orderItemsInfo);
				oSession.setAttribute(PENDING_ORDERS, alOrderStatusInfo.get(0));
				oSession.setAttribute(INCOMPLETE_ORDERS, alOrderStatusInfo.get(1));
				oSession.setAttribute(COMPLETED_ORDERS, alOrderStatusInfo.get(2));
			}
			logger.info("OrderAction::initPlaceOrder:EXIT");
		}catch(Exception e){		

			saveErrors(request, errors);
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initPlaceOrder:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}*/

	public ActionForward viewOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::viewOrderDetails:ENTER");
		String sFrdKey="AdminViewItemDetailsSuccess";
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		OrderItemForm oOrderItemForm = (OrderItemForm)form;
		ArrayList<CategoryVO> alCategory =null;
		HttpSession oSession = request.getSession();
		String sCateFolderName = "",sImageViewPath = "";
		try{
			String sOrderItemId=request.getParameter("OrderItemId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			sImageViewPath = sImageViewPath==null?"":sImageViewPath.trim();
			if(sOrderItemId != null){
				oOrderItemDetailsVO = OrderDAO.getCustOrderItemDetails(sOrderItemId);

				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(AIRLINE.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
				}

				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO);
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
			}
			logger.info("OrderAction::viewOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::viewOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}
	public ActionForward initPlaceOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::initPlaceOrder:ENTER");
		String sFrdKey="initPlaceOrderSuccess",sURI=null,sPath=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		ArrayList alOrderStatusInfo =null;

		SearchOrderForm oSearchOrderForm = null;
		if(form != null) {
			oSearchOrderForm = (SearchOrderForm)form;
			oSearchOrderForm.setOrderStatus(ORDER_PENDING_STATUS);
			oSearchOrderForm.setCategory(oSearchOrderForm.getCategory()==null?"":oSearchOrderForm.getCategory().trim());
			oSearchOrderForm.setOrderFromDate(oSearchOrderForm.getOrderFromDate()==null?"":oSearchOrderForm.getOrderFromDate().trim());
			oSearchOrderForm.setOrderToDate(oSearchOrderForm.getOrderToDate()==null?"":oSearchOrderForm.getOrderToDate().trim());
			oSearchOrderForm.setSearchBy(oSearchOrderForm.getSearchBy()==null?"":oSearchOrderForm.getSearchBy().trim());
			oSearchOrderForm.setSearchValue(oSearchOrderForm.getSearchValue()==null?"":oSearchOrderForm.getSearchValue().trim());
		}
		List<OrderItemDetailsVO> orderItemsInfo = null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			alOrderStatusInfo = OrderDAO.getOrderStatus(oLoginVO);
			if(oSearchOrderForm != null) {
				orderItemsInfo = OrderDAO.getSearchOrder(oSearchOrderForm, "", "");
				if(orderItemsInfo.size()==0) {
					request.setAttribute(NO_RECORDS_KEY, NO_RECORDS);
				}
			}

			if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
				request.setAttribute(PENDING_ORDERS_INFO, orderItemsInfo);
				oSession.setAttribute(PENDING_ORDERS, alOrderStatusInfo.get(0));
				oSession.setAttribute(INCOMPLETE_ORDERS, alOrderStatusInfo.get(1));
				oSession.setAttribute(COMPLETED_ORDERS, alOrderStatusInfo.get(2));
			}
			logger.info("OrderAction::initPlaceOrder:EXIT");
		}catch(Exception e){		

			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initPlaceOrder:EXCEPTION "+e.getMessage());
		}	
		saveToken(request);
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward processOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::processOrder:ENTER");

		byte[] bBlob = null;
		String sComments = null;
		String sCateFolderName=null;
		String sCCNo = null;
		String sFrdKey="processOrderSuccess";
		String sOrderItemStatus = null;
		/*
		 *  This variable is assigned empty space is because 
		 *  while iterating the selected process String array 
		 *  the first value will not be append with 'null' parameter.
		 *  
		 *  example 
		 *  IF selectedProcess is assiged as null
		 *  	selectedProcess=null+OrderItemId1+OrderItemId2+....
		 *  ELSE selectedProcess is assigned as ""
		 *  	selectedProcess=""+OrderItemId1+OrderItemId2+....
		 *  
		 *  IF part will create an DB Exception as Error in Converting varchar to Bigint
		 *  Else part won't make harm. 
		 */
		String selectedProcess = ""; 
		String sMsg=null;
		String sOwnerType=null;
		String[] selectedProcesses = null;
		String sOrderItemId = null;
		String sImageViewPath=null;
		String sCardType = null;
		String sSourceOfProcess = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		OrderItemDetailsVO oOrderItemDetailsVO = new OrderItemDetailsVO();
		OrderItemDetailsVO oOrderItemDetail = null;
		PaymentResponse  oPaymentResponse= new PaymentResponse();
		List<OrderItemDetailsVO> alOrderItemDetails = null;
		List<OrderItemDetailsVO> alItemStatusList = new ArrayList<OrderItemDetailsVO>();
		List<CategoryVO> alCategory = null;
		Map<String, String> orderItemReasonForFailure = new HashMap<String, String>();

		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			selectedProcesses = request.getParameterValues("selectProcess");
			sOrderItemId=request.getParameter("OrderItemId");
			sSourceOfProcess = request.getParameter("SourceOfProcess");
			
			if(selectedProcesses != null && selectedProcesses.length > 0) {
				for(int j=0;j<selectedProcesses.length;j++) {
					selectedProcess = selectedProcess+selectedProcesses[j]+",";
				}
				selectedProcess=selectedProcess.substring(0,selectedProcess.length()-1);
			}else if(sOrderItemId != null) {
				selectedProcess=sOrderItemId;
			}
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			if(oLoginVO!=null){
				sOwnerType= oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}
			alOrderItemDetails=OrderDAO.getVendorOrderItemDetails(selectedProcess);
			if(isTokenValid(request)) {
				if(alOrderItemDetails!=null && alOrderItemDetails.size()>0){
					for(int i=0;i<alOrderItemDetails.size();i++){
						oOrderItemDetailsVO = (OrderItemDetailsVO)alOrderItemDetails.get(i);

						//Validate CC No
						sCCNo=decryptInputData(oOrderItemDetailsVO.getCCNo(),"CLIENT",oOrderItemDetailsVO.getKeyRefId());
						sCCNo=sCCNo==null?"":sCCNo.trim();
						sCardType = oOrderItemDetailsVO.getCardType();
						oOrderItemDetailsVO.setCCNo(sCCNo);	
						if(sCCNo!=null && sCCNo.trim().length() > 0){
							if(CCUtils.validCC(sCCNo,sCardType)){
								oOrderItemDetailsVO.setMaskCCNo(CCUtils.getMaskCCNo(sCCNo));
								if(FULLY_AUTOMATED.equalsIgnoreCase(oOrderItemDetailsVO.getChargeType())) {
									if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
										if(alCategory!=null && alCategory.size()>0){
											for(CategoryVO oCategoryVO: alCategory){
												if(oCategoryVO.getCateId()!=null){
													if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
														sCateFolderName = oCategoryVO.getCateName();
												}					
											}
										}
									}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
										sCateFolderName = AIRLINE_CATAGEORY_NAME;
									}

									oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"thumb."+oOrderItemDetailsVO.getMainImgType());
									//Call Pre Authorize						
									if(PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetailsVO.getPreAuthCC())) {
										oPaymentResponse = preAuthorizePayment(oOrderItemDetailsVO,oLoginVO.getUserId(),request.getRemoteHost());
										if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() > 0) {

											if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())) {
												String sFilePath = getServlet().getServletContext().getRealPath("/");
												oOrderItemDetailsVO.setCateId(convertToCategoryName(alCategory,oOrderItemDetailsVO.getCateId()));

												VendorUtils.placeOrderItemsToVendor(oOrderItemDetailsVO,sFilePath, oLoginVO.getUserId(),request.getRemoteHost());

											}else{
												sMsg=oBundle.getString("preauth.ccno.error.msg");
												sMsg=sMsg==null?"":sMsg.trim();		
												oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);
												oOrderItemDetailsVO.setCCNoComments(sMsg);
												OrderDAO.sendOrderItemsFailureEmail(oOrderItemDetailsVO,sMsg,0,oLoginVO.getUserId(),"");
												OrderDAO.updateOrderItemStatus(oOrderItemDetailsVO.getOrderItemId(),ORDER_FAILED_STATUS,bBlob,oLoginVO.getUserId());

												/**** Insert Order audit details ******/							
												OrderDAO.insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								
											}
										}
									}else{
										String sFilePath = getServlet().getServletContext().getRealPath("/");
										oOrderItemDetailsVO.setCateId(convertToCategoryName(alCategory,oOrderItemDetailsVO.getCateId()));
										sOrderItemStatus = OrderDAO.getOrderItemStatus(oOrderItemDetailsVO.getOrderItemId());
										if(sOrderItemStatus != null && ORDER_PENDING_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
											VendorUtils.placeOrderItemsToVendor(oOrderItemDetailsVO,sFilePath, oLoginVO.getUserId(),request.getRemoteHost());
										}else {
											sComments = ORDER_ALREADY_PROCESSED;
										}
									}
								}else {
									if(PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetailsVO.getPreAuthCC())){
										oPaymentResponse = preAuthorizePayment(oOrderItemDetailsVO,oLoginVO.getUserId(),request.getRemoteHost());
										if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() > 0) {

											if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())) {
												String sFilePath = getServlet().getServletContext().getRealPath("/");
												oOrderItemDetailsVO.setCateId(convertToCategoryName(alCategory,oOrderItemDetailsVO.getCateId()));
												VendorUtils.placeOrderItemsToVendor(oOrderItemDetailsVO,sFilePath, oLoginVO.getUserId(),request.getRemoteHost());	
												OrderDAO.updateCvvStatus(oOrderItemDetailsVO.getCustId(), "S",oLoginVO.getUserId());
											}else{
												sMsg=oBundle.getString("preauth.ccno.error.msg");
												sMsg=sMsg==null?"":sMsg.trim();		
												oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);
												oOrderItemDetailsVO.setCCNoComments(sMsg);
												OrderDAO.sendOrderItemsFailureEmail(oOrderItemDetailsVO,sMsg,0,oLoginVO.getUserId(),"");
												OrderDAO.updateOrderItemStatus(oOrderItemDetailsVO.getOrderItemId(),ORDER_FAILED_STATUS,bBlob,oLoginVO.getUserId());
												OrderDAO.updateCvvStatus(oOrderItemDetailsVO.getCustId(), ORDER_FAILED_STATUS,oLoginVO.getUserId());
												/**** Insert Order audit details ******/							
												OrderDAO.insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								
											}
										}
									}else{
										String sFilePath = getServlet().getServletContext().getRealPath("/");
										oOrderItemDetailsVO.setCateId(convertToCategoryName(alCategory,oOrderItemDetailsVO.getCateId()));
										sOrderItemStatus = OrderDAO.getOrderItemStatus(oOrderItemDetailsVO.getOrderItemId());
										if(sOrderItemStatus != null && ORDER_PENDING_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
											VendorUtils.placeOrderItemsToVendor(oOrderItemDetailsVO,sFilePath, oLoginVO.getUserId(),request.getRemoteHost());
										}else {
											sComments = ORDER_ALREADY_PROCESSED;
										}
									}
								}
							}else{							
								sMsg=oBundle.getString("customer.ccno.error.msg");
								sMsg=sMsg==null?"":sMsg.trim();		
								oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);							
								OrderDAO.sendOrderItemsFailureEmail(oOrderItemDetailsVO,sMsg,0,oLoginVO.getUserId(),"");
								OrderDAO.updateOrderItemStatus(oOrderItemDetailsVO.getOrderItemId(),ORDER_FAILED_STATUS,bBlob,oLoginVO.getUserId());

								/**** Insert Order audit details ******/	
								OrderDAO.insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								
							}	
						}

						orderItemReasonForFailure.put(oOrderItemDetailsVO.getOrderItemId(), oPaymentResponse.getR_Error_Msg());
						oOrderItemDetail = OrderDAO.getOrderItemDetailStatus(oOrderItemDetailsVO.getOrderItemId());
						if(oOrderItemDetail != null && PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetail.getPreAuthCC())) {
							oOrderItemDetail.setReasonForFailure(oPaymentResponse.getR_Error_Msg());
						}

						if(PRE_AUTHORIZATION_NO.equalsIgnoreCase(oOrderItemDetail.getPreAuthCC())) { 
							if(SEMI_AUTOMATED.equalsIgnoreCase(oOrderItemDetail.getChargeType())) {
								oOrderItemDetail.setPaymentTxnRefId("");
								if(sComments != null && sComments.trim().length() > 0 ) {
									oOrderItemDetail.setReasonForFailure(sComments);
								}
							}else if(FULLY_AUTOMATED.equalsIgnoreCase(oOrderItemDetail.getChargeType())) {
								if(sComments != null && sComments.trim().length() > 0 ) {
									oOrderItemDetail.setReasonForFailure(sComments);
								}
							}
						}
						if(oPaymentResponse.getR_TxnRefId() < 0) {	
							if(oPaymentResponse.getR_TxnRefId() == -1 && PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetail.getPreAuthCC()))
								oOrderItemDetail.setReasonForFailure(ORDER_IN_PROCESS);
							else if(oPaymentResponse.getR_TxnRefId() == -2 && PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetail.getPreAuthCC()))
								oOrderItemDetail.setReasonForFailure(ORDER_ALREADY_PROCESSED);
						}
						alItemStatusList.add(oOrderItemDetail);
					}
				}
				oSession.setAttribute("ItemStatusList", alItemStatusList);
				oSession.setAttribute("completedProcessOrder", "true");
				if(alItemStatusList != null && !alItemStatusList.isEmpty() && alItemStatusList.size() <= 5)
					oSession.setAttribute("NoScroll", "NOSCROLL");
			}
			request.setAttribute("SourceOfProcess", sSourceOfProcess);
			
			resetToken(request);
			logger.info("OrderAction::processOrder:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::processOrder:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward initSearchOrderSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initSearchOrderSummary:ENTER");    	  
		String sFwrdKey="initAdminSearchOrderSuccess",sURI=null,sPath=null;    	
		HttpSession oSession = request.getSession();
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		LoginVO oLoginVO =null;
		String sOwnerType =null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFwrdKey="initVendorSearchOrderSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFwrdKey="initAirlineSearchOrderSuccess";  
			}
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchOrderForm");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oSearchOrderForm.setOrderToDate(sCurrentDate);

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initSearchOrderSummary:Exception "+e.getMessage());
		}
		logger.info("OrderAction::initSearchOrderSummary:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward searchOrderSummary(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::searchOrderSummary:ENTER");
		String sFrdKey="AdminSearchOrderSuccess",sURI=null,sPath=null;
		List<OrderItemDetailsVO> alOrderItemDetails = null;	
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sSearchBy=null,sSearchValue = null , sCategory =null,sOrderFromDate = null,sOrderToDate=null;
		String sOwnerId=null,sOwnerType=null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId ==null?"":sOwnerId.trim();
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			String sOrderStatus =  request.getParameter("OrderStatus");
			sOrderStatus = sOrderStatus == null?"":sOrderStatus.trim();

			if(oSearchOrderForm!=null){	
				sSearchBy=oSearchOrderForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchOrderForm.setSearchBy(sSearchBy);

				sSearchValue=oSearchOrderForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchOrderForm.setSearchValue(sSearchValue);

				sCategory = oSearchOrderForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchOrderForm.setCategory(sCategory);

				sOrderFromDate = oSearchOrderForm.getOrderFromDate();
				sOrderFromDate=sOrderFromDate==null?"":sOrderFromDate.trim();
				oSearchOrderForm.setOrderFromDate(sOrderFromDate);	

				sOrderToDate = oSearchOrderForm.getOrderToDate();
				sOrderToDate=sOrderToDate==null?"":sOrderToDate.trim();
				oSearchOrderForm.setOrderToDate(sOrderToDate);	

				if(sOrderStatus.trim().length()>0){
					oSearchOrderForm.setOrderStatus(sOrderStatus);
					oSearchOrderForm.setSearchBy("ALL");
					oSearchOrderForm.setSearchValue("");
					oSearchOrderForm.setCategory("ALL");	
				}


			}
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorSearchOrderSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineSearchOrderSuccess";  
			}

			alOrderItemDetails =  OrderDAO.getSearchOrderSummary(oSearchOrderForm,sOwnerType,sOwnerId);
			if(alOrderItemDetails!=null && alOrderItemDetails.size()>0){
				request.setAttribute("orderItemsInfo", alOrderItemDetails);
			}else{
				request.setAttribute(NO_RECORDS_KEY,NO_RECORDS);
			} 			

			logger.info("OrderAction::searchOrderSummary:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::searchOrderSummary:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward editCreditCardDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::editCreditCardDetails:ENTRY");

		String sForwardKey = "adminCreditCardDetailsSuccess";
		String sOrderItemId = request.getParameter("OrderItemId");
		PaymentInformationVO paymentInfoVO;
		CreditCardVO oCreditCardVO = null;
		String sSourceOfEdit = request.getParameter("SourceOfEdit");
		OrderItemForm orderItemForm = (OrderItemForm)form;
		HttpSession oSession = request.getSession();
		Calendar today = Calendar.getInstance();
		Integer year=today.get(Calendar.YEAR);
		List<CreditCardVO> years = new ArrayList<CreditCardVO>();
		try {
			for(int i=0;i<10;i++) {
				oCreditCardVO = new CreditCardVO();
				oCreditCardVO.setYearCode(year.toString());
				oCreditCardVO.setYearValue(year.toString());
				years.add(oCreditCardVO);
				year++;
			}
			oSession.setAttribute("ExpYears", years);
			paymentInfoVO = getCreditCardInformation(sOrderItemId);
			paymentInfoVO.setMaskedCCNo(paymentInfoVO.getCreditCardNoLFD());
			if(paymentInfoVO != null) {	
				BeanUtils.copyProperties(orderItemForm, paymentInfoVO);
				if(orderItemForm.getCreditCardNo() != null && orderItemForm.getCreditCardNo().trim().length()>0) 
					orderItemForm.setCreditCardNo(decryptInputData(orderItemForm.getCreditCardNo(),"CLIENT",paymentInfoVO.getKeyRefId()));
				
				oSession.setAttribute("CreditCardDetails", paymentInfoVO);
			}
			request.setAttribute("SourceOfEdit", sSourceOfEdit);
		}catch(Exception e) {
			logger.info("OrderAction::editCreditCardDetails:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		if(orderItemForm != null) {
			request.setAttribute("custCreditCardInfo", orderItemForm);
		}

		logger.info("OrderAction::editCreditCardDetails:EXIT");
		return mapping.findForward(sForwardKey);
	}
	public ActionForward updateCreditCardDetail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::updateCreditCardDetail::ENTRY");

		boolean isCreditCardDetailChanged = true;
		String sForwardKey="AdminEditItemDetailsSuccess";
		String sOrderItemId = request.getParameter("OrderItemId");
		String sSourceOfEdit = request.getParameter("SourceOfEdit");
		String sImageViewPath = null,sCateFolderName = null;
		HttpSession oSession = request.getSession(true);
		LoginVO oLoginVO = null;
		String sModifiedFields = null;
		OrderItemForm oOrderItemForm = (OrderItemForm) form;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		PaymentInformationVO oPaymentInfoVO;
		List<CreditCardVO> expYears = null;
		ArrayList alAllColors = null,alAllSizes = null;
		ArrayList<CategoryVO> alCategory = null;
		try{
			oSession.setAttribute("ISCREDITCARDCHANGED", "NO");
			if(oOrderItemForm != null) {
				oPaymentInfoVO=(PaymentInformationVO)oSession.getAttribute("CreditCardDetails");
				if(oOrderItemForm.getCreditCardNo() != null && oOrderItemForm.getCreditCardNo().trim().length() > 20) {
					oOrderItemForm.setCreditCardNo(decryptInputData(oOrderItemForm.getCreditCardNo(),"CLIENT",oPaymentInfoVO.getKeyRefId()));
				}
				if(!validCC(oOrderItemForm.getCreditCardNo(), oOrderItemForm.getCardType())) {
					sForwardKey="CreditCardValidFailure";
					request.setAttribute("InvalidCC", "Entered Credit Card No. is Invalid");
					expYears=(List<CreditCardVO>)oSession.getAttribute("ExpYears");
					request.setAttribute("ExpYears", expYears);
				}else if(!Utils.isValidExpDate(Integer.parseInt(oOrderItemForm.getExpMonth()), Integer.parseInt(oOrderItemForm.getExpYear()))){
					sForwardKey="CreditCardValidFailure";
					request.setAttribute("InvalidCC", "Invalid expiration date.");
					expYears=(List<CreditCardVO>)oSession.getAttribute("ExpYears");
					request.setAttribute("ExpYears", expYears);
				}else {
					oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
					
					if(oOrderItemForm.getCustAddress2() == null) 
						oOrderItemForm.setCustAddress2("");
					//isCreditCardDetailChanged = isCred itCardDetailsChanged(oPaymentInfoVO,oOrderItemForm);
					sModifiedFields = OrderDAO.insertOrderItemAuditMessage(oPaymentInfoVO,oOrderItemForm);

					if(isCreditCardDetailChanged && oOrderItemForm.getCvv()!=null) {
						oSession.setAttribute("ISCREDITCARDCHANGED", "YES");
						oOrderItemForm.setCreditCardLFD(getMaskCCNo(oOrderItemForm.getCreditCardNo()));

						oOrderItemForm.setCreditCardNo(encryptInputData(oOrderItemForm.getCreditCardNo(),"CLIENT",oPaymentInfoVO.getKeyRefId()));
						oOrderItemForm.setCvv(encryptInputData(oOrderItemForm.getCvv(),"CLIENT",oPaymentInfoVO.getKeyRefId()));
						oOrderItemForm.setCardType(encryptInputData(oOrderItemForm.getCardType(),"CLIENT",oPaymentInfoVO.getKeyRefId()));
						String sExpDate = oOrderItemForm.getExpMonth()+"/"+oOrderItemForm.getExpYear();
						oOrderItemForm.setExpDate(encryptInputData(sExpDate,"CLIENT",oPaymentInfoVO.getKeyRefId()));
						oOrderItemForm.setCreditCardHolderName(encryptInputData(oOrderItemForm.getCreditCardHolderName(),"CLIENT",oPaymentInfoVO.getKeyRefId()));

						updateCreditCardDetails(oOrderItemForm,oLoginVO.getUserId());

						insertOrderAuditDetails(oOrderItemForm.getOrderItemId(), oOrderItemForm.getOrderId(), oOrderItemForm.getCustTransId(), sModifiedFields, oLoginVO.getUserId(), null, oLoginVO.getUserId(), "");
					}else {
						oSession.setAttribute("ISCREDITCARDCHANGED", "NO");
					}
				}
			}

			oOrderItemDetailsVO = OrderDAO.getCustOrderItemDetails(sOrderItemId);
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(AIRLINE.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
			}
			if(sSourceOfEdit != null) {
				if("placeorder".equalsIgnoreCase(sSourceOfEdit)) {
					oSession.setAttribute("PlaceOrder", "YES");
				}else {
					oSession.setAttribute("PlaceOrder", "NO");
				}
			}
			if(oOrderItemDetailsVO!=null){		

				alAllColors = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllColors());
				alAllColors = alAllColors==null?new ArrayList():alAllColors;
				if(alAllColors!=null && alAllColors.size()>0)
					request.setAttribute("allColors",alAllColors);

				alAllSizes = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllSizes());
				alAllSizes = alAllSizes==null?new ArrayList():alAllSizes;

				if(alAllSizes!=null && alAllSizes.size()>0)
					request.setAttribute("allSizes",alAllSizes);

				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				// System.out.println("custOrderItemDetails:main img path:"+oOrderItemDetailsVO.getMainImgPath());
				BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO); 
				request.setAttribute("custOrderItemDetails",oOrderItemDetailsVO);
				request.setAttribute("LoginType", oLoginVO.getUserType());
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
				request.setAttribute("mode","Edit" );

			} 

			oSession.setAttribute("OrderDetailsUnchanged", oOrderItemDetailsVO);
		}catch(Exception e){
			logger.debug("OrderAction::updateCreditCardDetail::EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::updateCreditCardDetail:EXIT");
		return mapping.findForward(sForwardKey);
	}
	public ActionForward getOrderTrackingDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::getOrderTrackingDetails::ENTRY");

		String sForwardKey="adminOrderTrackingDetailsSuccess";
		String sOrderItemId = request.getParameter("OrderItemId");
		String sItemStatus = request.getParameter("ItemStatus");
		String sSourceOfEdit = request.getParameter("PlaceOrder");
		String sIsViewOrCharge = request.getParameter("ViewOrder");
		String sOrderItemStatus = "";
		HttpSession oSession = request.getSession();
		List<OrderTrackingDetailsVO> orderTrackingList = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		List<CategoryVO> alCategory = null;
		String sImageViewPath = null;
		String sCateFolderName = null;
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		try {
			OrderItemForm oOIForm = (OrderItemForm)form;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) 
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			else if(AIRLINE.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);	
			else if(VENDOR.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO != null) {
				if(AIRLINE.equalsIgnoreCase(oLoginVO.getUserType())) 
					sForwardKey="airlineOrderTrackingDetailsSuccess"; 
				else if(VENDOR.equalsIgnoreCase(oLoginVO.getUserType())) 
					sForwardKey="vendorOrderTrackingDetailsSuccess"; 
			}
			orderTrackingList = getOrderTrackingDetail(sOrderItemId);
			for(OrderTrackingDetailsVO oOrderTrackingDetailsVO:orderTrackingList){
				oOIForm.setShipmentStatus(oOrderTrackingDetailsVO.getShipmentStatus());
			}
			if(orderTrackingList==null || orderTrackingList.size()==0){
				oOIForm.setShipmentStatus("N");
			}

			sOrderItemStatus = OrderDAO.getOrderItemStatus(sOrderItemId);
			if(!sItemStatus.equalsIgnoreCase(sOrderItemStatus)) {
				if(ORDER_CANCELED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
					request.setAttribute("AlreadyProcessed", ORDER_ALREADY_CANCELED);
				}else if(ORDER_REJECTED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
					request.setAttribute("AlreadyProcessed", ORDER_ALREADY_REJECTED);
				}else if(ORDER_TOBE_CHARGED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
					request.setAttribute("AlreadyProcessed", ORDER_ALREADY_PROCESSED);
				}	
				oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
				sForwardKey = "AdminOrderProcessedSuccess";
			}
			request.setAttribute("ShipStatus", oOIForm.getShipmentStatus());
			request.setAttribute("TrackingDetails", orderTrackingList);
			request.setAttribute("OrderItemId", sOrderItemId);
			request.setAttribute("ItemStatus", sItemStatus);
			request.setAttribute("IsViewOrCharge", sIsViewOrCharge);
			request.setAttribute("SourceOfEdit", sSourceOfEdit);

		}catch(Exception e) {
			logger.debug("OrderAction::getOrderTrackingDetails::EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}

		logger.info("OrderAction::getOrderTrackingDetails::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward updateOrderTrackingDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		String sFrdKey="AdminEditItemDetailsSuccess";
		OrderItemDetailsVO oOrderItemDetailsVO = null;	
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null,sCateFolderName = null;
		HttpSession oSession = request.getSession();
		List<CreditCardVO> expYears = null;
		OrderItemForm oOrderItemForm=(OrderItemForm)form; 		
		String sOwnerType = null,sURI=null,sPath=null;
		LoginVO oLoginVO =null;

		String placeOrder=request.getParameter("PlaceOrder");

		String sOrderItemId = request.getParameter("OrderItemId");
		String sItemStatus = request.getParameter("ItemStatus");
		String sIsViewOrCharge = request.getParameter("ViewOrder");
		ArrayList alAllColors = null,alAllSizes = null;

		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			String sIsUpdate = request.getParameter("IsUpdate");

			if("add".equalsIgnoreCase(sIsUpdate)) {
				if(oOrderItemForm != null) {
					addOrderTrackingDetails(oOrderItemForm,oLoginVO.getUserId());
					if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(sItemStatus)) {
						sendTrackingDetailsEmailToCustomer(oOrderItemForm,oLoginVO.getUserId(),"");
						//sendTrackingDetailsEmailToVendor(oOrderItemForm,oLoginVO.getUserId(),"");
					}
				}
			}

			oOrderItemDetailsVO = OrderDAO.getCustOrderItemDetails(sOrderItemId);

			if(oLoginVO!= null){				
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}

			if(sOwnerType.equalsIgnoreCase(ADMIN)) {
				if("CHARGE".equalsIgnoreCase(sIsViewOrCharge)) {
					sFrdKey = "AdminChargeItemDetailsSuccess";
				}else if("VIEW".equalsIgnoreCase(sIsViewOrCharge) && ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())){
					sFrdKey = "AdminViewItemDetailsSuccess";
				}
			}
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorEditItemDetailsSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineEditItemDetailsSuccess";  
			}


			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(AIRLINE.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
			}
			if(placeOrder != null) {
				if("placeorder".equalsIgnoreCase(placeOrder)) {
					oSession.setAttribute("PlaceOrder", "YES");
				}else {
					oSession.setAttribute("PlaceOrder", "NO");
				}
			}
			if(oOrderItemDetailsVO!=null){		

				alAllColors = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllColors());
				alAllColors = alAllColors==null?new ArrayList():alAllColors;
				if(alAllColors!=null && alAllColors.size()>0)
					request.setAttribute("allColors",alAllColors);

				alAllSizes = Utils.commaSepertedStringToList(oOrderItemDetailsVO.getAllSizes());
				alAllSizes = alAllSizes==null?new ArrayList():alAllSizes;

				if(alAllSizes!=null && alAllSizes.size()>0)
					request.setAttribute("allSizes",alAllSizes);

				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				// System.out.println("custOrderItemDetails:main img path:"+oOrderItemDetailsVO.getMainImgPath());
				oOrderItemDetailsVO.setCvv("");

				BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO); 
				request.setAttribute("custOrderItemDetails",oOrderItemDetailsVO);
				request.setAttribute("LoginType", sOwnerType);
				request.setAttribute("SourceOfEdit", placeOrder);
				request.setAttribute("mode","Edit" );

			} 
			logger.info("OrderAction::editOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::editOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}
	public ActionForward chargeOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::chargeOrder::ENTER");

		String sForwardKey="adminOrderChargeSuccess";
		OrderItemDetailsVO oOrderItemDetailsVO = null;	
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		OrderItemForm oOrderItemForm = (OrderItemForm)form;
		String sOrderItemId= null;
		String sImageViewPath = null,sCateFolderName = null;
		ArrayList<CategoryVO> alCategory = null;
		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}

			sOrderItemId=request.getParameter("OrderItemId");
			if(sOrderItemId!=null){
				oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
//				oOrderItemDetailsVO.setMaskCCNo(getMaskCCNo(oOrderItemDetailsVO.getCCNo()));

				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					if(!ORDER_TOBE_CHARGED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
						if(ORDER_REJECTED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus()))
							request.setAttribute("AlreadyProcessed", ORDER_ALREADY_REJECTED);
						else if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus()))
							request.setAttribute("AlreadyProcessed", ORDER_ALREADY_COMPLETED);

						request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
						sForwardKey = "adminOrderAlreadyProcessed";
					}
				}

				BeanUtils.copyProperties(oOrderItemForm, oOrderItemDetailsVO);
				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				//// System.out.println("custOrderItemDetails:main img path:"+oOrderItemDetailsVO.getMainImgPath());
				oSession.setAttribute("chargeOrderItem", oOrderItemDetailsVO);
			}else{
				sForwardKey="failure";
				logger.error("OrderAction::chargeOrder:ERROR:"+NO_ORDERITEMID_PARAMETER);
			}
		}catch(Exception e){		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::chargeOrder:EXCEPTION "+e.getMessage());
		}
		logger.error("OrderAction::chargeOrder:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward processPayment(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		logger.info("OrderAction::processPayment:ENTRY");

		String sForwardKey="adminProcessPaymentSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		byte[] baPdf = null;
		PaymentResponse oPaymentResponse =null;
		String sMsg = null;
		int iOrderNumber = getRandomNo();
		List<OrderItemDetailsVO> alOrderItemDetails;
		List<OrderItemDetailsVO> alOrderItemStatus = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemDetails = null;
		String sFilePath = null, sOrderItemId=null, sPayTxnDetails = null, sCateFolderName = "", sImageViewPath = "", sOrderItemStatus = "";
		List<CategoryVO> alCategory;
		try {
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}

			sFilePath = getServlet().getServletContext().getRealPath("/");

			sOrderItemId=request.getParameter("OrderItemId");
			if(sOrderItemId!=null){
				alOrderItemDetails = getVendorOrderItemDetails(sOrderItemId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				sPayTxnDetails = getPayTxnRefId(sOrderItemId);
				sOrderItemStatus = OrderDAO.getOrderItemStatus(sOrderItemId);

				if(isTokenValid(request)) {
					if(alOrderItemDetails != null && alOrderItemDetails.size() > 0) {

						for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetails)  {

							if(oOrderItemDetailsVO != null) {
								String sCCNo=decryptInputData(oOrderItemDetailsVO.getCCNo(),"CLIENT",oOrderItemDetailsVO.getKeyRefId());
								sCCNo=sCCNo==null?"":sCCNo.trim();						
								oOrderItemDetailsVO.setCCNo(sCCNo);	

								if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
									if(alCategory!=null && alCategory.size()>0){
										for(CategoryVO oCategoryVO: alCategory){
											if(oCategoryVO.getCateId()!=null){
												if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
													sCateFolderName = oCategoryVO.getCateName();
											}					
										}
									}
								}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
									sCateFolderName = AIRLINE_CATAGEORY_NAME;
								}
								oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"thumb."+oOrderItemDetailsVO.getMainImgType());
							}

							oOrderItemDetailsVO.setPaymentTxnRefId(sPayTxnDetails);
							if(ORDER_TOBE_CHARGED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
								//Charge Amount from the card
								if(PRE_AUTHORIZATION_YES.equalsIgnoreCase(oOrderItemDetailsVO.getPreAuthCC())){
									oPaymentResponse = postAuthorizePayment(oOrderItemDetailsVO,oLoginVO.getUserId(),request.getRemoteHost());
								}else{
									oPaymentResponse = salePayment(oOrderItemDetailsVO,oLoginVO.getUserId(),request.getRemoteHost());
								}
								if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() > 0) {	
									if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){			
										
										//To delete the CVV
										if(PRE_AUTHORIZATION_NO.equalsIgnoreCase(oOrderItemDetailsVO.getPreAuthCC()))
											OrderDAO.updateCvvStatus(oOrderItemDetailsVO.getCustId(), "S",oLoginVO.getUserId());
										
										//Add Customer Invoice Details
										OrderDAO.addCustomerInvoiceDetails(oOrderItemDetailsVO,oLoginVO.getUserId());

										String htmlText = createBlob(oOrderItemDetailsVO);
										String sHTMLFileName = sFilePath+oOrderItemDetailsVO.getInvoiceNo()+".html";

										BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
										ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
										oBufWriter.write(htmlText);
										oBufWriter.close();

										PD4ML pd4ml = new PD4ML();
										pd4ml.useTTF( "java:/fonts", true );
										Dimension format = PD4Constants.A4;
										pd4ml.setPageSize(format);
										//pd4ml.setPageSize(new Dimension(615,790));
										pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
										pd4ml.setHtmlWidth(1050);

										pd4ml.enableDebugInfo();

										pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

										baPdf=oByteArrayOutputStream.toByteArray();

										String pdfFileName=sFilePath+oOrderItemDetailsVO.getInvoiceNo()+".pdf";
										FileOutputStream fileos = new FileOutputStream(pdfFileName);
										fileos.write(baPdf);
										fileos.close();

										File oPdfFile = new File(pdfFileName);
										File oHtmlFile = new File(sHTMLFileName);
										try {
											/* 
											 * To Update the Order Item Status and to Update the Invoice Email PDF
											 */
											updateOrderItemStatus(oOrderItemDetailsVO.getOrderItemId(),ORDER_COMPLETED_STATUS,baPdf,oLoginVO.getUserId());
											sendOrderItemsSuccessEmail(oOrderItemDetailsVO,baPdf,iOrderNumber,oPdfFile,oLoginVO.getUserId(),"");

											if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
												OrderDAO.createAndSendVendorPO(oOrderItemDetailsVO,sFilePath,oLoginVO.getUserId(),"");
											}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
												OrderDAO.createAndSendAirlinePO(oOrderItemDetailsVO,sFilePath,oLoginVO.getUserId(),"");
											}
											
											if(!oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
												OrderDAO.sendAirlineCommissionEMail(oOrderItemDetailsVO,sFilePath,oLoginVO.getUserId(),"");
											}
										}catch(Exception e) {
											e.printStackTrace();
										}
										
										/**** Insert Order audit details ******/							
										sMsg=oBundle.getString("customer.order.success.msg");
										sMsg=sMsg==null?"":sMsg.trim();								
										insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								

										
										if(oPdfFile.exists()) {
											oPdfFile.delete();
										}
										if(oHtmlFile.exists()) {
											oHtmlFile.delete();
										}

										
									}else{
										sMsg=oBundle.getString("postauth.ccno.error.msg");
										sMsg=sMsg==null?"":sMsg.trim();		
										oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);
										oOrderItemDetailsVO.setCCNoComments(sMsg);
										OrderDAO.updateOrderItemStatus(oOrderItemDetailsVO.getOrderItemId(),ORDER_FAILED_STATUS,null,oLoginVO.getUserId());
										OrderDAO.sendOrderItemsFailureEmail(oOrderItemDetailsVO,sMsg,0,oLoginVO.getUserId(),"");
										sendOrderItemsFailureEmailToVendor(oOrderItemDetailsVO,sMsg,0,oLoginVO.getUserId(),"");
										
										//To delete the CVV
										if(PRE_AUTHORIZATION_NO.equalsIgnoreCase(oOrderItemDetailsVO.getPreAuthCC()))
											OrderDAO.updateCvvStatus(oOrderItemDetailsVO.getCustId(), ORDER_FAILED_STATUS,oLoginVO.getUserId());
										/**** Insert Order audit details ******/												
										OrderDAO.insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								
									}
									oSession.removeAttribute("OrderProcessed");
								}

								oOrderItemDetails = OrderDAO.getOrderItemDetailStatus(sOrderItemId);

								if(oPaymentResponse.getR_TxnRefId() > 0) {

									if(ORDER_FAILED_STATUS.equalsIgnoreCase(oOrderItemDetails.getOrderItemStatus())) {
										oOrderItemDetails.setReasonForFailure(oPaymentResponse.getR_Error_Msg());
									}else {
										oOrderItemDetails.setReasonForFailure("");
									}
								}else {
									if(oPaymentResponse.getR_TxnRefId() == -1)
										//oOrderItemDetails.setReasonForFailure("Order is in process");
										oSession.setAttribute("OrderProcessed", ORDER_IN_PROCESS);
									if(oPaymentResponse.getR_TxnRefId() == -2)
										//oOrderItemDetails.setReasonForFailure(ORDER_ALREADY_PROCESSED);
										oSession.setAttribute("OrderProcessed", ORDER_ALREADY_PROCESSED);
								}
								alOrderItemStatus.add(oOrderItemDetails);
							}else {
								oOrderItemDetails = OrderDAO.getOrderItemDetailStatus(sOrderItemId);
								alOrderItemStatus.add(oOrderItemDetails);
								if(ORDER_REJECTED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
									oSession.setAttribute("OrderProcessed", ORDER_ALREADY_REJECTED);
								}else if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
									oSession.setAttribute("OrderProcessed", ORDER_ALREADY_COMPLETED);
								}
							}
						}
					}
					oSession.setAttribute("ProcessPaymentStatusList", alOrderItemStatus);
				}
				/*orderItemStatus = getOrderItemDetailsStatus(sOrderItemId);

			for(OrderItemDetailsVO oOrderItemStatus:orderItemStatus) {
				if(ORDER_FAILED_STATUS.equalsIgnoreCase(oOrderItemStatus.getOrderItemStatus())) {
					oOrderItemStatus.setReasonForFailure(oPaymentResponse.getR_Error_Msg());
					alOrderItemStatus.add(oOrderItemStatus);
				}else {
					oOrderItemStatus.setReasonForFailure("");
					alOrderItemStatus.add(oOrderItemStatus);
				}
			}
			request.setAttribute("ProcessPaymentStatusList", alOrderItemStatus);*/
			}else{
				sForwardKey="failure";
				logger.error("OrderAction::processPayment:ERROR:"+NO_ORDERITEMID_PARAMETER);
			}
		}catch(Exception e) {
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::processPayment:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::processPayment:EXIT");
		return mapping.findForward(sForwardKey);
	}
	public ActionForward processPaymentStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.error("OrderAction::processPaymentStatus:ENTER");
		String sForwardKey="adminProcessPaymentStatusSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}
		}catch(Exception e){		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::processPaymentStatus:EXCEPTION "+e.getMessage());
		}
		logger.error("OrderAction::processPaymentStatus:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward initRejectOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initRejectOrder:ENTRY");

		String sForwardKey="adminInitRejectOrderSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		String sOrderItemStatus = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		String sOrderItemId=request.getParameter("OrderItemId");
		String sCateFolderName = "";
		List<CategoryVO> alCategory = null;
		String sImageViewPath = "";
		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}
			request.setAttribute("OrderItemId", sOrderItemId);
			sOrderItemStatus = OrderDAO.getOrderItemStatus(sOrderItemId);
			if(!ORDER_TOBE_CHARGED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
				request.setAttribute("AlreadyProcessed", ORDER_ALREADY_REJECTED);
				oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
				sForwardKey = "adminOrderRejectSuccess";
			}
		}catch(Exception e){		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initRejectOrder:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::initRejectOrder:EXIT");
		return mapping.findForward(sForwardKey);
	}
	public ActionForward rejectOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::rejectOrder:ENTRY");

		String sForwardKey="adminRejectOrderSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		String sOrderItemId=request.getParameter("OrderItemId");
		String sOwnerType, sOwnerId;
		OrderItemForm oOrderItemForm = (OrderItemForm)form;
		oOrderItemForm.setOrderItemId(sOrderItemId);
		oOrderItemForm.setOrderItemStatus(ORDER_FAILED_STATUS);
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		String sCateFolderName = "";
		String sImageViewPath = "";
		List<CategoryVO> alCategory = null;
		SearchOrderForm oSearchOrderForm;
		ArrayList alStateList = null;
		try {
			alStateList = (ArrayList)oSession.getServletContext().getAttribute("StateList");
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}
			sOwnerId=oLoginVO.getRefId();
			sOwnerType=oLoginVO.getUserId();
			sOwnerId=sOwnerId==null?"":sOwnerId.trim();
			sOwnerType=sOwnerType==null?"":sOwnerType.trim();

			oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);

			if(oOrderItemDetailsVO != null && ORDER_TOBE_CHARGED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
				//Status and reject reason has to be updated.
				updateRejectReason(oOrderItemForm, oOrderItemDetailsVO,oLoginVO.getUserId());
				sendOrderItemsRejectionEmail(oOrderItemDetailsVO,oOrderItemForm.getRejectReason(),0,oLoginVO.getUserId(),"");
			}else{
				if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
					request.setAttribute("AlreadyProcessed", ORDER_ALREADY_COMPLETED);
				}else if(ORDER_REJECTED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
					request.setAttribute("AlreadyProcessed", ORDER_ALREADY_REJECTED);
				}
			}

			oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
			}
			request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);

		}catch(Exception e) {		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::rejectOrder:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::rejectOrder:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward getCustOrderTrackingDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getCustOrderTrackingDetails:ENTRY");

		String sForwardKey="customerOrderTrackingDetailsSuccess";
		String sOrderItemId = request.getParameter("OrderItemId");
		List<OrderTrackingDetailsVO> orderTrackingList = null;
		OrderTrackingDetailsVO oOrderTrackingDetailsVO=null;
		try {
			orderTrackingList = getOrderTrackingDetail(sOrderItemId);

			if(orderTrackingList != null && orderTrackingList.size() > 0) {
				oOrderTrackingDetailsVO = orderTrackingList.get(0);
				request.setAttribute("ShipmentStatus", oOrderTrackingDetailsVO.getShipmentStatus());
				request.setAttribute("NoTrackingDetails", "Shipped");
			}else {
				request.setAttribute("NoTrackingDetails", "This order is not yet shipped");
			}
			//			request.setAttribute("CustTrackingDetails", oOrderTrackingDetailsVO);
			request.setAttribute("TrackingDetails", orderTrackingList);
			request.setAttribute("OrderItemId", sOrderItemId);
		}catch(Exception e) {
			logger.debug("OrderAction::getCustOrderTrackingDetails:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}

		logger.info("OrderAction::getCustOrderTrackingDetails:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward getReasonForFailure(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getReasonForFailure:ENTRY");

		String sForwardKey="ReasonForFailureSuccess";
		String sCustTransId = null;
		String sOrderItemId=request.getParameter("OrderItemId");
		String sOrderItemStatus = request.getParameter("ItemStatus");
		String sSourceOfEdit = request.getParameter("PlaceOrder");
		String sMode = request.getParameter("Mode");
		HttpSession oSession = request.getSession();
		List<CustPayTxnVO> alCustPayTxnVO = new ArrayList<CustPayTxnVO>();
		try{
			alCustPayTxnVO = getTxnFailureDetails(sOrderItemId);

			if(alCustPayTxnVO != null && alCustPayTxnVO.size()>0) {
				for(CustPayTxnVO oCustPayTxnVO:alCustPayTxnVO) {

					sCustTransId = oCustPayTxnVO.getCustTransId();
					oCustPayTxnVO.setTransactionDate(dateStampConversion(oCustPayTxnVO.getTransactionDate()));
					oCustPayTxnVO.setMaskedCreditCardNo(oCustPayTxnVO.getCreditCardLFD());
					if(sOrderItemStatus == null || sOrderItemStatus.trim().length() == 0) {
						sOrderItemStatus = oCustPayTxnVO.getOrderItemStatus();
					}
				}
				oSession.setAttribute("ReasonForFailure", alCustPayTxnVO);
				request.setAttribute("custTransId", sCustTransId);
			}else{
				oSession.removeAttribute("ReasonForFailure");
			}
			request.setAttribute("OrderItemId", sOrderItemId);
			request.setAttribute("ItemStatus", sOrderItemStatus);
			request.setAttribute("SourceOfEdit", sSourceOfEdit);
			request.setAttribute("Mode", sMode);
		}catch(Exception e) {
			logger.debug("OrderAction::getReasonForFailure:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}

		logger.info("OrderAction::getReasonForFailure:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward displayTransactionDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::displayTransactionDetails:ENTRY");

		String sForwardKey="ViewTransactionDetailsSuccess";
		String sTransactionId = request.getParameter("TransactionId");
		HttpSession oSession = request.getSession();
		CustPayTxnVO oCustPayTxnVO = new CustPayTxnVO();
		List<CustPayTxnVO> alCustPayTxnVO = (List<CustPayTxnVO>) oSession.getAttribute("ReasonForFailure");
		try{
			if(alCustPayTxnVO != null && alCustPayTxnVO.size() > 0) {
				if(sTransactionId != null) {
					for(CustPayTxnVO oCustPayTxn:alCustPayTxnVO) {
						if(sTransactionId.equalsIgnoreCase(oCustPayTxn.getTransactionId())) {
							oCustPayTxnVO = oCustPayTxn;
							break;
						}
					}
				}
				request.setAttribute("TransactionDetails", oCustPayTxnVO);
			} 
		}catch(Exception e) {
			logger.debug("OrderAction::displayTransactionDetails:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::displayTransactionDetails:EXIT");
		return mapping.findForward(sForwardKey);
	}

	/**** Customer Feedback *******/

	public ActionForward initCustFeedbackSearch(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initCustFeedbackSearch:ENTRY");

		String sForwardKey="searchCustFeedbackSearchSuccess";
		try{
			DynaActionForm oForm = (DynaActionForm)form;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oForm.set("orderToDate", sCurrentDate);
		}catch(Exception e) {
			logger.debug("OrderAction::initCustFeedbackSearch:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}

		logger.info("OrderAction::initCustFeedbackSearch:EXIT");
		return mapping.findForward(sForwardKey);

	}

	public ActionForward searchCustFeedback(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::searchCustFeedback:ENTRY");
		List<CustomerSuggestionsVO> alCustFeedback = null;
		String sSearchType = null,sSearchValue = null, sFromDate = null, sToDate = null;
		String sForwardKey="searchCustFeedbackSearchSuccess";
		try{
			DynaActionForm oForm = (DynaActionForm)form;

			sSearchType = oForm.getString("searchType");
			sSearchType = sSearchType==null?"":sSearchType.trim();

			sSearchValue = oForm.getString("searchValue");
			sSearchValue = sSearchValue==null?"":sSearchValue.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();

			alCustFeedback = CustomerDAO.getCustomerSuggestions(sSearchType, sSearchValue, sFromDate, sToDate);

			if(alCustFeedback != null && alCustFeedback.size()>0) {
				request.setAttribute("CustSuggInfo", alCustFeedback);
			}else{
				request.setAttribute(NO_RECORDS_KEY, NO_RECORDS_FOUND);
			}
		}catch(Exception e) {
			logger.debug("OrderAction::searchCustFeedback:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::searchCustFeedback:EXIT");
		return mapping.findForward(sForwardKey);

	}

	public ActionForward viewCustFeedback(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::viewCustFeedback:ENTRY");
		CustomerSuggestionsVO oCustomerSuggestionsVO = null;
		String sCustId = null;
		String sForwardKey="viewCustFeedbackSearchSuccess";
		try{
			sCustId = request.getParameter("custId");
			sCustId = sCustId==null?"":sCustId.trim();

			if(sCustId!=null){
				oCustomerSuggestionsVO = CustomerDAO.getCustomerSuggestionsByCustId(sCustId);
			}
			if(oCustomerSuggestionsVO != null) {
				request.setAttribute("custSuggDetails", oCustomerSuggestionsVO);
			}else{
				request.setAttribute(NO_RECORDS_KEY, NO_RECORDS_FOUND);
			}
		}catch(Exception e) {
			logger.debug("OrderAction::viewCustFeedback:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::viewCustFeedback:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward getCustFeedbackEmail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getCustFeedbackEmail:ENTRY");
		CustomerSuggestionsVO oCustomerSuggestionsVO = null;
		String sCustId = null,sAdminEmailAddr = null;
		HttpSession oSession = request.getSession();
		String sAdminEmailAddress = "";
		String sForwardKey="viewCustFeedbackEmailSuccess";
		try{

			sAdminEmailAddr=sAdminEmailAddress;
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sCustId = request.getParameter("custId");
			sCustId = sCustId==null?"":sCustId.trim();

			if(sCustId!=null){
				oCustomerSuggestionsVO = CustomerDAO.getCustomerSuggestionsByCustId(sCustId);
			}
			if(oCustomerSuggestionsVO != null) {
				request.setAttribute("custSuggDetails", oCustomerSuggestionsVO);
			}else{
				request.setAttribute(NO_RECORDS_KEY, NO_RECORDS_FOUND);
			}
		}catch(Exception e) {
			logger.debug("OrderAction::getCustFeedbackEmail:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::getCustFeedbackEmail:EXIT");
		return mapping.findForward(sForwardKey);
	}


	public ActionForward sendCustFeedbackEmail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::sendCustFeedbackEmail:ENTRY");
		CustomerSuggestionsVO oCustomerSuggestionsVO = null;
		String sAdminEmailAddr = null, sEmailFromAddr = null, sEmailToAddr = null, sEmailCcAddr = null, sEmailBccAddr = null, sEmailSubject = null, sEmailContent = null;
		String sForwardKey="viewCustFeedbackEmailSuccess";
		HttpSession oSession = request.getSession();
		String sAdminEmailAddress = "";
		try{

			sAdminEmailAddr=sAdminEmailAddress;
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sEmailToAddr = request.getParameter("emailToAddr");
			sEmailToAddr = sEmailToAddr==null?"":sEmailToAddr.trim();

			/*sEmailFromAddr = request.getParameter("emailFromAddr");
			sEmailFromAddr = sEmailFromAddr==null?"":sEmailFromAddr.trim();*/

			sEmailCcAddr = request.getParameter("emailCcAddr");
			sEmailCcAddr = sEmailCcAddr==null?"":sEmailCcAddr.trim();

			sEmailBccAddr = request.getParameter("emailBccAddr");
			sEmailBccAddr = sEmailBccAddr==null?"":sEmailBccAddr.trim();

			sEmailSubject = request.getParameter("emailSubject");
			sEmailSubject = sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent = request.getParameter("emailContent");
			sEmailContent = sEmailContent==null?"":sEmailContent.trim();


			LoginVO oLoginVO = null;
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);	

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sEmailToAddr));
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sEmailCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			//oEmailLogVo.setOwnerId(p_lOwnerId);
			//oEmailLogVo.setOwnerType(ADMIN);
			oEmailLogVo.setEmailToAddr(sEmailToAddr);
			if(sEmailCcAddr!="")
				oEmailLogVo.setEmailCcAddr(sAdminEmailAddr+","+sEmailCcAddr);
			else
				oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			oEmailLogVo.setEmailBccAddr(sEmailBccAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,oLoginVO.getUserId(),"","");

		}catch(Exception e) {
			logger.debug("OrderAction::sendCustFeedbackEmail:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::sendCustFeedbackEmail:EXIT");
		return mapping.findForward(sForwardKey);
	}



	/*public ActionForward viewEmailPdf(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)
		throws Exception { 
			logger.info("OrderAction::viewEmailPdf:ENTER");
			byte[] baEmailPdf=null;
			String sOrderItemId = null;
			String sEmail = null;
			try{
				sOrderItemId=request.getParameter("orderItemId");
				sEmail=request.getParameter("email");
				if(sOrderItemId!=null){
					baEmailPdf = OrderDAO.getOrderItemEmailPdf(sOrderItemId,sEmail);
				}
				if(baEmailPdf!=null){
					ServletOutputStream stream = response.getOutputStream();
					response.setContentType("application/pdf");
					response.setContentLength(baEmailPdf.length);
					response.setHeader("Content-Disposition","Email" + ";filename=" + sOrderItemId);
					stream.write(baEmailPdf);
					stream.flush();
					stream.close();

				}


				logger.info("OrderAction::viewEmailPdf:EXIT");
			}catch(Exception e){		
				logger.error("OrderAction::viewEmailPdf:EXCEPTION "+e.getMessage());
				return mapping.findForward"failure";		

			}	
			return null;
		}*/

	public ActionForward viewEmailPdf(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::viewEmailPdf:ENTER");
		
		byte[] baEmailPdf=null;
		String sOrderItemId = null;
		String sEmail = null,sStatus = null;;
		try{
			sOrderItemId=request.getParameter("orderItemId");
			sEmail=request.getParameter("email");
			sStatus = request.getParameter("status");
			if(sOrderItemId!=null){
				baEmailPdf = OrderDAO.getOrderItemEmailPdf(sOrderItemId,sEmail,sStatus);
			}
			if(baEmailPdf!=null){
				ServletOutputStream stream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(baEmailPdf.length);
				response.setHeader("Content-Disposition","Email" + ";filename=" + sOrderItemId);
				stream.write(baEmailPdf);
				stream.flush();
				stream.close();
			}

			logger.info("OrderAction::viewEmailPdf:EXIT");
		}catch(Exception e){		
			logger.error("OrderAction::viewEmailPdf:EXCEPTION "+e.getMessage());
			return mapping.findForward("failure");		
		}	
		return null;
	}


	public ActionForward viewCancelOrderItem(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::viewCancelOrderItem:ENTRY");

		String sForwardKey="CancelOrderItemSuccess";
		String sOrderItemId = null;
		String sOrderItemStatus = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		String sCateFolderName = "";
		List<CategoryVO> alCategory = null;
		String sImageViewPath = "";
		HttpSession oSession = request.getSession();

		try{
			sOrderItemId = request.getParameter("OrderItemId");
			sOrderItemStatus = OrderDAO.getOrderItemStatus(sOrderItemId);

			if(!ORDER_COMPLETED_STATUS.equalsIgnoreCase(sOrderItemStatus)) {
				request.setAttribute("AlreadyProcessed", ORDER_ALREADY_CANCELED);
				oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())) {
						if(alCategory!=null && alCategory.size()>0) {
							for(CategoryVO oCategoryVO: alCategory) {
								if(oCategoryVO.getCateId()!=null) {
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
				sForwardKey = "AdminViewItemDetailsSuccess";
			}

			request.setAttribute("orderItemId", sOrderItemId);
			saveToken(request);
		}catch(Exception e) {
			logger.debug("OrderAction::viewCancelOrderItem:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::viewCancelOrderItem:EXIT");
		return mapping.findForward(sForwardKey);
	}


	//To Cancel the completed order and refund the amount.
	public ActionForward cancelOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		logger.info("OrderAction::cancelOrder:ENTRY");

		String sForwardKey="adminProcessPaymentSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null,sOrderItemId=null,sCancelReason=null,sFilePath = null,sPayTxnDetails = null,sCateFolderName = "",sImageViewPath = "";
		String sMsg = null;
		String sCCNo = null;
		LoginVO oLoginVO =null;
		PaymentResponse oPaymentResponse =null;
		List<OrderItemDetailsVO> alOrderItemDetails;
		List<OrderItemDetailsVO> alOrderItemStatus = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemStatus;
		List<CategoryVO> alCategory;
		
		try {

			sOrderItemId=request.getParameter("orderItemId");
			sCancelReason = request.getParameter("cancelReason");

			sFilePath = getServlet().getServletContext().getRealPath("/");
			sFilePath = sFilePath == null?"":sFilePath.trim();
			
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}

			alOrderItemDetails = getVendorOrderItemDetails(sOrderItemId);
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();

			sPayTxnDetails = getPayTxnRefId(sOrderItemId);
			if(isTokenValid(request)) {
				if(alOrderItemDetails != null && alOrderItemDetails.size() > 0) {

					for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetails)  {

						if(oOrderItemDetailsVO != null) {
							
							if(oOrderItemDetailsVO.getCCNo() != null && oOrderItemDetailsVO.getCCNo().trim().length() > 25 ) {
								sCCNo=decryptInputData(oOrderItemDetailsVO.getCCNo(),"CLIENT",oOrderItemDetailsVO.getKeyRefId());
							}else {
								sCCNo = oOrderItemDetailsVO.getCCNo();
							}
							sCCNo=sCCNo==null?"":sCCNo.trim();						
							oOrderItemDetailsVO.setCCNo(sCCNo);	

							if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
								if(alCategory!=null && alCategory.size()>0){
									for(CategoryVO oCategoryVO: alCategory){
										if(oCategoryVO.getCateId()!=null){
											if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
												sCateFolderName = oCategoryVO.getCateName();
										}					
									}
								}
							}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
								sCateFolderName = AIRLINE_CATAGEORY_NAME;
							}
							oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"thumb."+oOrderItemDetailsVO.getMainImgType());
						}
						oOrderItemDetailsVO.setPaymentTxnRefId(sPayTxnDetails);

						if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
							oPaymentResponse = PaymentDAO.creditAuthorizationToRepay(oOrderItemDetailsVO,"CANCEL",oLoginVO.getUserId(),request.getRemoteHost());
							if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() > 0) {

								if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){			

									OrderDAO.createAndSendCancelledPO(oOrderItemDetailsVO,sCancelReason,sFilePath,oLoginVO.getUserId(),"");
									OrderDAO.createAndSendCancelledInvoice(oOrderItemDetailsVO,sCancelReason,sFilePath,oLoginVO.getUserId(),"");
									if(!oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
										OrderDAO.sendCanceledAirlineCommissionEMail(oOrderItemDetailsVO,sCancelReason, sFilePath ,oLoginVO.getUserId(),"");
									}
									OrderDAO.updateCancelReason(oOrderItemDetailsVO,sCancelReason,oLoginVO.getUserId());

									/**** Insert Order audit details ******/							
									sMsg=oBundle.getString("customer.order.cancel.success.msg");
									sMsg=sMsg==null?"":sMsg.trim();								
									insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								

								}else{

									/***** Refund Failure mail have to go *****/
									sendOrderCancelFailureEmail(oOrderItemDetailsVO,null,oLoginVO.getUserId(),"");

									/**** Insert Order audit details ******/							
									sMsg=oBundle.getString("customer.order.cancel.failure.msg");
									sMsg=sMsg==null?"":sMsg.trim();								
									insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");								

								}
								oSession.removeAttribute("OrderProcessed");
							}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == -1) {
								oSession.setAttribute("OrderProcessed", "Cancelling the order is in process");
							}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == -2) {
								oSession.setAttribute("OrderProcessed", "Cancelling the order is already performed");
							}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == 0) {
								oSession.setAttribute("OrderProcessed", "Cancelling the order process is unsuccessful");
							}
							oOrderItemStatus = OrderDAO.getOrderItemDetailStatus(sOrderItemId);
							if(ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemStatus.getOrderItemStatus())) {
								oOrderItemStatus.setReasonForFailure(oPaymentResponse.getR_Error_Msg());
								alOrderItemStatus.add(oOrderItemStatus);
							}else {
								oOrderItemStatus.setReasonForFailure("");
								alOrderItemStatus.add(oOrderItemStatus);
							}
							if(!PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())) {
								oOrderItemStatus.setReasonForFailure(oPaymentResponse.getR_Error_Msg());
							}
						}else {
							oOrderItemStatus = OrderDAO.getOrderItemDetailStatus(sOrderItemId);
							alOrderItemStatus.add(oOrderItemStatus);
							request.setAttribute("OrderProcessed", "Cancelling the order is already performed");
						}
					}
				}
				oSession.setAttribute("ProcessPaymentStatusList", alOrderItemStatus);
			}

			resetToken(request);

		}catch(Exception e) {
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::cancelOrder:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::cancelOrder:EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward initOrderReturn(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initOrderReturn::ENTRY");

		String sForwardKey="AdminInitOrderReturnSuccess";
		String sURI = null;
		String sPath = null;
		String sOwnerType = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;

		sURI=request.getRequestURI();
		sPath=Utils.getUserTypeFromURI(sURI);
		try{
			if(AIRLINE.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(ADMIN.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(VENDOR.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
			}	
			if(VENDOR.equalsIgnoreCase(sOwnerType)){
				sForwardKey="VendorInitOrderReturnSuccess";  
			}
			else if(AIRLINE.equalsIgnoreCase(sOwnerType)){
				sForwardKey="AirlineInitOrderReturnSuccess";  
			}
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("orderReturnForm");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oOrderReturnForm.setOrderToDate(sCurrentDate);
		}catch(Exception e) {
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initOrderReturn:EXCEPTION "+e.getMessage());
		}
		logger.info("OrderAction::initOrderReturn::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward initSearchOrderReturn(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initOrderReturn::ENTRY");

		String sForwardKey="VendorInitSearchOrderReturnSuccess";
		String sURI = null;
		String sPath = null;
		String sOwnerType = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);

			if(AIRLINE.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
			}	
			if(AIRLINE.equalsIgnoreCase(sOwnerType)){
				sForwardKey="AirlineInitSearchOrderReturnSuccess";  
			}
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("orderReturnForm");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oOrderReturnForm.setOrderToDate(sCurrentDate);
		}catch(Exception e) {
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initOrderReturn:EXCEPTION "+e.getMessage());
		}
		logger.info("OrderAction::initOrderReturn::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward getOrderToReturn(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::OrderReturn::ENTRY");

		String sForwardKey = "AdminInitOrderReturnSuccess";
		String sURI = null;
		String sPath = null;
		String sOwnerId = null;
		String sOwnerType = null;
		List<OrderReturnDetailsVO> alOrderItemDetails = new ArrayList<OrderReturnDetailsVO>();
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		OrderReturnForm oOrderReturnForm = null;
		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);

			if(AIRLINE.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(ADMIN.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(VENDOR.equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerId = oLoginVO.getRefId();
			}

			if(VENDOR.equalsIgnoreCase(sOwnerType)){
				sForwardKey="VendorInitOrderReturnSuccess";  
			}else if(AIRLINE.equalsIgnoreCase(sOwnerType)){
				sForwardKey="AirlineInitOrderReturnSuccess";  
			}

			oOrderReturnForm = (OrderReturnForm)form;
			oOrderReturnForm.setCategory(oOrderReturnForm.getCategory()==null?"":oOrderReturnForm.getCategory().trim());
			oOrderReturnForm.setOrderFromDate(oOrderReturnForm.getOrderFromDate()==null?"":oOrderReturnForm.getOrderFromDate().trim());
			oOrderReturnForm.setOrderToDate(oOrderReturnForm.getOrderToDate()==null?"":oOrderReturnForm.getOrderToDate().trim());
			oOrderReturnForm.setSearchBy(oOrderReturnForm.getSearchBy()==null?"":oOrderReturnForm.getSearchBy().trim());
			oOrderReturnForm.setSearchValue(oOrderReturnForm.getSearchValue()==null?"":oOrderReturnForm.getSearchValue().trim());
			oOrderReturnForm.setOrderStatus(oOrderReturnForm.getOrderStatus()==null?"":oOrderReturnForm.getOrderStatus().trim());


			alOrderItemDetails =  getSearchOrderToReturn(oOrderReturnForm,sOwnerType,sOwnerId);
			if(alOrderItemDetails!=null && alOrderItemDetails.size()>0) {
				request.setAttribute("ReturnOrderItems", alOrderItemDetails);
			}else{
				request.setAttribute(NO_RECORDS_KEY,NO_RECORDS);
			}
		}catch(Exception exception) {
			sForwardKey = "failure";
			logger.debug("OrderAction::OrderReturn::EXCEPTION"+exception.getMessage());
			exception.printStackTrace();
		}

		logger.info("OrderAction::OrderReturn::EXIT");

		return mapping.findForward(sForwardKey);
	}

	public ActionForward updateRMAStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.debug("OrderAction::updateRMAStatus::EXIT");
		String sForwardKey = "VendorOrderReturnInfoSuccess";
		String sURI = null;
		String sPath = null;
		String sOwnerName = null;
		String sOwnerType = null;
		String sImageViewPath = null;
		String sCateFolderName = null;
		int iIsUpdate = 0;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		List<CategoryVO> alCategory = null;
		List<OrderItemDetailsVO> alOrderItemDetailsVO = null;
		OrderReturnDetailsVO oOrderReturnDetailsVO = null;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		OrderItemDetailsVO oOrderItemDetails = null;
		sURI=request.getRequestURI();
		sPath=Utils.getUserTypeFromURI(sURI);

		if(AIRLINE.equalsIgnoreCase(sPath))
			oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
		else if(VENDOR.equalsIgnoreCase(sPath))
			oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

		if(oLoginVO!= null){
			sOwnerType = oLoginVO.getUserType();
			sOwnerName = oLoginVO.getUserName();
		}

		if(AIRLINE.equalsIgnoreCase(sOwnerType)){
			sForwardKey="AirlineOrderReturnInfoSuccess";  
		}
		try {
			oOrderReturnDetailsVO = updateRMANumberStatus(oOrderReturnForm,sOwnerName,sOwnerType);
			oOrderItemDetails = getReturnOrderItemDetails(oOrderReturnForm.getRmaNumber());
			iIsUpdate = oOrderReturnDetailsVO.getRowCount();
			if(iIsUpdate == 5) {
				request.setAttribute("updated", "Successfully processed.");
				//if("WA".equalsIgnoreCase(oOrderReturnForm.getReturnStatus())) {

				sendRMAApprovedEmailToAdmin(oOrderItemDetails, oOrderReturnForm.getReturnComments(),oLoginVO.getUserId(),"");
				sendRMAApprovedEmailToVendor(oOrderItemDetails, oOrderReturnForm.getReturnComments(),oLoginVO.getUserId(),"");
				sendRMAApprovedEmailToCustomer(oOrderItemDetails, oOrderReturnForm.getReturnComments(), oOrderReturnForm.getReturnQuantity(),oLoginVO.getUserId(),"");
				/*}else { 
					sendRMARejectedEmailToAdmin(oOrderReturnDetailsVO.getOrderItemId(), oOrderReturnForm.getReturnComments());
					sendRMARejectedEmailToVendor(oOrderReturnDetailsVO.getOrderItemId(), oOrderReturnForm.getReturnComments());
				}*/
			}else if(iIsUpdate == 4) {
				request.setAttribute("updated", "Rejected the returned RMA.");
				sendRMARejectedEmailToAdmin(oOrderItemDetails, oOrderReturnForm.getReturnComments(),oLoginVO.getUserId(),"");
				sendRMARejectedEmailToVendor(oOrderItemDetails, oOrderReturnForm.getReturnComments(),oLoginVO.getUserId(),"");
				sendRMARejectedEmailToCustomer(oOrderItemDetails, oOrderReturnForm.getReturnComments(),oLoginVO.getUserId(),"");

			}else if(iIsUpdate == 3) {
				request.setAttribute("updated", "Enter less than or equal to customer returned.");
				if(AIRLINE.equalsIgnoreCase(sOwnerType)){
					sForwardKey="AirlineOrderReturnInfoFailure";  
				}else if(VENDOR.equalsIgnoreCase(sOwnerType)) {
					sForwardKey="VendorOrderReturnInfoFailure";  
				}
			}else if(iIsUpdate == 2) {
				request.setAttribute("updated", "This Order is already processed.");
			}else if(iIsUpdate == 1) {
				request.setAttribute("updated", "This Order is not belong to you.");
				if(AIRLINE.equalsIgnoreCase(sOwnerType)){
					sForwardKey="AirlineOrderReturnInfoFailure";  
				}else if(VENDOR.equalsIgnoreCase(sOwnerType)){
					sForwardKey="VendorOrderReturnInfoFailure";  
				}
			}else if(iIsUpdate == 0) {
				request.setAttribute("updated", "Invalid RMA Number.");
				if(AIRLINE.equalsIgnoreCase(sOwnerType)){
					sForwardKey="AirlineOrderReturnInfoFailure";  
				}else if(VENDOR.equalsIgnoreCase(sOwnerType)){
					sForwardKey="VendorOrderReturnInfoFailure";  
				}
			}

			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();

			if(oOrderItemDetails != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetails.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetails.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(oOrderItemDetails.getOwnerType().equalsIgnoreCase(AIRLINE)) {
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
				oOrderItemDetails.setMainImgPath(sImageViewPath+oOrderItemDetails.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetails.getOwnerId()+oOrderItemDetails.getProdId()+"med."+oOrderItemDetails.getMainImgType());
				oOrderItemDetails.setQty(oOrderReturnForm.getReturnQuantity());
			}

			request.setAttribute("ReturnOrderItemDetails", oOrderItemDetails);

		}catch(Exception exception) {
			sForwardKey="failure";
			exception.printStackTrace();
			logger.debug("OrderAction::updateRMAStatus::EXCEPTION"+exception.getMessage());
		}

		logger.debug("OrderAction::updateRMAStatus::EXIT");

		return mapping.findForward(sForwardKey);
	}

	public ActionForward viewChargeBackOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::viewChargeBackOrderDetails:ENTER");

		String sFrdKey="AdminViewOrderChargeBackSuccess";
		String sImageViewPath = null;
		String sCateFolderName = null;
		String sMode = null;
		String sReturnId = null;
		HttpSession oSession = request.getSession();
		List<CategoryVO> alCategory = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;	
		try{
			sReturnId=request.getParameter("ReturnId");
			sMode = request.getParameter("Mode"); 
			if(sReturnId != null){
				oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				saveToken(request);
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
				request.setAttribute("Mode", sMode);
			}
			logger.info("OrderAction::viewChargeBackOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::viewChargeBackOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}

	public ActionForward displayCreditChargeBack(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::displayCreditChargeBack:ENTER");

		String sFrdKey="AdminDisplayChargeBackSuccess";
		
		try{
			String sReturnId=request.getParameter("ReturnId");
			if(sReturnId != null){/*
				oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
			 */}
			logger.info("OrderAction::displayCreditChargeBack:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::displayCreditChargeBack:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}

	public ActionForward creditChargeBack(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		logger.info("OrderAction::creditChargeBack::ENTRY");
		String sForwardKey = "AdminChargeBackSuccess";
		String sMsg;
		String sURI;
		String sPath;
		String sReturnId = null;
		String sPayTxnDetails;
		String sImageViewPath = null;
		String sCateFolderName = null;
		List<CategoryVO> alCategory = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		PaymentResponse oPaymentResponse = null;

		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		sURI=request.getRequestURI();
		sPath=Utils.getUserTypeFromURI(sURI);
		if(ADMIN.equalsIgnoreCase(sPath)) {
			oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
		}
		sReturnId = request.getParameter("ReturnId");
		try{
			oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);
			sPayTxnDetails = getPayTxnRefId(oOrderItemDetailsVO.getOrderItemId());
			oOrderItemDetailsVO.setPaymentTxnRefId(sPayTxnDetails);
			oOrderItemDetailsVO.setChargeBackPrice(oOrderItemDetailsVO.getSbhPrice());
			oOrderItemDetailsVO.setAdminComments(oOrderReturnForm.getAdminComments());
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
			}
			if(isTokenValid(request)) {
				if("WA".equalsIgnoreCase(oOrderItemDetailsVO.getRmaStatus())) {
					oPaymentResponse = creditAuthorizationToRepay(oOrderItemDetailsVO,"CHARGEBACK",oLoginVO.getUserId(),request.getRemoteHost());
					if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() > 0) {
						if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())) {

							//Update the Return Status.
							updateOrderItemStatusAndReturnStatus(oOrderItemDetailsVO.getReturnId(),"A","RA",oOrderReturnForm.getChargeBackAmount(),oOrderReturnForm.getAdminComments(),oLoginVO.getUserId());
							oOrderItemDetailsVO.setRmaStatus("RA");
							//Send Email to Customer and Vendor for Credit Charge back success.
							sendOrderChargeBackSuccessEmailToCustomer(oOrderItemDetailsVO,oLoginVO.getUserId(),"");
							sendOrderChargeBackSuccessEmailToVendor(oOrderItemDetailsVO,oLoginVO.getUserId(),"");
							request.setAttribute("ProcessStatus", "Charge back process is successful");
						}else{
							sMsg=oBundle.getString("preauth.ccno.error.msg");
							sMsg=sMsg==null?"":sMsg.trim();		
							oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);
							oOrderItemDetailsVO.setCCNoComments(sMsg);
							sendChargeBackFailureEmail(oOrderItemDetailsVO,sMsg,oLoginVO.getUserId(),"");

							/**** Insert Order audit details ******/							
							insertOrderAuditDetails(oOrderItemDetailsVO.getOrderItemId(),oOrderItemDetailsVO.getOrderId(),oOrderItemDetailsVO.getCustTransId(),sMsg,oLoginVO.getUserId(),null,"","");
							request.setAttribute("ProcessStatus", "Charge back process is unsuccessful");
						}
						oSession.removeAttribute("ProcessStatus");
					}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == -1) {
						oSession.setAttribute("ProcessStatus", "Credit Charge back is in process");
					}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == -2) {
						oSession.setAttribute("ProcessStatus", "Credit Charge back is already performed");
					}else if(oPaymentResponse != null && oPaymentResponse.getR_TxnRefId() == 0) {
						oSession.setAttribute("ProcessStatus", "Charge back process is unsuccessful");
					}
					oSession.setAttribute("ViewReturnOrderDetail", oOrderItemDetailsVO);
				}else {
					oSession.setAttribute("ProcessStatus", "Credit Charge back is already performed");
				}
			}
		}catch (Exception exception) {
			logger.debug("OrderAction::creditChargeBack::EXCEPTION"+exception.getMessage());
			exception.printStackTrace();
			sForwardKey = "failure";
		}

		resetToken(request);
		logger.info("OrderAction::creditChargeBack::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward editChargeBackReject(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::editChargeBackReject::ENTRY");

		String sForwardKey = "AdminEditOrderRejectSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		String sOrderItemId=request.getParameter("OrderItemId");
		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}
			request.setAttribute("OrderItemId", sOrderItemId);
		}catch(Exception e) {
			sForwardKey="failure";
			logger.info("OrderAction::editChargeBackReject::EXCEPTION"+e.getMessage());
		}
		logger.info("OrderAction::editChargeBackReject::EXIT");
		return mapping.findForward(sForwardKey);
	}
	public ActionForward creditRejected(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		logger.info("OrderAction::creditRejected::ENTRY");
		
		String sForwardKey = "AdminOrderRejectSuccess";
		HttpSession oSession = request.getSession();
		String sURI=null,sPath=null;
		LoginVO oLoginVO =null;
		String sOrderItemId=request.getParameter("OrderItemId");
		String sRejectReason;
		String sOwnerType;
		String sOwnerId;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		oOrderReturnForm.setOrderItemId(sOrderItemId);
		List<OrderItemDetailsVO> alOrderItemDetails = new ArrayList<OrderItemDetailsVO>();

		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			}
			sOwnerId=oLoginVO.getRefId();
			sOwnerType=oLoginVO.getUserId();
			sOwnerId=sOwnerId==null?"":sOwnerId.trim();
			sOwnerType=sOwnerType==null?"":sOwnerType.trim();

			//Status and reject reason has to be updated.
			alOrderItemDetails = getVendorOrderItemDetails(sOrderItemId);
			if(alOrderItemDetails != null) {
				for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetails) {
					/* TODO Change the OrderItemStatus from single character to two characters.
					 * 
					 * As of Now the 
					 * RMA Approved Status as A
					 * RMA Rejected Status as E
					 */
					updateOrderItemStatusAndReturnStatus(oOrderItemDetailsVO.getOrderItemId(),"E","RR",null,oOrderReturnForm.getAdminComments(),oLoginVO.getUserId());
					sRejectReason = getRejectReason(sOrderItemId);
					sendOrderChargeBackRejectEmail(oOrderItemDetailsVO,sRejectReason,oLoginVO.getUserId(),"");
				}
			}
		}catch(Exception e) {		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::creditRejected:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::creditRejected::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward viewOrderReturnInfo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		logger.info("OrderAction::viewOrderReturnInfo::ENTRY");

		String sForwardKey = "VendorOrderReturnInfoSuccess";
		String sURI=null,sPath=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO =null;
		String sOrderItemId=request.getParameter("OrderItemId");
		String sOwnerType=null;
		String sOwnerId;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		oOrderReturnForm.setOrderItemId(sOrderItemId);
		OrderItemDetailsVO oOrderItemDetails = new OrderItemDetailsVO();

		try {
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			if(AIRLINE.equalsIgnoreCase(sPath)) {
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			}
			if(oLoginVO != null){
				sOwnerType = oLoginVO.getUserType();
			}
			oOrderItemDetails = getReturnOrderItemDetails(oOrderReturnForm.getRmaNumber());
			oOrderItemDetails.setRmaGeneratedNo(oOrderReturnForm.getRmaNumber());
			request.setAttribute("ReturnOrderItemDetails", oOrderItemDetails);

			if(AIRLINE.equalsIgnoreCase(sOwnerType)){
				sForwardKey = "AirlineOrderReturnInfoSuccess"; 
			}
			sOwnerId=oLoginVO.getRefId();
			sOwnerType=oLoginVO.getUserId();
			sOwnerId=sOwnerId==null?"":sOwnerId.trim();
			sOwnerType=sOwnerType==null?"":sOwnerType.trim();
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("OrderAction::viewOrderReturnInfo:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::viewOrderReturnInfo::EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward viewReturnOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::viewReturnOrderDetails:ENTER");
		String sFrdKey="AdminViewItemDetailsSuccess";
		String sURI = null;
		String sPath = null;
		String sOwnerType = null;
		LoginVO oLoginVO = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		OrderItemForm oOrderItemForm = (OrderItemForm)form;
		ArrayList<CategoryVO> alCategory =null;
		HttpSession oSession = request.getSession();
		String sCateFolderName = "",sImageViewPath = "";
		try{
			String sReturnId=request.getParameter("ReturnId");
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}		
			if(sOwnerType.equalsIgnoreCase(VENDOR)){
				sFrdKey="VendorViewItemDetailsSuccess";  
			}
			else if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineViewItemDetailsSuccess";  
			}
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			sImageViewPath = sImageViewPath==null?"":sImageViewPath.trim();
			if(sReturnId != null){
				oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);

				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(AIRLINE.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
				}

				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO);
				request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
			}
			logger.info("OrderAction::viewReturnOrderDetails:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::viewReturnOrderDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}

	public ActionForward editReturnDetails (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::editReturnDetails:ENTER");

		String sFrdKey="AdminEditReturnDetailSuccess";
		String sImageViewPath = null;
		String sCateFolderName = null;
		HttpSession oSession = request.getSession();
		List<CategoryVO> alCategory = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;	
		try{
			String sReturnId=request.getParameter("ReturnId");
			if(sReturnId != null){
				oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				request.setAttribute("EditReturnDetails", oOrderItemDetailsVO);
			}

		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::editReturnDetails:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::editReturnDetails:EXIT");

		return mapping.findForward(sFrdKey);
	}

	public ActionForward updateReturnDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::updateReturnDetails:ENTER");

		String sFrdKey="AdminEditReturnDetailSuccess";
		String sImageViewPath = null;
		String sCateFolderName = null;
		HttpSession oSession = request.getSession();
		List<CategoryVO> alCategory = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		OrderReturnForm oOrderReturnForm = (OrderReturnForm)form;
		try{

			LoginVO oLoginVO = null;
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);	
			String sReturnId=request.getParameter("ReturnId");
			if(sReturnId != null) {
				updateReturnDetail(oOrderReturnForm,sReturnId,oLoginVO.getUserId());
				oOrderItemDetailsVO = getReturnOrderItemDetail(sReturnId);
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				sImageViewPath = System.getProperty("ImageViewPath").trim();

				if(oOrderItemDetailsVO != null) {
					if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
						if(alCategory!=null && alCategory.size()>0){
							for(CategoryVO oCategoryVO: alCategory){
								if(oCategoryVO.getCateId()!=null){
									if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
										sCateFolderName = oCategoryVO.getCateName();
								}					
							}
						}
					}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
						sCateFolderName = AIRLINE_CATAGEORY_NAME;
					}
					oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
				}
				sendUpdateReturnOrderDetail(oOrderItemDetailsVO,oLoginVO.getUserId(),"");
				request.setAttribute("ViewReturnOrderDetail", oOrderItemDetailsVO);
			}

		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::updateReturnDetails:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::updateReturnDetails:EXIT");

		return mapping.findForward(sFrdKey);
	}

	public ActionForward initSearchAirlineAdvt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initSearchAirlineAdvt:ENTER");
		
		String sFwrdKey="AdminSearchAirlineAdvtSuccess",sURI=null,sPath=null;    	
		HttpSession oSession = request.getSession();
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		LoginVO oLoginVO =null;
		String sOwnerType =null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	

			if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFwrdKey="AirlineSearchAirlineAdvtSuccess";  
			}

			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchOrderForm");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);     
			oSearchOrderForm.setOrderToDate(sCurrentDate);

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::initSearchAirlineAdvt:Exception "+e.getMessage());
		}
		logger.info("OrderAction::initSearchAirlineAdvt:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward searchAirlineAdvt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::searchAirlineAdvt:ENTER");
		
		String sFrdKey="AdminSearchAirlineAdvtSuccess";
		ArrayList alAirlineAdvt = null;	
		SearchOrderForm oSearchOrderForm=(SearchOrderForm)form; 	
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sSearchBy=null,sSearchValue = null, sOrderFromDate = null,sOrderToDate=null,sOwner=null,sSearchId=null;
		String sOwnerId=null,sOwnerType=null,sURI=null,sPath=null;
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			if(oLoginVO!= null){
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId ==null?"":sOwnerId.trim();
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			String sOrderStatus =  request.getParameter("OrderStatus");
			sOrderStatus = sOrderStatus == null?"":sOrderStatus.trim();

			String sItemCode =  request.getParameter("ItemCode");
			sItemCode = sItemCode == null?"":sItemCode.trim();

			if(oSearchOrderForm!=null){	
				sSearchBy=oSearchOrderForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchOrderForm.setSearchBy(sSearchBy);

				sSearchValue=oSearchOrderForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchOrderForm.setSearchValue(sSearchValue);

				sOrderFromDate = oSearchOrderForm.getOrderFromDate();
				sOrderFromDate=sOrderFromDate==null?"":sOrderFromDate.trim();
				oSearchOrderForm.setOrderFromDate(sOrderFromDate);	

				sOrderToDate = oSearchOrderForm.getOrderToDate();
				sOrderToDate=sOrderToDate==null?"":sOrderToDate.trim();
				oSearchOrderForm.setOrderToDate(sOrderToDate);	

			}
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();


			if(sOwnerType.equalsIgnoreCase(AIRLINE)){
				sFrdKey="AirlineSearchAirlineAdvtSuccess";  
			}

			alAirlineAdvt =  OrderDAO.getAirlineAdvertisement(oSearchOrderForm,sOwnerType,sOwnerId);
			if(alAirlineAdvt!=null && alAirlineAdvt.size()>0){
				request.setAttribute("airlineAdvtInfo", alAirlineAdvt);
			}else{
				request.setAttribute(NO_RECORDS_KEY,NO_RECORDS);
			} 			

			logger.info("OrderAction::searchAirlineAdvt:EXIT");
		}catch(Exception e){		
			sFrdKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::searchAirlineAdvt:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward viewCompletedOrderDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::viewCompletedOrderDetails:ENTER");
		String sForwardKey = "AdminViewCompletedOrderSuccess";

		HttpSession oSession = request.getSession();
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		String sOrderItemId=request.getParameter("OrderItemId");
		OrderItemForm oOrderItemForm = (OrderItemForm)form;
		String sCateFolderName = "";
		List<CategoryVO> alCategory = null;
		String sImageViewPath = "";
		try {
			request.setAttribute("OrderItemId", sOrderItemId);
			oOrderItemDetailsVO = getCustOrderItemDetails(sOrderItemId);
			if(oOrderItemDetailsVO != null && !ORDER_COMPLETED_STATUS.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemStatus())) {
				request.setAttribute("AlreadyProcessed", ORDER_ALREADY_PROCESSED);
			}
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();

			if(oOrderItemDetailsVO != null) {
				if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oOrderItemDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
					sCateFolderName = AIRLINE_CATAGEORY_NAME;
				}
				oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"med."+oOrderItemDetailsVO.getMainImgType());
			}
			BeanUtils.copyProperties(oOrderItemForm,oOrderItemDetailsVO); 
			request.setAttribute("custOrderItemDetails", oOrderItemDetailsVO);
		}catch(Exception e){		
			sForwardKey="failure";
			e.printStackTrace();
			logger.error("OrderAction::viewCompletedOrderDetails:EXCEPTION "+e.getMessage());
		}

		logger.info("OrderAction::viewCompletedOrderDetails:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward renderImage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("OrderAction::renderImage:ENTER");
		byte[] baOrderItemImage=null;
		String sOrderItemId = null;
		String sEmail = null,sStatus = null;;
		try{
			sOrderItemId=request.getParameter("orderItemId");
			if(sOrderItemId!=null){
				baOrderItemImage = OrderDAO.getOrderItemImage(sOrderItemId);
			}
			if(baOrderItemImage!=null){
				ServletOutputStream stream = response.getOutputStream();
				response.setContentType("image/jpeg");
				response.setContentLength(baOrderItemImage.length);
				response.setHeader("Content-Disposition","Email" + ";filename=" + sOrderItemId);
				stream.write(baOrderItemImage);
				stream.flush();
				stream.close();

			}


			logger.info("OrderAction::renderImage:EXIT");
		}catch(Exception e){		
			logger.error("OrderAction::renderImage:EXCEPTION "+e.getMessage());
			return mapping.findForward("failure");		

		}	
		return null;
	}

	public ActionForward initOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initOrderReport:ENTRY");
		String sForwardKey="searchOrderReportSuccess";
		String sServerType = null,sOrderReportFileName = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

		try{
			HttpSession oSession = request.getSession(true);

			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("orderReportForm");

			oSession.removeAttribute("birtSearchBy");
			oSession.removeAttribute("birtSearchValue");
			oSession.removeAttribute("birtMonth");
			oSession.removeAttribute("birtYear");
			oSession.removeAttribute("birtOrderStartDate");
			oSession.removeAttribute("birtOrderEndDate");
			oSession.removeAttribute("birtLoginUser");
			oSession.removeAttribute("birtLoginUserId");
			oSession.removeAttribute("OrderReportDetails");
			DynaActionForm oForm = (DynaActionForm)form;
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH)+1;
			int year = cal.get(Calendar.YEAR);
			if(month == 12){
				month = 1;
				year = year - 1;
			}else{
				month = month - 1;
			}
			String sMonth = month+"";
			if(sMonth.length() < 2){
				sMonth = "0"+sMonth;
			}

			oForm.set("month", sMonth);
			oForm.set("year", year+"");

			/* To choose the .rptdesign file */
			sServerType = System.getProperty("ServerType");
			sServerType = sServerType == null?"":sServerType.trim();

			if("PRODUCTION".equalsIgnoreCase(sServerType)){
				sOrderReportFileName = oBundle.getString("OrderReportProdFile");
			}else{
				sOrderReportFileName = oBundle.getString("OrderReportFile");
			}
			oSession.setAttribute("ServerType", sServerType);
			oSession.setAttribute("OrderReportFileName", sOrderReportFileName);

		}catch(Exception e) {
			logger.debug("OrderAction::initOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::initOrderReport:EXIT");
		return mapping.findForward(sForwardKey);

	}


	public ActionForward getOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getOrderReport:ENTRY");
		List<CustomerSuggestionsVO> alCustFeedback = null;
		boolean bIsExist = false; 
		HashMap hmAllData = null;
		LoginVO oLoginVO = null;
		HashMap hmMonthName = new HashMap();
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sSearchType = null,sSearchValue = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey="searchOrderReportSuccess";
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();

			DynaActionForm oForm = (DynaActionForm)form;

			sSearchType = oForm.getString("searchType");
			sSearchType = sSearchType==null?"":sSearchType.trim();

			sSearchValue = oForm.getString("searchValue");
			sSearchValue = sSearchValue==null?"":sSearchValue.trim();

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();



			bIsExist = OrderDAO.getOrderReport(sSearchType,sSearchValue, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				oSession.setAttribute("birtSearchBy", sSearchType);
				oSession.setAttribute("birtSearchValue", sSearchValue);
				oSession.setAttribute("birtMonth", sMonth);
				oSession.setAttribute("birtYear", sYear);
				oSession.setAttribute("birtOrderStartDate", sFromDate);
				oSession.setAttribute("birtOrderEndDate", sToDate);
				oSession.setAttribute("birtLoginUser", sLoginUser);
				oSession.setAttribute("birtLoginUserId", sUserId);
				oSession.setAttribute("OrderReportDetails", "show");
			}else{
				oSession.removeAttribute("birtSearchBy");
				oSession.removeAttribute("birtSearchValue");
				oSession.removeAttribute("birtMonth");
				oSession.removeAttribute("birtYear");
				oSession.removeAttribute("birtOrderStartDate");
				oSession.removeAttribute("birtOrderEndDate");
				oSession.removeAttribute("birtLoginUser");
				oSession.removeAttribute("birtLoginUserId");
				oSession.removeAttribute("OrderReportDetails");
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
			}
			/*if(hmAllData != null && hmAllData.size()>0) {
				String month = null;
				if(!sMonth.equalsIgnoreCase("")){
				String[] monthName = {"","January", "February",
			            "March", "April", "May", "June", "July",
			            "August", "September", "October", "November",
			            "December"};

			        Calendar cal = Calendar.getInstance();
			        month = monthName[Integer.parseInt(sMonth)];

			        oSession.setAttribute("MonthName", month);
					oSession.setAttribute("Year", sYear);

					oSession.removeAttribute("FromDate");
					oSession.removeAttribute("ToDate");

				}else{
					oSession.setAttribute("FromDate", sFromDate);
					oSession.setAttribute("ToDate", sToDate);

					oSession.removeAttribute("MonthName");
					oSession.removeAttribute("Year");
				}

				tsOrderDetails = (TreeSet)hmAllData.get("Report");
				dTotalOrderValue = (Double)hmAllData.get("TotalValue");
				oSession.setAttribute("OrderReportDetails", tsOrderDetails);
				oSession.setAttribute("TotalValue", dTotalOrderValue);

			}else{
				oSession.removeAttribute("MonthName");
				oSession.removeAttribute("Year");
				oSession.removeAttribute("FromDate");
				oSession.removeAttribute("ToDate");
				oSession.removeAttribute("OrderReportDetails");
				oSession.removeAttribute("TotalValue");
				request.setAttribute(NO_RECORDS_KEY, NO_RECORDS_FOUND);
			}*/
		}catch(Exception e) {
			logger.debug("OrderAction::getOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::getOrderReport:EXIT");
		return mapping.findForward(sForwardKey);

	}





	/*public ActionForward birtExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::birtExport:ENTRY");
		String sForwardKey = null;

		String sSearchType = null,sSearchValue = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null,sLoginUser = null, sLoginUserId = null;
		String sFilePath = getServlet().getServletContext().getRealPath("/");
		HttpSession oSession = request.getSession(true);
		IReportEngine birtReportEngine = null;
		ServletContext sc = request.getSession().getServletContext();
		birtReportEngine = BirtEngine.getBirtEngine(sc);	
		String reportName = "OrderReport";
		// setup image directory
		HTMLRenderContext renderContext = new HTMLRenderContext();
		HashMap contextMap = new HashMap();
		contextMap.put(EngineConstants.APPCONTEXT_HTML_RENDER_CONTEXT, renderContext);
		IReportRunnable design;
		String sReportFileName = null;
		try {
			sSearchType = (String)oSession.getAttribute("birtSearchBy");
			sSearchValue = (String)oSession.getAttribute("birtSearchValue");
			sMonth = (String)oSession.getAttribute("birtMonth");
			sYear = (String)oSession.getAttribute("birtYear");
			sFromDate = (String)oSession.getAttribute("birtOrderStartDate");
			sToDate = (String)oSession.getAttribute("birtOrderEndDate");
			sLoginUser = (String)oSession.getAttribute("birtLoginUser");
			sLoginUserId = (String)oSession.getAttribute("birtLoginUserId");


			sReportFileName  = (String)oSession.getAttribute("OrderReportFileName");
			sReportFileName = sReportFileName == null?"":sReportFileName.trim();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename="+reportName+".pdf"); // inline, attachment

			// Open report design

			design = birtReportEngine.openReportDesign(sFilePath+"/"+sReportFileName);
			// create task to run and render report
			IRunAndRenderTask task = null;
			if ( design != null ) {
				//Create task to run the report - use the task to execute and run the report,
				task = birtReportEngine.createRunAndRenderTask(design); 

				//Set Render context to handle url and image locataions
				contextMap.put(EngineConstants.APPCONTEXT_PDF_RENDER_CONTEXT, renderContext);
				task.setAppContext( contextMap );
			}	
			// set output options
			if(task!=null){			

				HTMLRenderOption options = new HTMLRenderOption();

				options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
				options.setOutputStream(response.getOutputStream());
				task.setRenderOption(options);

				task.setParameterValue("@spSearchBy",sSearchType);
				task.setParameterValue("@spSearchValue",sSearchValue );
				task.setParameterValue("@spMonth",sMonth);
				task.setParameterValue("@spYear",sYear);
				task.setParameterValue("@spStartDate",sFromDate);
				task.setParameterValue("@spEndDate",sToDate);
				task.setParameterValue("@spLoginUser",sLoginUser);
				task.setParameterValue("@spLoginUserId",sLoginUserId);

				// run report
				task.run();
				task.close();
			}    
			logger.info("OrderAction::birtExport:EXIT");
		} catch (Exception e) {
			logger.error("OrderAction::birtExport:EXCEPTION "+e.getMessage());
			sForwardKey="failure";
		}finally{
			BirtEngine.destroyBirtEngine();
			birtReportEngine.shutdown();
			birtReportEngine =null;
		}	

		logger.info("OrderAction::birtExport:EXIT");
		return null;

	}*/



	public ActionForward initAirlineCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initAirlineCommReport:ENTRY");
		String sForwardKey="searchAirlineCommReportSuccess";
		ArrayList alAirlineNames = null;
		String sServerType = null,sAirCommReportFileName = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

		try{
			HttpSession oSession = request.getSession(true);

			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("airCommReportForm");

			oSession.removeAttribute("birtAirId");
			oSession.removeAttribute("birtMonth");
			oSession.removeAttribute("birtYear");
			oSession.removeAttribute("birtOrderStartDate");
			oSession.removeAttribute("birtOrderEndDate");
			oSession.removeAttribute("birtLoginUser");
			oSession.removeAttribute("birtLoginUserId");
			oSession.removeAttribute("AirlineCommReportDetails");
			alAirlineNames = OrderDAO.getAllAirlineNames();
			if(alAirlineNames!=null && alAirlineNames.size()>0){
				oSession.setAttribute("AirlineNamesList", alAirlineNames);
			}

			DynaActionForm oForm = (DynaActionForm)form;
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH)+1;
			int year = cal.get(Calendar.YEAR);
			if(month == 12){
				month = 1;
				year = year - 1;
			}else{
				month = month - 1;
			}
			String sMonth = month+"";
			if(sMonth.length() < 2){
				sMonth = "0"+sMonth;
			}

			oForm.set("month", sMonth);
			oForm.set("year", year+"");


			/* To choose the .rptdesign file */
			sServerType = System.getProperty("ServerType");
			sServerType = sServerType == null?"":sServerType.trim();

			if("PRODUCTION".equalsIgnoreCase(sServerType)){
				sAirCommReportFileName = oBundle.getString("AirCommReportProdFile");
			}else{
				sAirCommReportFileName = oBundle.getString("AirCommReportFile");
			}
			oSession.setAttribute("ServerType", sServerType);
			oSession.setAttribute("AirCommReportFileName", sAirCommReportFileName);		
		}catch(Exception e) {
			logger.debug("OrderAction::initAirlineCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::initAirlineCommReport:EXIT");
		return mapping.findForward(sForwardKey);

	}


	public ActionForward getAirlineCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getAirlineCommReport:ENTRY");
		List<CustomerSuggestionsVO> alCustFeedback = null;
		TreeSet tsOrderDetails =  null;
		Double dTotalOrderValue;
		HashMap hmAllData = null;
		LoginVO oLoginVO = null;
		HashMap hmMonthName = new HashMap();
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sAirId = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey="searchAirlineCommReportSuccess";
		boolean bIsExist = false;
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();

			DynaActionForm oForm = (DynaActionForm)form;

			if(sLoginUser.equalsIgnoreCase(ADMIN)){
				sAirId = oForm.getString("airId");
				sAirId = sAirId==null?"":sAirId.trim();
			}else{
				sAirId = sUserId;
				sAirId = sAirId==null?"":sAirId.trim();
			}

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();



			bIsExist = OrderDAO.getAirlineCommReport(sAirId, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				oSession.setAttribute("birtAirId", sAirId);
				oSession.setAttribute("birtMonth", sMonth);
				oSession.setAttribute("birtYear", sYear);
				oSession.setAttribute("birtOrderStartDate", sFromDate);
				oSession.setAttribute("birtOrderEndDate", sToDate);
				oSession.setAttribute("birtLoginUser", oLoginVO.getUserId());
				oSession.setAttribute("birtLoginUserId", oLoginVO.getRefId());
				oSession.setAttribute("AirlineCommReportDetails", "show");
			}else{
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
				oSession.removeAttribute("birtAirId");
				oSession.removeAttribute("birtMonth");
				oSession.removeAttribute("birtYear");
				oSession.removeAttribute("birtOrderStartDate");
				oSession.removeAttribute("birtOrderEndDate");
				oSession.removeAttribute("birtLoginUser");
				oSession.removeAttribute("birtLoginUserId");
				oSession.removeAttribute("AirlineCommReportDetails");
			}

		}catch(Exception e) {
			logger.debug("OrderAction::getAirlineCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::getAirlineCommReport:EXIT");
		return mapping.findForward(sForwardKey);

	}





	/*public ActionForward AirlineCommExport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::AirlineCommExport:ENTRY");
		String sForwardKey = null;

		String sAirId = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null,sLoginUser = null, sLoginUserId = null;
		String sFilePath = getServlet().getServletContext().getRealPath("/");
		HttpSession oSession = request.getSession(true);
		IReportEngine birtReportEngine = null;
		ServletContext sc = request.getSession().getServletContext();
		birtReportEngine = BirtEngine.getBirtEngine(sc);	
		String reportName = "AirlineCommission",sReportFileName = null;
		// setup image directory
		HTMLRenderContext renderContext = new HTMLRenderContext();
		HashMap contextMap = new HashMap();
		contextMap.put(EngineConstants.APPCONTEXT_HTML_RENDER_CONTEXT, renderContext);
		IReportRunnable design;

		try {


			sAirId = (String)oSession.getAttribute("birtAirId");
			//sSearchValue = (String)oSession.getAttribute("birtSearchValue");
			sMonth = (String)oSession.getAttribute("birtMonth");
			sYear = (String)oSession.getAttribute("birtYear");
			sFromDate = (String)oSession.getAttribute("birtOrderStartDate");
			sToDate = (String)oSession.getAttribute("birtOrderEndDate");
			sLoginUser = (String)oSession.getAttribute("birtLoginUser");
			sLoginUserId = (String)oSession.getAttribute("birtLoginUserId");

			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename="+reportName+".pdf"); // inline, attachment

			sReportFileName = (String)oSession.getAttribute("AirCommReportFileName");
			// Open report design

			design = birtReportEngine.openReportDesign(sFilePath+"/"+sReportFileName);
			// create task to run and render report
			IRunAndRenderTask task = null;
			if ( design != null ) {
				//Create task to run the report - use the task to execute and run the report,
				task = birtReportEngine.createRunAndRenderTask(design); 

				//Set Render context to handle url and image locataions
				contextMap.put(EngineConstants.APPCONTEXT_PDF_RENDER_CONTEXT, renderContext);
				task.setAppContext( contextMap );
			}	
			// set output options
			if(task!=null){			

				HTMLRenderOption options = new HTMLRenderOption();

				options.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);
				options.setOutputStream(response.getOutputStream());
				task.setRenderOption(options);

				task.setParameterValue("@spAirId",sAirId);
				task.setParameterValue("@spMonth",sMonth);
				task.setParameterValue("@spYear",sYear);
				task.setParameterValue("@spStartDate",sFromDate);
				task.setParameterValue("@spEndDate",sToDate);
				task.setParameterValue("@spLoginUser",sLoginUser);
				task.setParameterValue("@spLoginUserId",sLoginUserId);

				// run report
				task.run();
				task.close();
			}    
			logger.info("OrderAction::AirlineCommExport:EXIT");
		} catch (Exception e) {
			logger.error("OrderAction::AirlineCommExport:EXCEPTION "+e.getMessage());
			sForwardKey="failure";
		}finally{
			BirtEngine.destroyBirtEngine();
			birtReportEngine.shutdown();
			birtReportEngine =null;
		}	

		logger.info("OrderAction::AirlineCommExport:EXIT");
		return null;

	}*/


	public ActionForward exportOrderReportPdf(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::exportOrderReportPdf:ENTRY");
		String sFilePath = getServlet().getServletContext().getRealPath("/");
		TreeSet<OrderReportVO> tsOrderDetails =  null;
		HashMap hmOrderDetails = new HashMap();
		HashMap hmOrderItemDetails = null;
		ArrayList<ItemDetailsVO> alOrderItems = null;
		Double dVendorAmount,dSkyBuyAmount,dVendorTotalAmount = 0.0,dSkyBuyTotalAmount = 0.0;
		int iTotalQty = 0;
		byte[] baPdf;
		File oPdfFile = null;
		File oHtmlFile = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		String sHeaderHtml = null;
		Double dTotalOrderValue = 0.0;
		String sHtml = null;
		String sOrderItemsTagReplacedHtml = "";
		String sOrderItemHtml =null;
		String sFullHtml = "";
		String sForwardKey="searchOrderReportSuccess";
		String sDateRange = null,sFromDate = null,sToDate = null,sMonth = null,sYear = null,sSkyBuyLogoPath = null;
		int iCount = 0;
		try{
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sHeaderHtml = oBundle.getString("HeaderHtml");
			sHeaderHtml = sHeaderHtml == null?"":sHeaderHtml.trim();

			sHtml = oBundle.getString("ReportHtml");
			sHtml = sHtml == null?"":sHtml.trim();

			sOrderItemHtml = oBundle.getString("OrderItemHtml");
			sOrderItemHtml = sOrderItemHtml == null?"":sOrderItemHtml.trim();

			HttpSession oSession = request.getSession(true);

			sFromDate = (String)oSession.getAttribute("FromDate");
			sToDate = (String)oSession.getAttribute("ToDate");

			sMonth = (String)oSession.getAttribute("MonthName");
			sYear = (String)oSession.getAttribute("Year");

			if(sFromDate!=null && sToDate!=null){
				sDateRange = "from "+ sFromDate +" to " + sToDate ;
			}else if(sMonth!=null && sYear!=null){
				sDateRange = "for the Month of "+ sMonth +" " + sYear ;
			}

			dTotalOrderValue = (Double)oSession.getAttribute("TotalValue");
			sHeaderHtml = OrderDAO.replaceTagValues(null,sHeaderHtml,"{{DateRange}}",sDateRange);

			sHeaderHtml = OrderDAO.replaceTagValues(null,sHeaderHtml,"{{TotalAmount}}",numberFormat.format(dTotalOrderValue));

			tsOrderDetails = (TreeSet)oSession.getAttribute("OrderReportDetails");
			for(OrderReportVO oOrderReportVO:tsOrderDetails){

				sFullHtml = sFullHtml+sHtml;
				hmOrderDetails.put("{{Customer_Name}}", oOrderReportVO.getCustBillFname()+' '+oOrderReportVO.getCustBillLname());
				hmOrderDetails.put("{{OrderConfNo}}",oOrderReportVO.getCustTransId());
				hmOrderDetails.put("{{OrderDate}}",oOrderReportVO.getOrderDt());
				hmOrderDetails.put("{{AirCharterName}}",oOrderReportVO.getAirName());
				hmOrderDetails.put("{{FlightNo}}",oOrderReportVO.getFlightNo());
				hmOrderDetails.put("{{ShippingCustomerName}}", oOrderReportVO.getCustShipFname()+' '+oOrderReportVO.getCustShipLname());
				hmOrderDetails.put("{{ShippingAddress}}",Utils.toTitleCase(Utils.mergeAddressLines(oOrderReportVO.getCustShipAddr1(), oOrderReportVO.getCustShipAddr2())));
				hmOrderDetails.put("{{ShippingCity}}",oOrderReportVO.getCustShipCity());
				hmOrderDetails.put("{{ShippingState}}",oOrderReportVO.getCustShipState());
				hmOrderDetails.put("{{ShippingZip}}",oOrderReportVO.getCustShipZip());
				hmOrderDetails.put("{{ShippingPhone}}",oOrderReportVO.getCustShipPhone());
				hmOrderDetails.put("{{ShippingMail}}",oOrderReportVO.getCustShipEmail());

				hmOrderDetails.put("{{BillingCustomerName}}", oOrderReportVO.getCustBillFname()+' '+oOrderReportVO.getCustBillLname());
				hmOrderDetails.put("{{BillingAddress}}",Utils.toTitleCase(Utils.mergeAddressLines(oOrderReportVO.getCustBillAddr1(), oOrderReportVO.getCustBillAddr2())));
				hmOrderDetails.put("{{BillingCity}}",oOrderReportVO.getCustBillCity());
				hmOrderDetails.put("{{BillingState}}",oOrderReportVO.getCustBillState());
				hmOrderDetails.put("{{BillingZip}}",oOrderReportVO.getCustBillZip());
				hmOrderDetails.put("{{BillingPhone}}",oOrderReportVO.getCustBillPhone());
				hmOrderDetails.put("{{BillingMail}}",oOrderReportVO.getCustBillEmail());


				if(iCount == 0){
					iCount++;
					hmOrderDetails.put("{{Header}}",sHeaderHtml);
					hmOrderDetails.put("{{Logo}}","<img width='267' height='100' src='"+sSkyBuyLogoPath+"' alt='SkyBuyHigh' />");
				}else{
					hmOrderDetails.put("{{Header}}","");
					hmOrderDetails.put("{{Logo}}","");
				}
				sFullHtml = OrderDAO.replaceTagValues(hmOrderDetails,sFullHtml,null,null);

				alOrderItems= oOrderReportVO.getItemDetails();

				iTotalQty = 0;
				dVendorTotalAmount = 0.0;
				dSkyBuyTotalAmount = 0.0;
				sOrderItemsTagReplacedHtml = "";

				for(ItemDetailsVO oItemDetailsVO:alOrderItems){
					sOrderItemsTagReplacedHtml += sOrderItemHtml;
					hmOrderItemDetails = new HashMap();
					hmOrderItemDetails.put("{{OrderItemId}}",oItemDetailsVO.getOrderItemId());
					hmOrderItemDetails.put("{{ProdCode}}",oItemDetailsVO.getProdCode());
					hmOrderItemDetails.put("{{ProdTitle}}",oItemDetailsVO.getProdTitle());
					hmOrderItemDetails.put("{{Qty}}",oItemDetailsVO.getQty());
					hmOrderItemDetails.put("{{RetailPrice}}",numberFormat.format(Double.parseDouble(oItemDetailsVO.getVendPrice())));

					dVendorAmount = Integer.parseInt(oItemDetailsVO.getQty())*Double.parseDouble(oItemDetailsVO.getVendPrice());
					hmOrderItemDetails.put("{{RetailAmount}}", numberFormat.format(dVendorAmount));

					hmOrderItemDetails.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oItemDetailsVO.getSbhPrice())));

					dSkyBuyAmount = Integer.parseInt(oItemDetailsVO.getQty())*Double.parseDouble(oItemDetailsVO.getSbhPrice());
					hmOrderItemDetails.put("{{SkyBuyAmount}}", numberFormat.format(dSkyBuyAmount));

					sOrderItemsTagReplacedHtml = OrderDAO.replaceTagValues(hmOrderItemDetails,sOrderItemsTagReplacedHtml,null,null);

					dVendorTotalAmount = dVendorTotalAmount + dVendorAmount;
					dSkyBuyTotalAmount = dSkyBuyTotalAmount + dSkyBuyAmount;
					iTotalQty += Integer.parseInt(oItemDetailsVO.getQty());
				}

				sFullHtml = OrderDAO.replaceTagValues(null,sFullHtml,"{{OrderItems}}",sOrderItemsTagReplacedHtml);

				sFullHtml = OrderDAO.replaceTagValues(null,sFullHtml,"{{TotalQty}}",iTotalQty+"");

				sFullHtml = OrderDAO.replaceTagValues(null,sFullHtml,"{{TotalRetailAmount}}",numberFormat.format(dVendorTotalAmount));

				sFullHtml = OrderDAO.replaceTagValues(null,sFullHtml,"{{TotalSkyBuyAmount}}",numberFormat.format(dSkyBuyTotalAmount));

			}

			//To Generate PO PDF
			String sHTMLFileName = sFilePath+"test.html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sFullHtml);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			Dimension format = PD4Constants.A4;
			//pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageSize(format);
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1050);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+"test.pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			oPdfFile = new File(pdfFileName);
			oHtmlFile = new File(sHTMLFileName);

			if(baPdf!=null){
				ServletOutputStream stream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(baPdf.length);
				response.setHeader("Content-Disposition","Email" + ";filename=" + "Customer Details");
				stream.write(baPdf);
				stream.flush();
				stream.close();

			}

		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("OrderAction::exportOrderReportPdf:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		finally{
			if(oPdfFile.exists())
				oPdfFile.delete();
			if(oHtmlFile.exists())
				oHtmlFile.delete();
		}
		logger.info("OrderAction::exportOrderReportPdf:EXIT");
		return null;

	}
}
