package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.dao.AirlineDAO.getAirlineComments;
import static com.sbh.dao.OrderDAO.decryptInputData;
import static com.sbh.dao.OrderDAO.encryptData;
import static com.sbh.dao.OrderDAO.encryptInputData;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.sbh.dao.AirlineDAO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.ImageScale;
import com.sbh.dao.VendorDAO;
import com.sbh.forms.AirlineRegForm;
import com.sbh.forms.SearchAirlineForm;
import com.sbh.util.Utils;
import com.sbh.vo.AirlineRegVO;
import com.sbh.vo.KeyVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerRegistrationCommentsVO;


public class AirlineRegAction extends DispatchAction{

	private static Logger logger = LogManager.getLogger(AirlineRegAction.class);	

	public ActionForward initAddAirlineReg(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("AirlineRegAction::initAirlineReg:ENTER");    	  
		String sFwrdKey="initAirlineRegSuccess";     
		AirlineRegForm oAirlineRegForm =(AirlineRegForm)form;
		String sReturnPolicy ="";
		HttpSession oSession=request.getSession(true); 
		try{
			request.removeAttribute("airlineDetails");
			oSession.removeAttribute("airlineDetails");
			oSession.removeAttribute("airlineRegForm");
			request.setAttribute("mode","airlineAdd" );
		
			sReturnPolicy = System.getProperty("ReturnPolicy").trim();
			oAirlineRegForm.setAirReturnPolicy(sReturnPolicy);
			request.setAttribute("airlineDetails",oAirlineRegForm);
		
			
			saveToken(request);
		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("AirlineRegAction::initAirlineReg:Exception "+e.getMessage());
		}
		
		logger.info("AirlineRegAction::initAirlineReg:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	
	/* To add new airline */
	public ActionForward addAirlineReg(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("AirlineRegAction::addAirlineReg:ENTER");
		String sFrdKey="airlineRegSuccess";
		String sURI = null, sPath = null, sOwnerType = "", sUpdaterOwnerType = "";
		String encryptedAirlinePassword = null;
		LoginVO oLoginVO = null;
		HttpSession oSession=request.getSession(true);
		KeyVO oKeyVO = null;
		List<PartnerRegistrationCommentsVO> alAirlineComments = null;
		ArrayList alAirlineRegStatus = new ArrayList();
		AirlineRegVO oAirlineRegVO = new AirlineRegVO();
		String sImageViewPath = null;
		long lAirId = 0;
		try{
			AirlineRegForm oAirlineRegForm=(AirlineRegForm)form;
			if(isTokenValid(request)){
				String sAirName = oAirlineRegForm.getAirName();
				sAirName=sAirName==null?"":sAirName.trim();

				String sAirUserId = oAirlineRegForm.getAirUserId();
				sAirUserId=sAirUserId==null?"":sAirUserId.trim();

				//The following variables used for airline logo upload. 
				String sImageUploadPath = System.getProperty("ImageUploadPath").trim();	
				sImageViewPath = System.getProperty("ImageViewPath").trim();	
				String sSrcImgPath = sImageUploadPath+"\\AirlineLogo\\OriginalImg\\";
				String sImageName = "";
				String sOwnerId = null;
				ArrayList alImageInfo = null;
				
				alAirlineRegStatus = AirlineDAO.validateAirlineCode(sAirName,sAirUserId,"0","Add");		

				int iAirNameCount =(Integer) alAirlineRegStatus.get(0);
				int iAirUserIdCount =(Integer) alAirlineRegStatus.get(1);
				
				sURI=request.getRequestURI();
				sPath=Utils.getUserTypeFromURI(sURI);
				sPath=sPath==null?"":sPath.trim();
				
				if("HTTP".equalsIgnoreCase(request.getScheme()))
					sImageViewPath = System.getProperty("ImageViewPath").trim();
				else if("HTTPS".equalsIgnoreCase(request.getScheme()))
					sImageViewPath = System.getProperty("SecureImageViewPath").trim();
				
				if(sPath.equalsIgnoreCase(AIRLINE))
					oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
				else if(sPath.equalsIgnoreCase(ADMIN))
					oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
				
				if(oLoginVO != null) {
					sUpdaterOwnerType = oLoginVO.getUserId();
				}
				sOwnerType = sOwnerType==null?"":sOwnerType.trim();
				
				if(iAirNameCount==0 && iAirUserIdCount ==0){
					
					String sAirlineContactName = oAirlineRegForm.getAirContactName();
					sAirlineContactName = sAirlineContactName ==null?"":sAirlineContactName.trim();
					oAirlineRegForm.setAirContactName(CatalogueDAO.toTitleCase(sAirlineContactName));
					
					String sAirlineContactName2 = oAirlineRegForm.getAirContactName2();
					sAirlineContactName2 = sAirlineContactName2 ==null?"":sAirlineContactName2.trim();
					oAirlineRegForm.setAirContactName2(CatalogueDAO.toTitleCase(sAirlineContactName2));
					
					String sReturnPolicy = oAirlineRegForm.getAirReturnPolicy();
					sReturnPolicy =sReturnPolicy ==null?"":sReturnPolicy.trim();
					oAirlineRegForm.setAirReturnPolicy(sReturnPolicy);
					
					String sComments = oAirlineRegForm.getAirComments();
					sComments =sComments ==null?"":sComments.trim();
					oAirlineRegForm.setAirComments(sComments);
					
					String sAirPassword = oAirlineRegForm.getAirPassword();
					
					oKeyVO = encryptData(oAirlineRegForm.getAirPassword(),"SERVER","");
					if(oKeyVO != null)
						encryptedAirlinePassword = oKeyVO.getEncryptedData(); 
					encryptedAirlinePassword = encryptedAirlinePassword==null?"":encryptedAirlinePassword.trim();
					oAirlineRegForm.setAirPassword(encryptedAirlinePassword);
					
					FormFile oAirlineLogoFormFile = oAirlineRegForm.getAirlineLogo();
					FormFile oAboutMeFormFile = oAirlineRegForm.getAboutMe();
					
					//To add the airline details.
					lAirId = AirlineDAO.addAirlineDetails(oAirlineRegForm,oLoginVO.getUserId(),oKeyVO.getRefId());
					//Get all the airline comments.
					alAirlineComments = getAirlineComments(lAirId+"",AIRLINE);
					
					sOwnerType = AIRLINE;
					sOwnerId = lAirId+"";
					
					if(lAirId > 0) {
						sSrcImgPath = sImageUploadPath+"\\SkyBuyPics\\"+sOwnerType+"_"+sOwnerId+"\\AirlineLogo\\OriginalImg\\";
						sImageName = lAirId+"";
						
						if(oAirlineLogoFormFile!= null && !oAirlineLogoFormFile.getFileName().equals("")){
							alImageInfo = ImageScale.createAirlienLogoImage(oAirlineLogoFormFile,sImageViewPath,sImageUploadPath,sImageName,"logo",sSrcImgPath,sOwnerType,sOwnerId);
							if(alImageInfo !=null){
								oAirlineRegVO.setAirlineLogoType((String)alImageInfo.get(0));
								oAirlineRegVO.setAirlineLogoPath((String)alImageInfo.get(1));
							}
							AirlineDAO.updateAirlineLogoDetails(oAirlineRegVO, lAirId+"", sUpdaterOwnerType, sOwnerType, sOwnerId);
						}
						
						if(oAboutMeFormFile!= null && !oAboutMeFormFile.getFileName().equals("")){
							alImageInfo = ImageScale.createAboutMeFile(oAboutMeFormFile,sImageUploadPath,sImageName,sOwnerType,sOwnerId);
							if(alImageInfo !=null){
								oAirlineRegVO.setAboutMeType((String)alImageInfo.get(0));
								oAirlineRegVO.setAboutMePath((String)alImageInfo.get(1));
							}
							AirlineDAO.updatePartnerAboutMeDetails(sOwnerType, sOwnerId,oAirlineRegVO.getAboutMeType(), oLoginVO.getUserId());
						}
					}
					// Insert user audit details
					String sInsertedUserDetails =AirlineDAO.getInsertedAirlineDetails(oAirlineRegForm);
					VendorDAO.insertUserAuditDetails(lAirId+"",AIRLINE, sInsertedUserDetails,null,null,oAirlineRegForm.getAirComments(),ADMIN,"AIRLINE ADDED");
					
					oAirlineRegForm.setAirlineLogoPath(oAirlineRegVO.getAirlineLogoPath());
					AirlineRegVO oNewAirlineRegVO = AirlineDAO.getAirlineDetailsById(lAirId+"",oLoginVO.getId());
					BeanUtils.copyProperties(oAirlineRegForm,oNewAirlineRegVO); 
					oAirlineRegForm.setAirPhone1(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirPhone1()));
					oAirlineRegForm.setAirPhone2(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirPhone2()));
					oAirlineRegForm.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oAirlineRegForm.getCustServicePhoneno()));
					oAirlineRegForm.setAirFax(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirFax()));
					
					//send login details to airline through mail
					VendorDAO.sendEmail(oAirlineRegForm.getAirName(),oAirlineRegForm.getAirContactName(),oAirlineRegForm.getAirUserId(),sAirPassword,oAirlineRegForm.getAirEmail(),oAirlineRegForm.getAirEmail2(),lAirId,AIRLINE,oLoginVO.getUserId(),"");
					
					if(oAirlineRegForm.getAirStatus()!=null && oAirlineRegForm.getAirStatus().equalsIgnoreCase("I"))
						VendorDAO.sendStatusEmail(oAirlineRegForm.getAirStatus(),oAirlineRegForm.getAirName(),oAirlineRegForm.getAirContactName(),oAirlineRegForm.getAirEmail(),lAirId,AIRLINE,oLoginVO.getUserId(),"");
					
				}else{
					if(iAirNameCount>0 && iAirUserIdCount>0)				
						request.setAttribute("airNameairUserIdExist","true");				
					else if(iAirNameCount>0)				
						request.setAttribute("airNameExist","true");				
					else if(iAirUserIdCount>0)				
						request.setAttribute("airUserIdExist","true");			
					
					sFrdKey="airlineRegFailure";
				}
				if(oAirlineRegForm!=null) {
					if(oAirlineRegForm.getAirlineLogoPath()!=null && oAirlineRegForm.getAirlineLogoPath().trim().length()>0)
						oAirlineRegForm.setAirlineLogoPath(sImageViewPath+oAirlineRegForm.getAirlineLogoPath()+"/"+oAirlineRegForm.getAirId()+"_logo."+oAirlineRegForm.getAirlineLogoType());			
					else
						oAirlineRegForm.setAirlineLogoPath("");
					
					if(oAirlineRegForm.getAboutMePath()!=null && oAirlineRegForm.getAboutMePath().trim().length()>0)
						oAirlineRegForm.setAboutMePath(sImageViewPath+oAirlineRegForm.getAboutMePath()+"/"+oAirlineRegForm.getAirId()+"."+oAirlineRegForm.getAboutMeType());			
					else
						oAirlineRegForm.setAboutMePath("");
					
					if(oAirlineRegForm.getAboutMePath()!=null && !oAirlineRegForm.getAboutMePath().equalsIgnoreCase("")){
						oSession.setAttribute("AboutMeFileName", oAirlineRegForm.getAboutMePath());
					}else{
						oSession.removeAttribute("AboutMeFileName");
					}
				}
				resetToken(request);
			}
			
			
			request.setAttribute("mode","airlineAdd" );
			request.setAttribute("airlineDetails",oAirlineRegForm);
			
			if(alAirlineComments != null && alAirlineComments.size() > 0) {
				request.setAttribute("AirlineComments",alAirlineComments);
			}

			logger.info("AirlineRegAction::addAirlineReg:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("AirlineRegAction::addVendor:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}

	
	public ActionForward initSearchAirline(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("AirlineRegAction::initSearchAirline:ENTER");    	  
		String sFwrdKey="initSearchAirlineSuccess";    	
		HttpSession oSession = request.getSession();
		
		try{		
			oSession.removeAttribute("searchAirlineForm");		

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("AirlineRegAction::initSearchAirline:Exception "+e.getMessage());
		}
		logger.info("AirlineRegAction::initSearchAirline:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	public ActionForward searchAirline(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("AirlineRegAction::searchAirline:ENTER");
		String sFrdKey="searchAirlineSuccess";
		List<AirlineRegVO> alAirlineInfo = null;	
		SearchAirlineForm oSearchAirlineForm=(SearchAirlineForm)form; 	

		String sSearchBy=null,sSearchValue = null ;
		try{
			if(oSearchAirlineForm!=null){
				sSearchBy = oSearchAirlineForm.getAirlineSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchAirlineForm.setAirlineSearchBy(sSearchBy);

				sSearchValue = oSearchAirlineForm.getAirlineSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchAirlineForm.setAirlineSearchValue(sSearchValue);			

			}			

			alAirlineInfo = AirlineDAO.getAirlineDetails(oSearchAirlineForm);
			if(alAirlineInfo!=null && alAirlineInfo.size()>0){
				request.setAttribute("airlineInfo", alAirlineInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  
			logger.info("AirlineRegAction::searchAirline:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("AirlineRegAction::searchAirline:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward editAirlineDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("AirlineRegAction::editAirlineDetails:ENTER");
		String sFrdKey="editAirlineSuccess",sURI=null,sPath=null;
		String sImageViewPath = null;
		String sSourceofEdit = null;
		AirlineRegVO oAirlineRegVO = null;	
		AirlineRegForm oAirlineRegForm=(AirlineRegForm)form; 		
		LoginVO oLoginVO = null;
		HttpSession oSession=request.getSession(true);
		try{
			String sAirId=request.getParameter("airId");			
            sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			sSourceofEdit = request.getParameter("SourceOfEdit");
			sSourceofEdit = sSourceofEdit==null?"":sSourceofEdit.trim();
			
			if("HTTP".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("ImageViewPath").trim();
			else if("HTTPS".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("SecureImageViewPath").trim();
			
			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			

			if(oLoginVO!=null){
				if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
					sFrdKey="editAdminAirlineSuccess";			
			}	
			oAirlineRegVO = AirlineDAO.getAirlineDetailsById(sAirId,oLoginVO.getId());
			if(oAirlineRegVO!=null) {
				if(oAirlineRegVO.getAirPassword() != null && oAirlineRegVO.getAirPassword().trim().length() > 0) {
					String sDecryptedAirPassword = decryptInputData(oAirlineRegVO.getAirPassword(),"SERVER",oAirlineRegVO.getKeyRefId());
					oAirlineRegVO.setAirPassword(sDecryptedAirPassword);
				}
				if(oAirlineRegVO.getAirlineLogoPath()!=null && oAirlineRegVO.getAirlineLogoPath().trim().length()>0)
					oAirlineRegVO.setAirlineLogoPath(sImageViewPath+oAirlineRegVO.getAirlineLogoPath()+"/"+oAirlineRegVO.getAirId()+"_logo."+oAirlineRegVO.getAirlineLogoType());			
				else
					oAirlineRegVO.setAirlineLogoPath("");
				
				if(oAirlineRegVO.getAboutMePath()!=null && oAirlineRegVO.getAboutMePath().trim().length()>0)
					oAirlineRegVO.setAboutMePath(sImageViewPath+oAirlineRegVO.getAboutMePath()+"/"+oAirlineRegVO.getAirId()+"."+oAirlineRegVO.getAboutMeType());			
				else
					oAirlineRegVO.setAboutMePath("");
				
				if(oAirlineRegVO.getAboutMePath()!=null && !oAirlineRegVO.getAboutMePath().equalsIgnoreCase("")){
					oSession.setAttribute("AboutMeFileName", oAirlineRegVO.getAboutMePath());
				}else{
					oSession.removeAttribute("AboutMeFileName");
				}
				
				BeanUtils.copyProperties(oAirlineRegForm,oAirlineRegVO); 
				oSession.setAttribute("airlineDetails",oAirlineRegVO);
				request.setAttribute("mode","airlineEdit" );
				request.setAttribute("SourceOfEdit", sSourceofEdit);
			} 
			logger.info("AirlineRegAction::editAirlineDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("AirlineRegAction::editAirlineDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward updateAirlineDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("AirlineRegAction::updateAirlineDetails:ENTER");
		String sFrdKey="updateAirlineSuccess",sURI=null,sPath=null;
		String sAirlinePassword = null;
		KeyVO oKeyVO = null;
		AirlineRegForm oAirlineRegForm=(AirlineRegForm)form; 
		ArrayList alAirlineRegStatus = new ArrayList();
		List<PartnerRegistrationCommentsVO> alAirlineComments = null;
		LoginVO oLoginVO = null;
		AirlineRegVO oAirlineRegVO = null;	
		long lAirId =0;
		HttpSession oSession=request.getSession(true);
		
		//The following variables used for Image scaling.
		String sImageUploadPath = null;
		String sImageViewPath = null;
		String sSrcImgPath = null;
		String sImageName = "";
		ArrayList alImageInfo = null;
		String sOwnerType = "";
		String sUpdaterOwnerType = "";
		String sOwnerId = "";
		
		
		try{
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			sImageUploadPath = System.getProperty("ImageUploadPath").trim();	
			sSrcImgPath = sImageUploadPath+"\\AirlineLogo\\OriginalImg\\";
			
			if("HTTP".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("ImageViewPath").trim();
			else if("HTTPS".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("SecureImageViewPath").trim();
			
			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");

			if(oLoginVO!=null){
				sUpdaterOwnerType = oLoginVO.getUserType();
				if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
					sFrdKey="updateAdminAirlineSuccess";			
			}
			
			sOwnerType = sOwnerType == null?"":sOwnerType.trim();
			
			oAirlineRegVO = (AirlineRegVO)oSession.getAttribute("airlineDetails");
			
			sOwnerType = AIRLINE;
			sOwnerId = oAirlineRegForm.getAirId();
			
			String sAirId = oAirlineRegForm.getAirId();
			sAirId =sAirId==null?"":sAirId.trim();
			
			if(sAirId.trim().length()>0)
				lAirId = Long.parseLong(sAirId);

			String sAirName = oAirlineRegForm.getAirName();
			sAirName =sAirName==null?"":sAirName.trim();

			String sAirUserId = oAirlineRegForm.getAirUserId();
			sAirUserId=sAirUserId==null?"":sAirUserId.trim();
			
			String sAirPassword = oAirlineRegForm.getAirPassword();
			sAirPassword = sAirPassword==null?"":sAirPassword.trim();
			
			String sAirStatus = oAirlineRegForm.getAirStatus();
			sAirStatus=sAirStatus==null?"":sAirStatus.trim();

			alAirlineRegStatus = AirlineDAO.validateAirlineCode(sAirName,sAirUserId,sAirId,"Update");		

			int iAirNameCount =(Integer) alAirlineRegStatus.get(0);
			int iAirUserIdCount =(Integer) alAirlineRegStatus.get(1);

			if(iAirNameCount==0 && iAirUserIdCount ==0){
				
				String sAirlineContactName = oAirlineRegForm.getAirContactName();
				sAirlineContactName = sAirlineContactName ==null?"":sAirlineContactName.trim();
				oAirlineRegForm.setAirContactName(CatalogueDAO.toTitleCase(sAirlineContactName));
				
				String sOldAirUserId = oAirlineRegVO.getAirUserId();
				sOldAirUserId = sOldAirUserId ==null?"":sOldAirUserId.trim();
				
				String sOldAirPassword = oAirlineRegVO.getAirPassword();
				sOldAirPassword = sOldAirPassword ==null?"":sOldAirPassword.trim();
				
				String sOldAirStatus = oAirlineRegVO.getAirStatus();
				sOldAirStatus = sOldAirStatus ==null?"":sOldAirStatus.trim();
				
				String sReturnPolicy = oAirlineRegForm.getAirReturnPolicy();
				sReturnPolicy =sReturnPolicy ==null?"":sReturnPolicy.trim();
				oAirlineRegForm.setAirReturnPolicy(sReturnPolicy);
				
				String sComments = oAirlineRegForm.getAirComments();
				sComments =sComments ==null?"":sComments.trim();
				oAirlineRegForm.setAirComments(sComments);
				
				oKeyVO = encryptData(oAirlineRegForm.getAirPassword(),"SERVER",oAirlineRegVO.getKeyRefId());
				if(oKeyVO != null)
					sAirlinePassword = oKeyVO.getEncryptedData();
				sAirlinePassword = sAirlinePassword==null?"":sAirlinePassword.trim();
				oAirlineRegForm.setAirPassword(sAirlinePassword);
				
				//Update the airline details.
				AirlineDAO.updateAirlineDetails(oAirlineRegForm,oLoginVO.getUserType(),oLoginVO.getUserId(),oLoginVO.getId(),oKeyVO.getRefId());
				//Get all the airline comments.
				alAirlineComments = getAirlineComments(sAirId,AIRLINE);
				
				FormFile oAirlineLogoFormFile = oAirlineRegForm.getAirlineLogo();
				FormFile oAboutMeFormFile = oAirlineRegForm.getAboutMe();
				
				if(lAirId > 0) {
					sImageName = lAirId+"";
					sSrcImgPath = sImageUploadPath+"\\SkyBuyPics\\"+sOwnerType+"_"+sOwnerId+"\\AirlineLogo\\OriginalImg\\";
					
					if(oAirlineLogoFormFile!= null && !oAirlineLogoFormFile.getFileName().equals("")){
						alImageInfo = ImageScale.createAirlienLogoImage(oAirlineLogoFormFile,sImageViewPath,sImageUploadPath,sImageName,"logo",sSrcImgPath,sOwnerType,sOwnerId);
						if(alImageInfo !=null){
							oAirlineRegVO.setAirlineLogoType((String)alImageInfo.get(0));
							oAirlineRegVO.setAirlineLogoPath((String)alImageInfo.get(1));
						}
						AirlineDAO.updateAirlineLogoDetails(oAirlineRegVO, lAirId+"", sUpdaterOwnerType, sOwnerType, sOwnerId);
					}
					
					if(oAboutMeFormFile!= null && !oAboutMeFormFile.getFileName().equals("")){
						alImageInfo = ImageScale.createAboutMeFile(oAboutMeFormFile,sImageUploadPath,sImageName,sOwnerType,sOwnerId);
						if(alImageInfo !=null){
							oAirlineRegVO.setAboutMeType((String)alImageInfo.get(0));
							oAirlineRegVO.setAboutMePath((String)alImageInfo.get(1));
							//oAirlineRegForm.setAboutMePath(sImageViewPath+oAirlineRegVO.getAboutMePath()+"/"+oAirlineRegVO.getAirId()+"."+oAirlineRegVO.getAboutMeType());
						}
						AirlineDAO.updatePartnerAboutMeDetails(sOwnerType, sOwnerId,oAirlineRegVO.getAboutMeType(), oLoginVO.getUserId());
					}else{
						oAirlineRegForm.setAboutMePath(oAirlineRegVO.getAboutMePath());
						oAirlineRegForm.setAboutMeType(oAirlineRegVO.getAboutMeType());
					}
				}
				
				//send mail to vendor if admin chage Userid/password
				//if(oLoginVO.getUserType().equalsIgnoreCase(ADMIN)){	
					if(sAirStatus.equalsIgnoreCase("A")){
						if(!sAirUserId.equals(sOldAirUserId) || !sAirPassword.equals(sOldAirPassword)) {
							
							VendorDAO.sendEditUserDetailsByEmail(oAirlineRegForm.getAirName(),oAirlineRegForm.getAirContactName(),sAirUserId,sAirPassword,sOldAirUserId,sOldAirPassword,oAirlineRegForm.getAirEmail(),oAirlineRegForm.getAirEmail2(),lAirId,AIRLINE,oLoginVO.getUserId(),"",oLoginVO.getUserType());
						}
					}
				//}
				
				if(!sAirStatus.equalsIgnoreCase(sOldAirStatus)){					
					VendorDAO.sendStatusEmail(oAirlineRegForm.getAirStatus(),oAirlineRegForm.getAirName(),oAirlineRegForm.getAirContactName(),oAirlineRegForm.getAirEmail(),lAirId,AIRLINE,oLoginVO.getUserId(),"");
				}
				
				//Insert User Audit details
				AirlineDAO.getUpdatedUserDetails(oAirlineRegForm,oAirlineRegVO,oLoginVO,oKeyVO.getRefId());
				oAirlineRegForm.setAirlineLogoPath(oAirlineRegVO.getAirlineLogoPath());
				
				AirlineRegVO oNewAirlineRegVO = AirlineDAO.getAirlineDetailsById(sAirId,oLoginVO.getId());
				if(oNewAirlineRegVO!=null) {
					if(oNewAirlineRegVO.getAirlineLogoPath()!=null && oNewAirlineRegVO.getAirlineLogoPath().trim().length()>0)
						oNewAirlineRegVO.setAirlineLogoPath(sImageViewPath+oNewAirlineRegVO.getAirlineLogoPath()+"/"+oNewAirlineRegVO.getAirId()+"_logo."+oNewAirlineRegVO.getAirlineLogoType());			
					else
						oNewAirlineRegVO.setAirlineLogoPath("");
					
					if(oNewAirlineRegVO.getAboutMePath()!=null && oNewAirlineRegVO.getAboutMePath().trim().length()>0)
						oNewAirlineRegVO.setAboutMePath(sImageViewPath+oNewAirlineRegVO.getAboutMePath()+"/"+oNewAirlineRegVO.getAirId()+"."+oNewAirlineRegVO.getAboutMeType());			
					else
						oNewAirlineRegVO.setAboutMePath("");
					
					if(oNewAirlineRegVO.getAboutMePath()!=null && !oNewAirlineRegVO.getAboutMePath().equalsIgnoreCase("")){
						oSession.setAttribute("AboutMeFileName", oNewAirlineRegVO.getAboutMePath());
					}else{
						oSession.removeAttribute("AboutMeFileName");
					}
					
					BeanUtils.copyProperties(oAirlineRegForm,oNewAirlineRegVO); 
				}
				oAirlineRegForm.setAirPhone1(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirPhone1()));
				oAirlineRegForm.setAirPhone2(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirPhone2()));
				oAirlineRegForm.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oAirlineRegForm.getCustServicePhoneno()));
				oAirlineRegForm.setAirFax(AirlineDAO.getPhoneFormat(oAirlineRegForm.getAirFax()));
			}else{ 
				if(iAirNameCount>0 && iAirUserIdCount>0)				
					request.setAttribute("airNameairUserIdExist","true");				
				else if(iAirNameCount>0)				
					request.setAttribute("airNameExist","true");				
				else if(iAirUserIdCount>0)				
					request.setAttribute("airUserIdExist","true");

				if(oLoginVO!=null){
					if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
						sFrdKey="updateAdminAirlineFailure";	
					else	
						sFrdKey="updateAirlineFailure";					
				}
			}
			request.setAttribute("mode","airlineEdit" );
			
			request.setAttribute("airlineDetails",oAirlineRegForm);
			if(alAirlineComments != null && alAirlineComments.size() > 0) {
				request.setAttribute("AirlineComments",alAirlineComments);
			}

			logger.info("AirlineRegAction::updateAirlineDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("AirlineRegAction::updateAirlineDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	
/* View Airline Details*/
	
	public ActionForward viewAirlineDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("AirlineRegAction::viewAirlineDetails:ENTER");
		String sFrdKey="viewAdminAirlineSuccess",sURI=null,sPath=null;
		String sImageViewPath = null;
		AirlineRegVO oAirlineRegVO = null;	
		AirlineRegForm oAirlineRegForm=(AirlineRegForm)form; 		
		LoginVO oLoginVO = null;
		HttpSession oSession=request.getSession(true);
		List<PartnerRegistrationCommentsVO> alAirlineComments = null;
		try{
			String sAirId=request.getParameter("airId");			
            sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if("HTTP".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("ImageViewPath").trim();
			else if("HTTPS".equalsIgnoreCase(request.getScheme()))
				sImageViewPath = System.getProperty("SecureImageViewPath").trim();
			
			if(sPath.equalsIgnoreCase(AIRLINE)){
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
				sFrdKey="viewAirlineSuccess";
			}
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			

			oAirlineRegVO = AirlineDAO.getAirlineDetailsById(sAirId,oLoginVO.getId());
			if(oAirlineRegVO!=null) {
				if(oAirlineRegVO.getAirPassword() != null && oAirlineRegVO.getAirPassword().trim().length() > 0) {
					String sDecryptedAirPassword = decryptInputData(oAirlineRegVO.getAirPassword(),"SERVER",oAirlineRegVO.getKeyRefId());
					oAirlineRegVO.setAirPassword(sDecryptedAirPassword);
				}
				if(oAirlineRegVO.getAirlineLogoPath()!=null && oAirlineRegVO.getAirlineLogoPath().trim().length()>0)
					oAirlineRegVO.setAirlineLogoPath(sImageViewPath+oAirlineRegVO.getAirlineLogoPath()+"/"+oAirlineRegVO.getAirId()+"_logo."+oAirlineRegVO.getAirlineLogoType());			
				else
					oAirlineRegVO.setAirlineLogoPath("");
				
				if(oAirlineRegVO.getAboutMePath()!=null && oAirlineRegVO.getAboutMePath().trim().length()>0)
					oAirlineRegVO.setAboutMePath(sImageViewPath+oAirlineRegVO.getAboutMePath()+"/"+oAirlineRegVO.getAirId()+"."+oAirlineRegVO.getAboutMeType());			
				else
					oAirlineRegVO.setAboutMePath("");
				
				if(oAirlineRegVO.getAboutMePath()!=null && !oAirlineRegVO.getAboutMePath().equalsIgnoreCase("")){
					oSession.setAttribute("AboutMeFileName", oAirlineRegVO.getAboutMePath());
				}else{
					oSession.removeAttribute("AboutMeFileName");
				}
				
				BeanUtils.copyProperties(oAirlineRegForm,oAirlineRegVO); 
				
				oAirlineRegVO.setAirPhone1(AirlineDAO.getPhoneFormat(oAirlineRegVO.getAirPhone1()));
				oAirlineRegVO.setAirPhone2(AirlineDAO.getPhoneFormat(oAirlineRegVO.getAirPhone2()));
				oAirlineRegVO.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oAirlineRegVO.getCustServicePhoneno()));
				oAirlineRegVO.setAirFax(AirlineDAO.getPhoneFormat(oAirlineRegVO.getAirFax()));
				
				oSession.setAttribute("airlineDetails",oAirlineRegVO);
				request.setAttribute("mode","airlineEdit" );
				alAirlineComments = getAirlineComments(sAirId,AIRLINE);
				if(alAirlineComments != null && alAirlineComments.size() > 0) {
					request.setAttribute("AirlineComments",alAirlineComments);
				}
				
			} 
			logger.info("AirlineRegAction::viewAirlineDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("AirlineRegAction::viewAirlineDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

}
