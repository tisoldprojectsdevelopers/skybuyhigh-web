package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.ADMIN_LOGIN_INFO;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.AIRLINE_LOGIN_INFO;
import static com.sbh.contants.SkyBuyContants.NO_RECORDS;
import static com.sbh.contants.SkyBuyContants.NO_RECORDS_FOUND;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.contants.SkyBuyContants.VENDOR_LOGIN_INFO;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.FontKey;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;
import net.sf.jasperreports.engine.export.PdfFont;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;

import com.sbh.dao.OrderDAO;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.CustomerSuggestionsVO;
import com.sbh.vo.LoginVO;

public class ReportAction extends DispatchAction{
	private static Logger logger = LogManager.getLogger(ReportAction.class);
	
	public ActionForward initOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("ReportAction::initOrderReport:ENTRY");
		String sForwardKey="searchOrderReportSuccess";
		
		try{
			HttpSession oSession = request.getSession(true);

			/*if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("orderReportForm");*/
			
			DynaActionForm oForm = (DynaActionForm)form;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			int month = cal.get(Calendar.MONTH)+1;
			int year = cal.get(Calendar.YEAR);
			
			String sMonth = month+"";
			/*if(sMonth.length() < 2){
				sMonth = "0"+sMonth;
			}*/

			oForm.set("month", sMonth);
			oForm.set("year", year+"");
			oForm.set("searchType", "");
			oForm.set("searchValue", "");
			oForm.set("orderFromDate", "");
			oForm.set("orderToDate", "");

			
		}catch(Exception e) {
			logger.debug("ReportAction::initOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("ReportAction::initOrderReport:EXIT");
		return mapping.findForward(sForwardKey);

	}
	
	public ActionForward getOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("ReportAction::getOrderReport:ENTRY");
		Connection conn = null;
		java.util.Date dToday = null;
		java.sql.Timestamp tsToday = null;
		List<CustomerSuggestionsVO> alCustFeedback = null;
		boolean bIsExist = false; 
		HashMap hmAllData = null;
		Map hmParameter = null;
		JasperPrint jasperPrint = null;
		JRExporter exporter = null;
		LoginVO oLoginVO = null;
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sSearchType = null,sSearchValue = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey="searchOrderReportSuccess";
		String sHTMLFileName = null;
		String sHTMLFilePath = null;
		String sJasperFileName= null;
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);
		
			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();
			
			DynaActionForm oForm = (DynaActionForm)form;

			sSearchType = oForm.getString("searchType");
			sSearchType = sSearchType==null?"":sSearchType.trim();

			sSearchValue = oForm.getString("searchValue");
			sSearchValue = sSearchValue==null?"":sSearchValue.trim();

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();
			
			bIsExist = OrderDAO.getOrderReport(sSearchType,sSearchValue, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				request.setAttribute("OrderReportDetails", "OrderExist");
			}else{
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
				oSession.removeAttribute("OrderReportSearch");
			}
		
		}catch(Exception e) {
			logger.debug("ReportAction::getOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}finally{
			if(conn!=null){
				conn.close();
			}
		}
		logger.info("ReportAction::getOrderReport:EXIT");
		return mapping.findForward(sForwardKey);

	}
	
	
	public ActionForward getHTMLOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("ReportAction::getHTMLOrderReport:ENTRY");
		Connection conn = null;
		boolean bIsExist = false; 
		Map hmParameter = null;
		JasperPrint jasperPrint = null;
		JRExporter exporter = null;
		LoginVO oLoginVO = null;
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sSearchType = null,sSearchValue = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey;
		String sJasperFileName= null;
		String sJasperFilePath= null;
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);
		
			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();
			
			DynaActionForm oForm = (DynaActionForm)form;

			sSearchType = oForm.getString("searchType");
			sSearchType = sSearchType==null?"":sSearchType.trim();

			sSearchValue = oForm.getString("searchValue");
			sSearchValue = sSearchValue==null?"":sSearchValue.trim();

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();
			
			bIsExist = OrderDAO.getOrderReport(sSearchType,sSearchValue, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				
				
				sJasperFilePath = System.getProperty("OrderReportPath");
				sJasperFilePath = sJasperFilePath==null?"":sJasperFilePath.trim();
				
				sJasperFileName = System.getProperty("OrderReportName");
				sJasperFileName = sJasperFileName==null?"":sJasperFileName.trim();
				
				sJasperFileName = sJasperFilePath + sJasperFileName; 
				
				hmParameter = new HashMap();
				hmParameter.put("SearchBy",sSearchType);
				hmParameter.put("SearchValue",sSearchValue);
				hmParameter.put("Month",sMonth);
				hmParameter.put("Year",sYear);
				hmParameter.put("StartDate",sFromDate);
				hmParameter.put("EndDate",sToDate);
				hmParameter.put("LoginUserType",oLoginVO.getUserType());
				hmParameter.put("LoginUserId",oLoginVO.getRefId());
				hmParameter.put("SUBREPORT_DIR",sJasperFilePath);
				conn = DBConnection.getSQL2005Connection();
				jasperPrint = JasperFillManager.fillReport(sJasperFileName, hmParameter, conn);
				response.setContentType("text/html");
				response.setHeader("Content-Disposition", "inline; filename=\"OrderReport.html\"");
				exporter = new JRXhtmlExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
				exporter.setParameter(JRHtmlExporterParameter.IS_WRAP_BREAK_WORD, Boolean.TRUE);
				exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "");
				exporter.exportReport();
				oSession.setAttribute("OrderReportSearch", hmParameter);
			}else{
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
				oSession.removeAttribute("OrderReportSearch");
			}
		
		}catch(Exception e) {
			logger.debug("ReportAction::getHTMLOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
			//return mapping.findForward(sForwardKey);
		}finally{
			if(conn!=null){
				conn.close();
			}
		}
		logger.info("OrderAction::getHTMLOrderReport:EXIT");
		return null;

	}


	public ActionForward getPDFOrderReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getPDFOrderReport:ENTRY");
		Connection conn = null;
		Map hmParameter = null;
		JasperPrint jasperPrint = null;
		JRExporter exporter = null;
		LoginVO oLoginVO = null;
		String sURI = null, sPath = null ;
		String sForwardKey="searchOrderReportSuccess";
		
		String sFontFolderPath = null;
		String sJasperFileName= null;
		String sJasperFilePath = null;
		try{
			
			conn = DBConnection.getSQL2005Connection();
			
			sJasperFilePath = System.getProperty("OrderReportPath");
			sJasperFilePath = sJasperFilePath==null?"":sJasperFilePath.trim();
			
			sJasperFileName = System.getProperty("OrderReportName");
			sJasperFileName = sJasperFileName==null?"":sJasperFileName.trim();
			
			sJasperFileName = sJasperFilePath + sJasperFileName; 
			
			sFontFolderPath = getServlet().getServletContext().getRealPath("");
			
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);
			
			
			hmParameter = (HashMap)oSession.getAttribute("OrderReportSearch");
			if(hmParameter!=null){
				jasperPrint = JasperFillManager.fillReport(sJasperFileName, hmParameter, conn);
				
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+"OrderReport.pdf"); // inline, attachment
	
				exporter = new JRPdfExporter();
				HashMap   fontMap = new HashMap  ();
			    FontKey key = new FontKey("Georgia", false, false);
			    PdfFont font = new PdfFont(sFontFolderPath+"\\fonts\\Georgia.TTF", "Cp1252", true);
			    FontKey key1 = new FontKey("Georgia", true, false);
			    PdfFont font1 = new PdfFont(sFontFolderPath+"\\fonts\\georgiab.TTF", "Cp1252", true);
			    fontMap.put(key, font);
			    fontMap.put(key1, font1);
				 
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
				exporter.setParameter(JRExporterParameter.FONT_MAP, fontMap);
				
				exporter.exportReport();
			}
		
		}catch(Exception e) {
			logger.debug("OrderAction::getPDFOrderReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}finally{
			if(conn!=null){
				conn.close();
			}
		}
		logger.info("OrderAction::getPDFOrderReport:EXIT");
		return null;

	}
	
	
	
	
	
	public ActionForward initAirlineCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::initAirlineCommReport:ENTRY");
		String sForwardKey="searchAirlineCommReportSuccess";
		ArrayList alAirlineNames = null;
		
		try{
			HttpSession oSession = request.getSession(true);

			/*if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("airCommReportForm");*/
			
			alAirlineNames = OrderDAO.getAllAirlineNames();
			if(alAirlineNames!=null && alAirlineNames.size()>0){
				oSession.setAttribute("AirlineNamesList", alAirlineNames);
			}

			DynaActionForm oForm = (DynaActionForm)form;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			int month = cal.get(Calendar.MONTH)+1;
			int year = cal.get(Calendar.YEAR);
			
			String sMonth = month+"";
			/*if(sMonth.length() < 2){
				sMonth = "0"+sMonth;
			}*/

			oForm.set("month", sMonth);
			oForm.set("year", year+"");
			oForm.set("airId", "");
			oForm.set("orderFromDate", "");
			oForm.set("orderToDate", "");
			
		}catch(Exception e) {
			logger.debug("OrderAction::initAirlineCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::initAirlineCommReport:EXIT");
		return mapping.findForward(sForwardKey);

	}


	public ActionForward getAirlineCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getAirlineCommReport:ENTRY");
		List<CustomerSuggestionsVO> alCustFeedback = null;
		TreeSet tsOrderDetails =  null;
		Double dTotalOrderValue;
		HashMap hmAllData = null;
		LoginVO oLoginVO = null;
		HashMap hmMonthName = new HashMap();
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sAirId = null, sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey="searchAirlineCommReportSuccess";
		boolean bIsExist = false;
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();

			DynaActionForm oForm = (DynaActionForm)form;

			if(sLoginUser.equalsIgnoreCase(ADMIN)){
				sAirId = oForm.getString("airId");
				sAirId = sAirId==null?"":sAirId.trim();
			}else{
				sAirId = sUserId;
				sAirId = sAirId==null?"":sAirId.trim();
			}

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();



			bIsExist = OrderDAO.getAirlineCommReport(sAirId, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				request.setAttribute("AirlineCommReportDetails", "show");
			}else{
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
			}

		}catch(Exception e) {
			logger.debug("OrderAction::getAirlineCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
		}
		logger.info("OrderAction::getAirlineCommReport:EXIT");
		return mapping.findForward(sForwardKey);

	}
	
	
	public ActionForward getHTMLAirCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("ReportAction::getHTMLAirCommReport:ENTRY");
		Connection conn = null;
		boolean bIsExist = false; 
		Map hmParameter = null;
		JasperPrint jasperPrint = null;
		JRExporter exporter = null;
		LoginVO oLoginVO = null;
		String sLoginUser = null,sUserId = null,sURI = null, sPath = null ;
		String sAirId = null,sFromDate = null, sToDate = null, sMonth = null, sYear = null;
		String sForwardKey;
		String sJasperFileName= null;
		String sJasperFilePath = null;
		try{
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);

			sLoginUser = oLoginVO.getUserType();
			sLoginUser = sLoginUser==null?"":sLoginUser.trim();

			sUserId = oLoginVO.getRefId();
			sUserId = sUserId==null?"":sUserId.trim();

			DynaActionForm oForm = (DynaActionForm)form;

			if(sLoginUser.equalsIgnoreCase(ADMIN)){
				sAirId = oForm.getString("airId");
				sAirId = sAirId==null?"":sAirId.trim();
			}else{
				sAirId = sUserId;
				sAirId = sAirId==null?"":sAirId.trim();
			}

			sMonth = oForm.getString("month");
			sMonth = sMonth==null?"":sMonth.trim();

			sYear = oForm.getString("year");
			sYear = sYear==null?"":sYear.trim();

			sFromDate = oForm.getString("orderFromDate");
			sFromDate = sFromDate==null?"":sFromDate.trim();

			sToDate = oForm.getString("orderToDate");
			sToDate = sToDate==null?"":sToDate.trim();



			bIsExist = OrderDAO.getAirlineCommReport(sAirId, sFromDate, sToDate, sMonth , sYear, sLoginUser, sUserId );
			if(bIsExist) {
				
				sJasperFilePath = System.getProperty("CommissionReportPath");
				sJasperFilePath = sJasperFilePath==null?"":sJasperFilePath.trim();
				
				sJasperFileName = System.getProperty("CommissionReportName");
				sJasperFileName = sJasperFileName==null?"":sJasperFileName.trim();
				
				sJasperFileName = sJasperFilePath + sJasperFileName; 
				
				hmParameter = new HashMap();
				hmParameter.put("AirId",sAirId);
				hmParameter.put("Month",sMonth);
				hmParameter.put("Year",sYear);
				hmParameter.put("StartDate",sFromDate);
				hmParameter.put("EndDate",sToDate);
				hmParameter.put("LoginUserType",oLoginVO.getUserType());
				hmParameter.put("LoginUserId",oLoginVO.getRefId());
				hmParameter.put("SUBREPORT_DIR",sJasperFilePath);
				
				conn = DBConnection.getSQL2005Connection();
				jasperPrint = JasperFillManager.fillReport(sJasperFileName, hmParameter, conn);
				response.setContentType("text/html");
				response.setHeader("Content-Disposition", "inline; filename=\"CommissionReport.html\"");
				exporter = new JRXhtmlExporter();
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
				exporter.setParameter(JRHtmlExporterParameter.IS_WRAP_BREAK_WORD, Boolean.TRUE);
				exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "");
				exporter.exportReport();
				oSession.setAttribute("CommissionReportSearch", hmParameter);
			}else{
				request.setAttribute(NO_RECORDS, NO_RECORDS_FOUND);
				oSession.removeAttribute("CommissionReportSearch");
			}
		
		}catch(Exception e) {
			logger.debug("ReportAction::getHTMLAirCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
			//return mapping.findForward(sForwardKey);
		}finally{
			if(conn!=null){
				conn.close();
			}
		}
		logger.info("OrderAction::getHTMLAirCommReport:EXIT");
		return null;

	}


	public ActionForward getPDFAirCommReport(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("OrderAction::getPDFAirCommReport:ENTRY");
		Connection conn = null;
		Map hmParameter = null;
		JasperPrint jasperPrint = null;
		JRExporter exporter = null;
		LoginVO oLoginVO = null;
		String sURI = null, sPath = null ;
		String sForwardKey;
		String sFontFolderPath = null;
		String sJasperFileName= null;
		String sJasperFilePath = null;
		try{
			
			conn = DBConnection.getSQL2005Connection();

			sJasperFilePath = System.getProperty("CommissionReportPath");
			sJasperFilePath = sJasperFilePath==null?"":sJasperFilePath.trim();
			
			sJasperFileName = System.getProperty("CommissionReportName");
			sJasperFileName = sJasperFileName==null?"":sJasperFileName.trim();
			
			sJasperFileName = sJasperFilePath + sJasperFileName; 
			
			sFontFolderPath = getServlet().getServletContext().getRealPath("");
			
			HttpSession oSession = request.getSession(true);
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute(AIRLINE_LOGIN_INFO);
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute(ADMIN_LOGIN_INFO);	
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute(VENDOR_LOGIN_INFO);
			
			
			hmParameter = (HashMap)oSession.getAttribute("CommissionReportSearch");
			if(hmParameter!=null){
				jasperPrint = JasperFillManager.fillReport(sJasperFileName, hmParameter, conn);
				
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+"CommissionReport.pdf"); // inline, attachment
	
				exporter = new JRPdfExporter();
				HashMap   fontMap = new HashMap  ();
			    FontKey key = new FontKey("Georgia", false, false);
			    PdfFont font = new PdfFont(sFontFolderPath+"\\fonts\\Georgia.TTF", "Cp1252", true);
			    FontKey key1 = new FontKey("Georgia", true, false);
			    PdfFont font1 = new PdfFont(sFontFolderPath+"\\fonts\\georgiab.TTF", "Cp1252", true);
			    fontMap.put(key, font);
			    fontMap.put(key1, font1);
				 
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
				exporter.setParameter(JRExporterParameter.FONT_MAP, fontMap);
				
				exporter.exportReport();
			}
		
		}catch(Exception e) {
			logger.debug("OrderAction::getPDFAirCommReport:EXCEPTION"+e.getMessage());
			sForwardKey="failure";
			return mapping.findForward(sForwardKey);
		}finally{
			if(conn!=null){
				conn.close();
			}
		}
		logger.info("OrderAction::getPDFAirCommReport:EXIT");
		return null;

	}

}
