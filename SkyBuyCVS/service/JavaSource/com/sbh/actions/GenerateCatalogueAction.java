/**
 * 
 */
package com.sbh.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;

/**
 * @author nbvk
 *
 */
public class GenerateCatalogueAction extends DispatchAction {
	
	private static final Logger logger = LogManager.getLogger(CatalogueAction.class);
	private static final String DYNAMIC = "Dynamic";
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{

		logger.info("GenerateCatalogueAction::execute:ENTER");
		
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		HttpSession oSession =request.getSession(true);
		String sURI=null;
		String sPath=null;
		String sProdPreviewXmlPath = "";
		String sDestPath = null;
		String sE_CatalogueFileName = null;
		String sGenCatalogueBy = null;
		try
		{
			// System.out.println("CatalogueAction::execute:ENTER");	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			sDestPath = System.getProperty("GenerateXmlPath").trim();
			sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
			sGenCatalogueBy = System.getProperty("GenCatalogue").trim();
			if(DYNAMIC.equalsIgnoreCase(sGenCatalogueBy)) {
				sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
			}else {
				sProdPreviewXmlPath = System.getProperty("ProdPreviewStaticXMLPath").trim();
			}
			String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
			String sServerPath = getServlet().getServletContext().getRealPath("");

			oGenerateCatalogue.generateCatalogueXML(sDestPath,"Admin","0","Admin",sServerPath,"","","DEMONSTRATIONCATALOGUE","","");
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
			
			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("eCatalogueFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_"+"Admin"+".xml");
			oSession.setAttribute("eCatalogueCoverFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_cover_"+"Admin"+".xml");
			oSession.setAttribute("eCataloguePartnerFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_partner_"+"Admin"+".xml");
			
			// System.out.println("GenerateCatalogueAction::execute:EXit");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error((new StringBuilder("GenerateCatalogueAction::generateCatalogueXml:Exception ")).append(e.getMessage()).toString());
		}
		
		logger.info("GenerateCatalogueAction::execute:EXIT");
		return mapping.findForward("previewAdminDetails");
	}
}
