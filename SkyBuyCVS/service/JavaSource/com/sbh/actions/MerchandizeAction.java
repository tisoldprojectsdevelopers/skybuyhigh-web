package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.dao.MerchandizeDAO.getAllMerchandizeDetailsWithPriorityOfMerchandize;
import static com.sbh.util.Utils.isAnyRecurrencePriority;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.sbh.dao.AirlineDAO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.ImageScale;
import com.sbh.dao.MerchandizeDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.dao.VendorDAO;
import com.sbh.forms.PartnerPrioritizeForm;
import com.sbh.forms.ProductDetailsForm;
import com.sbh.forms.SearchMerchandisePriorityForm;
import com.sbh.forms.SearchMerchandizeForm;
import com.sbh.forms.SetPriorityMerchandizeForm;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;
import com.sbh.vo.AirlineRegVO;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CommentsVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.VendorDetailsVO;

public class MerchandizeAction extends DispatchAction
{
	private static Logger logger = LogManager.getLogger(MerchandizeAction.class);

	public ActionForward initSearchMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("MerchandizeAction::initSearchMerchandize:ENTER");    	  
		String sFwrdKey="initSearchMerchandizeSuccess";    	
		HttpSession oSession = request.getSession();
		try{		
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();
			request.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFwrdKey="initSearchApproveMerchandizeSuccess";	
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandizeForm");

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initSearchMerchandize:Exception "+e.getMessage());
		}
		logger.info("MerchandizeAction::initSearchMerchandize:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}

	public ActionForward searchMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::searchMerchandize:ENTER");
		String sFrdKey="searchMerchandizeSuccess";
		StringTokenizer oStringTokenizer=null;  
		List<ProductDetailsVO> alMerchandizeInfo = null;	
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		HttpSession oSession = request.getSession();
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null,sProductType=null;
		try{
			//For Approving the merchandize details
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			if(sMerchandizeAction.trim().length()==0)
				sMerchandizeAction=(String)oSession.getAttribute("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			oSession.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFrdKey="searchApproveMerchandizeSuccess";	

			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus ==null?"":sApprovalStatus.trim();	

			if(oSearchMerchandizeForm!=null){	
				sSearchBy=oSearchMerchandizeForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				sSearchId=sSearchBy;
				oStringTokenizer=new StringTokenizer(sSearchId,":");
				if(oStringTokenizer.hasMoreTokens())
					sOwner=oStringTokenizer.nextToken();
				sOwner=sOwner==null?"":sOwner.trim();
				if(oStringTokenizer.hasMoreTokens())
					sSearchId=oStringTokenizer.nextToken();
				sSearchId=sSearchId==null?"":sSearchId.trim();
				oSearchMerchandizeForm.setSearchBy(sSearchId);
				
				sSearchValue=oSearchMerchandizeForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchMerchandizeForm.setSearchValue(sSearchValue);
				
				sCategory = oSearchMerchandizeForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchMerchandizeForm.setCategory(sCategory);
				
				sProductType = oSearchMerchandizeForm.getProductType();
				sProductType = sProductType==null?"":sProductType.trim();
				oSearchMerchandizeForm.setProductType(sProductType);
					
				sEffectiveDate = oSearchMerchandizeForm.getEffectiveDate();
				sEffectiveDate=sEffectiveDate==null?"":sEffectiveDate.trim();
				oSearchMerchandizeForm.setEffectiveDate(sEffectiveDate);

				if(sApprovalStatus.trim().length()>0){
					oSearchMerchandizeForm.setSbhStatus(sApprovalStatus);	
					oSearchMerchandizeForm.setCategory("All");
					oSearchMerchandizeForm.setSearchBy("All");
					oSearchMerchandizeForm.setSearchValue("");
					oSearchMerchandizeForm.setProdStatus("A");
				}
			}				

			oSearchMerchandizeForm= MerchandizeDAO.getMerchandizeDetails(oSearchMerchandizeForm,sOwner,sMerchandizeAction);
			oSearchMerchandizeForm.setSearchBy(sSearchBy);
			if(oSearchMerchandizeForm.getRecordSet()!=null && oSearchMerchandizeForm.getRecordSet().size()>0){
				request.setAttribute("merchandizeInfo", alMerchandizeInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  

			if(sApprovalStatus.trim().length()>0){
				oSearchMerchandizeForm.setSearchBy("Admin:All");
			}

			logger.info("MerchandizeAction::searchMerchandize:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::searchMerchandize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward initEditSearchMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("MerchandizeAction::initEditSearchMerchandize:ENTER");    	  
		String sFwrdKey="initSearchMerchandizeSuccess";    	
		HttpSession oSession = request.getSession(true);
		try{		
			/*String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();
			request.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFwrdKey="initSearchApproveMerchandizeSuccess";	*/
			
			oSession.removeAttribute("searchMerchandizeForm");

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initEditSearchMerchandize:Exception "+e.getMessage());
		}
		logger.info("MerchandizeAction::initEditSearchMerchandize:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}

	public ActionForward editSearchMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::editSearchMerchandize:ENTER");
		String sFrdKey="searchMerchandizeSuccess";
		List<ProductDetailsVO> alMerchandizeInfo = null;	
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try{
			/*//For Approving the merchandize details
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			if(sMerchandizeAction.trim().length()==0)
				sMerchandizeAction=(String)oSession.getAttribute("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			oSession.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFrdKey="searchApproveMerchandizeSuccess";	*/

			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus ==null?"":sApprovalStatus.trim();


			if(oSearchMerchandizeForm!=null){	
				sSearchBy=oSearchMerchandizeForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				/*sSearchId=sSearchBy;
				oStringTokenizer=new StringTokenizer(sSearchId,":");
				if(oStringTokenizer.hasMoreTokens())
					sOwner=oStringTokenizer.nextToken();
				sOwner=sOwner==null?"":sOwner.trim();
				if(oStringTokenizer.hasMoreTokens())
					sSearchId=oStringTokenizer.nextToken();
				sSearchId=sSearchId==null?"":sSearchId.trim();*/
				oSearchMerchandizeForm.setSearchBy(sSearchBy);
				sSearchValue=oSearchMerchandizeForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchMerchandizeForm.setSearchValue(sSearchValue);
				sCategory = oSearchMerchandizeForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchMerchandizeForm.setCategory(sCategory);

				sEffectiveDate = oSearchMerchandizeForm.getEffectiveDate();
				sEffectiveDate=sEffectiveDate==null?"":sEffectiveDate.trim();
				oSearchMerchandizeForm.setEffectiveDate(sEffectiveDate);	

				if(sApprovalStatus.trim().length()>0){
					oSearchMerchandizeForm.setSbhStatus(sApprovalStatus);
					oSearchMerchandizeForm.setCategory("All");
					oSearchMerchandizeForm.setSearchBy("All");
					oSearchMerchandizeForm.setSearchValue("");
					oSearchMerchandizeForm.setProdStatus("All");

					Date dFromDate = new java.util.Date();
					dFromDate.setDate(dFromDate.getDate()-7);					
					oSearchMerchandizeForm.setUploadFromDate(dateFormat.format(dFromDate));			
					
					
					oSearchMerchandizeForm= MerchandizeDAO.getLastWeekUpdatedMerchandizeDetails(oSearchMerchandizeForm);
					
				}
			}				
			if(sApprovalStatus.trim().length()<=0){
				oSearchMerchandizeForm= MerchandizeDAO.getEditSearchMerchandizeDetails(oSearchMerchandizeForm);
			}
			if(oSearchMerchandizeForm.getRecordSet()!=null && oSearchMerchandizeForm.getRecordSet().size()>0){
				request.setAttribute("merchandizeInfo", alMerchandizeInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  

			logger.info("MerchandizeAction::editSearchMerchandize:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::editSearchMerchandize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward updateMerchandizeStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::updateMerchandizeStatus:ENTER");
		String sFrdKey="searchMerchandizeSuccess";
		String sSkyBuyLogoPath=null,sHMKey=null,sProdStatus=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form;  
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		List<CategoryVO> alCategory = null;
		List<ProductDetailsVO> alMerchandizeInfo = null;
		HashMap o_hmProdDetails=null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");
			alCategory = (List<CategoryVO>) oSession.getServletContext().getAttribute("Category");
			
			alMerchandizeInfo=(ArrayList)oSearchMerchandizeForm.getRecordSet();
			o_hmProdDetails=new HashMap();

			HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateMerchandizeStatus(alMerchandizeInfo,oLoginVO);

			System.out.print(hmMerchandizeDetails);
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			StringTokenizer oStringTokenizer=null;
			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=null;
			if(hmMerchandizeDetails != null && hmMerchandizeDetails.size() > 0){

				Iterator itr = hmMerchandizeDetails.entrySet().iterator();
				while(itr.hasNext()){
					Map.Entry oEntry = (Map.Entry)itr.next();
					sHMKey = (String)oEntry.getKey();
					alHMValue = (ArrayList) oEntry.getValue();

					oStringTokenizer=new StringTokenizer(sHMKey,"_");
					while(oStringTokenizer.hasMoreTokens()){
						sProdStatus=oStringTokenizer.nextToken();
					}
					sProdStatus=sProdStatus==null?"":sProdStatus.trim();

					//Header
					p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
					p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
					//SkyBuy Address
					p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
					p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
					p_hmTags.put("{{Phone}}",sSkyBuyPh);

					/*if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}*/
					if(alHMValue!=null && alHMValue.size()>0){
						MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
					}

				}
			}

			logger.info("MerchandizeAction::updateMerchandizeStatus:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updateMerchandizeStatus:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward editMerchandizeDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::editMerchandizeDetails:ENTER");
		String sFrdKey="editMerchandizeSuccess";
		ProductDetailsVO oProductDetailsVO = null;	
		List<CategoryVO> alCategory = null;
		String sImageViewPath = null,sCateFolderName = null;
		String sSourceOfEdit = null;
		HttpSession oSession = request.getSession();	
		ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form; 		
		LoginVO oLoginVO = null;
		try{
			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId=request.getParameter("ProdId");
			sSourceOfEdit = request.getParameter("SourceOfEdit");
			sSourceOfEdit = sSourceOfEdit==null?"":sSourceOfEdit.trim();
			
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			//sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory=(ArrayList<CategoryVO>)oSession.getServletContext().getAttribute("Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,oLoginVO.getUserType());

			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
				if(oProductDetailsVO.getView1ImgType()!=null && oProductDetailsVO.getView1ImgType().trim().length()>0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if(oProductDetailsVO.getView2ImgType()!=null && oProductDetailsVO.getView2ImgType().trim().length()>0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if(oProductDetailsVO.getView3ImgType()!=null && oProductDetailsVO.getView3ImgType().trim().length()>0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");

				BeanUtils.copyProperties(oProductDetailsForm,oProductDetailsVO); 
				oSession.setAttribute("merchandizeDetails",oProductDetailsVO);				
				request.setAttribute("mode","ProductEdit" );
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
			} 
			logger.info("MerchandizeAction::editMerchandizeDetails:EXIT");
		}catch(Exception e){		
			 
			
			
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::editMerchandizeDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward updateMerchandizeDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::updateMerchandizeDetails:ENTER");

		String sFrdKey="updateMerchandizeSuccess";
		String sLongDescription = "";
		String sShortDescription = "";
		boolean isChanged = false;
		HttpSession oSession = request.getSession();	
		List<CategoryVO> alCategory = null;
		ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form; 		
		String sImageName = null;
		LoginVO oLoginVO = null;
		ProductDetailsVO oProductDetailsVO =new ProductDetailsVO();
		ProductDetailsVO  oOldProductDetailsVO =null;
		ArrayList alImageInfo = null;
		List<CommentsVO> alProdComments = null;
		String sCateFolderName =null,sOldCateFolderName = null,sInShopStatus=null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
		boolean bIsProdCodeExist = false;
		try{
			alCategory=(ArrayList<CategoryVO>)oSession.getServletContext().getAttribute("Category");
			
			oProductDetailsVO =(ProductDetailsVO) oSession.getAttribute("merchandizeDetails");
			oOldProductDetailsVO = CatalogueDAO.getProductDetails(oProductDetailsForm.getProdId(),oLoginVO.getUserType());
			/*oOldProductDetailsVO = new ProductDetailsVO();
			BeanUtils.copyProperties(oOldProductDetailsVO, oProductDetailsVO);*/

			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oProductDetailsVO.getOwnerType(), oProductDetailsVO.getOwnerId(), oProductDetailsForm.getProdCode(),oProductDetailsForm.getProdId());
			if(!bIsProdCodeExist){
			/*if(oProductDetailsForm.getValidFromDate()==null)
				oProductDetailsForm.setValidFromDate("");
			else
				oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());

			if(oProductDetailsForm.getValidToDate()==null)
				oProductDetailsForm.setValidToDate("");
			else
				oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());*/

			if(oProductDetailsForm.getInstructions()==null)
				oProductDetailsForm.setInstructions("");
			else
				oProductDetailsForm.setInstructions(oProductDetailsForm.getInstructions().trim());				

			if(oProductDetailsVO!=null){
				sInShopStatus = oProductDetailsVO.getInShopStatus();
				sInShopStatus = sInShopStatus ==null?"":sInShopStatus.trim();
				
				if(oProductDetailsVO.getSbhProdStatus()!=null)
					oProductDetailsForm.setSbhProdStatus(oProductDetailsVO.getSbhProdStatus());
				
				if(oProductDetailsVO.getCategoryDate()!=null)
					oProductDetailsForm.setCategoryDate(oProductDetailsVO.getCategoryDate());
			}
			String sImageUploadPath = System.getProperty("ImageUploadPath").trim();	
			String sImageViewPath = System.getProperty("ImageViewPath").trim();	

			String sOldCateId= oProductDetailsVO.getCateId();
			String sNewCateId= oProductDetailsForm.getCateId();
			String sOwnerId= oProductDetailsVO.getOwnerId();
			String sProdId = oProductDetailsVO.getProdId();
			String sOwnerType = oProductDetailsVO.getOwnerType();
			
			String sSrcImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sOwnerType+"_"+sOwnerId+"\\OriginalImg\\";
			sImageName = sOwnerId+sProdId;


			if(alCategory!=null && alCategory.size()>0){
				for(CategoryVO oCategoryVO: alCategory){
					if(oCategoryVO.getCateId()!=null){
						if(oCategoryVO.getCateId().equals(sOldCateId))
							sOldCateFolderName = oCategoryVO.getCateName();
						if(oCategoryVO.getCateId().equals(sNewCateId))
							sCateFolderName = oCategoryVO.getCateName();
					}					
				}
			}
			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
				sOldCateFolderName = "PrivateJetJaunts";
			}	
			FormFile oMainFormFile = oProductDetailsForm.getUploadMainImagePath();
			FormFile oView1FormFile = oProductDetailsForm.getUploadView1ImagePath();
			FormFile oView2FormFile = oProductDetailsForm.getUploadView2ImagePath();
			FormFile oView3FormFile = oProductDetailsForm.getUploadView3ImagePath();
			// System.out.println("sOwnerId:"+sOwnerId);
			// System.out.println("sProdId:"+sProdId);
			// System.out.println("sOldCateFolderName:"+sOldCateFolderName);
			// System.out.println("sCateFolderName:"+sCateFolderName);

			// Category Type is changed
			if(!sOldCateId.equals(sNewCateId)){				
				//Main Image
				if(oMainFormFile != null && !oMainFormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oMainFormFile,sImageViewPath,sImageUploadPath,sImageName,"main","Main",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setMainImgCap(oProductDetailsForm.getUploadMainImgCap());
					ImageScale.deleteProductImage(sImageUploadPath,"Main",sOldCateFolderName,sImageName,oProductDetailsVO.getMainImgType(),sOwnerType,sOwnerId);
				}else{			

					alImageInfo=ImageScale.moveProductImage(sImageUploadPath,"Main",sOldCateFolderName,sCateFolderName,sImageName,oProductDetailsVO.getMainImgType(),sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1));
					}
				}		

				//View1 Image
				if(oView1FormFile != null && !oView1FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView1FormFile,sImageViewPath,sImageUploadPath,sImageName,"view1","View1",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView1ImgCap(oProductDetailsForm.getUploadView1ImgCap());
					ImageScale.deleteProductImage(sImageUploadPath,"View1",sOldCateFolderName,sImageName,oProductDetailsVO.getView1ImgType(),sOwnerType,sOwnerId);
				}else{	
					if(oProductDetailsForm.getDeleteView1Img()==null){
						alImageInfo=ImageScale.moveProductImage(sImageUploadPath,"View1",sOldCateFolderName,sCateFolderName,sImageName,oProductDetailsVO.getView1ImgType(),sOwnerType,sOwnerId);
						if(alImageInfo !=null){
							oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
							oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1));
						}
					}else if(oProductDetailsForm.getDeleteView1Img().equals("yes")){
						ImageScale.deleteProductImage(sImageUploadPath,"View1",sOldCateFolderName,sImageName,oProductDetailsVO.getView1ImgType(),sOwnerType,sOwnerId);
						oProductDetailsVO.setView1ImgType("");
						oProductDetailsVO.setView1ImgPath("");
					}
				}	

				//View2 Image
				if(oView2FormFile != null && !oView2FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView2FormFile,sImageViewPath,sImageUploadPath,sImageName,"view2","View2",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView2ImgCap(oProductDetailsForm.getUploadView2ImgCap());
					ImageScale.deleteProductImage(sImageUploadPath,"View2",sOldCateFolderName,sImageName,oProductDetailsVO.getView2ImgType(),sOwnerType,sOwnerId);
				}else{
					if(oProductDetailsForm.getDeleteView2Img()==null){
						alImageInfo=ImageScale.moveProductImage(sImageUploadPath,"View2",sOldCateFolderName,sCateFolderName,sImageName,oProductDetailsVO.getView2ImgType(),sOwnerType,sOwnerId);
						if(alImageInfo !=null){
							oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
							oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1));
						}
					}else if(oProductDetailsForm.getDeleteView2Img()!=null && oProductDetailsForm.getDeleteView2Img().equals("yes")){
						ImageScale.deleteProductImage(sImageUploadPath,"View2",sOldCateFolderName,sImageName,oProductDetailsVO.getView2ImgType(),sOwnerType,sOwnerId);
						oProductDetailsVO.setView2ImgType("");
						oProductDetailsVO.setView2ImgPath("");
					}
				}	

				//View3 Image
				if(oView3FormFile != null && !oView3FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView3FormFile,sImageViewPath,sImageUploadPath,sImageName,"view3","View3",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView3ImgCap(oProductDetailsForm.getUploadView3ImgCap());
					ImageScale.deleteProductImage(sImageUploadPath,"View3",sOldCateFolderName,sImageName,oProductDetailsVO.getView3ImgType(),sOwnerType,sOwnerId);
				}else{	
					if(oProductDetailsForm.getDeleteView3Img()==null){
						alImageInfo=ImageScale.moveProductImage(sImageUploadPath,"View3",sOldCateFolderName,sCateFolderName,sImageName,oProductDetailsVO.getView3ImgType(),sOwnerType,sOwnerId);
						if(alImageInfo !=null){
							oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
							oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1));
						}
					}else if(oProductDetailsForm.getDeleteView3Img()!=null && oProductDetailsForm.getDeleteView3Img().equals("yes")){
						ImageScale.deleteProductImage(sImageUploadPath,"View3",sOldCateFolderName,sImageName,oProductDetailsVO.getView3ImgType(),sOwnerType,sOwnerId);
						oProductDetailsVO.setView3ImgType("");
						oProductDetailsVO.setView3ImgPath("");
					}
				}		

			}//Catrgory is same
			else{	

				//Create Large,thumb,medium images
				if(oMainFormFile!= null && !oMainFormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oMainFormFile,sImageViewPath,sImageUploadPath,sImageName,"main","Main",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setMainImgCap(oProductDetailsForm.getUploadMainImgCap());
				}

				if(oView1FormFile!= null && !oView1FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView1FormFile,sImageViewPath,sImageUploadPath,sImageName,"view1","View1",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView1ImgCap(oProductDetailsForm.getUploadView1ImgCap());
				}else if(oProductDetailsForm.getDeleteView1Img()!= null && oProductDetailsForm.getDeleteView1Img().equals("yes")){
					ImageScale.deleteProductImage(sImageUploadPath,"View1",sOldCateFolderName,sImageName,oProductDetailsVO.getView1ImgType(),sOwnerType,sOwnerId);
					oProductDetailsVO.setView1ImgType("");
					oProductDetailsVO.setView1ImgPath("");
				}

				if(oView2FormFile!= null && !oView2FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView2FormFile,sImageViewPath,sImageUploadPath,sImageName,"view2","View2",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView2ImgCap(oProductDetailsForm.getUploadView2ImgCap());
				}else if(oProductDetailsForm.getDeleteView2Img()!= null && oProductDetailsForm.getDeleteView2Img().equals("yes")){
					ImageScale.deleteProductImage(sImageUploadPath,"View2",sOldCateFolderName,sImageName,oProductDetailsVO.getView2ImgType(),sOwnerType,sOwnerId);
					oProductDetailsVO.setView2ImgType("");
					oProductDetailsVO.setView2ImgPath("");
				}

				if(oView3FormFile!= null && !oView3FormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createProductImage(oView3FormFile,sImageViewPath,sImageUploadPath,sImageName,"view3","View3",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
						oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1));
					}
					oProductDetailsVO.setView3ImgCap(oProductDetailsForm.getUploadView3ImgCap());
				}else if(oProductDetailsForm.getDeleteView3Img()!= null && oProductDetailsForm.getDeleteView3Img().equals("yes")){
					ImageScale.deleteProductImage(sImageUploadPath,"View3",sOldCateFolderName,sImageName,oProductDetailsVO.getView3ImgType(),sOwnerType,sOwnerId);
					oProductDetailsVO.setView3ImgType("");
					oProductDetailsVO.setView3ImgPath("");
				}
			}

			if(oProductDetailsForm!=null){

				if(oProductDetailsVO.getMainImgType()==null || !(oProductDetailsVO.getMainImgType().trim().length()>0))
					oProductDetailsForm.setInShopStatus("I");

				int iProdId = CatalogueDAO.updateProductDetails(oProductDetailsForm,oProductDetailsVO,oLoginVO);
				if(oProductDetailsForm.getSbhComment()!=null && !oProductDetailsForm.getSbhComment().equalsIgnoreCase(""))
					MerchandizeDAO.addProductComments(oProductDetailsForm.getOwnerId(), oProductDetailsVO.getOwnerType(), oProductDetailsForm.getProdId(), oProductDetailsForm.getSbhComment(), oLoginVO.getUserId());
				String sShortDesc =oProductDetailsForm.getShortDesc();
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDescription = sShortDesc;
				sShortDesc = sShortDesc.replaceAll(" ","&nbsp;");
				oProductDetailsForm.setShortDesc(sShortDesc);				

				String sLongDesc =oProductDetailsForm.getLongDesc();
				sLongDesc = sLongDesc ==null?"":sLongDesc.trim();
				sLongDescription = sLongDesc;
				sLongDesc = sLongDesc.replaceAll(" ","&nbsp;");
				oProductDetailsForm.setLongDesc(sLongDesc);	

				
				alProdComments = MerchandizeDAO.getProductComments(oProductDetailsForm.getOwnerId(), oProductDetailsVO.getOwnerType(), oProductDetailsForm.getProdId(), oLoginVO.getUserType());

				if(alProdComments!=null && alProdComments.size()>0){
					request.setAttribute("productComments", alProdComments);
				}
			}

			//if vednor change any product information send a mail To Admin and CC to vendor 			
			CatalogueDAO.sendEditProdInfoByEmail(oProductDetailsForm,oProductDetailsVO,oOldProductDetailsVO,oLoginVO, oLoginVO.getUserId(),"",sCateFolderName);

			//send a mail to vendor if the product status is chaged from active to inactive
			if(!sInShopStatus.equals(oProductDetailsForm.getInShopStatus())){
				CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,oProductDetailsVO,oLoginVO, oLoginVO.getUserId(),"",sCateFolderName);
			}

			BeanUtils.copyProperties(oProductDetailsVO, oProductDetailsForm);
			
			oSession.setAttribute("merchandizeDetails",oProductDetailsVO);
			/**** Update Product audit details ****/
			CatalogueDAO.insertProductAuditTrail(oProductDetailsForm,oOldProductDetailsVO,oLoginVO);
			
			//CatalogueDAO.getUpdatedItemDetails(oProductDetailsForm,oOldProductDetailsVO,oLoginVO);

			// Generating Product Catalogue Xml 
			/*String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sProdXmlContent =oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),sOwnerId,oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
			String xmlContent = sProdXmlContent.replaceAll("\"", "&quot;");
			// System.out.println("xmlContent : "+xmlContent);
			oSession.setAttribute("ProductXmlContent", xmlContent);
			 */

			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();			
			String sServerPath = getServlet().getServletContext().getRealPath("");

			oGenerateCatalogue.generateCatalogueXML(sDestPath,oLoginVO.getUserType(),sOwnerId,oLoginVO.getUserId(),sServerPath,oProductDetailsVO.getProdId(),oProductDetailsVO.getCateId(),"PRODCATALOGUE","",oProductDetailsVO.getSeqId());
			String sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
			String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
			
			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("eCatalogueProdFileName", sE_CatalogueFileName+"_Prod_"+oLoginVO.getUserId()+".xml");
			
			if(sLongDescription != null && sLongDescription.trim().length() > 0) {
				oProductDetailsVO.setLongDesc(sLongDescription);
				isChanged = true;
			}
			if(sShortDescription != null && sShortDescription.trim().length() > 0) {
				oProductDetailsVO.setShortDesc(sShortDescription);
				isChanged = true;
			}
			if(isChanged) {
				oSession.setAttribute("merchandizeDetails",oProductDetailsVO);
			}
			}else{
				sFrdKey=("updateProductFailure");
				request.setAttribute("ErrMsg", "Item Code already exist.");
			}
			response.setHeader("cache-control", "no-cache");
			response.setHeader("pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			
			logger.info("MerchandizeAction::updateMerchandizeDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updateMerchandizeDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward initSearchAirlineAdvt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("MerchandizeAction::initSearchAirlineAdvt:ENTER");    	  
		String sFwrdKey="initSearchApproveAirlineAdvtSuccess";    	
		HttpSession oSession = request.getSession();
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		try{		
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();
			request.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFwrdKey="initSearchApproveAirlineAdvtSuccess";	
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandizeForm");

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initSearchAirlineAdvt:Exception "+e.getMessage());
		}
		logger.info("MerchandizeAction::initSearchAirlineAdvt:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}

	public ActionForward searchAirlineAdvt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::searchAirlineAdvt:ENTER");
		String sFrdKey="initSearchApproveAirlineAdvtSuccess";
		StringTokenizer oStringTokenizer=null;  
		List<ProductDetailsVO> alMerchandizeInfo = null;	
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		HttpSession oSession = request.getSession();
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null;
		try{
			//For Approving the merchandize details
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			if(sMerchandizeAction.equals("Approve"))
				sFrdKey="initSearchApproveAirlineAdvtSuccess";	

			if(sMerchandizeAction.trim().length()==0)
				sMerchandizeAction=(String)oSession.getAttribute("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			oSession.setAttribute("MerchandizeAction", sMerchandizeAction);


			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus ==null?"":sApprovalStatus.trim();	

			if(oSearchMerchandizeForm!=null){	
				sSearchBy=oSearchMerchandizeForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				sSearchId=sSearchBy;
				oStringTokenizer=new StringTokenizer(sSearchId,":");
				if(oStringTokenizer.hasMoreTokens())
					sOwner=oStringTokenizer.nextToken();
				sOwner=sOwner==null?"":sOwner.trim();
				if(oStringTokenizer.hasMoreTokens())
					sSearchId=oStringTokenizer.nextToken();
				sSearchId=sSearchId==null?"":sSearchId.trim();
				oSearchMerchandizeForm.setSearchBy(sSearchId);
				sSearchValue=oSearchMerchandizeForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchMerchandizeForm.setSearchValue(sSearchValue);
				sCategory = oSearchMerchandizeForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchMerchandizeForm.setCategory(sCategory);

				sEffectiveDate = oSearchMerchandizeForm.getEffectiveDate();
				sEffectiveDate=sEffectiveDate==null?"":sEffectiveDate.trim();
				oSearchMerchandizeForm.setEffectiveDate(sEffectiveDate);

				if(sApprovalStatus.trim().length()>0){
					oSearchMerchandizeForm.setSbhStatus(sApprovalStatus);	
					oSearchMerchandizeForm.setCategory("All");
					oSearchMerchandizeForm.setSearchBy("All");
					oSearchMerchandizeForm.setSearchValue("");
					oSearchMerchandizeForm.setProdStatus("A");
				}
			}				

			oSearchMerchandizeForm= MerchandizeDAO.getAirlineAdvtDetails(oSearchMerchandizeForm,sOwner,sMerchandizeAction);
			oSearchMerchandizeForm.setSearchBy(sSearchBy);
			if(oSearchMerchandizeForm.getRecordSet()!=null && oSearchMerchandizeForm.getRecordSet().size()>0){
				request.setAttribute("merchandizeInfo", alMerchandizeInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  

			if(sApprovalStatus.trim().length()>0){
				oSearchMerchandizeForm.setSearchBy("Admin:All");
			}


			logger.info("MerchandizeAction::searchAirlineAdvt:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::searchAirlineAdvt:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward updateAirlineAdvtStatus(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::updateAirlineAdvtStatus:ENTER");
		String sFrdKey="SearchAirlineAdvtSuccess";
		String sSkyBuyLogoPath=null,sHMKey=null,sProdStatus=null,sEmailCaptionForRejection=null,sEmailCaptionForAcceptance=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		List<CategoryVO> alCategory = null;
		List<ProductDetailsVO> alMerchandizeInfo = null;	
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form;  
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		HashMap o_hmProdDetails=null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");
			alCategory = (List<CategoryVO>) oSession.getServletContext().getAttribute("Category");
			
			alMerchandizeInfo=(ArrayList)oSearchMerchandizeForm.getRecordSet();
			o_hmProdDetails=new HashMap();

			HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateAirlineAdvtStatus(alMerchandizeInfo,oLoginVO);

			System.out.print(hmMerchandizeDetails);
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			StringTokenizer oStringTokenizer=null;
			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=null;
			if(hmMerchandizeDetails != null && hmMerchandizeDetails.size() > 0){

				Iterator itr = hmMerchandizeDetails.entrySet().iterator();
				while(itr.hasNext()){
					Map.Entry oEntry = (Map.Entry)itr.next();
					sHMKey = (String)oEntry.getKey();
					alHMValue = (ArrayList) oEntry.getValue();

					oStringTokenizer=new StringTokenizer(sHMKey,"_");
					while(oStringTokenizer.hasMoreTokens()){
						sProdStatus=oStringTokenizer.nextToken();
					}
					sProdStatus=sProdStatus==null?"":sProdStatus.trim();

					//Header
					p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
					p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
					//SkyBuy Address
					p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
					p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
					p_hmTags.put("{{Phone}}",sSkyBuyPh);

					if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}
					if(alHMValue!=null && alHMValue.size()>0){
						MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
					}

				}
			}


			/*String[] sToAddr=new String[2];

			sToAddr[0]="manikandan@thapovan-inc.com";
			sToAddr[1]="manikandan@thapovan-inc.com";
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom("manikandan@thapovan-inc.com");
			oEmailLogVo.setEmailTo(sToAddr);
			oEmailLogVo.setEmailSubject("Test Mail");
			oEmailLogVo.setEmailText("SkyBuy High Test Email");

			oEmail.sendEmail(oEmailLogVo);*/

			logger.info("MerchandizeAction::updateAirlineAdvtStatus:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updateAirlineAdvtStatus:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}


	public ActionForward initSearchAirlineAdvtDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("MerchandizeAction::initSearchAirlineAdvtDetails:ENTER");    	  
		String sFwrdKey="initSearchAirlineAdvtDetailsSuccess";    	
		HttpSession oSession = request.getSession();
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		try{		
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();
			//request.setAttribute("MerchandizeAction", sMerchandizeAction);
			oSession.removeAttribute("MerchandizeAction");
			if(sMerchandizeAction.equals("Approve"))
				sFwrdKey="initSearchAirlineAdvtDetailsSuccess";	
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandizeForm");

		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initSearchAirlineAdvtDetails:Exception "+e.getMessage());
		}
		logger.info("MerchandizeAction::initSearchAirlineAdvtDetails:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}

	public ActionForward searchAirlineAdvtDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::searchAirlineAdvtDetails:ENTER");
		String sFrdKey="initSearchAirlineAdvtDetailsSuccess";
		StringTokenizer oStringTokenizer=null;  
		ArrayList alMerchandizeInfo = null;	
		SearchMerchandizeForm oSearchMerchandizeForm=(SearchMerchandizeForm)form; 	
		HttpSession oSession = request.getSession();
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null;
		try{
			//For Approving the merchandize details
			String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			if(sMerchandizeAction.trim().length()==0)
				sMerchandizeAction=(String)oSession.getAttribute("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			if(sMerchandizeAction == null || sMerchandizeAction.equalsIgnoreCase("")){
				sMerchandizeAction = oSearchMerchandizeForm.getSbhStatus();
				sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();

			}
			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus ==null?"":sApprovalStatus.trim();	

			if(oSearchMerchandizeForm!=null){	
				sSearchBy=oSearchMerchandizeForm.getSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				sSearchId=sSearchBy;
				oStringTokenizer=new StringTokenizer(sSearchId,":");
				if(oStringTokenizer.hasMoreTokens())
					sOwner=oStringTokenizer.nextToken();
				sOwner=sOwner==null?"":sOwner.trim();
				if(oStringTokenizer.hasMoreTokens())
					sSearchId=oStringTokenizer.nextToken();
				sSearchId=sSearchId==null?"":sSearchId.trim();
				oSearchMerchandizeForm.setSearchBy(sSearchId);
				sSearchValue=oSearchMerchandizeForm.getSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchMerchandizeForm.setSearchValue(sSearchValue);
				sCategory = oSearchMerchandizeForm.getCategory();
				sCategory=sCategory==null?"":sCategory.trim();
				oSearchMerchandizeForm.setCategory(sCategory);

				sEffectiveDate = oSearchMerchandizeForm.getEffectiveDate();
				sEffectiveDate=sEffectiveDate==null?"":sEffectiveDate.trim();
				oSearchMerchandizeForm.setEffectiveDate(sEffectiveDate);

				if(sApprovalStatus.trim().length()>0){
					oSearchMerchandizeForm.setSbhStatus(sApprovalStatus);	
					oSearchMerchandizeForm.setCategory("All");
					oSearchMerchandizeForm.setSearchBy("All");
					oSearchMerchandizeForm.setSearchValue("");
					oSearchMerchandizeForm.setProdStatus("A");
				}
			}				

			//oSearchMerchandizeForm= MerchandizeDAO.getAirlineAdvtDetails(oSearchMerchandizeForm,sOwner,sMerchandizeAction);
			alMerchandizeInfo= MerchandizeDAO.getAdvertisementCatalogueDetails(oSearchMerchandizeForm,"0",ADMIN);

			oSearchMerchandizeForm.setSearchBy(sSearchBy);
			if(oSearchMerchandizeForm.getRecordSet()!=null && oSearchMerchandizeForm.getRecordSet().size()>0){
				request.setAttribute("merchandizeInfo", alMerchandizeInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  

			if(sApprovalStatus.trim().length()>0){
				oSearchMerchandizeForm.setSearchBy("Admin:All");
			}


			logger.info("MerchandizeAction::searchAirlineAdvtDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::searchAirlineAdvtDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward editAdvertisementDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::editAdvertisementDetails:ENTER");
		String sFrdKey="editAdvertisementSuccess";
		ProductDetailsVO oProductDetailsVO = null;	
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null,sCateFolderName = null;
		String sSourceOfEdit = null;
		HttpSession oSession = request.getSession();	
		ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form; 		
		LoginVO oLoginVO = null;
		try{
			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");	
			String sProdId=request.getParameter("ProdId");
			sSourceOfEdit = request.getParameter("SourceOfEdit");
			sSourceOfEdit = sSourceOfEdit == null?"":sSourceOfEdit.trim();
			
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			//sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());

			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
				if(oProductDetailsVO.getView1ImgType()!=null && oProductDetailsVO.getView1ImgType().trim().length()>0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if(oProductDetailsVO.getView2ImgType()!=null && oProductDetailsVO.getView2ImgType().trim().length()>0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if(oProductDetailsVO.getView3ImgType()!=null && oProductDetailsVO.getView3ImgType().trim().length()>0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");


				if(oProductDetailsVO.getSwfPath()!=null && oProductDetailsVO.getSwfPath().trim().length()>0)
					oProductDetailsVO.setSwfPath(sImageViewPath+oProductDetailsVO.getSwfPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSwfType());			
				else
					oProductDetailsVO.setSwfPath("");
				oSession.setAttribute("SwfFileName",oProductDetailsVO.getSwfPath());

				BeanUtils.copyProperties(oProductDetailsForm,oProductDetailsVO); 
				oSession.setAttribute("airlineAdvertisementDetails",oProductDetailsVO);				
				request.setAttribute("mode","ProductEdit" );
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
			} 
			logger.info("MerchandizeAction::editAdvertisementDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::editAdvertisementDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward acceptMerchandise(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::acceptMerchandise:ENTER");
		String sFrdKey="acceptMerchandizeSuccess";
		String sSkyBuyLogoPath=null,sHMKey=null,sHMValue=null,sProdStatus=null,sEmailCaptionForRejection=null,sEmailCaptionForAcceptance=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		ArrayList alMerchandizeInfo = null;	
		ProductDetailsVO oProductDetailsVO=null;
		HttpSession oSession = request.getSession();
		String sOwnerId =null;
		LoginVO oLoginVO = null;
		HashMap o_hmProdDetails=null;
		String sCateFolderName = "",sImageViewPath = "";
		ArrayList<ProductDetailsVO> alProductDetails=null;
		ArrayList<CategoryVO> alCategory = null;
		int iIsUpdate=-1;
		String sProdId = null,sSbhPrice = null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");	
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			sProdId = request.getParameter("prodId");
			sProdId = sProdId == null?"":sProdId.trim();
			
			sSbhPrice = request.getParameter("sbhPrice");
			sSbhPrice = sSbhPrice == null?"":sSbhPrice.trim();

			
			//HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateMerchandizeStatus(alMerchandizeInfo,oLoginVO);

			iIsUpdate=MerchandizeDAO.updateMerchandizeDetails(sProdId,sSbhPrice,"A",null,oLoginVO.getUserId());


			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			StringTokenizer oStringTokenizer=null;
			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=new ArrayList<ProductDetailsVO>();

			oProductDetailsVO =  CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());
			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=System.getProperty("ImageViewPath");
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
			}
			if("A".equalsIgnoreCase(oProductDetailsVO.getInShopStatus())){
				oProductDetailsVO.setInShopStatus("Active");
			}else{
				oProductDetailsVO.setInShopStatus("Inactive");
			}
			oProductDetailsVO.setEffectiveDate(oProductDetailsVO.getCategoryDate());
			alHMValue.add(oProductDetailsVO);
			if(sProdId != null){
				sProdStatus = oProductDetailsVO.getSbhProdStatus();
				sProdStatus=sProdStatus==null?"":sProdStatus.trim();

				//Header
				p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
				//SkyBuy Address
				p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
				p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
				p_hmTags.put("{{Phone}}",sSkyBuyPh);

				/*if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}*/
				if(alHMValue!=null && alHMValue.size()>0){
					MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
				}
			}


			/*String[] sToAddr=new String[2];

			sToAddr[0]="manikandan@thapovan-inc.com";
			sToAddr[1]="manikandan@thapovan-inc.com";
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom("manikandan@thapovan-inc.com");
			oEmailLogVo.setEmailTo(sToAddr);
			oEmailLogVo.setEmailSubject("Test Mail");
			oEmailLogVo.setEmailText("SkyBuy High Test Email");

			oEmail.sendEmail(oEmailLogVo);*/

			logger.info("MerchandizeAction::acceptMerchandise:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::acceptMerchandise:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward rejectMerchandise(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::rejectMerchandise:ENTER");
		String sFrdKey="acceptMerchandizeSuccess";
		String sSkyBuyLogoPath=null,sProdStatus=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		ProductDetailsVO oProductDetailsVO=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sCateFolderName = "",sImageViewPath = "";
		ArrayList<CategoryVO> alCategory = null;
		String sProdId = null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");	
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			sProdId = request.getParameter("prodId");
			sProdId = sProdId == null?"":sProdId.trim();

			//HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateMerchandizeStatus(alMerchandizeInfo,oLoginVO);

			MerchandizeDAO.updateMerchandizeDetails(sProdId,null,"R",null,oLoginVO.getUserId());


			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=new ArrayList<ProductDetailsVO>();

			oProductDetailsVO =  CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());
			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=System.getProperty("ImageViewPath");
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
			}
			if("A".equalsIgnoreCase(oProductDetailsVO.getInShopStatus())){
				oProductDetailsVO.setInShopStatus("Active");
			}else{
				oProductDetailsVO.setInShopStatus("Inactive");
			}
			oProductDetailsVO.setEffectiveDate(oProductDetailsVO.getCategoryDate());
			alHMValue.add(oProductDetailsVO);
			if(sProdId != null){
				sProdStatus = oProductDetailsVO.getSbhProdStatus();
				sProdStatus=sProdStatus==null?"":sProdStatus.trim();

				//Header
				p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
				//SkyBuy Address
				p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
				p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
				p_hmTags.put("{{Phone}}",sSkyBuyPh);

				/*if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}*/
				if(alHMValue!=null && alHMValue.size()>0){
					MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
				}
			}


			/*String[] sToAddr=new String[2];

			sToAddr[0]="manikandan@thapovan-inc.com";
			sToAddr[1]="manikandan@thapovan-inc.com";
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom("manikandan@thapovan-inc.com");
			oEmailLogVo.setEmailTo(sToAddr);
			oEmailLogVo.setEmailSubject("Test Mail");
			oEmailLogVo.setEmailText("SkyBuy High Test Email");

			oEmail.sendEmail(oEmailLogVo);*/

			logger.info("MerchandizeAction::rejectMerchandise:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::rejectMerchandise:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward acceptAirlinePackage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::acceptAirlinePackage:ENTER");
		String sFrdKey="acceptAirlineProductSuccess";
		String sSkyBuyLogoPath=null,sProdStatus=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		ProductDetailsVO oProductDetailsVO=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sCateFolderName = "",sImageViewPath = "";
		ArrayList<CategoryVO> alCategory = null;
		String sProdId = null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");	
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			sProdId = request.getParameter("prodId");
			sProdId = sProdId == null?"":sProdId.trim();

			//HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateMerchandizeStatus(alMerchandizeInfo,oLoginVO);

			MerchandizeDAO.updateMerchandizeDetails(sProdId,null,"A",null,oLoginVO.getUserId());


			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			StringTokenizer oStringTokenizer=null;
			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=new ArrayList<ProductDetailsVO>();

			oProductDetailsVO =  CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());
			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=System.getProperty("ImageViewPath");
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
			}
			if("A".equalsIgnoreCase(oProductDetailsVO.getInShopStatus())){
				oProductDetailsVO.setInShopStatus("Active");
			}else{
				oProductDetailsVO.setInShopStatus("Inactive");
			}
			oProductDetailsVO.setEffectiveDate(oProductDetailsVO.getCategoryDate());
			alHMValue.add(oProductDetailsVO);
			if(sProdId != null){
				sProdStatus = oProductDetailsVO.getSbhProdStatus();
				sProdStatus=sProdStatus==null?"":sProdStatus.trim();

				//Header
				p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
				//SkyBuy Address
				p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
				p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
				p_hmTags.put("{{Phone}}",sSkyBuyPh);

				/*if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}*/
				if(alHMValue!=null && alHMValue.size()>0){
					MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
				}


			}


			/*String[] sToAddr=new String[2];

			sToAddr[0]="manikandan@thapovan-inc.com";
			sToAddr[1]="manikandan@thapovan-inc.com";
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom("manikandan@thapovan-inc.com");
			oEmailLogVo.setEmailTo(sToAddr);
			oEmailLogVo.setEmailSubject("Test Mail");
			oEmailLogVo.setEmailText("SkyBuy High Test Email");

			oEmail.sendEmail(oEmailLogVo);*/

			logger.info("MerchandizeAction::acceptAirlinePackage:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::acceptAirlinePackage:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward rejectAirlinePackage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::rejectAirlinePackage:ENTER");
		String sFrdKey="acceptAirlineProductSuccess";
		String sSkyBuyLogoPath=null,sProdStatus=null;
		String sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		ProductDetailsVO oProductDetailsVO=null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sCateFolderName = "",sImageViewPath = "";
		ArrayList<CategoryVO> alCategory = null;
		String sProdId = null;
		try{	

			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");	
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");

			sProdId = request.getParameter("prodId");
			sProdId = sProdId == null?"":sProdId.trim();

			//HashMap hmMerchandizeDetails=(HashMap)MerchandizeDAO.updateMerchandizeStatus(alMerchandizeInfo,oLoginVO);

			MerchandizeDAO.updateMerchandizeDetails(sProdId,null,"R",null,oLoginVO.getUserId());


			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			/*sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();

			sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();*/

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			HashMap p_hmTags =new HashMap();
			ArrayList<ProductDetailsVO> alHMValue=new ArrayList<ProductDetailsVO>();

			oProductDetailsVO =  CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());
			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=System.getProperty("ImageViewPath");
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
			}
			if("A".equalsIgnoreCase(oProductDetailsVO.getInShopStatus())){
				oProductDetailsVO.setInShopStatus("Active");
			}else{
				oProductDetailsVO.setInShopStatus("Inactive");
			}
			oProductDetailsVO.setEffectiveDate(oProductDetailsVO.getCategoryDate());
			alHMValue.add(oProductDetailsVO);
			if(sProdId != null){
				sProdStatus = oProductDetailsVO.getSbhProdStatus();
				sProdStatus=sProdStatus==null?"":sProdStatus.trim();

				//Header
				p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				p_hmTags.put("{{Date}}", MerchandizeDAO.dateFormat(new Date()));
				//SkyBuy Address
				p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
				p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
				p_hmTags.put("{{Phone}}",sSkyBuyPh);

				/*if(sProdStatus.equalsIgnoreCase("A")){
						p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					}else{
						p_hmTags.put("{{Heading}}",sEmailCaptionForRejection);
					}*/
				if(alHMValue!=null && alHMValue.size()>0){
					MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null, null,sProdStatus,oLoginVO.getUserId(),"",alCategory);
				}


			}


			/*String[] sToAddr=new String[2];

			sToAddr[0]="manikandan@thapovan-inc.com";
			sToAddr[1]="manikandan@thapovan-inc.com";
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom("manikandan@thapovan-inc.com");
			oEmailLogVo.setEmailTo(sToAddr);
			oEmailLogVo.setEmailSubject("Test Mail");
			oEmailLogVo.setEmailText("SkyBuy High Test Email");

			oEmail.sendEmail(oEmailLogVo);*/

			logger.info("MerchandizeAction::rejectAirlinePackage:EXIT");
		}catch(Exception e){		
			 
			
			
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::rejectAirlinePackage:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	
	public ActionForward initSearchMerchandizePriority(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("MerchandizeAction::initSearchMerchandizePriority:ENTER");    	  
		String sFwrdKey="searchMerchandizePrioritySuccess";    	
		HttpSession oSession = request.getSession();
		SearchMerchandisePriorityForm oSearchMerchandizePriorityForm=(SearchMerchandisePriorityForm)form; 	
		try{		
			/*String sMerchandizeAction=(String)request.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null?"":sMerchandizeAction.trim();
			request.setAttribute("MerchandizeAction", sMerchandizeAction);
			if(sMerchandizeAction.equals("Approve"))
				sFwrdKey="initSearchApproveMerchandizeSuccess";	*/

			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandisePriorityForm");
		}catch(Exception e){
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initSearchMerchandizePriority:Exception "+e.getMessage());
		}
		logger.info("MerchandizeAction::initSearchMerchandizePriority:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}
	
	
	
	public ActionForward getMerchandizePriorityDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::getMerchandizePriorityDetails:ENTER");
		String sFrdKey="searchMerchandizePrioritySuccess";
		StringTokenizer oStringTokenizer=null;  
		ArrayList alMerchandizeInfo = null;	
		SearchMerchandisePriorityForm oSearchMerchandizePriorityForm=(SearchMerchandisePriorityForm)form; 	
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		ArrayList alMerchandiseDetails;
		String sOwnerId =null;
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try{
			
			alMerchandiseDetails = MerchandizeDAO.getMerchandizeDetailsWithPriority(oSearchMerchandizePriorityForm);
			if(alMerchandiseDetails!=null && alMerchandiseDetails.size()>0){
				oSearchMerchandizePriorityForm.setRecordSet(alMerchandiseDetails);
			}else{
				request.setAttribute("NoRecords", "No Records Found");
			}
			logger.info("MerchandizeAction::getMerchandizePriorityDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::getMerchandizePriorityDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	
	public ActionForward updateMerchandizePriorityDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::updateMerchandizePriorityDetails:ENTER");
		String sFrdKey="searchMerchandizePrioritySuccess";
		StringTokenizer oStringTokenizer=null;  
		ArrayList alMerchandizeInfo = null;
		ArrayList<ProductDetailsVO> alCoverflowInfo = null;
		SearchMerchandisePriorityForm oSearchMerchandizePriorityForm=(SearchMerchandisePriorityForm)form; 	
		HttpSession oSession = request.getSession();
		ArrayList<ProductDetailsVO> alProdDetails = null,alOldProdDetails = null,alProdDetailsCopy = null, alOldProdDetailsCopy = null;
		File fOrgImgFile = null;
		LoginVO oLoginVO = null;
		ArrayList alMerchandiseDetails = null;
		String sOwnerId =null,sImageUploadPath = null;
		boolean isDuplicatePriority = false, isAnyPriorityValueChanged = false;
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try{
			
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			sImageUploadPath = System.getProperty("ImageUploadPath");
			sImageUploadPath = sImageUploadPath==null?"":sImageUploadPath.trim();
			alProdDetails = oSearchMerchandizePriorityForm.getRecordSet();
			if(alProdDetails!= null){
				
				alProdDetailsCopy = new ArrayList<ProductDetailsVO>(alProdDetails);
			}
			
			//All products have been retrieved from the database.
			alOldProdDetails = MerchandizeDAO.getAllMerchandizeDetailsWithPriority();
			alOldProdDetailsCopy = alOldProdDetails;
			
			/*
			 * Newly updated product details has been update the changes in the existing products to
			 * check whether any recurrence is available or not.
			 */  
			for(ProductDetailsVO oProductDetailsVO:alOldProdDetailsCopy) {
				if(oProductDetailsVO.getProdId() !=null) {
					for(ProductDetailsVO oProductDetails:alProdDetails) {
						if(oProductDetailsVO.getProdId().equalsIgnoreCase(oProductDetails.getProdId())) {
							if(oProductDetailsVO.getCoverflowStatus() != null 
									&& !oProductDetailsVO.getCoverflowStatus().equalsIgnoreCase(oProductDetails.getCoverflowStatus())) {
								oProductDetailsVO.setCoverflowStatus(oProductDetails.getCoverflowStatus());
								isAnyPriorityValueChanged = true;
							}
						}
					}
				}
			}
			
			alProdDetails = alOldProdDetailsCopy;
			//To Sort the products by coverflow status in ascending order.
			alProdDetails = ProductDetailsVO.getProductsSortedByCoverflowStatus(alProdDetails);
			//To check if any duplicate entry is present or not.
			//isDuplicatePriority = Utils.isAnyDuplicatePriority(alProdDetails);
			/*
			 * If Duplicate entry is not available then save the coverflow status for the
			 * products. 
			 * Else don't save the status instead return a warning message as 
			 * 'Merchandise with this priority already set. Please choose a different priority.'  
			 */
			if(!isDuplicatePriority && isAnyPriorityValueChanged){
			
				alOldProdDetails = MerchandizeDAO.getAllMerchandizeDetailsWithPriority();
				
				for(ProductDetailsVO oProductDetailsVO:alProdDetails){
					for(ProductDetailsVO oOldProductDetailsVO: alOldProdDetails){
						if(oProductDetailsVO.getProdId().equals(oOldProductDetailsVO.getProdId())){
							if(!oProductDetailsVO.getCoverflowStatus().equals(oOldProductDetailsVO.getCoverflowStatus())){
								MerchandizeDAO.updateMerchandizePriority(oProductDetailsVO.getProdId(), oProductDetailsVO.getCoverflowStatus(),oLoginVO.getUserId());
							}
						}
					}
				}
				
				
				File dir = new File(sImageUploadPath+"/SkyBuyPics/CoverImages");
				String[] files = dir.list();
				
				alCoverflowInfo = MerchandizeDAO.getCoverflowMerchandise();

				for(ProductDetailsVO oCoverflowProdInfo:alCoverflowInfo){
					fOrgImgFile = new File(oCoverflowProdInfo.getOrignalImagePath());
					String sFileName = fOrgImgFile.getName();
					for (int i=0; i<files.length; i++){

						// Get filename of file or directory
						String filename = files[i];
						if(sFileName.equalsIgnoreCase(filename)){
							File f = new File(sImageUploadPath+"/SkyBuyPics/CoverImages/" + files[i]);
							//// System.out.println("C://incoming/" + children[i] +"/");
							boolean success= f.delete();
//							if(! success)
								// System.out.println("Fail to delete");
						}else{
							files[i] = "";
						}
					}
					ImageScale.createCoverflowImage(oCoverflowProdInfo.getOrignalImagePath(), sImageUploadPath);

				}
				alMerchandiseDetails = MerchandizeDAO.getMerchandizeDetailsWithPriority(oSearchMerchandizePriorityForm);
				oSearchMerchandizePriorityForm.setRecordSet(alMerchandiseDetails);
				
			}else{
				oSearchMerchandizePriorityForm.setRecordSet(alProdDetailsCopy);
				request.setAttribute("ErrorMsg", "Merchandise with this priority already set. Please choose a different priority.");
				
			}
			logger.info("MerchandizeAction::updateMerchandizePriorityDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updateMerchandizePriorityDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward initSetPriorityToMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::initSetPriorityToMerchandize:ENTER");
		
		String sFrdKey="initSetPriorityToMerchandizeSuccess";
		HttpSession oSession = request.getSession();	
		SetPriorityMerchandizeForm setPriorityMerchandizeForm = (SetPriorityMerchandizeForm)form;
		try{
			
			oSession.removeAttribute("setPriorityMerchandizeForm");
			logger.info("MerchandizeAction::initSetPriorityToMerchandize:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::initSetPriorityToMerchandize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward searchMerchandizeToSetPriority(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		
		logger.info("MerchandizeAction::searchMerchandizeToSetPriority:ENTER");
		String sFrdKey="searchMerchandizeToSetPrioritySuccess";
		StringTokenizer oStringTokenizer=null;  
		List<ProductDetailsVO> alMerchandizeInfo = null;	
		SetPriorityMerchandizeForm setPriorityMerchandizeForm = (SetPriorityMerchandizeForm)form; 	
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sOwnerId =null;
		String sSource = null;
		String sSearchBy=null,sSearchValue = null , sCategory =null,sEffectiveDate = null,sOwner=null,sSearchId=null;
		try{
			sSource = request.getParameter("source");
			
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			if(sSource!=null && sSource.equalsIgnoreCase("prioritizepartner")){
				if(setPriorityMerchandizeForm.getSearchBy()!=null && setPriorityMerchandizeForm.getSearchBy().equalsIgnoreCase("AIRLINENAME")){
					AirlineRegVO oNewAirlineRegVO = AirlineDAO.getAirlineDetailsById(setPriorityMerchandizeForm.getSearchValue(),oLoginVO.getId());
					setPriorityMerchandizeForm.setSearchValue(oNewAirlineRegVO.getAirName());
				}else if(setPriorityMerchandizeForm.getSearchBy()!=null && setPriorityMerchandizeForm.getSearchBy().equalsIgnoreCase("VENDORNAME")){
					VendorDetailsVO oVendorDetailsVO = VendorDAO.getVendorDetailsById(setPriorityMerchandizeForm.getSearchValue(),oLoginVO.getId());
					setPriorityMerchandizeForm.setSearchValue(oVendorDetailsVO.getVendName());
				}
			}
			if(setPriorityMerchandizeForm!=null){	
				setPriorityMerchandizeForm.setCategory(setPriorityMerchandizeForm.getCategory()==null?"":setPriorityMerchandizeForm.getCategory().trim());
				setPriorityMerchandizeForm.setSearchBy(setPriorityMerchandizeForm.getSearchBy()==null?"":setPriorityMerchandizeForm.getSearchBy().trim());
				setPriorityMerchandizeForm.setSearchValue(setPriorityMerchandizeForm.getSearchValue()==null?"":setPriorityMerchandizeForm.getSearchValue().trim());
			}				
			
			setPriorityMerchandizeForm = MerchandizeDAO.getProductDetailsToSetMerchandizePriority(setPriorityMerchandizeForm);
			if(setPriorityMerchandizeForm != null && setPriorityMerchandizeForm.getRecordSet() != null 
					&& setPriorityMerchandizeForm.getRecordSet().size() > 0) {
				request.setAttribute("merchandizeInfo", alMerchandizeInfo);
				
			}else{
				request.setAttribute("NoRecords","noRecords");
				
			}  
			
			logger.info("MerchandizeAction::searchMerchandizeToSetPriority:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::searchMerchandizeToSetPriority:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward updatePriorityOfMerchandize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		
		logger.info("MerchandizeAction::updatePriorityOfMerchandize:ENTER");
		
		boolean isAnyRecurrencePriority = false;
		boolean isAnyPriorityValueChanged = false;
		String sFrdKey="updatePriorityOfMerchandizeSuccess";
		int iPriorityOfMerchandize = 0;
		List<ProductDetailsVO> alProductDetailsVO = null;
		List<ProductDetailsVO> alOldProductDetailsVO = null;
		SetPriorityMerchandizeForm setPriorityMerchandizeForm = (SetPriorityMerchandizeForm)form;
		
		try{
			HttpSession oSession = request.getSession(true);
			LoginVO oLoginVO = null;
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			alProductDetailsVO = setPriorityMerchandizeForm.getRecordSet();
			ArrayList<ProductDetailsVO> alNewProductsCopy = new ArrayList<ProductDetailsVO>(alProductDetailsVO);
			
			alOldProductDetailsVO = getAllMerchandizeDetailsWithPriorityOfMerchandize();
			List<ProductDetailsVO> alOldProductsCopy = getAllMerchandizeDetailsWithPriorityOfMerchandize();
			
			//Newly updated product details has been saved the changes in the 
			for(ProductDetailsVO oProductDetails:alProductDetailsVO) {
				if(oProductDetails.getProdId() !=null) {
					for(ProductDetailsVO oProductDetailsVO:alOldProductDetailsVO) {
						if(oProductDetails.getProdId().equalsIgnoreCase(oProductDetailsVO.getProdId())) {
							if(oProductDetails.getPriorityOfMerchandize() != null 
									&& !oProductDetails.getPriorityOfMerchandize().equalsIgnoreCase(oProductDetailsVO.getPriorityOfMerchandize())) {
								oProductDetailsVO.setPriorityOfMerchandize(oProductDetails.getPriorityOfMerchandize());
								isAnyPriorityValueChanged = true;
							}
						}
					}
				}
			}
			
			//isAnyRecurrencePriority = isAnyRecurrencePriority(alOldProductDetailsVO);
			
			//if(!isAnyRecurrencePriority) {
				if(isAnyPriorityValueChanged) {
					for(ProductDetailsVO oProductDetailsVO:alNewProductsCopy){
						for(ProductDetailsVO oOldProductDetailsVO: alOldProductsCopy){
							if(oProductDetailsVO.getProdId().equals(oOldProductDetailsVO.getProdId())){
								if(!oProductDetailsVO.getPriorityOfMerchandize().equals(oOldProductDetailsVO.getPriorityOfMerchandize())){
									MerchandizeDAO.updatePriorityOfMerchandise(oProductDetailsVO,oLoginVO.getUserId());
								}
							}
						}
					}
					/*for(ProductDetailsVO oProductDetailsVO:setPriorityMerchandizeForm.getRecordSet()) {
						if(oProductDetailsVO != null && oProductDetailsVO.getPriorityOfMerchandize() != null 
								&& oProductDetailsVO.getPriorityOfMerchandize().trim().length() > 0) {
							iPriorityOfMerchandize = Integer.parseInt(oProductDetailsVO.getPriorityOfMerchandize());
							if(iPriorityOfMerchandize > 0) {
								MerchandizeDAO.updatePriorityOfMerchandise(oProductDetailsVO);
							}
						}
					}*/
				}
			/*}else {
				request.setAttribute("ErrorMsg", "Merchandise with this priority already set. Please choose a different priority.");
			}*/
			setPriorityMerchandizeForm = MerchandizeDAO.getProductDetailsToSetMerchandizePriority(setPriorityMerchandizeForm);
			
			logger.info("MerchandizeAction::updatePriorityOfMerchandize:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updatePriorityOfMerchandize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	
/* View Merchandise From Admin*/
	
	public ActionForward viewMerchandizeDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::viewMerchandizeDetails:ENTER");
		String sFrdKey="viewMerchandizeSuccess";
		ProductDetailsVO oProductDetailsVO = null;	
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null,sCateFolderName = null;
		HttpSession oSession = request.getSession();	
		ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form; 		
		LoginVO oLoginVO = null;
		List<CommentsVO> alProdComments = null;
		try{
			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId=request.getParameter("ProdId");			
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			//sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,oLoginVO.getUserType());

			if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)){
				if(alCategory!=null && alCategory.size()>0){
					for(CategoryVO oCategoryVO: alCategory){
						if(oCategoryVO.getCateId()!=null){
							if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}					
					}
				}
			}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase(AIRLINE)){
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath=sImageViewPath==null?"":sImageViewPath.trim();
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());			
				else
					oProductDetailsVO.setMainImgPath("");
				if(oProductDetailsVO.getView1ImgType()!=null && oProductDetailsVO.getView1ImgType().trim().length()>0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if(oProductDetailsVO.getView2ImgType()!=null && oProductDetailsVO.getView2ImgType().trim().length()>0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if(oProductDetailsVO.getView3ImgType()!=null && oProductDetailsVO.getView3ImgType().trim().length()>0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");

				BeanUtils.copyProperties(oProductDetailsForm,oProductDetailsVO); 
				oSession.setAttribute("merchandizeDetails",oProductDetailsVO);				
				request.setAttribute("mode","ProductEdit" );
				
				alProdComments = MerchandizeDAO.getProductComments(oProductDetailsForm.getOwnerId(), oProductDetailsVO.getOwnerType(), oProductDetailsForm.getProdId(), ADMIN);

				if(alProdComments!=null && alProdComments.size()>0){
					request.setAttribute("productComments", alProdComments);
				}
				
				
				
				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();			
				String sServerPath = getServlet().getServletContext().getRealPath("");
				GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
				oGenerateCatalogue.generateCatalogueXML(sDestPath,oLoginVO.getUserType(),oProductDetailsForm.getOwnerId(),oLoginVO.getUserId(),sServerPath,oProductDetailsVO.getProdId(),oProductDetailsVO.getCateId(),"PRODCATALOGUE","",oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
				
				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName", sE_CatalogueFileName+"_Prod_"+oLoginVO.getUserId()+".xml");
				
			} 
			logger.info("MerchandizeAction::viewMerchandizeDetails:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::viewMerchandizeDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward createMerchandiseXml(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::createMerchandiseXml:ENTER");
		String sFrdKey="viewMerchandizeSuccess";
		ProductDetailsVO oProductDetailsVO = null;	
		String sImageViewPath = null,sCateFolderName = null;
		HttpSession oSession = request.getSession();	
		LoginVO oLoginVO = null;
		try{
			oLoginVO =(LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId=request.getParameter("prodId");			
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			//sImagePath = getServlet().getServletContext().getRealPath("");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,oLoginVO.getUserType());
				
				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();			
				String sServerPath = getServlet().getServletContext().getRealPath("");
				GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
				oGenerateCatalogue.generateCatalogueXML(sDestPath,oLoginVO.getUserType(),oProductDetailsVO.getOwnerId(),oLoginVO.getUserId(),sServerPath,oProductDetailsVO.getProdId(),oProductDetailsVO.getCateId(),"PRODCATALOGUE","",oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
				
				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName", sE_CatalogueFileName+"_Prod_"+oLoginVO.getUserId()+".xml");
				
				String sIsExist = "<isExist>Yes</isExist>";
				response.setContentType("application/xml");
				PrintWriter pw = response.getWriter();
				pw.println(sIsExist);
				pw.flush();	
			
			logger.info("MerchandizeAction::createMerchandiseXml:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::createMerchandiseXml:EXCEPTION "+e.getMessage());
		}	
		return null;		
	}
	
	/* To Set Priority to Airline/Vendor(Partner)
	 * 
	 */
	
	public ActionForward initPartnerPriority(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("MerchandizeAction::initPartnerPriority:ENTER");
		
		String sFrdKey="searchPartnerPrioritySuccess";
		HttpSession oSession = request.getSession();	
		PartnerPrioritizeForm oPartnerFormForm = (PartnerPrioritizeForm)form;
		try{
			
			oSession.removeAttribute("partnerPrioritizeForm");
			logger.info("MerchandizeAction::initPartnerPriority:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::initPartnerPriority:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward searchPartnerPriority(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		
		logger.info("MerchandizeAction::searchPartnerPriority:ENTER");
		String sFrdKey="searchPartnerPrioritySuccess";
		PartnerPrioritizeForm oPartnerPriorityForm = (PartnerPrioritizeForm)form; 	
		try{
			
			if(oPartnerPriorityForm!=null){	
				oPartnerPriorityForm.setSearchValue(oPartnerPriorityForm.getSearchValue()==null?"":oPartnerPriorityForm.getSearchValue().trim());
			}				
			
			oPartnerPriorityForm = MerchandizeDAO.getPartnerDetails(oPartnerPriorityForm);
			if(oPartnerPriorityForm != null && oPartnerPriorityForm.getRecordSet() != null 
					&& oPartnerPriorityForm.getRecordSet().size() > 0) {
				request.setAttribute("partnerInfo", oPartnerPriorityForm.getRecordSet());
				
			}else{
				request.setAttribute("NoRecords","noRecords");
				
			}
			
			logger.info("MerchandizeAction::searchPartnerPriority:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::searchPartnerPriority:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	public ActionForward updatePartnerPrioritize(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		
		logger.info("MerchandizeAction::updatePartnerPrioritize:ENTER");
		
		boolean isAnyRecurrencePriority = false;
		boolean isAnyPriorityValueChanged = false;
		String sFrdKey="searchPartnerPrioritySuccess";
		int iPriorityOfMerchandize = 0;
		boolean bStatus = true;
		List<PartnerVO> alPartnerInfo = null;
		List<PartnerVO> alOldPartnerInfo = null;
		PartnerPrioritizeForm oPartnerPrioritizeForm = (PartnerPrioritizeForm)form;
		HashMap<Integer,Integer> hmOldPartnerInfo = new HashMap<Integer,Integer>();
		HashMap<String,HashMap> hmOldPartnerPriorityInfo = new HashMap<String,HashMap>();
		try{
			HttpSession oSession = request.getSession(true);
			LoginVO oLoginVO = null;
			String sAirId = null,sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			alPartnerInfo = oPartnerPrioritizeForm.getRecordSet();
			
			/* To get old partner Info from table */
			
			hmOldPartnerPriorityInfo = MerchandizeDAO.getPartnerPriorityDetails();
			
			//Newly updated partner priority details 
			for(PartnerVO oPartnerVO:alPartnerInfo) {
				if(oPartnerVO.getOwnerType()!=null && oPartnerVO.getOwnerId()!=null) {
					hmOldPartnerInfo = hmOldPartnerPriorityInfo.get(oPartnerVO.getOwnerType().toUpperCase());
					String sPriority = hmOldPartnerInfo.get(Integer.parseInt(oPartnerVO.getOwnerId())).toString();
					if(!oPartnerVO.getOwnerPriority().equalsIgnoreCase(sPriority)){
					// Update the data	
						bStatus = MerchandizeDAO.updatePartnerPriority(oPartnerVO,oLoginVO.getUserId());
					}
				}
				
			}
			
			oPartnerPrioritizeForm = MerchandizeDAO.getPartnerDetails(oPartnerPrioritizeForm);
			if(oPartnerPrioritizeForm != null && oPartnerPrioritizeForm.getRecordSet() != null 
					&& oPartnerPrioritizeForm.getRecordSet().size() > 0) {
				request.setAttribute("partnerInfo", oPartnerPrioritizeForm.getRecordSet());
			}else{
				request.setAttribute("NoRecords","noRecords");
				
			}
			
			
			logger.info("MerchandizeAction::updatePartnerPrioritize:EXIT");
		}catch(Exception e){		
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::updatePartnerPrioritize:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
}