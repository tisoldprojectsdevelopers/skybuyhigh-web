package com.sbh.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.forms.LoginForm;
import com.sbh.security.UserEntitlements;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.LoginAirlineVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.ProductDetailsVO;


public class LoginAction extends DispatchAction{
	private static Logger logger = LogManager.getLogger(LoginAction.class);
	
	
	public ActionForward adminLogin(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("LoginAction::loginAdmin::ENTER");
		
		String s_ForwardKey = "loginSuccess";
		
		logger.info("LoginAction::loginAdmin::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward airlineLogin(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("LoginAction::airlineLogin::ENTER");
		
		String s_ForwardKey = "loginSuccess";
		
		logger.info("LoginAction::airlineLogin::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward vendorLogin(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("LoginAction::vendorLogin::ENTER");
		
		String s_ForwardKey = "loginSuccess";
		
		logger.info("LoginAction::vendorLogin::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward preEntry(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception { 
		logger.info("LoginAction::preEntry:ENTER");
		String sFrdKey="preEntrySuccess";	
		HttpSession oSession=request.getSession(true);		
		ArrayList alOrderStatusInfo =null,alCatalogueStatusInfo =null;
		LoginVO oLoginVO = null;
		String sUserType = null,sOwnerId = null,sOwnerType=null,sURI=null,sPath=null;
		ArrayList<CategoryVO> alCategory = null;
		List<ProductDetailsVO> alProductInfo =null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		String sScheme = null;
		try{	
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase("vendor"))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			else if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			
			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
			alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
			if(oLoginVO!= null){
				sUserType = oLoginVO.getUserType();
				sUserType = sUserType ==null?"":sUserType.trim();
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId ==null?"":sOwnerId.trim();
				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType ==null?"":sOwnerType.trim();
			}	
			//getting order details
			alOrderStatusInfo = OrderDAO.getOrderStatus(oLoginVO);
			if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
				oSession.setAttribute("pendingOrders", alOrderStatusInfo.get(0));
				oSession.setAttribute("FailedOrders", alOrderStatusInfo.get(1));
				oSession.setAttribute("compltedOrders", alOrderStatusInfo.get(2));
				oSession.setAttribute("RejectedOrders", alOrderStatusInfo.get(3));
				oSession.setAttribute("ToBeChargedOrders", alOrderStatusInfo.get(4));
			}
			//getting catalogue status details
			alCatalogueStatusInfo = CatalogueDAO.getCatalogueStatus(sOwnerId,sOwnerType);
			if(alCatalogueStatusInfo!=null && alCatalogueStatusInfo.size()>0){
				oSession.setAttribute("pendingCatalogue", alCatalogueStatusInfo.get(0));
				oSession.setAttribute("acceptedCatalogue", alCatalogueStatusInfo.get(1));
				oSession.setAttribute("rejectedCatalogue", alCatalogueStatusInfo.get(2));
				oSession.setAttribute("productCount", alCatalogueStatusInfo.get(3));	
			}
			//getting Most Popular merchandize details
			sScheme = request.getScheme();
			alProductInfo = OrderDAO.getMostPopularMerchandizeDetails(alCategory,oLoginVO,sScheme);
			if(alProductInfo!=null && alProductInfo.size()>0){
				oSession.setAttribute("popualrMerchandizeInfo",alProductInfo);
			}
			
			//displaying the total order placed amount
			String sTotalAmount = OrderDAO.getorderPlacedTotAmount(oLoginVO);
			if(sTotalAmount!=null)
				oSession.setAttribute("orderPlacedTotAmt",sTotalAmount);
			
			
			//Generating e-Catalogue xml for the vendor				
			String sServerPath = getServlet().getServletContext().getRealPath("");
			
			oGenerateCatalogue.generateCatalogueXML(sDestPath,oLoginVO.getUserType(),oLoginVO.getRefId(),oLoginVO.getUserId(),sServerPath,"","","PREVIEWCATALOGUE","","");
			String sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
			String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
			
			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("personalshopperPath", sProdPreviewSwfPath+mUpdatedSwfIds.get("PersonalshopperPath"));
			/*oSession.setAttribute("eCatalogueFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_"+oLoginVO.getUserId()+".xml");
			oSession.setAttribute("eCatalogueCoverFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_cover_"+oLoginVO.getUserId()+".xml");
			oSession.setAttribute("eCataloguePartnerFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_partner_"+oLoginVO.getUserId()+".xml");
			*/// System.out.println("eCatalogueFileName:"+sE_CatalogueFileName+"_"+oLoginVO.getUserId());
			
			oSession.setAttribute("eCatalogueFileName", sE_CatalogueFileName+"_"+oLoginVO.getUserId()+".xml");
			oSession.setAttribute("eCatalogueCoverFileName", sE_CatalogueFileName+"_cover_"+oLoginVO.getUserId()+".xml");
			oSession.setAttribute("eCataloguePartnerFileName", sE_CatalogueFileName+"_partner_"+oLoginVO.getUserId()+".xml");
			
			
		    logger.info("LoginAction::preEntry:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("LoginAction::preEntry:Exception "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}
	public ActionForward login(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception { 
		logger.info("LoginAction::login:ENTER");
		String sFrdKey="loginFailure";
		String sUserId=null,sPassword=null,sUserType=null;
		LoginForm oLoginForm=null;
		HttpSession oSession=request.getSession(true);		
		LoginVO oLoginVO = null;
		ArrayList alOrderStatusInfo =null,alCatalogueStatusInfo=null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		ArrayList<CategoryVO> alCategory = null;
		List<ProductDetailsVO> alProductInfo =null;
		String sScheme = null;
		try{					
				oLoginForm=(LoginForm)form;		
				sUserId = oLoginForm.getUserId();
				logger.info("LoginAction::userId:"+sUserId);
				sPassword=(String)oLoginForm.getPassword();
				sUserType=(String)oLoginForm.getUserType();
				sUserId=sUserId==null?"":sUserId.trim();
				sPassword=sPassword==null?"":sPassword.trim();
				sUserType=sUserType==null?"":sUserType.trim();
				
				oLoginForm.setUserId(sUserId);
				oLoginForm.setPassword(sPassword);
				oLoginForm.setUserType(sUserType);
				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");	
				
				if(sUserId.trim().equals("") && sPassword.trim().equals("") && sUserType.trim().equals(""))
					throw new Exception("User Name/Password is required");
			
					
				oLoginVO=UserEntitlements.authenticateUser(sUserId,sPassword,sUserType);	
				if (oLoginVO == null)
					throw new Exception ("Invalid User Name/Password");
				else {
				
					if(sUserType!=null && sUserType.equalsIgnoreCase("admin"))
						oSession.setAttribute("adminLoginInfo",oLoginVO);
					else if(sUserType!=null && sUserType.equalsIgnoreCase("vendor"))
						oSession.setAttribute("vendorLoginInfo",oLoginVO);
					else if(sUserType!=null && sUserType.equalsIgnoreCase("airline"))
						oSession.setAttribute("airlineLoginInfo",oLoginVO);
				}
				
				ArrayList alListOfAction = UserEntitlements.buildEntitlementActions(oLoginVO);
				
				oSession.setAttribute("EntitlementActions", alListOfAction);
				
				if(oLoginVO.getUserType().equalsIgnoreCase("Airline")){
					sFrdKey="loginAirlineSuccess";
				}	
				else if(oLoginVO.getUserType().equalsIgnoreCase("Vendor")){			
					sFrdKey="loginVendorSuccess";
				}	
				else if(oLoginVO.getUserType().equalsIgnoreCase("admin")){
					sFrdKey="loginAdminSuccess";
				}	
				//getting order details
				alOrderStatusInfo = OrderDAO.getOrderStatus(oLoginVO);
				if(alOrderStatusInfo!=null && alOrderStatusInfo.size()>0){
					oSession.setAttribute("pendingOrders", alOrderStatusInfo.get(0));
					oSession.setAttribute("FailedOrders", alOrderStatusInfo.get(1));
					oSession.setAttribute("compltedOrders", alOrderStatusInfo.get(2));
					oSession.setAttribute("RejectedOrders", alOrderStatusInfo.get(3));
					oSession.setAttribute("ToBeChargedOrders", alOrderStatusInfo.get(4));
					
				}
				//getting catalogue status details
				alCatalogueStatusInfo = CatalogueDAO.getCatalogueStatus(oLoginVO.getRefId(),oLoginVO.getUserType());
				if(alCatalogueStatusInfo!=null && alCatalogueStatusInfo.size()>0){
					oSession.setAttribute("pendingCatalogue", alCatalogueStatusInfo.get(0));
					oSession.setAttribute("acceptedCatalogue", alCatalogueStatusInfo.get(1));
					oSession.setAttribute("rejectedCatalogue", alCatalogueStatusInfo.get(2));
					oSession.setAttribute("productCount", alCatalogueStatusInfo.get(3));
				}
				//getting Most Popular merchandize details
				sScheme = request.getScheme();
				alProductInfo = OrderDAO.getMostPopularMerchandizeDetails(alCategory,oLoginVO,sScheme);
				
				if(alProductInfo!=null && alProductInfo.size()>0){
					oSession.setAttribute("popualrMerchandizeInfo",alProductInfo);
				}
				String sTotalAmount = OrderDAO.getorderPlacedTotAmount(oLoginVO);
				sTotalAmount = sTotalAmount ==null?"0":sTotalAmount.trim();
				if(sTotalAmount!=null)
					oSession.setAttribute("orderPlacedTotAmt",sTotalAmount);
				
				//Generating e-Catalogue xml for the vendor				
				String sServerPath = getServlet().getServletContext().getRealPath("");
				
				oGenerateCatalogue.generateCatalogueXML(sDestPath,oLoginVO.getUserType(),oLoginVO.getRefId(),oLoginVO.getUserId(),sServerPath,"","","PREVIEWCATALOGUE","","");
				
				String sProdPreviewXmlPath = System.getProperty("ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty("ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO.getLastUpdatedCatalogueId();
				
				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath+mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("personalshopperPath", sProdPreviewSwfPath+mUpdatedSwfIds.get("PersonalshopperPath"));
				/*oSession.setAttribute("eCatalogueFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_"+oLoginVO.getUserId()+".xml");
				oSession.setAttribute("eCatalogueCoverFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_cover_"+oLoginVO.getUserId()+".xml");
				oSession.setAttribute("eCataloguePartnerFileName", sProdPreviewXmlPath+sE_CatalogueFileName+"_partner_"+oLoginVO.getUserId()+".xml");
				*/
				
				oSession.setAttribute("eCatalogueFileName", sE_CatalogueFileName+"_"+oLoginVO.getUserId()+".xml");
				oSession.setAttribute("eCatalogueCoverFileName", sE_CatalogueFileName+"_cover_"+oLoginVO.getUserId()+".xml");
				oSession.setAttribute("eCataloguePartnerFileName", sE_CatalogueFileName+"_partner_"+oLoginVO.getUserId()+".xml");
				
				// System.out.println("eCatalogueFileName:"+sE_CatalogueFileName+"_"+oLoginVO.getUserId());
				// System.out.println("eCatalogueCoverFileName:"+sE_CatalogueFileName+"_cover_"+oLoginVO.getUserId()+".xml");
				// System.out.println("eCataloguePartnerFileName:"+sE_CatalogueFileName+"_partner_"+oLoginVO.getUserId()+".xml");
				
			    logger.info("LoginAction::login:EXIT");
		}catch(Exception e){
			ActionMessages messages = new ActionMessages();       
			messages.add("loginFailed", new ActionMessage("login.failed.error","User Authentication Failed."));
			saveMessages(request, messages);
			e.printStackTrace();
			logger.error("LoginAction::login:Exception "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}

	public ActionForward loginAirline(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception { 
		logger.info("LoginAction::loginAirline:ENTER");
		String sFrdKey="loginAirlineFailure";
		String sUserId=null,sPassword=null;
		LoginForm oLoginForm=null;
		HttpSession oSession=request.getSession(true);		
		LoginAirlineVO oLoginAirlineVO = null;
	
		try{					
			
				oLoginForm=(LoginForm)form;		
				sUserId = oLoginForm.getUserId();
				sPassword=(String)oLoginForm.getPassword();
				sUserId=sUserId==null?"":sUserId.trim();
				sPassword=sPassword==null?"":sPassword.trim();
				
				oLoginForm.setUserId(sUserId);
				oLoginForm.setPassword(sPassword);
			
				if(sUserId.trim().equals("") && sPassword.trim().equals(""))
					throw new Exception("User Authentication failed: User Name/Password is required");
			
					
				oLoginAirlineVO=UserEntitlements.authenticateAirlineUser(sUserId,sPassword);	
				if (oLoginAirlineVO == null)
					throw new Exception ("Authentication Failed.");
				else 
					oSession.setAttribute("loginAirlineInfo",oLoginAirlineVO);
					
						
				 sFrdKey="loginAirlineSuccess";	
			    logger.info("LoginAction::loginAirline:EXIT");
		}catch(Exception e){
			ActionMessages messages = new ActionMessages();       
			messages.add("loginFailed", new ActionMessage("login.failed.error","User Authentication Failed."));
			saveMessages(request, messages);
			e.printStackTrace();
			logger.error("LoginAction::loginAirline:Exception "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);
	}
	public ActionForward logout(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception { 
		logger.info("LoginAction::logout:ENTER");
		
		String sFrdKey="logoutSuccess";
		
		String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		String sURI=request.getRequestURI();
		String sPath=Utils.getUserTypeFromURI(sURI);
		sPath=sPath==null?"":sPath.trim();
		
		String sBasePath = null;
		if(sPath.equalsIgnoreCase("vendor"))
			sFrdKey = "vendorLogoutSuccess";
			//sBasePath=basePath+"vendor/login.do?method=vendorLogin";
		else if(sPath.equalsIgnoreCase("airline"))
			sFrdKey = "airlineLogoutSuccess";
			//sBasePath=basePath+"airline/login.do?method=airlineLogin";
		else if(sPath.equalsIgnoreCase("admin"))
			sFrdKey = "adminLogoutSuccess";
		else if(sPath.equalsIgnoreCase("customer"))
			sFrdKey = "customerLogoutSuccess";
			//sBasePath=basePath+"admin/login.do?method=adminLogin";
		
		//response.sendRedirect(sBasePath);

		logger.info("LoginAction::logout:EXIT");
		return mapping.findForward(sFrdKey);
	}
}

