/**
 * 
 */
package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.INVOICE;
import static com.sbh.dao.CustomerDAO.createRMAGeneratedHTML;
import static com.sbh.dao.CustomerDAO.getCustomerOrderItemDetails;
import static com.sbh.dao.CustomerDAO.getCustomerTransactionDetails;
import static com.sbh.dao.CustomerDAO.getGeneratedRMADetails;
import static com.sbh.dao.CustomerDAO.getOrderConfirmationHistory;
import static com.sbh.dao.CustomerDAO.getOrderRMADetails;
import static com.sbh.dao.CustomerDAO.getOrderReturnDetails;
import static com.sbh.dao.CustomerDAO.getPartnerReturnDetails;
import static com.sbh.dao.CustomerDAO.getRMAgenerated;
import static com.sbh.dao.CustomerDAO.getReturnOrderRMADetails;
import static com.sbh.dao.CustomerDAO.getReturnPolicyDetails;
import static com.sbh.dao.CustomerDAO.sendRMAGeneratedToCustomer;
import static com.sbh.util.Utils.escapeRegExp;
import static com.sbh.util.Utils.getPhoneFormat;
import static com.sbh.util.Utils.timeStampConversion;

import java.awt.Dimension;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.zefer.pd4ml.PD4ML;

import com.sbh.dao.OrderDAO;
import com.sbh.forms.CustomerLoginForm;
import com.sbh.util.Utils;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CustomerReturnDetailsVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.OrderConfirmationVO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.OrderReturnDetailsVO;
import com.sbh.vo.StateVO;

/**
 * @author Thapovan
 *
 */
public class CustomerLoginAction extends DispatchAction {
	
	private static Logger logger = LogManager.getLogger(CustomerLoginAction.class);


	public ActionForward login(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::login::ENTER");
		
		String s_ForwardKey = "loginSuccess";
		
		logger.info("CustomerLoginAction::login::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward getTransactionDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::getTransactionDetails::ENTER");
		
		String s_ForwardKey="TransactionSuccess";
		List<OrderItemDetailsVO> orderItemDetailList= null;
		List<OrderItemDetailsVO> otherDetailsList = new ArrayList<OrderItemDetailsVO>();
		List<OrderConfirmationVO> orderConfirmationList = null;
		CustomerLoginForm o_CustomerForm;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO=null;
		List<CategoryVO> alCategory = null;
		List<OrderReturnDetailsVO> alOrderReturnDetailsVO = null;
		String orderConfNo=request.getParameter("OCN");
		String sCustEmailId = request.getParameter("CustEmailId");
		String sCateFolderName="";
		String sImageViewPath="";
		int iApprovedQuantity = 0;
		int iCustReturnQuantity = 0;
		int iQuantity = 0;
		
		if(form instanceof CustomerLoginForm) {
			o_CustomerForm = (CustomerLoginForm)form;
			try {
				if(o_CustomerForm.getTransactionId() == null || o_CustomerForm.getTransactionId().length() == 0 ) {
					o_CustomerForm.setTransactionId(orderConfNo == null?"":orderConfNo.trim());
				}
				if(o_CustomerForm.getEmailId() == null || o_CustomerForm.getEmailId().length() == 0 ) {
					o_CustomerForm.setEmailId(sCustEmailId == null?"":sCustEmailId.trim());
				}
				orderItemDetailList = getCustomerOrderItemDetails(o_CustomerForm);
				orderConfirmationList = getOrderConfirmationHistory(o_CustomerForm);
				
				sImageViewPath = System.getProperty("ImageViewPath").trim();
				alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
				for(OrderItemDetailsVO oOrderItemDetailsVO:orderItemDetailList) {
					if(oOrderItemDetailsVO != null) {
						if("Vendor".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())) {
							if(alCategory!=null && alCategory.size()>0) {
								for(CategoryVO oCategoryVO: alCategory) {
									if(oCategoryVO.getCateId()!=null) {
										if(oCategoryVO.getCateId().equalsIgnoreCase(oOrderItemDetailsVO.getCateId()))
											sCateFolderName = oCategoryVO.getCateName();
									}					
								}
							}
						}else if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("Airline")){
							sCateFolderName = "PrivateJetJaunts";
						}
						oOrderItemDetailsVO.setMainImgPath(sImageViewPath+oOrderItemDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oOrderItemDetailsVO.getOwnerId()+oOrderItemDetailsVO.getProdId()+"thumb."+oOrderItemDetailsVO.getMainImgType());
						
						/*
						 * This is used to calculate whether the return link should be available for the customer to 
						 * generate the RMA number for the product purchased. 
						 */
						alOrderReturnDetailsVO = getOrderReturnDetails(oOrderItemDetailsVO.getOrderItemId());
						iQuantity = Integer.parseInt(oOrderItemDetailsVO.getQty());
						if(alOrderReturnDetailsVO != null && !alOrderReturnDetailsVO.isEmpty()) {
							oOrderItemDetailsVO.setIsRMAGenerated("Y");
							for(OrderReturnDetailsVO oOrderReturnDetailsVO : alOrderReturnDetailsVO) {
								if(oOrderReturnDetailsVO.getCustomerReturnQuantity() != null) {
									iCustReturnQuantity = Integer.parseInt(oOrderReturnDetailsVO.getCustomerReturnQuantity());
								}else {
									iCustReturnQuantity = 0;
								}
								if(oOrderReturnDetailsVO.getVendorReturnQuantity() != null) {
									iApprovedQuantity = Integer.parseInt(oOrderReturnDetailsVO.getVendorReturnQuantity());
								}else {
									iApprovedQuantity = -1;
								}
								iQuantity = iQuantity - iCustReturnQuantity;
								if(iApprovedQuantity > -1) {
									iQuantity = iQuantity + (iCustReturnQuantity-iApprovedQuantity);
								}
							}
						}
						if(iQuantity <= 0) {
							oOrderItemDetailsVO.setIsEligibleToReturn("NO");
						}else {
							oOrderItemDetailsVO.setIsEligibleToReturn("YES");
						}
					}
				}
				
				if(orderItemDetailList != null && orderItemDetailList.size() > 0) {
					otherDetailsList.add(orderItemDetailList.get(0));
					oLoginVO=new LoginVO();
					oLoginVO.setUserId(o_CustomerForm.getTransactionId());
					oLoginVO.setUserName(orderItemDetailList.get(0).getCustFirstName()+" "+orderItemDetailList.get(0).getCustLastName());
					oSession.setAttribute("custLoginInfo",oLoginVO);
					oSession.setAttribute("custOrderList", orderItemDetailList);
					if(orderConfirmationList != null && orderConfirmationList.size() > 1) {
						request.setAttribute("History", "Available");
					}
					
					request.setAttribute("TransactionId", o_CustomerForm.getTransactionId());
					request.setAttribute("OtherDetails", otherDetailsList);
					request.setAttribute("orderConfirmationList", orderConfirmationList);
				}else {
					request.setAttribute("NoData", "Invalid Order Confirmation No.");
					s_ForwardKey="TransactionFailed";
					throw new Exception("OCN");
				}
			}catch(Exception e) {
				s_ForwardKey="TransactionFailed";
				ActionMessages messages = new ActionMessages();
				if("OCN".equalsIgnoreCase(e.getMessage())) {
					messages.add("loginfailed", new ActionMessage("login.failed.error","Invalid Order confirmation no."));
				}else {
					messages.add("loginfailed", new ActionMessage("login.failed.error","Authentication Failed: Please Contact SkyBuyHigh Admin."));
				}
				saveMessages(request, messages);
				e.printStackTrace();
				logger.debug("CustomerLoginAction::getTransactionDetails::EXCEPTION");
			}
		}
		
		logger.info("CustomerLoginAction::getTransactionDetails::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward sendTransactionDetailsToCustomer(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::sendTransactionDetailsToCustomer::ENTER");
		
		String sForwardKey = "TransactionFailed";
		CustomerLoginForm o_CustomerForm;
		Boolean isEmailSent;
		if(form instanceof CustomerLoginForm) {
			o_CustomerForm = (CustomerLoginForm)form;
			try {
				isEmailSent = getCustomerTransactionDetails(o_CustomerForm);
				if(isEmailSent.booleanValue()) {
					ActionMessages messages = new ActionMessages();       
					messages.add("EmailSent", new ActionMessage("login.failed.error","Order Confirmation No. has been sent to your Email Id."));
					saveMessages(request, messages);
				}else {
					ActionMessages messages = new ActionMessages();    
					request.setAttribute("hasDisplay", "display");
					messages.add("EmailSent", new ActionMessage("login.failed.error","Invalid Email Id."));
					saveMessages(request, messages);
				}
			}catch(Exception e) {
				logger.info("CustomerLoginAction::sendTransactionDetailsToCustomer::EXCEPTION");
				e.printStackTrace();
				request.setAttribute("hasDisplay", "display");
				ActionMessages messages = new ActionMessages();       
				messages.add("EmailSent", new ActionMessage("login.failed.error","Problem in processing your email. Contact admin"));
				saveMessages(request, messages);
				sForwardKey = "Failed";
			}
		}
		
	    logger.info("CustomerLoginAction::sendTransactionDetailsToCustomer::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward displayReturnPolicy(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
	throws ServletException {
		logger.info("CustomerLoginAction::displayReturnPolicy::ENTER");

		String s_ForwardKey = "ReturnPolicySuccess";		
		HttpSession oSession = request.getSession();
		String sURI=null;
		String loginType="";
		OrderItemDetailsVO oOrderItemDetail;
		LoginVO oLoginVO =null;
		String sPath=null;
		String sOwnerType = request.getParameter("OwnerType");
		sOwnerType = sOwnerType == null?"":sOwnerType.trim();
		String sOrderItemId = request.getParameter("OrderItemId");
		sOrderItemId = sOrderItemId == null?"":sOrderItemId.trim();
		String sIsEditOrder = request.getParameter("PlaceOrder");
		String sItemStatus = request.getParameter("ItemStatus");
		String sMode = request.getParameter("Mode"); 
		try {
			
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			
			if("vendor".equalsIgnoreCase(sPath)) 
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			else if("admin".equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");	
			else if("airline".equalsIgnoreCase(sPath))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			if(oLoginVO != null) {
				if("airline".equalsIgnoreCase(oLoginVO.getUserType())) {
					s_ForwardKey = "AirlineReturnPolicySuccess";
				}else if("admin".equalsIgnoreCase(oLoginVO.getUserType())) {
					s_ForwardKey = "AdminReturnPolicySuccess";
				}else if("vendor".equalsIgnoreCase(oLoginVO.getUserType())) {
					s_ForwardKey = "VendorReturnPolicySuccess";
				}
				loginType = oLoginVO.getUserType();
				loginType = loginType == null?"":loginType.trim();
			}
			if(sIsEditOrder != null && sIsEditOrder.trim().length() > 0) {
				request.setAttribute("IsEditOrder", "YES");
			}else {
				request.setAttribute("IsEditOrder", "NO");
			}
			oOrderItemDetail = getReturnPolicyDetails(sOrderItemId,sOwnerType,loginType);
			request.setAttribute("OrderItemId", sOrderItemId);
			oOrderItemDetail.setCustServicePhone(getPhoneFormat(oOrderItemDetail.getCustServicePhone()));
			request.setAttribute("ReturnPolicy",oOrderItemDetail);
			request.setAttribute("SourceOfEdit",sIsEditOrder);
			request.setAttribute("OwnerType", sOwnerType);
			request.setAttribute("ItemStatus", sItemStatus);
			request.setAttribute("Mode", sMode);
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("CustomerLoginAction::displayReturnPolicy::EXCEPTION"+e.getMessage());
		}

		logger.info("CustomerLoginAction::displayReturnPolicy::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	
	public ActionForward displayLogin(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) {
		logger.info("CustomerLoginAction::displayLogin::ENTER");
		String s_ForwardKey = "TransactionFailed";
		logger.info("CustomerLoginAction::displayLogin::EXIT");
		return mapping.findForward(s_ForwardKey);
	}
	public ActionForward generatePDF(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("CustomerLoginAction::generatePDF::Entry");
		
		byte[] baPdf = null;
		int iStartIndex = 0;
		int iEndIndex = 0;
		int iImageStartIndex = 0;
		int iImageEndIndex = 0;
		String htmlText = null;
		String sCurrentTime = null;
		String sImagePath="";
		String sSubStringFirst;
		String sSubStringLast;
		String s_ReplaceOrder=null;
		String s_replace="";
		String s_table="";
		String s_logo="";
		String s_replacetable="";
		String s_closetag="";
		String s_startTag="";
		String s_Tagname = "{{CustomerName}}";
		String sFilePath = null;
		String sHTMLFileName = null;
		String sStartImageTag = null;
		String sEndImageTag = null;
		String sFileName = null;
		String sImageViewPath = null;
		String sImageUploadPath = null;
		ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
		File oImageFile = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		List<OrderItemDetailsVO> orderItemDetailList = null;
		File oFile = null;
		File oImagePath = null;
		try{
			
			response.setContentType("application/pdf");
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sFilePath = getServlet().getServletContext().getRealPath("/");
			sCurrentTime = timeStampConversion(new Date());
			htmlText = (String)request.getParameter("pdfText");
			oLoginVO=(LoginVO)oSession.getAttribute("custLoginInfo");
			orderItemDetailList = (List<OrderItemDetailsVO>)oSession.getAttribute("custOrderList");
			sImageUploadPath = System.getProperty("ImageUploadPath");
			sImageViewPath = System.getProperty("ImageViewPath");
			
			s_replace = oBundle.getString("pdf.customer.replaceorder");
			s_replace = s_replace==null?"":s_replace.trim();
			s_ReplaceOrder = replaceString(s_Tagname,oLoginVO.getUserName(),s_replace);
			
			s_logo=oBundle.getString("pdf.customer.logo");
			s_logo=s_logo==null?"":s_logo.trim();
			htmlText = s_logo+htmlText;
			
			s_startTag = oBundle.getString("pdf.customer.starttag");
			s_startTag = s_startTag==null?"":s_startTag.trim();
			htmlText = s_startTag+htmlText;
			
			iStartIndex = htmlText.indexOf("<DIV class=ReplaceOrderDetailsStart></DIV>");
			iEndIndex = htmlText.indexOf("<DIV class=ReplaceOrderDetailsEnd></DIV>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				sSubStringFirst = htmlText.substring(0,iStartIndex);
				sSubStringLast = htmlText.substring(iEndIndex,htmlText.length());
				htmlText = sSubStringFirst+s_ReplaceOrder+sSubStringLast;
			}
			
			s_replacetable = oBundle.getString("pdf.customer.replacetable");
			s_replacetable = s_replacetable==null?"":s_replacetable.trim();
			
			iStartIndex = htmlText.indexOf("<DIV class=ReplaceOuterTableStart></DIV>");
			iEndIndex = htmlText.indexOf("<DIV class=ReplaceOuterTableEnd></DIV>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				sSubStringFirst = htmlText.substring(0,iStartIndex);
				sSubStringLast = htmlText.substring(iEndIndex,htmlText.length());
				htmlText = sSubStringFirst+s_replacetable+sSubStringLast;
			}
			
			s_table = oBundle.getString("pdf.customer.table");
			s_table = s_table==null?"":s_table.trim();
			
			iStartIndex = htmlText.indexOf("<DIV class=ReplaceTagStart></DIV>");
			iEndIndex = htmlText.indexOf("<DIV class=ReplaceTagEnd></DIV>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				sSubStringFirst = htmlText.substring(0,iStartIndex);
				sSubStringLast = htmlText.substring(iEndIndex,htmlText.length());
				htmlText = sSubStringFirst+s_table+sSubStringLast;
			}
			
			iStartIndex = htmlText.indexOf("<DIV class=RemoveReturnsLabelStarts></DIV>");
			iEndIndex = htmlText.indexOf("<DIV class=RemoveReturnsLabelEnd></DIV>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				sSubStringFirst = htmlText.substring(0,iStartIndex);
				sSubStringLast = htmlText.substring(iEndIndex,htmlText.length());
				htmlText = sSubStringFirst+sSubStringLast;
			}
			
			sStartImageTag = oBundle.getString("pdf.customer.start.imagetag");
			sStartImageTag = sStartImageTag==null?"":sStartImageTag.trim();
			
			sEndImageTag = oBundle.getString("pdf.customer.end.imagetag");
			sEndImageTag = sEndImageTag==null?"":sEndImageTag.trim();
			
			if(orderItemDetailList != null && orderItemDetailList.size() > 0) {
				for(OrderItemDetailsVO oOrderItemDetailsVO:orderItemDetailList) {
					iStartIndex = htmlText.indexOf("<DIV class=RemoveReturnsStarts></DIV>");
					iEndIndex = htmlText.indexOf("<DIV class=RemoveReturnsEnd></DIV>");
					if(iStartIndex > 0 && iEndIndex > 0) {
						sSubStringFirst = htmlText.substring(0,iStartIndex);
						sSubStringLast = htmlText.substring(iEndIndex+34,htmlText.length());
						htmlText = sSubStringFirst+sSubStringLast;
					}
					iImageStartIndex = htmlText.indexOf("<DIV id=IMAGETAGSTART></DIV>");
					iImageEndIndex = htmlText.indexOf("<DIV id=IMAGETAGEND></DIV>");
					if(iImageStartIndex > 0 && iImageEndIndex > 0) {
						sSubStringFirst = htmlText.substring(0,iImageStartIndex);
						sSubStringLast = htmlText.substring(iImageEndIndex+26,htmlText.length());
						oImageFile = renderImage(oOrderItemDetailsVO.getOrderItemId(), oOrderItemDetailsVO.getMainImgType());
						if(oImageFile != null && oImageFile.getName() != null)
							sFileName = oImageFile.getName();
						sFileName = sFileName==null?"":sFileName.trim();
						sImagePath = sStartImageTag+ sImageViewPath+"Customer/"+sFileName+" "+sEndImageTag;
						htmlText = sSubStringFirst+sImagePath+sSubStringLast;
					}
				}
			}
			
			iStartIndex = htmlText.indexOf("<DIV class=RemoveButtonTagStarts></DIV>");
			iEndIndex = htmlText.indexOf("<DIV class=RemoveButtonTagEnd></DIV>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				sSubStringFirst = htmlText.substring(0,iStartIndex);
				sSubStringLast = htmlText.substring(iEndIndex,htmlText.length());
				htmlText = sSubStringFirst+sSubStringLast;
			}
			
			s_closetag = oBundle.getString("pdf.customer.closetag");
			s_closetag = s_closetag==null?"":s_closetag.trim();
			htmlText = htmlText+s_closetag;
			
			// System.out.println("Generated PDF"+htmlText);
			
			sHTMLFileName = sFilePath+"SBH_Template_"+oLoginVO.getUserId()+"_"+sCurrentTime+".html";
			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			oBufWriter.write(htmlText);
			oBufWriter.close();
	
			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(900,1000));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);
	
			pd4ml.enableDebugInfo();
	
			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);
			oFile = new File(sHTMLFileName);
			if(oFile.exists()){
				oFile.delete();
			}
			
			baPdf=oByteArrayOutputStream.toByteArray();
			if(baPdf != null && baPdf.length > 0){
				response.setContentType("application/pdf");
				ServletOutputStream oOutputStream = response.getOutputStream();
				oOutputStream.write(baPdf);
				oOutputStream.flush();
				oOutputStream.close();
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("CustomerLoginAction::generatePDF::EXCEPTION"+e.getMessage());
		}
		finally{
			if(orderItemDetailList != null && orderItemDetailList.size() > 0) {
				for(OrderItemDetailsVO oOrderItemDetailsVO:orderItemDetailList){
					oImagePath = new File(sImageUploadPath+"//Customer//"+oOrderItemDetailsVO.getOrderItemId()+"."+oOrderItemDetailsVO.getMainImgType());
					if(oImagePath.exists())
						oImagePath.delete();
				}
			}
		}
		
		logger.info("CustomerLoginAction::generatePDF::Exit");
		return null;
	}
	public ActionForward displayOrderConfirmationHistory(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::displayOrderConfirmationHistory::Entry");
		ActionForward forward = null;
		
		try{
			String transactionId = request.getParameter("orderConfNo");
			transactionId = transactionId==null?"":transactionId.trim();
			
			CustomerLoginForm o_CustomerForm = new CustomerLoginForm();
			o_CustomerForm.setTransactionId(transactionId);
			forward=getTransactionDetails(mapping,o_CustomerForm,request,response);
		}catch(Exception ex) {
			ex.printStackTrace();
			logger.debug("CustomerLoginAction::displayOrderConfirmationHistory::Exception");
		}
		
		logger.info("CustomerLoginAction::displayOrderConfirmationHistory::Exit");
		return forward;
	}
	
	private static String replaceString(String p_sTagName, String p_sReplaceText, String p_sHtmlText) {
		
		String sRegex;
		
		if(p_sTagName != null && p_sReplaceText != null){
			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sHtmlText = p_sHtmlText.replaceAll(sRegex, p_sReplaceText);
		}
		return p_sHtmlText;
	}
	
	public ActionForward displayOrderReturn(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::displayOrderReturn::Entry");
		
		String sOrderItemId = request.getParameter("OrderItemId");
		String sForwardKey = "OrderReturnSuccess";
		int iCustomerReturnQty = 0;
		int iCustomerReturnTotalQty = 0;
		int iVendorReturnTotalQty = 0;
		int iVendorReturnQty = 0;
		int iTotalQuantity = 0;
		List<OrderItemDetailsVO> orderItemDetailList = null;
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		HttpSession oSession = request.getSession();
		List<OrderReturnDetailsVO> alOrderReturnDetailsVO = new ArrayList<OrderReturnDetailsVO>();
		try{ 
			orderItemDetailList = (List<OrderItemDetailsVO>)oSession.getAttribute("custOrderList");
			
			if(sOrderItemId != null) {
				for(OrderItemDetailsVO oOrderItemDetailsVO:orderItemDetailList) {
					if(sOrderItemId.equalsIgnoreCase(oOrderItemDetailsVO.getOrderItemId())) {
						
//						oOrderItemDetail = getReturnPolicyDetails(sOrderItemId, oOrderItemDetailsVO.getOwnerType(), null);
						alOrderReturnDetailsVO = getOrderReturnDetails(sOrderItemId);
						iTotalQuantity = Integer.parseInt(oOrderItemDetailsVO.getQty());
						if(alOrderReturnDetailsVO != null && !alOrderReturnDetailsVO.isEmpty()) {
							for(OrderReturnDetailsVO oOrderReturnDetailsVO : alOrderReturnDetailsVO) {
								if(oOrderReturnDetailsVO.getCustomerReturnQuantity() != null) {
									iCustomerReturnQty = Integer.parseInt(oOrderReturnDetailsVO.getCustomerReturnQuantity());
									
									//Customer Return total quantity is to show how many items vendor returned.
									iCustomerReturnTotalQty = iCustomerReturnTotalQty + iCustomerReturnQty;
								}else {
									iCustomerReturnQty = 0;
								}
								if(oOrderReturnDetailsVO.getVendorReturnQuantity() != null) {
									iVendorReturnQty = Integer.parseInt(oOrderReturnDetailsVO.getVendorReturnQuantity());
									
									//Vendor Return total quantity is to show how many items vendor returned.
									iVendorReturnTotalQty = iVendorReturnTotalQty + iVendorReturnQty;
								}else {
									iVendorReturnQty = -1;
								}
								iTotalQuantity = iTotalQuantity - iCustomerReturnQty;
								if(iVendorReturnQty > -1) {
									iTotalQuantity = iTotalQuantity + (iCustomerReturnQty-iVendorReturnQty);
								}
							}
						}
						oOrderItemDetailsVO.setCustReturnQuantity(iCustomerReturnTotalQty+"");
						
						if(iVendorReturnTotalQty > 0) {
							oOrderItemDetailsVO.setReturnQuantity(iVendorReturnTotalQty+"");
						} else {
							oOrderItemDetailsVO.setReturnQuantity("0");
						}
						
						if(iTotalQuantity <= 0) {
							oCustomerReturnDetailsVO = getPartnerReturnDetails(oOrderItemDetailsVO.getOrderItemId(), oOrderItemDetailsVO.getOwnerType());
							sForwardKey = "RMAGeneratedSuccess";
							request.setAttribute("RMAGeneratedFirst", "RMA has been already generated for this order item. ");
							request.setAttribute("RMAGeneratedSecond", "Please print this form below and mail it along with your item to be returned");
							request.setAttribute("PartnerReturnDetails", oCustomerReturnDetailsVO);
							request.setAttribute("PartnerOrderDetails", oOrderItemDetailsVO);
//							sendRMAGeneratedEmailToCustomer(oOrderItemDetailsVO,oSession,oCustomerReturnDetailsVO, sState);
						}
						oSession.setAttribute("OrderReturnDetails", oOrderItemDetailsVO);
						request.setAttribute("CustomerAbleToReturn", iTotalQuantity+"");
						
						break;
						/*if(oOrderItemDetail != null) {
							oOrderItemDetailsVO.setReturnPolicy(oOrderItemDetail.getReturnPolicy());
							oOrderItemDetailsVO.setReturnDays(oOrderItemDetail.getReturnDays());
							if(oOrderItemDetail.getRmaGeneratedNo() != null 
									&& oOrderItemDetail.getRmaGeneratedNo().trim().length() > 0) {
								
								sForwardKey = "RMAGeneratedSuccess";
								oCustomerReturnDetailsVO = getPartnerReturnDetails(oOrderItemDetailsVO.getOrderItemId(), oOrderItemDetailsVO.getOwnerType());
								oCustomerReturnDetailsVO.setRmaStatus(oOrderItemDetail.getRmaStatus());
								oCustomerReturnDetailsVO.setGeneratedRMA(oOrderItemDetail.getRmaGeneratedNo());
								oCustomerReturnDetailsVO.setRmaGeneratedDate(oOrderItemDetail.getRmaGeneratedDate());
								if(alStates != null && oCustomerReturnDetailsVO != null) {
									for(StateVO oStateVO:alStates) {
										if(oStateVO.getStateCode() != null && oStateVO.getStateCode().equalsIgnoreCase(oCustomerReturnDetailsVO.getPartnerState())) {
											sState = oStateVO.getStateName();
										}
									}
								}
								sendRMAGeneratedEmailToCustomer(oOrderItemDetailsVO,oSession,oCustomerReturnDetailsVO, sState);
								request.setAttribute("PartnerReturnDetails", oCustomerReturnDetailsVO);
								request.setAttribute("PartnerOrderDetails", oOrderItemDetailsVO);
								request.setAttribute("RMAGeneratedFirst", "RMA has been already generated for this order item. ");
								request.setAttribute("RMAGeneratedSecond", "Please print this form below and mail it along with your item to be returned");
							}
							oSession.setAttribute("OrderReturnDetails", oOrderItemDetailsVO);
							break;
						
						}*/
					}
				}
			}
			saveToken(request);
		}catch(Exception ex) {
			ex.printStackTrace();
			logger.debug("CustomerLoginAction::displayOrderReturn::Exception");
		}
		
		logger.info("CustomerLoginAction::displayOrderReturn::Exit");
		return mapping.findForward(sForwardKey);
	}

	
	public ActionForward viewRMADetails(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("CustomerLoginAction::viewRMADetails::ENTRY");
		
		String sForwardKey = "RMADetailsSuccess";
		String sOrderItemId = request.getParameter("OrderItemId");
		String sCustTransId = null;
		List<OrderReturnDetailsVO> alOrderReturnDetails = new ArrayList<OrderReturnDetailsVO>();
		try {
			alOrderReturnDetails = getOrderRMADetails(sOrderItemId);
			
			for(OrderReturnDetailsVO oOrderReturnDetailsVO:alOrderReturnDetails) {
				sCustTransId = oOrderReturnDetailsVO.getCustTransId();
				break;
			}
			request.setAttribute("RMADetails", alOrderReturnDetails);
			request.setAttribute("CustTransId", sCustTransId);
			request.setAttribute("OrderItemId", sOrderItemId);
			
		}catch(Exception exception) {
			logger.debug("CustomerLoginAction::viewRMADetails::EXCEPTION"+exception.getMessage());
			exception.printStackTrace();
		}
		
		logger.info("CustomerLoginAction::viewRMADetails::ENTRY");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward displayRMADetails(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("CustomerLoginAction::displayRMADetails::ENTRY");
		
		String sForwardKey = "DisplayRMADetailsSuccess";
		String sReturnId = request.getParameter("ReturnId");
		OrderReturnDetailsVO oOrderReturnDetails = null;
		try {
			oOrderReturnDetails = getReturnOrderRMADetails(sReturnId);
			request.setAttribute("DisplayRMADetails", oOrderReturnDetails);
			
		}catch(Exception exception) {
			logger.debug("CustomerLoginAction::displayRMADetails::EXCEPTION"+exception.getMessage());
			exception.printStackTrace();
		}
		
		logger.info("CustomerLoginAction::displayRMADetails::ENTRY");
		return mapping.findForward(sForwardKey);
	}
	
	
	public ActionForward getRMAGeneratedValue(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::getRMAGeneratedValue::ENTRY");
		
		
		String sForwardKey = "GenerateRMASuccess";
		
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		CustomerLoginForm oCustomerLoginForm = (CustomerLoginForm)form;
		HttpSession oSession = request.getSession();
		String sState = null;
		int iCustomerReturnQty = 0;
		int iCustomerReturnTotalQty = 0;
		int iVendorReturnTotalQty = 0;
		int iVendorReturnQty = 0;
		int iTotalQuantity = 0;
		List<OrderReturnDetailsVO> alOrderReturnDetailsVO = new ArrayList<OrderReturnDetailsVO>();
		List<StateVO> alStates = (ArrayList)oSession.getServletContext().getAttribute("StateList");
		oOrderItemDetailsVO = (OrderItemDetailsVO)oSession.getAttribute("OrderReturnDetails");
		try {
			if(isTokenValid(request)) {
				alOrderReturnDetailsVO = getOrderReturnDetails(oOrderItemDetailsVO.getOrderItemId());
				iTotalQuantity = Integer.parseInt(oOrderItemDetailsVO.getQty());
				if(alOrderReturnDetailsVO != null && !alOrderReturnDetailsVO.isEmpty()) {
					for(OrderReturnDetailsVO oOrderReturnDetailsVO : alOrderReturnDetailsVO) {
						if(oOrderReturnDetailsVO.getCustomerReturnQuantity() != null) {
							iCustomerReturnQty = Integer.parseInt(oOrderReturnDetailsVO.getCustomerReturnQuantity());
							
							//Customer Return total quantity is to show how many items vendor returned.
							iCustomerReturnTotalQty = iCustomerReturnTotalQty + iCustomerReturnQty;
						}else {
							iCustomerReturnQty = 0;
						}
						if(oOrderReturnDetailsVO.getVendorReturnQuantity() != null) {
							iVendorReturnQty = Integer.parseInt(oOrderReturnDetailsVO.getVendorReturnQuantity());
							
							//Vendor Return total quantity is to show how many items vendor returned.
							iVendorReturnTotalQty = iVendorReturnTotalQty + iVendorReturnQty;
						}else {
							iVendorReturnQty = -1;
						}
						iTotalQuantity = iTotalQuantity - iCustomerReturnQty;
						if(iVendorReturnQty > -1) {
							iTotalQuantity = iTotalQuantity + (iCustomerReturnQty-iVendorReturnQty);
						}
					}
				}
				if(iTotalQuantity > 0) {
					oCustomerReturnDetailsVO = getRMAgenerated(oOrderItemDetailsVO.getOrderItemId(), oOrderItemDetailsVO.getOwnerType(), oOrderItemDetailsVO.getOwnerId(), oOrderItemDetailsVO.getCustTransId(), oCustomerLoginForm);
					oCustomerReturnDetailsVO.setOwnerType(oOrderItemDetailsVO.getOwnerType());
					if(alStates != null && oCustomerReturnDetailsVO != null) {
						for(StateVO oStateVO:alStates) {
							if(oStateVO.getStateCode() != null && oStateVO.getStateCode().equalsIgnoreCase(oCustomerReturnDetailsVO.getPartnerState())) {
								sState = oStateVO.getStateName();
							}
						}
					}
					sendRMAGeneratedEmailToCustomer(oOrderItemDetailsVO,oCustomerReturnDetailsVO,sState);
				}else {
					oCustomerReturnDetailsVO = getPartnerReturnDetails(oOrderItemDetailsVO.getOrderItemId(), oOrderItemDetailsVO.getOwnerType());
					request.setAttribute("RMAGeneratedFirst", "RMA has been already generated for this order item. ");
				}
				oSession.setAttribute("PartnerReturnDetails", oCustomerReturnDetailsVO);
				oSession.setAttribute("PartnerOrderDetails", oOrderItemDetailsVO);
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("CustomerLoginAction::getRMAGeneratedValue::EXCEPTION"+e.getMessage());
		}
		resetToken(request);
		logger.info("CustomerLoginAction::getRMAGeneratedValue::EXIT");
		
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward viewEmailPdf(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("CustomerLoginAction::viewEmailPdf::ENTRY");
		
		byte[] baEmailPdf=null;
		String sOrderItemId = null;
		String sStatus = null;
		try{
			sOrderItemId=request.getParameter("OrderItemId");
			sStatus = request.getParameter("Status");
			if(sOrderItemId!=null){
				baEmailPdf = OrderDAO.getOrderItemEmailPdf(sOrderItemId,INVOICE,sStatus);
			}
			if(baEmailPdf!=null){
				ServletOutputStream stream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(baEmailPdf.length);
				response.setHeader("Content-Disposition","Email" + ";filename=" + sOrderItemId);
				stream.write(baEmailPdf);
				stream.flush();
				stream.close();
			}

		}catch(Exception e){		
			logger.info("CustomerLoginAction::viewEmailPdf::EXCEPTION "+e.getMessage());
			return mapping.findForward("failure");		
		}
		
		logger.info("CustomerLoginAction::viewEmailPdf::EXIT");
		return null;
	}
	
	public ActionForward displayGeneratedRMAForm(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::displayGeneratedRMAForm::ENTRY");
		
		String sForwardKey = "GeneratedRMAFormSuccess";
		String sIsToSendEmail = request.getParameter("SendEmail");
		String sOrderItemId = request.getParameter("OrderItemId");
		String sReturnId = request.getParameter("ReturnId");
//		String sState = null;
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		HttpSession oSession = request.getSession();
//		List<StateVO> alStates = (ArrayList)oSession.getServletContext().getAttribute("StateList");
		List<OrderItemDetailsVO> orderItemDetailList = null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		orderItemDetailList = (List<OrderItemDetailsVO>)oSession.getAttribute("custOrderList");
		
		//To write a method in customerDAO.
		
		/*if(alStates != null && oCustomerReturnDetailsVO != null) {
			for(StateVO oStateVO:alStates) {
				if(oStateVO.getStateCode() != null && oStateVO.getStateCode().equalsIgnoreCase(oCustomerReturnDetailsVO.getPartnerState())) {
					sState = oStateVO.getStateName();
				}
			}
		}*/
		for(OrderItemDetailsVO oOrderItemDetailVO:orderItemDetailList) {
			if(oOrderItemDetailVO.getOrderItemId() != null && oOrderItemDetailVO.getOrderItemId().equalsIgnoreCase(sOrderItemId)) {
				oOrderItemDetailsVO = oOrderItemDetailVO;
				break;
			}
		}
		try{
			oCustomerReturnDetailsVO = getGeneratedRMADetails(sReturnId,oOrderItemDetailsVO.getOwnerType());	
			if("YES".equalsIgnoreCase(sIsToSendEmail)) {
				//Need to write a new scenario for the send the rma generated email to customer.
//				sendRMAGeneratedEmailToCustomer(oOrderItemDetailsVO,oSession,null,sState);
				request.setAttribute("EmailSent", "RMA form has been sent to your email-id.");
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("CustomerLoginAction::displayGeneratedRMAForm::EXCEPTION"+e.getMessage());
		}
		
		request.setAttribute("PartnerReturnDetails", oCustomerReturnDetailsVO);
		request.setAttribute("PartnerOrderDetails", oOrderItemDetailsVO);
		
		logger.info("CustomerLoginAction::displayGeneratedRMAForm::EXIT");
		
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward toPrintTheGeneratedRMAForm(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws ServletException {
		logger.info("CustomerLoginAction::toPrintTheGeneratedRMAForm::ENTRY");
		
		try{
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("CustomerLoginAction::toPrintTheGeneratedRMAForm::EXCEPTION"+e.getMessage());
		}
		
		
		logger.info("CustomerLoginAction::toPrintTheGeneratedRMAForm::EXIT");
		
		return null;
	}
	
	private void sendRMAGeneratedEmailToCustomer(OrderItemDetailsVO oOrderItemDetailsVO, 
			CustomerReturnDetailsVO oCustomerReturnDetailsVO, String p_sState) {
		
		logger.info("CustomerLoginAction::sendRMAGeneratedEmailToCustomer::EXIT");
		
		byte[] baPdf = null;
		String sFilePath = getServlet().getServletContext().getRealPath("/");
		try {
			String htmlText = createRMAGeneratedHTML(oOrderItemDetailsVO, oCustomerReturnDetailsVO,p_sState);
			String sCurrentTime = timeStampConversion(new Date());
			String sHTMLFileName = sFilePath+"RMAGenerated_Details"+sCurrentTime+".html";
	
			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(htmlText);
			oBufWriter.close();
	
			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);
	
			pd4ml.enableDebugInfo();
	
			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);
	
			baPdf=oByteArrayOutputStream.toByteArray();
	
			String pdfFileName=sFilePath+"RMAGenerated_Details"+sCurrentTime+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();
	
			File oPdfFile = new File(pdfFileName);
			File oHtmlFile = new File(sHTMLFileName);
			try {
				sendRMAGeneratedToCustomer(oOrderItemDetailsVO,oCustomerReturnDetailsVO,oPdfFile,oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName(),"");
			}catch(Exception e) {
				e.printStackTrace();
			}
	
			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
		}catch(Exception exception) {
			logger.info("CustomerLoginAction::sendRMAGeneratedEmailToCustomer::EXCEPTION"+exception.getMessage());
			exception.printStackTrace();
		}
		
		logger.info("CustomerLoginAction::sendRMAGeneratedEmailToCustomer::EXIT");
	}
	
	private File renderImage(String p_sOrderItemId, String p_sImageType) {
		logger.info("OrderAction::renderImage:ENTER");
		
		byte[] baOrderItemImage=null;
		String sImageViewPath = null;
		File oSrcFilePath = null;
		File oPdfFile = null;
		try{
			sImageViewPath = System.getProperty("ImageUploadPath");
			sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			
			sImageViewPath = sImageViewPath+"\\Customer\\";
			if(p_sOrderItemId!=null){
				baOrderItemImage = OrderDAO.getOrderItemImage(p_sOrderItemId);
			}
			if(baOrderItemImage!=null){
				oSrcFilePath = new File(sImageViewPath);
				if(!oSrcFilePath.exists()) {
					oSrcFilePath.mkdirs();
				}
				
				String pdfFileName=sImageViewPath+p_sOrderItemId+"."+p_sImageType;
				FileOutputStream fileos = new FileOutputStream(pdfFileName);
				fileos.write(baOrderItemImage);
				fileos.flush();
				fileos.close();
				
				oPdfFile = new File(pdfFileName);
			}

			logger.info("OrderAction::renderImage:EXIT");
		}catch(Exception e){		
			logger.error("OrderAction::renderImage:EXCEPTION "+e.getMessage());
		}	
		return oPdfFile;
	}
}
