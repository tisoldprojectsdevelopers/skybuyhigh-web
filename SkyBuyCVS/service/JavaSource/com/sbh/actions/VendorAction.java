package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.dao.OrderDAO.decryptInputData;
import static com.sbh.dao.OrderDAO.encryptData;
import static com.sbh.dao.OrderDAO.encryptInputData;
import static com.sbh.dao.VendorDAO.getVendorComments;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.sbh.dao.AirlineDAO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.ImageScale;
import com.sbh.dao.VendorDAO;
import com.sbh.forms.ProductDetailsForm;
import com.sbh.forms.SearchVendorForm;
import com.sbh.forms.VendorDetailsForm;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateProdCatalogue;
import com.sbh.vo.KeyVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerRegistrationCommentsVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.VendorDetailsVO;




public class VendorAction extends DispatchAction{
	private static Logger logger = LogManager.getLogger(VendorAction.class);


	public ActionForward initAddVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::initAddVendor:ENTER");
		String sReturnPolicy="";
		HttpSession oSession=request.getSession(true); 
		String sFrdKey="initAddVendorSuccess";
		request.removeAttribute("vendorDetails");
		oSession.removeAttribute("vendorDetails");
		oSession.removeAttribute("vendorDetailsForm");
		
		request.setAttribute("mode","Add" );
		VendorDetailsForm oVendorDetailsForm=(VendorDetailsForm)form;
		
		sReturnPolicy=System.getProperty("ReturnPolicy").trim();
		oVendorDetailsForm.setVendReturnPolicy(sReturnPolicy);
		request.setAttribute("vendorDetails",oVendorDetailsForm);
		saveToken(request);
		logger.info("VendorAction::initAddVendor:EXIT");

		return mapping.findForward(sFrdKey);		
	}
	public ActionForward addVendorDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::addVendorDetails:ENTER");
		String sFrdKey="addVendorSuccess";
		HttpSession oSession=request.getSession(true);
		List<PartnerRegistrationCommentsVO> alVendorComments = null;
		ArrayList alVendorRegStatus = new ArrayList();
		String sFileName = null, sURI = null, sPath = null, sOwnerType = null, sOwnerId = null;
		String sVendorEncryptPassword = null;
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;
		long lVendId =0;
		try{
			String sImageViewPath = System.getProperty("ImageViewPath").trim();	
			sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			
			VendorDetailsForm oVendorDetailsForm=(VendorDetailsForm)form;
			String sImageUploadPath = System.getProperty("ImageUploadPath").trim();	
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			String sSrcImgPath = sImageUploadPath+"\\AirlineLogo\\OriginalImg\\";
			sOwnerType = VENDOR;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			

			if(isTokenValid(request)){
				String sVendName = oVendorDetailsForm.getVendName();
				sVendName =sVendName==null?"":sVendName.trim();
				
				String sVendContactName2=oVendorDetailsForm.getVendContactName2();
				sVendContactName2 =sVendContactName2==null?"":sVendContactName2.trim();

				String sVendUserId = oVendorDetailsForm.getVendUserId();
				sVendUserId =sVendUserId==null?"":sVendUserId.trim();
				
				String sVendorPassword = oVendorDetailsForm.getVendPassword();
				sVendorPassword =sVendorPassword==null?"":sVendorPassword.trim();
				
				alVendorRegStatus = VendorDAO.validateVendor(sVendName,sVendUserId,"0","Add");	

				int iVendorNameCount =(Integer) alVendorRegStatus.get(0);
				int iVendorUserIdCount =(Integer) alVendorRegStatus.get(1);
				
				if(iVendorNameCount==0 && iVendorUserIdCount ==0){
					String sVendContactName = oVendorDetailsForm.getVendContactName();
					sVendContactName = sVendContactName ==null?"":sVendContactName.trim();
					oVendorDetailsForm.setVendContactName(CatalogueDAO.toTitleCase(sVendContactName));
				
					String sReturnPolicy = oVendorDetailsForm.getVendReturnPolicy();
					sReturnPolicy =sReturnPolicy ==null?"":sReturnPolicy.trim();
					oVendorDetailsForm.setVendReturnPolicy(sReturnPolicy);
					
					String sComments = oVendorDetailsForm.getVendComments();
					sComments =sComments ==null?"":sComments.trim();
					oVendorDetailsForm.setVendComments(sComments);
					
					KeyVO oKeyVO = encryptData(oVendorDetailsForm.getVendPassword(),"SERVER","");
					if(oKeyVO != null) {
						sVendorEncryptPassword = oKeyVO.getEncryptedData();
					}
					sVendorEncryptPassword = sVendorEncryptPassword == null?"":sVendorEncryptPassword.trim();
					oVendorDetailsForm.setVendPassword(sVendorEncryptPassword);
					
					//To Insert vendor details those are captured.
					lVendId = VendorDAO.addVendorDetails(oVendorDetailsForm,oLoginVO.getUserId(),oKeyVO.getRefId());
					//To get the vendor comments which entered.
					alVendorComments = getVendorComments(lVendId+"",VENDOR);
					
					// Insert user audit details
					String sInsertedUserDetails = VendorDAO.getInsertedVendorDetails(oVendorDetailsForm);
					VendorDAO.insertUserAuditDetails(lVendId+"",VENDOR, sInsertedUserDetails,null,null,oVendorDetailsForm.getVendComments(),ADMIN,"VENDOR ADDED");
					
					FormFile oAboutMeFormFile = oVendorDetailsForm.getAboutMe();
					if(lVendId!=0){
						oVendorDetailsForm.setVendId(lVendId+"");
						sOwnerId = lVendId+"";
						sFileName = sOwnerId;
						if(oAboutMeFormFile!= null && !oAboutMeFormFile.getFileName().equals("")){
							alImageInfo = ImageScale.createAboutMeFile(oAboutMeFormFile,sImageUploadPath,sFileName,sOwnerType,sOwnerId);
							if(alImageInfo !=null){
								oVendorDetailsForm.setAboutMeType((String)alImageInfo.get(0));
								oVendorDetailsForm.setAboutMePath((String)alImageInfo.get(1));
							}
							AirlineDAO.updatePartnerAboutMeDetails(sOwnerType, sOwnerId,oVendorDetailsForm.getAboutMeType(), oLoginVO.getUserId());
						}
					}
					
					VendorDetailsVO oVendorDetailsVO = VendorDAO.getVendorDetailsById(lVendId+"",oLoginVO.getId());
					
					BeanUtils.copyProperties(oVendorDetailsForm,oVendorDetailsVO); 
					
					oVendorDetailsForm.setVendPhone1(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendPhone1()));
					oVendorDetailsForm.setVendPhone2(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendPhone2()));
					oVendorDetailsForm.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getCustServicePhoneno()));
					oVendorDetailsForm.setVendFax(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendFax()));			
					
					//send login details to vendor through mail
					VendorDAO.sendEmail(oVendorDetailsForm.getVendName(),oVendorDetailsForm.getVendContactName(),oVendorDetailsForm.getVendUserId(),sVendorPassword,oVendorDetailsForm.getVendEmail(),oVendorDetailsForm.getVendEmail2(),lVendId,VENDOR,oLoginVO.getUserId(),"");
				
					if(oVendorDetailsForm.getVendStatus()!=null && oVendorDetailsForm.getVendStatus().equalsIgnoreCase("I")){
						//send login details to vendor through mail
						VendorDAO.sendStatusEmail(oVendorDetailsForm.getVendStatus(),oVendorDetailsForm.getVendName(),oVendorDetailsForm.getVendContactName(),oVendorDetailsForm.getVendEmail(),lVendId,VENDOR,oLoginVO.getUserId(),"");
					}
				}
				else{
					if(iVendorNameCount>0 && iVendorUserIdCount>0)				
						request.setAttribute("vendorNamevendorUserIdExist","true");				
					else if(iVendorNameCount>0)				
						request.setAttribute("vendNameExist","true");				
					else if(iVendorUserIdCount>0)				
						request.setAttribute("vendorUserIdExist","true");			
					
					sFrdKey="addVendorFaiure";
				}
				if(oVendorDetailsForm!=null){
					if(oVendorDetailsForm.getAboutMePath()!=null && oVendorDetailsForm.getAboutMePath().trim().length()>0)
						oVendorDetailsForm.setAboutMePath(sImageViewPath+oVendorDetailsForm.getAboutMePath()+"/"+oVendorDetailsForm.getVendId()+"."+oVendorDetailsForm.getAboutMeType());			
					else
						oVendorDetailsForm.setAboutMePath("");
					
					if(oVendorDetailsForm.getAboutMePath()!=null && !oVendorDetailsForm.getAboutMePath().equalsIgnoreCase("")){
						oSession.setAttribute("AboutMeFileName", oVendorDetailsForm.getAboutMePath());
					}else{
						oSession.removeAttribute("AboutMeFileName");
					}
				}
			}
			
			resetToken(request);
			request.setAttribute("mode","Add" );
			request.setAttribute("vendorDetails",oVendorDetailsForm);
			if(alVendorComments != null && alVendorComments.size() > 0) {
				request.setAttribute("VendorComments",alVendorComments);
			}

			logger.info("VendorAction::addVendorDetails:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors();			
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::addVendorDetails:Exception "+e.getMessage());
		}

		return mapping.findForward(sFrdKey);		
	}
	public ActionForward initSearchVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("VendorAction::initSearchVendor:ENTER");    	  
		String sFwrdKey="initSearchVendorSuccess";   
		HttpSession oSession=request.getSession(true); 
		SearchVendorForm oSearchVendorForm=(SearchVendorForm)form; 	
		try{	
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchVendorForm");
			// System.out.println("Server path :"+getServlet().getServletContext().getRealPath(""));
		}catch(Exception e){
			ActionErrors oErrors = new ActionErrors(); 
			oErrors.add(Globals.ERROR_KEY,new ActionError("error.db",e.getMessage()));
			saveErrors(request,oErrors);
			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("VendorAction::initSearchVendor:Exception "+e.getMessage());
		}
		logger.info("VendorAction::initSearchVendor:EXIT");
		return mapping.findForward(sFwrdKey);        		
	}


	public ActionForward searchVendor(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::searchVendor:ENTER");
		String sFrdKey="searchVendorSuccess";
		ArrayList alProduInfo = null;	
		SearchVendorForm oSearchVendorForm=(SearchVendorForm)form; 	

		String sSearchBy=null,sSearchValue = null , sCategory =null,sCategoryDate = null;
		try{
			if(oSearchVendorForm!=null){
				sSearchBy = oSearchVendorForm.getVendorSearchBy();
				sSearchBy=sSearchBy==null?"":sSearchBy.trim();
				oSearchVendorForm.setVendorSearchBy(sSearchBy);

				sSearchValue = oSearchVendorForm.getVendorSearchValue();
				sSearchValue=sSearchValue==null?"":sSearchValue.trim();
				oSearchVendorForm.setVendorSearchValue(sSearchValue);			

			}			

			alProduInfo = VendorDAO.getVendorDetails(oSearchVendorForm);
			if(alProduInfo!=null && alProduInfo.size()>0){
				request.setAttribute("productInfo", alProduInfo);
			}else{
				request.setAttribute("NoRecords","noRecords");
			}  
			logger.info("VendorAction::searchVendor:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors(); 
			//errors.add(Globals.ERROR_KEY,new ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::searchVendor:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward EditVendorDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::EditVendorDetails:ENTER");
		String sFrdKey="editVendorSuccess",sURI=null,sPath=null;
		String sSourceOfEdit = null;
		VendorDetailsVO oVendorDetailsVO = null;	
		VendorDetailsForm oVendorDetailsForm=(VendorDetailsForm)form; 		
		LoginVO oLoginVO = null;
		HttpSession oSession=request.getSession(true);
		try{
			String sImageViewPath = System.getProperty("ImageViewPath").trim();	
			sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			sSourceOfEdit = request.getParameter("SourceOfEdit");
			sSourceOfEdit = sSourceOfEdit==null?"":sSourceOfEdit.trim();
			
			String sVendId=request.getParameter("VendId");		
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");


			if(oLoginVO!=null){
				if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
					sFrdKey="editAdminVendorSuccess";			
			}			

			oVendorDetailsVO = VendorDAO.getVendorDetailsById(sVendId,oLoginVO.getId());
			if(oVendorDetailsVO!=null){
				if(oVendorDetailsVO.getVendPassword() != null && oVendorDetailsVO.getVendPassword().trim().length() > 0) {
					String sDecryptedVendorPassword = decryptInputData(oVendorDetailsVO.getVendPassword(),"SERVER",oVendorDetailsVO.getKeyRefId());
					oVendorDetailsVO.setVendPassword(sDecryptedVendorPassword);
				}
				if(oVendorDetailsVO.getAboutMePath()!=null && oVendorDetailsVO.getAboutMePath().trim().length()>0)
					oVendorDetailsVO.setAboutMePath(sImageViewPath+oVendorDetailsVO.getAboutMePath()+"/"+oVendorDetailsVO.getVendId()+"."+oVendorDetailsVO.getAboutMeType());			
				else
					oVendorDetailsVO.setAboutMePath("");
				
				if(oVendorDetailsVO.getAboutMePath()!=null && !oVendorDetailsVO.getAboutMePath().equalsIgnoreCase("")){
					oSession.setAttribute("AboutMeFileName", oVendorDetailsVO.getAboutMePath());
				}else{
					oSession.removeAttribute("AboutMeFileName");
				}
				BeanUtils.copyProperties(oVendorDetailsForm,oVendorDetailsVO); 
				oSession.setAttribute("vendorDetails",oVendorDetailsVO);
				request.setAttribute("mode","Edit" );
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
			} 
			logger.info("VendorAction::EditVendorDetails:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors(); 
			//errors.add(Globals.ERROR_KEY,new ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::EditVendorDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	public ActionForward UpdateVendorDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::UpdateVendorDetails:ENTER");
		String sFrdKey="updateVendorSuccess",sURI=null,sPath=null;
		String sVendorPassword = null;
		KeyVO oKeyVO = null;
		VendorDetailsForm oVendorDetailsForm=(VendorDetailsForm)form; 		
		LoginVO oLoginVO = null;
		VendorDetailsVO oVendorDetailsVO = null;	
		VendorDetailsVO oNewVendorDetailsVO = null;
		HttpSession oSession=request.getSession(true);
		List<PartnerRegistrationCommentsVO> alVendorComments = null;
		ArrayList alVendorRegStatus = new ArrayList();
		long lVendId =0;
		String sOwnerType = null;
		String sOwnerId = null,sFileName = null;
		ArrayList alImageInfo = null;
		try{		
			String sImageViewPath = System.getProperty("ImageViewPath").trim();	
			sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			
			String sImageUploadPath = System.getProperty("ImageUploadPath").trim();	
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			sOwnerType = VENDOR;
			
			
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");

			if(oLoginVO!=null){
				if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
					sFrdKey="updateAdminVendorSuccess";			
			}
			oVendorDetailsVO =(VendorDetailsVO) oSession.getAttribute("vendorDetails");
			
			String sVendId = oVendorDetailsForm.getVendId();
			sVendId =sVendId==null?"":sVendId.trim();
			
			if(sVendId.trim().length()>0)
				lVendId = Long.parseLong(sVendId);

			String sUserType = oVendorDetailsForm.getVendId();

			String sVendName = oVendorDetailsForm.getVendName();
			sVendName =sVendName==null?"":sVendName.trim();

			String sVendUserId = oVendorDetailsForm.getVendUserId();
			sVendName =sVendName==null?"":sVendName.trim();
			
			String sVendPassword = oVendorDetailsForm.getVendPassword();
			sVendPassword =sVendPassword==null?"":sVendPassword.trim();
			
			String sVendStatus = oVendorDetailsForm.getVendStatus();
			sVendStatus =sVendStatus==null?"":sVendStatus.trim();
			
			String sVendContactName2=oVendorDetailsForm.getVendContactName2();
			sVendContactName2 =sVendContactName2==null?"":sVendContactName2.trim();

			alVendorRegStatus = VendorDAO.validateVendor(sVendName,sVendUserId,sVendId,"Update");	

			int iVendorNameCount =(Integer) alVendorRegStatus.get(0);
			int iVendorUserIdCount =(Integer) alVendorRegStatus.get(1);

			if(iVendorNameCount ==0 && iVendorUserIdCount == 0){
				
				String sVendContactName = oVendorDetailsForm.getVendContactName();
				sVendContactName = sVendContactName ==null?"":sVendContactName.trim();
				oVendorDetailsForm.setVendContactName(CatalogueDAO.toTitleCase(sVendContactName));
				
				String sOldVendUserId = oVendorDetailsVO.getVendUserId();
				sOldVendUserId = sOldVendUserId ==null?"":sOldVendUserId.trim();
				
				String sOldVendPassword = oVendorDetailsVO.getVendPassword();
				sOldVendPassword = sOldVendPassword ==null?"":sOldVendPassword.trim();
				
				String sOldVendStatus = oVendorDetailsVO.getVendStatus();
				sOldVendStatus = sOldVendStatus == null?"":sOldVendStatus.trim();
				
				String sReturnPolicy = oVendorDetailsForm.getVendReturnPolicy();
				sReturnPolicy =sReturnPolicy ==null?"":sReturnPolicy.trim();
				oVendorDetailsForm.setVendReturnPolicy(sReturnPolicy);
				
				String sComments = oVendorDetailsForm.getVendComments();
				sComments =sComments ==null?"":sComments.trim();
				oVendorDetailsForm.setVendComments(sComments);
				
				oKeyVO = encryptData(oVendorDetailsForm.getVendPassword(),"SERVER",oVendorDetailsVO.getKeyRefId());
				if(oKeyVO != null) 
					sVendorPassword = oKeyVO.getEncryptedData();
				
				sVendorPassword = sVendorPassword==null?"":sVendorPassword.trim();
				oVendorDetailsForm.setVendPassword(sVendorPassword);
				
				FormFile oAboutMeFormFile = oVendorDetailsForm.getAboutMe();
				//To update the vendor details
				int iIsUpdate= VendorDAO.updateVendorDetails(oVendorDetailsForm,oLoginVO.getUserType(),oLoginVO.getUserId(),oLoginVO.getId(),oKeyVO.getRefId());
				sOwnerId = oVendorDetailsForm.getVendId();
				sFileName = sOwnerId;
				if(oAboutMeFormFile!= null && !oAboutMeFormFile.getFileName().equals("")){
					alImageInfo = ImageScale.createAboutMeFile(oAboutMeFormFile,sImageUploadPath,sFileName,sOwnerType,sOwnerId);
					if(alImageInfo !=null){
						oVendorDetailsForm.setAboutMeType((String)alImageInfo.get(0));
						oVendorDetailsForm.setAboutMePath((String)alImageInfo.get(1));
						
					}
					AirlineDAO.updatePartnerAboutMeDetails(sOwnerType, sOwnerId,oVendorDetailsForm.getAboutMeType(), oLoginVO.getUserId());
				}else{
					oVendorDetailsForm.setAboutMeType(oVendorDetailsVO.getAboutMeType());
					oVendorDetailsForm.setAboutMePath(oVendorDetailsVO.getAboutMePath());
				}
				//To get the comments of the vendor.
				alVendorComments = getVendorComments(sVendId,VENDOR);
				
				//send mail to vendor if admin chage Userid/password
				//if(oLoginVO.getUserType().equalsIgnoreCase(ADMIN)){
					if(oVendorDetailsForm.getVendStatus()!=null && oVendorDetailsForm.getVendStatus().equalsIgnoreCase("A")){	
						if(!sVendUserId.equals(sOldVendUserId) || !sVendPassword.equals(sOldVendPassword))
							VendorDAO.sendEditUserDetailsByEmail(oVendorDetailsForm.getVendName(),oVendorDetailsForm.getVendContactName(),sVendUserId,sVendPassword,sOldVendUserId,sOldVendPassword,oVendorDetailsForm.getVendEmail(),oVendorDetailsForm.getVendEmail2(),lVendId,VENDOR,oLoginVO.getUserId(),"",oLoginVO.getUserType());
					}
				//}
				
				if(!sVendStatus.equalsIgnoreCase(sOldVendStatus)){					
					VendorDAO.sendStatusEmail(oVendorDetailsForm.getVendStatus(),oVendorDetailsForm.getVendName(),oVendorDetailsForm.getVendContactName(),oVendorDetailsForm.getVendEmail(),lVendId,VENDOR,oLoginVO.getUserId(),"");
				}
				
				//Insert User Audit details
				VendorDAO.getUpdatedUserDetails(oVendorDetailsForm,oVendorDetailsVO,oLoginVO,oKeyVO.getRefId());
				
				oNewVendorDetailsVO = VendorDAO.getVendorDetailsById(sVendId,oLoginVO.getId());
				BeanUtils.copyProperties(oVendorDetailsForm, oNewVendorDetailsVO);
				
				
				oVendorDetailsForm.setVendPhone1(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendPhone1()));
				oVendorDetailsForm.setVendPhone2(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendPhone2()));
				oVendorDetailsForm.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getCustServicePhoneno()));
				oVendorDetailsForm.setVendFax(AirlineDAO.getPhoneFormat(oVendorDetailsForm.getVendFax()));
			}else{
				if(iVendorNameCount>0 && iVendorUserIdCount>0)				
					request.setAttribute("vendorNamevendorUserIdExist","true");				
				else if(iVendorNameCount>0)				
					request.setAttribute("vendNameExist","true");				
				else if(iVendorUserIdCount>0)				
					request.setAttribute("vendorUserIdExist","true");			
				

				if(oLoginVO!=null){
					if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equals(ADMIN))
						sFrdKey="updateAdminVendorFailure";	
					else
						sFrdKey="updateVendorFailure";

				}	
			}			

			request.setAttribute("mode","Edit" );
			
			if(oVendorDetailsForm.getAboutMePath()!=null && oVendorDetailsForm.getAboutMePath().trim().length()>0)
				oVendorDetailsForm.setAboutMePath(sImageViewPath+oVendorDetailsForm.getAboutMePath()+"/"+oVendorDetailsForm.getVendId()+"."+oVendorDetailsForm.getAboutMeType());			
			else
				oVendorDetailsForm.setAboutMePath("");
			
			if(oVendorDetailsForm.getAboutMePath()!=null && !oVendorDetailsForm.getAboutMePath().equalsIgnoreCase("")){
				oSession.setAttribute("AboutMeFileName", oVendorDetailsForm.getAboutMePath());
			}else{
				oSession.removeAttribute("AboutMeFileName");
			}
			request.setAttribute("vendorDetails",oVendorDetailsForm);
			if(alVendorComments != null && alVendorComments.size() > 0) {
				for(PartnerRegistrationCommentsVO oPartnerRegistrationCommentsVO:alVendorComments) {
					if(oPartnerRegistrationCommentsVO.getComments() == null || oPartnerRegistrationCommentsVO.getComments().trim().length() == 0) {
						oPartnerRegistrationCommentsVO.setComments(" ");
					}
				}
				request.setAttribute("VendorComments",alVendorComments);
			}
			logger.info("VendorAction::UpdateVendorDetails:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors(); 
			//errors.add(Globals.ERROR_KEY,new ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::UpdateVendorDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}

	public ActionForward PreviewProductDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::PreviewProductDetails:ENTER");
		String sFrdKey="previewProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;	
		ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form; 		
		GenerateProdCatalogue oGenerateProdCatalogue = new GenerateProdCatalogue();
		try{
			String sProdId=request.getParameter("ProdId");			
			String sServerPath = getServlet().getServletContext().getRealPath("");		
			//GenerateProdCatalogue.createXML(oProductDetailsForm,sServerPath);			

			logger.info("VendorAction::PreviewProductDetails:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors(); 
			//errors.add(Globals.ERROR_KEY,new ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::PreviewProductDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}


	
	/* View Vendor Details */
	
	
	public ActionForward viewVendorDetails(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		logger.info("VendorAction::viewVendorDetails:ENTER");
		String sFrdKey="viewAdminVendorSuccess",sURI=null,sPath=null;
		VendorDetailsVO oVendorDetailsVO = null;	
		VendorDetailsForm oVendorDetailsForm=(VendorDetailsForm)form; 		
		LoginVO oLoginVO = null;
		List<PartnerRegistrationCommentsVO> alVendorComments = new ArrayList<PartnerRegistrationCommentsVO>();
		HttpSession oSession=request.getSession(true);
		try{
			String sImageViewPath = System.getProperty("ImageViewPath").trim();	
			sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			
			String sVendId=request.getParameter("VendId");		
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(VENDOR)){
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
				sFrdKey="viewVendorSuccess";
			}
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			
			oVendorDetailsVO = VendorDAO.getVendorDetailsById(sVendId,oLoginVO.getId());
			if(oVendorDetailsVO!=null){
				if(oVendorDetailsVO.getVendPassword() != null && oVendorDetailsVO.getVendPassword().trim().length() > 0) {
					String sDecryptedVendorPassword = decryptInputData(oVendorDetailsVO.getVendPassword(),"SERVER",oVendorDetailsVO.getKeyRefId());
					oVendorDetailsVO.setVendPassword(sDecryptedVendorPassword);
				}
				if(oVendorDetailsVO.getAboutMePath()!=null && oVendorDetailsVO.getAboutMePath().trim().length()>0)
					oVendorDetailsVO.setAboutMePath(sImageViewPath+oVendorDetailsVO.getAboutMePath()+"/"+oVendorDetailsVO.getVendId()+"."+oVendorDetailsVO.getAboutMeType());			
				else
					oVendorDetailsVO.setAboutMePath("");
				
				if(oVendorDetailsVO.getAboutMePath()!=null && !oVendorDetailsVO.getAboutMePath().equalsIgnoreCase("")){
					oSession.setAttribute("AboutMeFileName", oVendorDetailsVO.getAboutMePath());
				}else{
					oSession.removeAttribute("AboutMeFileName");
				}
				BeanUtils.copyProperties(oVendorDetailsForm,oVendorDetailsVO); 
				
				oVendorDetailsVO.setVendPhone1(AirlineDAO.getPhoneFormat(oVendorDetailsVO.getVendPhone1()));
				oVendorDetailsVO.setVendPhone2(AirlineDAO.getPhoneFormat(oVendorDetailsVO.getVendPhone2()));
				oVendorDetailsVO.setCustServicePhoneno(AirlineDAO.getPhoneFormat(oVendorDetailsVO.getCustServicePhoneno()));
				oVendorDetailsVO.setVendFax(AirlineDAO.getPhoneFormat(oVendorDetailsVO.getVendFax()));
				
				oSession.setAttribute("vendorDetails",oVendorDetailsVO);
				request.setAttribute("mode","Edit" );
				
				alVendorComments = getVendorComments(sVendId,VENDOR);
				if(alVendorComments != null && alVendorComments.size() > 0) {
					for(PartnerRegistrationCommentsVO oPartnerRegistrationCommentsVO:alVendorComments) {
						if(oPartnerRegistrationCommentsVO.getComments() == null || oPartnerRegistrationCommentsVO.getComments().trim().length() == 0) {
							oPartnerRegistrationCommentsVO.setComments(" ");
						}
					}
					request.setAttribute("VendorComments",alVendorComments);
				}
			} 
			logger.info("VendorAction::viewVendorDetails:EXIT");
		}catch(Exception e){		
			ActionErrors errors = new ActionErrors(); 
			//errors.add(Globals.ERROR_KEY,new ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			saveErrors(request, errors);
			sFrdKey=("failure");
			e.printStackTrace();
			logger.error("VendorAction::viewVendorDetails:EXCEPTION "+e.getMessage());
		}	
		return mapping.findForward(sFrdKey);		
	}
	
	
}
