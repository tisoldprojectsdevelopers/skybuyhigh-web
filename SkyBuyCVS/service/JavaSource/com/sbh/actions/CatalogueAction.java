package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_CLIENTCERT;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SCHEMA;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SERVICE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_USERNAME;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_VIRTUALDESKTOP;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.dao.CatalogueDAO.getDeviceType;
import static com.sbh.dao.DeviceRegDAO.insertAutoUpdateDeviceLogDetails;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.insertPackageDownloadLogDetails;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.isDownloadCatalogueAvailable;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.isUpdatedVersion;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateLatestVersionandStatus;
import static com.sbh.util.Utils.deleteFiles;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.sbh.client.dao.PlaceOrderDAO;
import com.sbh.client.vo.UpdatedCatalogueDetailsVO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.DeviceRegDAO;
import com.sbh.dao.ImageScale;
import com.sbh.dao.MerchandizeDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.forms.ProductDetailsForm;
import com.sbh.forms.SearchCatalogueForm;
import com.sbh.forms.SearchMerchandizeForm;
import com.sbh.forms.UploadImageForm;
import com.sbh.util.GenerateCatalogueZip;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;
import com.sbh.util.generatexml.GenerateProdCatalogue;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CommentsVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.ProductDetailsVO;

public class CatalogueAction extends DispatchAction {
	// public static String sSkyBuyVendId = "38";
	private static Logger logger = LogManager.getLogger(CatalogueAction.class);

	// Vendor Product
	public ActionForward initAddVendorProduct(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initAddVendorProduct:ENTER");
		String sFrdKey = "initAddProductSuccess";
		// ArrayList alVendorList =null;
		HttpSession oSession = request.getSession();
		// ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form;
		try {
			request.setAttribute("mode", "ProductAdd");
			oSession.removeAttribute("vendProdId");
			oSession.removeAttribute("productDetailsForm");
			// saveToken(request);
			logger.info("CatalogueAction::initAddVendorProduct:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::initAddVendorProduct:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward addVendorProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addVendorProductDetails:ENTER");
		String sFrdKey = "addProductSuccess";
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		boolean bIsProdCodeExist = false;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		String sMode = "";
		String sFullDescription = null;
		String sShortDescription = null;

		try {
			// if(isTokenValid(request)){
			sFullDescription = request.getParameter("fullDescription");
			sShortDescription = request.getParameter("shortDescription");
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			String sVendProdId = (String) oSession.getAttribute("vendProdId");
			sVendProdId = sVendProdId == null ? "" : sVendProdId.trim();
			if (sVendProdId.trim().length() > 0) {
				sMode = "UPDATE";
				oProductDetailsForm.setProdId(sVendProdId);
			} else
				sMode = "INSERT";

			if (sMode.equalsIgnoreCase("INSERT")) {
				bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oLoginVO
						.getUserType(), oLoginVO.getRefId(),
						oProductDetailsForm.getProdCode(), null);
			}
			if (!bIsProdCodeExist) {
				if (oProductDetailsForm != null) {
					if (oProductDetailsForm.getSbhComment() == null)
						oProductDetailsForm.setSbhComment("");

					if (oProductDetailsForm.getBrandName() == null)
						oProductDetailsForm.setBrandName("");
					else
						oProductDetailsForm.setBrandName(oProductDetailsForm
								.getBrandName().trim());

					/*
					 * if(oProductDetailsForm.getValidFromDate()==null)
					 * oProductDetailsForm.setValidFromDate(""); else
					 * oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());
					 * 
					 * if(oProductDetailsForm.getValidToDate()==null)
					 * oProductDetailsForm.setValidToDate(""); else
					 * oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());
					 */

					if (oProductDetailsForm.getInstructions() == null)
						oProductDetailsForm.setInstructions("");
					else
						oProductDetailsForm.setInstructions(oProductDetailsForm
								.getInstructions().trim());

					if (oProductDetailsForm.getLongDesc() == null)
						oProductDetailsForm.setLongDesc("");
					else
						oProductDetailsForm
								.setLongDesc(sFullDescription.trim());

					if (oProductDetailsForm.getShortDesc() == null)
						oProductDetailsForm.setShortDesc("");
					else
						oProductDetailsForm.setShortDesc(sShortDescription
								.trim());

					int iProdId = CatalogueDAO.addProductDetails(
							oProductDetailsForm, oLoginVO, sMode);
					oProductDetailsForm.setProdId(iProdId + "");

					/** ** Insert Product audit details *** */
					String sInsertedItemDetails = CatalogueDAO
							.getInsertedItemDetails(oProductDetailsForm,
									oLoginVO.getUserType());
					CatalogueDAO.insertProdAuditDetails(iProdId + "", oLoginVO
							.getRefId(), oLoginVO.getUserType(),
							sInsertedItemDetails, null, null, null, oLoginVO
									.getUserId(), "PRODUCT ADDED");

					BeanUtils.copyProperties(oProductDetailsVO,
							oProductDetailsForm);
					if (oProductDetailsVO != null)
						oProductDetailsVO.setOwnerType(oLoginVO.getUserType());

					oSession.setAttribute("vendorProductDetails",
							oProductDetailsVO);
					oSession.setAttribute("vendorProductFormDetails",
							oProductDetailsForm);
					oSession.setAttribute("vendProdId", iProdId + "");
				}
				// resetToken(request);
				// }
			} else {
				sFrdKey = "initAddProductSuccess";
				request.setAttribute("errorMsg",
						"Product Code is already exist");
			}

			logger.info("CatalogueAction::addVendorProductDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::addVendorProductDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward addUploadVendorProductImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addUploadVendorProductImage:ENTER");

		String sFrdKey = "addProductSuccess";
		String sProductStatus = "";
		ProductDetailsVO oProductDetailsVO = null;
		HttpSession oSession = request.getSession();
		List<CategoryVO> alCategory = null;
		UploadImageForm oUploadImageForm = (UploadImageForm) form;
		String sCateFolderName = null, sImageName = null;
		// GenerateProdCatalogue oGenerateProdCatalogue = new
		// GenerateProdCatalogue();
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;
		ProductDetailsForm oProductDetailsForm = null;

		// String sMainFileName= null,sMainImageType = null,sView1FileName =
		// null,sView1ImageType = null ,sImgType = null;
		// String sView2FileName = null,sView2ImageType = null,sView3FileName =
		// null,sView3ImageType = null;
		try {
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");

			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("vendorProductDetails");
			oProductDetailsForm = (ProductDetailsForm) oSession
					.getAttribute("vendorProductFormDetails");
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");

			String sImageUploadPath = System.getProperty("ImageUploadPath")
					.trim();
			String sImageViewPath = System.getProperty("ImageViewPath").trim();

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCatalogueDate = dateFormat.format(date);
			oProductDetailsVO.setCategoryDate(sCatalogueDate);

			String sCateId = oProductDetailsVO.getCateId();
			String sOwnerId = oProductDetailsVO.getOwnerId();
			String sOwnerType = oProductDetailsVO.getOwnerType();
			String sProdId = oProductDetailsVO.getProdId();

			String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
					+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
			sImageName = sOwnerId + sProdId;

			if (alCategory != null && alCategory.size() > 0) {
				for (CategoryVO oCategoryVO : alCategory) {
					if (oCategoryVO.getCateId() != null) {
						if (oCategoryVO.getCateId().equals(sCateId))
							sCateFolderName = oCategoryVO.getCateName();
					}
				}
			}

			FormFile oMainFormFile = oUploadImageForm.getMainImgPath();
			FormFile oView1FormFile = oUploadImageForm.getView1ImgPath();
			FormFile oView2FormFile = oUploadImageForm.getView2ImgPath();
			FormFile oView3FormFile = oUploadImageForm.getView3ImgPath();

			// Create Large,thumb,medium images
			if (oMainFormFile != null
					&& !oMainFormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oMainFormFile,
						sImageViewPath, sImageUploadPath, sImageName, "main",
						"Main", sCateFolderName, sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setMainImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setMainImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setMainImgCap(oUploadImageForm
						.getMainImgCap());
			}
			if (oView1FormFile != null
					&& !oView1FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView1FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view1",
						"View1", sCateFolderName, sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView1ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView1ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView1ImgCap(oUploadImageForm
						.getView1ImgCap());
			}
			if (oView2FormFile != null
					&& !oView2FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView2FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view2",
						"View2", sCateFolderName, sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView2ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView2ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView2ImgCap(oUploadImageForm
						.getView2ImgCap());
			}
			if (oView3FormFile != null
					&& !oView3FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView3FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view3",
						"View3", sCateFolderName, sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView3ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView3ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView3ImgCap(oUploadImageForm
						.getView3ImgCap());
			}

			sProductStatus = CatalogueDAO.getProductStatus(sProdId);
			oProductDetailsVO.setSbhProdStatus(sProductStatus);
			oSession.setAttribute("vendorProductDetails", oProductDetailsVO);

			CatalogueDAO.updateProductImage(oProductDetailsVO, oLoginVO);

			//oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId, oLoginVO.getUserType());
			// send mail to admin when a new product is added
			
			CatalogueDAO.sendAddProdInfoByEmail(oProductDetailsForm,
					oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
					sCateFolderName);

			// send a mail to vendor if the product status inactive
			if (oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I")) {
				CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
						oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
						sCateFolderName);
			}

			/*
			 * String sDestPath = System.getProperty("GenerateXmlPath").trim();
			 * String sProdXmlContent =
			 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
			 * String xmlContent = sProdXmlContent.replaceAll("\"", "&quot;"); //
			 * System.out.println("xmlContent : "+xmlContent);
			 * oSession.setAttribute("ProductXmlContent", xmlContent);
			 */

			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sE_CatalogueFileName = System.getProperty(
					"e-CatalogueFileName").trim();
			String sServerPath = getServlet().getServletContext().getRealPath(
					"");

			oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
					.getUserType(), sOwnerId, oLoginVO.getUserId(),
					sServerPath, oProductDetailsVO.getProdId(),
					oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
					oProductDetailsVO.getSeqId());
			// String sProdPreviewXmlPath =
			// System.getProperty("ProdPreviewXMLPath").trim();
			String sProdPreviewSwfPath = System.getProperty(
					"ProdPreviewSWFPath").trim();
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
					.getLastUpdatedCatalogueId();

			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("eCatalogueProdFileName",
					sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
							+ ".xml");

			request.setAttribute("mode", "addVendorProduct");
			logger.info("CatalogueAction::addUploadVendorProductImage:EXIT");
		} catch (Exception e) {
			/*
			 * //errors.add(Globals.ERROR_KEY,new
			 * ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			 */
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::addUploadVendorProductImage:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward skipUploadVendorProductImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::skipUploadVendorProductImage:ENTER");
		String sFrdKey = "skipProdImgSuccess";
		String sProductStatus = "";
		ProductDetailsVO oProductDetailsVO = null;
		HttpSession oSession = request.getSession();
		ArrayList<CategoryVO> alCategory = null;
		UploadImageForm oUploadImageForm = (UploadImageForm) form;
		String sCateFolderName = null, sImageName = null;
		// GenerateProdCatalogue oGenerateProdCatalogue = new
		// GenerateProdCatalogue();
		// GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;
		ProductDetailsForm oProductDetailsForm = null;

		// String sMainFileName= null,sMainImageType = null,sView1FileName =
		// null,sView1ImageType = null ,sImgType = null;
		// String sView2FileName = null,sView2ImageType = null,sView3FileName =
		// null,sView3ImageType = null;
		try {
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");

			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("vendorProductDetails");
			oProductDetailsForm = (ProductDetailsForm) oSession
					.getAttribute("vendorProductFormDetails");
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");

			String sImageUploadPath = System.getProperty("ImageUploadPath")
					.trim();
			String sImageViewPath = System.getProperty("ImageViewPath").trim();
			String sNoimagePath = System.getProperty("noImagePath").trim();
			sNoimagePath = sNoimagePath == null ? "" : sNoimagePath.trim();
			oProductDetailsVO.setNoImgPath(sNoimagePath);

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCatalogueDate = dateFormat.format(date);
			oProductDetailsVO.setCategoryDate(sCatalogueDate);

			String sCateId = oProductDetailsVO.getCateId();
			String sOwnerId = oProductDetailsVO.getOwnerId();
			String sProdId = oProductDetailsVO.getProdId();
			String sOwnerType = oProductDetailsVO.getOwnerType();

			String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
					+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
			sImageName = sOwnerId + sProdId;

			if (alCategory != null && alCategory.size() > 0) {
				for (CategoryVO oCategoryVO : alCategory) {
					if (oCategoryVO.getCateId() != null) {
						if (oCategoryVO.getCateId().equals(sCateId))
							sCateFolderName = oCategoryVO.getCateName();
					}
				}
			}

			FormFile oMainFormFile = oUploadImageForm.getMainImgPath();
			FormFile oView1FormFile = oUploadImageForm.getView1ImgPath();
			FormFile oView2FormFile = oUploadImageForm.getView2ImgPath();
			FormFile oView3FormFile = oUploadImageForm.getView3ImgPath();

			// Create Large,thumb,medium images
			/*
			 * if(oMainFormFile!= null &&
			 * !oMainFormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oMainFormFile,sImageViewPath,sImageUploadPath,sImageName,"main","Main",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setMainImgCap(oUploadImageForm.getMainImgCap()); }
			 * if(oView1FormFile!= null &&
			 * !oView1FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView1FormFile,sImageViewPath,sImageUploadPath,sImageName,"view1","View1",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView1ImgCap(oUploadImageForm.getView1ImgCap()); }
			 * if(oView2FormFile!= null &&
			 * !oView2FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView2FormFile,sImageViewPath,sImageUploadPath,sImageName,"view2","View2",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView2ImgCap(oUploadImageForm.getView2ImgCap()); }
			 * if(oView3FormFile!= null &&
			 * !oView3FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView3FormFile,sImageViewPath,sImageUploadPath,sImageName,"view3","View3",sCateFolderName,sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView3ImgCap(oUploadImageForm.getView3ImgCap()); }
			 */

			oProductDetailsForm.setInShopStatus("I");
			sProductStatus = CatalogueDAO.getProductStatus(sProdId);
			oProductDetailsVO.setSbhProdStatus(sProductStatus);
			oSession.setAttribute("vendorProductDetails", oProductDetailsVO);

			CatalogueDAO.updateProductStatus(oProductDetailsVO.getProdId(),
					oLoginVO.getUserId());

			// send mail to admin when a new product is added
			CatalogueDAO.sendIncompleteProdInfoByEmail(oProductDetailsForm,
					oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
					sCateFolderName);

			// send a mail to vendor if the product status inactive
			/*
			 * if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I")){
			 * CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,oProductDetailsVO,oLoginVO); }
			 */

			request.setAttribute("mode", "addVendorProduct");
			logger.info("CatalogueAction::skipUploadVendorProductImage:EXIT");
		} catch (Exception e) {
			/*
			 * //errors.add(Globals.ERROR_KEY,new
			 * ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			 */
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::skipUploadVendorProductImage:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward initSearchVendorCatalogue(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initSearchVendorCatalogue:ENTER");
		String sFwrdKey = "initSearchCatalogueSuccess";
		SearchCatalogueForm oSearchCatalogueForm = (SearchCatalogueForm) form;
		HttpSession oSession = request.getSession();

		try {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);
			oSearchCatalogueForm.setUploadToDate(sCurrentDate);

			oSession.removeAttribute("searchCatalogueForm");
		} catch (Exception e) {
			/*
			 * //
			 */
			sFwrdKey = "failure";
			e.printStackTrace();
			logger
					.error("CatalogueAction::initSearchVendorCatalogue:Exception "
							+ e.getMessage());
		}
		logger.info("CatalogueAction::initSearchVendorCatalogue:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward searchVendorCatalogue(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::searchVendorCatalogue:ENTER");
		String sFrdKey = "searchCatalogueSuccess";
		List<ProductDetailsVO> alCatelogueInfo = null;
		SearchCatalogueForm oSearchCatalogueForm = (SearchCatalogueForm) form;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sOwnerId = null, sOwnerType = null;
		String sCategory = null, sUploadFromDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus == null ? "" : sApprovalStatus
					.trim();

			if (oSearchCatalogueForm != null) {
				sCategory = oSearchCatalogueForm.getCategory();
				sCategory = sCategory == null ? "" : sCategory.trim();
				oSearchCatalogueForm.setCategory(sCategory);

				sUploadFromDate = oSearchCatalogueForm.getUploadFromDate();
				sUploadFromDate = sUploadFromDate == null ? ""
						: sUploadFromDate.trim();
				oSearchCatalogueForm.setUploadFromDate(sUploadFromDate);

				if (sApprovalStatus.trim().length() > 0) {
					oSearchCatalogueForm.setSbhProdStatus(sApprovalStatus);

					Date dFromDate = new java.util.Date();
					dFromDate.setDate(dFromDate.getDate() - 7);
					oSearchCatalogueForm.setUploadFromDate(dateFormat
							.format(dFromDate));

					if (sApprovalStatus.equalsIgnoreCase("N"))
						oSearchCatalogueForm.setProdStatus("A");
				}
			}
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerType = oLoginVO.getUserType();
			}
			alCatelogueInfo = CatalogueDAO.getCatalogueDetails(
					oSearchCatalogueForm, sOwnerId, sOwnerType);
			if (alCatelogueInfo != null && alCatelogueInfo.size() > 0) {
				request.setAttribute("catelogueInfo", alCatelogueInfo);
			} else {
				request.setAttribute("NoRecords", "noRecords");
			}
			logger.info("CatalogueAction::searchVendorCatalogue:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::searchVendorCatalogue:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward editVendorProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::editVendorProductDetails:ENTER");
		String sFrdKey = "editProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null, sCateFolderName = null;
		String sSourceOfEdit = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		try {
			String sProdId = request.getParameter("ProdId");
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			// sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());
			sSourceOfEdit = request.getParameter("SourceOfEdit");

			if (alCategory != null && alCategory.size() > 0) {
				for (CategoryVO oCategoryVO : alCategory) {
					if (oCategoryVO.getCateId() != null) {
						if (oCategoryVO.getCateId().equals(
								oProductDetailsVO.getCateId()))
							sCateFolderName = oCategoryVO.getCateName();
					}
				}
			}

			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");
				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");

				oProductDetailsVO.setSbhComment("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);
				oSession
						.setAttribute("vendorProductDetails", oProductDetailsVO);
				request.setAttribute("mode", "ProductEdit");
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
			}
			logger.info("CatalogueAction::editVendorProductDetails:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::editVendorProductDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward updateVendorProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::updateVendorProductDetails:ENTER");

		String sFrdKey = "updateProductSuccess";
		String sLongDescription = "";
		String sShortDescription = "";
		String sProductStatus = "";
		boolean isChanged = false;
		HttpSession oSession = request.getSession();
		ArrayList<CategoryVO> alCategory = null;
		List<CommentsVO> alProdComments = null;
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sImageName = null, sInShopStatus = null;
		LoginVO oLoginVO = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		ProductDetailsVO oOldProductDetailsVO = null;
		ArrayList alImageInfo = null;
		String sCateFolderName = null, sOldCateFolderName = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		boolean bUploadImg = false, bIsProdCodeExist = false;

		try {
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");

			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("vendorProductDetails");
			oOldProductDetailsVO = new ProductDetailsVO();
			// BeanUtils.copyProperties(oOldProductDetailsVO,
			// oProductDetailsVO);
			oOldProductDetailsVO = CatalogueDAO.getProductDetails(
					oProductDetailsForm.getProdId(), oLoginVO.getUserType());

			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oProductDetailsVO
					.getOwnerType(), oProductDetailsVO.getOwnerId(),
					oProductDetailsForm.getProdCode(), oProductDetailsForm
							.getProdId());

			if (!bIsProdCodeExist) {
				/*
				 * if(oProductDetailsForm.getValidFromDate()==null)
				 * oProductDetailsForm.setValidFromDate(""); else
				 * oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());
				 * 
				 * if(oProductDetailsForm.getValidToDate()==null)
				 * oProductDetailsForm.setValidToDate(""); else
				 * oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());
				 */

				if (oProductDetailsForm.getInstructions() == null)
					oProductDetailsForm.setInstructions("");
				else
					oProductDetailsForm.setInstructions(oProductDetailsForm
							.getInstructions().trim());

				if (oProductDetailsVO != null) {
					sInShopStatus = oProductDetailsVO.getInShopStatus();
					sInShopStatus = sInShopStatus == null ? "" : sInShopStatus
							.trim();

					if (oProductDetailsVO.getSbhProdStatus() != null) {
						oProductDetailsForm.setSbhProdStatus(oProductDetailsVO
								.getSbhProdStatus());
					}

					if (oProductDetailsVO.getCategoryDate() != null)
						oProductDetailsForm.setCategoryDate(oProductDetailsVO
								.getCategoryDate());
				}

				String sImageUploadPath = System.getProperty("ImageUploadPath")
						.trim();
				String sImageViewPath = System.getProperty("ImageViewPath")
						.trim();

				String sOldCateId = oProductDetailsVO.getCateId();
				String sNewCateId = oProductDetailsForm.getCateId();
				String sOwnerId = oProductDetailsVO.getOwnerId();
				String sProdId = oProductDetailsVO.getProdId();
				String sOwnerType = oProductDetailsVO.getOwnerType();

				String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
						+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
				sImageName = sOwnerId + sProdId;

				if (alCategory != null && alCategory.size() > 0) {
					for (CategoryVO oCategoryVO : alCategory) {
						if (oCategoryVO.getCateId() != null) {
							if (oCategoryVO.getCateId().equals(sOldCateId))
								sOldCateFolderName = oCategoryVO.getCateName();
							if (oCategoryVO.getCateId().equals(sNewCateId))
								sCateFolderName = oCategoryVO.getCateName();
						}
					}
				}
				FormFile oMainFormFile = oProductDetailsForm
						.getUploadMainImagePath();
				FormFile oView1FormFile = oProductDetailsForm
						.getUploadView1ImagePath();
				FormFile oView2FormFile = oProductDetailsForm
						.getUploadView2ImagePath();
				FormFile oView3FormFile = oProductDetailsForm
						.getUploadView3ImagePath();
				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")
						|| oView1FormFile != null
						&& !oView1FormFile.getFileName().equals("")
						|| oView2FormFile != null
						&& !oView2FormFile.getFileName().equals("")
						|| oView3FormFile != null
						&& !oView3FormFile.getFileName().equals("")) {
					bUploadImg = true;
				}

				// System.out.println("sOwnerId:"+sOwnerId);
				// System.out.println("sProdId:"+sProdId);
				// System.out.println("sOldCateFolderName:"+sOldCateFolderName);
				// System.out.println("sCateFolderName:"+sCateFolderName);

				// Category Type is changed
				if (!sOldCateId.equals(sNewCateId)) {
					// Main Image
					if (oMainFormFile != null
							&& !oMainFormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oMainFormFile, sImageViewPath,
								sImageUploadPath, sImageName, "main", "Main",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setMainImgType((String) alImageInfo.get(0));
							oProductDetailsVO
									.setMainImgPath((String) alImageInfo.get(1));
						}
						oProductDetailsVO.setMainImgCap(oProductDetailsForm
								.getUploadMainImgCap());
						ImageScale.deleteProductImage(sImageUploadPath, "Main",
								sOldCateFolderName, sImageName,
								oProductDetailsVO.getMainImgType(), sOwnerType,
								sOwnerId);
					} else {

						alImageInfo = ImageScale
								.moveProductImage(sImageUploadPath, "Main",
										sOldCateFolderName, sCateFolderName,
										sImageName, oProductDetailsVO
												.getMainImgType(), sOwnerType,
										sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setMainImgType((String) alImageInfo.get(0));
							oProductDetailsVO
									.setMainImgPath((String) alImageInfo.get(1));
						}
					}

					// View1 Image
					if (oView1FormFile != null
							&& !oView1FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView1FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view1", "View1",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView1ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView1ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView1ImgCap(oProductDetailsForm
								.getUploadView1ImgCap());
						ImageScale.deleteProductImage(sImageUploadPath,
								"View1", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView1ImgType(),
								sOwnerType, sOwnerId);
					} else {
						if (oProductDetailsForm.getDeleteView1Img() == null) {
							if (oProductDetailsVO.getView1ImgType() != null
									&& oProductDetailsVO.getView1ImgType()
											.trim().length() > 0) {
								alImageInfo = ImageScale.moveProductImage(
										sImageUploadPath, "View1",
										sOldCateFolderName, sCateFolderName,
										sImageName, oProductDetailsVO
												.getView1ImgType(), sOwnerType,
										sOwnerId);
								if (alImageInfo != null) {
									oProductDetailsVO
											.setView1ImgType((String) alImageInfo
													.get(0));
									oProductDetailsVO
											.setView1ImgPath((String) alImageInfo
													.get(1));
								}
							}
						} else if (oProductDetailsForm.getDeleteView1Img()
								.equals("yes")) {
							ImageScale.deleteProductImage(sImageUploadPath,
									"View1", sOldCateFolderName, sImageName,
									oProductDetailsVO.getView1ImgType(),
									sOwnerType, sOwnerId);
							oProductDetailsVO.setView1ImgType("");
							oProductDetailsVO.setView1ImgPath("");
							bUploadImg = true;
						}
					}

					// View2 Image
					if (oView2FormFile != null
							&& !oView2FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView2FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view2", "View2",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView2ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView2ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView2ImgCap(oProductDetailsForm
								.getUploadView2ImgCap());
						ImageScale.deleteProductImage(sImageUploadPath,
								"View2", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView2ImgType(),
								sOwnerType, sOwnerId);
					} else {
						if (oProductDetailsForm.getDeleteView2Img() == null) {
							if (oProductDetailsVO.getView2ImgType() != null
									&& oProductDetailsVO.getView2ImgType()
											.trim().length() > 0) {
								alImageInfo = ImageScale.moveProductImage(
										sImageUploadPath, "View2",
										sOldCateFolderName, sCateFolderName,
										sImageName, oProductDetailsVO
												.getView2ImgType(), sOwnerType,
										sOwnerId);
								if (alImageInfo != null) {
									oProductDetailsVO
											.setView2ImgType((String) alImageInfo
													.get(0));
									oProductDetailsVO
											.setView2ImgPath((String) alImageInfo
													.get(1));
								}
							}
						} else if (oProductDetailsForm.getDeleteView2Img() != null
								&& oProductDetailsForm.getDeleteView2Img()
										.equals("yes")) {
							ImageScale.deleteProductImage(sImageUploadPath,
									"View2", sOldCateFolderName, sImageName,
									oProductDetailsVO.getView2ImgType(),
									sOwnerType, sOwnerId);
							oProductDetailsVO.setView2ImgType("");
							oProductDetailsVO.setView2ImgPath("");
							bUploadImg = true;
						}
					}

					// View3 Image
					if (oView3FormFile != null
							&& !oView3FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView3FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view3", "View3",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView3ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView3ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView3ImgCap(oProductDetailsForm
								.getUploadView3ImgCap());
						ImageScale.deleteProductImage(sImageUploadPath,
								"View3", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView3ImgType(),
								sOwnerType, sOwnerId);
					} else {
						if (oProductDetailsForm.getDeleteView3Img() == null) {
							if (oProductDetailsVO.getView3ImgType() != null
									&& oProductDetailsVO.getView3ImgType()
											.trim().length() > 0) {
								alImageInfo = ImageScale.moveProductImage(
										sImageUploadPath, "View3",
										sOldCateFolderName, sCateFolderName,
										sImageName, oProductDetailsVO
												.getView3ImgType(), sOwnerType,
										sOwnerId);
								if (alImageInfo != null) {
									oProductDetailsVO
											.setView3ImgType((String) alImageInfo
													.get(0));
									oProductDetailsVO
											.setView3ImgPath((String) alImageInfo
													.get(1));
								}
							}
						} else if (oProductDetailsForm.getDeleteView3Img() != null
								&& oProductDetailsForm.getDeleteView3Img()
										.equals("yes")) {
							ImageScale.deleteProductImage(sImageUploadPath,
									"View3", sOldCateFolderName, sImageName,
									oProductDetailsVO.getView3ImgType(),
									sOwnerType, sOwnerId);
							oProductDetailsVO.setView3ImgType("");
							oProductDetailsVO.setView3ImgPath("");
							bUploadImg = true;
						}
					}

				}// Catrgory is same
				else {

					// Create Large,thumb,medium images
					if (oMainFormFile != null
							&& !oMainFormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oMainFormFile, sImageViewPath,
								sImageUploadPath, sImageName, "main", "Main",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setMainImgType((String) alImageInfo.get(0));
							oProductDetailsVO
									.setMainImgPath((String) alImageInfo.get(1));
						}
						oProductDetailsVO.setMainImgCap(oProductDetailsForm
								.getUploadMainImgCap());
					}

					if (oView1FormFile != null
							&& !oView1FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView1FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view1", "View1",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView1ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView1ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView1ImgCap(oProductDetailsForm
								.getUploadView1ImgCap());
					} else if (oProductDetailsForm.getDeleteView1Img() != null
							&& oProductDetailsForm.getDeleteView1Img().equals(
									"yes")) {
						ImageScale.deleteProductImage(sImageUploadPath,
								"View1", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView1ImgType(),
								sOwnerType, sOwnerId);
						oProductDetailsVO.setView1ImgType("");
						oProductDetailsVO.setView1ImgPath("");
						bUploadImg = true;
					}

					if (oView2FormFile != null
							&& !oView2FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView2FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view2", "View2",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView2ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView2ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView2ImgCap(oProductDetailsForm
								.getUploadView2ImgCap());
					} else if (oProductDetailsForm.getDeleteView2Img() != null
							&& oProductDetailsForm.getDeleteView2Img().equals(
									"yes")) {
						ImageScale.deleteProductImage(sImageUploadPath,
								"View2", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView2ImgType(),
								sOwnerType, sOwnerId);
						oProductDetailsVO.setView2ImgType("");
						oProductDetailsVO.setView2ImgPath("");
						bUploadImg = true;
					}

					if (oView3FormFile != null
							&& !oView3FormFile.getFileName().equals("")) {
						alImageInfo = ImageScale.createProductImage(
								oView3FormFile, sImageViewPath,
								sImageUploadPath, sImageName, "view3", "View3",
								sCateFolderName, sSrcImgPath, sOwnerType,
								sOwnerId);
						if (alImageInfo != null) {
							oProductDetailsVO
									.setView3ImgType((String) alImageInfo
											.get(0));
							oProductDetailsVO
									.setView3ImgPath((String) alImageInfo
											.get(1));
						}
						oProductDetailsVO.setView3ImgCap(oProductDetailsForm
								.getUploadView3ImgCap());
					} else if (oProductDetailsForm.getDeleteView3Img() != null
							&& oProductDetailsForm.getDeleteView3Img().equals(
									"yes")) {
						ImageScale.deleteProductImage(sImageUploadPath,
								"View3", sOldCateFolderName, sImageName,
								oProductDetailsVO.getView3ImgType(),
								sOwnerType, sOwnerId);
						oProductDetailsVO.setView3ImgType("");
						oProductDetailsVO.setView3ImgPath("");
						bUploadImg = true;
					}
				}
				if (oProductDetailsForm.getBrandName() == null)
					oProductDetailsForm.setBrandName("");
				else
					oProductDetailsForm.getBrandName().trim();

				if (oProductDetailsForm != null) {

					if (oProductDetailsVO.getMainImgType() == null
							|| !(oProductDetailsVO.getMainImgType().trim()
									.length() > 0))
						oProductDetailsForm.setInShopStatus("I");

					/** ** Update Product audit details *** */
					CatalogueDAO.insertProductAuditTrail(oProductDetailsForm,
							oOldProductDetailsVO, oLoginVO);

					String sModifiedFields = CatalogueDAO
							.getUpdatedItemDetails(oProductDetailsForm,
									oOldProductDetailsVO, oLoginVO);

					// If any fields changes update prod_status to pending
					if (bUploadImg || sModifiedFields.trim().length() > 0)
						CatalogueDAO.updateProductDetails(oProductDetailsForm,
								oProductDetailsVO, oLoginVO);
					if (oProductDetailsForm.getSbhComment() != null
							&& !oProductDetailsForm.getSbhComment()
									.equalsIgnoreCase(""))
						MerchandizeDAO.addProductComments(oProductDetailsForm
								.getOwnerId(),
								oProductDetailsVO.getOwnerType(),
								oProductDetailsForm.getProdId(),
								oProductDetailsForm.getSbhComment(), oLoginVO
										.getUserId());
					String sShortDesc = oProductDetailsForm.getShortDesc();
					sShortDesc = sShortDesc == null ? "" : sShortDesc.trim();
					sShortDescription = sShortDesc;
					sShortDesc = sShortDesc.replaceAll(" ", "&nbsp;");
					oProductDetailsForm.setShortDesc(sShortDesc);

					String sLongDesc = oProductDetailsForm.getLongDesc();
					sLongDesc = sLongDesc == null ? "" : sLongDesc.trim();
					sLongDescription = sLongDesc;
					sLongDesc = sLongDesc.replaceAll(" ", "&nbsp;");
					oProductDetailsForm.setLongDesc(sLongDesc);

					BeanUtils.copyProperties(oProductDetailsVO,
							oProductDetailsForm);
					sProductStatus = CatalogueDAO.getProductStatus(sProdId);
					oProductDetailsVO.setSbhProdStatus(sProductStatus);
					oSession.setAttribute("vendorProductDetails",
							oProductDetailsVO);
					alProdComments = MerchandizeDAO.getProductComments(
							oProductDetailsForm.getOwnerId(), oProductDetailsVO
									.getOwnerType(), oProductDetailsForm
									.getProdId(), oLoginVO.getUserId());

					if (alProdComments != null && alProdComments.size() > 0) {
						request.setAttribute("productComments", alProdComments);
					}
				}

				// if vednor change any product information send a mail To Admin
				// and CC to vendor
				CatalogueDAO.sendEditProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oOldProductDetailsVO, oLoginVO,
						oLoginVO.getUserId(), "", sCateFolderName);

				// send a mail to vendor if the product status is chaged from
				// active to inactive
				if (!sInShopStatus
						.equals(oProductDetailsForm.getInShopStatus())) {
					CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
							oProductDetailsVO, oLoginVO, oLoginVO.getUserId(),
							"", sCateFolderName);
				}

				// Generating Product Catalogue Xml
				/*
				 * String sDestPath =
				 * System.getProperty("GenerateXmlPath").trim(); String
				 * sProdXmlContent =
				 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
				 * String xmlContent = sProdXmlContent.replaceAll("\"",
				 * "&quot;"); // System.out.println("xmlContent : "+xmlContent);
				 * oSession.setAttribute("ProductXmlContent", xmlContent);
				 */

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

				request.setAttribute("mode", "editVendorProduct");

				if (sLongDescription != null
						&& sLongDescription.trim().length() > 0) {
					oProductDetailsVO.setLongDesc(sLongDescription);
					isChanged = true;
				}
				if (sShortDescription != null
						&& sShortDescription.trim().length() > 0) {
					oProductDetailsVO.setShortDesc(sShortDescription);
					isChanged = true;
				}
				if (isChanged) {
					oSession.setAttribute("merchandizeDetails",
							oProductDetailsVO);
				}
			} else {
				sFrdKey = "updateVendorProductFailure";
				request.setAttribute("ErrMsg", "Item Code already exist.");
			}

			logger.info("CatalogueAction::updateVendorProductDetails:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::updateVendorProductDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	/*
	 * public ActionForward editUploadVendorProductImage(ActionMapping mapping,
	 * ActionForm form, HttpServletRequest request, HttpServletResponse
	 * response) throws Exception {
	 * logger.info("CatalogueAction::editUploadVendorProductImage:ENTER");
	 * String sFrdKey="addProductSuccess"; ProductDetailsVO oProductDetailsVO =
	 * null; HttpSession oSession = request.getSession(); ArrayList<CategoryVO>
	 * alCategory = null; UploadImageForm
	 * oUploadImageForm=(UploadImageForm)form; String sCateFolderName =
	 * null,sImageName = null; GenerateProdCatalogue oGenerateProdCatalogue =
	 * new GenerateProdCatalogue(); LoginVO oLoginVO = null; ArrayList
	 * alImageInfo = null;
	 * 
	 * String sMainFileName= null,sMainImageType = null,sView1FileName =
	 * null,sView1ImageType = null ,sImgType = null; String sView2FileName =
	 * null,sView2ImageType = null,sView3FileName = null,sView3ImageType = null;
	 * try{
	 * alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
	 * oProductDetailsVO =(ProductDetailsVO)
	 * oSession.getAttribute("vendorProductDetails"); oLoginVO =(LoginVO)
	 * oSession.getAttribute("loginInfo");
	 * 
	 * 
	 * String sImageUploadPath = System.getProperty("ImageUploadPath").trim();
	 * String sImageViewPath = System.getProperty("ImageViewPath").trim();
	 * 
	 * String sCateId= oProductDetailsVO.getCateId(); String sOwnerId=
	 * oProductDetailsVO.getOwnerId(); String sProdId =
	 * oProductDetailsVO.getProdId();
	 * 
	 * String sSrcImgPath=sImageUploadPath+"\\SkyBuyPics\\OriginalImg\\";
	 * sImageName = sOwnerId+sProdId;
	 * 
	 * if(alCategory!=null && alCategory.size()>0){ for(CategoryVO oCategoryVO:
	 * alCategory){ if(oCategoryVO.getCateId()!=null){
	 * if(oCategoryVO.getCateId().equals(sCateId)) sCateFolderName =
	 * oCategoryVO.getCateName(); } } }
	 * 
	 * 
	 * FormFile oMainFormFile = oUploadImageForm.getMainImgPath(); FormFile
	 * oView1FormFile = oUploadImageForm.getView1ImgPath(); FormFile
	 * oView2FormFile = oUploadImageForm.getView2ImgPath(); FormFile
	 * oView3FormFile = oUploadImageForm.getView3ImgPath();
	 * 
	 * //Create Large,thumb,medium images if(oMainFormFile!= null &&
	 * !oMainFormFile.getFileName().equals("")){ alImageInfo =
	 * ImageScale.createProductImage(oMainFormFile,sImageViewPath,sImageUploadPath,sImageName,"main","Main",sCateFolderName,sSrcImgPath);
	 * if(alImageInfo !=null){
	 * oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
	 * oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1)); }
	 * oProductDetailsVO.setMainImgCap(oUploadImageForm.getMainImgCap()); }
	 * if(oView1FormFile!= null && !oView1FormFile.getFileName().equals("")){
	 * alImageInfo =
	 * ImageScale.createProductImage(oView1FormFile,sImageViewPath,sImageUploadPath,sImageName,"view1","View1",sCateFolderName,sSrcImgPath);
	 * if(alImageInfo !=null){
	 * oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
	 * oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1)); }
	 * oProductDetailsVO.setView1ImgCap(oUploadImageForm.getView1ImgCap()); }
	 * if(oView2FormFile!= null && !oView2FormFile.getFileName().equals("")){
	 * alImageInfo =
	 * ImageScale.createProductImage(oView2FormFile,sImageViewPath,sImageUploadPath,sImageName,"view2","View2",sCateFolderName,sSrcImgPath);
	 * if(alImageInfo !=null){
	 * oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
	 * oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1)); }
	 * oProductDetailsVO.setView2ImgCap(oUploadImageForm.getView2ImgCap()); }
	 * if(oView3FormFile!= null && !oView3FormFile.getFileName().equals("")){
	 * alImageInfo =
	 * ImageScale.createProductImage(oView3FormFile,sImageViewPath,sImageUploadPath,sImageName,"view3","View3",sCateFolderName,sSrcImgPath);
	 * if(alImageInfo !=null){
	 * oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
	 * oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1)); }
	 * 
	 * oProductDetailsVO.setView3ImgCap(oUploadImageForm.getView3ImgCap());
	 *  }
	 * 
	 * 
	 * 
	 * oSession.setAttribute("vendorProductDetails",oProductDetailsVO);
	 * 
	 * CatalogueDAO.updateProductImage(oProductDetailsVO,oLoginVO); String
	 * sDestPath = System.getProperty("GenerateXmlPath").trim();
	 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
	 * 
	 * logger.info("CatalogueAction::editUploadVendorProductImage:EXIT");
	 * }catch(Exception e){
	 * 
	 * //errors.add(Globals.ERROR_KEY,new
	 * ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
	 * 
	 * sFrdKey=("failure"); e.printStackTrace();
	 * logger.error("CatalogueAction::editUploadVendorProductImage:EXCEPTION
	 * "+e.getMessage()); } return mapping.findForward(sFrdKey); }
	 */

	/*
	 * public ActionForward getCatalogue(ActionMapping mapping, ActionForm form,
	 * HttpServletRequest request, HttpServletResponse response) throws
	 * Exception { logger.info("CatalogueAction::getCatalogue:ENTER"); String
	 * sFwrdKey = ""; boolean bDeviceStatus = false; FileInputStream fis = null;
	 * BufferedInputStream bis = null; GenerateCatalogueZip
	 * oGenerateCatalogueZip =null; GenerateCatalogue oGenerateCatalogue = new
	 * GenerateCatalogue(); try { //
	 * System.out.println("CatalogueAction::getCatalogue:ENTER"); String
	 * sCatalogueFolderPath = System.getProperty("CatalogueFolderPath").trim();
	 * String sCatalogueFileName =
	 * System.getProperty("CatalogueFileName").trim();
	 * 
	 * String sCataloguePath = sCatalogueFolderPath+"/"+sCatalogueFileName;
	 * 
	 * String sProductKey = request.getParameter("productKey"); sProductKey =
	 * sProductKey==null?"":sProductKey.trim();
	 * 
	 * 
	 * String sAirCode = request.getParameter("airCode"); sAirCode =
	 * sAirCode==null?"":sAirCode.trim();
	 * 
	 * String sAirId = DeviceRegDAO.getAirlineId(sProductKey); sAirId =
	 * sAirId==null?"":sAirId.trim();
	 * 
	 * String sDeviceId = request.getParameter("deviceId"); sDeviceId =
	 * sDeviceId== null ?" ":sDeviceId.trim();
	 * 
	 * 
	 * String sLastDownloadedCateDate =
	 * request.getParameter("LastDownloadedCateDate"); sLastDownloadedCateDate =
	 * sLastDownloadedCateDate == null ?" ":sLastDownloadedCateDate.trim();
	 * 
	 * 
	 * if(sLastDownloadedCateDate.trim().length()>0 &&
	 * !sLastDownloadedCateDate.equals("FullCatalogue")){
	 * if(sAirId.trim().length()>0 && sAirCode.trim().length()>0 &&
	 * !sAirId.equals(sAirCode)) sLastDownloadedCateDate = "FullCatalogue"; }
	 *  //
	 * System.out.println("sLastDownloadedCateDate"+sLastDownloadedCateDate);
	 * //DeviceRegDAO.validateDevice(sAirId, sDeviceId, sDevicePassword);
	 * 
	 * response.setContentType("application/xml"); PrintWriter pw =
	 * response.getWriter();
	 * 
	 * //Generate e-Catelogue String sDestPath =
	 * System.getProperty("GenerateXmlPath").trim();
	 * oGenerateCatalogue.generateCatalogueXML(sDestPath,"CLIENT",sAirId,"","","","","");
	 * 
	 * 
	 * String sDownloadDate =
	 * PlaceOrderDAO.getCatalogueDetailsByDate(sLastDownloadedCateDate,sCatalogueFolderPath,sCatalogueFileName,sAirId);
	 * sDownloadDate =sDownloadDate==null?"":sDownloadDate.trim();
	 * 
	 * DeviceRegDAO.insertDeviceLogDetails(sAirId,sDeviceId,"","Catalogue
	 * Download", 0);
	 * 
	 * File oFile = new File(sCataloguePath); fis = new FileInputStream(oFile);
	 *  // System.out.println("FileSize"+oFile.length());
	 * response.setHeader("FileSize",oFile.length()+"");
	 * response.setHeader("DownloadDate",sDownloadDate);
	 * response.setHeader("AirCode",sAirId); bis = new BufferedInputStream(fis);
	 * for(int readBytes = 0; (readBytes = bis.read()) != -1;)
	 * pw.write(readBytes);
	 * 
	 * pw.flush(); fis.close(); bis.close();
	 *  // System.out.println("CatalogueAction::getCatalogue:EXit"); }
	 * catch(Exception e) {
	 * 
	 * oErrors.add("org.apache.struts.action.ERROR", new ActionError("error.db",
	 * e.getMessage())); saveErrors(request, oErrors); sFwrdKey = "failure";
	 * e.printStackTrace(); logger.error((new
	 * StringBuilder("CatalogueAction::getCatalogue:Exception
	 * ")).append(e.getMessage()).toString()); }
	 * logger.info("CatalogueAction::getCatalogue:EXIT"); return null; }
	 */

	public ActionForward getCatalogue(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("CatalogueAction::getCatalogue:ENTER");

		String sAirId = null;
		String sAirCode = null;
		String sAirName = null;
		String sClientCertPass = null;
		String sDownloadDate = null;
		String sAccountInfo = null;
		String sCatalogueFolderPath = null;
		String sZipFolderPath = null;
		String sCatalogueFileName = null;
		String sIsAirlinePackageDownload = null;
		String sCommaSepCoverflowProdIds = null;
		String sProductKey = null;
		String sDeviceId = null;
		String sMacAddr = null;
		String sLastDownloadedCateDate = null;
		String sFullCatalogueDownload = "NO";
		BufferedInputStream bis = null;
		File fXML = null;
		File oCatalogueFile = null;
		FileInputStream fis = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		PrintWriter pw = null;
		ArrayList alCoverflowProdIds = null;
		ArrayList alAirlineDetails = null;
		String sIPhoneDesktopImage = null;
		Map<String, String> hmResult = new HashMap<String, String>();
		String iPhonePersonalShopper = null;
		String sIPhoneWelcomePage = null;
		try {
			// System.out.println("CatalogueAction::getCatalogue:ENTER");
			sCatalogueFolderPath = System.getProperty("CatalogueFolderPath")
					.trim();
			sZipFolderPath = System.getProperty("ZipFolderPath").trim();
			sCatalogueFileName = System.getProperty("CatalogueFileName").trim();

			InetAddress oInetAddress = InetAddress.getLocalHost();

			String sIsRegistered = request.getParameter("IsRegistered");
			logger.info("CatalogueAction::getCatalogue:IsRegistered Device:"
					+ sIsRegistered);
			if (sIsRegistered != null && sIsRegistered.equalsIgnoreCase("NO")) {
				sMacAddr = request.getParameter("macAddr");
				sMacAddr = sMacAddr == null ? " " : sMacAddr.trim();

				sDeviceId = sMacAddr;
				sAirId = "0";
				sAirName = "";
				sIsAirlinePackageDownload = "NO";
				sAirCode = "0";
			} else {
				sProductKey = request.getParameter("productKey");
				sProductKey = sProductKey == null ? "" : sProductKey.trim();

				sAirCode = request.getParameter("airCode");
				sAirCode = sAirCode == null ? "" : sAirCode.trim();

				alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);

				if (alAirlineDetails != null) {
					sAirId = (String) alAirlineDetails.get(0);
					sAirId = sAirId == null ? "" : sAirId.trim();

					sAirName = (String) alAirlineDetails.get(1);
					sAirName = sAirName == null ? "" : sAirName.trim();
				}
				sDeviceId = request.getParameter("deviceId");
				sDeviceId = sDeviceId == null ? " " : sDeviceId.trim();

				sMacAddr = request.getParameter("macAddr");
				sMacAddr = sMacAddr == null ? " " : sMacAddr.trim();

				sIsAirlinePackageDownload = request
						.getParameter("IsAirlinePackageDownload");
				sIsAirlinePackageDownload = sIsAirlinePackageDownload == null ? " "
						: sIsAirlinePackageDownload.trim();

			}
			sLastDownloadedCateDate = request
					.getParameter("LastDownloadedCateDate");
			sLastDownloadedCateDate = sLastDownloadedCateDate == null ? " "
					: sLastDownloadedCateDate.trim();

			// System.out.println("sLastDownloadedCateDate"+sLastDownloadedCateDate);
			// DeviceRegDAO.validateDevice(sAirId, sDeviceId, sDevicePassword);

			response.setContentType("application/xml");
			pw = response.getWriter();

			// Generate e-Catelogue
			String sCatelogueXmlFilePath = System
					.getProperty("GenerateXmlPath").trim();
			sCatelogueXmlFilePath = sCatelogueXmlFilePath == null ? ""
					: sCatelogueXmlFilePath.trim();
			sCatelogueXmlFilePath = sCatelogueXmlFilePath + "/XML/" + sDeviceId;
			fXML = new File(sCatelogueXmlFilePath);
			if (!fXML.exists()) {
				fXML.mkdir();
			}
			/*
			 * if("FullCatalogue".equalsIgnoreCase(sLastDownloadedCateDate)) {
			 * sCatelogueZipFilePath =
			 * System.getProperty("FullCatalogueZipFolderPath").trim();
			 * sCatelogueZipFilePath = sCatelogueZipFilePath ==
			 * null?"":sCatelogueZipFilePath.trim(); sCatalogueFileName =
			 * System.getProperty("CatalogueFileName").trim();
			 * sCatalogueFileName = sCatalogueFileName ==
			 * null?"":sCatalogueFileName.trim();
			 * 
			 * alCertAndNWPass =
			 * UploadSkyBuyCatalogueDAO.getClientCertAndNWUserName(sLastDownloadedCateDate);
			 * for(UpdatedCatalogueDetailsVO
			 * oUpdatedCatalogueDetailsVO:alCertAndNWPass){
			 * if(oUpdatedCatalogueDetailsVO.getUploadType().equalsIgnoreCase("USERNAME")){
			 * sNetworkUserName =
			 * OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkUsername(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
			 * sNetworkPassword =
			 * OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkPassword(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
			 * 
			 * sAccountInfo = sNetworkUserName+"|"+sNetworkPassword; } }
			 * sAccountInfo = sAccountInfo == null?"":sAccountInfo.trim();
			 * sClientCertPass = hmResult.get("ClientCertPass"); sClientCertPass =
			 * sClientCertPass == null?"":sClientCertPass.trim();
			 * 
			 * oCatalogueFile = new
			 * File(sCatelogueZipFilePath+"/"+sCatalogueFileName+"_"+sAirId+".zip");
			 * if(oCatalogueFile != null && oCatalogueFile.exists()){ fis = new
			 * FileInputStream(oCatalogueFile); sFullCatalogueDownload = "YES"; } }
			 */
			if (sLastDownloadedCateDate.trim().length() > 0
					&& "NO".equalsIgnoreCase(sFullCatalogueDownload)) {
				if (sAirId.trim().length() > 0 && sAirCode.trim().length() > 0
						&& !sAirId.equals(sAirCode)) {
					// sLastDownloadedCateDate = "FullCatalogue";
					response.setHeader("AirCode", sAirId);
					response.setHeader("AirName", sAirName);
					sIsAirlinePackageDownload = "YES";

				}

				alCoverflowProdIds = oGenerateCatalogue.generateCatalogueXML(
						sCatelogueXmlFilePath, "CLIENT", sAirId, "", "", "",
						"", "", sDeviceId, "");
				sCommaSepCoverflowProdIds = Utils
						.listToCommaSepertedString(alCoverflowProdIds);
				sCatalogueFileName = sCatalogueFileName + "_" + sDeviceId
						+ ".zip";

				hmResult = PlaceOrderDAO.getCatalogueDetailsByDate(
						sLastDownloadedCateDate, sCatalogueFolderPath,
						sCatalogueFileName, sAirId, sDeviceId,
						sCommaSepCoverflowProdIds, sIsAirlinePackageDownload,
						sCatelogueXmlFilePath, sZipFolderPath, sIsRegistered);
				sDownloadDate = hmResult.get("DownloadDate");
				sDownloadDate = sDownloadDate == null ? "" : sDownloadDate
						.trim();
				sAccountInfo = hmResult.get("AccountInfo");
				sAccountInfo = sAccountInfo == null ? "" : sAccountInfo.trim();
				sClientCertPass = hmResult.get("ClientCertPass");
				sClientCertPass = sClientCertPass == null ? ""
						: sClientCertPass.trim();
				sIPhoneDesktopImage = hmResult.get("iPhoneDesktop");
				sIPhoneDesktopImage = sIPhoneDesktopImage == null ? ""
						: sIPhoneDesktopImage.trim();

				iPhonePersonalShopper = hmResult.get("iPhonePersonalShopper");
				iPhonePersonalShopper = iPhonePersonalShopper == null ? ""
						: iPhonePersonalShopper.trim();

				sIPhoneWelcomePage = hmResult.get("iPhoneWelcomePage");
				sIPhoneWelcomePage = sIPhoneWelcomePage == null ? ""
						: sIPhoneWelcomePage.trim();

				if (sIsRegistered != null
						&& sIsRegistered.equalsIgnoreCase("NO")) {
					sDeviceId = "0";
				}
				if (sAccountInfo != null && !"".equalsIgnoreCase(sAccountInfo)) {
					DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
							"Account User Name and Passcode", 0, sMacAddr, "",
							oInetAddress.getHostName(), "CLIENT");
				}
				if (sClientCertPass != null
						&& !"".equalsIgnoreCase(sClientCertPass)) {
					DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
							"Client Cert and Passcode", 0, sMacAddr, "",
							oInetAddress.getHostName(), "CLIENT");
				}
				if (sIPhoneDesktopImage != null
						&& !"".equalsIgnoreCase(sIPhoneDesktopImage)) {
					DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
							"iPhone Desktop Image", 0, sMacAddr, "",
							oInetAddress.getHostName(), "CLIENT");
				}
				if (iPhonePersonalShopper != null
						&& !"".equalsIgnoreCase(iPhonePersonalShopper)) {
					DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
							"iPhone Personal Shopper", 0, sMacAddr, "",
							oInetAddress.getHostName(), "CLIENT");
				}
				if (sIPhoneWelcomePage != null
						&& !"".equalsIgnoreCase(sIPhoneWelcomePage)) {
					DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
							"iPhone Welcome Page", 0, sMacAddr, "",
							oInetAddress.getHostName(), "CLIENT");
				}

				DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
						"Catalogue Download", 0, sMacAddr, "", oInetAddress
								.getHostName(), "CLIENT");
				String sCataloguePath = sCatalogueFolderPath + "/"
						+ sCatalogueFileName;
				oCatalogueFile = new File(sCataloguePath);
				fis = new FileInputStream(oCatalogueFile);

			}
			// System.out.println("FileSize : "+oCatalogueFile.length() +"
			// DeviceId:"+sDeviceId);
			// response.setHeader("PassCert", sClientCertPass );
			response.setHeader("AccountInfo", sAccountInfo);
			response.setHeader("FileSize", oCatalogueFile.length() + "");
			response.setHeader("DownloadDate", sDownloadDate);
			/*
			 * response.setHeader("AirCode",sAirId);
			 * response.setHeader("AirName",sAirName);
			 */
			bis = new BufferedInputStream(fis);
			// System.out.println("Read Bytes:Enter::DeviceId:"+sDeviceId);
			for (int readBytes = 0; (readBytes = bis.read()) != -1;)
				pw.write(readBytes);

			logger.info("CatalogueAction::getCatalogue:Account Info:"
					+ sAccountInfo);
			logger.info("CatalogueAction::getCatalogue:File Size:"
					+ oCatalogueFile.length() + "");
			logger.info("CatalogueAction::getCatalogue:Download Date:"
					+ sDownloadDate);
			logger.info("CatalogueAction::getCatalogue:Air Id:" + sAirId);
			logger.info("CatalogueAction::getCatalogue:Air Name:" + sAirName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error((new StringBuilder(
					"CatalogueAction::getCatalogue:Exception ")).append(
					e.getMessage()).toString());
		} finally {
			if (pw != null) {
				pw.flush();
				pw.close();
			}
			if (fis != null) {
				fis.close();
			}
			if (bis != null) {
				bis.close();
			}
			if (fXML != null && fXML.exists()) {
				deleteFiles(fXML);
			}

			if ("NO".equalsIgnoreCase(sFullCatalogueDownload)) {
				// delete Skybuy zip folder
				if (oCatalogueFile.exists())
					oCatalogueFile.delete();
			}

		}
		logger.info("CatalogueAction::getCatalogue:EXIT");
		return null;
	}

	/*
	 * public ActionForward isAutoUpdatedVersionAvailable(ActionMapping mapping,
	 * ActionForm form, HttpServletRequest request, HttpServletResponse
	 * response) throws Exception {
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::ENTER");
	 * 
	 * String sFwrdKey = null; String sIsUpdated = null; String
	 * sUpdatedAdminVersion = null; String sUpdatedCatalogueVersion = null;
	 * String sAutoUdpateDate = null; String sAutoUpdateInstallationPath = null;
	 * String sAutoUpdateVersion= null; String sSchemaVersion = null;
	 * FileInputStream fis = null; BufferedInputStream bis = null; PrintWriter
	 * pw = null; File oAutoUpdateFile = null; try { String sCatalogueFolderPath =
	 * System.getProperty("CatalogueFolderPath").trim();
	 * 
	 * String sProductKey = request.getParameter("productKey"); sProductKey =
	 * sProductKey==null?"":sProductKey.trim();
	 * 
	 * String sDeviceId = request.getParameter("deviceId"); sDeviceId =
	 * sDeviceId== null ?"":sDeviceId.trim();
	 * 
	 * String sMacAddr = request.getParameter("macAddr"); sMacAddr = sMacAddr ==
	 * null?"":sMacAddr.trim();
	 * 
	 * String sLastAutoUpdateVersion =
	 * request.getParameter("AutoUpdateVersion"); sLastAutoUpdateVersion =
	 * sLastAutoUpdateVersion == null ?"":sLastAutoUpdateVersion.trim();
	 * 
	 * String sAirCode = request.getParameter("airCode"); sAirCode =
	 * sAirCode==null?"":sAirCode.trim();
	 * 
	 * String sLastDownloadedCateDate =
	 * request.getParameter("LastDownloadedCateDate"); sLastDownloadedCateDate =
	 * sLastDownloadedCateDate == null ?"":sLastDownloadedCateDate.trim();
	 * 
	 * response.setContentType("application/xml");
	 * 
	 * pw = response.getWriter(); String sInetAddress =
	 * InetAddress.getLocalHost().getHostName();
	 * 
	 * String sAirId = "1005"; String sLastDownloadedCateDate = "JUN 03 2009
	 * 02:45:25 AM";
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::Device
	 * Id......."+sDeviceId);
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::Mac
	 * Address......."+sMacAddr);
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::Requested
	 * Auto Update Version......."+sLastAutoUpdateVersion);
	 * logger.info("sAirCode......."+sAirCode);
	 * 
	 * UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO =
	 * isAutoUpdateVersionAvailable(sLastAutoUpdateVersion);
	 * 
	 * sAutoUdpateDate = oUpdatedCatalogueDetailsVO.getAutoUpdateDate();
	 * sAutoUpdateInstallationPath =
	 * oUpdatedCatalogueDetailsVO.getAutoUpdateInstallationPath();
	 * sAutoUpdateVersion= oUpdatedCatalogueDetailsVO.getAutoUpdateVersion();
	 * 
	 * if(sAutoUpdateVersion!=null && sAutoUpdateVersion.trim().length() > 0) {
	 * sIsUpdated = "TRUE"; }else { sIsUpdated = "FALSE"; }
	 * response.setHeader("IsUpdatedVersionAvailable", sIsUpdated);
	 * 
	 * if(sAutoUpdateVersion != null && sAutoUpdateVersion.trim().length() > 0) {
	 * String sAutoUpdatePath =
	 * sCatalogueFolderPath+sAutoUpdateInstallationPath; oAutoUpdateFile = new
	 * File(sAutoUpdatePath); fis = new FileInputStream(oAutoUpdateFile);
	 * response.setHeader("AutoUpdateVersion",sAutoUpdateVersion+"@"+sAutoUdpateDate);
	 * response.setHeader("FileSize",oAutoUpdateFile.length()+"");
	 * 
	 * if(oAutoUpdateFile != null) { // System.out.println("FileSize :
	 * "+oAutoUpdateFile.length() +" DeviceId:"+sDeviceId); bis = new
	 * BufferedInputStream(fis); // System.out.println("Read
	 * Bytes:Enter::DeviceId:"+sDeviceId); for(int readBytes = 0; (readBytes =
	 * bis.read()) != -1;) pw.write(readBytes); // System.out.println("Read
	 * Bytes:Exit::DeviceId:"+sDeviceId);
	 * 
	 * insertPackageDownloadLogDetails(sDeviceId,"Core Auto Update
	 * Download","AUTOUPDATE",sUpdatedAdminVersion,sUpdatedCatalogueVersion,sAutoUpdateVersion,sMacAddr,sInetAddress);
	 * insertAutoUpdateDeviceLogDetails(sAirCode,sDeviceId,"Core Auto Update
	 * Activity",sUpdatedAdminVersion,sUpdatedCatalogueVersion,sAutoUpdateVersion,sSchemaVersion,"Core
	 * Auto Update",sMacAddr,sInetAddress,"CLIENT"); } }else {
	 * response.setHeader("AutoUpdateVersion","");
	 * response.setHeader("FileSize","0"); }
	 * 
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::Reponse Auto
	 * Update Version "+sAutoUpdateVersion);
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable::Response
	 * Auto Update Version Updated "+sAutoUdpateDate); }catch(Exception e){
	 * sFwrdKey = "failure"; e.printStackTrace();
	 * logger.error("CatalogueAction::isAutoUpdatedVersionAvailable:Exception
	 * "+e.getMessage()); }finally{ if(pw != null) { pw.flush(); } if(fis !=
	 * null) { fis.close(); } if(bis != null) { bis.close(); }
	 * if(oAutoUpdateFile != null && oAutoUpdateFile.exists()) {
	 * oAutoUpdateFile.delete(); } }
	 * logger.info("CatalogueAction::isAutoUpdatedVersionAvailable:EXIT");
	 * return mapping.findForward(sFwrdKey); }
	 */

	public ActionForward isUpdatedVersionAvailable(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::isUpdatedVersionAvailable:ENTER");

		boolean bIsExist = false;
		int len;
		String sFwrdKey = null;
		String sIsUpdated = "";
		/*
		 * String sUpdatedAdminVersion = null; String sUpdatedCatalogueVersion =
		 * null; String sAdminUpdateDate = null; String sCatalogueUpdateDate =
		 * null;
		 */
		String sUpdatedServiceVersion = null;
		String sUpdatedVirtualDesktopVersion = null;
		String sServiceUpdateDate = null;
		String sVirtualDesktopUpdateDate = null;
		String sDownloadSchemaVersion = null;
		String sDownloadSchemaDate = null;
		String sDownloadedSchemaPath = null;
		String sPackageDetails = "";
		String sNetworkUserName = null;
		String sNetworkPassword = null;
		String sClientCertPath = null;
		String sClientCertPass = "";
		String sAccountInfo = "";
		String sCatalogueFolderPath = null;
		String sDeviceId = null;
		String sUpdatesDownloaded = "";
		String sSchemaDestFilePath = null;
		String sAirId = "";
		String sAirName = "";
		String sIsAirlineChanged = "NO";
		byte[] buf = new byte[1024];
		Date dDate = new Date();
		FileInputStream fis = null;
		InputStream input = null;
		OutputStream output = null;
		BufferedInputStream bis = null;
		GenerateCatalogueZip oGenerateCatalogueZip = null;
		ArrayList alAirlineDetails = null;
		File oCatalogueFile = null;
		PrintWriter pw = null;
		try {
			InetAddress oInetAddress = InetAddress.getLocalHost();
			sCatalogueFolderPath = System.getProperty("CatalogueFolderPath")
					.trim();

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey == null ? "" : sProductKey.trim();

			sDeviceId = request.getParameter("deviceId");
			sDeviceId = sDeviceId == null ? "" : sDeviceId.trim();

			String sMacAddr = request.getParameter("macAddr");
			sMacAddr = sMacAddr == null ? "" : sMacAddr.trim();

			/*
			 * String sLastAdminVersion = request.getParameter("adminVersion");
			 * sLastAdminVersion = sLastAdminVersion == null
			 * ?"":sLastAdminVersion.trim();
			 * 
			 * String sLastCatalogueVersion =
			 * request.getParameter("CatalogueVersion"); sLastCatalogueVersion =
			 * sLastCatalogueVersion == null ?"":sLastCatalogueVersion.trim();
			 */

			String sLastServiceVersion = request.getParameter("serviceVersion");
			sLastServiceVersion = sLastServiceVersion == null ? ""
					: sLastServiceVersion.trim();

			String sLastVirtualVersion = request
					.getParameter("virtualDesktopVersion");
			sLastVirtualVersion = sLastVirtualVersion == null ? ""
					: sLastVirtualVersion.trim();

			String sSchemaVersion = request.getParameter("SchemaVersion");
			sSchemaVersion = sSchemaVersion == null ? "" : sSchemaVersion
					.trim();

			String sAirCode = request.getParameter("airCode");
			sAirCode = sAirCode == null ? "" : sAirCode.trim();

			alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);

			if (alAirlineDetails != null) {
				sAirId = (String) alAirlineDetails.get(0);
				sAirId = sAirId == null ? "" : sAirId.trim();

				sAirName = (String) alAirlineDetails.get(1);
				sAirName = sAirName == null ? "" : sAirName.trim();

			}
			String sLastDownloadedCateDate = request
					.getParameter("LastDownloadedCateDate");
			sLastDownloadedCateDate = sLastDownloadedCateDate == null ? ""
					: sLastDownloadedCateDate.trim();

			/*
			 * String sAirId = "1005"; String sLastDownloadedCateDate = "JUN 03
			 * 2009 02:45:25 AM";
			 */
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Device Id......."
							+ sDeviceId);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Mac Address......."
							+ sMacAddr);
			/*
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Requested
			 * Admin Version......."+sLastAdminVersion);
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Requested
			 * Shopping Cart Version......."+sLastCatalogueVersion);
			 */
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Requested Service Version......."
							+ sLastServiceVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Requested Virtual Desktop Version......."
							+ sLastVirtualVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Requested Schema Version......."
							+ sSchemaVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::sAirCode......."
							+ sAirCode);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::sLastDownloadDate......."
							+ sLastDownloadedCateDate);

			// UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO =
			// isUpdatedVersion(sLastAdminVersion, sLastCatalogueVersion,
			// sTypeOfRequest,sLastDownloadedCateDate,sAirCode,null);

			if (sLastDownloadedCateDate.trim().length() > 0
					&& !sLastDownloadedCateDate.equals("FullCatalogue")) {
				if (sAirId.trim().length() > 0 && sAirCode.trim().length() > 0
						&& !sAirId.equals(sAirCode))
					sIsAirlineChanged = "YES";
				// sLastDownloadedCateDate = "FullCatalogue";
			}

			ArrayList<UpdatedCatalogueDetailsVO> alCatgDetails = UploadSkyBuyCatalogueDAO
					.getDownloadByProductVersion(sLastServiceVersion,
							sLastVirtualVersion, sSchemaVersion);
			ArrayList<UpdatedCatalogueDetailsVO> alCertAndNWPass = UploadSkyBuyCatalogueDAO
					.getInstallationPackageByDate(sLastDownloadedCateDate);
			String sIsDownloadCatatalogueAvailable = isDownloadCatalogueAvailable(
					sLastDownloadedCateDate, sAirCode, sIsAirlineChanged);

			// To get Latest Update of Application
			for (UpdatedCatalogueDetailsVO  oUpdatedCatalogueDetailsVO : alCatgDetails) {
				if (UPLOADTYPE_SERVICE
						.equalsIgnoreCase(oUpdatedCatalogueDetailsVO
								.getUploadType())) {
					sUpdatedServiceVersion = oUpdatedCatalogueDetailsVO
							.getServiceVersion();
					sServiceUpdateDate = oUpdatedCatalogueDetailsVO
							.getUpdateDate();
					if ("".equalsIgnoreCase(sPackageDetails)) {
						sPackageDetails = "Service@" + sUpdatedServiceVersion
								+ "@" + sServiceUpdateDate;
					} else {
						sPackageDetails += "|" + "Service@"
								+ sUpdatedServiceVersion + "@"
								+ sServiceUpdateDate;
					}
				}
				if (UPLOADTYPE_VIRTUALDESKTOP
						.equalsIgnoreCase(oUpdatedCatalogueDetailsVO
								.getUploadType())) {
					sUpdatedVirtualDesktopVersion = oUpdatedCatalogueDetailsVO
							.getVirtualDesktopVersion();
					sVirtualDesktopUpdateDate = oUpdatedCatalogueDetailsVO
							.getUpdateDate();
					if ("".equalsIgnoreCase(sPackageDetails)) {
						sPackageDetails = "Virtual@"
								+ sUpdatedVirtualDesktopVersion + "@"
								+ sVirtualDesktopUpdateDate;
					} else {
						sPackageDetails += "|" + "Virtual@"
								+ sUpdatedVirtualDesktopVersion + "@"
								+ sVirtualDesktopUpdateDate;
					}
				}

				if (UPLOADTYPE_SCHEMA
						.equalsIgnoreCase(oUpdatedCatalogueDetailsVO
								.getUploadType())) {
					if (!bIsExist) {
						oGenerateCatalogueZip = new GenerateCatalogueZip(
								sCatalogueFolderPath, "ClientPass_" + sDeviceId
										+ ".zip");
						sSchemaDestFilePath = sCatalogueFolderPath + "/Schema_"
								+ sDeviceId + ".sql";
						output = new FileOutputStream(new File(
								sSchemaDestFilePath));
					}
					sDownloadedSchemaPath = oUpdatedCatalogueDetailsVO
							.getSchemaPath();
					sDownloadSchemaVersion = oUpdatedCatalogueDetailsVO
							.getSchemaVersion();
					sDownloadSchemaDate = oUpdatedCatalogueDetailsVO
							.getUpdateDate();
					sDownloadedSchemaPath = sCatalogueFolderPath + "/"
							+ sDownloadedSchemaPath;

					input = new FileInputStream(sDownloadedSchemaPath);
					while ((len = input.read(buf)) > 0) {
						output.write(buf, 0, len);
					}
					input.close();

					bIsExist = true;
					sUpdatesDownloaded = "Schema|";
				}
			}
			if (bIsExist) {
				if (output != null) {
					output.close();
				}
				File fSourceFile = new File(sSchemaDestFilePath);
				oGenerateCatalogueZip.addFile(sDownloadedSchemaPath, "", "ClientPass_"+ sDeviceId + "/" + fSourceFile.getName());
				// oGenerateCatalogueZip.addFile(sDownloadedSchemaPath, "",
				// "ClientPass/"+fSourceFile.getName());
			}

			// To get Account Information and Certificate Information
			for (UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO : alCertAndNWPass) {
				if (UPLOADTYPE_USERNAME.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())) {
					sNetworkUserName = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkUsername(),"SERVER", oUpdatedCatalogueDetailsVO.getKeyRefId());
					sNetworkPassword = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkPassword(),"SERVER", oUpdatedCatalogueDetailsVO.getKeyRefId());

					sAccountInfo = sNetworkUserName + "|" + sNetworkPassword;
					sUpdatesDownloaded = "AccountInfo|";
				}
				if (UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())) {
					sClientCertPath = oUpdatedCatalogueDetailsVO.getClientCertPath();
					sClientCertPass = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getClientCertPassword(),"SERVER", oUpdatedCatalogueDetailsVO.getKeyRefId());
					sClientCertPath = sCatalogueFolderPath + "/"+ sClientCertPath;
					File fSourceFile = new File(sClientCertPath);
					if (!bIsExist) {
						oGenerateCatalogueZip = new GenerateCatalogueZip(
								sCatalogueFolderPath, "ClientPass_" + sDeviceId+ ".zip");
					}
					oGenerateCatalogueZip.addFile(sClientCertPath, "","ClientPass_" + sDeviceId + "/"
									+ fSourceFile.getName());
					// oGenerateCatalogueZip.addFile(sClientCertPath, "",
					// "ClientPass/"+fSourceFile.getName());
					bIsExist = true;
					sUpdatesDownloaded = "ClientCert|";
				}
			}

			if (sUpdatesDownloaded != null
					&& sUpdatesDownloaded.trim().length() > 0) {
				sUpdatesDownloaded = sUpdatesDownloaded.substring(0,
						sUpdatesDownloaded.length() - 2);
			}
			if (sUpdatedVirtualDesktopVersion == null
					&& sUpdatedServiceVersion == null) {
				sPackageDetails = "Service@NA@NA|Virtual@NA@NA";
			} else if (sUpdatedVirtualDesktopVersion == null) {
				sPackageDetails += "|Virtual@NA@NA";
			} else if (sUpdatedServiceVersion == null) {
				sPackageDetails = "Service@NA@NA|" + sPackageDetails;
			}
			/*
			 * if(alCatgDetails==null || alCatgDetails.size()<1){
			 * sPackageDetails = "Service@NA@NA|Virtual@NA@NA"; }
			 */

			// sCatalogueUpdateDate =
			// oUpdatedCatalogueDetailsVO.getCatalogueUpdateDate();
			if (sUpdatedServiceVersion != null
					&& sUpdatedServiceVersion.trim().length() > 0) {
				sIsUpdated = "TRUE|Schema@NA@NA";
			} else if (sUpdatedVirtualDesktopVersion != null
					&& sUpdatedVirtualDesktopVersion.trim().length() > 0) {
				sIsUpdated = "TRUE|Schema@NA@NA";
			}
			if ("CATALOGUEAVAILABLE"
					.equalsIgnoreCase(sIsDownloadCatatalogueAvailable)) {
				if ("".equalsIgnoreCase(sPackageDetails)) {
					sPackageDetails = sIsDownloadCatatalogueAvailable + "@"
							+ Utils.dateFormat(dDate);
				} else {
					sPackageDetails += "|" + sIsDownloadCatatalogueAvailable
							+ "@" + Utils.dateFormat(dDate);
				}
				if ("".equalsIgnoreCase(sIsUpdated)) {
					sIsUpdated = "TRUE|Schema@NA@NA";
				}
			} else {
				if ("".equalsIgnoreCase(sPackageDetails)) {
					sPackageDetails = sIsDownloadCatatalogueAvailable + "@NA";
				} else {
					sPackageDetails += "|" + sIsDownloadCatatalogueAvailable
							+ "@NA";
				}
			}
			if (sDownloadSchemaVersion != null
					&& sDownloadSchemaVersion.trim().length() > 0) {
				if (sIsUpdated != null && sIsUpdated.trim().length() > 0)
					sIsUpdated = "TRUE|Schema@" + sDownloadSchemaVersion + "@"
							+ sDownloadSchemaDate;
				else
					sIsUpdated = "FALSE|Schema@" + sDownloadSchemaVersion + "@"
							+ sDownloadSchemaDate;
			}

			if ("".equalsIgnoreCase(sIsUpdated)) {
				sIsUpdated = "FALSE|Schema@NA@NA";
			}

			pw = response.getWriter();

			response.setHeader("IsUpdatedVersionAvailable", sIsUpdated);
			response.setHeader("PackageDetails", sPackageDetails);
			response.setHeader("AccountInfo ", sAccountInfo);
			response.setHeader("PassCert ", ""); // sClientCertPass
			if (bIsExist) {
				oGenerateCatalogueZip.finish();
				response.setHeader("FileSize", "0");
				oCatalogueFile = new File(sCatalogueFolderPath + "/ClientPass_"
						+ sDeviceId + ".zip");
				fis = new FileInputStream(oCatalogueFile);
				bis = new BufferedInputStream(fis);
				response.setHeader("FileSize ", oCatalogueFile.length() + "");
				// System.out.println("Read Bytes:Enter::DeviceId:"+sDeviceId);
				for (int readBytes = 0; (readBytes = bis.read()) != -1;)
					pw.write(readBytes);
				// System.out.println("Read Bytes:Exit::DeviceId:"+sDeviceId);

				insertAutoUpdateDeviceLogDetails(sAirCode, sDeviceId,
						"Auto Update Activity", null, null,
						sDownloadSchemaVersion, "", sUpdatesDownloaded,
						sMacAddr, oInetAddress.getHostName(), "CLIENT");
			} else {
				response.setHeader("FileSize ", "0");
			}
			response.setHeader("AirCode", sAirId);
			response.setHeader("AirName", sAirName);
			/*
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Response
			 * Admin Version "+sUpdatedAdminVersion);
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Response
			 * Admin Version Updated "+sAdminUpdateDate);
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Response
			 * Catalogue Version "+sUpdatedCatalogueVersion);
			 * logger.info("CatalogueAction::isUpdatedVersionAvailable::Response
			 * Catalogue Version Updated "+sCatalogueUpdateDate);
			 */
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Service Version  "
							+ sUpdatedServiceVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Service Version Updated  "
							+ sServiceUpdateDate);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Virtual Version  "
							+ sUpdatedVirtualDesktopVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Virtual Version Updated  "
							+ sVirtualDesktopUpdateDate);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Schema Version  "
							+ sDownloadSchemaVersion);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Schema Version Updated  "
							+ sDownloadSchemaDate);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Download Catalogue  "
							+ sIsDownloadCatatalogueAvailable);
			logger
					.info("CatalogueAction::isUpdatedVersionAvailable::Response Download Catalogue Updated  "
							+ Utils.dateFormat(dDate));
		} catch (Exception e) {
			sFwrdKey = "failure";
			response.setHeader("ErrorMsg", e.getMessage());
			logger
					.error("CatalogueAction::isUpdatedVersionAvailable:Exception "
							+ e.getMessage());
		} finally {

			if (pw != null) {
				pw.flush();
			}
			if (fis != null) {
				fis.close();
			}
			if (bis != null) {
				bis.close();
			}
			File fZip = new File(sCatalogueFolderPath + "/ClientPass_"
					+ sDeviceId + ".zip");
			if (fZip != null) {
				if (fZip.exists()) {
					fZip.delete();
				}
			}
			if (sSchemaDestFilePath != null
					&& sSchemaDestFilePath.trim().length() > 0) {
				File fSchema = new File(sSchemaDestFilePath);
				if (fSchema != null) {
					if (fSchema.exists())
						fSchema.delete();
				}
			}
		}

		logger.info("CatalogueAction::isUpdatedVersionAvailable:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	/* To validate device password */

	public ActionForward validateDevice(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.info("CatalogueAction::validateDevice::ENTER");

		String sFwrdKey = null;
		String sStatus = null;

		try {

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey == null ? "" : sProductKey.trim();

			String sDeviceId = request.getParameter("deviceId");
			sDeviceId = sDeviceId == null ? "" : sDeviceId.trim();

			String sMacAddr = request.getParameter("macAddr");
			sMacAddr = sMacAddr == null ? "" : sMacAddr.trim();

			String sAirCode = request.getParameter("airCode");
			sAirCode = sAirCode == null ? "" : sAirCode.trim();

			String sDeviceCode = request.getParameter("deviceCode");
			sDeviceCode = sDeviceCode == null ? "" : sDeviceCode.trim();

			String sDevicePassword = request.getParameter("devicePassword");
			sDevicePassword = sDevicePassword == null ? "" : sDevicePassword
					.trim();

			String sKeyRefId = request.getParameter("checkSumRefID");
			sKeyRefId = sKeyRefId == null ? "" : sKeyRefId.trim();

			/*
			 * if(!"".equalsIgnoreCase(sKeyRefId)){ sDevicePassword =
			 * OrderDAO.decryptInputData(sDevicePassword, "CLIENT", sKeyRefId); }
			 */

			response.setContentType("application/xml");
			PrintWriter pw = response.getWriter();
			sStatus = CatalogueDAO.validateDevicePassword(sDeviceId,
					sDeviceCode, sDevicePassword);

			response.setHeader("STATUS", sStatus);

			logger.error("CatalogueAction::validateDevice:Status " + sStatus);
		} catch (Exception e) {
			sFwrdKey = "failure";
			e.printStackTrace();
			logger.error("CatalogueAction::validateDevice:Exception "
					+ e.getMessage());
		}
		logger.info("CatalogueAction::validateDevice:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward getUpdatedPackage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::getUpdatedPackage::ENTER");

		Boolean isECatalogueAvailable = false;
		String sAirCode = null;
		String sAirId = null;
		String sAirName = null;
		String sFwrdKey = null;
		String sZipFileName = "";
		String sDownloadDate = "";
		String sUpdatesDownloaded = "";
		String sUpdatedServiceVersion = null;
		String sUpdatedVirtualDesktopVersion = null;
		String sIsDownloadCatatalogueAvailable = null;
		String sCommaSepCoverflowProdIds = null;
		String sCatalogueFolderPath = null;
		String sDownloadCatalogueFolderPath = null;
		String sInstallationPackagePath = null;
		String sIsAirlineChanged = "NO";
		String sCatelogueXmlFilePath = null;
		String sCatelogueZipFilePath = null;
		String sCatalogueFileName = null;
		String sIsFullcatalogueDownload = "NO";
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		File oAdminFile = null;
		File oCatalogueFile = null;
		File oAllFolderFile = null;
		File oCatalogueDownloadFile = null;
		File fXML = null;
		File fECatalogue = null;
		GenerateCatalogueZip oGenerateCatalogueZip = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		ArrayList alCoverflowProdIds = null;
		Map<String, String> hmResult = null;
		ArrayList alAirlineDetails = null;
		PrintWriter pw = null;
		String sECatalogue = null;
		String sWelcomePage = null;
		String sPersonalShopper = null;
		try {
			
			sCatalogueFolderPath = System.getProperty("CatalogueFolderPath").trim();
			sInstallationPackagePath = System.getProperty("InstallationPackageFolderPath").trim();

			/*
			 * String sAirId = request.getParameter("airId"); sAirId =
			 * sAirId==null?"":sAirId.trim();
			 */

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey == null ? "" : sProductKey.trim();

			String sTypeOfRequest = request.getParameter("downloadType");
			sTypeOfRequest = sTypeOfRequest == null ? "" : sTypeOfRequest
					.trim();

			String sDeviceId = request.getParameter("deviceId");
			sDeviceId = sDeviceId == null ? " " : sDeviceId.trim();

			String sMacAddr = request.getParameter("macAddr");
			sMacAddr = sMacAddr == null ? " " : sMacAddr.trim();

			String sLastServiceVersion = request.getParameter("serviceVersion");
			sLastServiceVersion = sLastServiceVersion == null ? "": sLastServiceVersion.trim();

			String sLastVirtualDestopVersion = request.getParameter("virtualDesktopVersion");
			sLastVirtualDestopVersion = sLastVirtualDestopVersion == null ? "": sLastVirtualDestopVersion.trim();

			sAirCode = request.getParameter("airCode");
			sAirCode = sAirCode == null ? "" : sAirCode.trim();

			alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);

			if (alAirlineDetails != null) {
				sAirId = (String) alAirlineDetails.get(0);
				sAirId = sAirId == null ? "" : sAirId.trim();

				sAirName = (String) alAirlineDetails.get(1);
				sAirName = sAirName == null ? "" : sAirName.trim();
			}

			String sLastDownloadedCateDate = request.getParameter("LastDownloadedCateDate");
			sLastDownloadedCateDate = sLastDownloadedCateDate == null ? "": sLastDownloadedCateDate.trim();

			/*
			 * String sAirId = "1005"; String sLastDownloadedCateDate = "JUN 03
			 * 2009 02:45:25 AM";
			 */

			logger.info("CatalogueAction::getUpdatedPackage::Device Id......."+ sDeviceId);
			logger.info("CatalogueAction::getUpdatedPackage::Mac Address......."+ sMacAddr);
			logger.info("CatalogueAction::getUpdatedPackage::Requested Service Version......."+ sLastServiceVersion);
			logger.info("CatalogueAction::getUpdatedPackage::Requested Virtual Desktop Version......."+ sLastVirtualDestopVersion);
			logger.info("CatalogueAction::getUpdatedPackage::sAirCode......."+ sAirId);
			logger.info("CatalogueAction::getUpdatedPackage::sLastDownloadDate......."+ sLastDownloadedCateDate);

			String sInetAddress = InetAddress.getLocalHost().getHostName();

			response.setContentType("application/xml");
			pw = response.getWriter();
			if (sLastDownloadedCateDate.trim().length() > 0 && sLastDownloadedCateDate.equalsIgnoreCase("FullCatalogue")) {
				sCatelogueZipFilePath = System.getProperty("FullCatalogueZipFolderPath").trim();
				sCatelogueZipFilePath = sCatelogueZipFilePath == null ? "": sCatelogueZipFilePath.trim();
				sCatalogueFileName = System.getProperty("CatalogueFileName").trim();
				sCatalogueFileName = sCatalogueFileName == null ? "": sCatalogueFileName.trim();
				oAllFolderFile = new File(sCatelogueZipFilePath + "/"+ sCatalogueFileName + "_" + sAirId + ".zip");
				if (oAllFolderFile != null && oAllFolderFile.exists()) {
					fis = new FileInputStream(oAllFolderFile);
					sIsFullcatalogueDownload = "YES";
				}
			}
			if (sLastDownloadedCateDate.trim().length() > 0
					&& sIsFullcatalogueDownload.equalsIgnoreCase("NO")) {
				if (sAirId.trim().length() > 0 && sAirCode.trim().length() > 0
						&& !sAirId.equals(sAirCode)) {
					sIsAirlineChanged = "YES";
				}
				// sLastDownloadedCateDate = "FullCatalogue";

				UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO = isUpdatedVersion(
						sLastServiceVersion, sLastVirtualDestopVersion,
						sTypeOfRequest, sLastDownloadedCateDate, sAirId, null);
				sUpdatedServiceVersion = oUpdatedCatalogueDetailsVO.getServiceVersion();
				sUpdatedVirtualDesktopVersion = oUpdatedCatalogueDetailsVO.getVirtualDesktopVersion();
				sIsDownloadCatatalogueAvailable = isDownloadCatalogueAvailable(sLastDownloadedCateDate, sAirId, sIsAirlineChanged);

				if ((sUpdatedServiceVersion != null && sUpdatedServiceVersion.trim().length() > 0)
						|| (sUpdatedVirtualDesktopVersion != null && sUpdatedVirtualDesktopVersion.trim().length() > 0)
						|| "CatalogueAvailable".equalsIgnoreCase(sIsDownloadCatatalogueAvailable)) {
					if ("ALL".equalsIgnoreCase(sTypeOfRequest)) {
						// To create the file name to generate the zip file.
						sZipFileName = "ALL_CATALOGUEDOWNLOAD_" + sDeviceId
								+ ".zip";
						String sFileName = null;
						// Instantiate Generate catalogue zip to create a zip
						// file
						oGenerateCatalogueZip = new GenerateCatalogueZip(sInstallationPackagePath, sZipFileName);

						if (sUpdatedServiceVersion != null && sUpdatedServiceVersion.trim().length() > 0) {
							String sAdminFolderPath = sCatalogueFolderPath + oUpdatedCatalogueDetailsVO.getServiceInstallationPath();
							oAdminFile = new File(sAdminFolderPath);
						}
						if (sUpdatedVirtualDesktopVersion != null
								&& sUpdatedVirtualDesktopVersion.trim().length() > 0) {
							String sECatalogueFolderPath = sCatalogueFolderPath
									+ oUpdatedCatalogueDetailsVO
											.getVirtualDesktopInstallationPath();
							oCatalogueFile = new File(sECatalogueFolderPath);
						}

						sCatelogueXmlFilePath = System.getProperty(
								"GenerateXmlPath").trim();
						sCatelogueXmlFilePath = sCatelogueXmlFilePath == null ? ""
								: sCatelogueXmlFilePath.trim();
						sCatelogueXmlFilePath = sCatelogueXmlFilePath + "/XML/"
								+ sDeviceId;
						fXML = new File(sCatelogueXmlFilePath);
						if (!fXML.exists()) {
							fXML.mkdir();
						}
						if ("CatalogueAvailable"
								.equalsIgnoreCase(sIsDownloadCatatalogueAvailable)) {
							alCoverflowProdIds = oGenerateCatalogue
									.generateCatalogueXML(
											sCatelogueXmlFilePath, "CLIENT",
											sAirId, "", "", "", "", "",
											sDeviceId, "");

							sCatalogueFileName = System.getProperty(
									"CatalogueFileName").trim();
							sCatalogueFileName = sCatalogueFileName == null ? ""
									: sCatalogueFileName.trim();
							sCommaSepCoverflowProdIds = Utils
									.listToCommaSepertedString(alCoverflowProdIds);
							// sCatalogueFileName=
							// sCatalogueFileName+"_"+sDeviceId+".zip";
							sCatalogueFileName = sCatalogueFileName + ".zip";
							sDownloadCatalogueFolderPath = sCatalogueFolderPath
									+ "/DownloadCatalogue/" + sDeviceId;
							fECatalogue = new File(sDownloadCatalogueFolderPath);
							if (!fECatalogue.exists()) {
								fECatalogue.mkdir();
							}
							hmResult = PlaceOrderDAO.getCatalogueDetailsByDate(
									sLastDownloadedCateDate,
									sCatalogueFolderPath, sCatalogueFileName,
									sAirId, sDeviceId,
									sCommaSepCoverflowProdIds,
									sIsAirlineChanged, sCatelogueXmlFilePath,
									sDownloadCatalogueFolderPath, null);
							sDownloadDate = hmResult.get("DownloadDate");
							sDownloadDate = sDownloadDate == null ? ""
									: sDownloadDate.trim();

							sECatalogue = hmResult.get("ECatalogue");
							sECatalogue = sECatalogue == null ? "": sECatalogue.trim();

							sWelcomePage = hmResult.get("WelcomePage");
							sWelcomePage = sWelcomePage == null ? "": sWelcomePage.trim();

							sPersonalShopper = hmResult.get("PersonalShopper");
							sPersonalShopper = sPersonalShopper == null ? "": sPersonalShopper.trim();

							
							
							if (sECatalogue != null && !"".equalsIgnoreCase(sECatalogue)) {
								DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
										"ECatalogue", 0, sMacAddr, "",
										sInetAddress, "CLIENT");
							}
							
							if (sWelcomePage != null && !"".equalsIgnoreCase(sWelcomePage)) {
								DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
										"Welcome Page", 0, sMacAddr, "",
										sInetAddress, "CLIENT");
							}
							
							if (sPersonalShopper != null && !"".equalsIgnoreCase(sPersonalShopper)) {
								DeviceRegDAO.insertDeviceLogDetails(sAirId, sDeviceId, "",
										"Personal Shopper", 0, sMacAddr, "",
										sInetAddress, "CLIENT");
							}
							
							DeviceRegDAO.insertDeviceLogDetails(sAirId,
									sDeviceId, "", "Catalogue Download", 0,
									sMacAddr, "", sInetAddress, "CLIENT");

							String sCataloguePath = sDownloadCatalogueFolderPath
									+ "/" + sCatalogueFileName;
							oCatalogueDownloadFile = new File(sCataloguePath);

							oGenerateCatalogueZip.addFile(
									sDownloadCatalogueFolderPath + "/",
									oCatalogueDownloadFile.getName(),
									oCatalogueDownloadFile.getName());
							sUpdatesDownloaded = sUpdatesDownloaded
									+ "ECatalogue|";
						}
						if (oAdminFile != null && oAdminFile.exists()
								&& oAdminFile.isFile()) {
							// oGenerateCatalogueZip.addFile(sInstallationPackagePath+"/Admin/",
							// oAdminFile.getName(), oAdminFile.getName());
							oGenerateCatalogueZip.addFile(
									sInstallationPackagePath + "/Service/",
									oAdminFile.getName(), oAdminFile.getName());
							sUpdatesDownloaded = sUpdatesDownloaded
									+ "Service|";
						}
						if (oCatalogueFile != null && oCatalogueFile.exists()
								&& oCatalogueFile.isFile()) {
							// oGenerateCatalogueZip.addFile(sInstallationPackagePath+"/Catalogue/",
							// oCatalogueFile.getName(),
							// oCatalogueFile.getName());
							oGenerateCatalogueZip.addFile(
									sInstallationPackagePath + "/Virtual/",
									oCatalogueFile.getName(), oCatalogueFile
											.getName());
							sUpdatesDownloaded = sUpdatesDownloaded
									+ "VirtualDesktop|";
						}
						// This is set to true because the zip file created
						// should be deleted in this case alone. For other two
						// cases the zip files are required.
						isECatalogueAvailable = true;

						if (sUpdatesDownloaded != null
								&& sUpdatesDownloaded.trim().length() > 0) {
							sUpdatesDownloaded = sUpdatesDownloaded.substring(
									0, sUpdatesDownloaded.trim().length() - 2);
						}
						oGenerateCatalogueZip.finish();

						oAllFolderFile = new File(sInstallationPackagePath,
								sZipFileName);
						if (oAllFolderFile != null && oAllFolderFile.exists())
							fis = new FileInputStream(oAllFolderFile);

						sUpdatedServiceVersion = oUpdatedCatalogueDetailsVO
								.getServiceVersion();
						sUpdatedVirtualDesktopVersion = oUpdatedCatalogueDetailsVO
								.getVirtualDesktopVersion();
						response.setHeader("UpdatedServiceVersion",
								sUpdatedServiceVersion);
						response.setHeader("UpdatedVirtualVersion",
								sUpdatedVirtualDesktopVersion);
						response.setHeader("DownloadDate", sDownloadDate);

					}
				}
			}
			response.setHeader("AirCode", sAirId);
			response.setHeader("AirName", sAirName);
			response.setHeader("FileSize", oAllFolderFile.length() + "");
			if (oAllFolderFile != null) {
				// System.out.println("FileSize : "+oAllFolderFile.length() +"
				// DeviceId:"+sDeviceId);
				if (fis != null) {
					bis = new BufferedInputStream(fis);
					// System.out.println("Read
					// Bytes:Enter::DeviceId:"+sDeviceId);
					for (int readBytes = 0; (readBytes = bis.read()) != -1;)
						pw.write(readBytes);
					// System.out.println("Read
					// Bytes:Exit::DeviceId:"+sDeviceId);

					insertPackageDownloadLogDetails(sDeviceId,
							"Installation Package Download", sTypeOfRequest,
							sUpdatedServiceVersion,
							sUpdatedVirtualDesktopVersion, "", sMacAddr,
							sInetAddress);
					insertAutoUpdateDeviceLogDetails(sAirId, sDeviceId,
							"Auto Update Activity", sUpdatedServiceVersion,
							sUpdatedVirtualDesktopVersion, null, null,
							sUpdatesDownloaded, sMacAddr, sInetAddress,
							"CLIENT");
				}
			}

			logger
					.info("CatalogueAction::getUpdatedPackage::Response Service Version  "
							+ sUpdatedServiceVersion);
			logger
					.info("CatalogueAction::getUpdatedPackage::Response Virtual Version  "
							+ sUpdatedVirtualDesktopVersion);
			logger
					.info("CatalogueAction::getUpdatedPackage::Response Download Catalogue  "
							+ sIsDownloadCatatalogueAvailable);
			logger.info("CatalogueAction::getUpdatedPackage::File Size  "
					+ oAllFolderFile.length() + "");
			logger
					.info("CatalogueAction::getUpdatedPackage::Last Catalogue Download  "
							+ sLastDownloadedCateDate);
			logger
					.info("CatalogueAction::getUpdatedPackage::Catalogue Download Date  "
							+ sDownloadDate);
		} catch (Exception e) {
			sFwrdKey = "failure";
			response.setHeader("ErrorMsg", e.getMessage());
			logger.error("CatalogueAction::getUpdatedPackage:Exception "
					+ e.getMessage());
		} finally {
			if (pw != null) {
				pw.flush();
				pw.close();
			}
			if (fis != null) {
				fis.close();
			}
			if (bis != null) {
				bis.close();
			}

			if (fXML != null && fXML.exists()) {
				deleteFiles(fXML);
			}
			if (fECatalogue != null && fECatalogue.exists()) {
				deleteFiles(fECatalogue);
			}

			if (sIsFullcatalogueDownload.equalsIgnoreCase("NO")) {
				if (oAllFolderFile != null && oAllFolderFile.exists()) {
					oAllFolderFile.delete();
				}
			}
		}
		logger.info("CatalogueAction::getUpdatedPackage::EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward updateLatestVersionAndStatus(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::updateLatestVersionAndStatus::ENTER");

		String sFwrdKey = null;
		String sErrorMsg = null;
		String sUpdatedStatus = null;

		try {

			String sProductKey = request.getParameter("productKey");
			sProductKey = sProductKey == null ? "" : sProductKey.trim();

			String sDeviceId = request.getParameter("deviceId");
			sDeviceId = sDeviceId == null ? " " : sDeviceId.trim();

			String sMacAddr = request.getParameter("macAddr");
			sMacAddr = sMacAddr == null ? " " : sMacAddr.trim();

			String sLastAdminVersion = request.getParameter("serviceVersion");
			sLastAdminVersion = sLastAdminVersion == null ? " "
					: sLastAdminVersion.trim();

			String sLastCatalogueVersion = request
					.getParameter("virtualDesktopVersion");
			sLastCatalogueVersion = sLastCatalogueVersion == null ? " "
					: sLastCatalogueVersion.trim();

			/*
			 * String sLastAutoUpdateVersion =
			 * request.getParameter("AutoUpdateVersion"); sLastAutoUpdateVersion =
			 * sLastAutoUpdateVersion == null ?"
			 * ":sLastAutoUpdateVersion.trim();
			 */

			logger
					.info("CatalogueAction::updateLatestVersionAndStatus::Device Id......."
							+ sDeviceId);
			logger
					.info("CatalogueAction::updateLatestVersionAndStatus::Mac Address......."
							+ sMacAddr);
			logger
					.info("CatalogueAction::updateLatestVersionAndStatus::Updated Admin Version......."
							+ sLastAdminVersion);
			logger
					.info("CatalogueAction::updateLatestVersionAndStatus::Updated Shopping Cart Version......."
							+ sLastCatalogueVersion);
			// logger.info("CatalogueAction::updateLatestVersionAndStatus::Updated
			// Auto Update Version......."+sLastAutoUpdateVersion);

			updateLatestVersionandStatus(sDeviceId, sLastAdminVersion,
					sLastCatalogueVersion, null);
			sUpdatedStatus = "True";

		} catch (Exception e) {
			sFwrdKey = "failure";
			sErrorMsg = e.getMessage();
			sUpdatedStatus = "False";
			logger
					.error("CatalogueAction::updateLatestVersionAndStatus::Exception "
							+ e.getMessage());
		} finally {
			response.setHeader("UpdatedStatus", sUpdatedStatus);
			response.setHeader("ErrorMsg", sErrorMsg);
		}
		logger.info("CatalogueAction::updateLatestVersionAndStatus:::EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward generateCatalogue(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::generateCatalogueXml:ENTER");
		String sFwrdKey = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		LoginVO oLoginVO = null;
		HttpSession oSession = request.getSession(true);
		String sOwnerId = null, sURI = null, sPath = null;
		try {
			logger.info("CatalogueAction::generateCatalogueXml:ENTER");
			String sUserType = request.getParameter("UserType");
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");

			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId == null ? "" : sOwnerId.trim();
			}

			if (sUserType.equalsIgnoreCase(VENDOR))
				sFwrdKey = "geneateVendorCateSuccess";
			else if (sUserType.equalsIgnoreCase(AIRLINE))
				sFwrdKey = "geneateAirlineCateSuccess";
			else
				sFwrdKey = "geneateAdminCateSuccess";

			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sE_CatalogueFileName = System.getProperty(
					"e-CatalogueFileName").trim();
			String sProdPreviewXmlPath = System.getProperty(
					"ProdPreviewXMLPath").trim();
			String sProdPreviewSwfPath = System.getProperty(
					"ProdPreviewSWFPath").trim();
			String sServerPath = getServlet().getServletContext().getRealPath(
					"");

			oGenerateCatalogue.generateCatalogueXML(sDestPath, sUserType,
					sOwnerId, oLoginVO.getUserId(), sServerPath, "", "",
					"PREVIEWCATALOGUE", "", "");
			// String sProdPreviewXmlPath =
			// System.getProperty("ProdPreviewXMLPath").trim();
			// String sProdPreviewSwfPath =
			// System.getProperty("ProdPreviewSWFPath").trim();
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
					.getLastUpdatedCatalogueId();

			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("eCatalogueFileName", sProdPreviewXmlPath
					+ sE_CatalogueFileName + "_" + oLoginVO.getUserId()
					+ ".xml");
			oSession.setAttribute("eCatalogueCoverFileName",
					sProdPreviewXmlPath + sE_CatalogueFileName + "_cover_"
							+ oLoginVO.getUserId() + ".xml");

			logger.info("CatalogueAction::generateCatalogueXml:EXit");
		} catch (Exception e) {
			sFwrdKey = "failure";
			e.printStackTrace();
			logger.error((new StringBuilder(
					"CatalogueAction::generateCatalogueXml:Exception "))
					.append(e.getMessage()).toString());
		}
		logger.info("CatalogueAction::generateCatalogueXml:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	// Airline Product
	public ActionForward initAddAirlineProduct(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initAddAirlineProduct:ENTER");
		String sFrdKey = "initAddProductSuccess";
		ArrayList alVendorList = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		try {
			request.setAttribute("mode", "ProductAdd");
			oSession.removeAttribute("airProdId");
			oSession.removeAttribute("productDetailsForm");
			// saveToken(request);
			logger.info("CatalogueAction::initAddAirlineProduct:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("VendorAction::initAddVendorProduct:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward addAirlineProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addAirlineProductDetails:ENTER");
		String sFrdKey = "addProductSuccess";
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		boolean bIsProdCodeExist = false;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		String sMode = "";
		String sFullDescription = request.getParameter("fullDescription");
		String sShortDescription = request.getParameter("shortDescription");

		try {
			// if(isTokenValid(request)){
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			String sAirProdId = (String) oSession.getAttribute("airProdId");
			sAirProdId = sAirProdId == null ? "" : sAirProdId.trim();
			if (sAirProdId.trim().length() > 0) {
				sMode = "UPDATE";
				oProductDetailsForm.setProdId(sAirProdId);
			} else
				sMode = "INSERT";

			if (sMode.equalsIgnoreCase("INSERT")) {
				bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oLoginVO
						.getUserType(), oLoginVO.getRefId(),
						oProductDetailsForm.getProdCode(), null);
			}
			if (!bIsProdCodeExist) {
				if (oProductDetailsForm != null) {
					if (oProductDetailsForm.getSbhComment() == null)
						oProductDetailsForm.setSbhComment("");

					if (oProductDetailsForm.getBrandName() == null)
						oProductDetailsForm.setBrandName("");
					else
						oProductDetailsForm.setBrandName(oProductDetailsForm
								.getBrandName().trim());

					/*
					 * if(oProductDetailsForm.getValidFromDate()==null)
					 * oProductDetailsForm.setValidFromDate(""); else
					 * oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());
					 * 
					 * if(oProductDetailsForm.getValidToDate()==null)
					 * oProductDetailsForm.setValidToDate(""); else
					 * oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());
					 */

					if (oProductDetailsForm.getInstructions() == null)
						oProductDetailsForm.setInstructions("");
					else
						oProductDetailsForm.setInstructions(oProductDetailsForm
								.getInstructions().trim());

					if (oProductDetailsForm.getLongDesc() == null)
						oProductDetailsForm.setLongDesc("");
					else
						oProductDetailsForm
								.setLongDesc(sFullDescription.trim());

					if (oProductDetailsForm.getShortDesc() == null)
						oProductDetailsForm.setShortDesc("");
					else
						oProductDetailsForm.setShortDesc(sShortDescription
								.trim());

					int iProdId = CatalogueDAO.addProductDetails(
							oProductDetailsForm, oLoginVO, sMode);
					oProductDetailsForm.setProdId(iProdId + "");

					// **** Update Product audit details ****//
					String sInsertedItemDetails = CatalogueDAO
							.getInsertedItemDetails(oProductDetailsForm,
									oLoginVO.getUserType());
					CatalogueDAO.insertProdAuditDetails(iProdId + "", oLoginVO
							.getRefId(), oLoginVO.getUserType(),
							sInsertedItemDetails, null, null, null, oLoginVO
									.getUserId(), "PRODUCT ADDED");

					BeanUtils.copyProperties(oProductDetailsVO,
							oProductDetailsForm);
					if (oProductDetailsVO != null)
						oProductDetailsVO.setOwnerType(oLoginVO.getUserType());

					oSession.setAttribute("airlineProductDetails",
							oProductDetailsVO);
					oSession.setAttribute("airlineProductFormDetails",
							oProductDetailsForm);
					oSession.setAttribute("airProdId", iProdId + "");
				}
				// resetToken(request);
				// }

			} else {
				sFrdKey = "initAddProductSuccess";
				request.setAttribute("errorMsg",
						"Product Code is already exist");
			}

			logger.info("CatalogueAction::addAirlineProductDetails:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("VendorAction::addAirlineProductDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward addUploadAirlineProductImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addUploadAirlineProductImage:ENTER");

		String sFrdKey = "addProductSuccess";
		String sProductStatus = "";
		ProductDetailsVO oProductDetailsVO = null;
		HttpSession oSession = request.getSession();
		UploadImageForm oUploadImageForm = (UploadImageForm) form;
		String sImageName = null;
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;
		ProductDetailsForm oProductDetailsForm = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();

		String sMainFileName = null, sMainImageType = null, sView1FileName = null, sView1ImageType = null, sImgType = null;
		String sView2FileName = null, sView2ImageType = null, sView3FileName = null, sView3ImageType = null;
		try {
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("airlineProductDetails");
			oProductDetailsForm = (ProductDetailsForm) oSession
					.getAttribute("airlineProductFormDetails");

			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

			String sImageUploadPath = System.getProperty("ImageUploadPath")
					.trim();
			String sImageViewPath = System.getProperty("ImageViewPath").trim();

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCatalogueDate = dateFormat.format(date);
			oProductDetailsVO.setCategoryDate(sCatalogueDate);

			String sOwnerId = oProductDetailsVO.getOwnerId();
			String sProdId = oProductDetailsVO.getProdId();
			String sOwnerType = oProductDetailsVO.getOwnerType();

			String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
					+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
			sImageName = sOwnerId + sProdId;

			FormFile oMainFormFile = oUploadImageForm.getMainImgPath();
			FormFile oView1FormFile = oUploadImageForm.getView1ImgPath();
			FormFile oView2FormFile = oUploadImageForm.getView2ImgPath();
			FormFile oView3FormFile = oUploadImageForm.getView3ImgPath();

			// Create Large,thumb,medium images
			if (oMainFormFile != null
					&& !oMainFormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oMainFormFile,
						sImageViewPath, sImageUploadPath, sImageName, "main",
						"Main", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setMainImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setMainImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setMainImgCap(oUploadImageForm
						.getMainImgCap());
			}
			if (oView1FormFile != null
					&& !oView1FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView1FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view1",
						"View1", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView1ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView1ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView1ImgCap(oUploadImageForm
						.getView1ImgCap());
			}
			if (oView2FormFile != null
					&& !oView2FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView2FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view2",
						"View2", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView2ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView2ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView2ImgCap(oUploadImageForm
						.getView2ImgCap());
			}
			if (oView3FormFile != null
					&& !oView3FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView3FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view3",
						"View3", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView3ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView3ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView3ImgCap(oUploadImageForm
						.getView3ImgCap());
			}
			oProductDetailsVO.setIsAdvt("N");

			sProductStatus = CatalogueDAO.getProductStatus(sProdId);
			oProductDetailsVO.setSbhProdStatus(sProductStatus);
			oSession.setAttribute("airlineProductDetails", oProductDetailsVO);

			CatalogueDAO.updateProductImage(oProductDetailsVO, oLoginVO);

			// send mail to admin when a new product is added
			CatalogueDAO.sendAddProdInfoByEmail(oProductDetailsForm,
					oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "", "");

			// send a mail to vendor if the product status inactive
			if (oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I")) {
				CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
						oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
						"");
			}

			/*
			 * String sDestPath = System.getProperty("GenerateXmlPath").trim();
			 * String sProdXmlContent =
			 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
			 * String xmlContent = sProdXmlContent.replaceAll("\"", "&quot;"); //
			 * System.out.println("xmlContent : "+xmlContent);
			 * oSession.setAttribute("ProductXmlContent", xmlContent);
			 */

			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sE_CatalogueFileName = System.getProperty(
					"e-CatalogueFileName").trim();
			String sServerPath = getServlet().getServletContext().getRealPath(
					"");

			oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
					.getUserType(), sOwnerId, oLoginVO.getUserId(),
					sServerPath, oProductDetailsVO.getProdId(),
					oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
					oProductDetailsVO.getSeqId());
			// String sProdPreviewXmlPath =
			// System.getProperty("ProdPreviewXMLPath").trim();
			String sProdPreviewSwfPath = System.getProperty(
					"ProdPreviewSWFPath").trim();
			Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
					.getLastUpdatedCatalogueId();

			oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("CataloguePath"));
			oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
					+ mUpdatedSwfIds.get("WelcomepagePath"));
			oSession.setAttribute("eCatalogueProdFileName",
					sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
							+ ".xml");

			request.setAttribute("mode", "addAirlineProduct");
			logger.info("CatalogueAction::addUploadAirlineProductImage:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::addUploadAirlineProductImage:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward skipUploadAirlineProductImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::skipUploadAirlineProductImage:ENTER");

		String sFrdKey = "addProductSuccess";
		String sProductStatus = "";
		ProductDetailsVO oProductDetailsVO = null;
		HttpSession oSession = request.getSession();
		UploadImageForm oUploadImageForm = (UploadImageForm) form;
		String sImageName = null;
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;
		List<CategoryVO> alCategory = null;
		ProductDetailsForm oProductDetailsForm = null;

		try {
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("airlineProductDetails");
			oProductDetailsForm = (ProductDetailsForm) oSession
					.getAttribute("airlineProductFormDetails");

			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

			alCategory = (List<CategoryVO>) oSession.getServletContext()
					.getAttribute("Category");
			String sImageUploadPath = System.getProperty("ImageUploadPath")
					.trim();
			String sImageViewPath = System.getProperty("ImageViewPath").trim();
			String sNoimagePath = System.getProperty("noImagePath").trim();
			sNoimagePath = sNoimagePath == null ? "" : sNoimagePath.trim();
			oProductDetailsVO.setNoImgPath(sNoimagePath);

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCatalogueDate = dateFormat.format(date);
			oProductDetailsVO.setCategoryDate(sCatalogueDate);

			String sOwnerId = oProductDetailsVO.getOwnerId();
			String sProdId = oProductDetailsVO.getProdId();
			String sOwnerType = oProductDetailsVO.getOwnerType();

			String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
					+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
			sImageName = sOwnerId + sProdId;

			FormFile oMainFormFile = oUploadImageForm.getMainImgPath();
			FormFile oView1FormFile = oUploadImageForm.getView1ImgPath();
			FormFile oView2FormFile = oUploadImageForm.getView2ImgPath();
			FormFile oView3FormFile = oUploadImageForm.getView3ImgPath();

			// Create Large,thumb,medium images
			/*
			 * if(oMainFormFile!= null &&
			 * !oMainFormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oMainFormFile,sImageViewPath,sImageUploadPath,sImageName,"main","Main","PrivateJetJaunts",sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setMainImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setMainImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setMainImgCap(oUploadImageForm.getMainImgCap()); }
			 * if(oView1FormFile!= null &&
			 * !oView1FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView1FormFile,sImageViewPath,sImageUploadPath,sImageName,"view1","View1","PrivateJetJaunts",sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView1ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView1ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView1ImgCap(oUploadImageForm.getView1ImgCap()); }
			 * if(oView2FormFile!= null &&
			 * !oView2FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView2FormFile,sImageViewPath,sImageUploadPath,sImageName,"view2","View2","PrivateJetJaunts",sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView2ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView2ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView2ImgCap(oUploadImageForm.getView2ImgCap()); }
			 * if(oView3FormFile!= null &&
			 * !oView3FormFile.getFileName().equals("")){ alImageInfo =
			 * ImageScale.createProductImage(oView3FormFile,sImageViewPath,sImageUploadPath,sImageName,"view3","View3","PrivateJetJaunts",sSrcImgPath,sOwnerType,sOwnerId);
			 * if(alImageInfo !=null){
			 * oProductDetailsVO.setView3ImgType((String)alImageInfo.get(0));
			 * oProductDetailsVO.setView3ImgPath((String)alImageInfo.get(1)); }
			 * oProductDetailsVO.setView3ImgCap(oUploadImageForm.getView3ImgCap()); }
			 */

			oProductDetailsForm.setInShopStatus("I");
			sProductStatus = CatalogueDAO.getProductStatus(sProdId);
			oProductDetailsVO.setSbhProdStatus(sProductStatus);
			oSession.setAttribute("airlineProductDetails", oProductDetailsVO);

			CatalogueDAO.updateProductStatus(oProductDetailsVO.getProdId(),
					oLoginVO.getUserId());

			// send mail to admin when a new product is added
			CatalogueDAO.sendIncompleteProdInfoByEmail(oProductDetailsForm,
					oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "", "");

			// send a mail to vendor if the product status inactive
			/*
			 * if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I")){
			 * CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,oProductDetailsVO,oLoginVO); }
			 */

			request.setAttribute("mode", "addAirlineProduct");
			logger.info("CatalogueAction::skipUploadAirlineProductImage:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::skipUploadAirlineProductImage:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward initSearchAirlineCatalogue(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initSearchAirlineCatalogue:ENTER");
		String sFwrdKey = "initSearchCatalogueSuccess";
		SearchCatalogueForm oSearchCatalogueForm = (SearchCatalogueForm) form;
		HttpSession oSession = request.getSession();
		try {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date date = new java.util.Date();
			String sCurrentDate = dateFormat.format(date);
			// System.out.println("Current Date: " + sCurrentDate);
			oSearchCatalogueForm.setUploadToDate(sCurrentDate);

			oSession.removeAttribute("searchCatalogueForm");
		} catch (Exception e) {
			sFwrdKey = "failure";
			e.printStackTrace();
			logger
					.error("CatalogueAction::initSearchAirlineCatalogue:Exception "
							+ e.getMessage());
		}
		logger.info("CatalogueAction::initSearchAirlineCatalogue:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward searchAirlineCatalogue(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::searchAirlineCatalogue:ENTER");
		String sFrdKey = "searchCatalogueSuccess";
		List<ProductDetailsVO> alCatelogueInfo = null;
		SearchCatalogueForm oSearchCatalogueForm = (SearchCatalogueForm) form;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sOwnerId = null, sOwnerType = null;
		String sCategory = null, sUploadFromDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus == null ? "" : sApprovalStatus
					.trim();

			if (oSearchCatalogueForm != null) {
				sCategory = oSearchCatalogueForm.getCategory();
				sCategory = sCategory == null ? "" : sCategory.trim();
				oSearchCatalogueForm.setCategory(sCategory);

				sUploadFromDate = oSearchCatalogueForm.getUploadFromDate();
				sUploadFromDate = sUploadFromDate == null ? ""
						: sUploadFromDate.trim();
				oSearchCatalogueForm.setUploadFromDate(sUploadFromDate);

				if (sApprovalStatus.trim().length() > 0) {
					oSearchCatalogueForm.setSbhProdStatus(sApprovalStatus);

					Date dFromDate = new java.util.Date();
					dFromDate.setDate(dFromDate.getDate() - 7);
					oSearchCatalogueForm.setUploadFromDate(dateFormat
							.format(dFromDate));

					if (sApprovalStatus.equalsIgnoreCase("N"))
						oSearchCatalogueForm.setProdStatus("A");
				}

			}
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerType = oLoginVO.getUserType();
			}
			alCatelogueInfo = CatalogueDAO.getCatalogueDetails(
					oSearchCatalogueForm, sOwnerId, sOwnerType);
			if (alCatelogueInfo != null && alCatelogueInfo.size() > 0) {
				request.setAttribute("catelogueInfo", alCatelogueInfo);
			} else {
				request.setAttribute("NoRecords", "noRecords");
			}
			logger.info("CatalogueAction::searchAirlineCatalogue:EXIT");
		} catch (Exception e) {
			/*
			 * //errors.add(Globals.ERROR_KEY,new
			 * ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
			 */
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::searchAirlineCatalogue:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward editAirlineProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::editAirlineProductDetails:ENTER");
		String sFrdKey = "editProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		String sImageViewPath = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		try {
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");
				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);
				oSession.setAttribute("airlineProductDetails",
						oProductDetailsVO);
				request.setAttribute("mode", "ProductEdit");
			}
			logger.info("CatalogueAction::editAirlineProductDetails:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::editAirlineProductDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward updateAirlineProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::updateAirlineProductDetails:ENTER");

		String sFrdKey = "updateProductSuccess";
		String sProductStatus = "";
		String sLongDescription = "";
		String sShortDescription = "";
		boolean isChanged = false;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sImageName = null;
		LoginVO oLoginVO = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		ProductDetailsVO oOldProductDetailsVO = null;
		List<CommentsVO> alProdComments = null;
		boolean bUploadImg = false, bIsProdCodeExist = false;
		int iProdId = 0;

		ArrayList alImageInfo = null;
		String sInShopStatus = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		try {
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("airlineProductDetails");
			oOldProductDetailsVO = new ProductDetailsVO();
			BeanUtils.copyProperties(oOldProductDetailsVO, oProductDetailsVO);
			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oProductDetailsVO
					.getOwnerType(), oProductDetailsVO.getOwnerId(),
					oProductDetailsForm.getProdCode(), oProductDetailsForm
							.getProdId());

			if (!bIsProdCodeExist) {
				/*
				 * if(oProductDetailsForm.getValidFromDate()==null)
				 * oProductDetailsForm.setValidFromDate(""); else
				 * oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());
				 * 
				 * if(oProductDetailsForm.getValidToDate()==null)
				 * oProductDetailsForm.setValidToDate(""); else
				 * oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());
				 * 
				 * if(oProductDetailsForm.getValidFromDate()==null)
				 * oProductDetailsForm.setValidFromDate(""); else
				 * oProductDetailsForm.setValidFromDate(oProductDetailsForm.getValidFromDate().trim());
				 * 
				 * if(oProductDetailsForm.getValidToDate()==null)
				 * oProductDetailsForm.setValidToDate(""); else
				 * oProductDetailsForm.setValidToDate(oProductDetailsForm.getValidToDate().trim());
				 */

				if (oProductDetailsForm.getInstructions() == null)
					oProductDetailsForm.setInstructions("");
				else
					oProductDetailsForm.setInstructions(oProductDetailsForm
							.getInstructions().trim());

				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

				if (oProductDetailsVO != null) {
					sInShopStatus = oProductDetailsVO.getInShopStatus();
					sInShopStatus = sInShopStatus == null ? "" : sInShopStatus
							.trim();

					if (oProductDetailsVO.getSbhProdStatus() != null)
						oProductDetailsForm.setSbhProdStatus(oProductDetailsVO
								.getSbhProdStatus());

					if (oProductDetailsVO.getCategoryDate() != null)
						oProductDetailsForm.setCategoryDate(oProductDetailsVO
								.getCategoryDate());
				}
				String sImageUploadPath = System.getProperty("ImageUploadPath")
						.trim();
				String sImageViewPath = System.getProperty("ImageViewPath")
						.trim();

				// String sCateId= oProductDetailsVO.getCateId();
				String sOwnerId = oProductDetailsVO.getOwnerId();
				String sProdId = oProductDetailsVO.getProdId();
				String sOwnerType = oProductDetailsVO.getOwnerType();

				String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
						+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
				sImageName = sOwnerId + sProdId;

				FormFile oMainFormFile = oProductDetailsForm
						.getUploadMainImagePath();
				FormFile oView1FormFile = oProductDetailsForm
						.getUploadView1ImagePath();
				FormFile oView2FormFile = oProductDetailsForm
						.getUploadView2ImagePath();
				FormFile oView3FormFile = oProductDetailsForm
						.getUploadView3ImagePath();

				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")
						|| oView1FormFile != null
						&& !oView1FormFile.getFileName().equals("")
						|| oView2FormFile != null
						&& !oView2FormFile.getFileName().equals("")
						|| oView3FormFile != null
						&& !oView3FormFile.getFileName().equals("")) {
					bUploadImg = true;
				}

				// Create Large,thumb,medium images
				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oMainFormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"main", "Main", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setMainImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setMainImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setMainImgCap(oProductDetailsForm
							.getUploadMainImgCap());
				}

				if (oView1FormFile != null
						&& !oView1FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView1FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view1", "View1", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView1ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView1ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView1ImgCap(oProductDetailsForm
							.getUploadView1ImgCap());
				} else if (oProductDetailsForm.getDeleteView1Img() != null
						&& oProductDetailsForm.getDeleteView1Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View1",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView1ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView1ImgType("");
					oProductDetailsVO.setView1ImgPath("");
					bUploadImg = true;
				}

				if (oView2FormFile != null
						&& !oView2FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView2FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view2", "View2", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView2ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView2ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView2ImgCap(oProductDetailsForm
							.getUploadView2ImgCap());
				} else if (oProductDetailsForm.getDeleteView2Img() != null
						&& oProductDetailsForm.getDeleteView2Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View2",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView2ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView2ImgType("");
					oProductDetailsVO.setView2ImgPath("");
					bUploadImg = true;
				}

				if (oView3FormFile != null
						&& !oView3FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView3FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view3", "View3", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView3ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView3ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView3ImgCap(oProductDetailsForm
							.getUploadView3ImgCap());
				} else if (oProductDetailsForm.getDeleteView3Img() != null
						&& oProductDetailsForm.getDeleteView3Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View3",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView3ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView3ImgType("");
					oProductDetailsVO.setView3ImgPath("");
					bUploadImg = true;
				}

				if (oProductDetailsForm.getBrandName() == null)
					oProductDetailsForm.setBrandName("");
				else
					oProductDetailsForm.getBrandName().trim();

				if (oProductDetailsForm != null) {

					if (oProductDetailsVO.getMainImgType() == null
							|| !(oProductDetailsVO.getMainImgType().trim()
									.length() > 0))
						oProductDetailsForm.setInShopStatus("I");

					/** ** Update Product audit details *** */
					CatalogueDAO.insertProductAuditTrail(oProductDetailsForm,
							oOldProductDetailsVO, oLoginVO);

					String sModifiedFields = CatalogueDAO
							.getUpdatedItemDetails(oProductDetailsForm,
									oOldProductDetailsVO, oLoginVO);

					//
					if (bUploadImg || sModifiedFields.trim().length() > 0)
						iProdId = CatalogueDAO.updateProductDetails(
								oProductDetailsForm, oProductDetailsVO,
								oLoginVO);

					if (oProductDetailsForm.getSbhComment() != null
							&& !oProductDetailsForm.getSbhComment()
									.equalsIgnoreCase(""))
						MerchandizeDAO.addProductComments(oProductDetailsForm
								.getOwnerId(),
								oProductDetailsVO.getOwnerType(),
								oProductDetailsForm.getProdId(),
								oProductDetailsForm.getSbhComment(), oLoginVO
										.getUserId());

					String sShortDesc = oProductDetailsForm.getShortDesc();
					sShortDesc = sShortDesc == null ? "" : sShortDesc.trim();
					sShortDescription = sShortDesc;
					sShortDesc = sShortDesc.replaceAll(" ", "&nbsp;");
					oProductDetailsForm.setShortDesc(sShortDesc);

					String sLongDesc = oProductDetailsForm.getLongDesc();
					sLongDesc = sLongDesc == null ? "" : sLongDesc.trim();
					sLongDescription = sLongDesc;
					sLongDesc = sLongDesc.replaceAll(" ", "&nbsp;");
					oProductDetailsForm.setLongDesc(sLongDesc);

					BeanUtils.copyProperties(oProductDetailsVO,
							oProductDetailsForm);
					sProductStatus = CatalogueDAO.getProductStatus(sProdId);
					oProductDetailsVO.setSbhProdStatus(sProductStatus);
					oSession.setAttribute("airlineProductDetails",
							oProductDetailsVO);

					alProdComments = MerchandizeDAO.getProductComments(
							oProductDetailsForm.getOwnerId(), oProductDetailsVO
									.getOwnerType(), oProductDetailsForm
									.getProdId(), oLoginVO.getUserId());

					if (alProdComments != null && alProdComments.size() > 0) {
						request.setAttribute("productComments", alProdComments);
					}
				}

				// CatalogueDAO.updateProductImage(oProductDetailsVO,oLoginVO);

				// if vednor change any product information send a mail To Admin
				// and CC to vendor
				CatalogueDAO.sendEditProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oOldProductDetailsVO, oLoginVO,
						oLoginVO.getUserId(), "", "");

				// send a mail to vendor if the product status is chaged from
				// active to inactive
				if (!sInShopStatus
						.equals(oProductDetailsForm.getInShopStatus())) {
					CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
							oProductDetailsVO, oLoginVO, oLoginVO.getUserId(),
							"", "");
				}

				/*
				 * String sDestPath =
				 * System.getProperty("GenerateXmlPath").trim(); String
				 * sProdXmlContent =
				 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
				 * String xmlContent = sProdXmlContent.replaceAll("\"",
				 * "&quot;"); // System.out.println("xmlContent : "+xmlContent);
				 * oSession.setAttribute("ProductXmlContent", xmlContent);
				 */

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

				if (sLongDescription != null
						&& sLongDescription.trim().length() > 0) {
					oProductDetailsVO.setLongDesc(sLongDescription);
					isChanged = true;
				}
				if (sShortDescription != null
						&& sShortDescription.trim().length() > 0) {
					oProductDetailsVO.setShortDesc(sShortDescription);
					isChanged = true;
				}
				if (isChanged) {
					oSession.setAttribute("merchandizeDetails",
							oProductDetailsVO);
				}
			} else {
				sFrdKey = "updateAirlineProductFailure";
				request.setAttribute("ErrMsg", "Item Code already exist.");
			}
			logger.info("CatalogueAction::updateAirlineProductDetails:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::updateVendorProductDetails:EXCEPTION "
							+ e.getMessage());
		}
		request.setAttribute("mode", "editAirlineProduct");
		return mapping.findForward(sFrdKey);
	}

	/*
	 * public ActionForward updateAirlineProductDetails(ActionMapping mapping,
	 * ActionForm form, HttpServletRequest request, HttpServletResponse
	 * response) throws Exception {
	 * logger.info("CatalogueAction::updateAirlineProductDetails:ENTER");
	 * 
	 * String sFrdKey="updateProductSuccess",sImagePath=null; HttpSession
	 * oSession = request.getSession(); ArrayList<CategoryVO> alCategory =
	 * null; ProductDetailsForm oProductDetailsForm=(ProductDetailsForm)form;
	 * String sSubFolderName = null,sImageName = null; String sMedImgPath =
	 * null,sProdImgPath= null,sThumbImgPath= null,sLargeImgPath
	 * =null,sImageType = null; GenerateProdCatalogue oGenerateProdCatalogue =
	 * new GenerateProdCatalogue(); LoginVO oLoginVO = null; ProductDetailsVO
	 * oProductDetailsVO =null; try{ oLoginVO =(LoginVO)
	 * oSession.getAttribute("loginInfo"); oProductDetailsVO =(ProductDetailsVO)
	 * oSession.getAttribute("airlineProductDetails");
	 * if(oProductDetailsForm!=null){ //int iProdId =
	 * CatalogueDAO.updateProductDetails(oProductDetailsForm,oLoginVO);
	 * BeanUtils.copyProperties(oProductDetailsVO, oProductDetailsForm);
	 * oSession.setAttribute("airlineProductDetails",oProductDetailsVO); }
	 * logger.info("CatalogueAction::updateAirlineProductDetails:EXIT");
	 * }catch(Exception e){
	 * 
	 * //errors.add(Globals.ERROR_KEY,new
	 * ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));
	 * 
	 * sFrdKey=("failure"); e.printStackTrace();
	 * logger.error("CatalogueAction::updateVendorProductDetails:EXCEPTION
	 * "+e.getMessage()); } return mapping.findForward(sFrdKey); }
	 */
	public ActionForward editUploadAirlineProductImage(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::editUploadAirlineProductImage:ENTER");
		String sFrdKey = "addProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		HttpSession oSession = request.getSession();
		ArrayList<CategoryVO> alCategory = null;
		UploadImageForm oUploadImageForm = (UploadImageForm) form;
		String sCateFolderName = null, sImageName = null;
		GenerateProdCatalogue oGenerateProdCatalogue = new GenerateProdCatalogue();
		LoginVO oLoginVO = null;
		ArrayList alImageInfo = null;

		String sMainFileName = null, sMainImageType = null, sView1FileName = null, sView1ImageType = null, sImgType = null;
		String sView2FileName = null, sView2ImageType = null, sView3FileName = null, sView3ImageType = null;
		try {
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("airlineProductDetails");
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

			String sImageUploadPath = System.getProperty("ImageUploadPath")
					.trim();
			String sImageViewPath = System.getProperty("ImageViewPath").trim();

			// String sCateId= oProductDetailsVO.getCateId();
			String sOwnerId = oProductDetailsVO.getOwnerId();
			String sProdId = oProductDetailsVO.getProdId();
			String sOwnerType = oProductDetailsVO.getOwnerType();

			String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
					+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
			sImageName = sOwnerId + sProdId;

			FormFile oMainFormFile = oUploadImageForm.getMainImgPath();
			FormFile oView1FormFile = oUploadImageForm.getView1ImgPath();
			FormFile oView2FormFile = oUploadImageForm.getView2ImgPath();
			FormFile oView3FormFile = oUploadImageForm.getView3ImgPath();

			// Create Large,thumb,medium images
			if (oMainFormFile != null
					&& !oMainFormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oMainFormFile,
						sImageViewPath, sImageUploadPath, sImageName, "main",
						"Main", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setMainImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setMainImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setMainImgCap(oUploadImageForm
						.getMainImgCap());
			}
			if (oView1FormFile != null
					&& !oView1FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView1FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view1",
						"View1", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView1ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView1ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView1ImgCap(oUploadImageForm
						.getView1ImgCap());
			}
			if (oView2FormFile != null
					&& !oView2FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView2FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view2",
						"View2", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView2ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView2ImgPath((String) alImageInfo
							.get(1));
				}
				oProductDetailsVO.setView2ImgCap(oUploadImageForm
						.getView2ImgCap());
			}
			if (oView3FormFile != null
					&& !oView3FormFile.getFileName().equals("")) {
				alImageInfo = ImageScale.createProductImage(oView3FormFile,
						sImageViewPath, sImageUploadPath, sImageName, "view3",
						"View3", "PrivateJetJaunts", sSrcImgPath, sOwnerType,
						sOwnerId);
				if (alImageInfo != null) {
					oProductDetailsVO.setView3ImgType((String) alImageInfo
							.get(0));
					oProductDetailsVO.setView3ImgPath((String) alImageInfo
							.get(1));
				}

				oProductDetailsVO.setView3ImgCap(oUploadImageForm
						.getView3ImgCap());

			}
			oSession.setAttribute("airlineProductDetails", oProductDetailsVO);

			CatalogueDAO.updateProductImage(oProductDetailsVO, oLoginVO);
			String sDestPath = System.getProperty("GenerateXmlPath").trim();
			String sProdXmlContent = oGenerateProdCatalogue.createXML(
					oProductDetailsVO.getProdId(), oProductDetailsVO
							.getOwnerId(), oProductDetailsVO.getCateId(),
					sDestPath, oLoginVO.getUserType());
			String xmlContent = sProdXmlContent.replaceAll("\"", "&quot;");
			// System.out.println("xmlContent : "+xmlContent);
			oSession.setAttribute("ProductXmlContent", xmlContent);

			logger.info("CatalogueAction::editUploadAirlineProductImage:EXIT");
		} catch (Exception e) {

			// errors.add(Globals.ERROR_KEY,new
			// ActionError("error.db",IMessage.ERROR_RETRIEVE_MSG));

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::editUploadAirlineProductImage:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward getCatalogueStatusDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::getCatalogueStatusDetails:ENTER");
		String sFrdKey = "vendorCateDetailsSuccess";
		List<ProductDetailsVO> alCatalogueStatusInfo = null;
		LoginVO oLoginVO = null;
		HttpSession oSession = request.getSession();
		String sOwnerId = null, sOwnerType = null, sURI = null, sPath = null;
		try {
			String sCateStatus = request.getParameter("CatalogueStatus");
			sCateStatus = sCateStatus == null ? "" : sCateStatus.trim();
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");

			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId == null ? "" : sOwnerId.trim();

				sOwnerType = oLoginVO.getUserType();
				sOwnerType = sOwnerType == null ? "" : sOwnerType.trim();

				if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(ADMIN)) {
					sFrdKey = "adminCateDetailsSuccess";
				} else if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(AIRLINE)) {
					sFrdKey = "airlineCateDetailsSuccess";
				}
			}

			alCatalogueStatusInfo = CatalogueDAO.getCatalogueStatusDetails(
					sCateStatus, sOwnerId, sOwnerType);
			if (alCatalogueStatusInfo != null
					&& alCatalogueStatusInfo.size() > 0) {
				request
						.setAttribute("cateStatusDetails",
								alCatalogueStatusInfo);
			}
			request.setAttribute("catalogueStatus", sCateStatus);
			logger.info("CatalogueAction::getCatalogueStatusDetails:EXIT");
		} catch (Exception e) {

			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::getCatalogueStatusDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward deleteProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::deleteProductDetails:ENTER");
		String sFrdKey = "deleteVendorProductSuccess", sURI = null, sPath = null;
		ArrayList alCatalogueStatusInfo = null;
		LoginVO oLoginVO = null;
		HttpSession oSession = request.getSession();
		String sOwnerId = null, sOwnerName = null, sImageViewPath = null, sCateFolderName = null;
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		try {

			String sProdId = request.getParameter("ProdId");
			sProdId = sProdId == null ? "" : sProdId.trim();
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId == null ? "" : sOwnerId.trim();

				sOwnerName = oLoginVO.getUserId();
				sOwnerName = sOwnerName == null ? "" : sOwnerName.trim();

				if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(ADMIN)) {
					sFrdKey = "deleteAdminProductSuccess";
				} else if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(AIRLINE)) {
					sFrdKey = "deleteAirlineProductSuccess";
				}
			}

			CatalogueDAO.deleteProduct(sProdId, sOwnerName);

			/** ** Update Product audit details *** */
			CatalogueDAO.insertProdAuditDetails(sProdId, oLoginVO.getRefId(),
					oLoginVO.getUserType(), "", null, null, null, oLoginVO
							.getUserId(), "PRODUCT DELETED");

			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)) {
				if (alCategory != null && alCategory.size() > 0) {
					for (CategoryVO oCategoryVO : alCategory) {
						if (oCategoryVO.getCateId() != null) {
							if (oCategoryVO.getCateId().equals(
									oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}
					}
				}
			} else if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(
					AIRLINE)) {
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if ((oProductDetailsVO.getMainImgPath() != null && oProductDetailsVO
						.getMainImgPath().trim().length() > 0)
						&& (oProductDetailsVO.getMainImgType() != null && oProductDetailsVO
								.getMainImgType().trim().length() > 0))
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath(oProductDetailsVO
							.getNoImgPath());

				CatalogueDAO.sendDeletedProductsByEmail(oProductDetailsVO,
						oLoginVO, oLoginVO.getUserId(), "", sCateFolderName);
			}
			logger.info("CatalogueAction::deleteProductDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::deleteProductDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward initAddAdvertisement(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initAddAdvertisement:ENTER");
		String sForwardKey = "AirlineInitAddAdvertisementSuccess";
		try {
			HttpSession oSession = request.getSession();
			ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
			oSession.removeAttribute("productDetailsForm");
		} catch (Exception e) {
			sForwardKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::initAddAdvertisement:EXCEPTION "
					+ e.getMessage());
		}
		logger.info("CatalogueAction::initAddAdvertisement:EXIT");
		return mapping.findForward(sForwardKey);

	}

	public ActionForward addAdvertisement(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addAdvertisement:ENTER");

		String sForwardKey = "AirlineAddAdvertisementSuccess";
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sImageName = null;
		LoginVO oLoginVO = null;
		String sURI = null;
		String sPath = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		boolean bIsProdCodeExist = false;
		String sAirProdId = (String) oSession.getAttribute("ProductId");
		String sMode = "INSERT";
		String sImageUploadPath = System.getProperty("ImageUploadPath").trim();
		String sImageViewPath = System.getProperty("ImageViewPath").trim();
		int iProdId = 0;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		ArrayList alImageInfo = null;
		try {

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);

			if (AIRLINE.equalsIgnoreCase(sPath))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

			sAirProdId = sAirProdId == null ? "" : sAirProdId.trim();

			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oLoginVO
					.getUserType(), oLoginVO.getRefId(), oProductDetailsForm
					.getProdCode(), oProductDetailsForm.getProdId());

			if (!bIsProdCodeExist) {
				iProdId = CatalogueDAO.addAdvertisementDetails(
						oProductDetailsForm, oLoginVO, sMode, oLoginVO
								.getUserType());
				oProductDetailsForm.setProdId(iProdId + "");

				BeanUtils
						.copyProperties(oProductDetailsVO, oProductDetailsForm);
				// **** Update Product audit details ****//
				String sInsertedItemDetails = CatalogueDAO
						.getInsertedItemDetails(oProductDetailsForm, oLoginVO
								.getUserType());
				CatalogueDAO.insertProdAuditDetails(iProdId + "", oLoginVO
						.getRefId(), oLoginVO.getUserType(),
						sInsertedItemDetails, null, null, null, oLoginVO
								.getUserId(), "PRODUCT ADDED");

				String sOwnerId = oLoginVO.getRefId();
				String sProdId = oProductDetailsForm.getProdId();
				String sOwnerType = oLoginVO.getUserType();

				String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
						+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
				sImageName = sOwnerId + sProdId;

				FormFile oMainFormFile = oProductDetailsForm
						.getAdvtMainImgPath();
				FormFile oSwfFormFile = oProductDetailsForm
						.getAdvertisementPath();
				FormFile oView1FormFile = oProductDetailsForm
						.getUploadView1ImagePath();
				FormFile oView2FormFile = oProductDetailsForm
						.getUploadView2ImagePath();
				FormFile oView3FormFile = oProductDetailsForm
						.getUploadView3ImagePath();

				/*
				 * if(oMainFormFile != null &&
				 * !oMainFormFile.getFileName().equals("")){ bUploadImg = true; }
				 */

				// Create Large,thumb,medium images
				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oMainFormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"main", "Main", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setMainImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setMainImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setMainImgCap(oProductDetailsForm
							.getUploadMainImgCap());
				}
				if (oView1FormFile != null
						&& !oView1FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView1FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view1", "View1", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView1ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView1ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView1ImgCap(oProductDetailsForm
							.getUploadView1ImgCap());
				}
				if (oView2FormFile != null
						&& !oView2FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView2FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view2", "View2", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView2ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView2ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView2ImgCap(oProductDetailsForm
							.getUploadView2ImgCap());
				}
				if (oView3FormFile != null
						&& !oView3FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView3FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view3", "View3", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView3ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView3ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView3ImgCap(oProductDetailsForm
							.getUploadView3ImgCap());
				}
				if (oSwfFormFile != null
						&& !"".equals(oSwfFormFile.getFileName())) {
					alImageInfo = ImageScale.createAdvertisementApplication(
							oSwfFormFile, sImageViewPath, sImageUploadPath,
							sImageName, "main", "Main", "PrivateJetJaunts",
							sSrcImgPath, sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO
								.setAdvertisementType((String) alImageInfo
										.get(0));
						oProductDetailsVO
								.setAdvertisementPath((String) alImageInfo
										.get(1));
					}
				}
				oProductDetailsVO.setInShopStatus(oProductDetailsForm
						.getInShopStatus());
				oProductDetailsVO
						.setProdCode(oProductDetailsForm.getProdCode());
				oProductDetailsVO.setProdTitle(oProductDetailsForm
						.getProdTitle());

				// Update the image path and the type.
				CatalogueDAO.updateAdvertisementImage(oProductDetailsVO,
						oLoginVO);

				oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
						oLoginVO.getUserType());

				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath() + "/"
							+ "PrivateJetJaunts" + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");

				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");

				CatalogueDAO.sendAddProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
						"");

				String sLongDesc = oProductDetailsForm.getLongDesc();
				sLongDesc = sLongDesc == null ? "" : sLongDesc.trim();
				// sLongDescription = sLongDesc;
				sLongDesc = sLongDesc.replaceAll(" ", "&nbsp;");
				oProductDetailsVO.setLongDesc(sLongDesc);

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				// String sProdPreviewXmlPath =
				// System.getProperty("ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

				oSession.setAttribute("AdvertisementProductDetails",
						oProductDetailsVO);
				oSession.setAttribute("AdvertisementProductFormDetails",
						oProductDetailsForm);
				oSession.setAttribute("ProductId", iProdId + "");
				request.setAttribute("mode", "addAdvertisementProduct");

			} else {
				sForwardKey = "AirlineInitAddAdvertisementSuccess";
				request.setAttribute("errorMsg",
						"Product Code is already exist");
			}
		} catch (Exception exception) {
			logger.debug("CatalogueAction::addAdvertisement::EXCEPTION"
					+ exception.getMessage());
			sForwardKey = "failure";
		}
		logger.info("CatalogueAction::addAdvertisement:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward initSearchAirlineAdvertisement(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initSearchAirlineAdvertisement:ENTER");

		String sForwardKey = "AirlineViewAdvertisementSuccess";
		HttpSession oSession = request.getSession();

		try {

			oSession.removeAttribute("searchCatalogueForm");
		} catch (Exception e) {
			sForwardKey = "failure";
			logger
					.debug("CatalogueAction::initSearchAirlineAdvertisement::EXCEPTION"
							+ e.getMessage());
		}
		logger.info("CatalogueAction::initSearchAirlineAdvertisement:EXIT");

		return mapping.findForward(sForwardKey);
	}

	public ActionForward searchAirlineAdvertisement(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::searchAirlineAdvertisement:ENTER");
		String sFrdKey = "AirlineViewAdvertisementSuccess";
		List<ProductDetailsVO> alCatelogueInfo = null;
		SearchCatalogueForm oSearchCatalogueForm = (SearchCatalogueForm) form;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sOwnerId = null, sOwnerType = null;
		String sCategory = null, sUploadFromDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus == null ? "" : sApprovalStatus
					.trim();

			if (oSearchCatalogueForm != null) {
				sCategory = oSearchCatalogueForm.getCategory();
				sCategory = sCategory == null ? "" : sCategory.trim();
				oSearchCatalogueForm.setCategory(sCategory);

				sUploadFromDate = oSearchCatalogueForm.getUploadFromDate();
				sUploadFromDate = sUploadFromDate == null ? ""
						: sUploadFromDate.trim();
				oSearchCatalogueForm.setUploadFromDate(sUploadFromDate);

				if (sApprovalStatus.trim().length() > 0) {
					oSearchCatalogueForm.setSbhProdStatus(sApprovalStatus);

					Date dFromDate = new java.util.Date();
					dFromDate.setDate(dFromDate.getDate() - 7);
					oSearchCatalogueForm.setUploadFromDate(dateFormat
							.format(dFromDate));

					if (sApprovalStatus.equalsIgnoreCase("N"))
						oSearchCatalogueForm.setProdStatus("A");
				}

			}
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerType = oLoginVO.getUserType();
			}
			alCatelogueInfo = CatalogueDAO.getAdvertisementCatalogueDetails(
					oSearchCatalogueForm, sOwnerId, sOwnerType);
			if (alCatelogueInfo != null && alCatelogueInfo.size() > 0) {
				request.setAttribute("catelogueInfo", alCatelogueInfo);
			} else {
				request.setAttribute("NoRecords", "noRecords");
			}
			logger.info("CatalogueAction::searchAirlineAdvertisement:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::searchAirlineAdvertisement:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward editAirlineAdvertisementDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::editAirlineAdvertisementDetails:ENTER");

		String sFrdKey = "editProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		String sImageViewPath = null, sURI = null, sPath = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		try {

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());

				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);

				/*
				 * oProductDetailsForm.setProdCode(oProductDetailsVO.getProdCode());
				 * oProductDetailsForm.setProdTitle(oProductDetailsVO.getProdTitle());
				 * oProductDetailsForm.setInShopStatus(oProductDetailsVO.getInShopStatus());
				 */

				if (oProductDetailsVO.getSwfPath() != null
						&& oProductDetailsVO.getSwfPath().trim().length() > 0)
					oProductDetailsVO.setSwfPath(sImageViewPath
							+ oProductDetailsVO.getSwfPath() + "/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "."
							+ oProductDetailsVO.getSwfType());
				else
					oProductDetailsVO.setSwfPath("");
				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSwfPath());

				oSession.setAttribute("airlineAdvertisementDetails",
						oProductDetailsVO);
				request.setAttribute("mode", "EditAdvertisement");
			}
			logger
					.info("CatalogueAction::editAirlineAdvertisementDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::editAirlineAdvertisementDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward updateAirlineAdvertisementDetails(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::updateAirlineAdvertisementDetails:ENTER");

		String sFrdKey = "updateProductSuccess";
		String sInShopStatus = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sImageName = null;
		LoginVO oLoginVO = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		ProductDetailsVO oOldProductDetailsVO = null;
		List<CommentsVO> alProdComments = null;
		boolean bUploadImg = false, bIsProdCodeExist = false;
		int iProdId = 0;
		String sURI = null, sPath = null;
		ArrayList alImageInfo = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		try {
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("airlineAdvertisementDetails");

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			if (ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
				oLoginVO.setRefId("");
			} else {
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			}
			oOldProductDetailsVO = new ProductDetailsVO();
			BeanUtils.copyProperties(oOldProductDetailsVO, oProductDetailsVO);

			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oProductDetailsVO
					.getOwnerType(), oProductDetailsVO.getOwnerId(),
					oProductDetailsForm.getProdCode(), oProductDetailsForm
							.getProdId());
			if (!bIsProdCodeExist) {
				oProductDetailsForm.setProdId(oProductDetailsVO.getProdId());

				iProdId = CatalogueDAO.addAdvertisementDetails(
						oProductDetailsForm, oLoginVO, "UPDATE", oLoginVO
								.getUserType());
				oProductDetailsForm.setProdId(iProdId + "");

				if (oProductDetailsForm.getInstructions() == null)
					oProductDetailsForm.setInstructions("");
				else
					oProductDetailsForm.setInstructions(oProductDetailsForm
							.getInstructions().trim());

				if (oProductDetailsVO != null) {
					sInShopStatus = oProductDetailsVO.getInShopStatus();
					sInShopStatus = sInShopStatus == null ? "" : sInShopStatus
							.trim();

					if (oProductDetailsVO.getCategoryDate() != null)
						oProductDetailsForm.setCategoryDate(oProductDetailsVO
								.getCategoryDate());
				}
				String sImageUploadPath = System.getProperty("ImageUploadPath")
						.trim();
				String sImageViewPath = System.getProperty("ImageViewPath")
						.trim();

				// String sCateId= oProductDetailsVO.getCateId();
				String sOwnerId = oProductDetailsVO.getOwnerId();
				String sProdId = oProductDetailsVO.getProdId();
				String sOwnerType = oProductDetailsVO.getOwnerType();

				String sSrcImgPath = sImageUploadPath + "\\SkyBuyPics\\"
						+ sOwnerType + "_" + sOwnerId + "\\OriginalImg\\";
				sImageName = sOwnerId + sProdId;

				FormFile oMainFormFile = oProductDetailsForm
						.getAdvtMainImgPath();
				FormFile oSwfFormFile = oProductDetailsForm
						.getAdvertisementPath();
				FormFile oView1FormFile = oProductDetailsForm
						.getUploadView1ImagePath();
				FormFile oView2FormFile = oProductDetailsForm
						.getUploadView2ImagePath();
				FormFile oView3FormFile = oProductDetailsForm
						.getUploadView3ImagePath();

				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")) {
					bUploadImg = true;
				}

				// Create Large,thumb,medium images
				if (oMainFormFile != null
						&& !oMainFormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oMainFormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"main", "Main", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setMainImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setMainImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setMainImgCap(oProductDetailsForm
							.getUploadMainImgCap());
				}

				if (oView1FormFile != null
						&& !oView1FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView1FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view1", "View1", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView1ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView1ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView1ImgCap(oProductDetailsForm
							.getUploadView1ImgCap());
				} else if (oProductDetailsForm.getDeleteView1Img() != null
						&& oProductDetailsForm.getDeleteView1Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View1",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView1ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView1ImgType("");
					oProductDetailsVO.setView1ImgPath("");
					bUploadImg = true;
				}
				if (oView2FormFile != null
						&& !oView2FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView2FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view2", "View2", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView2ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView2ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView2ImgCap(oProductDetailsForm
							.getUploadView2ImgCap());
				} else if (oProductDetailsForm.getDeleteView2Img() != null
						&& oProductDetailsForm.getDeleteView2Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View2",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView2ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView2ImgType("");
					oProductDetailsVO.setView2ImgPath("");
					bUploadImg = true;
				}
				if (oView3FormFile != null
						&& !oView3FormFile.getFileName().equals("")) {
					alImageInfo = ImageScale.createProductImage(oView3FormFile,
							sImageViewPath, sImageUploadPath, sImageName,
							"view3", "View3", "PrivateJetJaunts", sSrcImgPath,
							sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO.setView3ImgType((String) alImageInfo
								.get(0));
						oProductDetailsVO.setView3ImgPath((String) alImageInfo
								.get(1));
					}
					oProductDetailsVO.setView3ImgCap(oProductDetailsForm
							.getUploadView3ImgCap());
				} else if (oProductDetailsForm.getDeleteView3Img() != null
						&& oProductDetailsForm.getDeleteView3Img()
								.equals("yes")) {
					ImageScale.deleteProductImage(sImageUploadPath, "View3",
							"PrivateJetJaunts", sImageName, oProductDetailsVO
									.getView3ImgType(), sOwnerType, sOwnerId);
					oProductDetailsVO.setView3ImgType("");
					oProductDetailsVO.setView3ImgPath("");
					bUploadImg = true;
				}

				if (oSwfFormFile != null
						&& !"".equals(oSwfFormFile.getFileName())) {
					alImageInfo = ImageScale.createAdvertisementApplication(
							oSwfFormFile, sImageViewPath, sImageUploadPath,
							sImageName, "main", "Main", "PrivateJetJaunts",
							sSrcImgPath, sOwnerType, sOwnerId);
					if (alImageInfo != null) {
						oProductDetailsVO
								.setAdvertisementType((String) alImageInfo
										.get(0));
						oProductDetailsVO
								.setAdvertisementPath((String) alImageInfo
										.get(1));
					}
				}

				// Update the image path and the type.
				CatalogueDAO.updateAdvertisementImage(oProductDetailsVO,
						oLoginVO);

				CatalogueDAO.sendEditProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oOldProductDetailsVO, oLoginVO,
						oLoginVO.getUserId(), "", "");

				if (!oProductDetailsForm.getInShopStatus().equalsIgnoreCase(
						oOldProductDetailsVO.getInShopStatus())) {
					CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
							oProductDetailsVO, oLoginVO, oLoginVO.getUserId(),
							"", "");
				}
				if (oProductDetailsForm.getBrandName() == null)
					oProductDetailsForm.setBrandName("");
				else
					oProductDetailsForm.getBrandName().trim();

				if (oProductDetailsForm != null) {

					if (oProductDetailsVO.getMainImgType() == null
							|| !(oProductDetailsVO.getMainImgType().trim()
									.length() > 0))
						oProductDetailsForm.setInShopStatus("I");

					/** ** Update Product audit details *** */
					String sModifiedFields = CatalogueDAO
							.getUpdatedAdvertisementDetails(
									oProductDetailsForm, oOldProductDetailsVO,
									oLoginVO);

					//
					/*
					 * if(bUploadImg || sModifiedFields.trim().length()>0)
					 * iProdId =
					 * CatalogueDAO.updateAdvertisementDetails(oProductDetailsForm,oProductDetailsVO,oLoginVO);
					 */
					if (oProductDetailsForm.getSbhComment() != null
							&& !oProductDetailsForm.getSbhComment()
									.equalsIgnoreCase(""))
						MerchandizeDAO.addAdvertisementComments(
								oProductDetailsVO.getOwnerId(),
								oProductDetailsVO.getOwnerType(),
								oProductDetailsVO.getProdId(),
								oProductDetailsForm.getSbhComment(), oLoginVO
										.getUserId());

					String sLongDesc = oProductDetailsForm.getLongDesc();
					sLongDesc = sLongDesc == null ? "" : sLongDesc.trim();
					// sLongDescription = sLongDesc;
					sLongDesc = sLongDesc.replaceAll(" ", "&nbsp;");
					oProductDetailsForm.setLongDesc(sLongDesc);

					BeanUtils.copyProperties(oProductDetailsVO,
							oProductDetailsForm);
					oSession.setAttribute("airlineAdvertisementDetails",
							oProductDetailsVO);
					request.setAttribute("AdvertisementProductDetails",
							oProductDetailsVO);

					alProdComments = MerchandizeDAO.getProductComments(
							oProductDetailsVO.getOwnerId(), oProductDetailsVO
									.getOwnerType(), oProductDetailsVO
									.getProdId(), oLoginVO.getUserId());

					if (alProdComments != null && alProdComments.size() > 0) {
						request.setAttribute("productComments", alProdComments);
					}
				}

				/*
				 * String sDestPath =
				 * System.getProperty("GenerateXmlPath").trim(); String
				 * sProdXmlContent =
				 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
				 * String xmlContent = sProdXmlContent.replaceAll("\"",
				 * "&quot;"); // System.out.println("xmlContent : "+xmlContent);
				 * oSession.setAttribute("ProductXmlContent", xmlContent);
				 */

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");
			} else {
				sFrdKey = "updateAirlineAdvtFailure";
				request.setAttribute("ErrMsg", "Item Code already exist.");
			}

		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::updateAirlineAdvertisementDetails:EXCEPTION "
							+ e.getMessage());
		}
		request.setAttribute("mode", "editAirlineProduct");
		logger.info("CatalogueAction::updateAirlineAdvertisementDetails:EXIT");
		return mapping.findForward(sFrdKey);
	}

	public ActionForward deleteAdvertisementDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::deleteAdvertisementDetails:ENTER");
		String sFrdKey = "deleteVendorProductSuccess", sURI = null, sPath = null;
		LoginVO oLoginVO = null;
		HttpSession oSession = request.getSession();
		String sOwnerId = null, sOwnerName = null, sImageViewPath = null, sCateFolderName = null;
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		try {

			String sProdId = request.getParameter("ProdId");
			sProdId = sProdId == null ? "" : sProdId.trim();
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN)) {
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
				sFrdKey = "initSearchAirlineAdvtDetailsSuccess";
			}
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId == null ? "" : sOwnerId.trim();

				sOwnerName = oLoginVO.getUserId();
				sOwnerName = sOwnerName == null ? "" : sOwnerName.trim();

				if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(ADMIN)) {
					sFrdKey = "deleteAdminProductSuccess";
				} else if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(AIRLINE)) {
					sFrdKey = "deleteAirlineProductSuccess";
				}
			}

			CatalogueDAO.deleteAdvertisement(sProdId, sOwnerName);

			/** ** Update Product audit details *** */
			CatalogueDAO.insertProdAuditDetails(sProdId, oLoginVO.getRefId(),
					oLoginVO.getUserType(), "", null, null, null, oLoginVO
							.getUserId(), "PRODUCT DELETED");

			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			// CatalogueDAO.sendDeletedProductsByEmail(oProductDetailsVO,
			// oLoginVO);

			if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)) {
				if (alCategory != null && alCategory.size() > 0) {
					for (CategoryVO oCategoryVO : alCategory) {
						if (oCategoryVO.getCateId() != null) {
							if (oCategoryVO.getCateId().equals(
									oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}
					}
				}
			} else if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(
					AIRLINE)) {
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				oProductDetailsVO.setMainImgPath(sImageViewPath
						+ oProductDetailsVO.getMainImgPath() + "/"
						+ sCateFolderName + "/Thumb/"
						+ oProductDetailsVO.getOwnerId()
						+ oProductDetailsVO.getProdId() + "thumb."
						+ oProductDetailsVO.getMainImgType());
				CatalogueDAO.sendDeletedProductsByEmail(oProductDetailsVO,
						oLoginVO, oLoginVO.getUserId(), "", sCateFolderName);
			}
			logger.info("CatalogueAction::deleteAdvertisementDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::deleteAdvertisementDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward cancelAddProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::cancelAddProducts:ENTER");

		String sFrdKey = "VendorCancelSuccess";
		String sShortDesc = null;
		String sLongDesc = null;
		String sURI = null;
		String sPath = null;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;

		try {
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");

			if (oLoginVO.getUserType() != null
					&& oLoginVO.getUserType().equalsIgnoreCase(AIRLINE))
				sFrdKey = "AirlineCancelSuccess";

			sShortDesc = oProductDetailsForm.getShortDesc();
			sShortDesc = sShortDesc.replaceAll("<br/>", "\n");
			sShortDesc = sShortDesc.replaceAll("<br>", "\n");

			sLongDesc = oProductDetailsForm.getLongDesc();
			sLongDesc = sLongDesc.replaceAll("<br/>", "\n");
			sLongDesc = sLongDesc.replaceAll("<br>", "\n");

			oProductDetailsForm.setShortDesc(sShortDesc);
			oProductDetailsForm.setLongDesc(sLongDesc);

		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::cancelAddProducts:EXCEPTION "
					+ e.getMessage());
		}

		logger.info("CatalogueAction::cancelAddProducts:EXIT");

		return mapping.findForward(sFrdKey);
	}

	/* To Add/Search/Update and delete the special Products */

	public ActionForward initAddSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initAddSpecialProducts:ENTER");
		String sForwardKey = "initAddSpecialProductSuccess";
		ArrayList alVendorDetails = new ArrayList();
		try {
			HttpSession oSession = request.getSession();
			ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
			oSession.removeAttribute("productDetailsForm");
			request.setAttribute("mode", "addSpecialProduct");
			alVendorDetails = OrderDAO.getAllVendorNames();

			if (alVendorDetails != null && alVendorDetails.size() > 0)
				oSession.setAttribute("AllVendorNames", alVendorDetails);
		} catch (Exception e) {
			sForwardKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::initAddSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}

		logger.info("CatalogueAction::initAddSpecialProducts:EXIT");
		return mapping.findForward(sForwardKey);

	}

	public ActionForward addSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::addSpecialProducts:ENTER");
		String sForwardKey = "AddSpecialProductSuccess";
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sSpecialProdName = null;
		LoginVO oLoginVO = null;
		String sURI = null, sPath = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		boolean bUploadImg = false, bIsProdCodeExist = false;
		String sVendorId = null;
		String sMode = "INSERT";
		String sImageUploadPath = System.getProperty("ImageUploadPath").trim();
		String sImageViewPath = System.getProperty("ImageViewPath").trim();
		int iProdId = 0;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		ArrayList alSpecialProdInfo = null;
		try {

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");

			sVendorId = oProductDetailsForm.getOwnerId();
			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(VENDOR, sVendorId,
					oProductDetailsForm.getProdCode(), oProductDetailsForm
							.getProdId());

			if (!bIsProdCodeExist) {
				iProdId = CatalogueDAO.saveSpecialProdDetails(
						oProductDetailsForm, oLoginVO, sMode);
				oProductDetailsForm.setProdId(iProdId + "");

				BeanUtils
						.copyProperties(oProductDetailsVO, oProductDetailsForm);
				// **** Update Product audit details ****//
				String sInsertedItemDetails = CatalogueDAO
						.getInsertedItemDetails(oProductDetailsForm, oLoginVO
								.getUserType());
				CatalogueDAO.insertProdAuditDetails(iProdId + "", oLoginVO
						.getRefId(), oLoginVO.getUserType(),
						sInsertedItemDetails, null, null, null, oLoginVO
								.getUserId(), "PRODUCT ADDED");

				String sOwnerId = oLoginVO.getRefId();
				String sProdId = oProductDetailsForm.getProdId();
				String sOwnerType = oLoginVO.getUserType();

				// String
				// sSrcImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sOwnerType+"_"+sOwnerId+"\\OriginalImg\\";
				sSpecialProdName = sVendorId + sProdId;

				FormFile oSwfFormFile = oProductDetailsForm
						.getSpecialProdPath();

				if (oSwfFormFile != null
						&& !"".equals(oSwfFormFile.getFileName())) {
					alSpecialProdInfo = ImageScale.createSpecialProductSWF(
							oSwfFormFile, sImageViewPath, sImageUploadPath,
							sSpecialProdName, VENDOR, sVendorId);
					if (alSpecialProdInfo != null) {
						oProductDetailsVO
								.setSpecialProdType((String) alSpecialProdInfo
										.get(0));
						oProductDetailsVO
								.setSpecialProductPath((String) alSpecialProdInfo
										.get(1));
					}
				}
				oProductDetailsVO.setInShopStatus(oProductDetailsForm
						.getInShopStatus());
				oProductDetailsVO
						.setProdCode(oProductDetailsForm.getProdCode());

				// Update the image path and the type.
				CatalogueDAO.updateSpecialProdSwf(oProductDetailsVO, oLoginVO);

				oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
						oLoginVO.getUserType());

				CatalogueDAO.sendAddProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oLoginVO, oLoginVO.getUserId(), "",
						"");
				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSpecialProductPath());

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

				oSession.setAttribute("SpecialProductDetails",
						oProductDetailsVO);
				// oSession.setAttribute("AdvertisementProductFormDetails",oProductDetailsForm);
				oSession.setAttribute("ProductId", iProdId + "");

				if (bUploadImg) {
					// iProdId =
					// CatalogueDAO.updateProductDetails(oProductDetailsForm,oProductDetailsVO,oLoginVO);
				}

			} else {
				sForwardKey = "initAddSpecialProductSuccess";
				request.setAttribute("errorMsg",
						"Product Code is already exist");
			}

		} catch (Exception exception) {
			logger.debug("CatalogueAction::addSpecialProducts::EXCEPTION"
					+ exception.getMessage());
			sForwardKey = "failure";
		}
		logger.info("CatalogueAction::addSpecialProducts:EXIT");
		return mapping.findForward(sForwardKey);
	}

	public ActionForward initSearchSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::initSearchSpecialProducts:ENTER");

		String sForwardKey = "searchSpecialProductSuccess";
		HttpSession oSession = request.getSession();

		try {
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandizeForm");
			// oSession.removeAttribute("searchCatalogueForm");
		} catch (Exception e) {
			sForwardKey = "failure";
			logger
					.debug("CatalogueAction::initSearchSpecialProducts::EXCEPTION"
							+ e.getMessage());
		}
		logger.info("CatalogueAction::initSearchSpecialProducts:EXIT");

		return mapping.findForward(sForwardKey);
	}

	public ActionForward searchSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::searchSpecialProducts:ENTER");
		String sFrdKey = "searchSpecialProductSuccess";
		List<ProductDetailsVO> alCatelogueInfo = null;
		// SearchCatalogueForm oSearchCatalogueForm=(SearchCatalogueForm)form;
		SearchMerchandizeForm oSearchCatalogueForm = (SearchMerchandizeForm) form;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		String sOwnerId = null, sOwnerType = null, sURI = null, sPath = null;
		String sCategory = null, sUploadFromDate = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");

			sOwnerId = oLoginVO.getRefId();
			sOwnerType = oLoginVO.getUserType();
			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus == null ? "" : sApprovalStatus
					.trim();

			if (oSearchCatalogueForm != null) {
				sCategory = oSearchCatalogueForm.getCategory();
				sCategory = sCategory == null ? "" : sCategory.trim();
				oSearchCatalogueForm.setCategory(sCategory);

				sUploadFromDate = oSearchCatalogueForm.getUploadFromDate();
				sUploadFromDate = sUploadFromDate == null ? ""
						: sUploadFromDate.trim();
				oSearchCatalogueForm.setUploadFromDate(sUploadFromDate);

				if (sApprovalStatus.trim().length() > 0) {
					oSearchCatalogueForm.setSbhStatus(sApprovalStatus);

					Date dFromDate = new java.util.Date();
					dFromDate.setDate(dFromDate.getDate() - 7);
					oSearchCatalogueForm.setUploadFromDate(dateFormat
							.format(dFromDate));

					if (sApprovalStatus.equalsIgnoreCase("N"))
						oSearchCatalogueForm.setProdStatus("A");
				}

			}
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerType = oLoginVO.getUserType();
			}
			alCatelogueInfo = CatalogueDAO.getSpecialProductDetails(
					oSearchCatalogueForm, sOwnerId, sOwnerType);
			oSearchCatalogueForm.setRecordSet(alCatelogueInfo);
			if (alCatelogueInfo != null && alCatelogueInfo.size() > 0) {
				request.setAttribute("catelogueInfo", alCatelogueInfo);

			} else {
				request.setAttribute("NoRecords", "noRecords");
				oSearchCatalogueForm.setRecordSet(null);
			}
			logger.info("CatalogueAction::searchSpecialProducts:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::searchSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward editSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::editSpecialProducts:ENTER");
		String sFrdKey = "initAddSpecialProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		String sImageViewPath = null, sURI = null, sPath = null;
		String sSourceOfEdit = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		try {

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			sSourceOfEdit = request.getParameter("SourceOfEdit");
			sSourceOfEdit = sSourceOfEdit == null ? "" : sSourceOfEdit.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			if (oProductDetailsVO != null) {
				oProductDetailsVO.setSbhComment("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);

				/*
				 * oProductDetailsForm.setProdCode(oProductDetailsVO.getProdCode());
				 * oProductDetailsForm.setProdTitle(oProductDetailsVO.getProdTitle());
				 * oProductDetailsForm.setInShopStatus(oProductDetailsVO.getInShopStatus());
				 */

				if (oProductDetailsVO.getSpecialProductPath() != null
						&& oProductDetailsVO.getSpecialProductPath().trim()
								.length() > 0)
					oProductDetailsVO.setSpecialProductPath(sImageViewPath
							+ oProductDetailsVO.getSpecialProductPath() + "/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "."
							+ oProductDetailsVO.getSpecialProdType());
				else
					oProductDetailsVO.setSpecialProductPath("");
				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSpecialProductPath());

				oSession.setAttribute("SpecialProductDetails",
						oProductDetailsVO);
				request.setAttribute("mode", "editSpecialProduct");
				request.setAttribute("SourceOfEdit", sSourceOfEdit);
			}
			logger.info("CatalogueAction::editSpecialProducts:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::editSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward updateSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::updateSpecialProducts:ENTER");

		String sFrdKey = "AddSpecialProductSuccess";
		HttpSession oSession = request.getSession();

		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		String sSpecialProdName = null;
		String sInShopStatus = null;
		LoginVO oLoginVO = null;
		ProductDetailsVO oProductDetailsVO = new ProductDetailsVO();
		ProductDetailsVO oOldProductDetailsVO = null;
		List<CommentsVO> alProdComments = null;
		boolean bUploadImg = false, bIsProdCodeExist = false;
		int iProdId = 0;
		String sURI = null, sPath = null;
		ArrayList alSpecialProdInfo = null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		try {
			oProductDetailsVO = (ProductDetailsVO) oSession
					.getAttribute("SpecialProductDetails");

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			if (ADMIN.equalsIgnoreCase(sPath)) {
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
				oLoginVO.setRefId("");
			} else {
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			}
			oOldProductDetailsVO = new ProductDetailsVO();
			BeanUtils.copyProperties(oOldProductDetailsVO, oProductDetailsVO);

			bIsProdCodeExist = CatalogueDAO.isProdCodeExist(oProductDetailsVO
					.getOwnerType(), oProductDetailsVO.getOwnerId(),
					oProductDetailsForm.getProdCode(), oProductDetailsForm
							.getProdId());
			if (!bIsProdCodeExist) {
				oProductDetailsForm.setProdId(oProductDetailsVO.getProdId());

				iProdId = CatalogueDAO.saveSpecialProdDetails(
						oProductDetailsForm, oLoginVO, "UPDATE");
				oProductDetailsForm.setProdId(iProdId + "");

				if (oProductDetailsForm.getInstructions() == null)
					oProductDetailsForm.setInstructions("");
				else
					oProductDetailsForm.setInstructions(oProductDetailsForm
							.getInstructions().trim());

				if (oProductDetailsVO != null) {
					sInShopStatus = oProductDetailsVO.getInShopStatus();
					sInShopStatus = sInShopStatus == null ? "" : sInShopStatus
							.trim();

					if (oProductDetailsVO.getCategoryDate() != null)
						oProductDetailsForm.setCategoryDate(oProductDetailsVO
								.getCategoryDate());
				}
				String sImageUploadPath = System.getProperty("ImageUploadPath")
						.trim();
				String sImageViewPath = System.getProperty("ImageViewPath")
						.trim();

				// String sCateId= oProductDetailsVO.getCateId();
				String sOwnerId = oProductDetailsVO.getOwnerId();
				String sProdId = oProductDetailsVO.getProdId();
				String sOwnerType = oProductDetailsVO.getOwnerType();

				sSpecialProdName = sOwnerId + sProdId;

				FormFile oSwfFormFile = oProductDetailsForm
						.getSpecialProdPath();

				if (oSwfFormFile != null
						&& !oSwfFormFile.getFileName().equals("")) {
					bUploadImg = true;
				}

				if (oSwfFormFile != null
						&& !"".equals(oSwfFormFile.getFileName())) {
					alSpecialProdInfo = ImageScale.createSpecialProductSWF(
							oSwfFormFile, sImageViewPath, sImageUploadPath,
							sSpecialProdName, VENDOR, sOwnerId);
					if (alSpecialProdInfo != null) {
						oProductDetailsVO
								.setSpecialProdType((String) alSpecialProdInfo
										.get(0));
						oProductDetailsVO
								.setSpecialProductPath((String) alSpecialProdInfo
										.get(1));
					}
				}

				// Update the image path and the type.
				CatalogueDAO.updateSpecialProdSwf(oProductDetailsVO, oLoginVO);

				CatalogueDAO.sendEditProdInfoByEmail(oProductDetailsForm,
						oProductDetailsVO, oOldProductDetailsVO, oLoginVO,
						oLoginVO.getUserId(), "", "");

				if (!oProductDetailsForm.getInShopStatus().equalsIgnoreCase(
						oOldProductDetailsVO.getInShopStatus())) {
					CatalogueDAO.sendProdStatusByEmail(oProductDetailsForm,
							oProductDetailsVO, oLoginVO, oLoginVO.getUserId(),
							"", "");
				}
				if (oProductDetailsForm.getBrandName() == null)
					oProductDetailsForm.setBrandName("");
				else
					oProductDetailsForm.getBrandName().trim();

				if (oProductDetailsForm != null) {

					/** ** Update Product audit details *** */
					String sModifiedFields = CatalogueDAO
							.getUpdatedSpecialProductDetails(
									oProductDetailsForm, oOldProductDetailsVO,
									oLoginVO);

					//
					/*
					 * if(bUploadImg || sModifiedFields.trim().length()>0)
					 * iProdId =
					 * CatalogueDAO.updateAdvertisementDetails(oProductDetailsForm,oProductDetailsVO,oLoginVO);
					 */
					if (oProductDetailsForm.getSbhComment() != null
							&& !oProductDetailsForm.getSbhComment()
									.equalsIgnoreCase(""))
						MerchandizeDAO.addAdvertisementComments(
								oProductDetailsVO.getOwnerId(),
								oProductDetailsVO.getOwnerType(),
								oProductDetailsVO.getProdId(),
								oProductDetailsForm.getSbhComment(), oLoginVO
										.getUserId());

					oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
							oLoginVO.getUserType());

					if (oProductDetailsVO != null) {

						/*
						 * oProductDetailsForm.setProdCode(oProductDetailsVO.getProdCode());
						 * oProductDetailsForm.setProdTitle(oProductDetailsVO.getProdTitle());
						 * oProductDetailsForm.setInShopStatus(oProductDetailsVO.getInShopStatus());
						 */

						if (oProductDetailsVO.getSpecialProductPath() != null
								&& oProductDetailsVO.getSpecialProductPath()
										.trim().length() > 0)
							oProductDetailsVO
									.setSpecialProductPath(sImageViewPath
											+ oProductDetailsVO
													.getSpecialProductPath()
											+ "/"
											+ oProductDetailsVO.getOwnerId()
											+ oProductDetailsVO.getProdId()
											+ "."
											+ oProductDetailsVO
													.getSpecialProdType());
						else
							oProductDetailsVO.setSpecialProductPath("");
						oSession.setAttribute("SwfFileName", oProductDetailsVO
								.getSpecialProductPath());

						oSession.setAttribute("SpecialProductDetails",
								oProductDetailsVO);
						request.setAttribute("SpecialProductDetails",
								oProductDetailsVO);
					}

					alProdComments = MerchandizeDAO.getProductComments(
							oProductDetailsVO.getOwnerId(), oProductDetailsVO
									.getOwnerType(), oProductDetailsVO
									.getProdId(), oLoginVO.getUserId());

					if (alProdComments != null && alProdComments.size() > 0) {
						request.setAttribute("productComments", alProdComments);
					}
				}

				/*
				 * String sDestPath =
				 * System.getProperty("GenerateXmlPath").trim(); String
				 * sProdXmlContent =
				 * oGenerateProdCatalogue.createXML(oProductDetailsVO.getProdId(),oProductDetailsVO.getOwnerId(),oProductDetailsVO.getCateId(),sDestPath,oLoginVO.getUserType());
				 * String xmlContent = sProdXmlContent.replaceAll("\"",
				 * "&quot;"); // System.out.println("xmlContent : "+xmlContent);
				 * oSession.setAttribute("ProductXmlContent", xmlContent);
				 */
				/*
				 * if(oProductDetailsVO.getSpecialProductPath()!=null &&
				 * oProductDetailsVO.getSpecialProductPath().trim().length()>0)
				 * oProductDetailsVO.setSpecialProductPath(sImageViewPath+oProductDetailsVO.getSpecialProductPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSpecialProductPath());
				 * else oProductDetailsVO.setSpecialProductPath("");
				 */

				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSpecialProductPath());

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), sOwnerId, oLoginVO.getUserId(),
						sServerPath, oProductDetailsVO.getProdId(),
						oProductDetailsVO.getCateId(), "PRODCATALOGUE", "",
						oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");
			} else {
				sFrdKey = "initAddSpecialProductSuccess";
				request.setAttribute("ErrMsg", "Item Code already exist.");
			}

		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::updateSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}
		request.setAttribute("mode", "editAirlineProduct");
		logger.info("CatalogueAction::updateSpecialProducts:EXIT");
		return mapping.findForward(sFrdKey);
	}

	public ActionForward deleteSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::deleteSpecialProducts:ENTER");
		String sFrdKey = "deleteSpecialProductSuccess", sURI = null, sPath = null;
		LoginVO oLoginVO = null;
		HttpSession oSession = request.getSession();
		String sOwnerId = null, sOwnerName = null;
		ProductDetailsVO oProductDetailsVO = null;
		try {
			String sProdId = request.getParameter("ProdId");
			sProdId = sProdId == null ? "" : sProdId.trim();
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN)) {
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
				sFrdKey = "deleteSpecialProductSuccess";
			}

			if (oLoginVO != null) {
				sOwnerId = oLoginVO.getRefId();
				sOwnerId = sOwnerId == null ? "" : sOwnerId.trim();

				sOwnerName = oLoginVO.getUserId();
				sOwnerName = sOwnerName == null ? "" : sOwnerName.trim();

				if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(ADMIN)) {
					sFrdKey = "deleteSpecialProductSuccess";
				} else if (oLoginVO.getUserType() != null
						&& oLoginVO.getUserType().equalsIgnoreCase(AIRLINE)) {
					sFrdKey = "deleteSpecialProductSuccess";
				}
			}

			CatalogueDAO.deleteAdvertisement(sProdId, sOwnerName);

			/** ** Update Product audit details *** */
			CatalogueDAO.insertProdAuditDetails(sProdId, oLoginVO.getRefId(),
					oLoginVO.getUserType(), "", null, null, null, oLoginVO
							.getUserId(), "PRODUCT DELETED");

			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			CatalogueDAO.sendDeletedProductsByEmail(oProductDetailsVO,
					oLoginVO, oLoginVO.getUserId(), "", "");

			if (oProductDetailsVO != null) {
				// CatalogueDAO.sendDeletedProductsByEmail(oProductDetailsVO,oLoginVO);
			}
			logger.info("CatalogueAction::deleteSpecialProducts:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::deleteSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward initApproveSpecialProd(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("MerchandizeAction::initApproveSpecialProd:ENTER");
		String sFwrdKey = "approveSpecialProductSuccess";
		HttpSession oSession = request.getSession();
		SearchMerchandizeForm oSearchMerchandizeForm = (SearchMerchandizeForm) form;
		try {
			String sMerchandizeAction = (String) request
					.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null ? ""
					: sMerchandizeAction.trim();
			request.setAttribute("MerchandizeAction", sMerchandizeAction);
			if (sMerchandizeAction.equals("Approve"))
				sFwrdKey = "approveSpecialProductSuccess";
			if ("session".equals(mapping.getScope()))
				oSession.removeAttribute("searchMerchandizeForm");

		} catch (Exception e) {
			sFwrdKey = "failure";
			e.printStackTrace();
			logger.error("MerchandizeAction::initApproveSpecialProd:Exception "
					+ e.getMessage());
		}
		logger.info("MerchandizeAction::initApproveSpecialProd:EXIT");
		return mapping.findForward(sFwrdKey);
	}

	public ActionForward searchApproveSpecialProd(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("MerchandizeAction::searchApproveSpecialProd:ENTER");
		String sFrdKey = "approveSpecialProductSuccess";
		ArrayList alMerchandizeInfo = null;
		SearchMerchandizeForm oSearchMerchandizeForm = (SearchMerchandizeForm) form;
		HttpSession oSession = request.getSession();
		String sSearchBy = null, sSearchValue = null, sCategory = null, sEffectiveDate = null, sOwner = null, sSearchId = null;
		try {
			// For Approving the merchandize details
			String sMerchandizeAction = (String) request
					.getParameter("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null ? ""
					: sMerchandizeAction.trim();

			if (sMerchandizeAction.trim().length() == 0)
				sMerchandizeAction = (String) oSession
						.getAttribute("MerchandizeAction");
			sMerchandizeAction = sMerchandizeAction == null ? ""
					: sMerchandizeAction.trim();

			oSession.setAttribute("MerchandizeAction", sMerchandizeAction);
			if (sMerchandizeAction.equals("Approve"))
				sFrdKey = "approveSpecialProductSuccess";

			String sApprovalStatus = request.getParameter("ApprovalStatus");
			sApprovalStatus = sApprovalStatus == null ? "" : sApprovalStatus
					.trim();

			if (oSearchMerchandizeForm != null) {
				sSearchBy = oSearchMerchandizeForm.getSearchBy();
				sSearchBy = sSearchBy == null ? "" : sSearchBy.trim();
				sSearchId = sSearchBy;
				/*
				 * oStringTokenizer=new StringTokenizer(sSearchId,":");
				 * if(oStringTokenizer.hasMoreTokens())
				 * sOwner=oStringTokenizer.nextToken();
				 * sOwner=sOwner==null?"":sOwner.trim();
				 * if(oStringTokenizer.hasMoreTokens())
				 * sSearchId=oStringTokenizer.nextToken();
				 */
				sSearchId = sSearchId == null ? "" : sSearchId.trim();
				oSearchMerchandizeForm.setSearchBy(sSearchId);
				sSearchValue = oSearchMerchandizeForm.getSearchValue();
				sSearchValue = sSearchValue == null ? "" : sSearchValue.trim();
				oSearchMerchandizeForm.setSearchValue(sSearchValue);
				sCategory = oSearchMerchandizeForm.getCategory();
				sCategory = sCategory == null ? "" : sCategory.trim();
				oSearchMerchandizeForm.setCategory(sCategory);

				sEffectiveDate = oSearchMerchandizeForm.getEffectiveDate();
				sEffectiveDate = sEffectiveDate == null ? "" : sEffectiveDate
						.trim();
				oSearchMerchandizeForm.setEffectiveDate(sEffectiveDate);

				if (sApprovalStatus.trim().length() > 0) {
					oSearchMerchandizeForm.setSbhStatus(sApprovalStatus);
					oSearchMerchandizeForm.setCategory("All");
					oSearchMerchandizeForm.setSearchBy("All");
					oSearchMerchandizeForm.setSearchValue("");
					oSearchMerchandizeForm.setProdStatus("A");
				}
			}

			oSearchMerchandizeForm = CatalogueDAO
					.getNewSpecialProdDetails(oSearchMerchandizeForm);
			oSearchMerchandizeForm.setSearchBy(sSearchBy);
			if (oSearchMerchandizeForm.getRecordSet() != null
					&& oSearchMerchandizeForm.getRecordSet().size() > 0) {
				request.setAttribute("specialProdInfo", alMerchandizeInfo);
			} else {
				request.setAttribute("NoRecords", "noRecords");
			}

			if (sApprovalStatus.trim().length() > 0) {
				oSearchMerchandizeForm.setSearchBy("Admin:All");
			}

			logger.info("MerchandizeAction::searchApproveSpecialProd:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("MerchandizeAction::searchApproveSpecialProd:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	public ActionForward approveSpecialProd(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("MerchandizeAction::approveSpecialProd:ENTER");
		String sFrdKey = "approveOrRejectSpecialProductSuccess";
		String sSkyBuyLogoPath = null, sHMKey = null, sProdStatus = null;
		String sSkyBuyAddr1 = null, sSkyBuyAddr2 = null, sSkyBuyPh = null;
		ArrayList alMerchandizeInfo = null;
		List<CategoryVO> alCategory = null;
		SearchMerchandizeForm oSearchMerchandizeForm = (SearchMerchandizeForm) form;
		HttpSession oSession = request.getSession();
		LoginVO oLoginVO = null;
		int iIsUpdate = -1;
		try {

			oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			alCategory = (List<CategoryVO>) oSession.getServletContext()
					.getAttribute("Category");

			alMerchandizeInfo = (ArrayList) oSearchMerchandizeForm
					.getRecordSet();

			HashMap hmMerchandizeDetails = (HashMap) MerchandizeDAO
					.updateMerchandizeStatus(alMerchandizeInfo, oLoginVO);

			System.out.print(hmMerchandizeDetails);
			ResourceBundle oBundle = ResourceBundle
					.getBundle("com.sbh.properties.resources.ApplicationResources");
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			oBundle.getString("email.skybuy.logoPath");
			sSkyBuyLogoPath = sSkyBuyLogoPath == null ? "" : sSkyBuyLogoPath
					.trim();

			/*
			 * sEmailCaptionForAcceptance=oBundle.getString("merchandize.acceptance");
			 * sEmailCaptionForAcceptance=sEmailCaptionForAcceptance==null?"":sEmailCaptionForAcceptance.trim();
			 * 
			 * sEmailCaptionForRejection=oBundle.getString("merchandize.rejection");
			 * sEmailCaptionForRejection=sEmailCaptionForRejection==null?"":sEmailCaptionForRejection.trim();
			 */

			sSkyBuyAddr1 = oBundle.getString("skybuy.address1");
			sSkyBuyAddr1 = sSkyBuyAddr1 == null ? "" : sSkyBuyAddr1.trim();
			sSkyBuyAddr2 = oBundle.getString("skybuy.address2");
			sSkyBuyAddr2 = sSkyBuyAddr1 == null ? "" : sSkyBuyAddr2.trim();
			sSkyBuyPh = oBundle.getString("skybuy.phone");
			sSkyBuyPh = sSkyBuyPh == null ? "" : sSkyBuyPh.trim();

			StringTokenizer oStringTokenizer = null;
			HashMap p_hmTags = new HashMap();
			ArrayList<ProductDetailsVO> alHMValue = null;
			if (hmMerchandizeDetails != null && hmMerchandizeDetails.size() > 0) {

				Iterator itr = hmMerchandizeDetails.entrySet().iterator();
				while (itr.hasNext()) {
					Map.Entry oEntry = (Map.Entry) itr.next();
					sHMKey = (String) oEntry.getKey();
					alHMValue = (ArrayList) oEntry.getValue();

					oStringTokenizer = new StringTokenizer(sHMKey, "_");
					while (oStringTokenizer.hasMoreTokens()) {
						sProdStatus = oStringTokenizer.nextToken();
					}
					sProdStatus = sProdStatus == null ? "" : sProdStatus.trim();

					// Header
					p_hmTags.put("{{Logo}}", sSkyBuyLogoPath);
					p_hmTags.put("{{Date}}", MerchandizeDAO
							.dateFormat(new Date()));
					// SkyBuy Address
					p_hmTags.put("{{Address1}}", sSkyBuyAddr1);
					p_hmTags.put("{{Address2}}", sSkyBuyAddr2);
					p_hmTags.put("{{Phone}}", sSkyBuyPh);

					/*
					 * if(sProdStatus.equalsIgnoreCase("A")){
					 * p_hmTags.put("{{Heading}}",sEmailCaptionForAcceptance);
					 * }else{
					 * p_hmTags.put("{{Heading}}",sEmailCaptionForRejection); }
					 */
					if (alHMValue != null && alHMValue.size() > 0) {
						MerchandizeDAO.sendEmail(alHMValue, p_hmTags, null,
								null, sProdStatus, oLoginVO.getUserId(), "",
								alCategory);
					}

				}
			}

			logger.info("MerchandizeAction::approveSpecialProd:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("MerchandizeAction::approveSpecialProd:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	/* View Checkout Product Details */

	public ActionForward viewSpecialProducts(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::viewSpecialProducts:ENTER");
		String sFrdKey = "viewSpecialProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null, sCateFolderName = null, sURI = null, sPath = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		List<CommentsVO> alProdComments = null;
		try {

			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			if (oProductDetailsVO != null) {
				oProductDetailsVO.setSbhComment("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);

				/*
				 * oProductDetailsForm.setProdCode(oProductDetailsVO.getProdCode());
				 * oProductDetailsForm.setProdTitle(oProductDetailsVO.getProdTitle());
				 * oProductDetailsForm.setInShopStatus(oProductDetailsVO.getInShopStatus());
				 */

				if (oProductDetailsVO.getSpecialProductPath() != null
						&& oProductDetailsVO.getSpecialProductPath().trim()
								.length() > 0)
					oProductDetailsVO.setSpecialProductPath(sImageViewPath
							+ oProductDetailsVO.getSpecialProductPath() + "/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "."
							+ oProductDetailsVO.getSpecialProdType());
				else
					oProductDetailsVO.setSpecialProductPath("");
				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSpecialProductPath());

				oSession.setAttribute("SpecialProductDetails",
						oProductDetailsVO);
				request.setAttribute("mode", "editSpecialProduct");

				alProdComments = MerchandizeDAO.getProductComments(
						oProductDetailsVO.getOwnerId(), oProductDetailsVO
								.getOwnerType(), oProductDetailsVO.getProdId(),
						oLoginVO.getUserId());

				if (alProdComments != null && alProdComments.size() > 0) {
					request.setAttribute("productComments", alProdComments);
				}
			}
			logger.info("CatalogueAction::viewSpecialProducts:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::viewSpecialProducts:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	/* View Vendor Product Details */
	public ActionForward viewVendorProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::viewVendorProductDetails:ENTER");
		String sFrdKey = "viewProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null, sCateFolderName = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		List<CommentsVO> alProdComments = null;
		try {
			String sProdId = request.getParameter("ProdId");
			oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			// sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			if (alCategory != null && alCategory.size() > 0) {
				for (CategoryVO oCategoryVO : alCategory) {
					if (oCategoryVO.getCateId() != null) {
						if (oCategoryVO.getCateId().equals(
								oProductDetailsVO.getCateId()))
							sCateFolderName = oCategoryVO.getCateName();
					}
				}
			}

			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");
				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");

				oProductDetailsVO.setSbhComment("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);
				oSession
						.setAttribute("vendorProductDetails", oProductDetailsVO);
				request.setAttribute("mode", "editVendorProduct");

				alProdComments = MerchandizeDAO.getProductComments(
						oProductDetailsForm.getOwnerId(), oProductDetailsVO
								.getOwnerType(), oProductDetailsForm
								.getProdId(), oLoginVO.getUserId());

				if (alProdComments != null && alProdComments.size() > 0) {
					request.setAttribute("productComments", alProdComments);
				}

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), oProductDetailsForm.getOwnerId(),
						oLoginVO.getUserId(), sServerPath, oProductDetailsVO
								.getProdId(), oProductDetailsVO.getCateId(),
						"PRODCATALOGUE", "", oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

			}
			logger.info("CatalogueAction::viewVendorProductDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::viewVendorProductDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	/* View Airline Product Details */

	public ActionForward viewAirlineProductDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::viewAirlineProductDetails:ENTER");
		String sFrdKey = "viewProductSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null, sCateFolderName = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		List<CommentsVO> alProdComments = null;
		try {
			oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");
				if (oProductDetailsVO.getView1ImgType() != null
						&& oProductDetailsVO.getView1ImgType().trim().length() > 0)
					oProductDetailsVO.setView1ImgPath(sImageViewPath
							+ oProductDetailsVO.getView1ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView1ImgType());
				else
					oProductDetailsVO.setView1ImgPath("");
				if (oProductDetailsVO.getView2ImgType() != null
						&& oProductDetailsVO.getView2ImgType().trim().length() > 0)
					oProductDetailsVO.setView2ImgPath(sImageViewPath
							+ oProductDetailsVO.getView2ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView2ImgType());
				else
					oProductDetailsVO.setView2ImgPath("");
				if (oProductDetailsVO.getView3ImgType() != null
						&& oProductDetailsVO.getView3ImgType().trim().length() > 0)
					oProductDetailsVO.setView3ImgPath(sImageViewPath
							+ oProductDetailsVO.getView3ImgPath()
							+ "/PrivateJetJaunts/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getView3ImgType());
				else
					oProductDetailsVO.setView3ImgPath("");
				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);
				oSession.setAttribute("airlineProductDetails",
						oProductDetailsVO);
				request.setAttribute("mode", "editAirlineProduct");

				alProdComments = MerchandizeDAO.getProductComments(
						oProductDetailsForm.getOwnerId(), oProductDetailsVO
								.getOwnerType(), oProductDetailsForm
								.getProdId(), oLoginVO.getUserId());

				if (alProdComments != null && alProdComments.size() > 0) {
					request.setAttribute("productComments", alProdComments);
				}

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), oProductDetailsVO.getOwnerId(),
						oLoginVO.getUserId(), sServerPath, oProductDetailsVO
								.getProdId(), oProductDetailsVO.getCateId(),
						"PRODCATALOGUE", "", oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

			}
			logger.info("CatalogueAction::viewAirlineProductDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger
					.error("CatalogueAction::viewAirlineProductDetails:EXCEPTION "
							+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

	// To View Airline Advertisement

	public ActionForward viewAdvertisementDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("CatalogueAction::viewAdvertisementDetails:ENTER");
		String sFrdKey = "viewAdvertisementSuccess";
		ProductDetailsVO oProductDetailsVO = null;
		ArrayList<CategoryVO> alCategory = null;
		String sImageViewPath = null, sCateFolderName = null;
		HttpSession oSession = request.getSession();
		ProductDetailsForm oProductDetailsForm = (ProductDetailsForm) form;
		LoginVO oLoginVO = null;
		String sURI = null, sPath = null;
		try {
			sURI = request.getRequestURI();
			sPath = Utils.getUserTypeFromURI(sURI);
			sPath = sPath == null ? "" : sPath.trim();

			if (sPath.equalsIgnoreCase(VENDOR))
				oLoginVO = (LoginVO) oSession.getAttribute("vendorLoginInfo");
			else if (sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO = (LoginVO) oSession.getAttribute("airlineLoginInfo");
			else if (sPath.equalsIgnoreCase(ADMIN))
				oLoginVO = (LoginVO) oSession.getAttribute("adminLoginInfo");
			String sProdId = request.getParameter("ProdId");
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			// sImagePath = getServlet().getServletContext().getRealPath("");
			alCategory = (ArrayList) oSession.getServletContext().getAttribute(
					"Category");
			oProductDetailsVO = CatalogueDAO.getProductDetails(sProdId,
					oLoginVO.getUserType());

			if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(VENDOR)) {
				if (alCategory != null && alCategory.size() > 0) {
					for (CategoryVO oCategoryVO : alCategory) {
						if (oCategoryVO.getCateId() != null) {
							if (oCategoryVO.getCateId().equals(
									oProductDetailsVO.getCateId()))
								sCateFolderName = oCategoryVO.getCateName();
						}
					}
				}
			} else if (oProductDetailsVO.getOwnerType().equalsIgnoreCase(
					AIRLINE)) {
				sCateFolderName = "PrivateJetJaunts";
			}
			sImageViewPath = sImageViewPath == null ? "" : sImageViewPath
					.trim();
			if (oProductDetailsVO != null) {
				if (oProductDetailsVO.getMainImgPath() != null
						&& oProductDetailsVO.getMainImgPath().trim().length() > 0)
					oProductDetailsVO.setMainImgPath(sImageViewPath
							+ oProductDetailsVO.getMainImgPath() + "/"
							+ sCateFolderName + "/Thumb/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "thumb."
							+ oProductDetailsVO.getMainImgType());
				else
					oProductDetailsVO.setMainImgPath("");

				if (oProductDetailsVO.getSwfPath() != null
						&& oProductDetailsVO.getSwfPath().trim().length() > 0)
					oProductDetailsVO.setSwfPath(sImageViewPath
							+ oProductDetailsVO.getSwfPath() + "/"
							+ oProductDetailsVO.getOwnerId()
							+ oProductDetailsVO.getProdId() + "."
							+ oProductDetailsVO.getSwfType());
				else
					oProductDetailsVO.setSwfPath("");
				oSession.setAttribute("SwfFileName", oProductDetailsVO
						.getSwfPath());

				BeanUtils
						.copyProperties(oProductDetailsForm, oProductDetailsVO);
				oSession.setAttribute("airlineAdvertisementDetails",
						oProductDetailsVO);

				request.setAttribute("AdvertisementProductDetails",
						oProductDetailsVO);
				List<CommentsVO> alProdComments = MerchandizeDAO
						.getProductComments(oProductDetailsVO.getOwnerId(),
								oProductDetailsVO.getOwnerType(),
								oProductDetailsVO.getProdId(), oLoginVO
										.getUserId());

				if (alProdComments != null && alProdComments.size() > 0) {
					request.setAttribute("productComments", alProdComments);
				}

				String sDestPath = System.getProperty("GenerateXmlPath").trim();
				String sE_CatalogueFileName = System.getProperty(
						"e-CatalogueFileName").trim();
				String sServerPath = getServlet().getServletContext()
						.getRealPath("");

				GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
				oGenerateCatalogue.generateCatalogueXML(sDestPath, oLoginVO
						.getUserType(), oProductDetailsVO.getOwnerId(),
						oLoginVO.getUserId(), sServerPath, oProductDetailsVO
								.getProdId(), oProductDetailsVO.getCateId(),
						"PRODCATALOGUE", "", oProductDetailsVO.getSeqId());
				String sProdPreviewXmlPath = System.getProperty(
						"ProdPreviewXMLPath").trim();
				String sProdPreviewSwfPath = System.getProperty(
						"ProdPreviewSWFPath").trim();
				Map<String, String> mUpdatedSwfIds = UploadSkyBuyCatalogueDAO
						.getLastUpdatedCatalogueId();

				oSession.setAttribute("eCataloguePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("CataloguePath"));
				oSession.setAttribute("welcomePagePath", sProdPreviewSwfPath
						+ mUpdatedSwfIds.get("WelcomepagePath"));
				oSession.setAttribute("eCatalogueProdFileName",
						sE_CatalogueFileName + "_Prod_" + oLoginVO.getUserId()
								+ ".xml");

				request.setAttribute("mode", "editAirlineProduct");
			}
			logger.info("CatalogueAction::viewAdvertisementDetails:EXIT");
		} catch (Exception e) {
			sFrdKey = ("failure");
			e.printStackTrace();
			logger.error("CatalogueAction::viewAdvertisementDetails:EXCEPTION "
					+ e.getMessage());
		}
		return mapping.findForward(sFrdKey);
	}

}