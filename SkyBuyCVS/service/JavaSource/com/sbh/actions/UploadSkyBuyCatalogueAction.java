/**
 * 
 */
package com.sbh.actions;

import static com.sbh.contants.SkyBuyContants.ADMIN;
import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_ADMIN;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_AUTOUDPATE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_CLIENTCERT;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SCHEMA;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SERVICE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SHOPPINGCART;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_USERNAME;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_VIRTUALDESKTOP;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_IPHONEDESKTOPIMAGE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SKYBUYCATALOGUE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_IPHONEPERSONAL;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_WELCOMEPAGE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_IPHONEWELCOMEPAGE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_PERSONALSHOPPER;

import static com.sbh.dao.FileUpload.uploadSkyBuyCatalogue;
import static com.sbh.dao.FileUpload.uploadSkyBuyInstallationPackage;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.addSkyBuyCatalogueDetails;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.addSkyBuyInstallationUploadedDate;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.getInstallationPackageInfo;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.getSearchInstallationPackageInfo;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.getSearchSkyBuyCatalogueDetails;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.getSkyBuyCatalogueInfo;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateClientCertAccountInfo;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateInstallationPackageDetails;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateInstallationPackageStatus;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateSkyBuyCatalogueUploadedDate;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.updateSkyBuyInstallationUploadedDate;
import static com.sbh.dao.UploadSkyBuyCatalogueDAO.validateVersion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.sbh.dao.OrderDAO;
import com.sbh.forms.SearchInstallationPackageForm;
import com.sbh.forms.UploadSkyBuyCatalogueForm;
import com.sbh.util.Utils;
import com.sbh.vo.KeyVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.SkyBuyCatalogueUploadVO;
import com.sbh.vo.UploadInstallationPackageVO;

/**
 * @author nbvk
 *
 */
public class UploadSkyBuyCatalogueAction extends DispatchAction {
	
	private static Logger logger = LogManager.getLogger(UploadSkyBuyCatalogueAction.class);
	
	public ActionForward initUploadSkybuyCatalogue(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		logger.info("UploadSkyBuyCatalogueAction::initUploadSkybuyCatalogue::ENTRY");
		
		String sForwardKey = "initUploadCatalogueSuccess";
		HttpSession oSession = request.getSession();
		
		oSession.removeAttribute("uploadSkyBuyCatalogueForm");
		
		logger.info("UploadSkyBuyCatalogueAction::initUploadSkybuyCatalogue::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward uploadSkybuyCatalogue(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::uploadSkybuyCatalogue::ENTRY");
		
		String sForwardKey = "uploadCatalogueSuccess";
		boolean bIsUploaded = false;
		String /*sWelcomePageFileName = null,*/sSkyBuyCatgFileName = null;
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		HttpSession oSession = request.getSession();
		String sEcatalogueUploadPath = null;	
		String sDemoCatalogueFolderPath = null;
		String sECatalogueFilePath = null;
		try {
			
			sEcatalogueUploadPath = System.getProperty("ECatalogueUploadPath").trim();	
			sDemoCatalogueFolderPath = System.getProperty("DemoCatalogueXMLPath").trim();
			sECatalogueFilePath = System.getProperty("SkyBuyCataloguePath").trim();
			
			LoginVO oLoginVO = null;
			String sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");	
			
			if("WelcomePage".equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
				sSkyBuyCatgFileName = System.getProperty("welcomePageFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			}else if("ECatalogue".equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
				sSkyBuyCatgFileName = System.getProperty("skybuyCatgFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			}else {
				sSkyBuyCatgFileName = System.getProperty("operatorCheckListFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			}
			
			FormFile oCatalogueFormFile = oUploadSkyBuyCatalogueForm.getCataloguePath();
			long lFolderId = addSkyBuyCatalogueDetails(oLoginVO.getUserId());
			if(oCatalogueFormFile!= null && !oCatalogueFormFile.getFileName().equals("")){
				bIsUploaded = uploadSkyBuyCatalogue(oCatalogueFormFile, sEcatalogueUploadPath, lFolderId+"",  sSkyBuyCatgFileName, sDemoCatalogueFolderPath);
				if(bIsUploaded){
					request.setAttribute("CatalogueUploaded", "Y");
				}else {
					request.setAttribute("CatalogueUploaded", "N");
				}
			}else {
				request.setAttribute("CatalogueUploaded", "NU");
			}
			request.setAttribute("UploadType", oUploadSkyBuyCatalogueForm.getUploadType());
			/*if(oWelcomePageFormFile != null && !"".equals(oWelcomePageFormFile.getFileName())) {
				bIsUploaded = uploadWelcomePage(oWelcomePageFormFile, sEcatalogueUploadPath, lFolderId+"", sWelcomePageFileName);
				if(bIsUploaded){
					request.setAttribute("WelcomepageUploaded", "Y");
				}else {
					request.setAttribute("WelcomepageUploaded", "N");
				}
			}else {
				request.setAttribute("WelcomepageUploaded", "NU");
			}*/
			updateSkyBuyCatalogueUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), oUploadSkyBuyCatalogueForm.getReasonForUpload(), lFolderId+"",oLoginVO.getUserId());
			//updateSkyBuyCatalogueUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), oUploadSkyBuyCatalogueForm.getReasonForUpload(), lFolderId+"",oLoginVO.getUserId(), sECatalogueFilePath, sSkyBuyCatgFileName);
		}catch(Exception e) {
			logger.debug("UploadSkyBuyCatalogueAction::uploadSkybuyCatalogue::Exception"+e.getMessage());
			sForwardKey = "failure";
		}
		
		logger.info("UploadSkyBuyCatalogueAction::uploadSkybuyCatalogue::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward initSearchSkyBuyCatalogueDetails(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::initSearchInstallationPackage::ENTRY");
		
		String sForwardKey = "initSearchInstallationPackageSuccess";
		HttpSession oSession = request.getSession();
		
		oSession.removeAttribute("searchInstallationPackageForm");
		
		logger.info("UploadSkyBuyCatalogueAction::initSearchInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward searchSkyBuyCatalogueDetails(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::ENTRY");
		
		String sForwardKey = "searchSkyBuyCatalogueSuccess";
		List<SkyBuyCatalogueUploadVO> alSkyBuyCatalogueUploadVO = null;
		try {
			alSkyBuyCatalogueUploadVO = getSearchSkyBuyCatalogueDetails();
			
			if(alSkyBuyCatalogueUploadVO != null && alSkyBuyCatalogueUploadVO.size() > 0) {
				request.setAttribute("UploadDetails", alSkyBuyCatalogueUploadVO);
			}else {
				request.setAttribute("NoRecords", "No Records");
			}
		}catch(Exception e) {
			logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::EXECEPTION"+e.getMessage());
			sForwardKey = "failure";
		}
		logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward editSkyBuyCatalogueDetails(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::editSkyBuyCatalogueDetails::ENTRY");
		
		String sForwardKey = "editSkyBuyCatalogueSuccess";
		String sPackageId = request.getParameter("PackageId");
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		try {
			SkyBuyCatalogueUploadVO oSkyBuyCatalogueUploadVO = getSkyBuyCatalogueInfo(sPackageId);
			BeanUtils.copyProperties(oUploadSkyBuyCatalogueForm, oSkyBuyCatalogueUploadVO);
			request.setAttribute("PackageId", sPackageId);
			request.setAttribute("Mode", "EDIT");
		}catch(Exception e) {
			logger.info("UploadSkyBuyCatalogueAction::editSkyBuyCatalogueDetails::EXECEPTION"+e.getMessage());
			sForwardKey = "failure";
		}
		logger.info("UploadSkyBuyCatalogueAction::editSkyBuyCatalogueDetails::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	
	public ActionForward updateSkybuyCatalogue(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::updateSkybuyCatalogue::ENTRY");
		
		String sForwardKey = "updateSkyBuyCatalogueSuccess";
		String sCatalogueUpload = "N";
//		String sWelcomePageUpload = "N";
		String sPackageId = request.getParameter("PackageId");
		boolean bIsUploaded = false;
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		String /*sWelcomePageFileName = null,*/sSkyBuyCatgFileName = null;
		String sEcatalogueUploadPath = null;	
		String sDemoCatalogueFolderPath = null;
		String sECatalogueFilePath = null;
		try {
			sEcatalogueUploadPath = System.getProperty("ECatalogueUploadPath").trim();	
			sDemoCatalogueFolderPath = System.getProperty("DemoCatalogueXMLPath").trim();	
			sECatalogueFilePath = System.getProperty("SkyBuyCataloguePath").trim();
			
			LoginVO oLoginVO = null;
			String sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			HttpSession oSession = request.getSession(true);
			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			if("WelcomePage".equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
				sSkyBuyCatgFileName = System.getProperty("welcomePageFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			}else {
				sSkyBuyCatgFileName = System.getProperty("skybuyCatgFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			}

			FormFile oCatalogueFormFile = oUploadSkyBuyCatalogueForm.getCataloguePath();
			if(sPackageId != null && sPackageId.trim().length() > 0) {
				if(oCatalogueFormFile!= null && !oCatalogueFormFile.getFileName().equals("")){
					bIsUploaded = uploadSkyBuyCatalogue(oCatalogueFormFile, sEcatalogueUploadPath, sPackageId, sSkyBuyCatgFileName, sDemoCatalogueFolderPath);
					if(bIsUploaded){
						request.setAttribute("CatalogueUploaded", "Y");
						sCatalogueUpload = "Y";
					}else {
						request.setAttribute("CatalogueUploaded", "N");
					}
				}else {
					request.setAttribute("CatalogueUploaded", "NU");
				}
				request.setAttribute("UploadType", oUploadSkyBuyCatalogueForm.getUploadType());
				if("Y".equalsIgnoreCase(sCatalogueUpload)) {
					updateSkyBuyCatalogueUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(),oUploadSkyBuyCatalogueForm.getReasonForUpload(), sPackageId,oLoginVO.getUserId());
					//updateSkyBuyCatalogueUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(),oUploadSkyBuyCatalogueForm.getReasonForUpload(), sPackageId,oLoginVO.getUserId(),sECatalogueFilePath,sSkyBuyCatgFileName);
				}
			}
		}catch(Exception e) {
			logger.debug("UploadSkyBuyCatalogueAction::updateSkybuyCatalogue::Exception"+e.getMessage());
			sForwardKey = "failure";
		}
		
		logger.info("UploadSkyBuyCatalogueAction::updateSkybuyCatalogue::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward initUploadInstallationPackage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		logger.info("UploadSkyBuyCatalogueAction::initUploadInstallationPackage::ENTRY");
		
		String sForwardKey = "initUploadInstallationPackageSuccess";
		HttpSession oSession = request.getSession();
		
		oSession.removeAttribute("uploadSkyBuyCatalogueForm");
		
		logger.info("UploadSkyBuyCatalogueAction::initUploadInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	/*public ActionForward uploadInstallationPackage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::uploadInstallationPackage::ENTRY");
		
		String sForwardKey = "uploadInstallationPackageSuccess";
		String sFileName = null;
		String sFolderName = null;
		String sSrcImgPath = "";
		String sURI=null;
		String sPath=null;
		String sImageUploadPath = null;
		String sInstallationPackageFilePath = null;
		String sUploadType = null;
		Boolean bIsUploaded = false;
		boolean bIsUpdatedVersion = false;
		KeyVO oEncryptedName = null;
		KeyVO oEncryptedPasscode = null;
		HttpSession oSession = request.getSession();
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		List alImageInfo = new ArrayList();
		
		try {
			sImageUploadPath = System.getProperty("UploadInstallationPath").trim();
			sInstallationPackageFilePath = System.getProperty("InstallationPackageFilePath").trim();
			FormFile oInstallationFormFile = oUploadSkyBuyCatalogueForm.getInstallationPath();
			sSrcImgPath = sImageUploadPath+"/"+sInstallationPackageFilePath+"/";
			sFileName = oInstallationFormFile.getFileName();
			
			LoginVO oLoginVO = null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			sUploadType = oUploadSkyBuyCatalogueForm.getUploadType();
			if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SCHEMA);
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_CLIENTCERT);
			}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SERVICE);
			}else if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_VIRTUALDESKTOP);
			}else if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_ADMIN);
			}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SHOPPINGCART);
			}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_AUTOUDPATE);
			}else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(sUploadType)) {
				sFolderName =  UPLOADTYPE_IPHONEDESKTOPIMAGE;
			}
			if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUploadType) || UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUploadType) 
					|| UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUploadType) || UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUploadType)
					|| UPLOADTYPE_SERVICE.equalsIgnoreCase(sUploadType) || UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUploadType)) {
				bIsUpdatedVersion = validateVersion(sUploadType, oUploadSkyBuyCatalogueForm.getVersion());
				if(bIsUpdatedVersion) {
					long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
					if(lFolderId > 0) {
						if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
							alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sImageUploadPath,sSrcImgPath,sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(), sFolderName);
							bIsUploaded = (Boolean)alImageInfo.get(0);
							sFileName = (String)alImageInfo.get(1);
							if(bIsUploaded != null && bIsUploaded.booleanValue()){
								request.setAttribute("InstallationUploaded", "Y");
							}else {
								request.setAttribute("InstallationUploaded", "N");
							}
						}
						updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId());
						//updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId(),"/"+sInstallationPackageFilePath+"/"+sFolderName+"/");
					}else {
						request.setAttribute("ProcessStatus", "Upload is in progress");
						sForwardKey = "uploadInstallationPackageFailure";
					}
				}else {
					String errorMsg = "";
					if(oUploadSkyBuyCatalogueForm != null) {
						if(UPLOADTYPE_ADMIN.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous admin version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous auto update version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous catalogue version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous virtual desktop version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous schema version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous service version";
							sForwardKey = "uploadInstallationPackageFailure";
						}
					}
						
					request.setAttribute("errorMsg", errorMsg);
				}
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
						alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sImageUploadPath,sSrcImgPath,sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),sFolderName);
						bIsUploaded = (Boolean)alImageInfo.get(0);
						sFileName = (String)alImageInfo.get(1);
						if(bIsUploaded != null && bIsUploaded.booleanValue()){
							request.setAttribute("InstallationUploaded", "Y");
						}else {
							request.setAttribute("InstallationUploaded", "N");
						}
					}
					oEncryptedName = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getUserName(),"SERVER","");
					oEncryptedPasscode = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getPassword(),"SERVER","");
					
					updateClientCertAccountInfo(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, oEncryptedName.getEncryptedData(), oEncryptedPasscode.getEncryptedData(), oEncryptedPasscode.getRefId(), lFolderId+"",oLoginVO.getUserId());
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else if(UPLOADTYPE_USERNAME.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					
					oEncryptedName = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getUserName(),"SERVER","");
					oEncryptedPasscode = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getPassword(),"SERVER","");
					updateClientCertAccountInfo(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName,  oEncryptedName.getEncryptedData(), oEncryptedPasscode.getEncryptedData(), oEncryptedPasscode.getRefId(), lFolderId+"",oLoginVO.getUserId());
					request.setAttribute("InstallationUploaded", "Y");
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
						alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sImageUploadPath,sSrcImgPath,sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,lFolderId+"",sFolderName);
						bIsUploaded = (Boolean)alImageInfo.get(0);
						sFileName = (String)alImageInfo.get(1);
						if(bIsUploaded != null && bIsUploaded.booleanValue()){
							request.setAttribute("InstallationUploaded", "Y");
						}else {
							request.setAttribute("InstallationUploaded", "N");
						}
					}
					updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId());
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else {
				request.setAttribute("ProcessStatus", "Invalid Upload.");
				sForwardKey = "uploadInstallationPackageFailure";
			}
		}catch(Exception e) {
			logger.debug("UploadSkyBuyCatalogueAction::uploadInstallationPackage::Exception"+e.getMessage());
			sForwardKey = "failure";
		}
		
		request.setAttribute("UploadType", sUploadType);
		
		logger.info("UploadSkyBuyCatalogueAction::uploadInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}*/
	
	

	public ActionForward uploadInstallationPackage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::uploadInstallationPackage::ENTRY");
		
		String sForwardKey = "uploadInstallationPackageSuccess";
		String sFileName = null;
		String sFolderName = null;
		String sURI=null;
		String sPath=null;
		String sUploadType = null;
		Boolean bIsUploaded = false;
		boolean bIsUpdatedVersion = false;
		KeyVO oEncryptedName = null;
		KeyVO oEncryptedPasscode = null;
		HttpSession oSession = request.getSession();
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		List alImageInfo = new ArrayList();
		
		try {
			
			
			FormFile oInstallationFormFile = oUploadSkyBuyCatalogueForm.getInstallationPath();
			sFileName = oInstallationFormFile.getFileName();
			
			LoginVO oLoginVO = null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			
			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");
			
			sUploadType = oUploadSkyBuyCatalogueForm.getUploadType();
			if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SCHEMA);
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_CLIENTCERT);
			}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SERVICE);
			}else if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_VIRTUALDESKTOP);
			}/*else if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_ADMIN);
			}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_SHOPPINGCART);
			}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUploadType)) {
				sFolderName = System.getProperty(UPLOADTYPE_AUTOUDPATE);
			}*/else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(sUploadType)) {
				//sFolderName =  UPLOADTYPE_IPHONEDESKTOPIMAGE;
				sFolderName =System.getProperty(UPLOADTYPE_IPHONEDESKTOPIMAGE);
			}else if(UPLOADTYPE_IPHONEPERSONAL.equalsIgnoreCase(sUploadType)) {
				//sFolderName =  UPLOADTYPE_IPHONEPERSONAL;
				sFolderName =System.getProperty(UPLOADTYPE_IPHONEPERSONAL);
			}else if(UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(sUploadType)) {
				//sFolderName =  UPLOADTYPE_SKYBUYCATALOGUE;
				sFolderName =System.getProperty(UPLOADTYPE_SKYBUYCATALOGUE);
			}else if(UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(sUploadType)) {
				//sFolderName =  UPLOADTYPE_SKYBUYCATALOGUE;
				sFolderName =System.getProperty(UPLOADTYPE_WELCOMEPAGE);
				
			} 
			
			else if(UPLOADTYPE_IPHONEWELCOMEPAGE.equalsIgnoreCase(sUploadType)){
				sFolderName =System.getProperty(UPLOADTYPE_IPHONEWELCOMEPAGE);
			}
			else if(UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(sUploadType)) {
				//sFolderName =  UPLOADTYPE_SKYBUYCATALOGUE;
				sFolderName =System.getProperty(UPLOADTYPE_PERSONALSHOPPER);
				
			}
			
			
			
			if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUploadType) || UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUploadType) 
					|| UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUploadType) || UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUploadType)
					|| UPLOADTYPE_SERVICE.equalsIgnoreCase(sUploadType) || UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUploadType)) {
				bIsUpdatedVersion = validateVersion(sUploadType, oUploadSkyBuyCatalogueForm.getVersion());
				if(bIsUpdatedVersion) {
					long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
					if(lFolderId > 0) {
						if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
							alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(), sFolderName);
							bIsUploaded = (Boolean)alImageInfo.get(0);
							sFileName = (String)alImageInfo.get(1);
							if(bIsUploaded != null && bIsUploaded.booleanValue()){
								request.setAttribute("InstallationUploaded", "Y");
							}else {
								request.setAttribute("InstallationUploaded", "N");
							}
						}
						updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId());
						//updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId(),"/"+sInstallationPackageFilePath+"/"+sFolderName+"/");
					}else {
						request.setAttribute("ProcessStatus", "Upload is in progress");
						sForwardKey = "uploadInstallationPackageFailure";
					}
				}else {
					String errorMsg = "";
					if(oUploadSkyBuyCatalogueForm != null) {
						/*if(UPLOADTYPE_ADMIN.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous admin version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous auto update version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous catalogue version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else*/ 
						if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous virtual desktop version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous schema version";
							sForwardKey = "uploadInstallationPackageFailure";
						}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
							errorMsg = "You entered a previous service version";
							sForwardKey = "uploadInstallationPackageFailure";
						}
					}
						
					request.setAttribute("errorMsg", errorMsg);
				}
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
						alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),sFolderName);
						bIsUploaded = (Boolean)alImageInfo.get(0);
						sFileName = (String)alImageInfo.get(1);
						if(bIsUploaded != null && bIsUploaded.booleanValue()){
							request.setAttribute("InstallationUploaded", "Y");
						}else {
							request.setAttribute("InstallationUploaded", "N");
						}
					}
					oEncryptedName = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getUserName(),"SERVER","");
					oEncryptedPasscode = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getPassword(),"SERVER","");
					
					updateClientCertAccountInfo(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, oEncryptedName.getEncryptedData(), oEncryptedPasscode.getEncryptedData(), oEncryptedPasscode.getRefId(), lFolderId+"",oLoginVO.getUserId());
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else if(UPLOADTYPE_USERNAME.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					
					oEncryptedName = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getUserName(),"SERVER","");
					oEncryptedPasscode = OrderDAO.encryptData(oUploadSkyBuyCatalogueForm.getPassword(),"SERVER","");
					updateClientCertAccountInfo(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName,  oEncryptedName.getEncryptedData(), oEncryptedPasscode.getEncryptedData(), oEncryptedPasscode.getRefId(), lFolderId+"",oLoginVO.getUserId());
					request.setAttribute("InstallationUploaded", "Y");
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(sUploadType)|| UPLOADTYPE_IPHONEPERSONAL.equalsIgnoreCase(sUploadType)||UPLOADTYPE_IPHONEWELCOMEPAGE.equalsIgnoreCase(sUploadType)) {
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				if(lFolderId > 0) {
					if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
						alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,lFolderId+"",sFolderName);
						bIsUploaded = (Boolean)alImageInfo.get(0);
						sFileName = (String)alImageInfo.get(1);
						if(bIsUploaded != null && bIsUploaded.booleanValue()){
							request.setAttribute("InstallationUploaded", "Y");
						}else {
							request.setAttribute("InstallationUploaded", "N");
						}
					}
					updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId());
				}else {
					request.setAttribute("ProcessStatus", "Upload is in progress");
					sForwardKey = "uploadInstallationPackageFailure";
				}
			}else if(UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(sUploadType)||UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(sUploadType)||UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(sUploadType)){
				String sEcatalogueUploadPath = System.getProperty("ECatalogueUploadPath").trim();	
				String sDemoCatalogueFolderPath = System.getProperty("DemoCatalogueXMLPath").trim();
				String	sECatalogueFilePath = System.getProperty("SkyBuyCataloguePath").trim();
				String sSkyBuyCatgFileName = System.getProperty("skybuyCatgFileName");
				sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
			//	long lFolderId = addSkyBuyCatalogueDetails(oLoginVO.getUserId());
				long lFolderId = addSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(),oLoginVO.getUserId());
				
				if(UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
					sSkyBuyCatgFileName = System.getProperty("welcomePageFileName");
					sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
				}
				if(UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(oUploadSkyBuyCatalogueForm.getUploadType())) {
					sSkyBuyCatgFileName = System.getProperty("personalShopperFileName");
					sSkyBuyCatgFileName = sSkyBuyCatgFileName == null?"":sSkyBuyCatgFileName.trim();
				}
				
				if(oUploadSkyBuyCatalogueForm!= null ){
					alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,lFolderId+"",sFolderName);
					bIsUploaded = (Boolean)alImageInfo.get(0);
					sFileName = (String)alImageInfo.get(1);
					//bIsUploaded = uploadSkyBuyCatalogue(oUploadSkyBuyCatalogueForm.getInstallationPath(), sEcatalogueUploadPath, lFolderId+"",  sSkyBuyCatgFileName, sDemoCatalogueFolderPath);
				if(bIsUploaded){
					request.setAttribute("InstallationUploaded", "Y");
				}else {
					request.setAttribute("InstallationUploaded", "N");
				}
				}else {
					request.setAttribute("InstallationUploaded", "NU");
				}
				
				//updateSkyBuyCatalogueUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), oUploadSkyBuyCatalogueForm.getReasonForUpload(), lFolderId+"",oLoginVO.getUserId());
				updateSkyBuyInstallationUploadedDate(oUploadSkyBuyCatalogueForm.getUploadType(), sFileName, lFolderId+"",oLoginVO.getUserId());
			}
			
			
			
			else {
				request.setAttribute("ProcessStatus", "Invalid Upload.");
				sForwardKey = "uploadInstallationPackageFailure";
			}
		}catch(Exception e) {
			logger.debug("UploadSkyBuyCatalogueAction::uploadInstallationPackage::Exception"+e.getMessage());
			sForwardKey = "failure";
		}
		
		request.setAttribute("UploadType", sUploadType);
		
		logger.info("UploadSkyBuyCatalogueAction::uploadInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	
	
	
	public ActionForward initSearchInstallationPackage(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::initSearchInstallationPackage::ENTRY");
		
		String sForwardKey = "initSearchInstallationPackageSuccess";
		HttpSession oSession = request.getSession();
		
		oSession.removeAttribute("searchInstallationPackageForm");
		
		logger.info("UploadSkyBuyCatalogueAction::initSearchInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward searchInstallationPackage(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::ENTRY");
		
		String sForwardKey = "initSearchInstallationPackageSuccess";
		List<UploadInstallationPackageVO> alUploadInstallationPackageVO = null;
		SearchInstallationPackageForm oSearchInstallationPackageForm = (SearchInstallationPackageForm) form;
		try {
			alUploadInstallationPackageVO = getSearchInstallationPackageInfo(oSearchInstallationPackageForm.getSearchBy(),oSearchInstallationPackageForm.getSearchValue());
			
			if(alUploadInstallationPackageVO != null && alUploadInstallationPackageVO.size() > 0) {
				request.setAttribute("UploadDetails", alUploadInstallationPackageVO);
			}else {
				request.setAttribute("NoRecords", "No Records");
			}
		}catch(Exception e) {
			logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::EXECEPTION"+e.getMessage());
			sForwardKey = "failure";
		}
		logger.info("UploadSkyBuyCatalogueAction::searchInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	public ActionForward editInstallationPackage(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::editInstallationPackage::ENTRY");
		
		String sForwardKey = "editInstallationPackageSuccess";
		String sPackageId = request.getParameter("PackageId");
		UploadInstallationPackageVO oUploadInstallationPackageVO = null;
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		try {
			oUploadInstallationPackageVO = getInstallationPackageInfo(sPackageId);
			
			if(oUploadInstallationPackageVO != null) {
				request.setAttribute("InstallationDetails", oUploadInstallationPackageVO);
				oUploadSkyBuyCatalogueForm.setUploadType(oUploadInstallationPackageVO.getUploadType());
				oUploadSkyBuyCatalogueForm.setVersion(oUploadInstallationPackageVO.getVersion());
			}else {
				request.setAttribute("NoRecords", "No Records");
			}
		}catch(Exception e) {
			logger.info("UploadSkyBuyCatalogueAction::editInstallationPackage::EXECEPTION"+e.getMessage());
			sForwardKey = "failure";
		}
		logger.info("UploadSkyBuyCatalogueAction::editInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}
	
	
	/*
	public ActionForward updateInstallationPackage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		throws Exception {
		logger.info("UploadSkyBuyCatalogueAction::updateInstallationPackage::ENTRY");
		
		Boolean bIsUploaded = false;
		String sForwardKey = "updateInstallationPackageSuccess";
		String sFileName = "";
		String sFolderName = "";
		String sSrcImgPath = "";
		String sUpdatedValue = "";
		String sPackageId = request.getParameter("PackageId");
		UploadInstallationPackageVO oUploadInstallationPackageVO = new UploadInstallationPackageVO();
		List alImageInfo = new ArrayList();
		UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
		
		String sImageUploadPath = System.getProperty("UploadInstallationPath").trim();	
		
		FormFile oInstallationFormFile = oUploadSkyBuyCatalogueForm.getInstallationPath();
		sSrcImgPath = sImageUploadPath+"/InstallationPackage/";
		sFileName = oInstallationFormFile.getFileName();
		try {
			HttpSession oSession = request.getSession(true);
			LoginVO oLoginVO = null;
			String sURI=null,sPath=null;
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();

			if(sPath.equalsIgnoreCase(AIRLINE))
				oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase(ADMIN))
				oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase(VENDOR))
				oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");

			sUpdatedValue = updateInstallationPackageStatus(sPackageId,oLoginVO.getUserId());
			if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_SCHEMA);
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_CLIENTCERT);
			}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_SERVICE);
			}else if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_VIRTUALDESKTOP);
			}else if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_ADMIN);
			}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_SHOPPINGCART);
			}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUpdatedValue)) {
				sFolderName = System.getProperty(UPLOADTYPE_AUTOUDPATE);
			}
			if("Y".equalsIgnoreCase(sUpdatedValue)) {
				if(sPackageId != null && sPackageId.trim().length() > 0) {
					if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
						alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sImageUploadPath,sSrcImgPath,sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(), sFolderName);
						bIsUploaded = (Boolean)alImageInfo.get(0);
						sFileName = (String)alImageInfo.get(1);
						if(bIsUploaded != null && bIsUploaded.booleanValue()){
							request.setAttribute("InstallationUploaded", "Y");
						}else {
							request.setAttribute("InstallationUploaded", "N");
						}
					}
					updateInstallationPackageDetails(sPackageId, sFileName, oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType(),oLoginVO.getUserId());
					request.setAttribute("UploadType", oUploadSkyBuyCatalogueForm.getUploadType());
				} 
			}else {
				String errorMsg = "";
				errorMsg = "Updation is in progress";
				sForwardKey = "updateInstallationPackageFailure";
				request.setAttribute("errorMsg", errorMsg);
				oUploadInstallationPackageVO.setPackageId(sPackageId);
				request.setAttribute("InstallationDetails", oUploadInstallationPackageVO);
			}
		}catch(Exception e) {
			logger.debug("UploadSkyBuyCatalogueAction::updateInstallationPackage::Exception"+e.getMessage());
			sForwardKey = "failure";
		}
		
		logger.info("UploadSkyBuyCatalogueAction::updateInstallationPackage::EXIT");
		return mapping.findForward(sForwardKey);
	}*/
	
	public ActionForward updateInstallationPackage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
	throws Exception {
	logger.info("UploadSkyBuyCatalogueAction::updateInstallationPackage::ENTRY");
	
	Boolean bIsUploaded = false;
	String sForwardKey = "updateInstallationPackageSuccess";
	String sFileName = "";
	String sFolderName = "";
	String sSrcImgPath = "";
	String sUpdatedValue = "";
	String sPackageId = request.getParameter("PackageId");
	UploadInstallationPackageVO oUploadInstallationPackageVO = new UploadInstallationPackageVO();
	List alImageInfo = new ArrayList();
	UploadSkyBuyCatalogueForm oUploadSkyBuyCatalogueForm = (UploadSkyBuyCatalogueForm) form;
	
	String sImageUploadPath = System.getProperty("UploadInstallationPath").trim();	
	
	FormFile oInstallationFormFile = oUploadSkyBuyCatalogueForm.getInstallationPath();
	sSrcImgPath = sImageUploadPath+"/InstallationPackage/";
	sFileName = oInstallationFormFile.getFileName();
	try {
		HttpSession oSession = request.getSession(true);
		LoginVO oLoginVO = null;
		String sURI=null,sPath=null;
		sURI=request.getRequestURI();
		sPath=Utils.getUserTypeFromURI(sURI);
		sPath=sPath==null?"":sPath.trim();

		if(sPath.equalsIgnoreCase(AIRLINE))
			oLoginVO=(LoginVO)oSession.getAttribute("airlineLoginInfo");
		else if(sPath.equalsIgnoreCase(ADMIN))
			oLoginVO=(LoginVO)oSession.getAttribute("adminLoginInfo");
		else if(sPath.equalsIgnoreCase(VENDOR))
			oLoginVO=(LoginVO)oSession.getAttribute("vendorLoginInfo");

		sUpdatedValue = updateInstallationPackageStatus(sPackageId,oLoginVO.getUserId());
		if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_SCHEMA);
		}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_CLIENTCERT);
		}else if(UPLOADTYPE_SERVICE.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_SERVICE);
		}else if(UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_VIRTUALDESKTOP);
		}else if(UPLOADTYPE_ADMIN.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_ADMIN);
		}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_SHOPPINGCART);
		}else if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUpdatedValue)) {
			sFolderName = System.getProperty(UPLOADTYPE_AUTOUDPATE);
		}
		if("Y".equalsIgnoreCase(sUpdatedValue)) {
			if(sPackageId != null && sPackageId.trim().length() > 0) {
				if(oInstallationFormFile!= null && !oInstallationFormFile.getFileName().equals("")){
					alImageInfo = uploadSkyBuyInstallationPackage(oInstallationFormFile, sFileName, oUploadSkyBuyCatalogueForm.getUploadType() ,oUploadSkyBuyCatalogueForm.getVersion(), sFolderName);
					bIsUploaded = (Boolean)alImageInfo.get(0);
					sFileName = (String)alImageInfo.get(1);
					if(bIsUploaded != null && bIsUploaded.booleanValue()){
						request.setAttribute("InstallationUploaded", "Y");
					}else {
						request.setAttribute("InstallationUploaded", "N");
					}
				}
				updateInstallationPackageDetails(sPackageId, sFileName, oUploadSkyBuyCatalogueForm.getReasonForUpload(),oUploadSkyBuyCatalogueForm.getUploadType(),oLoginVO.getUserId());
				request.setAttribute("UploadType", oUploadSkyBuyCatalogueForm.getUploadType());
			} 
		}else {
			String errorMsg = "";
			errorMsg = "Updation is in progress";
			sForwardKey = "updateInstallationPackageFailure";
			request.setAttribute("errorMsg", errorMsg);
			oUploadInstallationPackageVO.setPackageId(sPackageId);
			request.setAttribute("InstallationDetails", oUploadInstallationPackageVO);
		}
	}catch(Exception e) {
		logger.debug("UploadSkyBuyCatalogueAction::updateInstallationPackage::Exception"+e.getMessage());
		sForwardKey = "failure";
	}
	
	logger.info("UploadSkyBuyCatalogueAction::updateInstallationPackage::EXIT");
	return mapping.findForward(sForwardKey);
}


	
	/*eCatalogueFileName
	eCatalogueCoverFileName
	eCataloguePartnerFileName*/
	
	
	
	
	
}
