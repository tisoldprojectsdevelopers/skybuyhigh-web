package com.sbh.vo;


public class UploadInstallationPackageVO {
	
	private String packageId;
	private String uploadType;
	private String version;
	private String reasonForUpload;
	private String filePath;
	private String updatedDate;
	private String rank;
	
	/**
	 * @return the rank
	 */
	public String getRank() {
		return rank;
	}
	/**
	 * @param rank the rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}
	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the uploadType
	 */
	public String getUploadType() {
		return uploadType;
	}
	/**
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	/**
	 * @return the reasonForUpload
	 */
	public String getReasonForUpload() {
		return reasonForUpload;
	}
	/**
	 * @param reasonForUpload the reasonForUpload to set
	 */
	public void setReasonForUpload(String reasonForUpload) {
		this.reasonForUpload = reasonForUpload;
	}
	/**
	 * @return the updatedDate
	 */
	public String getUpdatedDate() {
		return updatedDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
}
