package com.sbh.vo;

public class EmailSchedulerVO {
	
	public String airId;
	public String airCharterName;
	public String contactName;
	public String deviceId;
	public String deviceCode;
	public String deviceType;
	public String deviceSerialNo;
	public String productNo;
	public String deviceModel;
	public String status;
	public String lastUpdatedDate;
	public String airContactEmail;
	public String airContactSecondaryEmail;
	public String lastDownloadDate;
	public String shoppingcartUpdateDate;
	public String adminUpdatedDate;
	public String catalogueUpdatedDate;
	public String swfUpdateDate;
	public String welcomepageUpdateDate;
	public String currentAdminVersion;
	public String currentCatalogueVersion;
	public String latestAdminVersion;
	public String latestCatalogueVersion;
	
	
	/**
	 * @return the lastDownloadDate
	 */
	public String getLastDownloadDate() {
		return lastDownloadDate;
	}
	/**
	 * @param lastDownloadDate the lastDownloadDate to set
	 */
	public void setLastDownloadDate(String lastDownloadDate) {
		this.lastDownloadDate = lastDownloadDate;
	}
	/**
	 * @return the adminUpdatedDate
	 */
	public String getAdminUpdatedDate() {
		return adminUpdatedDate;
	}
	/**
	 * @param adminUpdatedDate the adminUpdatedDate to set
	 */
	public void setAdminUpdatedDate(String adminUpdatedDate) {
		this.adminUpdatedDate = adminUpdatedDate;
	}
	/**
	 * @return the catalogueUpdatedDate
	 */
	public String getCatalogueUpdatedDate() {
		return catalogueUpdatedDate;
	}
	/**
	 * @param catalogueUpdatedDate the catalogueUpdatedDate to set
	 */
	public void setCatalogueUpdatedDate(String catalogueUpdatedDate) {
		this.catalogueUpdatedDate = catalogueUpdatedDate;
	}
	/**
	 * @return the airCharterName
	 */
	public String getAirCharterName() {
		return airCharterName;
	}
	/**
	 * @param airCharterName the airCharterName to set
	 */
	public void setAirCharterName(String airCharterName) {
		this.airCharterName = airCharterName;
	}
	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}
	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * @return the deviceSerialNo
	 */
	public String getDeviceSerialNo() {
		return deviceSerialNo;
	}
	/**
	 * @param deviceSerialNo the deviceSerialNo to set
	 */
	public void setDeviceSerialNo(String deviceSerialNo) {
		this.deviceSerialNo = deviceSerialNo;
	}
	/**
	 * @return the productNo
	 */
	public String getProductNo() {
		return productNo;
	}
	/**
	 * @param productNo the productNo to set
	 */
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the airContactEmail
	 */
	public String getAirContactEmail() {
		return airContactEmail;
	}
	/**
	 * @param airContactEmail the airContactEmail to set
	 */
	public void setAirContactEmail(String airContactEmail) {
		this.airContactEmail = airContactEmail;
	}
	/**
	 * @return the shoppingcartUpdateDate
	 */
	public String getShoppingcartUpdateDate() {
		return shoppingcartUpdateDate;
	}
	/**
	 * @param shoppingcartUpdateDate the shoppingcartUpdateDate to set
	 */
	public void setShoppingcartUpdateDate(String shoppingcartUpdateDate) {
		this.shoppingcartUpdateDate = shoppingcartUpdateDate;
	}
	/**
	 * @return the swfUpdateDate
	 */
	public String getSwfUpdateDate() {
		return swfUpdateDate;
	}
	/**
	 * @param swfUpdateDate the swfUpdateDate to set
	 */
	public void setSwfUpdateDate(String swfUpdateDate) {
		this.swfUpdateDate = swfUpdateDate;
	}
	/**
	 * @return the welcomepageUpdateDate
	 */
	public String getWelcomepageUpdateDate() {
		return welcomepageUpdateDate;
	}
	/**
	 * @param welcomepageUpdateDate the welcomepageUpdateDate to set
	 */
	public void setWelcomepageUpdateDate(String welcomepageUpdateDate) {
		this.welcomepageUpdateDate = welcomepageUpdateDate;
	}
	/**
	 * @return the deviceCode
	 */
	public String getDeviceCode() {
		return deviceCode;
	}
	/**
	 * @param deviceCode the deviceCode to set
	 */
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the airContactSecondaryEmail
	 */
	public String getAirContactSecondaryEmail() {
		return airContactSecondaryEmail;
	}
	/**
	 * @param airContactSecondaryEmail the airContactSecondaryEmail to set
	 */
	public void setAirContactSecondaryEmail(String airContactSecondaryEmail) {
		this.airContactSecondaryEmail = airContactSecondaryEmail;
	}
	/**
	 * @return the currentAdminVersion
	 */
	public String getCurrentAdminVersion() {
		return currentAdminVersion;
	}
	/**
	 * @param currentAdminVersion the currentAdminVersion to set
	 */
	public void setCurrentAdminVersion(String currentAdminVersion) {
		this.currentAdminVersion = currentAdminVersion;
	}
	/**
	 * @return the currentCatalogueVersion
	 */
	public String getCurrentCatalogueVersion() {
		return currentCatalogueVersion;
	}
	/**
	 * @param currentCatalogueVersion the currentCatalogueVersion to set
	 */
	public void setCurrentCatalogueVersion(String currentCatalogueVersion) {
		this.currentCatalogueVersion = currentCatalogueVersion;
	}
	/**
	 * @return the latestAdminVersion
	 */
	public String getLatestAdminVersion() {
		return latestAdminVersion;
	}
	/**
	 * @param latestAdminVersion the latestAdminVersion to set
	 */
	public void setLatestAdminVersion(String latestAdminVersion) {
		this.latestAdminVersion = latestAdminVersion;
	}
	/**
	 * @return the latestCatalogueVersion
	 */
	public String getLatestCatalogueVersion() {
		return latestCatalogueVersion;
	}
	/**
	 * @param latestCatalogueVersion the latestCatalogueVersion to set
	 */
	public void setLatestCatalogueVersion(String latestCatalogueVersion) {
		this.latestCatalogueVersion = latestCatalogueVersion;
	}
}
