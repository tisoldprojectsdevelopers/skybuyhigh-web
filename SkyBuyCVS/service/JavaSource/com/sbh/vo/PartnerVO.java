package com.sbh.vo;

import java.io.Serializable;

public class PartnerVO implements Serializable{

	private String ownerId;
	private String ownerType;
	private String ownerName;
	private String ownerContactName;
	private String ownerPhone;
	private String ownerPhoneExt;
	private String ownerEmail;
	private String ownerFax;
	private String ownerMobile;
	private String ownerAddress1;
	private String ownerAddress2;
	private String ownerCity;
	private String ownerState;
	private String ownerZip;
	private String ownerCountry;
	private String ownerStatus;
	private String ownerPOPct;
	private String ownerChargeType;
	private String ownerPriority;
	private String aboutMePath;
	private String aboutMeType;
	
	public String getOwnerContactName() {
		return ownerContactName;
	}
	public void setOwnerContactName(String ownerContactName) {
		this.ownerContactName = ownerContactName;
	}
	public String getOwnerPhone() {
		return ownerPhone;
	}
	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}
	public String getOwnerPhoneExt() {
		return ownerPhoneExt;
	}
	public void setOwnerPhoneExt(String ownerPhoneExt) {
		this.ownerPhoneExt = ownerPhoneExt;
	}
	public String getOwnerEmail() {
		return ownerEmail;
	}
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	public String getOwnerFax() {
		return ownerFax;
	}
	public void setOwnerFax(String ownerFax) {
		this.ownerFax = ownerFax;
	}
	public String getOwnerMobile() {
		return ownerMobile;
	}
	public void setOwnerMobile(String ownerMobile) {
		this.ownerMobile = ownerMobile;
	}
	public String getOwnerAddress1() {
		return ownerAddress1;
	}
	public void setOwnerAddress1(String ownerAddress1) {
		this.ownerAddress1 = ownerAddress1;
	}
	public String getOwnerAddress2() {
		return ownerAddress2;
	}
	public void setOwnerAddress2(String ownerAddress2) {
		this.ownerAddress2 = ownerAddress2;
	}
	public String getOwnerCity() {
		return ownerCity;
	}
	public void setOwnerCity(String ownerCity) {
		this.ownerCity = ownerCity;
	}
	public String getOwnerState() {
		return ownerState;
	}
	public void setOwnerState(String ownerState) {
		this.ownerState = ownerState;
	}
	public String getOwnerZip() {
		return ownerZip;
	}
	public void setOwnerZip(String ownerZip) {
		this.ownerZip = ownerZip;
	}
	public String getOwnerCountry() {
		return ownerCountry;
	}
	public void setOwnerCountry(String ownerCountry) {
		this.ownerCountry = ownerCountry;
	}
	public String getOwnerStatus() {
		return ownerStatus;
	}
	public void setOwnerStatus(String ownerStatus) {
		this.ownerStatus = ownerStatus;
	}
	public String getOwnerPOPct() {
		return ownerPOPct;
	}
	public void setOwnerPOPct(String ownerPOPct) {
		this.ownerPOPct = ownerPOPct;
	}
	public String getOwnerChargeType() {
		return ownerChargeType;
	}
	public void setOwnerChargeType(String ownerChargeType) {
		this.ownerChargeType = ownerChargeType;
	}
	public String getOwnerPriority() {
		return ownerPriority;
	}
	public void setOwnerPriority(String ownerPriority) {
		this.ownerPriority = ownerPriority;
	}
	
	
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getAboutMePath() {
		return aboutMePath;
	}
	public void setAboutMePath(String aboutMePath) {
		this.aboutMePath = aboutMePath;
	}
	public String getAboutMeType() {
		return aboutMeType;
	}
	public void setAboutMeType(String aboutMeType) {
		this.aboutMeType = aboutMeType;
	}
	
	
}
