package com.sbh.vo;

import java.io.Serializable;

public class CCStoreInfoVO implements Serializable{
	/**
	 * Serial Version User Id
	 */
	private static final long serialVersionUID = 1L;
	private String storeNumber;
	private String clientCertificatePath;
	private String password;
	private String host;
	private String port;
	private String privateKey;
	private String keyRefId;
	
	/**
	 * @return the privateKey
	 */
	public String getPrivateKey() {
		return privateKey;
	}
	/**
	 * @param privateKey the privateKey to set
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	/**
	 * @return the storeNumber
	 */
	public String getStoreNumber() {
		return storeNumber;
	}
	/**
	 * @param storeNumber the storeNumber to set
	 */
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	/**
	 * @return the clientCertificatePath
	 */
	public String getClientCertificatePath() {
		return clientCertificatePath;
	}
	/**
	 * @param clientCertificatePath the clientCertificatePath to set
	 */
	public void setClientCertificatePath(String clientCertificatePath) {
		this.clientCertificatePath = clientCertificatePath;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
	

}
