package com.sbh.vo;

import java.io.Serializable;

public class OrderItemDetailsVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String orderItemStatus;
	private String prodCode;
	private String cateId;
	private String qty;
	private String vendPrice;
	private String sbhPrice;
	private String orderCreatedDt;
	private String custFirstName;
	private String custLastName;
	private String custId;
	private String custEmail;
	private String custPhone;
	private String custTransId;
	private String airCommPct;
	private String airComm;	
	private String airName;
	private String airContactName;	
	private String airAddr1;
	private String airAddr2;
	private String airPhone;
	private String airEmail;
	private String airCity;
	private String airState;
	private String airZip;
	private String airUserId;
	private String flightNo;
	private String orderId;
	private String orderItemId;
	private String custAddress1;
	private String custAddress2;
	private String custCity;
	private String custState;
	private String custCountry;
	private String custZip;
	private String custShipFirstName;
	private String custShipLastName;
	private String custShipAddress1;
	private String custShipAddress2;
	private String custShipCity;
	private String custShipState;
	private String custShipCountry;
	private String custShipZip;
	private String custShipEmail;
	private String custShipPhone;
	private String vendorName;
	private String prodId;
	private String ownerId;
	private String ownerType;
	private String ownerEmail;
	private String ownerSecondaryEmail;
	private String ownerContactName;
	private String ownerAddr1;
	private String ownerAddr2;
	private String ownerCity;
	private String ownerState;
	private String ownerZip;
	private String ownerCountry;
	private String ownerPhone;
	private String ownerPhoneExt;
	private String itemName;
	private String brandName;
	private String itemSize;
	private String itemColor;
	private String mainImgPath;
	private String mainImgType;
	private String mainImgCaption;
	private String cCNo ;
	private String cvv;
	private String cvvStatus;
	private String maskCCNo;
	private String CCNoStatus ;
	private String CCNoComments ;
	private String cardFname;
	private String cardLname;	
	private String cardType;
	private String cardBankCode;
	private String expDt;
	private String payAmount;
	private String custShipAddressSecond;
	private String custAddressSecond;
	private String returnPolicy;
	private String seturnPolicy;
	private String custServicePhone;
	private String paymentTxnRefId;
	private String custServicePhoneExtnNo;
	private String custServiceEmailId;
	private String chargeType;
	private String paymentId;
	private String travelDate;
	private String reasonForFailure;
	private String reasonForReject;
	private String returnDateExpired;
	private String returnDays;
	private String rmaGeneratedNo;
	private String rmaStatus;
	private String returnReason;
	private String vendorComments;
	private String poNo;
	private String poDate;
	private String poPct;
	private String poPrice;
	private String invoiceNo;
	private String preAuthCC;
	private String chargeBackAmount;
	private String rmaGeneratedDate;
	private String adminComments;
	private String shipmentStatus;
	private String cancelReason;
	private String returnCustomerQuantity;
	private String ownerLoginId;
	private String allColors;
	private String allSizes;
	private String color;
	private String size;
	private String shortDesc;
	private String personalInfo;
	private String specialInst;
	private String returnId;
	private String returnQuantity;
	private String custReturnQuantity;
	private String chargeBackPrice;
	private String rmaApprovedQuantity;
	private String isEligibleToReturn;
	private String rmaAuthorizeSignatory;
	private String isRMAGenerated;
	private String isSpecialProd;
	private String creditCardNoLFD;//Credit Card's Last Four Digits.
	private String keyRefId;//Key reference id.
	
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
	/**
	 * @return the isSpecialProd
	 */
	public String getIsSpecialProd() {
		return isSpecialProd;
	}
	/**
	 * @param isSpecialProd the isSpecialProd to set
	 */
	public void setIsSpecialProd(String isSpecialProd) {
		this.isSpecialProd = isSpecialProd;
	}
	public String getIsRMAGenerated() {
		return isRMAGenerated;
	}
	public void setIsRMAGenerated(String isRMAGenerated) {
		this.isRMAGenerated = isRMAGenerated;
	}
	public String getRmaAuthorizeSignatory() {
		return rmaAuthorizeSignatory;
	}
	public void setRmaAuthorizeSignatory(String rmaAuthorizeSignatory) {
		this.rmaAuthorizeSignatory = rmaAuthorizeSignatory;
	}
	/**
	 * @return the allColors
	 */
	public String getAllColors() {
		return allColors;
	}
	/**
	 * @param allColors the allColors to set
	 */
	public void setAllColors(String allColors) {
		this.allColors = allColors;
	}
	/**
	 * @return the allSizes
	 */
	public String getAllSizes() {
		return allSizes;
	}
	/**
	 * @param allSizes the allSizes to set
	 */
	public void setAllSizes(String allSizes) {
		this.allSizes = allSizes;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the shortDesc
	 */
	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * @param shortDesc the shortDesc to set
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	/**
	 * @return the personalInfo
	 */
	public String getPersonalInfo() {
		return personalInfo;
	}
	/**
	 * @param personalInfo the personalInfo to set
	 */
	public void setPersonalInfo(String personalInfo) {
		this.personalInfo = personalInfo;
	}
	/**
	 * @return the specialInst
	 */
	public String getSpecialInst() {
		return specialInst;
	}
	/**
	 * @param specialInst the specialInst to set
	 */
	public void setSpecialInst(String specialInst) {
		this.specialInst = specialInst;
	}
	/**
	 * @return the preAuthCC
	 */
	public String getPreAuthCC() {
		return preAuthCC;
	}
	/**
	 * @param preAuthCC the preAuthCC to set
	 */
	public void setPreAuthCC(String preAuthCC) {
		this.preAuthCC = preAuthCC;
	}
	/**
	 * @return the poPct
	 */
	public String getPoPct() {
		return poPct;
	}
	/**
	 * @param poPct the poPct to set
	 */
	public void setPoPct(String poPct) {
		this.poPct = poPct;
	}
	/**
	 * @return the poNo
	 */
	public String getPoNo() {
		return poNo;
	}
	/**
	 * @param poNo the poNo to set
	 */
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	/**
	 * @return the poDate
	 */
	public String getPoDate() {
		return poDate;
	}
	/**
	 * @param poDate the poDate to set
	 */
	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	/**
	 * @return the custServiceExtnNo
	 */
	public String getCustServicePhoneExtnNo() {
		return custServicePhoneExtnNo;
	}
	/**
	 * @param custServiceExtnNo the custServiceExtnNo to set
	 */
	public void setCustServicePhoneExtnNo(String custServicePhoneExtnNo) {
		this.custServicePhoneExtnNo = custServicePhoneExtnNo;
	}
	/**
	 * @return the custServiceEmailId
	 */
	public String getCustServiceEmailId() {
		return custServiceEmailId;
	}
	/**
	 * @param custServiceEmailId the custServiceEmailId to set
	 */
	public void setCustServiceEmailId(String custServiceEmailId) {
		this.custServiceEmailId = custServiceEmailId;
	}
	public String getSeturnPolicy() {
		return seturnPolicy;
	}
	public void setSeturnPolicy(String seturnPolicy) {
		this.seturnPolicy = seturnPolicy;
	}
	public String getReturnPolicy() {
		return returnPolicy;
	}
	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}	
	public String getCateId() {
		return cateId;
	}
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getVendPrice() {
		return vendPrice;
	}
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	public String getSbhPrice() {
		return sbhPrice;
	}
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	public String getOrderCreatedDt() {
		return orderCreatedDt;
	}
	public void setOrderCreatedDt(String orderCreatedDt) {
		this.orderCreatedDt = orderCreatedDt;
	}
	public String getOrderItemStatus() {
		return orderItemStatus;
	}
	public void setOrderItemStatus(String orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}
	
	/**
	 * @return the custEmail
	 */
	public String getCustEmail() {
		return custEmail;
	}
	/**
	 * @param custEmail the custEmail to set
	 */
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the custZip
	 */
	public String getCustZip() {
		return custZip;
	}
	/**
	 * @param custZip the custZip to set
	 */
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}	
	/**
	 * @return the custCity
	 */
	public String getCustCity() {
		return custCity;
	}
	/**
	 * @param custCity the custCity to set
	 */
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	/**
	 * @return the custState
	 */
	public String getCustState() {
		return custState;
	}
	/**
	 * @param custState the custState to set
	 */
	public void setCustState(String custState) {
		this.custState = custState;
	}
	/**
	 * @return the custCountry
	 */
	public String getCustCountry() {
		return custCountry;
	}
	/**
	 * @param custCountry the custCountry to set
	 */
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	/**
	 * @return the custShipCity
	 */
	public String getCustShipCity() {
		return custShipCity;
	}
	/**
	 * @param custShipCity the custShipCity to set
	 */
	public void setCustShipCity(String custShipCity) {
		this.custShipCity = custShipCity;
	}
	/**
	 * @return the custShipState
	 */
	public String getCustShipState() {
		return custShipState;
	}
	/**
	 * @param custShipState the custShipState to set
	 */
	public void setCustShipState(String custShipState) {
		this.custShipState = custShipState;
	}
	/**
	 * @return the custShipCountry
	 */
	public String getCustShipCountry() {
		return custShipCountry;
	}
	/**
	 * @param custShipCountry the custShipCountry to set
	 */
	public void setCustShipCountry(String custShipCountry) {
		this.custShipCountry = custShipCountry;
	}
	/**
	 * @return the custShipZip
	 */
	public String getCustShipZip() {
		return custShipZip;
	}
	/**
	 * @param custShipZip the custShipZip to set
	 */
	public void setCustShipZip(String custShipZip) {
		this.custShipZip = custShipZip;
	}
	/**
	 * @return the custShipEmail
	 */
	public String getCustShipEmail() {
		return custShipEmail;
	}
	/**
	 * @param custShipEmail the custShipEmail to set
	 */
	public void setCustShipEmail(String custShipEmail) {
		this.custShipEmail = custShipEmail;
	}
	/**
	 * @return the custShipPhone
	 */
	public String getCustShipPhone() {
		return custShipPhone;
	}
	/**
	 * @param custShipPhone the custShipPhone to set
	 */
	public void setCustShipPhone(String custShipPhone) {
		this.custShipPhone = custShipPhone;
	}	
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the custFirstName
	 */
	public String getCustFirstName() {
		return custFirstName;
	}
	/**
	 * @param custFirstName the custFirstName to set
	 */
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	/**
	 * @return the custLastName
	 */
	public String getCustLastName() {
		return custLastName;
	}
	/**
	 * @param custLastName the custLastName to set
	 */
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	/**
	 * @return the custShipFirstName
	 */
	public String getCustShipFirstName() {
		return custShipFirstName;
	}
	/**
	 * @param custShipFirstName the custShipFirstName to set
	 */
	public void setCustShipFirstName(String custShipFirstName) {
		this.custShipFirstName = custShipFirstName;
	}
	/**
	 * @return the custShipLastName
	 */
	public String getCustShipLastName() {
		return custShipLastName;
	}
	/**
	 * @param custShipLastName the custShipLastName to set
	 */
	public void setCustShipLastName(String custShipLastName) {
		this.custShipLastName = custShipLastName;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return prodId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the mainImgPath
	 */
	public String getMainImgPath() {
		return mainImgPath;
	}
	/**
	 * @param mainImgPath the mainImgPath to set
	 */
	public void setMainImgPath(String mainImgPath) {
		this.mainImgPath = mainImgPath;
	}
	/**
	 * @return the mainImgType
	 */
	public String getMainImgType() {
		return mainImgType;
	}
	/**
	 * @param mainImgType the mainImgType to set
	 */
	public void setMainImgType(String mainImgType) {
		this.mainImgType = mainImgType;
	}
	/**
	 * @return the mainImgCaption
	 */
	public String getMainImgCaption() {
		return mainImgCaption;
	}
	/**
	 * @param mainImgCaption the mainImgCaption to set
	 */
	public void setMainImgCaption(String mainImgCaption) {
		this.mainImgCaption = mainImgCaption;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the ownerEmail
	 */
	public String getOwnerEmail() {
		return ownerEmail;
	}
	/**
	 * @param ownerEmail the ownerEmail to set
	 */
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	/**
	 * @return the ownerContactName
	 */
	public String getOwnerContactName() {
		return ownerContactName;
	}
	/**
	 * @param ownerContactName the ownerContactName to set
	 */
	public void setOwnerContactName(String ownerContactName) {
		this.ownerContactName = ownerContactName;
	}
	/**
	 * @return the cCNo
	 */
	public String getCCNo() {
		return cCNo;
	}
	/**
	 * @param no the cCNo to set
	 */
	public void setCCNo(String no) {
		cCNo = no;
	}
	/**
	 * @return the cCNoStatus
	 */
	public String getCCNoStatus() {
		return CCNoStatus;
	}
	/**
	 * @param noStatus the cCNoStatus to set
	 */
	public void setCCNoStatus(String noStatus) {
		CCNoStatus = noStatus;
	}
	/**
	 * @return the cCNoComments
	 */
	public String getCCNoComments() {
		return CCNoComments;
	}
	/**
	 * @param noComments the cCNoComments to set
	 */
	public void setCCNoComments(String noComments) {
		CCNoComments = noComments;
	}
	/**
	 * @return the cardFname
	 */
	public String getCardFname() {
		return cardFname;
	}
	/**
	 * @param cardFname the cardFname to set
	 */
	public void setCardFname(String cardFname) {
		this.cardFname = cardFname;
	}
	/**
	 * @return the cardLname
	 */
	public String getCardLname() {
		return cardLname;
	}
	/**
	 * @param cardLname the cardLname to set
	 */
	public void setCardLname(String cardLname) {
		this.cardLname = cardLname;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the cardBankCode
	 */
	public String getCardBankCode() {
		return cardBankCode;
	}
	/**
	 * @param cardBankCode the cardBankCode to set
	 */
	public void setCardBankCode(String cardBankCode) {
		this.cardBankCode = cardBankCode;
	}
	/**
	 * @return the expDt
	 */
	public String getExpDt() {
		return expDt;
	}
	/**
	 * @param expDt the expDt to set
	 */
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	/**
	 * @return the payAmount
	 */
	public String getPayAmount() {
		return payAmount;
	}
	/**
	 * @param payAmount the payAmount to set
	 */
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	/**
	 * @return the custShipAddressSecond
	 */
	public String getCustShipAddressSecond() {
		return custShipAddressSecond;
	}
	/**
	 * @param custShipAddressSecond the custShipAddressSecond to set
	 */
	public void setCustShipAddressSecond(String custShipAddressSecond) {
		this.custShipAddressSecond = custShipAddressSecond;
	}
	/**
	 * @return the custAddressSecond
	 */
	public String getCustAddressSecond() {
		return custAddressSecond;
	}
	/**
	 * @param custAddressSecond the custAddressSecond to set
	 */
	public void setCustAddressSecond(String custAddressSecond) {
		this.custAddressSecond = custAddressSecond;
	}
	/**
	 * @return the custServicePhone
	 */
	public String getCustServicePhone() {
		return custServicePhone;
	}
	/**
	 * @param custServicePhone the custServicePhone to set
	 */
	public void setCustServicePhone(String custServicePhone) {
		this.custServicePhone = custServicePhone;
	}
	/**
	 * @return the maskCCNo
	 */
	public String getMaskCCNo() {
		return maskCCNo;
	}
	/**
	 * @param maskCCNo the maskCCNo to set
	 */
	public void setMaskCCNo(String maskCCNo) {
		this.maskCCNo = maskCCNo;
	}
	/**
	 * @return the custAddress1
	 */
	public String getCustAddress1() {
		return custAddress1;
	}
	/**
	 * @param custAddress1 the custAddress1 to set
	 */
	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}
	/**
	 * @return the custAddress2
	 */
	public String getCustAddress2() {
		return custAddress2;
	}
	/**
	 * @param custAddress2 the custAddress2 to set
	 */
	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}
	/**
	 * @return the custShipAddress1
	 */
	public String getCustShipAddress1() {
		return custShipAddress1;
	}
	/**
	 * @param custShipAddress1 the custShipAddress1 to set
	 */
	public void setCustShipAddress1(String custShipAddress1) {
		this.custShipAddress1 = custShipAddress1;
	}
	/**
	 * @return the custShipAddress2
	 */
	public String getCustShipAddress2() {
		return custShipAddress2;
	}
	/**
	 * @param custShipAddress2 the custShipAddress2 to set
	 */
	public void setCustShipAddress2(String custShipAddress2) {
		this.custShipAddress2 = custShipAddress2;
	}
	public String getPaymentTxnRefId() {
		return paymentTxnRefId;
	}
	public void setPaymentTxnRefId(String paymentTxnRefId) {
		this.paymentTxnRefId = paymentTxnRefId;
	}
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	/**
	 * @return the travelDate
	 */
	public String getTravelDate() {
		return travelDate;
	}
	/**
	 * @param travelDate the travelDate to set
	 */
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	public String getReasonForFailure() {
		return reasonForFailure;
	}
	public void setReasonForFailure(String reasonForFailure) {
		this.reasonForFailure = reasonForFailure;
	}
	/**
	 * @return the reasonForReject
	 */
	public String getReasonForReject() {
		return reasonForReject;
	}
	/**
	 * @param reasonForReject the reasonForReject to set
	 */
	public void setReasonForReject(String reasonForReject) {
		this.reasonForReject = reasonForReject;
	}
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	/**
	 * @return the cvvStatus
	 */
	public String getCvvStatus() {
		return cvvStatus;
	}
	/**
	 * @param cvvStatus the cvvStatus to set
	 */
	public void setCvvStatus(String cvvStatus) {
		this.cvvStatus = cvvStatus;
	}
	/**
	 * @return the returnDateExpired
	 */
	public String getReturnDateExpired() {
		return returnDateExpired;
	}
	/**
	 * @param returnDateExpired the returnDateExpired to set
	 */
	public void setReturnDateExpired(String returnDateExpired) {
		this.returnDateExpired = returnDateExpired;
	}
	/**
	 * @return the returnDays
	 */
	public String getReturnDays() {
		return returnDays;
	}
	/**
	 * @param returnDays the returnDays to set
	 */
	public void setReturnDays(String returnDays) {
		this.returnDays = returnDays;
	}
	/**
	 * @return the rmaGeneratedNo
	 */
	public String getRmaGeneratedNo() {
		return rmaGeneratedNo;
	}
	/**
	 * @param rmaGeneratedNo the rmaGeneratedNo to set
	 */
	public void setRmaGeneratedNo(String rmaGeneratedNo) {
		this.rmaGeneratedNo = rmaGeneratedNo;
	}
	/**
	 * @return the ownerAddr1
	 */
	public String getOwnerAddr1() {
		return ownerAddr1;
	}
	/**
	 * @param ownerAddr1 the ownerAddr1 to set
	 */
	public void setOwnerAddr1(String ownerAddr1) {
		this.ownerAddr1 = ownerAddr1;
	}
	/**
	 * @return the ownerAddr2
	 */
	public String getOwnerAddr2() {
		return ownerAddr2;
	}
	/**
	 * @param ownerAddr2 the ownerAddr2 to set
	 */
	public void setOwnerAddr2(String ownerAddr2) {
		this.ownerAddr2 = ownerAddr2;
	}
	/**
	 * @return the ownerCity
	 */
	public String getOwnerCity() {
		return ownerCity;
	}
	/**
	 * @param ownerCity the ownerCity to set
	 */
	public void setOwnerCity(String ownerCity) {
		this.ownerCity = ownerCity;
	}
	/**
	 * @return the ownerState
	 */
	public String getOwnerState() {
		return ownerState;
	}
	/**
	 * @param ownerState the ownerState to set
	 */
	public void setOwnerState(String ownerState) {
		this.ownerState = ownerState;
	}
	/**
	 * @return the ownerZip
	 */
	public String getOwnerZip() {
		return ownerZip;
	}
	/**
	 * @param ownerZip the ownerZip to set
	 */
	public void setOwnerZip(String ownerZip) {
		this.ownerZip = ownerZip;
	}
	/**
	 * @return the ownerCountry
	 */
	public String getOwnerCountry() {
		return ownerCountry;
	}
	/**
	 * @param ownerCountry the ownerCountry to set
	 */
	public void setOwnerCountry(String ownerCountry) {
		this.ownerCountry = ownerCountry;
	}
	/**
	 * @return the ownerPhone
	 */
	public String getOwnerPhone() {
		return ownerPhone;
	}
	/**
	 * @param ownerPhone the ownerPhone to set
	 */
	public void setOwnerPhone(String ownerPhone) {
		this.ownerPhone = ownerPhone;
	}
	/**
	 * @return the ownerPhoneExt
	 */
	public String getOwnerPhoneExt() {
		return ownerPhoneExt;
	}
	/**
	 * @param ownerPhoneExt the ownerPhoneExt to set
	 */
	public void setOwnerPhoneExt(String ownerPhoneExt) {
		this.ownerPhoneExt = ownerPhoneExt;
	}
	/**
	 * @return the invoiceNo
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}
	/**
	 * @param invoiceNo the invoiceNo to set
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	/**
	 * @return the rmaStatus
	 */
	public String getRmaStatus() {
		return rmaStatus;
	}
	/**
	 * @param rmaStatus the rmaStatus to set
	 */
	public void setRmaStatus(String rmaStatus) {
		this.rmaStatus = rmaStatus;
	}
	/**
	 * @return the chargeBackAmount
	 */
	public String getChargeBackAmount() {
		return chargeBackAmount;
	}
	/**
	 * @param chargeBackAmount the chargeBackAmount to set
	 */
	public void setChargeBackAmount(String chargeBackAmount) {
		this.chargeBackAmount = chargeBackAmount;
	}
	/**
	 * @return the returnReason
	 */
	public String getReturnReason() {
		return returnReason;
	}
	/**
	 * @param returnReason the returnReason to set
	 */
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	/**
	 * @return the vendorComments
	 */
	public String getVendorComments() {
		return vendorComments;
	}
	/**
	 * @param vendorComments the vendorComments to set
	 */
	public void setVendorComments(String vendorComments) {
		this.vendorComments = vendorComments;
	}
	/**
	 * @return the rmaGeneratedDate
	 */
	public String getRmaGeneratedDate() {
		return rmaGeneratedDate;
	}
	/**
	 * @param rmaGeneratedDate the rmaGeneratedDate to set
	 */
	public void setRmaGeneratedDate(String rmaGeneratedDate) {
		this.rmaGeneratedDate = rmaGeneratedDate;
	}
	/**
	 * @return the adminComments
	 */
	public String getAdminComments() {
		return adminComments;
	}
	/**
	 * @param adminComments the adminComments to set
	 */
	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}
	/**
	 * @return the shipmentStatus
	 */
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	/**
	 * @param shipmentStatus the shipmentStatus to set
	 */
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	/**
	 * @return the cancelReason
	 */
	public String getCancelReason() {
		return cancelReason;
	}
	/**
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	/**
	 * @return the returnQuantity
	 */
	public String getReturnQuantity() {
		return returnQuantity;
	}
	/**
	 * @param returnQuantity the returnQuantity to set
	 */
	public void setReturnQuantity(String returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	/**
	 * @return the poPrice
	 */
	public String getPoPrice() {
		return poPrice;
	}
	/**
	 * @param poPrice the poPrice to set
	 */
	public void setPoPrice(String poPrice) {
		this.poPrice = poPrice;
	}
	/**
	 * @return the itemSize
	 */
	public String getItemSize() {
		return itemSize;
	}
	/**
	 * @param itemSize the itemSize to set
	 */
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	/**
	 * @return the itemColor
	 */
	public String getItemColor() {
		return itemColor;
	}
	/**
	 * @param itemColor the itemColor to set
	 */
	public void setItemColor(String itemColor) {
		this.itemColor = itemColor;
	}
	/**
	 * @return the returnCustomerQuantity
	 */
	public String getReturnCustomerQuantity() {
		return returnCustomerQuantity;
	}
	/**
	 * @param returnCustomerQuantity the returnCustomerQuantity to set
	 */
	public void setReturnCustomerQuantity(String returnCustomerQuantity) {
		this.returnCustomerQuantity = returnCustomerQuantity;
	}
	/**
	 * @return the ownerLoginId
	 */
	public String getOwnerLoginId() {
		return ownerLoginId;
	}
	/**
	 * @param ownerLoginId the ownerLoginId to set
	 */
	public void setOwnerLoginId(String ownerLoginId) {
		this.ownerLoginId = ownerLoginId;
	}
	/**
	 * @return the returnId
	 */
	public String getReturnId() {
		return returnId;
	}
	/**
	 * @param returnId the returnId to set
	 */
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	/**
	 * @return the custReturnQuantity
	 */
	public String getCustReturnQuantity() {
		return custReturnQuantity;
	}
	/**
	 * @param custReturnQuantity the custReturnQuantity to set
	 */
	public void setCustReturnQuantity(String custReturnQuantity) {
		this.custReturnQuantity = custReturnQuantity;
	}
	/**
	 * @return the chargeBackPrice
	 */
	public String getChargeBackPrice() {
		return chargeBackPrice;
	}
	/**
	 * @param chargeBackPrice the chargeBackPrice to set
	 */
	public void setChargeBackPrice(String chargeBackPrice) {
		this.chargeBackPrice = chargeBackPrice;
	}
	/**
	 * @return the airCommPct
	 */
	public String getAirCommPct() {
		return airCommPct;
	}
	/**
	 * @param airCommPct the airCommPct to set
	 */
	public void setAirCommPct(String airCommPct) {
		this.airCommPct = airCommPct;
	}
	/**
	 * @return the airComm
	 */
	public String getAirComm() {
		return airComm;
	}
	/**
	 * @param airComm the airComm to set
	 */
	public void setAirComm(String airComm) {
		this.airComm = airComm;
	}
	/**
	 * @return the airContactName
	 */
	public String getAirContactName() {
		return airContactName;
	}
	/**
	 * @param airContactName the airContactName to set
	 */
	public void setAirContactName(String airContactName) {
		this.airContactName = airContactName;
	}
	/**
	 * @return the airAddr1
	 */
	public String getAirAddr1() {
		return airAddr1;
	}
	/**
	 * @param airAddr1 the airAddr1 to set
	 */
	public void setAirAddr1(String airAddr1) {
		this.airAddr1 = airAddr1;
	}
	/**
	 * @return the airAddr2
	 */
	public String getAirAddr2() {
		return airAddr2;
	}
	/**
	 * @param airAddr2 the airAddr2 to set
	 */
	public void setAirAddr2(String airAddr2) {
		this.airAddr2 = airAddr2;
	}
	/**
	 * @return the airPhone
	 */
	public String getAirPhone() {
		return airPhone;
	}
	/**
	 * @param airPhone the airPhone to set
	 */
	public void setAirPhone(String airPhone) {
		this.airPhone = airPhone;
	}
	/**
	 * @return the airEmail
	 */
	public String getAirEmail() {
		return airEmail;
	}
	/**
	 * @param airEmail the airEmail to set
	 */
	public void setAirEmail(String airEmail) {
		this.airEmail = airEmail;
	}
	/**
	 * @return the airCity
	 */
	public String getAirCity() {
		return airCity;
	}
	/**
	 * @param airCity the airCity to set
	 */
	public void setAirCity(String airCity) {
		this.airCity = airCity;
	}
	/**
	 * @return the airState
	 */
	public String getAirState() {
		return airState;
	}
	/**
	 * @param airState the airState to set
	 */
	public void setAirState(String airState) {
		this.airState = airState;
	}
	/**
	 * @return the airZip
	 */
	public String getAirZip() {
		return airZip;
	}
	/**
	 * @param airZip the airZip to set
	 */
	public void setAirZip(String airZip) {
		this.airZip = airZip;
	}
	/**
	 * @return the airUserId
	 */
	public String getAirUserId() {
		return airUserId;
	}
	/**
	 * @param airUserId the airUserId to set
	 */
	public void setAirUserId(String airUserId) {
		this.airUserId = airUserId;
	}
	/**
	 * @return the rmaApprovedQuantity
	 */
	public String getRmaApprovedQuantity() {
		return rmaApprovedQuantity;
	}
	/**
	 * @param rmaApprovedQuantity the rmaApprovedQuantity to set
	 */
	public void setRmaApprovedQuantity(String rmaApprovedQuantity) {
		this.rmaApprovedQuantity = rmaApprovedQuantity;
	}
	/**
	 * @return the isEligibleToReturn
	 */
	public String getIsEligibleToReturn() {
		return isEligibleToReturn;
	}
	/**
	 * @param isEligibleToReturn the isEligibleToReturn to set
	 */
	public void setIsEligibleToReturn(String isEligibleToReturn) {
		this.isEligibleToReturn = isEligibleToReturn;
	}
	/**
	 * @return the ownerSecondaryEmail
	 */
	public String getOwnerSecondaryEmail() {
		return ownerSecondaryEmail;
	}
	/**
	 * @param ownerSecondaryEmail the ownerSecondaryEmail to set
	 */
	public void setOwnerSecondaryEmail(String ownerSecondaryEmail) {
		this.ownerSecondaryEmail = ownerSecondaryEmail;
	}
	/**
	 * @return the creditCardNoLFD
	 */
	public String getCreditCardNoLFD() {
		return creditCardNoLFD;
	}
	/**
	 * @param creditCardNoLFD the creditCardNoLFD to set
	 */
	public void setCreditCardNoLFD(String creditCardNoLFD) {
		this.creditCardNoLFD = creditCardNoLFD;
	}
}
