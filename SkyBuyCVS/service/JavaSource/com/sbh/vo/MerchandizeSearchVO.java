package com.sbh.vo;

import java.io.Serializable;

public class MerchandizeSearchVO implements Serializable{
	private String searchLabel;
	private String searchId;

	
	public void setSearchLabel(String searchLabel) {
		this.searchLabel = searchLabel;
	}
	public String getSearchLabel() {
		return searchLabel;
	}
	
	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
	public String getSearchId() {
		return searchId;
	}

}
