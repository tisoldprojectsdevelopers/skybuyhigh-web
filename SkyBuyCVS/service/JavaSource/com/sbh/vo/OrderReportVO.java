package com.sbh.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderReportVO implements Serializable,Comparable{

	private String orderId;
	private String flightNo;
	private String orderDt;
	private String airId;
	private String airName;
	private String custId;
	private String custTransId;
	private String custBillEmail;
	private String custBillFname;
	private String custBillLname;
	private String custBillPhone;
	private String custBillAddr1;
	private String custBillAddr2;
	private String custBillCity;
	private String custBillState;
	private String custBillCountry;
	private String custBillZip;
	private String custShipEmail;
	private String custShipFname;
	private String custShipLname;
	private String custShipPhone;
	private String custShipAddr1;
	private String custShipAddr2;
	private String custShipCity;
	private String custShipState;
	private String custShipCountry;
	private String custShipZip;
	private ArrayList itemDetails;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public String getOrderDt() {
		return orderDt;
	}
	public void setOrderDt(String orderDt) {
		this.orderDt = orderDt;
	}
	public String getAirId() {
		return airId;
	}
	public void setAirId(String airId) {
		this.airId = airId;
	}
	public String getAirName() {
		return airName;
	}
	public void setAirName(String airName) {
		this.airName = airName;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getCustTransId() {
		return custTransId;
	}
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	public String getCustBillEmail() {
		return custBillEmail;
	}
	public void setCustBillEmail(String custBillEmail) {
		this.custBillEmail = custBillEmail;
	}
	public String getCustBillFname() {
		return custBillFname;
	}
	public void setCustBillFname(String custBillFname) {
		this.custBillFname = custBillFname;
	}
	public String getCustBillLname() {
		return custBillLname;
	}
	public void setCustBillLname(String custBillLname) {
		this.custBillLname = custBillLname;
	}
	public String getCustBillPhone() {
		return custBillPhone;
	}
	public void setCustBillPhone(String custBillPhone) {
		this.custBillPhone = custBillPhone;
	}
	public String getCustBillAddr1() {
		return custBillAddr1;
	}
	public void setCustBillAddr1(String custBillAddr1) {
		this.custBillAddr1 = custBillAddr1;
	}
	public String getCustBillAddr2() {
		return custBillAddr2;
	}
	public void setCustBillAddr2(String custBillAddr2) {
		this.custBillAddr2 = custBillAddr2;
	}
	public String getCustBillCity() {
		return custBillCity;
	}
	public void setCustBillCity(String custBillCity) {
		this.custBillCity = custBillCity;
	}
	public String getCustBillState() {
		return custBillState;
	}
	public void setCustBillState(String custBillState) {
		this.custBillState = custBillState;
	}
	public String getCustBillCountry() {
		return custBillCountry;
	}
	public void setCustBillCountry(String custBillCountry) {
		this.custBillCountry = custBillCountry;
	}
	public String getCustBillZip() {
		return custBillZip;
	}
	public void setCustBillZip(String custBillZip) {
		this.custBillZip = custBillZip;
	}
	public String getCustShipEmail() {
		return custShipEmail;
	}
	public void setCustShipEmail(String custShipEmail) {
		this.custShipEmail = custShipEmail;
	}
	public String getCustShipFname() {
		return custShipFname;
	}
	public void setCustShipFname(String custShipFname) {
		this.custShipFname = custShipFname;
	}
	public String getCustShipLname() {
		return custShipLname;
	}
	public void setCustShipLname(String custShipLname) {
		this.custShipLname = custShipLname;
	}
	public String getCustShipPhone() {
		return custShipPhone;
	}
	public void setCustShipPhone(String custShipPhone) {
		this.custShipPhone = custShipPhone;
	}
	public String getCustShipAddr1() {
		return custShipAddr1;
	}
	public void setCustShipAddr1(String custShipAddr1) {
		this.custShipAddr1 = custShipAddr1;
	}
	public String getCustShipAddr2() {
		return custShipAddr2;
	}
	public void setCustShipAddr2(String custShipAddr2) {
		this.custShipAddr2 = custShipAddr2;
	}
	public String getCustShipCity() {
		return custShipCity;
	}
	public void setCustShipCity(String custShipCity) {
		this.custShipCity = custShipCity;
	}
	public String getCustShipState() {
		return custShipState;
	}
	public void setCustShipState(String custShipState) {
		this.custShipState = custShipState;
	}
	public String getCustShipCountry() {
		return custShipCountry;
	}
	public void setCustShipCountry(String custShipCountry) {
		this.custShipCountry = custShipCountry;
	}
	public String getCustShipZip() {
		return custShipZip;
	}
	public void setCustShipZip(String custShipZip) {
		this.custShipZip = custShipZip;
	}
	public ArrayList getItemDetails() {
		return itemDetails;
	}
	public void setItemDetails(ArrayList itemDetails) {
		this.itemDetails = itemDetails;
	}

	public int hashCode() {
		int iCustId = Integer.parseInt(this.custId);
		return iCustId;
	}

	public boolean equals(Object o) {
		boolean bStatus = false;
		if((o != null) && (o instanceof OrderReportVO)){
			OrderReportVO oOrderReportVO = (OrderReportVO)o;
			if(oOrderReportVO.getCustId().equalsIgnoreCase(this.custId)){
				bStatus = true;
			}
		}
		return bStatus;
	}

	public int compareTo(Object o){
		int iRet = 0;
		if((o != null) && (o instanceof OrderReportVO)){
			OrderReportVO oOrderReportVO = (OrderReportVO)o;
			int iCurCustId = Integer.parseInt(this.custId);
			int iPassingCustId = Integer.parseInt(oOrderReportVO.getCustId());
			if(iCurCustId == iPassingCustId){
				iRet = 0;
			}else if(iCurCustId > iPassingCustId){
				iRet = 1;
			}else{
				iRet = -1;
			}
		}

		return iRet;

	}

}
