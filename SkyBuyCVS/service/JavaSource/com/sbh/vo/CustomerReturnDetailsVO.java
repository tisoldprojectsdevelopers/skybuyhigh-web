/**
 * 
 */
package com.sbh.vo;

/**
 * @author Thapovan
 *
 */
public class CustomerReturnDetailsVO {
	
	private String generatedRMA;
	private String isGeneratedRMA;
	private String rmaStatus;
	private String partnerPhone;
	private String partnerMobile;
	private String partnerPhoneExtn;
	private String partnerEmail;
	private String ownerName;
	private String ownerType;
	private String partnerAddress1;
	private String partnerAddress2;
	private String partnerCity;
	private String partnerState;
	private String partnerZip;
	private String custTransId;
	private String orderItemId;
	private String partnerCountry;
	private String reasonForReturn;
	private String rmaGeneratedDate;
	private String returnQuantity;
	
	/**
	 * @return the partnerCountry
	 */
	public String getPartnerCountry() {
		return partnerCountry;
	}
	/**
	 * @param partnerCountry the partnerCountry to set
	 */
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	/**
	 * @return the generatedRMA
	 */
	public String getGeneratedRMA() {
		return generatedRMA;
	}
	/**
	 * @param generatedRMA the generatedRMA to set
	 */
	public void setGeneratedRMA(String generatedRMA) {
		this.generatedRMA = generatedRMA;
	}
	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}
	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	/**
	 * @return the partnerAddress1
	 */
	public String getPartnerAddress1() {
		return partnerAddress1;
	}
	/**
	 * @param partnerAddress1 the partnerAddress1 to set
	 */
	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}
	/**
	 * @return the partnerAddress2
	 */
	public String getPartnerAddress2() {
		return partnerAddress2;
	}
	/**
	 * @param partnerAddress2 the partnerAddress2 to set
	 */
	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}
	/**
	 * @return the partnerCity
	 */
	public String getPartnerCity() {
		return partnerCity;
	}
	/**
	 * @param partnerCity the partnerCity to set
	 */
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	/**
	 * @return the partnerState
	 */
	public String getPartnerState() {
		return partnerState;
	}
	/**
	 * @param partnerState the partnerState to set
	 */
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	/**
	 * @return the partnerZip
	 */
	public String getPartnerZip() {
		return partnerZip;
	}
	/**
	 * @param partnerZip the partnerZip to set
	 */
	public void setPartnerZip(String partnerZip) {
		this.partnerZip = partnerZip;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the isGeneratedRMA
	 */
	public String getIsGeneratedRMA() {
		return isGeneratedRMA;
	}
	/**
	 * @param isGeneratedRMA the isGeneratedRMA to set
	 */
	public void setIsGeneratedRMA(String isGeneratedRMA) {
		this.isGeneratedRMA = isGeneratedRMA;
	}
	/**
	 * @return the partnerPhone
	 */
	public String getPartnerPhone() {
		return partnerPhone;
	}
	/**
	 * @param partnerPhone the partnerPhone to set
	 */
	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}
	/**
	 * @return the partnerMobile
	 */
	public String getPartnerMobile() {
		return partnerMobile;
	}
	/**
	 * @param partnerMobile the partnerMobile to set
	 */
	public void setPartnerMobile(String partnerMobile) {
		this.partnerMobile = partnerMobile;
	}
	/**
	 * @return the partnerPhoneExtn
	 */
	public String getPartnerPhoneExtn() {
		return partnerPhoneExtn;
	}
	/**
	 * @param partnerPhoneExtn the partnerPhoneExtn to set
	 */
	public void setPartnerPhoneExtn(String partnerPhoneExtn) {
		this.partnerPhoneExtn = partnerPhoneExtn;
	}
	/**
	 * @return the partnerEmail
	 */
	public String getPartnerEmail() {
		return partnerEmail;
	}
	/**
	 * @param partnerEmail the partnerEmail to set
	 */
	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the reasonForReturn
	 */
	public String getReasonForReturn() {
		return reasonForReturn;
	}
	/**
	 * @param reasonForReturn the reasonForReturn to set
	 */
	public void setReasonForReturn(String reasonForReturn) {
		this.reasonForReturn = reasonForReturn;
	}
	/**
	 * @return the rmaStatus
	 */
	public String getRmaStatus() {
		return rmaStatus;
	}
	/**
	 * @param rmaStatus the rmaStatus to set
	 */
	public void setRmaStatus(String rmaStatus) {
		this.rmaStatus = rmaStatus;
	}
	/**
	 * @return the rmaGeneratedDate
	 */
	public String getRmaGeneratedDate() {
		return rmaGeneratedDate;
	}
	/**
	 * @param rmaGeneratedDate the rmaGeneratedDate to set
	 */
	public void setRmaGeneratedDate(String rmaGeneratedDate) {
		this.rmaGeneratedDate = rmaGeneratedDate;
	}
	/**
	 * @return the returnQuantity
	 */
	public String getReturnQuantity() {
		return returnQuantity;
	}
	/**
	 * @param returnQuantity the returnQuantity to set
	 */
	public void setReturnQuantity(String returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	
}
