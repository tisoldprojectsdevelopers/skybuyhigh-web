package com.sbh.vo;

import java.io.Serializable;

public class CustPayTxnVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderItemId;
	private String orderItemStatus;
	private String paymentTxnRefId;
	private String creditCardNo;
	private String maskedCreditCardNo;
	private String cardType;
	private String expiryDate;
	private String creditCardHolderName;
	private String txnMsg;
	private String rejectReason;
	private String custTransId;
	private String custAddress1;
	private String custAddress2;
	private String custCity;
	private String custState;
	private String custCountry;
	private String custZip;
	private String transactionType;
	private String transactionStatus;
	private String transactionDate;
	private String transactionId;
	private String custFirstName;
	private String custLastName;
	private String creditCardLFD;
	private String keyRefId;
	
	
	
	/**
	 * @return the creditCardLFD
	 */
	public String getCreditCardLFD() {
		return creditCardLFD;
	}
	/**
	 * @param creditCardLFD the creditCardLFD to set
	 */
	public void setCreditCardLFD(String creditCardLFD) {
		this.creditCardLFD = creditCardLFD;
	}
	/**
	 * @return the custAddress1
	 */
	public String getCustAddress1() {
		return custAddress1;
	}
	/**
	 * @param custAddress1 the custAddress1 to set
	 */
	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}
	/**
	 * @return the custAddress2
	 */
	public String getCustAddress2() {
		return custAddress2;
	}
	/**
	 * @param custAddress2 the custAddress2 to set
	 */
	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}
	/**
	 * @return the custCity
	 */
	public String getCustCity() {
		return custCity;
	}
	/**
	 * @param custCity the custCity to set
	 */
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	/**
	 * @return the custState
	 */
	public String getCustState() {
		return custState;
	}
	/**
	 * @param custState the custState to set
	 */
	public void setCustState(String custState) {
		this.custState = custState;
	}
	/**
	 * @return the custCountry
	 */
	public String getCustCountry() {
		return custCountry;
	}
	/**
	 * @param custCountry the custCountry to set
	 */
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	/**
	 * @return the custZip
	 */
	public String getCustZip() {
		return custZip;
	}
	/**
	 * @param custZip the custZip to set
	 */
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}
	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}	
	/**
	 * @return the txnMsg
	 */
	public String getTxnMsg() {
		return txnMsg;
	}
	/**
	 * @param txnMsg the txnMsg to set
	 */
	public void setTxnMsg(String txnMsg) {
		this.txnMsg = txnMsg;
	}
	/**
	 * @return the rejectReason
	 */
	public String getRejectReason() {
		return rejectReason;
	}
	/**
	 * @param rejectReason the rejectReason to set
	 */
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	
	/**
	 * @return the paymentTxnRefId
	 */
	public String getPaymentTxnRefId() {
		return paymentTxnRefId;
	}
	/**
	 * @param paymentTxnRefId the paymentTxnRefId to set
	 */
	public void setPaymentTxnRefId(String paymentTxnRefId) {
		this.paymentTxnRefId = paymentTxnRefId;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the transactionStatus
	 */
	public String getTransactionStatus() {
		return transactionStatus;
	}
	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	/**
	 * @return the transactionDate
	 */
	public String getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the creditCardNo
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}
	/**
	 * @param creditCardNo the creditCardNo to set
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}
	/**
	 * @return the maskedCreditCardNo
	 */
	public String getMaskedCreditCardNo() {
		return maskedCreditCardNo;
	}
	/**
	 * @param maskedCreditCardNo the maskedCreditCardNo to set
	 */
	public void setMaskedCreditCardNo(String maskedCreditCardNo) {
		this.maskedCreditCardNo = maskedCreditCardNo;
	}
	
	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the creditCardHolderName
	 */
	public String getCreditCardHolderName() {
		return creditCardHolderName;
	}
	/**
	 * @param creditCardHolderName the creditCardHolderName to set
	 */
	public void setCreditCardHolderName(String creditCardHolderName) {
		this.creditCardHolderName = creditCardHolderName;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the custFirstName
	 */
	public String getCustFirstName() {
		return custFirstName;
	}
	/**
	 * @param custFirstName the custFirstName to set
	 */
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	/**
	 * @return the custLastName
	 */
	public String getCustLastName() {
		return custLastName;
	}
	/**
	 * @param custLastName the custLastName to set
	 */
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	/**
	 * @return the orderItemStatus
	 */
	public String getOrderItemStatus() {
		return orderItemStatus;
	}
	/**
	 * @param orderItemStatus the orderItemStatus to set
	 */
	public void setOrderItemStatus(String orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
}
