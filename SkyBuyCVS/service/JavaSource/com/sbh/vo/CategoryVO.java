package com.sbh.vo;

import java.io.Serializable;

public class CategoryVO implements Serializable{
	
	private String cateId;
	private String cateName;
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return cateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	/**
	 * @return the cateName
	 */
	public String getCateName() {
		return cateName;
	}
	/**
	 * @param cateName the cateName to set
	 */
	public void setCateName(String cateName) {
		this.cateName = cateName;
	}
	
	
}
