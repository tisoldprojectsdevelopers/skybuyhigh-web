package com.sbh.vo;

import java.io.Serializable;
import java.util.Date;

public class EmailLogVO implements Serializable{
	
	
	private Date m_oEmailDt;
	private String m_sEmailFrom;
	private String[] m_saEmailTo;
	private String[] m_saEmailCc;
	private String[] m_saEmailBcc;
	private String m_sEmailSubject;
	private String m_sEmailText;
	private String m_sAttachmentFile;
	private String emailToAddr;
	private String emailCcAddr;
	private String emailBccAddr;
	private String emailStatus;
	private String emailTemplateId;
	private byte[] emailAttachment;
	
	
	/**
	 * @return the emailTemplateId
	 */
	public String getEmailTemplateId() {
		return emailTemplateId;
	}
	/**
	 * @param emailTemplateId the emailTemplateId to set
	 */
	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}
	
	public Date getEmailDt() {
		return m_oEmailDt;
	}
	public void setEmailDt(Date emailDt) {
		m_oEmailDt = emailDt;
	}
	public String getEmailFrom() {
		return m_sEmailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		m_sEmailFrom = emailFrom;
	}
	public String getEmailSubject() {
		return m_sEmailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		m_sEmailSubject = emailSubject;
	}
	
	public void setEmailText(String emailText) {
		this.m_sEmailText = emailText;
	}
	public String getEmailText() {
		return m_sEmailText;
	}
	public String[] getEmailTo() {
		return m_saEmailTo;
	}
	public void setEmailTo(String[] emailTo) {
		m_saEmailTo = emailTo;
	}
	public String[] getEmailCc() {
		return m_saEmailCc;
	}
	public void setEmailCc(String[] emailCc) {
		m_saEmailCc = emailCc;
	}
	
	public void setAttachmentFile(String m_sAttachmentFile) {
		this.m_sAttachmentFile = m_sAttachmentFile;
	}
	public String getAttachmentFile() {
		return m_sAttachmentFile;
	}
	
	public void setEmailBcc(String[] m_saEmailBcc) {
		this.m_saEmailBcc = m_saEmailBcc;
	}	
	public String[] getEmailBcc() {
		return m_saEmailBcc;
	}
	public void setEmailToAddr(String emailToAddr) {
		this.emailToAddr = emailToAddr;
	}
	public String getEmailToAddr() {
		return emailToAddr;
	}
	public void setEmailCcAddr(String emailCcAddr) {
		this.emailCcAddr = emailCcAddr;
	}
	public String getEmailCcAddr() {
		return emailCcAddr;
	}
	public void setEmailBccAddr(String emailBccAddr) {
		this.emailBccAddr = emailBccAddr;
	}
	public String getEmailBccAddr() {
		return emailBccAddr;
	}
	/**
	 * @return the emailStatus
	 */
	public String getEmailStatus() {
		return emailStatus;
	}
	/**
	 * @param emailStatus the emailStatus to set
	 */
	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
	/**
	 * @return the emailAttachment
	 */
	public byte[] getEmailAttachment() {
		return emailAttachment;
	}
	/**
	 * @param emailAttachment the emailAttachment to set
	 */
	public void setEmailAttachment(byte[] emailAttachment) {
		this.emailAttachment = emailAttachment;
	}
	
}
