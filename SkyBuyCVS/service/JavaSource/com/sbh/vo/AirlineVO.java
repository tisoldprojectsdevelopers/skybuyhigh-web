package com.sbh.vo;

import java.io.Serializable;

public class AirlineVO implements Serializable{
	
	private String airId;
	private String airCode;
	private String airUserId;
	private String airName;
	
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the airCode
	 */
	public String getAirCode() {
		return airCode;
	}
	/**
	 * @param airCode the airCode to set
	 */
	public void setAirCode(String airCode) {
		this.airCode = airCode;
	}
	/**
	 * @return the airUserId
	 */
	public String getAirUserId() {
		return airUserId;
	}
	/**
	 * @param airUserId the airUserId to set
	 */
	public void setAirUserId(String airUserId) {
		this.airUserId = airUserId;
	}

}
