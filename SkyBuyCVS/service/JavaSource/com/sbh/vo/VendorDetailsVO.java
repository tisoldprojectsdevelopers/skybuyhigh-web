package com.sbh.vo;

import java.io.Serializable;

public class VendorDetailsVO implements Serializable{
	
	private String vendId;
	private String vendType;
	private String vendName;
	private String vendContactName;
	private String vendAddr1;
	private String vendAddr2;
	private String vendCity;
	private String vendState;
	private String vendCountry;
	private String vendZip;
	private String vendPhone1;
	private String vendPhone2;
	private String vendPhoneExt1;
	private String vendPhoneExt2;
	private String vendEmail;
	private String vendFax;
	private String vendStatus;
	private String vendPassword;
	private String vendUserId;
	private String vendReturnPolicy;
	private String vendComments;
	private String vendContactName2;
	private String custServicePhoneno;
	private String custServicePhonenoExt;
	private String custServiceEmail;
	private String vendMobile1;
	private String vendMobile2;
	private String vendEmail2;
	private String chargeType;
	
	private String aboutMePath;
	private String aboutMeType;
	
	private String returnAddr1;
	private String returnAddr2;
	private String returnCity;
	private String returnState;
	private String returnCountry;
	private String returnZip;
	private String returnDays;
	private String poPct;	
	private String keyRefId;
	
	
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}


	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}


	/**
	 * @return the poPct
	 */
	public String getPoPct() {
		return poPct;
	}


	/**
	 * @param poPct the poPct to set
	 */
	public void setPoPct(String poPct) {
		this.poPct = poPct;
	}


	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return vendId;
	}
	
	
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		this.vendId = vendId;
	}
	/**
	 * @return the vendType
	 */
	public String getVendType() {
		return vendType;
	}
	/**
	 * @param vendType the vendType to set
	 */
	public void setVendType(String vendType) {
		this.vendType = vendType;
	}
	/**
	 * @return the vendName
	 */
	public String getVendName() {
		return vendName;
	}
	/**
	 * @param vendName the vendName to set
	 */
	public void setVendName(String vendName) {
		this.vendName = vendName;
	}
	/**
	 * @return the vendAddr1
	 */
	public String getVendAddr1() {
		return vendAddr1;
	}
	/**
	 * @param vendAddr1 the vendAddr1 to set
	 */
	public void setVendAddr1(String vendAddr1) {
		this.vendAddr1 = vendAddr1;
	}
	/**
	 * @return the vendAddr2
	 */
	public String getVendAddr2() {
		return vendAddr2;
	}
	/**
	 * @param vendAddr2 the vendAddr2 to set
	 */
	public void setVendAddr2(String vendAddr2) {
		this.vendAddr2 = vendAddr2;
	}
	/**
	 * @return the vendCity
	 */
	public String getVendCity() {
		return vendCity;
	}
	/**
	 * @param vendCity the vendCity to set
	 */
	public void setVendCity(String vendCity) {
		this.vendCity = vendCity;
	}
	/**
	 * @return the vendState
	 */
	public String getVendState() {
		return vendState;
	}
	/**
	 * @param vendState the vendState to set
	 */
	public void setVendState(String vendState) {
		this.vendState = vendState;
	}
	/**
	 * @return the vendCountry
	 */
	public String getVendCountry() {
		return vendCountry;
	}
	/**
	 * @param vendCountry the vendCountry to set
	 */
	public void setVendCountry(String vendCountry) {
		this.vendCountry = vendCountry;
	}
	/**
	 * @return the vendStatus
	 */
	public String getVendStatus() {
		return vendStatus;
	}
	/**
	 * @param vendStatus the vendStatus to set
	 */
	public void setVendStatus(String vendStatus) {
		this.vendStatus = vendStatus;
	}	
	/**
	 * @return the vendEmail
	 */
	public String getVendEmail() {
		return vendEmail;
	}
	/**
	 * @param vendEmail the vendEmail to set
	 */
	public void setVendEmail(String vendEmail) {
		this.vendEmail = vendEmail;
	}
	/**
	 * @return the vendFax
	 */
	public String getVendFax() {
		return vendFax;
	}
	/**
	 * @param vendFax the vendFax to set
	 */
	public void setVendFax(String vendFax) {
		this.vendFax = vendFax;
	}
	/**
	 * @return the vendZip
	 */
	public String getVendZip() {
		return vendZip;
	}
	/**
	 * @param vendZip the vendZip to set
	 */
	public void setVendZip(String vendZip) {
		this.vendZip = vendZip;
	}
	/**
	 * @return the vendContactName
	 */
	public String getVendContactName() {
		return vendContactName;
	}
	/**
	 * @param vendContactName the vendContactName to set
	 */
	public void setVendContactName(String vendContactName) {
		this.vendContactName = vendContactName;
	}
	public void setVendPassword(String vendPassword) {
		this.vendPassword = vendPassword;
	}
	public String getVendPassword() {
		return vendPassword;
	}
	public void setVendUserId(String vendUserId) {
		this.vendUserId = vendUserId;
	}
	public String getVendUserId() {
		return vendUserId;
	}
	/**
	 * @return the vendPhone2
	 */
	public String getVendPhone2() {
		return vendPhone2;
	}
	/**
	 * @param vendPhone2 the vendPhone2 to set
	 */
	public void setVendPhone2(String vendPhone2) {
		this.vendPhone2 = vendPhone2;
	}
	/**
	 * @return the vendPhoneExt1
	 */
	public String getVendPhoneExt1() {
		return vendPhoneExt1;
	}
	/**
	 * @param vendPhoneExt1 the vendPhoneExt1 to set
	 */
	public void setVendPhoneExt1(String vendPhoneExt1) {
		this.vendPhoneExt1 = vendPhoneExt1;
	}
	/**
	 * @return the vendPhoneExt2
	 */
	public String getVendPhoneExt2() {
		return vendPhoneExt2;
	}
	/**
	 * @param vendPhoneExt2 the vendPhoneExt2 to set
	 */
	public void setVendPhoneExt2(String vendPhoneExt2) {
		this.vendPhoneExt2 = vendPhoneExt2;
	}
	/**
	 * @return the vendPhone1
	 */
	public String getVendPhone1() {
		return vendPhone1;
	}
	/**
	 * @param vendPhone1 the vendPhone1 to set
	 */
	public void setVendPhone1(String vendPhone1) {
		this.vendPhone1 = vendPhone1;
	}
	/**
	 * @return the vendReturnPolicy
	 */
	
	public void setVendContactName2(String vendContactName2)
	{
		this.vendContactName2=vendContactName2;
	}
	public String getVendContactName2()
	{
		return vendContactName2;
	}
	public void setCustServicePhoneno(String custServicePhoneno)
	{
		this.custServicePhoneno=custServicePhoneno;
	}
	public String getCustServicePhoneno()
	{
		return custServicePhoneno;
	}
	public void setCustServicePhonenoExt(String custServicePhonenoExt)
	{
		this.custServicePhonenoExt=custServicePhonenoExt;
	}
	public String getCustServicePhonenoExt()
	{
		return custServicePhonenoExt;
	}
	public String getCustServiceEmail()
	{
		return custServiceEmail;
	}
	
	public void setCustServiceEmail(String custServiceEmail)
	{
		this.custServiceEmail=custServiceEmail;
	}
	public void setVendMobile1(String vendMobile1)
	{
		this.vendMobile1=vendMobile1;
	}
	public String getVendMobile1()
	{
		return vendMobile1;
	}
	public void setVendMobile2(String vendMobile2)
	{
		this.vendMobile2=vendMobile2;
	}
	public String getVendMobile2()
	{
		return vendMobile2;
	}
	
	public void setVendEmail2(String vendEmail2)
	{
		this.vendEmail2=vendEmail2;
	}
	public String getVendEmail2()
	{
		return vendEmail2;
	}
	
	
	
	
	
	public String getVendReturnPolicy() {
		return vendReturnPolicy;
	}
	/**
	 * @param vendReturnPolicy the vendReturnPolicy to set
	 */
	public void setVendReturnPolicy(String vendReturnPolicy) {
		this.vendReturnPolicy = vendReturnPolicy;
	}
	/**
	 * @return the vendComments
	 */
	public String getVendComments() {
		return vendComments;
	}
	/**
	 * @param vendComments the vendComments to set
	 */
	public void setVendComments(String vendComments) {
		this.vendComments = vendComments;
	}


	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}


	/**
	 * @param chargeType the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}


	/**
	 * @return the returnAddr1
	 */
	public String getReturnAddr1() {
		return returnAddr1;
	}


	/**
	 * @param returnAddr1 the returnAddr1 to set
	 */
	public void setReturnAddr1(String returnAddr1) {
		this.returnAddr1 = returnAddr1;
	}


	/**
	 * @return the returnAddr2
	 */
	public String getReturnAddr2() {
		return returnAddr2;
	}


	/**
	 * @param returnAddr2 the returnAddr2 to set
	 */
	public void setReturnAddr2(String returnAddr2) {
		this.returnAddr2 = returnAddr2;
	}


	/**
	 * @return the returnCity
	 */
	public String getReturnCity() {
		return returnCity;
	}


	/**
	 * @param returnCity the returnCity to set
	 */
	public void setReturnCity(String returnCity) {
		this.returnCity = returnCity;
	}


	/**
	 * @return the returnState
	 */
	public String getReturnState() {
		return returnState;
	}


	/**
	 * @param returnState the returnState to set
	 */
	public void setReturnState(String returnState) {
		this.returnState = returnState;
	}


	/**
	 * @return the returnCountry
	 */
	public String getReturnCountry() {
		return returnCountry;
	}


	/**
	 * @param returnCountry the returnCountry to set
	 */
	public void setReturnCountry(String returnCountry) {
		this.returnCountry = returnCountry;
	}


	/**
	 * @return the returnZip
	 */
	public String getReturnZip() {
		return returnZip;
	}


	/**
	 * @param returnZip the returnZip to set
	 */
	public void setReturnZip(String returnZip) {
		this.returnZip = returnZip;
	}


	/**
	 * @return the returnDays
	 */
	public String getReturnDays() {
		return returnDays;
	}


	/**
	 * @param returnDays the returnDays to set
	 */
	public void setReturnDays(String returnDays) {
		this.returnDays = returnDays;
	}


	/**
	 * @return the aboutMePath
	 */
	public String getAboutMePath() {
		return aboutMePath;
	}


	/**
	 * @param aboutMePath the aboutMePath to set
	 */
	public void setAboutMePath(String aboutMePath) {
		this.aboutMePath = aboutMePath;
	}


	/**
	 * @return the aboutMeType
	 */
	public String getAboutMeType() {
		return aboutMeType;
	}


	/**
	 * @param aboutMeType the aboutMeType to set
	 */
	public void setAboutMeType(String aboutMeType) {
		this.aboutMeType = aboutMeType;
	}
	
	
	
	
	
	
	

}