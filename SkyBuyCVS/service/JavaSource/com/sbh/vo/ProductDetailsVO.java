package com.sbh.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.struts.upload.FormFile;

public class ProductDetailsVO implements Serializable{
	
	private String prodId;
	private String prodCode;
	private String prodTitle;
	private String brandName;
	private String vendPrice;
	private String cateId;
	private String seqId;
	private String ownerId;
	private String shortDesc;
	private String longDesc;
	private String sbhPrice;
	private FormFile prodImgPath;	
	private String cateName;
	private String inShopStatus;
	private String sbhProdStatus;
	private String showProd;
	private String CategoryDate;
	private String effectiveDate;
	private String mainImgType;
	private String view1ImgType;
	private String view2ImgType;
	private String view3ImgType;
	private String mainImgPath;
	private String view1ImgPath;
	private String view2ImgPath;
	private String view3ImgPath;
	private String mainImgCap;
	private String view1ImgCap;
	private String view2ImgCap;
	private String view3ImgCap;
    private String ownerName;
    private String ownerType;
	private String sbhComment;
	private String emailId;
	private String adminApprovalDt;
	private String vendorEmail;
	private String secondaryEmailId;
	private String ownerContactName;
	private String comments;
	private String returnPolicy;
	/*private String validFromDate;
	private String validToDate;*/
	private String instructions;
	private String noImgPath;
	private String poPct;
	private String preAuthCC;
	private String color;
	private String size;
	private String swfPath;
	private String swfType;
	private String isAdvt;
	private String advertisementType;
	private String advertisementPath;
	private String coverflowStatus;
	private String priorityOfMerchandize;
	private String counter;
	private String orignalImagePath;
	private String orignalImageType;
	private String specialProductPath;
	private String specialProdType;
	private String isSpecialProd;
	
	/**
	 * @return the specialProductPath
	 */
	public String getSpecialProductPath() {
		return specialProductPath;
	}
	/**
	 * @param specialProductPath the specialProductPath to set
	 */
	public void setSpecialProductPath(String specialProductPath) {
		this.specialProductPath = specialProductPath;
	}
	/**
	 * @return the specialProdType
	 */
	public String getSpecialProdType() {
		return specialProdType;
	}
	/**
	 * @param specialProdType the specialProdType to set
	 */
	public void setSpecialProdType(String specialProdType) {
		this.specialProdType = specialProdType;
	}
	/**
	 * @return the swfPath
	 */
	public String getSwfPath() {
		return swfPath;
	}
	/**
	 * @param swfPath the swfPath to set
	 */
	public void setSwfPath(String swfPath) {
		this.swfPath = swfPath;
	}
	/**
	 * @return the swfType
	 */
	public String getSwfType() {
		return swfType;
	}
	/**
	 * @param swfType the swfType to set
	 */
	public void setSwfType(String swfType) {
		this.swfType = swfType;
	}
	/**
	 * @return the isAdvt
	 */
	public String getIsAdvt() {
		return isAdvt;
	}
	/**
	 * @param isAdvt the isAdvt to set
	 */
	public void setIsAdvt(String isAdvt) {
		this.isAdvt = isAdvt;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}
	/**
	 * @return the preAuthCC
	 */
	public String getPreAuthCC() {
		return preAuthCC;
	}
	/**
	 * @param preAuthCC the preAuthCC to set
	 */
	public void setPreAuthCC(String preAuthCC) {
		this.preAuthCC = preAuthCC;
	}
	/**
	 * @return the poPct
	 */
	public String getPoPct() {
		return poPct;
	}
	/**
	 * @param poPct the poPct to set
	 */
	public void setPoPct(String poPct) {
		this.poPct = poPct;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return prodId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}	
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return vendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return cateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	/**
	 * @return the shortDesc
	 */
	public String getShortDesc() {
		return shortDesc;
	}
	/**
	 * @param shortDesc the shortDesc to set
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	/**
	 * @return the longDesc
	 */
	public String getLongDesc() {
		return longDesc;
	}
	/**
	 * @param longDesc the longDesc to set
	 */
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return sbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	
	/**
	 * @return the cateName
	 */
	public String getCateName() {
		return cateName;
	}
	/**
	 * @param cateName the cateName to set
	 */
	public void setCateName(String cateName) {
		this.cateName = cateName;
	}
		
	/**
	 * @param showProd the showProd to set
	 */
	public void setShowProd(String showProd) {
		this.showProd = showProd;
	}
	/**
	 * @return the showProd
	 */
	public String getShowProd() {
		return showProd;
	}
	/**
	 * @param categoryDate the categoryDate to set
	 */
	public void setCategoryDate(String categoryDate) {
		CategoryDate = categoryDate;
	}
	/**
	 * @return the categoryDate
	 */
	public String getCategoryDate() {
		return CategoryDate;
	}
	/**
	 * @return the prodTitle
	 */
	public String getProdTitle() {
		return prodTitle;
	}
	/**
	 * @param prodTitle the prodTitle to set
	 */
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	/**
	 * @return the prodImgPath
	 */
	public FormFile getProdImgPath() {
		return prodImgPath;
	}
	/**
	 * @param prodImgPath the prodImgPath to set
	 */
	public void setProdImgPath(FormFile prodImgPath) {
		this.prodImgPath = prodImgPath;
	}	
	/**
	 * @return the seqId
	 */
	public String getSeqId() {
		return seqId;
	}
	/**
	 * @param seqId the seqId to set
	 */
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	public void setEffectiveDate(String EffectiveDate) {
		this.effectiveDate = EffectiveDate;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @return the mainImgType
	 */
	public String getMainImgType() {
		return mainImgType;
	}
	/**
	 * @param mainImgType the mainImgType to set
	 */
	public void setMainImgType(String mainImgType) {
		this.mainImgType = mainImgType;
	}
	/**
	 * @return the view1ImgType
	 */
	public String getView1ImgType() {
		return view1ImgType;
	}
	/**
	 * @param view1ImgType the view1ImgType to set
	 */
	public void setView1ImgType(String view1ImgType) {
		this.view1ImgType = view1ImgType;
	}
	/**
	 * @return the view2ImgType
	 */
	public String getView2ImgType() {
		return view2ImgType;
	}
	/**
	 * @param view2ImgType the view2ImgType to set
	 */
	public void setView2ImgType(String view2ImgType) {
		this.view2ImgType = view2ImgType;
	}
	/**
	 * @return the view3ImgType
	 */
	public String getView3ImgType() {
		return view3ImgType;
	}
	/**
	 * @param view3ImgType the view3ImgType to set
	 */
	public void setView3ImgType(String view3ImgType) {
		this.view3ImgType = view3ImgType;
	}
	/**
	 * @return the mainImgPath
	 */
	public String getMainImgPath() {
		return mainImgPath;
	}
	/**
	 * @param mainImgPath the mainImgPath to set
	 */
	public void setMainImgPath(String mainImgPath) {
		this.mainImgPath = mainImgPath;
	}
	/**
	 * @return the view1ImgPath
	 */
	public String getView1ImgPath() {
		return view1ImgPath;
	}
	/**
	 * @param view1ImgPath the view1ImgPath to set
	 */
	public void setView1ImgPath(String view1ImgPath) {
		this.view1ImgPath = view1ImgPath;
	}
	/**
	 * @return the view2ImgPath
	 */
	public String getView2ImgPath() {
		return view2ImgPath;
	}
	/**
	 * @param view2ImgPath the view2ImgPath to set
	 */
	public void setView2ImgPath(String view2ImgPath) {
		this.view2ImgPath = view2ImgPath;
	}
	/**
	 * @return the view3ImgPath
	 */
	public String getView3ImgPath() {
		return view3ImgPath;
	}
	/**
	 * @param view3ImgPath the view3ImgPath to set
	 */
	public void setView3ImgPath(String view3ImgPath) {
		this.view3ImgPath = view3ImgPath;
	}
	/**
	 * @return the mainImgCap
	 */
	public String getMainImgCap() {
		return mainImgCap;
	}
	/**
	 * @param mainImgCap the mainImgCap to set
	 */
	public void setMainImgCap(String mainImgCap) {
		this.mainImgCap = mainImgCap;
	}
	/**
	 * @return the view1ImgCap
	 */
	public String getView1ImgCap() {
		return view1ImgCap;
	}
	/**
	 * @param view1ImgCap the view1ImgCap to set
	 */
	public void setView1ImgCap(String view1ImgCap) {
		this.view1ImgCap = view1ImgCap;
	}
	/**
	 * @return the view2ImgCap
	 */
	public String getView2ImgCap() {
		return view2ImgCap;
	}
	/**
	 * @param view2ImgCap the view2ImgCap to set
	 */
	public void setView2ImgCap(String view2ImgCap) {
		this.view2ImgCap = view2ImgCap;
	}
	/**
	 * @return the view3ImgCap
	 */
	public String getView3ImgCap() {
		return view3ImgCap;
	}
	/**
	 * @param view3ImgCap the view3ImgCap to set
	 */
	public void setView3ImgCap(String view3ImgCap) {
		this.view3ImgCap = view3ImgCap;
	}
	/**
	 * @return the inShopStatus
	 */
	public String getInShopStatus() {
		return inShopStatus;
	}
	/**
	 * @param inShopStatus the inShopStatus to set
	 */
	public void setInShopStatus(String inShopStatus) {
		this.inShopStatus = inShopStatus;
	}
	/**
	 * @return the sbhProdStatus
	 */
	public String getSbhProdStatus() {
		return sbhProdStatus;
	}
	/**
	 * @param sbhProdStatus the sbhProdStatus to set
	 */
	public void setSbhProdStatus(String sbhProdStatus) {
		this.sbhProdStatus = sbhProdStatus;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}
	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	public void setSbhComment(String sbhComment) {
		this.sbhComment = sbhComment;
	}
	public String getSbhComment() {
		return sbhComment;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmailId() {
		return emailId;
	}
	public String getAdminApprovalDt() {
		return adminApprovalDt;
	}
	public void setAdminApprovalDt(String adminApprovalDt) {
		this.adminApprovalDt = adminApprovalDt;
	}
	/**
	 * @return the vendorEmail
	 */
	public String getVendorEmail() {
		return vendorEmail;
	}
	/**
	 * @param vendorEmail the vendorEmail to set
	 */
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	/**
	 * @return the ownerContactName
	 */
	public String getOwnerContactName() {
		return ownerContactName;
	}
	/**
	 * @param ownerContactName the ownerContactName to set
	 */
	public void setOwnerContactName(String ownerContactName) {
		this.ownerContactName = ownerContactName;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the returnPolicy
	 */
	public String getReturnPolicy() {
		return returnPolicy;
	}
	/**
	 * @param returnPolicy the returnPolicy to set
	 */
	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}	
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	/*public String getValidFromDate() {
		return validFromDate;
	}
	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}
	public String getValidToDate() {
		return validToDate;
	}
	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}*/
	/**
	 * @return the noImgPath
	 */
	public String getNoImgPath() {
		return noImgPath;
	}
	/**
	 * @param noImgPath the noImgPath to set
	 */
	public void setNoImgPath(String noImgPath) {
		this.noImgPath = noImgPath;
	}
	/**
	 * @return the advertisementType
	 */
	public String getAdvertisementType() {
		return advertisementType;
	}
	/**
	 * @param advertisementType the advertisementType to set
	 */
	public void setAdvertisementType(String advertisementType) {
		this.advertisementType = advertisementType;
	}
	/**
	 * @return the advertisementPath
	 */
	public String getAdvertisementPath() {
		return advertisementPath;
	}
	/**
	 * @param advertisementPath the advertisementPath to set
	 */
	public void setAdvertisementPath(String advertisementPath) {
		this.advertisementPath = advertisementPath;
	}
	
	/**
	 * @return the coverflowStatus
	 */
	public String getCoverflowStatus() {
		return coverflowStatus;
	}
	/**
	 * @param coverflowStatus the coverflowStatus to set
	 */
	public void setCoverflowStatus(String coverflowStatus) {
		this.coverflowStatus = coverflowStatus;
	}
	
	public static ArrayList<ProductDetailsVO> getProductsSortedByCoverflowStatus(ArrayList<ProductDetailsVO> oProductDetailsVO) {  
		Collections.sort(oProductDetailsVO,new Comparator<ProductDetailsVO>() {  
			public int compare(ProductDetailsVO p1, ProductDetailsVO p2) {  
				return p1.getCoverflowStatus().compareTo(p2.getCoverflowStatus());  
			}  
		});  
		return oProductDetailsVO;  
	}
	/**
	 * @return the priorityOfMerchandize
	 */
	public String getPriorityOfMerchandize() {
		return priorityOfMerchandize;
	}
	/**
	 * @param priorityOfMerchandize the priorityOfMerchandize to set
	 */
	public void setPriorityOfMerchandize(String priorityOfMerchandize) {
		this.priorityOfMerchandize = priorityOfMerchandize;
	}
	/**
	 * @return the counter
	 */
	public String getCounter() {
		return counter;
	}
	/**
	 * @param counter the counter to set
	 */
	public void setCounter(String counter) {
		this.counter = counter;
	}
	/**
	 * @return the orignalImagePath
	 */
	public String getOrignalImagePath() {
		return orignalImagePath;
	}
	/**
	 * @param orignalImagePath the orignalImagePath to set
	 */
	public void setOrignalImagePath(String orignalImagePath) {
		this.orignalImagePath = orignalImagePath;
	}
	/**
	 * @return the orignalImageType
	 */
	public String getOrignalImageType() {
		return orignalImageType;
	}
	/**
	 * @param orignalImageType the orignalImageType to set
	 */
	public void setOrignalImageType(String orignalImageType) {
		this.orignalImageType = orignalImageType;
	}
	/**
	 * @return the isSpecialProd
	 */
	public String getIsSpecialProd() {
		return isSpecialProd;
	}
	/**
	 * @param isSpecialProd the isSpecialProd to set
	 */
	public void setIsSpecialProd(String isSpecialProd) {
		this.isSpecialProd = isSpecialProd;
	}
	/**
	 * @return the secondaryEmail
	 */
	public String getSecondaryEmailId() {
		return secondaryEmailId;
	}
	/**
	 * @param secondaryEmail the secondaryEmail to set
	 */
	public void setSecondaryEmailId(String secondaryEmailId) {
		this.secondaryEmailId = secondaryEmailId;
	}
	
}
