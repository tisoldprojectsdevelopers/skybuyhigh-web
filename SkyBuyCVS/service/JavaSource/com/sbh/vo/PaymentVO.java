package com.sbh.vo;

import java.io.Serializable;

public class PaymentVO implements Serializable{
	private String paymentId;
	private String orderItemId;
	private String fName;
	private String CCNo;
	private String cardType;
	private String cardBankCode;
	private String expDt;
	
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getOrderItemId() {
		return orderItemId;
	}
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	public String getFName() {
		return fName;
	}
	public void setFName(String name) {
		fName = name;
	}
	public String getCCNo() {
		return CCNo;
	}
	public void setCCNo(String no) {
		CCNo = no;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardBankCode() {
		return cardBankCode;
	}
	public void setCardBankCode(String cardBankCode) {
		this.cardBankCode = cardBankCode;
	}
	public String getExpDt() {
		return expDt;
	}
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	
	
}
