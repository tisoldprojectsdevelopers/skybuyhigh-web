package com.sbh.vo;

import java.io.Serializable;

public class ItemDetailsVO implements Serializable{

	private String ownerName;
	private String orderItemId;
	private String prodCode;
	private String cateId;
	private String ownerId;
	private String ownerType;
	private String prodTitle;
	private String brandName;
	private String qty;
	private String color;
	private String size;
	private String vendPrice;
	private String sbhPrice;
	private String travelDate;
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOrderItemId() {
		return orderItemId;
	}
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getCateId() {
		return cateId;
	}
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	public String getProdTitle() {
		return prodTitle;
	}
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getVendPrice() {
		return vendPrice;
	}
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	public String getSbhPrice() {
		return sbhPrice;
	}
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	public String getTravelDate() {
		return travelDate;
	}
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	
}
