/**
 * 
 */
package com.sbh.vo;

/**
 * @author nbvk
 *
 */
public class AdminEmailAddressVO {
	private String id;
	private String emailAddress;
	private String adminContactName;
	private String adminUserId;
	private String isSuperUser;
	private String isDefault;
	
	/**
	 * @return the isSuperUser
	 */
	public String getIsSuperUser() {
		return isSuperUser;
	}
	/**
	 * @param isSuperUser the isSuperUser to set
	 */
	public void setIsSuperUser(String isSuperUser) {
		this.isSuperUser = isSuperUser;
	}
	/**
	 * @return the isDefault
	 */
	public String getIsDefault() {
		return isDefault;
	}
	/**
	 * @param isDefault the isDefault to set
	 */
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the adminContactName
	 */
	public String getAdminContactName() {
		return adminContactName;
	}
	/**
	 * @param adminContactName the adminContactName to set
	 */
	public void setAdminContactName(String adminContactName) {
		this.adminContactName = adminContactName;
	}
	/**
	 * @return the adminUserId
	 */
	public String getAdminUserId() {
		return adminUserId;
	}
	/**
	 * @param adminUserId the adminUserId to set
	 */
	public void setAdminUserId(String adminUserId) {
		this.adminUserId = adminUserId;
	}
	
}
