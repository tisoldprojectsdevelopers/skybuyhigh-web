/**
 * 
 */
package com.sbh.vo;

/**
 * @author Thapovan
 *
 */
public class OrderReturnDetailsVO {
	
	private int rowCount;
	private String orderItemId;
	private String returnId;
	private String RMANumber;
	private String vendorComments;
	private String customerReason;
	private String adminComments;
	private String custTransId;
	private String custFirstName;
	private String custLastName;
	private String vendorName;
	private String airlineName;
	private String custPhone;
	private String custBillAddr1;
	private String custBillAddr2;
	private String custBillCity;
	private String custBillState;
	private String custBillCountry;
	private String custBillZip;
	private String custShipAddr1;
	private String custShipAddr2;
	private String custShipCity;
	private String custShipState;
	private String custShipCountry;
	private String custShipZip;
	private String prodCode;
	private String orderItemStatus;
	private String categoryId;
	private String sbhPrice;
	private String quantity;
	private String orderCreatedDt;
	private String itemName;
	private String brandName;
	private String airName;
	private String vendPrice;
	private String orderId;
	private String ownerType;
	private String ownerName;
	private String ownerId;
	private String payAmount;
	private String flightNo;
	private String status;
	private String rmaStatus;
	private String totalQuantity;
	private String rmaGeneratedDate;
	private String returnQuantity;
	private String customerReturnQuantity;
	private String vendorReturnQuantity;
	private String partnerAddress1;
	private String partnerAddress2;
	private String partnerCity;
	private String partnerState;
	private String partnerZip;
	private String partnerPhone;
	private String partnerMobile;
	private String partnerPhoneExtn;
	private String partnerEmail;
	private String partnerCountry;
	private String partnerContactName;
	private String rmaAuthorizeSignatory;
	
	
	public String getRmaAuthorizeSignatory() {
		return rmaAuthorizeSignatory;
	}
	public void setRmaAuthorizeSignatory(String rmaAuthorizeSignatory) {
		this.rmaAuthorizeSignatory = rmaAuthorizeSignatory;
	}
	/**
	 * @return the payAmount
	 */
	public String getPayAmount() {
		return payAmount;
	}
	/**
	 * @param payAmount the payAmount to set
	 */
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return vendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the rMANumber
	 */
	public String getRMANumber() {
		return RMANumber;
	}
	/**
	 * @param number the rMANumber to set
	 */
	public void setRMANumber(String number) {
		RMANumber = number;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	
	/**
	 * @return the custFirstName
	 */
	public String getCustFirstName() {
		return custFirstName;
	}
	/**
	 * @param custFirstName the custFirstName to set
	 */
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	/**
	 * @return the custLastName
	 */
	public String getCustLastName() {
		return custLastName;
	}
	/**
	 * @param custLastName the custLastName to set
	 */
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the airlineName
	 */
	public String getAirlineName() {
		return airlineName;
	}
	/**
	 * @param airlineName the airlineName to set
	 */
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custBillAddr1
	 */
	public String getCustBillAddr1() {
		return custBillAddr1;
	}
	/**
	 * @param custBillAddr1 the custBillAddr1 to set
	 */
	public void setCustBillAddr1(String custBillAddr1) {
		this.custBillAddr1 = custBillAddr1;
	}
	/**
	 * @return the custBillAddr2
	 */
	public String getCustBillAddr2() {
		return custBillAddr2;
	}
	/**
	 * @param custBillAddr2 the custBillAddr2 to set
	 */
	public void setCustBillAddr2(String custBillAddr2) {
		this.custBillAddr2 = custBillAddr2;
	}
	/**
	 * @return the custBillCity
	 */
	public String getCustBillCity() {
		return custBillCity;
	}
	/**
	 * @param custBillCity the custBillCity to set
	 */
	public void setCustBillCity(String custBillCity) {
		this.custBillCity = custBillCity;
	}
	/**
	 * @return the custBillState
	 */
	public String getCustBillState() {
		return custBillState;
	}
	/**
	 * @param custBillState the custBillState to set
	 */
	public void setCustBillState(String custBillState) {
		this.custBillState = custBillState;
	}
	/**
	 * @return the custBillCountry
	 */
	public String getCustBillCountry() {
		return custBillCountry;
	}
	/**
	 * @param custBillCountry the custBillCountry to set
	 */
	public void setCustBillCountry(String custBillCountry) {
		this.custBillCountry = custBillCountry;
	}
	/**
	 * @return the custBillZip
	 */
	public String getCustBillZip() {
		return custBillZip;
	}
	/**
	 * @param custBillZip the custBillZip to set
	 */
	public void setCustBillZip(String custBillZip) {
		this.custBillZip = custBillZip;
	}
	/**
	 * @return the custShipAddr1
	 */
	public String getCustShipAddr1() {
		return custShipAddr1;
	}
	/**
	 * @param custShipAddr1 the custShipAddr1 to set
	 */
	public void setCustShipAddr1(String custShipAddr1) {
		this.custShipAddr1 = custShipAddr1;
	}
	/**
	 * @return the custShipAddr2
	 */
	public String getCustShipAddr2() {
		return custShipAddr2;
	}
	/**
	 * @param custShipAddr2 the custShipAddr2 to set
	 */
	public void setCustShipAddr2(String custShipAddr2) {
		this.custShipAddr2 = custShipAddr2;
	}
	/**
	 * @return the custShipCity
	 */
	public String getCustShipCity() {
		return custShipCity;
	}
	/**
	 * @param custShipCity the custShipCity to set
	 */
	public void setCustShipCity(String custShipCity) {
		this.custShipCity = custShipCity;
	}
	/**
	 * @return the custShipState
	 */
	public String getCustShipState() {
		return custShipState;
	}
	/**
	 * @param custShipState the custShipState to set
	 */
	public void setCustShipState(String custShipState) {
		this.custShipState = custShipState;
	}
	/**
	 * @return the custShipCountry
	 */
	public String getCustShipCountry() {
		return custShipCountry;
	}
	/**
	 * @param custShipCountry the custShipCountry to set
	 */
	public void setCustShipCountry(String custShipCountry) {
		this.custShipCountry = custShipCountry;
	}
	/**
	 * @return the custShipZip
	 */
	public String getCustShipZip() {
		return custShipZip;
	}
	/**
	 * @param custShipZip the custShipZip to set
	 */
	public void setCustShipZip(String custShipZip) {
		this.custShipZip = custShipZip;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	/**
	 * @return the orderItemStatus
	 */
	public String getOrderItemStatus() {
		return orderItemStatus;
	}
	/**
	 * @param orderItemStatus the orderItemStatus to set
	 */
	public void setOrderItemStatus(String orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return sbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the orderCreatedDt
	 */
	public String getOrderCreatedDt() {
		return orderCreatedDt;
	}
	/**
	 * @param orderCreatedDt the orderCreatedDt to set
	 */
	public void setOrderCreatedDt(String orderCreatedDt) {
		this.orderCreatedDt = orderCreatedDt;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}
	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	/**
	 * @return the returnQuantity
	 */
	public String getReturnQuantity() {
		return returnQuantity;
	}
	/**
	 * @param returnQuantity the returnQuantity to set
	 */
	public void setReturnQuantity(String returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	/**
	 * @return the customerReturnQuantity
	 */
	public String getCustomerReturnQuantity() {
		return customerReturnQuantity;
	}
	/**
	 * @param customerReturnQuantity the customerReturnQuantity to set
	 */
	public void setCustomerReturnQuantity(String customerReturnQuantity) {
		this.customerReturnQuantity = customerReturnQuantity;
	}
	/**
	 * @return the vendorComments
	 */
	public String getVendorComments() {
		return vendorComments;
	}
	/**
	 * @param vendorComments the vendorComments to set
	 */
	public void setVendorComments(String vendorComments) {
		this.vendorComments = vendorComments;
	}
	/**
	 * @return the customerReason
	 */
	public String getCustomerReason() {
		return customerReason;
	}
	/**
	 * @param customerReason the customerReason to set
	 */
	public void setCustomerReason(String customerReason) {
		this.customerReason = customerReason;
	}
	/**
	 * @return the adminComments
	 */
	public String getAdminComments() {
		return adminComments;
	}
	/**
	 * @param adminComments the adminComments to set
	 */
	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}
	/**
	 * @return the rmaStatus
	 */
	public String getRmaStatus() {
		return rmaStatus;
	}
	/**
	 * @param rmaStatus the rmaStatus to set
	 */
	public void setRmaStatus(String rmaStatus) {
		this.rmaStatus = rmaStatus;
	}
	/**
	 * @return the totalQuantity
	 */
	public String getTotalQuantity() {
		return totalQuantity;
	}
	/**
	 * @param totalQuantity the totalQuantity to set
	 */
	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	/**
	 * @return the partnerAddress1
	 */
	public String getPartnerAddress1() {
		return partnerAddress1;
	}
	/**
	 * @param partnerAddress1 the partnerAddress1 to set
	 */
	public void setPartnerAddress1(String partnerAddress1) {
		this.partnerAddress1 = partnerAddress1;
	}
	/**
	 * @return the partnerAddress2
	 */
	public String getPartnerAddress2() {
		return partnerAddress2;
	}
	/**
	 * @param partnerAddress2 the partnerAddress2 to set
	 */
	public void setPartnerAddress2(String partnerAddress2) {
		this.partnerAddress2 = partnerAddress2;
	}
	/**
	 * @return the partnerCity
	 */
	public String getPartnerCity() {
		return partnerCity;
	}
	/**
	 * @param partnerCity the partnerCity to set
	 */
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	/**
	 * @return the partnerState
	 */
	public String getPartnerState() {
		return partnerState;
	}
	/**
	 * @param partnerState the partnerState to set
	 */
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	/**
	 * @return the partnerZip
	 */
	public String getPartnerZip() {
		return partnerZip;
	}
	/**
	 * @param partnerZip the partnerZip to set
	 */
	public void setPartnerZip(String partnerZip) {
		this.partnerZip = partnerZip;
	}
	/**
	 * @return the partnerPhone
	 */
	public String getPartnerPhone() {
		return partnerPhone;
	}
	/**
	 * @param partnerPhone the partnerPhone to set
	 */
	public void setPartnerPhone(String partnerPhone) {
		this.partnerPhone = partnerPhone;
	}
	/**
	 * @return the partnerMobile
	 */
	public String getPartnerMobile() {
		return partnerMobile;
	}
	/**
	 * @param partnerMobile the partnerMobile to set
	 */
	public void setPartnerMobile(String partnerMobile) {
		this.partnerMobile = partnerMobile;
	}
	/**
	 * @return the partnerPhoneExtn
	 */
	public String getPartnerPhoneExtn() {
		return partnerPhoneExtn;
	}
	/**
	 * @param partnerPhoneExtn the partnerPhoneExtn to set
	 */
	public void setPartnerPhoneExtn(String partnerPhoneExtn) {
		this.partnerPhoneExtn = partnerPhoneExtn;
	}
	/**
	 * @return the partnerEmail
	 */
	public String getPartnerEmail() {
		return partnerEmail;
	}
	/**
	 * @param partnerEmail the partnerEmail to set
	 */
	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}
	/**
	 * @return the partnerCountry
	 */
	public String getPartnerCountry() {
		return partnerCountry;
	}
	/**
	 * @param partnerCountry the partnerCountry to set
	 */
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	/**
	 * @return the rmaGeneratedDate
	 */
	public String getRmaGeneratedDate() {
		return rmaGeneratedDate;
	}
	/**
	 * @param rmaGeneratedDate the rmaGeneratedDate to set
	 */
	public void setRmaGeneratedDate(String rmaGeneratedDate) {
		this.rmaGeneratedDate = rmaGeneratedDate;
	}
	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}
	/**
	 * @param ownerName the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	/**
	 * @return the vendorReturnQuantity
	 */
	public String getVendorReturnQuantity() {
		return vendorReturnQuantity;
	}
	/**
	 * @param vendorReturnQuantity the vendorReturnQuantity to set
	 */
	public void setVendorReturnQuantity(String vendorReturnQuantity) {
		this.vendorReturnQuantity = vendorReturnQuantity;
	}
	/**
	 * @return the returnId
	 */
	public String getReturnId() {
		return returnId;
	}
	/**
	 * @param returnId the returnId to set
	 */
	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}
	/**
	 * @return the partnerContactName
	 */
	public String getPartnerContactName() {
		return partnerContactName;
	}
	/**
	 * @param partnerContactName the partnerContactName to set
	 */
	public void setPartnerContactName(String partnerContactName) {
		this.partnerContactName = partnerContactName;
	}
}
