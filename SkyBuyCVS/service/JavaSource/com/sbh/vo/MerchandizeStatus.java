package com.sbh.vo;

import java.io.Serializable;

public class MerchandizeStatus implements Serializable {
  private String prodId;
  private String sbhPrice="";
  private String sbhProdStatus="N";
  private String sbhComment="";
public void setProdId(String prodId) {
	this.prodId = prodId;
}
public String getProdId() {
	return prodId;
}
/**
 * @return the sbhPrice
 */
public String getSbhPrice() {
	return sbhPrice;
}
/**
 * @param sbhPrice the sbhPrice to set
 */
public void setSbhPrice(String sbhPrice) {
	this.sbhPrice = sbhPrice;
}
/**
 * @return the sbhComment
 */
public String getSbhComment() {
	return sbhComment;
}
/**
 * @param sbhComment the sbhComment to set
 */
public void setSbhComment(String sbhComment) {
	this.sbhComment = sbhComment;
}
/**
 * @return the sbhProdStatus
 */
public String getSbhProdStatus() {
	return sbhProdStatus;
}
/**
 * @param sbhProdStatus the sbhProdStatus to set
 */
public void setSbhProdStatus(String sbhProdStatus) {
	this.sbhProdStatus = sbhProdStatus;
}

}
