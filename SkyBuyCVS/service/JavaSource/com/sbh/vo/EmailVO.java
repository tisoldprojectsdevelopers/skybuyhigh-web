package com.sbh.vo;

import java.io.Serializable;

public class EmailVO implements Serializable{
	
	private String emailSubject;
	private String emailContent;
	private String emailOptionalContent;
	private String emailCcList;
	private String emailBccList;
	private String adminEmailAddress;

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}
	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	/**
	 * @return the emailContent
	 */
	public String getEmailContent() {
		return emailContent;
	}
	/**
	 * @param emailContent the emailContent to set
	 */
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	/**
	 * @return the emailOptionalContent
	 */
	public String getEmailOptionalContent() {
		return emailOptionalContent;
	}
	/**
	 * @param emailOptionalContent the emailOptionalContent to set
	 */
	public void setEmailOptionalContent(String emailOptionalContent) {
		this.emailOptionalContent = emailOptionalContent;
	}
	/**
	 * @return the emailCcList
	 */
	public String getEmailCcList() {
		return emailCcList;
	}
	/**
	 * @param emailCcList the emailCcList to set
	 */
	public void setEmailCcList(String emailCcList) {
		this.emailCcList = emailCcList;
	}
	/**
	 * @return the emailBccList
	 */
	public String getEmailBccList() {
		return emailBccList;
	}
	/**
	 * @param emailBccList the emailBccList to set
	 */
	public void setEmailBccList(String emailBccList) {
		this.emailBccList = emailBccList;
	}
	/**
	 * @return the adminEmailAddress
	 */
	public String getAdminEmailAddress() {
		return adminEmailAddress;
	}
	/**
	 * @param adminEmailAddress the adminEmailAddress to set
	 */
	public void setAdminEmailAddress(String adminEmailAddress) {
		this.adminEmailAddress = adminEmailAddress;
	}


}
