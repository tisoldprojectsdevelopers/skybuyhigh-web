/**
 * 
 */
package com.sbh.vo;

/**
 * @author Thapovan
 *
 */
public class SkyBuyCatalogueUploadVO {
	private String id;
	private String uploadType;
	private String catalogueUpdateDate;
	private String welcomePageUpdateDate;
	private String reasonForUpload;
	private String skyBuyCataloguePath;
	private String skyBuyWelcomePath;
	private String rank;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the catalogueUpdateDate
	 */
	public String getCatalogueUpdateDate() {
		return catalogueUpdateDate;
	}
	/**
	 * @param catalogueUpdateDate the catalogueUpdateDate to set
	 */
	public void setCatalogueUpdateDate(String catalogueUpdateDate) {
		this.catalogueUpdateDate = catalogueUpdateDate;
	}
	/**
	 * @return the welcomePageUpdateDate
	 */
	public String getWelcomePageUpdateDate() {
		return welcomePageUpdateDate;
	}
	/**
	 * @param welcomePageUpdateDate the welcomePageUpdateDate to set
	 */
	public void setWelcomePageUpdateDate(String welcomePageUpdateDate) {
		this.welcomePageUpdateDate = welcomePageUpdateDate;
	}
	/**
	 * @return the reasonForUpload
	 */
	public String getReasonForUpload() {
		return reasonForUpload;
	}
	/**
	 * @param reasonForUpload the reasonForUpload to set
	 */
	public void setReasonForUpload(String reasonForUpload) {
		this.reasonForUpload = reasonForUpload;
	}
	/**
	 * @return the rank
	 */
	public String getRank() {
		return rank;
	}
	/**
	 * @param rank the rank to set
	 */
	public void setRank(String rank) {
		this.rank = rank;
	}
	/**
	 * @return the skyBuyCataloguePath
	 */
	public String getSkyBuyCataloguePath() {
		return skyBuyCataloguePath;
	}
	/**
	 * @param skyBuyCataloguePath the skyBuyCataloguePath to set
	 */
	public void setSkyBuyCataloguePath(String skyBuyCataloguePath) {
		this.skyBuyCataloguePath = skyBuyCataloguePath;
	}
	/**
	 * @return the skyBuyWelcomePath
	 */
	public String getSkyBuyWelcomePath() {
		return skyBuyWelcomePath;
	}
	/**
	 * @param skyBuyWelcomePath the skyBuyWelcomePath to set
	 */
	public void setSkyBuyWelcomePath(String skyBuyWelcomePath) {
		this.skyBuyWelcomePath = skyBuyWelcomePath;
	}
	/**
	 * @return the uploadType
	 */
	public String getUploadType() {
		return uploadType;
	}
	/**
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	
}
