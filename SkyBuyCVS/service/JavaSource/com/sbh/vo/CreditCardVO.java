/**
 * 
 */
package com.sbh.vo;

import java.io.Serializable;

/**
 * @author Thapovan
 *
 */
public class CreditCardVO implements Serializable{
	private String yearCode;
	private String yearValue;
	/**
	 * @return the yearCode
	 */
	public String getYearCode() {
		return yearCode;
	}
	/**
	 * @param yearCode the yearCode to set
	 */
	public void setYearCode(String yearCode) {
		this.yearCode = yearCode;
	}
	/**
	 * @return the yearValue
	 */
	public String getYearValue() {
		return yearValue;
	}
	/**
	 * @param yearValue the yearValue to set
	 */
	public void setYearValue(String yearValue) {
		this.yearValue = yearValue;
	}
}
