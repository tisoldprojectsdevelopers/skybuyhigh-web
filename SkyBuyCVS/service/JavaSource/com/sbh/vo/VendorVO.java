package com.sbh.vo;

import java.io.Serializable;

public class VendorVO implements Serializable{
	
	private String vendId;
	private String vendName;
	
	/**
	 * @return the vendName
	 */
	public String getVendName() {
		return vendName;
	}
	/**
	 * @param vendName the vendName to set
	 */
	public void setVendName(String vendName) {
		this.vendName = vendName;
	}
	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return vendId;
	}
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		this.vendId = vendId;
	}
	

}
