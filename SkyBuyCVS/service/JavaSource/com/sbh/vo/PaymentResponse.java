package com.sbh.vo;

import java.io.Serializable;

public class PaymentResponse implements Serializable {
	private  String R_Time;;
	private String R_Ref;;
	private String R_Approved;;
	private String R_Code;
	private String R_Authresr;;
	private String R_Error;
	private String R_Error_Msg;
	private String R_Error_Code;
	private String R_OrderNum;
	private String R_Message;
	private String R_Score;
	private String R_TDate;
	private String R_AVS;
	private String R_FraudCode;
	private String R_ESD;
	private String R_Tax;
	private String R_Shipping;	
	private String inBoundMsg;
	private String outBoundMsg;
	private long R_TxnRefId;
	
	public long getR_TxnRefId() {
		return R_TxnRefId;
	}
	public void setR_TxnRefId(long txnRefId) {
		R_TxnRefId = txnRefId;
	}
	public String getR_Time() {
		return R_Time;
	}
	public void setR_Time(String time) {
		R_Time = time;
	}
	public String getR_Ref() {
		return R_Ref;
	}
	public void setR_Ref(String ref) {
		R_Ref = ref;
	}
	public String getR_Approved() {
		return R_Approved;
	}
	public void setR_Approved(String approved) {
		R_Approved = approved;
	}
	public String getR_Code() {
		return R_Code;
	}
	public void setR_Code(String code) {
		R_Code = code;
	}
	public String getR_Authresr() {
		return R_Authresr;
	}
	public void setR_Authresr(String authresr) {
		R_Authresr = authresr;
	}
	public String getR_Error() {
		return R_Error;
	}
	public void setR_Error(String error) {
		R_Error = error;
	}
	public String getR_OrderNum() {
		return R_OrderNum;
	}
	public void setR_OrderNum(String orderNum) {
		R_OrderNum = orderNum;
	}
	public String getR_Message() {
		return R_Message;
	}
	public void setR_Message(String message) {
		R_Message = message;
	}
	public String getR_Score() {
		return R_Score;
	}
	public void setR_Score(String score) {
		R_Score = score;
	}
	public String getR_TDate() {
		return R_TDate;
	}
	public void setR_TDate(String date) {
		R_TDate = date;
	}
	public String getR_AVS() {
		return R_AVS;
	}
	public void setR_AVS(String r_avs) {
		R_AVS = r_avs;
	}
	public String getR_FraudCode() {
		return R_FraudCode;
	}
	public void setR_FraudCode(String fraudCode) {
		R_FraudCode = fraudCode;
	}
	public String getR_ESD() {
		return R_ESD;
	}
	public void setR_ESD(String r_esd) {
		R_ESD = r_esd;
	}
	public String getR_Tax() {
		return R_Tax;
	}
	public void setR_Tax(String tax) {
		R_Tax = tax;
	}
	public String getR_Shipping() {
		return R_Shipping;
	}
	public void setR_Shipping(String shipping) {
		R_Shipping = shipping;
	}
	public String getR_Error_Msg() {
		return R_Error_Msg;
	}
	public void setR_Error_Msg(String error_Msg) {
		R_Error_Msg = error_Msg;
	}
	public String getR_Error_Code() {
		return R_Error_Code;
	}
	public void setR_Error_Code(String error_Code) {
		R_Error_Code = error_Code;
	}
	public String getInBoundMsg() {
		return inBoundMsg;
	}
	public void setInBoundMsg(String inBoundMsg) {
		this.inBoundMsg = inBoundMsg;
	}
	public String getOutBoundMsg() {
		return outBoundMsg;
	}
	public void setOutBoundMsg(String outBoundMsg) {
		this.outBoundMsg = outBoundMsg;
	}

}
