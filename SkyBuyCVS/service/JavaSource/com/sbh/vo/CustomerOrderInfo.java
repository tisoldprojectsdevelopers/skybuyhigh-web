package com.sbh.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomerOrderInfo implements Serializable{
	
	/**
	 * Serial Version User Id.
	 */
	private static final long serialVersionUID = 1L;
	private String taxexempt;
	private String orderType;
	private String paymentTxnRefId;
	private String orderConfirmationNo;
	private String storeNumber;
	private String cardNumber;
	private String cardExpMonth;
	private String cardExpYear;
	private String addrNum;
	private String zip;
	private String chargeTotal;
	private String billingCustName;
	private String billingAddr1;
    private String billingAddr2;
    private String billingCity;
    private String billingState;
    private String billingCountry;
    private String billingPhone;
    private String billingEmail;
    private String shippingCustName;
	private String shippingAddr1;
    private String shippingAddr2;
    private String shippingCity;
    private String shippingState;
    private String shippingCountry;
    private String shippingZip;
    private String ipAddress;
    private String cvv;
    private String custSpecialInstruction;
	
    private ArrayList<OrderItemDetails>  orderItems;
    private OrderItemDetails orderItemDetails;
	public String getTaxexempt() {
		return taxexempt;
	}
	public void setTaxexempt(String taxexempt) {
		this.taxexempt = taxexempt;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardExpMonth() {
		return cardExpMonth;
	}
	public void setCardExpMonth(String cardExpMonth) {
		this.cardExpMonth = cardExpMonth;
	}
	public String getCardExpYear() {
		return cardExpYear;
	}
	public void setCardExpYear(String cardExpYear) {
		this.cardExpYear = cardExpYear;
	}
	public String getAddrNum() {
		return addrNum;
	}
	public void setAddrNum(String addrNum) {
		this.addrNum = addrNum;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getChargeTotal() {
		return chargeTotal;
	}
	public void setChargeTotal(String chargeTotal) {
		this.chargeTotal = chargeTotal;
	}
	public String getBillingCustName() {
		return billingCustName;
	}
	public void setBillingCustName(String billingCustName) {
		this.billingCustName = billingCustName;
	}
	public String getBillingAddr1() {
		return billingAddr1;
	}
	public void setBillingAddr1(String billingAddr1) {
		this.billingAddr1 = billingAddr1;
	}
	public String getBillingAddr2() {
		return billingAddr2;
	}
	public void setBillingAddr2(String billingAddr2) {
		this.billingAddr2 = billingAddr2;
	}
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getBillingPhone() {
		return billingPhone;
	}
	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	public String getShippingCustName() {
		return shippingCustName;
	}
	public void setShippingCustName(String shippingCustName) {
		this.shippingCustName = shippingCustName;
	}
	public String getShippingAddr1() {
		return shippingAddr1;
	}
	public void setShippingAddr1(String shippingAddr1) {
		this.shippingAddr1 = shippingAddr1;
	}
	public String getShippingAddr2() {
		return shippingAddr2;
	}
	public void setShippingAddr2(String shippingAddr2) {
		this.shippingAddr2 = shippingAddr2;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getShippingZip() {
		return shippingZip;
	}
	public void setShippingZip(String shippingZip) {
		this.shippingZip = shippingZip;
	}
	public ArrayList<OrderItemDetails> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(ArrayList<OrderItemDetails> orderItems) {
		this.orderItems = orderItems;
	}
	public OrderItemDetails getOrderItemDetails() {
		return orderItemDetails;
	}
	public void setOrderItemDetails(OrderItemDetails orderItemDetails) {
		this.orderItemDetails = orderItemDetails;
	}
	/**
	 * @return the paymentTxnRefId
	 */
	public String getPaymentTxnRefId() {
		return paymentTxnRefId;
	}
	/**
	 * @param paymentTxnRefId the paymentTxnRefId to set
	 */
	public void setPaymentTxnRefId(String paymentTxnRefId) {
		this.paymentTxnRefId = paymentTxnRefId;
	}
	/**
	 * @return the orderConfirmationNo
	 */
	public String getOrderConfirmationNo() {
		return orderConfirmationNo;
	}
	/**
	 * @param orderConfirmationNo the orderConfirmationNo to set
	 */
	public void setOrderConfirmationNo(String orderConfirmationNo) {
		this.orderConfirmationNo = orderConfirmationNo;
	}
	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	/**
	 * @return the custSpecialInstruction
	 */
	public String getCustSpecialInstruction() {
		return custSpecialInstruction;
	}
	/**
	 * @param custSpecialInstruction the custSpecialInstruction to set
	 */
	public void setCustSpecialInstruction(String custSpecialInstruction) {
		this.custSpecialInstruction = custSpecialInstruction;
	}
	

}
