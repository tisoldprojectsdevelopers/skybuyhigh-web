package com.sbh.vo;

import java.io.Serializable;

public class DeviceRegVO implements Serializable{
	
	/**
	 * Serial Version User Id
	 */
	private static final long serialVersionUID = 1L;
	private String deviceId;
	private String airId;
	private String airUserId;
	private String airName;
	private String deviceType;
	private String deviceSerialNo;
	private String macAddr;
	private String macDesc;
	private String devicePassword;
	private String deviceModel;
	private String processorId;
	private String flightNo;
	private String productKey;
	private String status;
	private String isActive;
	private String deviceCode;
	private String productNo;
	private String adminVersion;
	private String catalogueVersion;
	
	public String getDeviceCode() {
		return deviceCode;
	}
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getProductKey() {
		return productKey;
	}
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * @return the deviceSerialNo
	 */
	public String getDeviceSerialNo() {
		return deviceSerialNo;
	}
	/**
	 * @param deviceSerialNo the deviceSerialNo to set
	 */
	public void setDeviceSerialNo(String deviceSerialNo) {
		this.deviceSerialNo = deviceSerialNo;
	}	
	
	
	/**
	 * @return the devicePassword
	 */
	public String getDevicePassword() {
		return devicePassword;
	}
	/**
	 * @param devicePassword the devicePassword to set
	 */
	public void setDevicePassword(String devicePassword) {
		this.devicePassword = devicePassword;
	}
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	
	/**
	 * @return the macAddr
	 */
	public String getMacAddr() {
		return macAddr;
	}
	/**
	 * @param macAddr the macAddr to set
	 */
	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}
	/**
	 * @return the macDesc
	 */
	public String getMacDesc() {
		return macDesc;
	}
	/**
	 * @param macDesc the macDesc to set
	 */
	public void setMacDesc(String macDesc) {
		this.macDesc = macDesc;
	}
	/**
	 * @return the processorId
	 */
	public String getProcessorId() {
		return processorId;
	}
	/**
	 * @param processorId the processorId to set
	 */
	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the productNo
	 */
	public String getProductNo() {
		return productNo;
	}
	/**
	 * @param productNo the productNo to set
	 */
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	/**
	 * @return the adminVersion
	 */
	public String getAdminVersion() {
		return adminVersion;
	}
	/**
	 * @param adminVersion the adminVersion to set
	 */
	public void setAdminVersion(String adminVersion) {
		this.adminVersion = adminVersion;
	}
	/**
	 * @return the catalogueVersion
	 */
	public String getCatalogueVersion() {
		return catalogueVersion;
	}
	/**
	 * @param catalogueVersion the catalogueVersion to set
	 */
	public void setCatalogueVersion(String catalogueVersion) {
		this.catalogueVersion = catalogueVersion;
	}
	/**
	 * @return the airUserId
	 */
	public String getAirUserId() {
		return airUserId;
	}
	/**
	 * @param airUserId the airUserId to set
	 */
	public void setAirUserId(String airUserId) {
		this.airUserId = airUserId;
	}

}
