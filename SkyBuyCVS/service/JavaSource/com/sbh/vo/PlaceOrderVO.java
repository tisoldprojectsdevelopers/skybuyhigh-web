package com.sbh.vo;

import java.io.Serializable;

public class PlaceOrderVO implements Serializable {
	
	private String custId;
	private String custTransId;
	private String custBillFname;
	private String custBillLname;
	private String custBillEmail;
	private String custPhone;
	private String custBillAddr1;
	private String custBillAddr2;
	private String custBillCity;
	private String custBillState;
	private String custBillCountry;
	private String custBillZip;
	private String custShipFname;
	private String custShipLname;
	private String custShipEmail;
	private String custShipAddr1;
	private String custShipAddr2;
	private String custShipCity;
	private String custShipState;
	private String custShipCountry;
	private String custShipZip;
	private String custStatus;
	private String OrderId;
	private String airCode;
	private String processorId;
	private String OrderDt;
	private String OrderStatus;
	private String orderFullFilldt;
	private String prodeCode;
	private String cateId;
	private String vendId;
	private String ProdId;
	private String status;
	private String qty;
	private String vendPrice;
	private String sbhPrice;
	private String ccNo;
	private String cardFname;
	private String cardLname;
	private String cardType;
	private String cardExpDt;
	private String payAmount;
	private String chargeDt;
	private String payStatus;
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return custTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		this.custTransId = custTransId;
	}
	/**
	 * @return the custBillFname
	 */
	public String getCustBillFname() {
		return custBillFname;
	}
	/**
	 * @param custBillFname the custBillFname to set
	 */
	public void setCustBillFname(String custBillFname) {
		this.custBillFname = custBillFname;
	}
	/**
	 * @return the custBillLname
	 */
	public String getCustBillLname() {
		return custBillLname;
	}
	/**
	 * @param custBillLname the custBillLname to set
	 */
	public void setCustBillLname(String custBillLname) {
		this.custBillLname = custBillLname;
	}
	/**
	 * @return the custBillEmail
	 */
	public String getCustBillEmail() {
		return custBillEmail;
	}
	/**
	 * @param custBillEmail the custBillEmail to set
	 */
	public void setCustBillEmail(String custBillEmail) {
		this.custBillEmail = custBillEmail;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custBillAddr1
	 */
	public String getCustBillAddr1() {
		return custBillAddr1;
	}
	/**
	 * @param custBillAddr1 the custBillAddr1 to set
	 */
	public void setCustBillAddr1(String custBillAddr1) {
		this.custBillAddr1 = custBillAddr1;
	}
	/**
	 * @return the custBillAddr2
	 */
	public String getCustBillAddr2() {
		return custBillAddr2;
	}
	/**
	 * @param custBillAddr2 the custBillAddr2 to set
	 */
	public void setCustBillAddr2(String custBillAddr2) {
		this.custBillAddr2 = custBillAddr2;
	}
	/**
	 * @return the custBillCity
	 */
	public String getCustBillCity() {
		return custBillCity;
	}
	/**
	 * @param custBillCity the custBillCity to set
	 */
	public void setCustBillCity(String custBillCity) {
		this.custBillCity = custBillCity;
	}
	/**
	 * @return the custBillState
	 */
	public String getCustBillState() {
		return custBillState;
	}
	/**
	 * @param custBillState the custBillState to set
	 */
	public void setCustBillState(String custBillState) {
		this.custBillState = custBillState;
	}
	/**
	 * @return the custBillCountry
	 */
	public String getCustBillCountry() {
		return custBillCountry;
	}
	/**
	 * @param custBillCountry the custBillCountry to set
	 */
	public void setCustBillCountry(String custBillCountry) {
		this.custBillCountry = custBillCountry;
	}
	/**
	 * @return the custBillZip
	 */
	public String getCustBillZip() {
		return custBillZip;
	}
	/**
	 * @param custBillZip the custBillZip to set
	 */
	public void setCustBillZip(String custBillZip) {
		this.custBillZip = custBillZip;
	}
	/**
	 * @return the custShipFname
	 */
	public String getCustShipFname() {
		return custShipFname;
	}
	/**
	 * @param custShipFname the custShipFname to set
	 */
	public void setCustShipFname(String custShipFname) {
		this.custShipFname = custShipFname;
	}
	/**
	 * @return the custShipLname
	 */
	public String getCustShipLname() {
		return custShipLname;
	}
	/**
	 * @param custShipLname the custShipLname to set
	 */
	public void setCustShipLname(String custShipLname) {
		this.custShipLname = custShipLname;
	}
	/**
	 * @return the custShipEmail
	 */
	public String getCustShipEmail() {
		return custShipEmail;
	}
	/**
	 * @param custShipEmail the custShipEmail to set
	 */
	public void setCustShipEmail(String custShipEmail) {
		this.custShipEmail = custShipEmail;
	}
	/**
	 * @return the custShipAddr1
	 */
	public String getCustShipAddr1() {
		return custShipAddr1;
	}
	/**
	 * @param custShipAddr1 the custShipAddr1 to set
	 */
	public void setCustShipAddr1(String custShipAddr1) {
		this.custShipAddr1 = custShipAddr1;
	}
	/**
	 * @return the custShipAddr2
	 */
	public String getCustShipAddr2() {
		return custShipAddr2;
	}
	/**
	 * @param custShipAddr2 the custShipAddr2 to set
	 */
	public void setCustShipAddr2(String custShipAddr2) {
		this.custShipAddr2 = custShipAddr2;
	}
	/**
	 * @return the custShipCity
	 */
	public String getCustShipCity() {
		return custShipCity;
	}
	/**
	 * @param custShipCity the custShipCity to set
	 */
	public void setCustShipCity(String custShipCity) {
		this.custShipCity = custShipCity;
	}
	/**
	 * @return the custShipState
	 */
	public String getCustShipState() {
		return custShipState;
	}
	/**
	 * @param custShipState the custShipState to set
	 */
	public void setCustShipState(String custShipState) {
		this.custShipState = custShipState;
	}
	/**
	 * @return the custShipCountry
	 */
	public String getCustShipCountry() {
		return custShipCountry;
	}
	/**
	 * @param custShipCountry the custShipCountry to set
	 */
	public void setCustShipCountry(String custShipCountry) {
		this.custShipCountry = custShipCountry;
	}
	/**
	 * @return the custShipZip
	 */
	public String getCustShipZip() {
		return custShipZip;
	}
	/**
	 * @param custShipZip the custShipZip to set
	 */
	public void setCustShipZip(String custShipZip) {
		this.custShipZip = custShipZip;
	}
	/**
	 * @return the custStatus
	 */
	public String getCustStatus() {
		return custStatus;
	}
	/**
	 * @param custStatus the custStatus to set
	 */
	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return OrderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	/**
	 * @return the airCode
	 */
	public String getAirCode() {
		return airCode;
	}
	/**
	 * @param airCode the airCode to set
	 */
	public void setAirCode(String airCode) {
		this.airCode = airCode;
	}
	/**
	 * @return the processorId
	 */
	public String getProcessorId() {
		return processorId;
	}
	/**
	 * @param processorId the processorId to set
	 */
	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}
	/**
	 * @return the orderDt
	 */
	public String getOrderDt() {
		return OrderDt;
	}
	/**
	 * @param orderDt the orderDt to set
	 */
	public void setOrderDt(String orderDt) {
		OrderDt = orderDt;
	}
	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return OrderStatus;
	}
	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	/**
	 * @return the orderFullFilldt
	 */
	public String getOrderFullFilldt() {
		return orderFullFilldt;
	}
	/**
	 * @param orderFullFilldt the orderFullFilldt to set
	 */
	public void setOrderFullFilldt(String orderFullFilldt) {
		this.orderFullFilldt = orderFullFilldt;
	}
	/**
	 * @return the prodeCode
	 */
	public String getProdeCode() {
		return prodeCode;
	}
	/**
	 * @param prodeCode the prodeCode to set
	 */
	public void setProdeCode(String prodeCode) {
		this.prodeCode = prodeCode;
	}
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return cateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		this.cateId = cateId;
	}
	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return vendId;
	}
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		this.vendId = vendId;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return ProdId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		ProdId = prodId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return vendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		this.vendPrice = vendPrice;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return sbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		this.sbhPrice = sbhPrice;
	}
	/**
	 * @return the ccNo
	 */
	public String getCcNo() {
		return ccNo;
	}
	/**
	 * @param ccNo the ccNo to set
	 */
	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}
	/**
	 * @return the cardFname
	 */
	public String getCardFname() {
		return cardFname;
	}
	/**
	 * @param cardFname the cardFname to set
	 */
	public void setCardFname(String cardFname) {
		this.cardFname = cardFname;
	}
	/**
	 * @return the cardLname
	 */
	public String getCardLname() {
		return cardLname;
	}
	/**
	 * @param cardLname the cardLname to set
	 */
	public void setCardLname(String cardLname) {
		this.cardLname = cardLname;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the cardExpDt
	 */
	public String getCardExpDt() {
		return cardExpDt;
	}
	/**
	 * @param cardExpDt the cardExpDt to set
	 */
	public void setCardExpDt(String cardExpDt) {
		this.cardExpDt = cardExpDt;
	}
	/**
	 * @return the payAmount
	 */
	public String getPayAmount() {
		return payAmount;
	}
	/**
	 * @param payAmount the payAmount to set
	 */
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	/**
	 * @return the chargeDt
	 */
	public String getChargeDt() {
		return chargeDt;
	}
	/**
	 * @param chargeDt the chargeDt to set
	 */
	public void setChargeDt(String chargeDt) {
		this.chargeDt = chargeDt;
	}
	/**
	 * @return the payStatus
	 */
	public String getPayStatus() {
		return payStatus;
	}
	/**
	 * @param payStatus the payStatus to set
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	

}
