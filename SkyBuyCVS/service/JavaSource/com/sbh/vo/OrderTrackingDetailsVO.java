/**
 * 
 */
package com.sbh.vo;

import java.io.Serializable;

/**
 * @author Thapovan
 *
 */
public class OrderTrackingDetailsVO  implements Serializable{
	private String trackingId;
	private String trackingDetails;
	private String create_dt;
	private String orderItemId;
	private String shipmentStatus;
	
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the trackingId
	 */
	public String getTrackingId() {
		return trackingId;
	}
	/**
	 * @param trackingId the trackingId to set
	 */
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}
	/**
	 * @return the trackingDetails
	 */
	public String getTrackingDetails() {
		return trackingDetails;
	}
	/**
	 * @param trackingDetails the trackingDetails to set
	 */
	public void setTrackingDetails(String trackingDetails) {
		this.trackingDetails = trackingDetails;
	}
	/**
	 * @return the create_dt
	 */
	public String getCreate_dt() {
		return create_dt;
	}
	/**
	 * @param create_dt the create_dt to set
	 */
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	/**
	 * @return the shipmentStatus
	 */
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	/**
	 * @param shipmentStatus the shipmentStatus to set
	 */
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
}
