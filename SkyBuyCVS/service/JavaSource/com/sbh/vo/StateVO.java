package com.sbh.vo;

import java.io.Serializable;

public class StateVO implements Serializable{
	
	private String stateCode;
	private String StateName;
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getStateName() {
		return StateName;
	}
	public void setStateName(String stateName) {
		StateName = stateName;
	}

}