package com.sbh.vo;

import java.io.Serializable;

public class CustomerSuggestionsVO implements Serializable{
	private String orderId;
	private String custId;
	private String orderConfirmationNo;
	private String orderDate;
	private String custPhone;
	private String billingEmail;
	private String billingFName;
	private String billingLName;
	private String billingAddr1;
	private String billingAddr2;
	private String billingCity;
	private String billingState;
	private String billingCountry;
	private String billingZip;
	private String shippingZip;
	private String shippingEmail;
	private String shippingFName;
	private String shippingLName;
	private String shippingAddr1;
	private String shippingAddr2;
	private String shippingPhone;
	private String shippingCity;
	private String shippingState;
	private String shippingCountry;
	private String custSuggestions;
	private String custFeedback;
	private String flightNo;
	private String airlineName;
	
	public String getShippingPhone() {
		return shippingPhone;
	}
	public void setShippingPhone(String shippingPhone) {
		this.shippingPhone = shippingPhone;
	}
	/**
	 * @return the airlineName
	 */
	public String getAirlineName() {
		return airlineName;
	}
	/**
	 * @param airlineName the airlineName to set
	 */
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the custSuggestions
	 */
	public String getCustSuggestions() {
		return custSuggestions;
	}
	/**
	 * @param custSuggestions the custSuggestions to set
	 */
	public void setCustSuggestions(String custSuggestions) {
		this.custSuggestions = custSuggestions;
	}
	/**
	 * @return the custFeedback
	 */
	public String getCustFeedback() {
		return custFeedback;
	}
	/**
	 * @param custFeedback the custFeedback to set
	 */
	public void setCustFeedback(String custFeedback) {
		this.custFeedback = custFeedback;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the orderConfirmationNo
	 */
	public String getOrderConfirmationNo() {
		return orderConfirmationNo;
	}
	/**
	 * @param orderConfirmationNo the orderConfirmationNo to set
	 */
	public void setOrderConfirmationNo(String orderConfirmationNo) {
		this.orderConfirmationNo = orderConfirmationNo;
	}
	/**
	 * @return the orderDate
	 */
	public String getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the billingEmail
	 */
	public String getBillingEmail() {
		return billingEmail;
	}
	/**
	 * @param billingEmail the billingEmail to set
	 */
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	/**
	 * @return the billingFName
	 */
	public String getBillingFName() {
		return billingFName;
	}
	/**
	 * @param billingFName the billingFName to set
	 */
	public void setBillingFName(String billingFName) {
		this.billingFName = billingFName;
	}
	/**
	 * @return the billingLName
	 */
	public String getBillingLName() {
		return billingLName;
	}
	/**
	 * @param billingLName the billingLName to set
	 */
	public void setBillingLName(String billingLName) {
		this.billingLName = billingLName;
	}
	/**
	 * @return the billingAddr1
	 */
	public String getBillingAddr1() {
		return billingAddr1;
	}
	/**
	 * @param billingAddr1 the billingAddr1 to set
	 */
	public void setBillingAddr1(String billingAddr1) {
		this.billingAddr1 = billingAddr1;
	}
	/**
	 * @return the billingAddr2
	 */
	public String getBillingAddr2() {
		return billingAddr2;
	}
	/**
	 * @param billingAddr2 the billingAddr2 to set
	 */
	public void setBillingAddr2(String billingAddr2) {
		this.billingAddr2 = billingAddr2;
	}
	/**
	 * @return the billingCity
	 */
	public String getBillingCity() {
		return billingCity;
	}
	/**
	 * @param billingCity the billingCity to set
	 */
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	/**
	 * @return the billingState
	 */
	public String getBillingState() {
		return billingState;
	}
	/**
	 * @param billingState the billingState to set
	 */
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	/**
	 * @return the billingCountry
	 */
	public String getBillingCountry() {
		return billingCountry;
	}
	/**
	 * @param billingCountry the billingCountry to set
	 */
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	/**
	 * @return the shippingEmail
	 */
	public String getShippingEmail() {
		return shippingEmail;
	}
	/**
	 * @param shippingEmail the shippingEmail to set
	 */
	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}
	/**
	 * @return the shippingFName
	 */
	public String getShippingFName() {
		return shippingFName;
	}
	/**
	 * @param shippingFName the shippingFName to set
	 */
	public void setShippingFName(String shippingFName) {
		this.shippingFName = shippingFName;
	}
	/**
	 * @return the shippingLName
	 */
	public String getShippingLName() {
		return shippingLName;
	}
	/**
	 * @param shippingLName the shippingLName to set
	 */
	public void setShippingLName(String shippingLName) {
		this.shippingLName = shippingLName;
	}
	/**
	 * @return the shippingAddr1
	 */
	public String getShippingAddr1() {
		return shippingAddr1;
	}
	/**
	 * @param shippingAddr1 the shippingAddr1 to set
	 */
	public void setShippingAddr1(String shippingAddr1) {
		this.shippingAddr1 = shippingAddr1;
	}
	/**
	 * @return the shippingAddr2
	 */
	public String getShippingAddr2() {
		return shippingAddr2;
	}
	/**
	 * @param shippingAddr2 the shippingAddr2 to set
	 */
	public void setShippingAddr2(String shippingAddr2) {
		this.shippingAddr2 = shippingAddr2;
	}
	/**
	 * @return the shippingCity
	 */
	public String getShippingCity() {
		return shippingCity;
	}
	/**
	 * @param shippingCity the shippingCity to set
	 */
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	/**
	 * @return the shippingState
	 */
	public String getShippingState() {
		return shippingState;
	}
	/**
	 * @param shippingState the shippingState to set
	 */
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	/**
	 * @return the shippingCountry
	 */
	public String getShippingCountry() {
		return shippingCountry;
	}
	/**
	 * @param shippingCountry the shippingCountry to set
	 */
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the billingZip
	 */
	public String getBillingZip() {
		return billingZip;
	}
	/**
	 * @param billingZip the billingZip to set
	 */
	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}
	/**
	 * @return the shippingZip
	 */
	public String getShippingZip() {
		return shippingZip;
	}
	/**
	 * @param shippingZip the shippingZip to set
	 */
	public void setShippingZip(String shippingZip) {
		this.shippingZip = shippingZip;
	}
	
	

}
