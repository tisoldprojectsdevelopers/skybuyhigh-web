package com.sbh.vo;

import java.io.Serializable;

public class DeviceDetailsVO implements Serializable{
	
	/**
	 * Serial Version User Id.
	 */
	private static final long serialVersionUID = 1L;
	
	private String deviceRegId;
	private String deviceRegActivity;
	private String deviceRegAirId;
	private String deviceRegAirName;
	private String deviceRegDeviceId;
	private String deviceRegMacAddress;
	private String deviceRegUpdateDate;
	private String deviceRegUpdateId;
	
	private String deviceAllocateId;
	private String deviceAllocateActivity;
	private String deviceAllocateAirId;
	private String deviceAllocateAirName;
	private String deviceAllocateDeviceId;
	private String deviceAllocateMacAddress;
	private String deviceAllocateUpdateDate;
	private String deviceAllocateUpdateId;
	
	private String ctlgDownloadId;
	private String ctlgDownloadActivity;
	private String ctlgDownloadAirId;
	private String ctlgDownloadAirName;
	private String ctlgDownloadDeviceId;
	private String ctlgDownloadMacAddress;
	private String ctlgDownloadUpdateDate;
	private String ctlgDownloadUpdateId;
	
	private String orderPlaceId;
	private String orderPlaceActivity;
	private String orderPlaceAirId;
	private String orderPlaceAirName;
	private String orderPlaceDeviceId;
	private String orderPlaceFlightNo;
	private String orderPlaceTotalOrders;
	private String orderPlaceMacAddress;
	private String orderPlaceUpdateDate;
	private String orderPlaceUpdateId;
	
	private String packageDownloadId;
	private String packageDownloadActivity;
	private String packageDownloadType;
	private String packageDownloadAdminVersion;
	private String packageDownloadCatalogueVersion;
	private String packageDownloadDeviceId;
	private String packageDownloadMacAddress;
	private String packageDownloadDate;
	private String packageDownloadUpdateId;
	
	/**
	 * @return the deviceRegId
	 */
	public String getDeviceRegId() {
		return deviceRegId;
	}
	/**
	 * @param deviceRegId the deviceRegId to set
	 */
	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
	/**
	 * @return the deviceRegActivity
	 */
	public String getDeviceRegActivity() {
		return deviceRegActivity;
	}
	/**
	 * @param deviceRegActivity the deviceRegActivity to set
	 */
	public void setDeviceRegActivity(String deviceRegActivity) {
		this.deviceRegActivity = deviceRegActivity;
	}
	/**
	 * @return the deviceRegAirId
	 */
	public String getDeviceRegAirId() {
		return deviceRegAirId;
	}
	/**
	 * @param deviceRegAirId the deviceRegAirId to set
	 */
	public void setDeviceRegAirId(String deviceRegAirId) {
		this.deviceRegAirId = deviceRegAirId;
	}
	/**
	 * @return the deviceRegDeviceId
	 */
	public String getDeviceRegDeviceId() {
		return deviceRegDeviceId;
	}
	/**
	 * @param deviceRegDeviceId the deviceRegDeviceId to set
	 */
	public void setDeviceRegDeviceId(String deviceRegDeviceId) {
		this.deviceRegDeviceId = deviceRegDeviceId;
	}
	/**
	 * @return the deviceRegMacAddress
	 */
	public String getDeviceRegMacAddress() {
		return deviceRegMacAddress;
	}
	/**
	 * @param deviceRegMacAddress the deviceRegMacAddress to set
	 */
	public void setDeviceRegMacAddress(String deviceRegMacAddress) {
		this.deviceRegMacAddress = deviceRegMacAddress;
	}
	/**
	 * @return the deviceRegUpdateDate
	 */
	public String getDeviceRegUpdateDate() {
		return deviceRegUpdateDate;
	}
	/**
	 * @param deviceRegUpdateDate the deviceRegUpdateDate to set
	 */
	public void setDeviceRegUpdateDate(String deviceRegUpdateDate) {
		this.deviceRegUpdateDate = deviceRegUpdateDate;
	}
	/**
	 * @return the deviceRegUpdateId
	 */
	public String getDeviceRegUpdateId() {
		return deviceRegUpdateId;
	}
	/**
	 * @param deviceRegUpdateId the deviceRegUpdateId to set
	 */
	public void setDeviceRegUpdateId(String deviceRegUpdateId) {
		this.deviceRegUpdateId = deviceRegUpdateId;
	}
	/**
	 * @return the ctlgDownloadId
	 */
	public String getCtlgDownloadId() {
		return ctlgDownloadId;
	}
	/**
	 * @param ctlgDownloadId the ctlgDownloadId to set
	 */
	public void setCtlgDownloadId(String ctlgDownloadId) {
		this.ctlgDownloadId = ctlgDownloadId;
	}
	/**
	 * @return the ctlgDownloadActivity
	 */
	public String getCtlgDownloadActivity() {
		return ctlgDownloadActivity;
	}
	/**
	 * @param ctlgDownloadActivity the ctlgDownloadActivity to set
	 */
	public void setCtlgDownloadActivity(String ctlgDownloadActivity) {
		this.ctlgDownloadActivity = ctlgDownloadActivity;
	}
	/**
	 * @return the ctlgDownloadAirId
	 */
	public String getCtlgDownloadAirId() {
		return ctlgDownloadAirId;
	}
	/**
	 * @param ctlgDownloadAirId the ctlgDownloadAirId to set
	 */
	public void setCtlgDownloadAirId(String ctlgDownloadAirId) {
		this.ctlgDownloadAirId = ctlgDownloadAirId;
	}
	/**
	 * @return the ctlgDownloadDeviceId
	 */
	public String getCtlgDownloadDeviceId() {
		return ctlgDownloadDeviceId;
	}
	/**
	 * @param ctlgDownloadDeviceId the ctlgDownloadDeviceId to set
	 */
	public void setCtlgDownloadDeviceId(String ctlgDownloadDeviceId) {
		this.ctlgDownloadDeviceId = ctlgDownloadDeviceId;
	}
	/**
	 * @return the ctlgDownloadMacAddress
	 */
	public String getCtlgDownloadMacAddress() {
		return ctlgDownloadMacAddress;
	}
	/**
	 * @param ctlgDownloadMacAddress the ctlgDownloadMacAddress to set
	 */
	public void setCtlgDownloadMacAddress(String ctlgDownloadMacAddress) {
		this.ctlgDownloadMacAddress = ctlgDownloadMacAddress;
	}
	/**
	 * @return the ctlgDownloadUpdateDate
	 */
	public String getCtlgDownloadUpdateDate() {
		return ctlgDownloadUpdateDate;
	}
	/**
	 * @param ctlgDownloadUpdateDate the ctlgDownloadUpdateDate to set
	 */
	public void setCtlgDownloadUpdateDate(String ctlgDownloadUpdateDate) {
		this.ctlgDownloadUpdateDate = ctlgDownloadUpdateDate;
	}
	/**
	 * @return the ctlgDownloadUpdateId
	 */
	public String getCtlgDownloadUpdateId() {
		return ctlgDownloadUpdateId;
	}
	/**
	 * @param ctlgDownloadUpdateId the ctlgDownloadUpdateId to set
	 */
	public void setCtlgDownloadUpdateId(String ctlgDownloadUpdateId) {
		this.ctlgDownloadUpdateId = ctlgDownloadUpdateId;
	}
	/**
	 * @return the orderPlaceId
	 */
	public String getOrderPlaceId() {
		return orderPlaceId;
	}
	/**
	 * @param orderPlaceId the orderPlaceId to set
	 */
	public void setOrderPlaceId(String orderPlaceId) {
		this.orderPlaceId = orderPlaceId;
	}
	/**
	 * @return the orderPlaceActivity
	 */
	public String getOrderPlaceActivity() {
		return orderPlaceActivity;
	}
	/**
	 * @param orderPlaceActivity the orderPlaceActivity to set
	 */
	public void setOrderPlaceActivity(String orderPlaceActivity) {
		this.orderPlaceActivity = orderPlaceActivity;
	}
	/**
	 * @return the orderPlaceAirId
	 */
	public String getOrderPlaceAirId() {
		return orderPlaceAirId;
	}
	/**
	 * @param orderPlaceAirId the orderPlaceAirId to set
	 */
	public void setOrderPlaceAirId(String orderPlaceAirId) {
		this.orderPlaceAirId = orderPlaceAirId;
	}
	/**
	 * @return the orderPlaceDeviceId
	 */
	public String getOrderPlaceDeviceId() {
		return orderPlaceDeviceId;
	}
	/**
	 * @param orderPlaceDeviceId the orderPlaceDeviceId to set
	 */
	public void setOrderPlaceDeviceId(String orderPlaceDeviceId) {
		this.orderPlaceDeviceId = orderPlaceDeviceId;
	}
	/**
	 * @return the orderPlaceFlightNo
	 */
	public String getOrderPlaceFlightNo() {
		return orderPlaceFlightNo;
	}
	/**
	 * @param orderPlaceFlightNo the orderPlaceFlightNo to set
	 */
	public void setOrderPlaceFlightNo(String orderPlaceFlightNo) {
		this.orderPlaceFlightNo = orderPlaceFlightNo;
	}
	/**
	 * @return the orderPlaceTotalOrders
	 */
	public String getOrderPlaceTotalOrders() {
		return orderPlaceTotalOrders;
	}
	/**
	 * @param orderPlaceTotalOrders the orderPlaceTotalOrders to set
	 */
	public void setOrderPlaceTotalOrders(String orderPlaceTotalOrders) {
		this.orderPlaceTotalOrders = orderPlaceTotalOrders;
	}
	/**
	 * @return the orderPlaceMacAddress
	 */
	public String getOrderPlaceMacAddress() {
		return orderPlaceMacAddress;
	}
	/**
	 * @param orderPlaceMacAddress the orderPlaceMacAddress to set
	 */
	public void setOrderPlaceMacAddress(String orderPlaceMacAddress) {
		this.orderPlaceMacAddress = orderPlaceMacAddress;
	}
	/**
	 * @return the orderPlaceUpdateDate
	 */
	public String getOrderPlaceUpdateDate() {
		return orderPlaceUpdateDate;
	}
	/**
	 * @param orderPlaceUpdateDate the orderPlaceUpdateDate to set
	 */
	public void setOrderPlaceUpdateDate(String orderPlaceUpdateDate) {
		this.orderPlaceUpdateDate = orderPlaceUpdateDate;
	}
	/**
	 * @return the orderPlaceUpdateId
	 */
	public String getOrderPlaceUpdateId() {
		return orderPlaceUpdateId;
	}
	/**
	 * @param orderPlaceUpdateId the orderPlaceUpdateId to set
	 */
	public void setOrderPlaceUpdateId(String orderPlaceUpdateId) {
		this.orderPlaceUpdateId = orderPlaceUpdateId;
	}
	/**
	 * @return the packageDownloadId
	 */
	public String getPackageDownloadId() {
		return packageDownloadId;
	}
	/**
	 * @param packageDownloadId the packageDownloadId to set
	 */
	public void setPackageDownloadId(String packageDownloadId) {
		this.packageDownloadId = packageDownloadId;
	}
	/**
	 * @return the packageDownloadActivity
	 */
	public String getPackageDownloadActivity() {
		return packageDownloadActivity;
	}
	/**
	 * @param packageDownloadActivity the packageDownloadActivity to set
	 */
	public void setPackageDownloadActivity(String packageDownloadActivity) {
		this.packageDownloadActivity = packageDownloadActivity;
	}
	/**
	 * @return the packageDownloadDeviceId
	 */
	public String getPackageDownloadDeviceId() {
		return packageDownloadDeviceId;
	}
	/**
	 * @param packageDownloadDeviceId the packageDownloadDeviceId to set
	 */
	public void setPackageDownloadDeviceId(String packageDownloadDeviceId) {
		this.packageDownloadDeviceId = packageDownloadDeviceId;
	}
	/**
	 * @return the packageDownloadMacAddress
	 */
	public String getPackageDownloadMacAddress() {
		return packageDownloadMacAddress;
	}
	/**
	 * @param packageDownloadMacAddress the packageDownloadMacAddress to set
	 */
	public void setPackageDownloadMacAddress(String packageDownloadMacAddress) {
		this.packageDownloadMacAddress = packageDownloadMacAddress;
	}
	/**
	 * @return the packageDownloadUpdateId
	 */
	public String getPackageDownloadUpdateId() {
		return packageDownloadUpdateId;
	}
	/**
	 * @param packageDownloadUpdateId the packageDownloadUpdateId to set
	 */
	public void setPackageDownloadUpdateId(String packageDownloadUpdateId) {
		this.packageDownloadUpdateId = packageDownloadUpdateId;
	}
	/**
	 * @return the deviceRegAirName
	 */
	public String getDeviceRegAirName() {
		return deviceRegAirName;
	}
	/**
	 * @param deviceRegAirName the deviceRegAirName to set
	 */
	public void setDeviceRegAirName(String deviceRegAirName) {
		this.deviceRegAirName = deviceRegAirName;
	}
	/**
	 * @return the ctlgDownloadAirName
	 */
	public String getCtlgDownloadAirName() {
		return ctlgDownloadAirName;
	}
	/**
	 * @param ctlgDownloadAirName the ctlgDownloadAirName to set
	 */
	public void setCtlgDownloadAirName(String ctlgDownloadAirName) {
		this.ctlgDownloadAirName = ctlgDownloadAirName;
	}
	/**
	 * @return the orderPlaceAirName
	 */
	public String getOrderPlaceAirName() {
		return orderPlaceAirName;
	}
	/**
	 * @param orderPlaceAirName the orderPlaceAirName to set
	 */
	public void setOrderPlaceAirName(String orderPlaceAirName) {
		this.orderPlaceAirName = orderPlaceAirName;
	}
	/**
	 * @return the packageDownloadAdminVersion
	 */
	public String getPackageDownloadAdminVersion() {
		return packageDownloadAdminVersion;
	}
	/**
	 * @param packageDownloadAdminVersion the packageDownloadAdminVersion to set
	 */
	public void setPackageDownloadAdminVersion(String packageDownloadAdminVersion) {
		this.packageDownloadAdminVersion = packageDownloadAdminVersion;
	}
	/**
	 * @return the packageDownloadCatalogueVersion
	 */
	public String getPackageDownloadCatalogueVersion() {
		return packageDownloadCatalogueVersion;
	}
	/**
	 * @param packageDownloadCatalogueVersion the packageDownloadCatalogueVersion to set
	 */
	public void setPackageDownloadCatalogueVersion(
			String packageDownloadCatalogueVersion) {
		this.packageDownloadCatalogueVersion = packageDownloadCatalogueVersion;
	}
	/**
	 * @return the packageDownloadType
	 */
	public String getPackageDownloadType() {
		return packageDownloadType;
	}
	/**
	 * @param packageDownloadType the packageDownloadType to set
	 */
	public void setPackageDownloadType(String packageDownloadType) {
		this.packageDownloadType = packageDownloadType;
	}
	/**
	 * @return the packageDownloadDate
	 */
	public String getPackageDownloadDate() {
		return packageDownloadDate;
	}
	/**
	 * @param packageDownloadDate the packageDownloadDate to set
	 */
	public void setPackageDownloadDate(String packageDownloadDate) {
		this.packageDownloadDate = packageDownloadDate;
	}
	/**
	 * @return the deviceAllocateId
	 */
	public String getDeviceAllocateId() {
		return deviceAllocateId;
	}
	/**
	 * @param deviceAllocateId the deviceAllocateId to set
	 */
	public void setDeviceAllocateId(String deviceAllocateId) {
		this.deviceAllocateId = deviceAllocateId;
	}
	/**
	 * @return the deviceAllocateActivity
	 */
	public String getDeviceAllocateActivity() {
		return deviceAllocateActivity;
	}
	/**
	 * @param deviceAllocateActivity the deviceAllocateActivity to set
	 */
	public void setDeviceAllocateActivity(String deviceAllocateActivity) {
		this.deviceAllocateActivity = deviceAllocateActivity;
	}
	/**
	 * @return the deviceAllocateAirId
	 */
	public String getDeviceAllocateAirId() {
		return deviceAllocateAirId;
	}
	/**
	 * @param deviceAllocateAirId the deviceAllocateAirId to set
	 */
	public void setDeviceAllocateAirId(String deviceAllocateAirId) {
		this.deviceAllocateAirId = deviceAllocateAirId;
	}
	/**
	 * @return the deviceAllocateAirName
	 */
	public String getDeviceAllocateAirName() {
		return deviceAllocateAirName;
	}
	/**
	 * @param deviceAllocateAirName the deviceAllocateAirName to set
	 */
	public void setDeviceAllocateAirName(String deviceAllocateAirName) {
		this.deviceAllocateAirName = deviceAllocateAirName;
	}
	/**
	 * @return the deviceAllocateDeviceId
	 */
	public String getDeviceAllocateDeviceId() {
		return deviceAllocateDeviceId;
	}
	/**
	 * @param deviceAllocateDeviceId the deviceAllocateDeviceId to set
	 */
	public void setDeviceAllocateDeviceId(String deviceAllocateDeviceId) {
		this.deviceAllocateDeviceId = deviceAllocateDeviceId;
	}
	/**
	 * @return the deviceAllocateMacAddress
	 */
	public String getDeviceAllocateMacAddress() {
		return deviceAllocateMacAddress;
	}
	/**
	 * @param deviceAllocateMacAddress the deviceAllocateMacAddress to set
	 */
	public void setDeviceAllocateMacAddress(String deviceAllocateMacAddress) {
		this.deviceAllocateMacAddress = deviceAllocateMacAddress;
	}
	/**
	 * @return the deviceAllocateUpdateDate
	 */
	public String getDeviceAllocateUpdateDate() {
		return deviceAllocateUpdateDate;
	}
	/**
	 * @param deviceAllocateUpdateDate the deviceAllocateUpdateDate to set
	 */
	public void setDeviceAllocateUpdateDate(String deviceAllocateUpdateDate) {
		this.deviceAllocateUpdateDate = deviceAllocateUpdateDate;
	}
	/**
	 * @return the deviceAllocateUpdateId
	 */
	public String getDeviceAllocateUpdateId() {
		return deviceAllocateUpdateId;
	}
	/**
	 * @param deviceAllocateUpdateId the deviceAllocateUpdateId to set
	 */
	public void setDeviceAllocateUpdateId(String deviceAllocateUpdateId) {
		this.deviceAllocateUpdateId = deviceAllocateUpdateId;
	}
}
