package com.sbh.vo;

import java.io.Serializable;

public class DeviceTypesVO implements Serializable{
	
	private String deviceId;
	private String deviceName;
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

}
