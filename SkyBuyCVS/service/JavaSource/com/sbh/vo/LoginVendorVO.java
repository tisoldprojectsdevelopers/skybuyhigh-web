package com.sbh.vo;

import java.io.Serializable;

public class LoginVendorVO implements Serializable{
	
	private String vendUserId;
	private String vendPassword;
	private String vendName;
	private String vendId;
	/**
	 * @return the vendUserId
	 */
	public String getVendUserId() {
		return vendUserId;
	}
	/**
	 * @param vendUserId the vendUserId to set
	 */
	public void setVendUserId(String vendUserId) {
		this.vendUserId = vendUserId;
	}
	/**
	 * @return the vendPassword
	 */
	public String getVendPassword() {
		return vendPassword;
	}
	/**
	 * @param vendPassword the vendPassword to set
	 */
	public void setVendPassword(String vendPassword) {
		this.vendPassword = vendPassword;
	}
	/**
	 * @return the vendName
	 */
	public String getVendName() {
		return vendName;
	}
	/**
	 * @param vendName the vendName to set
	 */
	public void setVendName(String vendName) {
		this.vendName = vendName;
	}
	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return vendId;
	}
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		this.vendId = vendId;
	}
	
	

}
