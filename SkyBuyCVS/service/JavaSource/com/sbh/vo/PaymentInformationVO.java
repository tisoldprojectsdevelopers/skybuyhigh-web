/**
 * 
 */
package com.sbh.vo;

/**
 * @author Thapovan
 *
 */
public class PaymentInformationVO {
	private String orderItemId;
	private String custFirstName;
	private String custLastName;
	private String custEmail;
	private String custPhone;
	private String custAddress1;
	private String custAddress2;
	private String custCity;
	private String custState;
	private String custCountry;
	private String custZip;
	private String cardType;
	private String creditCardNo;
	private String creditCardHolderName;
	private String expMonth;
	private String expYear;
	private String cvv;
	private String maskedCCNo;
	private String creditCardNoLFD; // Credit Card's Last Four Digits.
	private String keyRefId;
	
	
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
	/**
	 * @return the maskedCCNo
	 */
	public String getMaskedCCNo() {
		return maskedCCNo;
	}
	/**
	 * @param maskedCCNo the maskedCCNo to set
	 */
	public void setMaskedCCNo(String maskedCCNo) {
		this.maskedCCNo = maskedCCNo;
	}
	public String getOrderItemId() {
		return orderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}
	/**
	 * @return the custFirstName
	 */
	public String getCustFirstName() {
		return custFirstName;
	}
	/**
	 * @param custFirstName the custFirstName to set
	 */
	public void setCustFirstName(String custFirstName) {
		this.custFirstName = custFirstName;
	}
	/**
	 * @return the custLastName
	 */
	public String getCustLastName() {
		return custLastName;
	}
	/**
	 * @param custLastName the custLastName to set
	 */
	public void setCustLastName(String custLastName) {
		this.custLastName = custLastName;
	}
	/**
	 * @return the custEmail
	 */
	public String getCustEmail() {
		return custEmail;
	}
	/**
	 * @param custEmail the custEmail to set
	 */
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custAddress1
	 */
	public String getCustAddress1() {
		return custAddress1;
	}
	/**
	 * @param custAddress1 the custAddress1 to set
	 */
	public void setCustAddress1(String custAddress1) {
		this.custAddress1 = custAddress1;
	}
	/**
	 * @return the custCity
	 */
	public String getCustCity() {
		return custCity;
	}
	/**
	 * @param custCity the custCity to set
	 */
	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}
	/**
	 * @return the custState
	 */
	public String getCustState() {
		return custState;
	}
	/**
	 * @param custState the custState to set
	 */
	public void setCustState(String custState) {
		this.custState = custState;
	}
	/**
	 * @return the custCountry
	 */
	public String getCustCountry() {
		return custCountry;
	}
	/**
	 * @param custCountry the custCountry to set
	 */
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	/**
	 * @return the custZip
	 */
	public String getCustZip() {
		return custZip;
	}
	/**
	 * @param custZip the custZip to set
	 */
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	/**
	 * @return the creditCardNo
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}
	/**
	 * @param creditCardNo the creditCardNo to set
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}
	/**
	 * @return the creditCardHolderName
	 */
	public String getCreditCardHolderName() {
		return creditCardHolderName;
	}
	/**
	 * @param creditCardHolderName the creditCardHolderName to set
	 */
	public void setCreditCardHolderName(String creditCardHolderName) {
		this.creditCardHolderName = creditCardHolderName;
	}
	/**
	 * @return the expMonth
	 */
	public String getExpMonth() {
		return expMonth;
	}
	/**
	 * @param expMonth the expMonth to set
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}
	/**
	 * @return the expYear
	 */
	public String getExpYear() {
		return expYear;
	}
	/**
	 * @param expYear the expYear to set
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}
	/**
	 * @return the custAddress2
	 */
	public String getCustAddress2() {
		return custAddress2;
	}
	/**
	 * @param custAddress2 the custAddress2 to set
	 */
	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	/**
	 * @return the creditCardNoLFD
	 */
	public String getCreditCardNoLFD() {
		return creditCardNoLFD;
	}
	/**
	 * @param creditCardNoLFD the creditCardNoLFD to set
	 */
	public void setCreditCardNoLFD(String creditCardNoLFD) {
		this.creditCardNoLFD = creditCardNoLFD;
	}
}