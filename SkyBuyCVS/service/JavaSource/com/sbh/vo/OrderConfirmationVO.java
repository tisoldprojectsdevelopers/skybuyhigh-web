package com.sbh.vo;

import java.io.Serializable;

public class OrderConfirmationVO implements Serializable{
	private String orderConfirmationNo;
	private String orderDate;
	private String airlineName;

	/**
	 * @return the orderDate
	 */
	public String getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the airlineName
	 */
	public String getAirlineName() {
		return airlineName;
	}

	/**
	 * @param airlineName the airlineName to set
	 */
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	/**
	 * @return the orderConfirmationNo
	 */
	public String getOrderConfirmationNo() {
		return orderConfirmationNo;
	}

	/**
	 * @param orderConfirmationNo the orderConfirmationNo to set
	 */
	public void setOrderConfirmationNo(String orderConfirmationNo) {
		this.orderConfirmationNo = orderConfirmationNo;
	}
}
