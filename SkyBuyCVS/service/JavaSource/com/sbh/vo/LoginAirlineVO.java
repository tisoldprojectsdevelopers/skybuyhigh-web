package com.sbh.vo;

import java.io.Serializable;

public class LoginAirlineVO implements Serializable{
	
	private String airUserId;
	private String airPassword;
	private String airName;
	private String airId;
	public String getAirUserId() {
		return airUserId;
	}
	public void setAirUserId(String airUserId) {
		this.airUserId = airUserId;
	}
	public String getAirPassword() {
		return airPassword;
	}
	public void setAirPassword(String airPassword) {
		this.airPassword = airPassword;
	}
	public String getAirName() {
		return airName;
	}
	public void setAirName(String airName) {
		this.airName = airName;
	}
	public String getAirId() {
		return airId;
	}
	public void setAirId(String airId) {
		this.airId = airId;
	}
}
