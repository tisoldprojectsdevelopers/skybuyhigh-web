/**
 * 
 */
package com.sbh.vo;

/**
 * @author nbvk
 *
 */
public class KeyVO {
	private String publicKey;
	private String privateKey;
	private String refId;
	private String encryptedData;
	private String decryptedData;
	
	/**
	 * @return the publicKey
	 */
	public String getPublicKey() {
		return publicKey;
	}
	/**
	 * @return the encryptedData
	 */
	public String getEncryptedData() {
		return encryptedData;
	}
	/**
	 * @param encryptedData the encryptedData to set
	 */
	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}
	/**
	 * @return the decryptedData
	 */
	public String getDecryptedData() {
		return decryptedData;
	}
	/**
	 * @param decryptedData the decryptedData to set
	 */
	public void setDecryptedData(String decryptedData) {
		this.decryptedData = decryptedData;
	}
	/**
	 * @param publicKey the publicKey to set
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * @return the privateKey
	 */
	public String getPrivateKey() {
		return privateKey;
	}
	/**
	 * @param privateKey the privateKey to set
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
}
