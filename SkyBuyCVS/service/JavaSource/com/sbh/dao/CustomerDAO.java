/**
 * 
 */
package com.sbh.dao;

import static com.sbh.util.DBConnection.getSQL2005Connection;
import static com.sbh.util.Utils.convertToNameFormat;
import static com.sbh.util.Utils.dateAndTimeStampConversion;
import static com.sbh.util.Utils.dateStampConversion;
import static com.sbh.util.Utils.getPhoneFormat;
import static com.sbh.util.Utils.mergeAddressLine;
import static com.sbh.util.Utils.replaceTagValues;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.actions.CustomerLoginAction;
import com.sbh.email.Email;
import com.sbh.forms.CustomerLoginForm;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.CustomerReturnDetailsVO;
import com.sbh.vo.CustomerSuggestionsVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.OrderConfirmationVO;
import com.sbh.vo.OrderDetailsVO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.OrderReturnDetailsVO;

/**
 * @author Thapovan
 *
 */
public class CustomerDAO {
	
	private static Logger logger = LogManager.getLogger(CustomerLoginAction.class);
	/**
	 * 
	 * @param p_oCustomerLoginForm
	 * @return
	 * @throws Exception
	 */
	public static List<OrderItemDetailsVO> getCustomerOrderItemDetails(CustomerLoginForm p_oCustomerLoginForm) 
		throws Exception {
		
		logger.info("CustomerDAO::getCustomerOrderItemDetails::ENTRY");
		
		double dSbhPrice = 0.00;
		double dQty = 0.00;
		String sQry="{call usp_sbh_get_order_and_cust_info(?)}";
		logger.info("CustomerDAO::getCustomerOrderItemDetails::Query "+sQry);
		logger.info("CustomerDAO::getCustomerOrderItemDetails::Transaction Id "+p_oCustomerLoginForm.getTransactionId());
		String return_policy=null;
		String shipmentStatus = null;
		List<OrderItemDetailsVO> orderItemDetailList = null;
		Connection connection=null;		
		CallableStatement callStatement=null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		ResultSet resultSet=null;
		OrderItemDetailsVO oOrderItemDetailsVO;
	
		try{
			connection = DBConnection.getSQL2005Connection();
			callStatement=connection.prepareCall(sQry);
			callStatement.setString(1,p_oCustomerLoginForm.getTransactionId());	
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				orderItemDetailList = new ArrayList<OrderItemDetailsVO>();
				while(resultSet.next()){				
					oOrderItemDetailsVO=new OrderItemDetailsVO();
					oOrderItemDetailsVO.setAirName(resultSet.getString("air_name"));
					oOrderItemDetailsVO.setFlightNo(resultSet.getString("flight_no"));
					oOrderItemDetailsVO.setCustPhone(getPhoneFormat(resultSet.getString("cust_bill_phone")));
					
					oOrderItemDetailsVO.setCustShipAddress1(resultSet.getString("cust_ship_addr1"));
					oOrderItemDetailsVO.setCustShipAddressSecond(resultSet.getString("cust_ship_addr2"));
					oOrderItemDetailsVO.setCustShipCity(resultSet.getString("cust_ship_city"));
					oOrderItemDetailsVO.setCustShipCountry(resultSet.getString("cust_ship_country"));
					oOrderItemDetailsVO.setCustShipEmail(resultSet.getString("cust_ship_email"));
					//To convert the first letter of the First name to Upper case
					oOrderItemDetailsVO.setCustShipFirstName(convertToNameFormat(resultSet.getString("cust_ship_fname")));
					//To convert the first letter of the Last name to Upper case
					oOrderItemDetailsVO.setCustShipLastName(convertToNameFormat(resultSet.getString("cust_ship_lname")));
					oOrderItemDetailsVO.setCustShipState(resultSet.getString("cust_ship_state"));
					oOrderItemDetailsVO.setCustShipZip(resultSet.getString("cust_ship_zip"));
					oOrderItemDetailsVO.setCustShipPhone(getPhoneFormat(resultSet.getString("cust_ship_phone")));
					
					//To convert the first letter of the First name to Upper case
					oOrderItemDetailsVO.setCustFirstName(convertToNameFormat(resultSet.getString("cust_bill_fname")));
					//To convert the first letter of the Last name to Upper case
					oOrderItemDetailsVO.setCustLastName(convertToNameFormat(resultSet.getString("cust_bill_lname")));
					oOrderItemDetailsVO.setCustAddress1(resultSet.getString("cust_bill_addr1"));
					oOrderItemDetailsVO.setCustAddressSecond(resultSet.getString("cust_bill_addr2"));
					oOrderItemDetailsVO.setCustCity(resultSet.getString("cust_bill_city"));
					oOrderItemDetailsVO.setCustCountry(resultSet.getString("cust_bill_country"));
					oOrderItemDetailsVO.setCustEmail(resultSet.getString("cust_bill_email"));
					oOrderItemDetailsVO.setCustState(resultSet.getString("cust_bill_state"));
					oOrderItemDetailsVO.setCustZip(resultSet.getString("cust_bill_zip"));
					//Travel Date should be fetched from DB.
					String travelDate = resultSet.getString("travel_date");
					
					if(travelDate != null && travelDate != "") 
						oOrderItemDetailsVO.setTravelDate(dateAndTimeStampConversion(travelDate));
					
					oOrderItemDetailsVO.setProdCode(resultSet.getString("prod_code"));
					oOrderItemDetailsVO.setCateId(resultSet.getString("cate_id"));
					oOrderItemDetailsVO.setQty(resultSet.getString("qty"));
					oOrderItemDetailsVO.setSbhPrice(numberFormat.format(Float.parseFloat(resultSet.getString("sbh_price"))));
					oOrderItemDetailsVO.setOrderCreatedDt(dateStampConversion(resultSet.getString("order_dt")));
					oOrderItemDetailsVO.setBrandName(resultSet.getString("brand_name"));
					oOrderItemDetailsVO.setItemName(resultSet.getString("prod_title"));
					oOrderItemDetailsVO.setVendorName(resultSet.getString("owner_name"));
					oOrderItemDetailsVO.setOwnerType(resultSet.getString("owner_type"));
					oOrderItemDetailsVO.setOrderItemId(resultSet.getString("order_item_id"));			
					if("Airline".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())) {
						return_policy="Restrictions /Validity /Cancellation";
					}else {
						return_policy="Return Policy";
					}
					oOrderItemDetailsVO.setSeturnPolicy(return_policy);
					oOrderItemDetailsVO.setReturnPolicy(resultSet.getString("return_policy"));
					oOrderItemDetailsVO.setReturnDays(resultSet.getString("return_days"));
					oOrderItemDetailsVO.setOrderItemStatus(resultSet.getString("status"));
					oOrderItemDetailsVO.setMainImgCaption(resultSet.getString("main_img_caption"));
					oOrderItemDetailsVO.setMainImgPath(resultSet.getString("main_img_path"));
					oOrderItemDetailsVO.setMainImgType(resultSet.getString("main_img_type"));
					oOrderItemDetailsVO.setOwnerId(resultSet.getString("owner_id"));
					oOrderItemDetailsVO.setProdId(resultSet.getString("prod_id"));
					oOrderItemDetailsVO.setReturnDateExpired(resultSet.getString("is_return_dt_exp"));
					oOrderItemDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
					oOrderItemDetailsVO.setCustId(resultSet.getString("cust_id"));
					oOrderItemDetailsVO.setSize(resultSet.getString("size"));
					oOrderItemDetailsVO.setColor(resultSet.getString("color"));
					oOrderItemDetailsVO.setCustReturnQuantity(resultSet.getString("return_quantity"));
					oOrderItemDetailsVO.setRmaApprovedQuantity(resultSet.getString("approved_quantity"));
					shipmentStatus = resultSet.getString("shipment_status");
					shipmentStatus = shipmentStatus==null?"N":shipmentStatus.trim();
					oOrderItemDetailsVO.setShipmentStatus(shipmentStatus);
					if(resultSet.getString("sbh_price")!= null)
						dSbhPrice = Double.parseDouble(resultSet.getString("sbh_price"));
					if(resultSet.getString("qty")!= null)
						dQty = Double.parseDouble(resultSet.getString("qty"));
					
					oOrderItemDetailsVO.setPayAmount(numberFormat.format(dQty*dSbhPrice));
					oOrderItemDetailsVO.setIsSpecialProd(resultSet.getString("is_special_prod"));
					
					orderItemDetailList.add(oOrderItemDetailsVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getCustomerOrderItemDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		 
		logger.info("CustomerDAO::getCustomerOrderItemDetails::EXIT");
		return orderItemDetailList;
	}
	
	public static Boolean getCustomerTransactionDetails(CustomerLoginForm p_oCustomerForm) 
		throws Exception {
		logger.debug("CustomerDAO::getCustomerTransactionDetails::ENTRY");
		
		Boolean b_EmailSent = Boolean.TRUE;
		String s_Query = "{call usp_sbh_get_customer_txn_details(?)}";
		logger.debug("CustomerDAO::getCustomerTransactionDetails::Query "+s_Query);
		logger.debug("CustomerDAO::getCustomerTransactionDetails::Email "+p_oCustomerForm.getEmailId());
		List<OrderDetailsVO> orderDetailList = null;
		Connection connection=null;		
		CallableStatement callStatement=null;
		OrderDetailsVO oOrderDetailVO;
		ResultSet resultSet=null;
		
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(s_Query);
			callStatement.setString(1,p_oCustomerForm.getEmailId());	
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				orderDetailList = new ArrayList<OrderDetailsVO>();
				while(resultSet.next()){				
					oOrderDetailVO=new OrderDetailsVO();
					oOrderDetailVO.setOrderDt(dateStampConversion(resultSet.getString("order_dt")));
					oOrderDetailVO.setCustTransId(resultSet.getString("cust_trans_id"));
					oOrderDetailVO.setAirName(resultSet.getString("air_name"));
					oOrderDetailVO.setCustName(convertToNameFormat(resultSet.getString("cust_name")));
					orderDetailList.add(oOrderDetailVO);
				}
			}
			if(orderDetailList != null && orderDetailList.size() > 0) {
				sendEmail(orderDetailList,p_oCustomerForm.getEmailId());
			}else {
				b_EmailSent = Boolean.FALSE;
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getCustomerTransactionDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::getCustomerTransactionDetails::EXIT");
		return b_EmailSent;
	}
	
	public static void sendEmail(List<OrderDetailsVO> orderDetailList, String p_sEmail_Id) throws Exception {
		logger.debug("CustomerDAO::sendEmail::ENTRY");
		
		String sToAddr = "";
		HashMap<String,String> o_HMTags = new HashMap<String,String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sOrderConfiramtionNoDetails="";
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId =null,sEmailContent=null,sEmailOrderConfirmationDetails="";
		EmailVO oEmailVO = null;
		String sBccAddr="",sCcAddr="";
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId=oBundle.getString("email.order.confirmation.number");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			
			
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
			
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
			
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header
			o_HMTags.put("{{Logo}}",sSkyBuyLogoPath);
			o_HMTags.put("{{Date}}",dateFormat(new Date()));
			
			//SkyBuy Address
			o_HMTags.put("{{Address1}}",sSkyBuyAddr1);
			o_HMTags.put("{{Address2}}",sSkyBuyAddr2);
			o_HMTags.put("{{Phone}}",sSkyBuyPh);
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sEmailOrderConfirmationDetails = oEmailVO.getEmailOptionalContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();

			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
		
			sEmailOrderConfirmationDetails=sEmailOrderConfirmationDetails==null?"":sEmailOrderConfirmationDetails.trim();
			
			if(orderDetailList != null && !orderDetailList.isEmpty()) {
				for(OrderDetailsVO oOrderDetail:orderDetailList) {
					o_HMTags.put("{{custName}}", Utils.toTitleCase(oOrderDetail.getCustName()));
					o_HMTags.put("{{CustomerTransId}}",oOrderDetail.getCustTransId());
					o_HMTags.put("{{OrderDate}}",oOrderDetail.getOrderDt());
					o_HMTags.put("{{AirlineName}}",oOrderDetail.getAirName());
					sOrderConfiramtionNoDetails=sOrderConfiramtionNoDetails+replaceTagValues(o_HMTags,sEmailOrderConfirmationDetails,null,null);	
				}
				// System.out.println(sOrderConfiramtionNoDetails);
				o_HMTags.put("{{CustomerTransactionList}}",sOrderConfiramtionNoDetails);
			}
			
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(o_HMTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			
			sToAddr=p_sEmail_Id;
			sToAddr=sToAddr==null?"":sToAddr.trim();
			
			
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			oEmail.sendEmail(oEmailLogVo);
			System.out.print("EmailContent "+sEmailContent);
			
			logger.info("CustomerDAO::sendEmail::EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CustomerDAO::sendEmail::Exception "+e.getMessage());
			throw e;
		}	
	}
	public static List<OrderConfirmationVO> getOrderConfirmationHistory(CustomerLoginForm p_oCustomerForm) 
		throws Exception {
		logger.info("CustomerDAO::getOrderConfirmationHistory::ENTRY");
		
		String s_Query="{call usp_sbh_get_order_confirmation_details(?)}";
		logger.info("CustomerDAO::getOrderConfirmationHistory::Query "+s_Query);
		logger.info("CustomerDAO::getOrderConfirmationHistory::Transaction Id "+p_oCustomerForm.getTransactionId());
		List<OrderConfirmationVO> orderConfirmationList = new ArrayList<OrderConfirmationVO>();
		Connection connection=null;		
		CallableStatement callStatement=null;
		OrderConfirmationVO oOrderConfirmationVO;
		ResultSet resultSet=null;
		
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(s_Query);
			callStatement.setString(1,p_oCustomerForm.getTransactionId());	
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				orderConfirmationList = new ArrayList<OrderConfirmationVO>();
				while(resultSet.next()){
					oOrderConfirmationVO = new OrderConfirmationVO();
					oOrderConfirmationVO.setOrderConfirmationNo(resultSet.getString("cust_trans_id"));
					oOrderConfirmationVO.setOrderDate(dateStampConversion(resultSet.getString("order_dt")));
					oOrderConfirmationVO.setAirlineName(resultSet.getString("air_name"));
					orderConfirmationList.add(oOrderConfirmationVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getOrderConfirmationHistory::Exception "+e.getMessage());
		}
		finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::getOrderConfirmationHistory::EXIT");
		return orderConfirmationList;
	}
	
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}
	
	public static OrderItemDetailsVO getReturnPolicyDetails(String p_sOrderItemId, String p_sOwnerType, String p_sLoginType) throws Exception {
		logger.info("CustomerDAO::getReturnPolicyDetails::ENTRY");
		
		String sQuery = "{call usp_sbh_get_customer_service_details(?,?,?)}";
		logger.info("CustomerDAO::getReturnPolicyDetails::Query "+sQuery);
		logger.info("CustomerDAO::getReturnPolicyDetails::Order Item Id "+p_sOrderItemId);
		logger.info("CustomerDAO::getReturnPolicyDetails::Owner Type "+p_sOwnerType);
		logger.info("CustomerDAO::getReturnPolicyDetails::Login Type "+p_sLoginType);
		Connection connection = null;		
		CallableStatement callStatement = null;
		OrderItemDetailsVO oOrderItemDetailVO = null;
		ResultSet resultSet=null;
		
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sOrderItemId);
			callStatement.setString(2, p_sOwnerType);
			callStatement.setString(3, p_sLoginType);
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				while(resultSet.next()) {
					oOrderItemDetailVO = new OrderItemDetailsVO();
					oOrderItemDetailVO.setReturnPolicy(resultSet.getString("return_policy"));
					oOrderItemDetailVO.setReturnDays(resultSet.getString("return_days"));
					oOrderItemDetailVO.setCustServicePhone(resultSet.getString("cust_service_phoneno"));
					oOrderItemDetailVO.setCustServicePhoneExtnNo(resultSet.getString("cust_service_phoneno_ext"));
					oOrderItemDetailVO.setCustServiceEmailId(resultSet.getString("cust_service_email"));
				}
			}
			if(callStatement.getMoreResults()) {
				resultSet = callStatement.getResultSet();
				if(resultSet != null) {
					while(resultSet.next()) {
						oOrderItemDetailVO.setRmaGeneratedNo(resultSet.getString("rma_number"));
						oOrderItemDetailVO.setRmaStatus(resultSet.getString("status"));
						oOrderItemDetailVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getReturnPolicyDetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getReturnPolicyDetails::EIXT");
		
		return oOrderItemDetailVO;
	}
	
	/*public static String addCustReturnDetails(OrderItemDetailsVO p_oOrderItemDetailVO) 
		throws Exception {
		logger.info("CustomerDAO::addCustReturnDetails::ENTRY");
		
		String sReturnId="";
		String sQuery = "{call usp_sbh_add_customer_return_details(?,?,?,?)}";
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.registerOutParameter(1, Types.VARCHAR);
			if(p_oOrderItemDetailVO != null) {
				callStatement.setString(2, p_oOrderItemDetailVO.getOrderItemId());
				callStatement.setString(3, p_oOrderItemDetailVO.getOwnerId());
				callStatement.setString(4, p_oOrderItemDetailVO.getOwnerType());
			}
			callStatement.execute();
			sReturnId = callStatement.getString(0);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getReturnPolicyDetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::addCustReturnDetails::EXIT");
		return sReturnId;
	}
	
	public static void updateCustReturnDetails(String p_sRMAGeneratedCode) 
		throws Exception {
		logger.info("CustomerDAO::updateCustReturnDetails::ENTRY");
		
		String sQuery = "{call usp_sbh_upd_customer_return_details(?)}";
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			if(p_sRMAGeneratedCode != null) {
				callStatement.setString(1, p_sRMAGeneratedCode);
			}
			callStatement.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getReturnPolicyDetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::updateCustReturnDetails::EXIT");
	}
	
	public static CustomerReturnDetailsVO getPartnerReturnDetails(String p_sOrderItemId) 
		throws Exception {
		logger.info("CustomerDAO::getPartnerReturnAddress::ENTRY");
		
		String sQuery = "{call usp_sbh_get_partner_return_details(?)}";
		Connection connection = null;		
		CallableStatement callStatement = null;
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		ResultSet resultSet=null;
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			if(p_sOrderItemId != null) {
				callStatement.setString(1, p_sOrderItemId);
			}
			callStatement.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getReturnPolicyDetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::getPartnerReturnAddress::EXIT");
		
		return oCustomerReturnDetailsVO;
	}*/
	
	public static List<CustomerSuggestionsVO> getCustomerSuggestions(String p_sSearchType, String p_sSearchValue, String p_sOrderFromDate, String p_sOrderToDate) throws Exception {
		logger.info("CustomerDAO::getCustomerSuggestions::ENTRY");

		String sQuery = "{call usp_sbh_get_cust_suggestions(?,?,?,?)}";
		logger.info("CustomerDAO::getCustomerSuggestions::Query "+sQuery);
		logger.info("CustomerDAO::getCustomerSuggestions::Search Type "+p_sSearchType);
		logger.info("CustomerDAO::getCustomerSuggestions::Search Value "+p_sSearchValue);
		logger.info("CustomerDAO::getCustomerSuggestions::Order From Date "+p_sOrderFromDate);
		logger.info("CustomerDAO::getCustomerSuggestions::Order To Date "+p_sOrderToDate);
		Connection connection = null;		
		CallableStatement callStatement = null;
		List<CustomerSuggestionsVO> alSuggestions = new ArrayList<CustomerSuggestionsVO>();
		CustomerSuggestionsVO oCustomerSuggestionsVO = null;
		String sDBData = null;
		ResultSet rs=null;
		Date  dOrderDt = null;
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy hh:mm"  ) ; 
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sSearchType);
			callStatement.setString(2, p_sSearchValue);
			callStatement.setString(3, p_sOrderFromDate);
			callStatement.setString(4, p_sOrderToDate);
			rs = callStatement.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oCustomerSuggestionsVO = new CustomerSuggestionsVO();

					sDBData = rs.getString("order_dt");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setOrderDate(sDBData);
					
					sDBData = rs.getString("order_id");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setOrderId(sDBData);
					
					sDBData = rs.getString("cust_id");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setCustId(sDBData);

					sDBData = rs.getString("order_conf_no");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setOrderConfirmationNo(sDBData);

					sDBData = rs.getString("cust_phone");
					sDBData = sDBData==null?"":sDBData.trim();
					sDBData = AirlineDAO.getPhoneFormat(sDBData);
					oCustomerSuggestionsVO.setCustPhone(sDBData);
					
					sDBData = rs.getString("cust_bill_fname");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setBillingFName(sDBData);

					sDBData = rs.getString("cust_bill_lname");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setBillingLName(sDBData);
					
					sDBData = rs.getString("cust_bill_email");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setBillingEmail(sDBData);

					sDBData = rs.getString("cust_bill_city");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setBillingCity(sDBData);

					sDBData = rs.getString("cust_bill_state");
					sDBData = sDBData==null?"":sDBData.trim();
					oCustomerSuggestionsVO.setBillingState(sDBData);
					
					sDBData = rs.getString("cust_ship_phone");
					sDBData = sDBData==null?"":sDBData.trim();
					sDBData = AirlineDAO.getPhoneFormat(sDBData);
					oCustomerSuggestionsVO.setShippingPhone(sDBData);

					sDBData = rs.getString("cust_suggestion");
					sDBData = sDBData==null?"":sDBData.trim();
					sDBData = sDBData.replaceAll("<br/>","\n");	
					sDBData = sDBData.replaceAll("<br>","\n");	
					oCustomerSuggestionsVO.setCustSuggestions("\n"+sDBData+"\n"+"\n");
					
					sDBData = rs.getString("cust_new_services_feedback");
					sDBData = sDBData==null?"":sDBData.trim();
					sDBData = sDBData.replaceAll("<br/>","\n");	
					sDBData = sDBData.replaceAll("<br>","\n");	
					oCustomerSuggestionsVO.setCustFeedback("\n"+sDBData);
					
					alSuggestions.add(oCustomerSuggestionsVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getCustomerSuggestions::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(rs!=null)
				rs.close();
		}
		logger.info("CustomerDAO::getCustomerSuggestions::EXIT");

		return alSuggestions;
	}


	public static CustomerSuggestionsVO getCustomerSuggestionsByCustId(String p_sCustId) throws Exception {
		logger.info("CustomerDAO::getCustomerSuggestionsByCustId::ENTRY");

		String sQuery = "{call usp_sbh_get_cust_sugg_by_cust_id(?)}";
		logger.info("CustomerDAO::getCustomerSuggestionsByCustId::Query "+sQuery);
		logger.info("CustomerDAO::getCustomerSuggestionsByCustId::Customer Id "+p_sCustId);
		Connection connection = null;		
		CallableStatement callStatement = null;
		CustomerSuggestionsVO oCustomerSuggestionsVO = null;
		String sDBData = null;
		ResultSet rs=null;
		Date  dOrderDt = null;
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy hh:mm"  ) ; 

		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sCustId);
			rs = callStatement.executeQuery();
			if(rs.next()) {
				oCustomerSuggestionsVO = new CustomerSuggestionsVO();

				sDBData = rs.getString("order_dt");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setOrderDate(sDBData);

				sDBData = rs.getString("order_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setOrderId(sDBData);
				
				sDBData = rs.getString("cust_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setCustId(sDBData);

				sDBData = rs.getString("order_conf_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setOrderConfirmationNo(sDBData);

				sDBData = rs.getString("cust_phone");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setCustPhone(sDBData);
				
				sDBData = rs.getString("cust_bill_fname");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingFName(sDBData);

				sDBData = rs.getString("cust_bill_lname");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingLName(sDBData);
				
				sDBData = rs.getString("cust_bill_email");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingEmail(sDBData);
				
				sDBData = rs.getString("cust_bill_addr1");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingAddr1(sDBData);
				
				
				sDBData = rs.getString("cust_bill_addr2");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingAddr2(sDBData);

				sDBData = rs.getString("cust_bill_city");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingCity(sDBData);

				sDBData = rs.getString("cust_bill_state");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingState(sDBData);
				
				sDBData = rs.getString("cust_bill_country");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingCountry(sDBData);
				
				sDBData = rs.getString("cust_bill_zip");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setBillingZip(sDBData);
				
				sDBData = rs.getString("cust_ship_fname");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingFName(sDBData);

				sDBData = rs.getString("cust_ship_lname");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingLName(sDBData);
				
				sDBData = rs.getString("cust_ship_phone");
				sDBData = sDBData==null?"":sDBData.trim();
				sDBData = AirlineDAO.getPhoneFormat(sDBData);
				oCustomerSuggestionsVO.setShippingPhone(sDBData);
				
				sDBData = rs.getString("cust_ship_email");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingEmail(sDBData);

				sDBData = rs.getString("cust_ship_city");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingCity(sDBData);

				sDBData = rs.getString("cust_ship_addr1");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingAddr1(sDBData);
				
				
				sDBData = rs.getString("cust_ship_addr2");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingAddr2(sDBData);

				sDBData = rs.getString("cust_ship_state");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingState(sDBData);
				
				sDBData = rs.getString("cust_ship_country");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingCountry(sDBData);
				
				sDBData = rs.getString("cust_ship_zip");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setShippingZip(sDBData);
				
				sDBData = rs.getString("flight_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setFlightNo(sDBData);
				
				sDBData = rs.getString("air_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oCustomerSuggestionsVO.setAirlineName(sDBData);
				
				sDBData = rs.getString("cust_suggestion");
				sDBData = sDBData==null?"":sDBData.trim();
				sDBData = sDBData.replaceAll("<br/>","\n");	
				sDBData = sDBData.replaceAll("<br>","\n");	
				oCustomerSuggestionsVO.setCustSuggestions("\n"+sDBData+"\n"+"\n");
				
				sDBData = rs.getString("cust_new_services_feedback");
				sDBData = sDBData==null?"":sDBData.trim();
				sDBData = sDBData.replaceAll("<br/>","\n");	
				sDBData = sDBData.replaceAll("<br>","\n");	
				oCustomerSuggestionsVO.setCustFeedback("\n"+sDBData);
				
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getCustomerSuggestionsByCustId::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(rs!=null)
				rs.close();
		}
		logger.info("CustomerDAO::getCustomerSuggestionsByCustId::EXIT");

		return oCustomerSuggestionsVO;
	}
	
	public static CustomerReturnDetailsVO getGeneratedRMADetails(String p_sReturnId, String p_sOwnerType) 
		throws Exception {
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		logger.info("CustomerDAO::getGeneratedRMADetails::ENTRY");
		
		String sQuery = "{call usp_sbh_get_generated_rma_details(?,?)}";
		logger.info("CustomerDAO::getGeneratedRMADetails::QUERY "+sQuery);
		logger.info("CustomerDAO::getGeneratedRMADetails::Return Id "+p_sReturnId);
		logger.info("CustomerDAO::getGeneratedRMADetails::Owner Type "+p_sOwnerType);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		
		try {
			oCustomerReturnDetailsVO = new CustomerReturnDetailsVO();
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sReturnId);
			callStatement.setString(2, p_sOwnerType);
			
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				while(resultSet.next()) {
					oCustomerReturnDetailsVO.setGeneratedRMA(resultSet.getString("rma_number"));
					oCustomerReturnDetailsVO.setRmaStatus(resultSet.getString("status"));
					oCustomerReturnDetailsVO.setReasonForReturn(resultSet.getString("reason"));
					oCustomerReturnDetailsVO.setReturnQuantity(resultSet.getString("cust_return_qty"));
					oCustomerReturnDetailsVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					oCustomerReturnDetailsVO.setOwnerName(resultSet.getString("owner_name"));
					oCustomerReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
					oCustomerReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
					oCustomerReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
					oCustomerReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
					oCustomerReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
					oCustomerReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
					oCustomerReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
					oCustomerReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
					oCustomerReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
					oCustomerReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
				}
			}
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.error("CustomerDAO::getGeneratedRMADetails::Exception "+exception.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		
		logger.info("CustomerDAO::getGeneratedRMADetails::EXIT");
		return oCustomerReturnDetailsVO;
	}
	
	public static CustomerReturnDetailsVO getRMAgenerated(String p_sOrderItemId, String p_sOwnerType, String p_sOwnerId, String p_sOrderConfNo, CustomerLoginForm p_oCustomerLoginForm) 
		throws Exception {
		logger.info("CustomerDAO::getRMAgenerated::ENTRY");
		
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		String sQuery = "{call usp_sbh_gen_rma_number(?,?,?,?,?,?)}";
		logger.info("CustomerDAO::getRMAgenerated::QUERY "+sQuery);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			oCustomerReturnDetailsVO = new CustomerReturnDetailsVO();
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sOrderItemId);
			callStatement.setString(2, p_sOwnerId);
			callStatement.setString(3, p_sOwnerType);
			callStatement.setString(4, p_sOrderConfNo);
			callStatement.setString(5, p_oCustomerLoginForm.getReasonForReturn());
			callStatement.setString(6, p_oCustomerLoginForm.getReturnQuantity());
			
			callStatement.execute();
			callStatement.getMoreResults();
			if(callStatement.getMoreResults()) {
				resultSet = callStatement.getResultSet();
				if(resultSet != null) {
					while(resultSet.next()) {
						oCustomerReturnDetailsVO.setGeneratedRMA(resultSet.getString("rma_number"));
						oCustomerReturnDetailsVO.setRmaStatus(resultSet.getString("status"));
						oCustomerReturnDetailsVO.setReasonForReturn(resultSet.getString("reason"));
						oCustomerReturnDetailsVO.setReturnQuantity(resultSet.getString("cust_return_qty"));
						oCustomerReturnDetailsVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					}
				}
			}
			if(callStatement.getMoreResults()) {
				resultSet = callStatement.getResultSet();
				if(resultSet != null) {
					while(resultSet.next()) {
						oCustomerReturnDetailsVO.setOwnerName(resultSet.getString("owner_name"));
						oCustomerReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
						oCustomerReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
						oCustomerReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
						oCustomerReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
						oCustomerReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
						oCustomerReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
						oCustomerReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
						oCustomerReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
						oCustomerReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
						oCustomerReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getRMAgenerated::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getRMAgenerated::EXIT");
		
		return oCustomerReturnDetailsVO;
	}
	
	public static List<OrderReturnDetailsVO> getOrderReturnDetails(String p_sOrderItemId) 
		throws Exception {
		logger.info("CustomerDAO::getOrderReturnDetails::ENTER");
		List<OrderReturnDetailsVO> alOrderReturnDetails = null;
		OrderReturnDetailsVO oOrderReturnDetailsVO = null;
		String sQuery = "{call usp_sbh_get_cust_order_return_details(?)}";
		logger.info("CustomerDAO::getOrderReturnDetails::QUERY "+sQuery);
		logger.info("CustomerDAO::getOrderReturnDetails::Order Item Id "+p_sOrderItemId);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sOrderItemId);
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				alOrderReturnDetails = new ArrayList<OrderReturnDetailsVO>();
				while(resultSet.next()) {
					oOrderReturnDetailsVO = new OrderReturnDetailsVO();
					
					oOrderReturnDetailsVO.setReturnId(resultSet.getString("return_id"));
					/*oOrderReturnDetailsVO.setOwnerName(resultSet.getString("owner_name"));
					oOrderReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
					oOrderReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
					oOrderReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
					oOrderReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
					oOrderReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
					oOrderReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
					oOrderReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
					oOrderReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
					oOrderReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
					oOrderReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));*/
					oOrderReturnDetailsVO.setCustomerReason(resultSet.getString("reason"));
					oOrderReturnDetailsVO.setCustomerReturnQuantity(resultSet.getString("cust_return_qty"));
					oOrderReturnDetailsVO.setVendorReturnQuantity(resultSet.getString("vend_return_qty"));
					
					alOrderReturnDetails.add(oOrderReturnDetailsVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("CustomerDAO::getOrderReturnDetails::EXCEPTION "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getOrderReturnDetails::EXIT");
		return alOrderReturnDetails;
	}
	
	public static CustomerReturnDetailsVO getPartnerReturnDetails(String p_sOrderItemId, String p_sOwnerType) 
		throws Exception {
		logger.info("CustomerDAO::getPartnerReturnDetails::ENTRY");
		
		CustomerReturnDetailsVO oCustomerReturnDetailsVO = null;
		String sQuery = "{call usp_sbh_get_partner_return_address(?,?)}";
		logger.info("CustomerDAO::getPartnerReturnDetails::QUERY "+sQuery);
		logger.info("CustomerDAO::getPartnerReturnDetails::Order Item Id "+p_sOrderItemId);
		logger.info("CustomerDAO::getPartnerReturnDetails::Owner Type "+p_sOwnerType);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			oCustomerReturnDetailsVO = new CustomerReturnDetailsVO();
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sOrderItemId);
			callStatement.setString(2, p_sOwnerType);
			
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				while(resultSet.next()) {
					oCustomerReturnDetailsVO.setOwnerName(resultSet.getString("owner_name"));
					oCustomerReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
					oCustomerReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
					oCustomerReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
					oCustomerReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
					oCustomerReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
					oCustomerReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
					oCustomerReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
					oCustomerReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
					oCustomerReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
					oCustomerReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
					oCustomerReturnDetailsVO.setReasonForReturn(resultSet.getString("reason"));
					oCustomerReturnDetailsVO.setGeneratedRMA(resultSet.getString("rma_number"));
					oCustomerReturnDetailsVO.setRmaStatus(resultSet.getString("status"));
					oCustomerReturnDetailsVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					oCustomerReturnDetailsVO.setReturnQuantity(resultSet.getString("cust_return_qty"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getPartnerReturnDetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getPartnerReturnDetails::EXIT");
		
		return oCustomerReturnDetailsVO;
	}
	
	public static List<OrderReturnDetailsVO> getOrderRMADetails(String p_sOrderItemId) throws Exception {
		logger.info("CustomerDAO::getOrderRMADetails::ENTRY");
		
		List<OrderReturnDetailsVO> alOrderReturnDetails = null;
		OrderReturnDetailsVO oOrderReturnDetailsVO = null;
		String sQuery = "{call usp_sbh_get_RMA_details(?)}";
		logger.info("CustomerDAO::getOrderRMADetails::QUERY "+sQuery);
		logger.info("CustomerDAO::getOrderRMADetails::Order Item Id "+p_sOrderItemId);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sOrderItemId);
			
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				alOrderReturnDetails = new ArrayList<OrderReturnDetailsVO>();
				while(resultSet.next()) {
					oOrderReturnDetailsVO = new OrderReturnDetailsVO();
					
					oOrderReturnDetailsVO.setReturnId(resultSet.getString("return_id"));
					oOrderReturnDetailsVO.setRmaStatus(resultSet.getString("status"));
					oOrderReturnDetailsVO.setRMANumber(resultSet.getString("rma_number"));
					oOrderReturnDetailsVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					
	//				oOrderReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
	//				oOrderReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
	//				oOrderReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
	//				oOrderReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
	//				oOrderReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
	//				oOrderReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
	//				oOrderReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
	//				oOrderReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
	//				oOrderReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
					
					oOrderReturnDetailsVO.setOrderItemId(resultSet.getString("order_item_id"));
					oOrderReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
					oOrderReturnDetailsVO.setCustomerReason(resultSet.getString("reason"));
					oOrderReturnDetailsVO.setVendorComments(resultSet.getString("comments"));
					oOrderReturnDetailsVO.setAdminComments(resultSet.getString("admin_comments"));
					oOrderReturnDetailsVO.setCustomerReturnQuantity(resultSet.getString("cust_return_qty"));
					oOrderReturnDetailsVO.setVendorReturnQuantity(resultSet.getString("vend_return_qty"));
					
					alOrderReturnDetails.add(oOrderReturnDetailsVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getOrderRMADetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getOrderRMADetails::EXIT");
		return alOrderReturnDetails;
	}
	
	public static OrderReturnDetailsVO getReturnOrderRMADetails(String p_sReturnId) throws Exception {
		logger.info("CustomerDAO::getReturnOrderRMADetails::ENTRY");
		
		OrderReturnDetailsVO oOrderReturnDetailsVO = null;
		String sQuery = "{call usp_sbh_get_order_and_RMA_details(?)}";
		logger.info("CustomerDAO::getReturnOrderRMADetails::QUERY "+sQuery);
		logger.info("CustomerDAO::getReturnOrderRMADetails::Return Id "+p_sReturnId);
		Connection connection = null;		
		CallableStatement callStatement = null;
		ResultSet resultSet=null;
		try{
			
			connection = getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sReturnId);
			
			resultSet = callStatement.executeQuery();
			if(resultSet != null) {
				while(resultSet.next()) {
					oOrderReturnDetailsVO = new OrderReturnDetailsVO();
					
					oOrderReturnDetailsVO.setReturnId(resultSet.getString("return_id"));
					oOrderReturnDetailsVO.setOwnerName(resultSet.getString("owner_name"));
					oOrderReturnDetailsVO.setRmaStatus(resultSet.getString("status"));
					oOrderReturnDetailsVO.setRMANumber(resultSet.getString("rma_number"));
					oOrderReturnDetailsVO.setRmaGeneratedDate(dateStampConversion(resultSet.getString("sbh_create_dt")));
					
					oOrderReturnDetailsVO.setPartnerAddress1(resultSet.getString("partner_addr1"));
					oOrderReturnDetailsVO.setPartnerAddress2(resultSet.getString("partner_addr2"));
					oOrderReturnDetailsVO.setPartnerCity(resultSet.getString("partner_city"));
					oOrderReturnDetailsVO.setPartnerState(resultSet.getString("partner_state"));
					oOrderReturnDetailsVO.setPartnerZip(resultSet.getString("partner_zip"));
					oOrderReturnDetailsVO.setPartnerCountry(resultSet.getString("partner_country"));
					oOrderReturnDetailsVO.setPartnerPhone(resultSet.getString("partner_phone"));
	//				oOrderReturnDetailsVO.setPartnerMobile(resultSet.getString("partner_mobile"));
	//				oOrderReturnDetailsVO.setPartnerPhoneExtn(resultSet.getString("partner_phone_extn"));
					oOrderReturnDetailsVO.setPartnerContactName(resultSet.getString("owner_contact_name"));
					oOrderReturnDetailsVO.setRmaAuthorizeSignatory(resultSet.getString("authorise_signatory"));
					
					oOrderReturnDetailsVO.setOrderItemId(resultSet.getString("order_item_id"));
					oOrderReturnDetailsVO.setCustTransId(resultSet.getString("cust_trans_id"));
					oOrderReturnDetailsVO.setCustomerReason(resultSet.getString("reason"));
					oOrderReturnDetailsVO.setVendorComments(resultSet.getString("comments"));
					oOrderReturnDetailsVO.setAdminComments(resultSet.getString("admin_comments"));
					oOrderReturnDetailsVO.setCustomerReturnQuantity(resultSet.getString("cust_return_qty"));
					oOrderReturnDetailsVO.setVendorReturnQuantity(resultSet.getString("vend_return_qty"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CustomerDAO::getReturnOrderRMADetails::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(resultSet!=null)
				resultSet.close();
		}
		logger.info("CustomerDAO::getReturnOrderRMADetails::EXIT");
		return oOrderReturnDetailsVO;
	}
	
	public static boolean sendRMAGeneratedToCustomer(OrderItemDetailsVO p_oOrderItemDetailsVO, CustomerReturnDetailsVO p_oCustomerReturnDetailsVO, File p_oPdfFile,String p_sLoginId,String p_sReqeustFrom) 
		throws Exception {
		logger.info("CustomerDAO::getReturnOrderRMADetails::ENTER");
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sCustomerId = null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sCustomerEmailAddr = null;
		String sEmailBccAddr = "";
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.rmagenerated.form.customer");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendTrackingDetailsEmail::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetailsVO != null) {
			sCustomerId = p_oOrderItemDetailsVO.getCustId();
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
	
			hmTags.put("{{RMANumber}}",p_oCustomerReturnDetailsVO.getGeneratedRMA());
			hmTags.put("{{ReasonForReturn}}",p_oCustomerReturnDetailsVO.getReasonForReturn());
			hmTags.put("{{CustomerName}}",p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			/*hmTags.put("{{VendorName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ItemName}}",p_oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Status}}",p_oOrderItemDetailsVO.getOrderItemStatus());
			hmTags.put("{{Quantity}}",p_oOrderItemDetailsVO.getQty());
			hmTags.put("{{ReturnAddress}}",mergeAddressLines(p_oCustomerReturnDetailsVO.getPartnerAddress1(), p_oCustomerReturnDetailsVO.getPartnerAddress2()));
			hmTags.put("{{ReturnCity}}",p_oCustomerReturnDetailsVO.getPartnerCity());
			hmTags.put("{{ReturnState}}",p_oCustomerReturnDetailsVO.getPartnerState());
			hmTags.put("{{ReturnZip}}",p_oCustomerReturnDetailsVO.getPartnerZip());*/
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		sCustomerEmailAddr = p_oOrderItemDetailsVO.getCustEmail();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("CustomerDAO::getReturnOrderRMADetails::EXCEPTION "+exception.getMessage());
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sCustomerEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
		oEmailLogVo.setAttachmentFile(p_oPdfFile.getAbsolutePath());
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sCustomerEmailAddr);
		oEmailLogVo.setEmailBccAddr(sEmailBccAddr);
		oEmailLogVo.setAttachmentFile(p_oPdfFile.getAbsolutePath());
		EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
		
		logger.info("CustomerDAO::getReturnOrderRMADetails::EXIT");
		return bEmailSent;
	}
	
	public static String createRMAGeneratedHTML(OrderItemDetailsVO p_oOrderItemDetailsVO, CustomerReturnDetailsVO p_oCustomerReturnDetailsVO, String p_sState) 
		throws Exception {
		logger.info("OrderDAO::createRMAGeneratedHTML::ENTRY");
		
		String sEmailContent = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sSkyBuyLogoPath;
		String sFromEmailAddr = null;
		String sSkyBuyAddr1;
		String sScissor;
		String sCutMark;
		String sStatus=null;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		sEmailTemplateId=oBundle.getString("pdf.rmagenerated.form.customer");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		
		sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
		sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		sScissor = System.getProperty("ScissorImagePath").trim();
		sScissor = sScissor==null?"":sScissor.trim();
		
		sCutMark = System.getProperty("CutmarkImagePath").trim();;
		sCutMark = sCutMark==null?"":sCutMark.trim();
		
		sSkyBuyAddr1=oBundle.getString("skybuy.address1");
		sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
		sSkyBuyAddr2=oBundle.getString("skybuy.address2");
		sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
		sSkyBuyPh=oBundle.getString("skybuy.phone");
		sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
		//Header
		hmTags.put("{{Logo}}",sSkyBuyLogoPath);
		hmTags.put("{{Date}}",dateFormat(new Date()));
		//SkyBuy Address
		hmTags.put("{{Address1}}",sSkyBuyAddr1);
		hmTags.put("{{Address2}}",sSkyBuyAddr2);
		hmTags.put("{{Phone}}",sSkyBuyPh);	
		
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::createRMAGeneratedHTML::EXCEPTION");
			throw exception;
		}
		hmTags.put("{{Logo}}",sSkyBuyLogoPath);
		hmTags.put("{{CustomerName}}",p_oOrderItemDetailsVO.getCustFirstName()+" "+ p_oOrderItemDetailsVO.getCustLastName());
		hmTags.put("{{RMAGeneratedDate}}",p_oCustomerReturnDetailsVO.getRmaGeneratedDate());
		hmTags.put("{{OrderConfNo}}",p_oOrderItemDetailsVO.getCustTransId());
		hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
		hmTags.put("{{RMANumber}}",p_oCustomerReturnDetailsVO.getGeneratedRMA());
		hmTags.put("{{ReasonForReturn}}",p_oCustomerReturnDetailsVO.getReasonForReturn());
		hmTags.put("{{ReturnPolicy}}",p_oOrderItemDetailsVO.getReturnPolicy());
		hmTags.put("{{ReturnDays}}",p_oOrderItemDetailsVO.getReturnDays());
		hmTags.put("{{VendorName}}",p_oOrderItemDetailsVO.getVendorName());
		hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
		hmTags.put("{{ItemName}}",p_oOrderItemDetailsVO.getItemName());
		hmTags.put("{{BrandName}}",p_oOrderItemDetailsVO.getBrandName());
		hmTags.put("{{OrderDate}}",p_oOrderItemDetailsVO.getOrderCreatedDt());
		if("RG".equalsIgnoreCase(p_oCustomerReturnDetailsVO.getRmaStatus())) {
			sStatus = "RMA Generated";
		}else if("WA".equalsIgnoreCase(p_oOrderItemDetailsVO.getRmaStatus())) {
			sStatus = "Waiting for Admin to charge back";
		}else if("WR".equalsIgnoreCase(p_oOrderItemDetailsVO.getRmaStatus())) {
			sStatus = "Waiting for Admin to rejected";
		}else if("RA".equalsIgnoreCase(p_oOrderItemDetailsVO.getRmaStatus())) {
			sStatus = "RMA Approved";
		}else if("RR".equalsIgnoreCase(p_oOrderItemDetailsVO.getRmaStatus())) {
			sStatus = "RMA Rejected";
		}
		hmTags.put("{{Status}}",sStatus);
		
		String ItemDetails = "";
		if("VENDOR".equalsIgnoreCase(p_oOrderItemDetailsVO.getOwnerType())) {
			ItemDetails = "<table><tr><td align=left nowrap=nowrap><B>Brand Name</B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getBrandName()+"</B></td></tr><tr><td align=left nowrap=nowrap><B>Item Name</B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getItemName()+"</B></td></tr>";
			if(p_oOrderItemDetailsVO.getColor() != null) {
				ItemDetails = ItemDetails+"<tr><td align=left nowrap=nowrap><B>Color</B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getColor()+"</B></td></tr>";
			}
			if(p_oOrderItemDetailsVO.getSize() != null) {
				ItemDetails = ItemDetails+"<tr><td align=left nowrap=nowrap><B>Size </B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getSize()+"</B></td></tr>";
			}
			ItemDetails = ItemDetails+"</table>";
		}else if("AIRLINE".equalsIgnoreCase(p_oOrderItemDetailsVO.getOwnerType())) {
			ItemDetails = "<table><tr><td align=left nowrap=nowrap><B>Item Name</B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getItemName()+"</B></td></tr><tr><td align=left nowrap=nowrap><B>Travel Date</B></td><td align=center><B>:</B></td><td align=left nowrap=nowrap><B>"+p_oOrderItemDetailsVO.getTravelDate()+"</B></td></tr></table>";
		}
		
		hmTags.put("{{ItemDetails}}",ItemDetails);
		hmTags.put("{{Quantity}}",p_oCustomerReturnDetailsVO.getReturnQuantity());
		hmTags.put("{{ReturnAddress}}",mergeAddressLine(p_oCustomerReturnDetailsVO.getPartnerAddress1(), p_oCustomerReturnDetailsVO.getPartnerAddress2()));
		hmTags.put("{{ReturnCity}}",p_oCustomerReturnDetailsVO.getPartnerCity());
		hmTags.put("{{ReturnState}}",p_oCustomerReturnDetailsVO.getPartnerState());
		hmTags.put("{{ReturnZip}}",p_oCustomerReturnDetailsVO.getPartnerZip());
		hmTags.put("{{IMGPath}}",p_oOrderItemDetailsVO.getMainImgPath());
		hmTags.put("{{Scissor}}",sScissor);
		hmTags.put("{{CutMark}}",sCutMark);
		
		if(oEmailVO != null) {
			sEmailContent = oEmailVO.getEmailContent();
		}
		sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
		sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		
		logger.info("OrderDAO::createRMAGeneratedHTML::EXIT");
		
		return sEmailContent;
	}
	
	
}
