package com.sbh.dao;

import static com.sbh.util.Utils.convertToNameFormat;
import static com.sbh.util.Utils.dateAndTimeStampConversion;
import static com.sbh.dao.OrderDAO.decryptInputData;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.email.Email;
import com.sbh.forms.SearchVendorForm;
import com.sbh.forms.VendorDetailsForm;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerRegistrationCommentsVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.VendorDetailsVO;

public class VendorDAO {
	private static Logger logger = LogManager.getLogger(VendorDAO.class);
	
	public static ArrayList getVendorDetails(SearchVendorForm p_oSearchVendorForm) throws Exception{	
		logger.info("VendorDAO::getVendorDetails::ENTER");		
		VendorDetailsVO oVendorDetailsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_vendor_details(?,?)}";
		
		logger.info("VendorDAO::getVendorDetails::SQL QUERY "+sQry);
		logger.info("VendorDAO::getVendorDetails::Search By "+p_oSearchVendorForm.getVendorSearchBy());
		logger.info("VendorDAO::getVendorDetails::Search Type "+p_oSearchVendorForm.getVendorSearchValue());	
		
		ResultSet rs=null;
		ArrayList alProductInfo=new ArrayList();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchVendorForm.getVendorSearchBy());
			cstmt.setString(2,p_oSearchVendorForm.getVendorSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oVendorDetailsVO=new VendorDetailsVO();
				oVendorDetailsVO.setVendId(rs.getString("vend_id"));
				oVendorDetailsVO.setVendName(rs.getString("vend_name"));
				String sVendContactName = rs.getString("vend_contact_name1");
				sVendContactName = sVendContactName ==null?"":sVendContactName.trim();
				oVendorDetailsVO.setVendContactName(CatalogueDAO.toTitleCase(sVendContactName));
				oVendorDetailsVO.setVendAddr1(rs.getString("vend_addr1"));	
				oVendorDetailsVO.setVendAddr2(rs.getString("vend_addr2"));	
				oVendorDetailsVO.setVendCity(rs.getString("vend_city"));
				oVendorDetailsVO.setVendState(rs.getString("vend_state"));
				oVendorDetailsVO.setVendCountry(rs.getString("vend_country"));
				oVendorDetailsVO.setVendZip(rs.getString("vend_zip"));
				oVendorDetailsVO.setVendStatus(rs.getString("vend_status"));
				oVendorDetailsVO.setCustServiceEmail(rs.getString("cust_service_email"));				
				oVendorDetailsVO.setChargeType(rs.getString("charge_type"));
				
				alProductInfo.add(oVendorDetailsVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::getVendorDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		logger.info("VendorDAO::getVendorDetails::EXIT");
		return alProductInfo;
	}
	
	public static VendorDetailsVO getVendorDetailsById(String p_sVendId,String p_sUserTableId) throws Exception{	
		logger.info("VendorDAO::getVendorDetailsById::ENTER");		
		VendorDetailsVO oVendorDetailsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_vendor_by_id(?,?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.info("VendorDAO::getVendorDetailsById::SQL QUERY "+sQry);
		logger.info("VendorDAO::getVendorDetailsById::Vendor Id "+p_sVendId);	
		logger.info("VendorDAO::getVendorDetailsById::User Table Id "+p_sUserTableId);	
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sVendId);	
			cstmt.setString(2,p_sUserTableId);
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oVendorDetailsVO=new VendorDetailsVO();
				oVendorDetailsVO.setVendId(rs.getString("vend_id"));
				oVendorDetailsVO.setVendName(rs.getString("vend_name"));
				String sVendContactName = rs.getString("vend_contact_name1");
				sVendContactName = sVendContactName ==null?"":sVendContactName.trim();
				oVendorDetailsVO.setVendContactName(CatalogueDAO.toTitleCase(sVendContactName));
				oVendorDetailsVO.setVendType(rs.getString("vend_type"));
				oVendorDetailsVO.setVendAddr1(rs.getString("vend_addr1"));	
				oVendorDetailsVO.setVendAddr2(rs.getString("vend_addr2"));	
				oVendorDetailsVO.setVendCity(rs.getString("vend_city"));
				oVendorDetailsVO.setVendState(rs.getString("vend_state"));
				oVendorDetailsVO.setVendCountry(rs.getString("vend_country"));
				oVendorDetailsVO.setVendZip(rs.getString("vend_zip"));
				oVendorDetailsVO.setVendPhone1(rs.getString("vend_phone1"));
				String sPhoneExt1 = rs.getString("vend_phone_ext1");
				sPhoneExt1 = sPhoneExt1==null?"":sPhoneExt1.trim();
				oVendorDetailsVO.setVendPhoneExt1(sPhoneExt1.trim());
				oVendorDetailsVO.setKeyRefId(rs.getString("key_ref_id"));
				String sPhone2 = rs.getString("vend_phone2");
				sPhone2 = sPhone2==null?"":sPhone2.trim();
				oVendorDetailsVO.setVendPhone2(sPhone2.trim());
				
				String sPhoneExt2 =  rs.getString("vend_phone_ext2");
				sPhoneExt2 = sPhoneExt2 ==null?"":sPhoneExt2.trim();
				oVendorDetailsVO.setVendPhoneExt2(sPhoneExt2.trim());
				oVendorDetailsVO.setVendFax(rs.getString("vend_fax"));
				oVendorDetailsVO.setVendEmail(rs.getString("vend_email1"));				
				oVendorDetailsVO.setVendStatus(rs.getString("vend_status"));
				oVendorDetailsVO.setVendUserId(rs.getString("user_id"));
				oVendorDetailsVO.setVendPassword(rs.getString("password"));
				
				String sReturnPolicy = rs.getString("vend_return_policy");
				sReturnPolicy =sReturnPolicy ==null?"":sReturnPolicy.trim();
				oVendorDetailsVO.setVendReturnPolicy(sReturnPolicy);
				
				String sComments = rs.getString("vend_comments");
				sComments =sComments ==null?"":sComments.trim();
				oVendorDetailsVO.setVendComments(sComments);
				
				
				String sVendContactName2 = rs.getString("vend_contact_name2");
				sVendContactName2 = sVendContactName2 ==null?"":sVendContactName2.trim();
				oVendorDetailsVO.setVendContactName2(CatalogueDAO.toTitleCase(sVendContactName2));
				String sCustServicePhoneno = rs.getString("cust_service_phoneno");
				sCustServicePhoneno = sCustServicePhoneno==null?"":sCustServicePhoneno.trim();
				oVendorDetailsVO.setCustServicePhoneno(sCustServicePhoneno.trim());
				
				String sCustServicePhonenoExt =  rs.getString("cust_service_phoneno_ext");
				sCustServicePhonenoExt = sCustServicePhonenoExt ==null?"":sCustServicePhonenoExt.trim();
				oVendorDetailsVO.setCustServicePhonenoExt(sCustServicePhonenoExt.trim());
				oVendorDetailsVO.setCustServiceEmail(rs.getString("cust_service_email"));
				
				
				String sVendMobile1 =  rs.getString("vend_mobile1");
				sVendMobile1 = sVendMobile1 ==null?"":sVendMobile1.trim();
				oVendorDetailsVO.setVendMobile1(sVendMobile1.trim());
				
				String sVendMobile2 =  rs.getString("vend_mobile2");
				sVendMobile2 = sVendMobile2 ==null?"":sVendMobile2.trim();
				oVendorDetailsVO.setVendMobile2(sVendMobile2.trim());
				
				oVendorDetailsVO.setVendEmail2(rs.getString("vend_email2"));
				
				oVendorDetailsVO.setChargeType(rs.getString("charge_type"));
				
				oVendorDetailsVO.setReturnAddr1(rs.getString("vend_return_addr1"));	
				oVendorDetailsVO.setReturnAddr2(rs.getString("vend_return_addr2"));	
				oVendorDetailsVO.setReturnCity(rs.getString("vend_return_city"));
				oVendorDetailsVO.setReturnState(rs.getString("vend_return_state"));
				oVendorDetailsVO.setReturnCountry(rs.getString("vend_return_country"));
				oVendorDetailsVO.setReturnZip(rs.getString("vend_return_zip"));
				oVendorDetailsVO.setReturnDays(rs.getString("vend_return_days"));
				
				String sPoPct = rs.getString("po_pct");
				sPoPct =sPoPct ==null?"":sPoPct.trim();
				oVendorDetailsVO.setPoPct(sPoPct);
				
				String sAboutMePath = rs.getString("about_me_path");
				sAboutMePath =sAboutMePath ==null?"":sAboutMePath.trim();
				oVendorDetailsVO.setAboutMePath(sAboutMePath);
				
				String sAboutMeType = rs.getString("about_me_type");
				sAboutMeType =sAboutMeType ==null?"":sAboutMeType.trim();
				oVendorDetailsVO.setAboutMeType(sAboutMeType);
				
			}
			logger.info("VendorDAO::getVendorDetailsById::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::getVendorDetailsById::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return oVendorDetailsVO;
	}
	public static int addVendorDetails(VendorDetailsForm p_oVendorDetailsForm, String p_sLoginId,String p_sKeyRefId) throws Exception{	
		logger.info("VendorDAO::addVendorDetails::ENTER");		
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_vendor_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		logger.info("VendorDAO::addVendorDetails::SQL QUERY "+sQry);			
		logger.info("VendorDAO::addVendorDetails::Vendor Name "+p_oVendorDetailsForm.getVendName());		
		logger.info("VendorDAO::addVendorDetails::Vendor Type "+p_oVendorDetailsForm.getVendType());		
		logger.info("VendorDAO::addVendorDetails::Vendor Status "+p_oVendorDetailsForm.getVendStatus());
		logger.info("VendorDAO::addVendorDetails::Vendor Address 1 "+p_oVendorDetailsForm.getVendAddr1());
		logger.info("VendorDAO::addVendorDetails::Vendor Address 2 "+p_oVendorDetailsForm.getVendAddr2());
		logger.info("VendorDAO::addVendorDetails::Vendor City "+p_oVendorDetailsForm.getVendCity());
		logger.info("VendorDAO::addVendorDetails::Vendor State "+p_oVendorDetailsForm.getVendState());
		logger.info("VendorDAO::addVendorDetails::Vendor Country "+p_oVendorDetailsForm.getVendCountry());
		logger.info("VendorDAO::addVendorDetails::Vendor Zip "+p_oVendorDetailsForm.getVendZip());
		logger.info("VendorDAO::addVendorDetails::Vendor Phone 1"+p_oVendorDetailsForm.getVendPhone1());
		logger.info("VendorDAO::addVendorDetails::Vendor Phone Extn 1"+p_oVendorDetailsForm.getVendPhoneExt1());
		logger.info("VendorDAO::addVendorDetails::Vendor Phone 2 "+p_oVendorDetailsForm.getVendPhone2());
		logger.info("VendorDAO::addVendorDetails::Vendor Phone Extn 2 "+p_oVendorDetailsForm.getVendPhoneExt2());
		logger.info("VendorDAO::addVendorDetails::Vendor Email "+p_oVendorDetailsForm.getVendEmail());
		logger.info("VendorDAO::addVendorDetails::Vendor Fax "+p_oVendorDetailsForm.getVendFax());	
		logger.info("VendorDAO::addVendorDetails::Vendor Contact Name "+CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName()));	
		logger.info("VendorDAO::addVendorDetails::Vendor Return Policy "+p_oVendorDetailsForm.getVendReturnPolicy());	
		logger.info("VendorDAO::addVendorDetails::Vendor Comments "+p_oVendorDetailsForm.getVendComments().trim());	
		logger.info("VendorDAO::addVendorDetails::Vendor Contact Name 2 "+p_oVendorDetailsForm.getVendContactName2());
		logger.info("VendorDAO::addVendorDetails::Vendor Service Phone No "+p_oVendorDetailsForm.getCustServicePhoneno());
		logger.info("VendorDAO::addVendorDetails::Vendor Service Phone No Ext "+p_oVendorDetailsForm.getCustServicePhonenoExt());
		logger.info("VendorDAO::addVendorDetails::Vendor Service Email "+p_oVendorDetailsForm.getCustServiceEmail());
		logger.info("VendorDAO::addVendorDetails::Vendor Mobile 1 "+p_oVendorDetailsForm.getVendMobile1());
		logger.info("VendorDAO::addVendorDetails::Vendor Mobile 2 "+p_oVendorDetailsForm.getVendMobile2());
		logger.info("VendorDAO::addVendorDetails::Vendor Email 2 "+p_oVendorDetailsForm.getVendEmail2());
		logger.info("VendorDAO::addVendorDetails::Vendor Charge Type "+p_oVendorDetailsForm.getChargeType());		
		logger.info("VendorDAO::addVendorDetails::Vendor Return Address 1 "+p_oVendorDetailsForm.getReturnAddr1());
		logger.info("VendorDAO::addVendorDetails::Vendor Return Address 2 "+p_oVendorDetailsForm.getReturnAddr2());
		logger.info("VendorDAO::addVendorDetails::Vendor Return City "+p_oVendorDetailsForm.getReturnCity());
		logger.info("VendorDAO::addVendorDetails::Vendor Return State "+p_oVendorDetailsForm.getReturnState());
		logger.info("VendorDAO::addVendorDetails::Vendor Return Country "+p_oVendorDetailsForm.getReturnCountry());
		logger.info("VendorDAO::addVendorDetails::Vendor Return Zip "+p_oVendorDetailsForm.getReturnZip());
		logger.info("VendorDAO::addVendorDetails::Vendor Return Days "+p_oVendorDetailsForm.getReturnDays());
		logger.info("VendorDAO::addVendorDetails::Vendor Login Id "+p_sLoginId);
		logger.info("VendorDAO::addVendorDetails::Key Reference Id "+p_sKeyRefId);
		int iVendId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.setString(2,p_oVendorDetailsForm.getVendName());
			cstmt.setString(3,p_oVendorDetailsForm.getVendType());
			cstmt.setString(4,p_oVendorDetailsForm.getVendStatus());
			cstmt.setString(5,p_oVendorDetailsForm.getVendAddr1());
			cstmt.setString(6,p_oVendorDetailsForm.getVendAddr2());
			cstmt.setString(7,p_oVendorDetailsForm.getVendCity());
			cstmt.setString(8,p_oVendorDetailsForm.getVendState());
			cstmt.setString(9,p_oVendorDetailsForm.getVendCountry());
			cstmt.setString(10,p_oVendorDetailsForm.getVendZip());
			cstmt.setString(11,p_oVendorDetailsForm.getVendPhone1());
			cstmt.setString(12,p_oVendorDetailsForm.getVendPhoneExt1());
			cstmt.setString(13,p_oVendorDetailsForm.getVendPhone2());
			cstmt.setString(14,p_oVendorDetailsForm.getVendPhoneExt2());
			cstmt.setString(15,p_oVendorDetailsForm.getVendEmail());
			cstmt.setString(16,p_oVendorDetailsForm.getVendFax());	
			cstmt.setString(17,CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName()));	
			cstmt.setString(18,p_oVendorDetailsForm.getVendUserId().trim());	
			cstmt.setString(19,p_oVendorDetailsForm.getVendPassword().trim());	
			cstmt.setString(20,p_oVendorDetailsForm.getVendReturnPolicy());	
			cstmt.setString(21,p_oVendorDetailsForm.getVendComments().trim());	
			cstmt.setString(22,p_oVendorDetailsForm.getVendContactName2());
			cstmt.setString(23,p_oVendorDetailsForm.getCustServicePhoneno());
			cstmt.setString(24,p_oVendorDetailsForm.getCustServicePhonenoExt());
			cstmt.setString(25,p_oVendorDetailsForm.getCustServiceEmail());
			cstmt.setString(26,p_oVendorDetailsForm.getVendMobile1());
			cstmt.setString(27,p_oVendorDetailsForm.getVendMobile2());
			cstmt.setString(28,p_oVendorDetailsForm.getVendEmail2());
			cstmt.setString(29,p_oVendorDetailsForm.getChargeType());		
			cstmt.setString(30,p_oVendorDetailsForm.getReturnAddr1());
			cstmt.setString(31,p_oVendorDetailsForm.getReturnAddr2());
			cstmt.setString(32,p_oVendorDetailsForm.getReturnCity());
			cstmt.setString(33,p_oVendorDetailsForm.getReturnState());
			cstmt.setString(34,p_oVendorDetailsForm.getReturnCountry());
			cstmt.setString(35,p_oVendorDetailsForm.getReturnZip());
			cstmt.setString(36,p_oVendorDetailsForm.getReturnDays());
			cstmt.setString(37,p_oVendorDetailsForm.getPoPct());
			cstmt.setString(38,p_sLoginId);
			cstmt.setString(39,p_sKeyRefId);
			cstmt.execute();
			iVendId=cstmt.getInt(1);
			
			logger.info("VendorDAO::addVendorDetails::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::addVendorDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return iVendId;
	}
	public static int updateVendorDetails(VendorDetailsForm p_oVendorDetailsForm,String p_sUserType, String p_sLoginUserId,String p_sUserTableId, String p_sRefId) throws Exception{	
		logger.info("VendorDAO::updateVendorDetails::ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_vendor_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		logger.info("VendorDAO::updateVendorDetails::SQL QUERY "+sQry);
		logger.info("VendorDAO::updateVendorDetails::Vendor Id "+p_oVendorDetailsForm.getVendId());		
		logger.info("VendorDAO::updateVendorDetails::Vendor Name "+p_oVendorDetailsForm.getVendName());		
		logger.info("VendorDAO::updateVendorDetails::Vendor Vend Type "+p_oVendorDetailsForm.getVendType());		
		logger.info("VendorDAO::updateVendorDetails::Vendor Status "+p_oVendorDetailsForm.getVendStatus());
		logger.info("VendorDAO::updateVendorDetails::Vendor Address 1 "+p_oVendorDetailsForm.getVendAddr1());
		logger.info("VendorDAO::updateVendorDetails::Vendor Address 2 "+p_oVendorDetailsForm.getVendAddr2());
		logger.info("VendorDAO::updateVendorDetails::Vendor City "+p_oVendorDetailsForm.getVendCity());
		logger.info("VendorDAO::updateVendorDetails::Vendor State "+p_oVendorDetailsForm.getVendState());
		logger.info("VendorDAO::updateVendorDetails::Vendor Country "+p_oVendorDetailsForm.getVendCountry());
		logger.info("VendorDAO::updateVendorDetails::Vendor Zip "+p_oVendorDetailsForm.getVendZip());
		logger.info("VendorDAO::updateVendorDetails::Vendor Phone 1 "+p_oVendorDetailsForm.getVendPhone1());
		logger.info("VendorDAO::updateVendorDetails::Vendor Phone No Extn 1 "+p_oVendorDetailsForm.getVendPhoneExt1());
		logger.info("VendorDAO::updateVendorDetails::Vendor Phone 2 "+p_oVendorDetailsForm.getVendPhone2());
		logger.info("VendorDAO::updateVendorDetails::Vendor Phone Extn 2 "+p_oVendorDetailsForm.getVendPhoneExt2());
		logger.info("VendorDAO::updateVendorDetails::Vendor Email "+p_oVendorDetailsForm.getVendEmail());
		logger.info("VendorDAO::updateVendorDetails::Vendor Fax "+p_oVendorDetailsForm.getVendFax());	
		logger.info("VendorDAO::updateVendorDetails::Vendor Contact Name "+CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName()));	
		logger.info("VendorDAO::updateVendorDetails::Vendor User Type "+p_sUserType);	
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Policy "+p_oVendorDetailsForm.getVendReturnPolicy());	
		logger.info("VendorDAO::updateVendorDetails::Vendor Comments "+p_oVendorDetailsForm.getVendComments());	
		logger.info("VendorDAO::updateVendorDetails::Vendor Contact Name 2 "+CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName2()));
		logger.info("VendorDAO::updateVendorDetails::Vendor Service Phone No "+p_oVendorDetailsForm.getCustServicePhoneno());
		logger.info("VendorDAO::updateVendorDetails::Vendor Service Phone No Extn "+p_oVendorDetailsForm.getCustServicePhonenoExt());
		logger.info("VendorDAO::updateVendorDetails::Vendor Service Email "+p_oVendorDetailsForm.getCustServiceEmail());
		logger.info("VendorDAO::updateVendorDetails::Vendor Mobile 1 "+p_oVendorDetailsForm.getVendMobile1());
		logger.info("VendorDAO::updateVendorDetails::Vendor Mobile 2 "+p_oVendorDetailsForm.getVendMobile2());
		logger.info("VendorDAO::updateVendorDetails::Vendor Email 2 "+p_oVendorDetailsForm.getVendEmail2());
		logger.info("VendorDAO::updateVendorDetails::Vendor Charge Type "+p_oVendorDetailsForm.getChargeType());			
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Address 1 "+p_oVendorDetailsForm.getReturnAddr1());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Address 2 "+p_oVendorDetailsForm.getReturnAddr2());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return City "+p_oVendorDetailsForm.getReturnCity());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return State "+p_oVendorDetailsForm.getReturnState());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Country "+p_oVendorDetailsForm.getReturnCountry());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Zip "+p_oVendorDetailsForm.getReturnZip());
		logger.info("VendorDAO::updateVendorDetails::Vendor Return Days "+p_oVendorDetailsForm.getReturnDays());
		logger.info("VendorDAO::updateVendorDetails::Login Id "+p_sLoginUserId);
		logger.info("VendorDAO::updateVendorDetails::Key Ref Id "+p_sRefId);
		
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oVendorDetailsForm.getVendId());
			cstmt.setString(3,p_oVendorDetailsForm.getVendName());
			cstmt.setString(4,p_oVendorDetailsForm.getVendType());
			cstmt.setString(5,p_oVendorDetailsForm.getVendStatus());
			cstmt.setString(6,p_oVendorDetailsForm.getVendAddr1());
			cstmt.setString(7,p_oVendorDetailsForm.getVendAddr2());
			cstmt.setString(8,p_oVendorDetailsForm.getVendCity());
			cstmt.setString(9,p_oVendorDetailsForm.getVendState());
			cstmt.setString(10,p_oVendorDetailsForm.getVendCountry());
			cstmt.setString(11,p_oVendorDetailsForm.getVendZip());
			cstmt.setString(12,p_oVendorDetailsForm.getVendPhone1());
			cstmt.setString(13,p_oVendorDetailsForm.getVendPhoneExt1());
			cstmt.setString(14,p_oVendorDetailsForm.getVendPhone2());
			cstmt.setString(15,p_oVendorDetailsForm.getVendPhoneExt2());
			cstmt.setString(16,p_oVendorDetailsForm.getVendEmail());
			cstmt.setString(17,p_oVendorDetailsForm.getVendFax());	
			cstmt.setString(18,CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName()));	
			cstmt.setString(19,p_oVendorDetailsForm.getVendUserId());	
			cstmt.setString(20,p_oVendorDetailsForm.getVendPassword().trim());	
			cstmt.setString(21,p_sUserType);	
			cstmt.setString(22,p_oVendorDetailsForm.getVendReturnPolicy());	
			cstmt.setString(23,p_oVendorDetailsForm.getVendComments());	
			cstmt.setString(24,CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName2()));
			cstmt.setString(25,p_oVendorDetailsForm.getCustServicePhoneno());
			cstmt.setString(26,p_oVendorDetailsForm.getCustServicePhonenoExt());
			cstmt.setString(27,p_oVendorDetailsForm.getCustServiceEmail());
			cstmt.setString(28,p_oVendorDetailsForm.getVendMobile1());
			cstmt.setString(29,p_oVendorDetailsForm.getVendMobile2());
			cstmt.setString(30,p_oVendorDetailsForm.getVendEmail2());
			cstmt.setString(31,p_oVendorDetailsForm.getChargeType());			
			cstmt.setString(32,p_oVendorDetailsForm.getReturnAddr1());
			cstmt.setString(33,p_oVendorDetailsForm.getReturnAddr2());
			cstmt.setString(34,p_oVendorDetailsForm.getReturnCity());
			cstmt.setString(35,p_oVendorDetailsForm.getReturnState());
			cstmt.setString(36,p_oVendorDetailsForm.getReturnCountry());
			cstmt.setString(37,p_oVendorDetailsForm.getReturnZip());
			cstmt.setString(38,p_oVendorDetailsForm.getReturnDays());
			cstmt.setString(39,p_oVendorDetailsForm.getPoPct());
			cstmt.setString(40,p_sLoginUserId);
			cstmt.setString(41,p_sUserTableId);
			cstmt.setString(42,p_sRefId);
			
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::updateVendorDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		logger.info("VendorDAO::updateVendorDetails::EXIT");
		return iIsUpdate;
	}
	public static ArrayList validateVendor(String p_sVendName,String p_sVendorUserId,String p_sVendId,String p_sMode) throws Exception{	
		logger.info("VendorDAO::validateVendor::ENTER");		
			
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int iVendNameCnt = -1;
		ArrayList alVendorRegStatus = new ArrayList();
		
		String sQry="{call usp_sbh_validate_vendor(?,?,?,?,?,?)}";
	
		logger.info("VendorDAO::validateVendor::SQL QUERY "+sQry);	
		logger.info("VendorDAO::validateVendor::Vendor Name"+p_sVendName);
		logger.info("VendorDAO::validateVendor::Vendor User Id"+p_sVendorUserId);
		logger.info("VendorDAO::validateVendor::Vendor Id"+p_sVendId);
		logger.info("VendorDAO::validateVendor::Mode"+p_sMode);
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);	
			cstmt.registerOutParameter(2, Types.INTEGER);	
			cstmt.setString(3,p_sVendName);				
			cstmt.setString(4,p_sVendorUserId);	
			cstmt.setString(5,p_sVendId);	
			cstmt.setString(6,p_sMode);				
			cstmt.execute();
			
			alVendorRegStatus.add(0,cstmt.getInt(1));
			alVendorRegStatus.add(1,cstmt.getInt(2));
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::validateVendor::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("VendorDAO::validateVendor::EXIT");
		return alVendorRegStatus;
	}
	
	public static boolean sendEmail(String p_sVendorName,String p_sContactName,String p_sUserId,String p_sPassword,String p_sEmail,String p_sSecondaryEmail,long p_lOwnerId,String p_sOwnerType,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("VendorDAO:sendEmail:ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null,sToaddr="",sEmailTemplateId =null,sEmailFromAddr=null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sSkyBuyURL=null;
		HashMap p_hmTags =new HashMap();
		EmailVO oEmailVO = null;
		String sBccAddr="",sCcAddr="";
		String sOwnerType = "";
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId = oBundle.getString("email.registration.templateId");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
			
			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
					
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();
			
			if(p_sOwnerType.equalsIgnoreCase("airline")){
				sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
				sOwnerType = "Air Charter";
			}else if(p_sOwnerType.equalsIgnoreCase("vendor")){
				sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
				sOwnerType = "Vendor";
			}
			sSkyBuyURL=sSkyBuyURL==null?"":sSkyBuyURL.trim();
			
			
			sToaddr=p_sEmail;
			

			
			if(p_sSecondaryEmail!=null && !p_sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToaddr.contains(p_sSecondaryEmail)){
					sToaddr = sToaddr + ","+p_sSecondaryEmail;
				}
			}
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header		
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{VendorName}}",Utils.toTitleCase(p_sVendorName));
			p_hmTags.put("{{UserName}}",Utils.toTitleCase(p_sContactName));
			p_hmTags.put("{{UserId}}",p_sUserId);
			p_hmTags.put("{{Password}}",p_sPassword);
			p_hmTags.put("{{SkyBuyURL}}",sSkyBuyURL);
			p_hmTags.put("{{OwnerType}}",sOwnerType);
			
			
			if(oEmailVO!=null){
				
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailSubject = sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject =replaceTagValues(p_hmTags,sEmailSubject,null,null);			
				
				sEmailContent = oEmailVO.getEmailContent();
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
				
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
								
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToaddr));
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			/*oEmailLogVo.setOwnerId(p_lOwnerId);
			oEmailLogVo.setOwnerType(p_sOwnerType);*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToaddr);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
			
			System.out.print("EmailContent "+sEmailContent);
			
			logger.info("VendorDAO::v:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("VendorDAO:sendEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	
	public static boolean sendStatusEmail(String p_sVendStatus,String p_sVendorName,String p_sContactName,String p_sEmail,long p_lOwnerId,String p_sOwnerType,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("VendorDAO:sendEmail:ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null,sToaddr="",sEmailTemplateId =null,sEmailFromAddr=null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sSkyBuyURL=null;
		HashMap p_hmTags =new HashMap();
		EmailVO oEmailVO = null;
		String sBccAddr="",sCcAddr="";
		
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			if(p_sVendStatus!= null && p_sVendStatus.equalsIgnoreCase("A"))
				sEmailTemplateId = oBundle.getString("email.activate.account.templateId");
			else
				sEmailTemplateId = oBundle.getString("email.deactivate.account.templateId");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
			
			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
					
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();
			
			if(p_sOwnerType.equalsIgnoreCase("airline")){
				sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
			}else if(p_sOwnerType.equalsIgnoreCase("vendor")){
				sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
			}
			sSkyBuyURL=sSkyBuyURL==null?"":sSkyBuyURL.trim();
			
			
			sToaddr=p_sEmail;
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header		
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{VendorName}}",Utils.toTitleCase(p_sVendorName));
			p_hmTags.put("{{UserName}}",Utils.toTitleCase(p_sContactName));
			//p_hmTags.put("{{UserId}}",p_sUserId);
			//p_hmTags.put("{{Password}}",p_sPassword);
			p_hmTags.put("{{SkyBuyURL}}",sSkyBuyURL);
			
			
			if(oEmailVO!=null){
				
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailSubject = sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject =replaceTagValues(p_hmTags,sEmailSubject,null,null);			
				
				sEmailContent = oEmailVO.getEmailContent();
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);				

				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
								
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToaddr));

			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			/*oEmailLogVo.setOwnerId(p_lOwnerId);
			oEmailLogVo.setOwnerType(p_sOwnerType);*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToaddr);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
			
			System.out.print("EmailContent "+sEmailContent);
			
			logger.info("VendorDAO::v:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("VendorDAO:sendEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	public static boolean sendEditUserDetailsByEmail(String p_sVendorName,String p_sContactName,String p_sUserId,String p_sPassword,String p_sOldUserId,String p_sOldPassword,String p_sEmail,String p_sSecondEmail,long p_lOwnerId,String p_sOwnerType,String p_sLoginId,String p_sReqeustFrom,String p_sUserType) throws Exception{
		logger.info("VendorDAO:sendEditUserDetailsByEmail:ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null,sToaddr="",sEmailTemplateId =null,sEmailFromAddr=null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sSkyBuyURL=null,sUpdatedUserDetails ="";
		HashMap p_hmTags =new HashMap();
		EmailVO oEmailVO = null;
		String sBccAddr="",sCcAddr="";
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId = oBundle.getString("email.edit.registration.templateId");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
			
			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
					
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();
			
			if(p_sOwnerType.equalsIgnoreCase("airline")){
				sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
			}else if(p_sOwnerType.equalsIgnoreCase("vendor")){
				sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
			}
			sSkyBuyURL=sSkyBuyURL==null?"":sSkyBuyURL.trim();
			
			
			sToaddr=p_sEmail;
			
			/*if(p_sSecondEmail!=null && !p_sSecondEmail.equalsIgnoreCase("")){
				if(!p_sSecondEmail.equalsIgnoreCase(sToaddr)){
					sToaddr = sToaddr + ","+p_sSecondEmail;
				}
			}*/
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header		
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{VendorName}}",Utils.toTitleCase(p_sVendorName));
			p_hmTags.put("{{UserName}}",Utils.toTitleCase(p_sContactName));
			p_hmTags.put("{{UserId}}",p_sUserId);
			p_hmTags.put("{{Password}}",p_sPassword);
			p_hmTags.put("{{SkyBuyURL}}",sSkyBuyURL);
			if(p_sUserType.equalsIgnoreCase("admin")){
				p_hmTags.put("{{LoginUser}}","by SkyBuyHigh Admin");
			}else{
				p_hmTags.put("{{LoginUser}}","");
			}
			if(p_sUserType.equalsIgnoreCase("admin")){
				if(!p_sUserId.equalsIgnoreCase(p_sOldUserId) && !p_sPassword.equalsIgnoreCase(p_sOldPassword)){
					sUpdatedUserDetails = "Your <b>User Name</b> and <b>Password</b> has been modified by SkyBuy<sup>High</sup> Admin." ;
				}
				else if(!p_sUserId.equalsIgnoreCase(p_sOldUserId)){
					sUpdatedUserDetails = "Your <b>User Name</b> has been modified by SkyBuy<sup>High</sup> Admin." ;
				}
				else if(!p_sPassword.equalsIgnoreCase(p_sOldPassword)){
					sUpdatedUserDetails = "Your <b>Password</b> has been modified by SkyBuy<sup>High</sup> Admin." ;
				}
			}else if(p_sUserType.equalsIgnoreCase("vendor") || p_sUserType.equalsIgnoreCase("airline")){
				if(!p_sUserId.equalsIgnoreCase(p_sOldUserId) && !p_sPassword.equalsIgnoreCase(p_sOldPassword)){
					sUpdatedUserDetails = "Your <b>User Name</b> and <b>Password</b> has been modified by "+Utils.toTitleCase(p_sContactName)+"." ;
				}
				else if(!p_sUserId.equalsIgnoreCase(p_sOldUserId)){
					sUpdatedUserDetails = "Your <b>User Name</b> has been modified by  "+Utils.toTitleCase(p_sContactName)+"." ;
				}
				else if(!p_sPassword.equalsIgnoreCase(p_sOldPassword)){
					sUpdatedUserDetails = "Your <b>Password</b> has been modified by  "+Utils.toTitleCase(p_sContactName)+"."  ;
				}
			}
			p_hmTags.put("{{UserIdAndPassword}}",sUpdatedUserDetails);
			
			if(oEmailVO!=null){
				
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailSubject = sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject =replaceTagValues(p_hmTags,sEmailSubject,null,null);			
				
				sEmailContent = oEmailVO.getEmailContent();
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);	
				
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			

								
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToaddr));

			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));	
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			/*oEmailLogVo.setOwnerId(p_lOwnerId);
			oEmailLogVo.setOwnerType(p_sOwnerType);*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToaddr);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
			
			System.out.print("EmailContent "+sEmailContent);
			
			logger.info("VendorDAO::sendEditUserDetailsByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("VendorDAO:sendEditUserDetailsByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	public static void insertUserAuditDetails(String p_sOwnerId,String p_sOwnerType,String p_sCurrentUserDetails,String p_sPreviousUserDetails,String p_sModifiedFields,String p_sComments,String p_sCreateId,String p_sProcess) throws Exception{	
		logger.info("VendorDAO::insertUserAuditDetails::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_user_audit_msg(?,?,?,?,?,?,?,?)}";
		logger.info("VendorDAO::insertUserAuditDetails::SQL QUERY "+sQry);	
		logger.info("VendorDAO::insertUserAuditDetails::Owner Id : "+p_sOwnerId);
		logger.info("VendorDAO::insertUserAuditDetails::Owner Type :"+p_sOwnerType);
		logger.info("VendorDAO::insertUserAuditDetails::Current User Details :"+p_sCurrentUserDetails);
		logger.info("VendorDAO::insertUserAuditDetails::Previous User Details :"+p_sPreviousUserDetails);
		logger.info("VendorDAO::insertUserAuditDetails::Modified Fields :"+p_sModifiedFields);		
		logger.info("VendorDAO::insertUserAuditDetails::Msg :"+p_sComments);
		logger.info("VendorDAO::insertUserAuditDetails::Create Id :"+p_sCreateId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sProcess);
			cstmt.setString(2,p_sOwnerId);
			cstmt.setString(3,p_sOwnerType);
			cstmt.setString(4,p_sCurrentUserDetails);
			cstmt.setString(5,p_sPreviousUserDetails);
			cstmt.setString(6,p_sModifiedFields);
			cstmt.setString(7,p_sComments);
			cstmt.setString(8,p_sCreateId);			
			cstmt.execute();			
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::insertUserAuditDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();						
		}
		logger.info("VendorDAO::insertUserAuditDetails::EXIT");
	}
	public static String getInsertedVendorDetails(VendorDetailsForm p_oVendorDetailsForm) throws Exception{	
		logger.info("VendorDAO::getInsertedVendorDetails::ENTER");			
		String sInsertedUserDetails = "";
		try{
			if(p_oVendorDetailsForm!=null){
				sInsertedUserDetails = "User Id:"+p_oVendorDetailsForm.getVendUserId().trim()+"|";
				//sInsertedUserDetails = sInsertedUserDetails+"Password:"+p_oVendorDetailsForm.getVendPassword().trim()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Name:"+p_oVendorDetailsForm.getVendName()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName())+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Vend Type:"+p_oVendorDetailsForm.getVendType()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Vend Status:"+p_oVendorDetailsForm.getVendStatus()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Charge Type:"+p_oVendorDetailsForm.getChargeType()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Company Address:"+p_oVendorDetailsForm.getVendAddr1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Additonal Address:"+p_oVendorDetailsForm.getVendAddr2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"City:"+p_oVendorDetailsForm.getVendCity()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"State:"+p_oVendorDetailsForm.getVendState()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Country:"+p_oVendorDetailsForm.getVendCountry()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Zip:"+p_oVendorDetailsForm.getVendZip()+"|";					
				sInsertedUserDetails = sInsertedUserDetails+"Phone1:"+p_oVendorDetailsForm.getVendPhone1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Ext1:"+p_oVendorDetailsForm.getVendPhoneExt1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Mobile1:"+p_oVendorDetailsForm.getVendMobile1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Email:"+p_oVendorDetailsForm.getVendEmail()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Fax:"+p_oVendorDetailsForm.getVendFax()+"|";
				
				sInsertedUserDetails = sInsertedUserDetails+"Contact Name2:"+p_oVendorDetailsForm.getVendContactName2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Phone2:"+p_oVendorDetailsForm.getVendPhone2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Ext2:"+p_oVendorDetailsForm.getVendPhoneExt2()+"|";			
				sInsertedUserDetails = sInsertedUserDetails+"Mobile2:"+p_oVendorDetailsForm.getVendMobile2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Email2:"+p_oVendorDetailsForm.getVendEmail2()+"|";	
			
				sInsertedUserDetails = sInsertedUserDetails+"Return Policy:"+p_oVendorDetailsForm.getVendReturnPolicy()+"|";				
				sInsertedUserDetails = sInsertedUserDetails+"Comments:"+p_oVendorDetailsForm.getVendComments().trim()+"|";
			
				sInsertedUserDetails = sInsertedUserDetails+"Cust Service Phoneno:"+p_oVendorDetailsForm.getCustServicePhoneno()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"CustService Phoneno Ext:"+p_oVendorDetailsForm.getCustServicePhonenoExt()+"|";				
				sInsertedUserDetails = sInsertedUserDetails+"Cust Service Email:"+p_oVendorDetailsForm.getCustServiceEmail()+"|";
				
				sInsertedUserDetails = sInsertedUserDetails+"Return Company Address:"+p_oVendorDetailsForm.getReturnAddr1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Return Additonal Address:"+p_oVendorDetailsForm.getReturnAddr2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Return City:"+p_oVendorDetailsForm.getReturnCity()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Return State:"+p_oVendorDetailsForm.getReturnState()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Return Country:"+p_oVendorDetailsForm.getReturnCountry()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Return Zip:"+p_oVendorDetailsForm.getReturnZip()+"|";			
				sInsertedUserDetails = sInsertedUserDetails+"Return Days:"+p_oVendorDetailsForm.getReturnDays()+"|";
				
				
			}
			// System.out.println("getInsertedVendorDetails:"+sInsertedUserDetails);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.info("VendorDAO::getInsertedVendorDetails::Exception "+e.getMessage());
			throw e;
		}
		

		logger.info("VendorDAO::getInsertedVendorDetails::EXIT");
		return sInsertedUserDetails;

	}
	public static void getUpdatedUserDetails(VendorDetailsForm p_oVendorDetailsForm,VendorDetailsVO p_oVendorDetailsVO,LoginVO p_oLoginVO, String p_sRefId) throws Exception{	
		logger.info("VendorDAO::getUpdatedUserDetails::ENTER");			
		
		String sCurrentUserDetails = "",sPreviousUserDetails = "" ,sModifiedFields ="";		
	
		//get Current user details
		if(p_oVendorDetailsForm!=null){
			sCurrentUserDetails = "User Id:"+p_oVendorDetailsForm.getVendUserId().trim()+"|";
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				sCurrentUserDetails = "User Id:"+p_oVendorDetailsForm.getVendUserId().trim()+"|";
				//sCurrentUserDetails = sCurrentUserDetails+"Password:"+p_oVendorDetailsForm.getVendPassword().trim()+"|";
			}
			sCurrentUserDetails = sCurrentUserDetails+"Name:"+p_oVendorDetailsForm.getVendName()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName())+"|";
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				sCurrentUserDetails = sCurrentUserDetails+"Vend Type:"+p_oVendorDetailsForm.getVendType()+"|";
			}
			sCurrentUserDetails = sCurrentUserDetails+"Vend Status:"+p_oVendorDetailsForm.getVendStatus()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Charge Type:"+p_oVendorDetailsForm.getChargeType()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Company Address:"+p_oVendorDetailsForm.getVendAddr1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Additonal Address:"+p_oVendorDetailsForm.getVendAddr2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"City:"+p_oVendorDetailsForm.getVendCity()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"State:"+p_oVendorDetailsForm.getVendState()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Country:"+p_oVendorDetailsForm.getVendCountry()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Zip:"+p_oVendorDetailsForm.getVendZip()+"|";					
			sCurrentUserDetails = sCurrentUserDetails+"Phone1:"+p_oVendorDetailsForm.getVendPhone1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Ext1:"+p_oVendorDetailsForm.getVendPhoneExt1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Mobile1:"+p_oVendorDetailsForm.getVendMobile1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Email:"+p_oVendorDetailsForm.getVendEmail()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Fax:"+p_oVendorDetailsForm.getVendFax()+"|";
			
			sCurrentUserDetails = sCurrentUserDetails+"Contact Name2:"+p_oVendorDetailsForm.getVendContactName2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Phone2:"+p_oVendorDetailsForm.getVendPhone2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Ext2:"+p_oVendorDetailsForm.getVendPhoneExt2()+"|";			
			sCurrentUserDetails = sCurrentUserDetails+"Mobile2:"+p_oVendorDetailsForm.getVendMobile2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Email2:"+p_oVendorDetailsForm.getVendEmail2()+"|";	
		
			sCurrentUserDetails = sCurrentUserDetails+"Return Policy:"+p_oVendorDetailsForm.getVendReturnPolicy()+"|";				
			sCurrentUserDetails = sCurrentUserDetails+"Comments:"+p_oVendorDetailsForm.getVendComments().trim()+"|";
		
			sCurrentUserDetails = sCurrentUserDetails+"Cust Service Phoneno:"+p_oVendorDetailsForm.getCustServicePhoneno()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Cust Service Phoneno Ext:"+p_oVendorDetailsForm.getCustServicePhonenoExt()+"|";				
			sCurrentUserDetails = sCurrentUserDetails+"Cust Service Email:"+p_oVendorDetailsForm.getCustServiceEmail()+"|";
			
			sCurrentUserDetails = sCurrentUserDetails+"Return Company Address:"+p_oVendorDetailsForm.getReturnAddr1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Additonal Address:"+p_oVendorDetailsForm.getReturnAddr2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return City:"+p_oVendorDetailsForm.getReturnCity()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return State:"+p_oVendorDetailsForm.getReturnState()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Country:"+p_oVendorDetailsForm.getReturnCountry()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Zip:"+p_oVendorDetailsForm.getReturnZip()+"|";			
			sCurrentUserDetails = sCurrentUserDetails+"Return Days:"+p_oVendorDetailsForm.getReturnDays()+"|";
			
			sCurrentUserDetails = sCurrentUserDetails+"Comments:"+p_oVendorDetailsForm.getVendComments()+"|";
		}
		
		//get previous user details
		if(p_oVendorDetailsVO!=null){
			
			sPreviousUserDetails = "User Id:"+p_oVendorDetailsVO.getVendUserId().trim()+"|";
			
			sPreviousUserDetails = sPreviousUserDetails+"Name:"+p_oVendorDetailsForm.getVendName()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oVendorDetailsVO.getVendContactName())+"|";
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				sPreviousUserDetails = sPreviousUserDetails+"Vend Type:"+p_oVendorDetailsVO.getVendType()+"|";
			}	
			sPreviousUserDetails = sPreviousUserDetails+"Vend Status:"+p_oVendorDetailsVO.getVendStatus()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Charge Type:"+p_oVendorDetailsVO.getChargeType()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Company Address:"+p_oVendorDetailsVO.getVendAddr1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Additonal Address:"+p_oVendorDetailsVO.getVendAddr2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"City:"+p_oVendorDetailsVO.getVendCity()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"State:"+p_oVendorDetailsVO.getVendState()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Country:"+p_oVendorDetailsVO.getVendCountry()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Zip:"+p_oVendorDetailsVO.getVendZip()+"|";					
			sPreviousUserDetails = sPreviousUserDetails+"Phone1:"+p_oVendorDetailsVO.getVendPhone1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Ext1:"+p_oVendorDetailsVO.getVendPhoneExt1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Mobile1:"+p_oVendorDetailsVO.getVendMobile1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Email:"+p_oVendorDetailsVO.getVendEmail()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Fax:"+p_oVendorDetailsVO.getVendFax()+"|";
			
			sPreviousUserDetails = sPreviousUserDetails+"Contact Name2:"+p_oVendorDetailsVO.getVendContactName2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Phone2:"+p_oVendorDetailsVO.getVendPhone2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Ext2:"+p_oVendorDetailsVO.getVendPhoneExt2()+"|";			
			sPreviousUserDetails = sPreviousUserDetails+"Mobile2:"+p_oVendorDetailsVO.getVendMobile2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Email2:"+p_oVendorDetailsVO.getVendEmail2()+"|";	
		
			sPreviousUserDetails = sPreviousUserDetails+"Return Policy:"+p_oVendorDetailsVO.getVendReturnPolicy()+"|";				
			sPreviousUserDetails = sPreviousUserDetails+"Comments:"+p_oVendorDetailsVO.getVendComments().trim()+"|";
		
			sPreviousUserDetails = sPreviousUserDetails+"Cust Service Phoneno:"+p_oVendorDetailsVO.getCustServicePhoneno()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"CustService Phoneno Ext:"+p_oVendorDetailsVO.getCustServicePhonenoExt()+"|";				
			sPreviousUserDetails = sPreviousUserDetails+"Cust Service Email:"+p_oVendorDetailsVO.getCustServiceEmail()+"|";
			
			sPreviousUserDetails = sPreviousUserDetails+"Return Company Address:"+p_oVendorDetailsVO.getReturnAddr1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Additonal Address:"+p_oVendorDetailsVO.getReturnAddr2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return City:"+p_oVendorDetailsVO.getReturnCity()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return State:"+p_oVendorDetailsVO.getReturnState()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Country:"+p_oVendorDetailsVO.getReturnCountry()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Zip:"+p_oVendorDetailsVO.getReturnZip()+"|";			
			sPreviousUserDetails = sPreviousUserDetails+"Return Days:"+p_oVendorDetailsVO.getReturnDays()+"|";
			
		}
		
			
		//get modified fields name
		
		if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
			if(!p_oVendorDetailsForm.getVendUserId().equals(p_oVendorDetailsVO.getVendUserId())){		
				sModifiedFields =sModifiedFields+"User Id|";	
			}
			
			
			if(!p_oVendorDetailsForm.getPoPct().equals(p_oVendorDetailsVO.getPoPct())){		
				sModifiedFields =sModifiedFields+"PO Percentage|";	
			}
		}
		if(p_oVendorDetailsForm.getVendPassword()!=null && p_oVendorDetailsForm.getVendPassword().trim().length()>0){
			if(!decryptInputData(p_oVendorDetailsForm.getVendPassword(),"SERVER",p_sRefId).equals(p_oVendorDetailsVO.getVendPassword())){		
				sModifiedFields =sModifiedFields+"Password|";	
			}
		}
		
		if(!p_oVendorDetailsForm.getVendName().equals(p_oVendorDetailsVO.getVendName())){		
			sModifiedFields =sModifiedFields+"Name|";	
		}
		if(!CatalogueDAO.toTitleCase(p_oVendorDetailsForm.getVendContactName()).equals(p_oVendorDetailsVO.getVendContactName())){		
			sModifiedFields =sModifiedFields+"Contact Name|";	
		}
		if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
			if(!p_oVendorDetailsForm.getVendType().equals(p_oVendorDetailsVO.getVendType())){		
				sModifiedFields =sModifiedFields+"Vend Type|";	
			}
		}
		if(!p_oVendorDetailsForm.getVendStatus().equals(p_oVendorDetailsVO.getVendStatus())){		
			sModifiedFields =sModifiedFields+"Vend Status|";	
		}
		if(!p_oVendorDetailsForm.getChargeType().equals(p_oVendorDetailsVO.getChargeType())){		
			sModifiedFields =sModifiedFields+"Charge Type|";	
		}
		if(!p_oVendorDetailsForm.getVendAddr1().equals(p_oVendorDetailsVO.getVendAddr1())){		
			sModifiedFields =sModifiedFields+"Company Address|";	
		}
		if(!p_oVendorDetailsForm.getVendAddr2().equals(p_oVendorDetailsVO.getVendAddr2())){		
			sModifiedFields =sModifiedFields+"Additonal Address|";	
		}
		if(!p_oVendorDetailsForm.getVendCity().equals(p_oVendorDetailsVO.getVendCity())){		
			sModifiedFields =sModifiedFields+"City|";	
		}
		if(!p_oVendorDetailsForm.getVendState().equals(p_oVendorDetailsVO.getVendState())){		
			sModifiedFields =sModifiedFields+"State|";	
		}
		if(!p_oVendorDetailsForm.getVendCountry().equals(p_oVendorDetailsVO.getVendCountry())){		
			sModifiedFields =sModifiedFields+"Country|";	
		}
		if(!p_oVendorDetailsForm.getVendZip().equals(p_oVendorDetailsVO.getVendZip())){		
			sModifiedFields =sModifiedFields+"Zip|";	
		}
		if(!p_oVendorDetailsForm.getVendPhone1().equals(p_oVendorDetailsVO.getVendPhone1())){		
			sModifiedFields =sModifiedFields+"Phone1|";	
		}
		if(!p_oVendorDetailsForm.getVendPhoneExt1().equals(p_oVendorDetailsVO.getVendPhoneExt1())){		
			sModifiedFields =sModifiedFields+"Ext1|";	
		}
		if(!p_oVendorDetailsForm.getVendMobile1().equals(p_oVendorDetailsVO.getVendMobile1())){		
			sModifiedFields =sModifiedFields+"Mobile1|";	
		}
		if(!p_oVendorDetailsForm.getVendEmail().equals(p_oVendorDetailsVO.getVendEmail())){		
			sModifiedFields =sModifiedFields+"Email|";	
		}
		if(!p_oVendorDetailsForm.getVendFax().equals(p_oVendorDetailsVO.getVendFax())){		
			sModifiedFields =sModifiedFields+"Fax|";	
		}
		
		if(!p_oVendorDetailsForm.getVendContactName2().equals(p_oVendorDetailsVO.getVendContactName2())){		
			sModifiedFields =sModifiedFields+"Contact Name2|";	
		}
		if(!p_oVendorDetailsForm.getVendPhone2().equals(p_oVendorDetailsVO.getVendPhone2())){		
			sModifiedFields =sModifiedFields+"Phone2|";	
		}
		if(!p_oVendorDetailsForm.getVendPhoneExt2().equals(p_oVendorDetailsVO.getVendPhoneExt2())){		
			sModifiedFields =sModifiedFields+"Ext2|";	
		}
		if(!p_oVendorDetailsForm.getVendMobile2().equals(p_oVendorDetailsVO.getVendMobile2())){		
			sModifiedFields =sModifiedFields+"Mobile2|";	
		}
		if(!p_oVendorDetailsForm.getVendEmail2().equals(p_oVendorDetailsVO.getVendEmail2())){		
			sModifiedFields =sModifiedFields+"Email2|";	
		}
		if(!p_oVendorDetailsForm.getVendReturnPolicy().equals(p_oVendorDetailsVO.getVendReturnPolicy())){		
			sModifiedFields =sModifiedFields+"Return Policy|";	
		}
		
		if(!p_oVendorDetailsForm.getCustServicePhoneno().trim().equals(p_oVendorDetailsVO.getCustServicePhoneno())){		
			sModifiedFields =sModifiedFields+"Cust Service Phoneno|";	
		}
		if(!p_oVendorDetailsForm.getCustServicePhonenoExt().trim().equals(p_oVendorDetailsVO.getCustServicePhonenoExt())){		
			sModifiedFields =sModifiedFields+"CustService Phoneno Ext|";	
		}
		if(!p_oVendorDetailsForm.getCustServiceEmail().trim().equals(p_oVendorDetailsVO.getCustServiceEmail())){		
			sModifiedFields =sModifiedFields+"Cust Service Email|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnAddr1().trim().equals(p_oVendorDetailsVO.getReturnAddr1())){		
			sModifiedFields =sModifiedFields+"Return Company Address|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnAddr2().trim().equals(p_oVendorDetailsVO.getReturnAddr2())){		
			sModifiedFields =sModifiedFields+"Return Additional Address|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnCity().trim().equals(p_oVendorDetailsVO.getReturnCity())){		
			sModifiedFields =sModifiedFields+"Return City|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnState().trim().equals(p_oVendorDetailsVO.getReturnState())){		
			sModifiedFields =sModifiedFields+"Return State|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnCountry().trim().equals(p_oVendorDetailsVO.getReturnCountry())){		
			sModifiedFields =sModifiedFields+"Return Country|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnZip().trim().equals(p_oVendorDetailsVO.getReturnZip())){		
			sModifiedFields =sModifiedFields+"Return Zip|";	
		}
		
		if(!p_oVendorDetailsForm.getReturnDays().trim().equals(p_oVendorDetailsVO.getReturnDays())){		
			sModifiedFields =sModifiedFields+"Return Days|";	
		}
		
		if(p_oVendorDetailsForm.getAboutMe()!=null && !p_oVendorDetailsForm.getAboutMe().getFileName().equals("")){
			sModifiedFields =sModifiedFields+"About Me Swf|";	
		}
		
		
		//update user audit details
		insertUserAuditDetails(p_oVendorDetailsForm.getVendId(),"vendor",sCurrentUserDetails, sPreviousUserDetails,sModifiedFields,p_oVendorDetailsForm.getVendComments(),p_oLoginVO.getUserId(),"VENDOR UPDATED");

		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("VendorDAO::getUpdatedUserDetails::EXIT");
	}
	
	public static List<PartnerRegistrationCommentsVO> getVendorComments(String p_sOwnerId, String p_sOwnerType) throws Exception{	
		logger.info("VendorDAO::getVendorComments::ENTER");	
		
		PartnerRegistrationCommentsVO oPartnerRegistrationCommentsVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_partner_comments(?,?)}";
		
		logger.debug("VendorDAO::getVendorComments:SQL QUERY "+sQry);
		logger.debug("VendorDAO::getVendorComments:Owner Id "+p_sOwnerId);
		logger.debug("VendorDAO::getVendorComments:Owner Type "+p_sOwnerType);	
		
		ResultSet rs=null;
		List<PartnerRegistrationCommentsVO> alAirlineComments=new ArrayList<PartnerRegistrationCommentsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOwnerId);
			cstmt.setString(2,p_sOwnerType);	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				
				oPartnerRegistrationCommentsVO=new PartnerRegistrationCommentsVO();
				oPartnerRegistrationCommentsVO.setOwnerId(rs.getString("owner_id"));
				oPartnerRegistrationCommentsVO.setOwnerType(convertToNameFormat(rs.getString("owner_type")));
				oPartnerRegistrationCommentsVO.setComments(rs.getString("comments"));
				oPartnerRegistrationCommentsVO.setCreateDate(dateAndTimeStampConversion(rs.getString("sbh_create_dt")));	
				oPartnerRegistrationCommentsVO.setCreateId(convertToNameFormat(rs.getString("sbh_create_id")));	

				alAirlineComments.add(oPartnerRegistrationCommentsVO);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("VendorDAO::getVendorComments::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		
		logger.info("VendorDAO::getVendorComments::EXIT");
		return alAirlineComments;
	}
	
	
	public static String replaceTagValues(HashMap p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	
	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}

	
	
	
}
