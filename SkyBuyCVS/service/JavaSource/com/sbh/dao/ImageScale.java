package com.sbh.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

import com.sbh.util.FileUtil;


public class ImageScale {

	private static Logger logger = LogManager.getLogger(ImageScale.class);


	public static void scale(String p_sSrcPath,String p_sSrcImageName,String p_sSrcImageType,String p_sDestPath,String p_sDestImageName,String p_sDestImageType,int p_iWidth,int p_iHeight )throws Exception{
		String sImageScalingPath=null,sSrcFile,sDestFile;

		try{
			logger.info("Image Scaling:Scale::Enter");
			sImageScalingPath=System.getProperty("ImageScalingPath").trim();
			if(p_sSrcPath!=null && p_sSrcPath.length()>0 && p_sSrcImageName!=null && p_sSrcImageName.length()>0 && p_sSrcImageType!=null && p_sSrcImageType.length()>0 && p_sDestPath!=null && p_sDestPath.length()>0 && p_sDestImageName!=null && p_sDestImageName.length()>0 && p_sDestImageType!=null && p_sDestImageType.length()>0 && p_iWidth!=0 && p_iHeight!=0){
				Runtime r = Runtime.getRuntime ();
				Process p =null;
				sSrcFile=p_sSrcPath+p_sSrcImageName+"."+p_sSrcImageType;
				sDestFile=p_sDestPath+p_sDestImageName+"."+p_sDestImageType;
				String sCmd="cmd /c "+sImageScalingPath+"/convert "+sSrcFile+" -resize "+p_iWidth+"x"+p_iHeight 
				+"^ -gravity center -extent "+p_iWidth+"x"+p_iHeight+" "+sDestFile;
				System.out.print(sCmd);
				p = r.exec (sCmd);
				p.waitFor();


			}	
			logger.info("Image Scaling:Scale::Exit");
		}catch(Exception e){
			logger.info("ImageScale:scale::Exception "+e.getMessage());
			e.printStackTrace();
		}


	}		



	public static void deleteFileFromDir(String  p_sFilePath, 
			String p_sFileName, String p_sImageType) throws Exception{
		String sFileName=tokeniseFileName(p_sFileName);
		if(p_sFilePath != null && p_sFilePath.length() > 0){

			if(p_sFilePath != null){
				File fileToDelete = new File(p_sFilePath,p_sFileName);
				if(fileToDelete.exists())
					fileToDelete.delete();

			}

		}	



	}
	public static String tokeniseImgType(String p_sFileName ) throws Exception{

		StringTokenizer stImageType=new StringTokenizer(p_sFileName,".");
		String sImageType="";
		while(stImageType.hasMoreTokens()){
			sImageType=stImageType.nextToken();
		}
		sImageType =sImageType == null ? "" : sImageType.trim();

		return sImageType;

	}


	public static String tokeniseFileName(String p_sFileName ) throws Exception{

		if(p_sFileName != null){
			StringTokenizer stImageName=new StringTokenizer(p_sFileName,".");
			String sImageName="";
			if(stImageName.hasMoreTokens()){
				sImageName=stImageName.nextToken();
			}
			/*if(stImageName.hasMoreTokens()){
					sImageType=stImageName.nextToken();
				}*/
			sImageName =sImageName == null ? "" : sImageName.trim();
			//sImageType =sImageType == null ? "" : sImageType.trim();

			//return sImageName+"_org."+sImageType;
			return sImageName;
		}
		return "";

	}
	public static ArrayList createProductImage(FormFile oFormFile,String sImageViewPath,String sImageUploadPath,String sImageName,String sImageCateType,String sSubFolderName,String sCateFolderName,String sSrcImgPath,String p_sOwnerType,String p_sOwnerId) throws Exception{
		logger.info("Image Scaling:createProductImage::Enter");
		
		String sFileName = null,sImageType = null;
		int iImagewidth=0;
		int iImageheight=0;
		int iMediumImagewidth=0;
		int iMediumImageheight=0;
		int iThumbImagewidth=0;
		int iThumbImageheight=0;
		int iLargeImagewidth=0;
		int iLargeImageheight=0;
		int iMainImagewidth=0;
		int iMainImageheight=0;
		ArrayList alImageInfo = null;
		String sProdImgPath = null,sThumbImgPath =null,sMedImgPath = null,sLargeImgPath = null,sCoverFlowImgPath =null;
		try{
			if(oFormFile!=null){
				sFileName   = oFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sFileName);
			}		
			
			iImagewidth = Integer.parseInt(System.getProperty("ImageWidth"));
			iImageheight = Integer.parseInt(System.getProperty("ImageHeight"));
			iMediumImagewidth = Integer.parseInt(System.getProperty("MediumImageWidth"));
			iMediumImageheight = Integer.parseInt(System.getProperty("MediumImageHeight"));
			iThumbImagewidth = Integer.parseInt(System.getProperty("ThumbImageWidth"));
			iThumbImageheight = Integer.parseInt(System.getProperty("ThumbImageHeight"));
			iLargeImagewidth = Integer.parseInt(System.getProperty("LargeImageWidth"));
			iLargeImageheight = Integer.parseInt(System.getProperty("LargeImageHeight"));
			iMainImagewidth = Integer.parseInt(System.getProperty("MainImageWidth"));
			iMainImageheight = Integer.parseInt(System.getProperty("MainImageHeight"));
			
			File fSrcFilePath = new File(sSrcImgPath);
			if(!fSrcFilePath.exists())
				fSrcFilePath.mkdirs();
			
			if(!oFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,sImageName+sImageCateType+"."+sImageType);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}  
				
				//To Cover Images
				if(sImageCateType.equalsIgnoreCase("main")){
					String sCoverImgPath = sImageUploadPath + "/SkyBuyPics/CoverImages/";
					if(!fSrcFilePath.exists())
						fSrcFilePath.mkdirs();
					File fCoverImage = new File(sCoverImgPath+sImageName+sImageCateType+"."+sImageType);
					if(fCoverImage.exists()){
						fCoverImage.delete();
						
						createCoverflowImage(sCoverImgPath+sImageName+sImageCateType+"."+sImageType, sImageUploadPath);
					}
				}
				
				//Image Upload path
				sProdImgPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\"+sSubFolderName+"\\"+sCateFolderName+"\\Images\\";
				sThumbImgPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\"+sSubFolderName+"\\"+sCateFolderName+"\\Thumb\\";
				sMedImgPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\"+sSubFolderName+"\\"+sCateFolderName+"\\Med\\";
				sLargeImgPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\"+sSubFolderName+"\\"+sCateFolderName+"\\Large\\";
				if(sImageCateType!=null && sImageCateType.equalsIgnoreCase("main"))
					sCoverFlowImgPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\"+sSubFolderName+"\\CoverFlow\\";

				
				File fProdPath = new File(sProdImgPath);
				if(!fProdPath.exists())
					fProdPath.mkdirs();
				
				File fThumbPath = new File(sThumbImgPath);
				if(!fThumbPath.exists())
					fThumbPath.mkdirs();
				
				File fMedPath = new File(sMedImgPath);
				if(!fMedPath.exists())
					fMedPath.mkdirs();
				
				File fLargePath = new File(sLargeImgPath);
				if(!fLargePath.exists())
					fLargePath.mkdirs();
				
				if(sImageCateType!=null && sImageCateType.equalsIgnoreCase("main")) {
					File fCoverFlowPath = new File(sCoverFlowImgPath);
					if(!fCoverFlowPath.exists())
						fCoverFlowPath.mkdirs();
				}
				
				//old Scale 
				/*ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sProdImgPath,sImageName+"image",sImageType,397,442);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sThumbImgPath,sImageName+"thumb",sImageType,79,79);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sMedImgPath,sImageName+"med",sImageType,300,400);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLargeImgPath,sImageName+"large",sImageType,600,800);*/

				
				/*ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sProdImgPath,sImageName+"image",sImageType,485,453);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sThumbImgPath,sImageName+"thumb",sImageType,121,113);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sMedImgPath,sImageName+"med",sImageType,485,453);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLargeImgPath,sImageName+"large",sImageType,970,906);
				if(sImageCateType!=null && sImageCateType.equalsIgnoreCase("main"))
					ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sCoverFlowImgPath,sImageName,sImageType,250,300);
				*/
				//Scale Original image
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sProdImgPath,sImageName+"image",sImageType,iImagewidth, iImageheight);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sThumbImgPath,sImageName+"thumb",sImageType,iThumbImagewidth,iThumbImageheight);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sMedImgPath,sImageName+"med",sImageType,iMediumImagewidth,iMediumImageheight);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLargeImgPath,sImageName+"large",sImageType,iLargeImagewidth,iLargeImageheight);
				if(sImageCateType!=null && sImageCateType.equalsIgnoreCase("main"))
					ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sCoverFlowImgPath,sImageName,sImageType,iMainImagewidth,iMainImageheight);
				
				//Image View path			
				sThumbImgPath=sImageViewPath+"SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sSubFolderName+"/"+sCateFolderName+"/Thumb/";

				alImageInfo = new ArrayList();
				alImageInfo.add(0,sImageType);
				alImageInfo.add(1,sThumbImgPath+sImageName+"thumb."+sImageType);
			}	
			logger.info("Image Scaling:createProductImage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:createProductImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	
	public static ArrayList createAirlienLogoImage(FormFile oFormFile,String sImageViewPath,String sImageUploadPath,String sImageName,String sImageCateType,String sSrcImgPath,String p_sOwnerType,String p_sOwnerId) {
		logger.info("Image Scaling:createAirlienLogoImage::Enter");
		
		String sFileName = null,sImageType = null;
		int iAircharterLogoWidth = 0;
		int iAircharterLogoHeight = 0;
		int iAircharterIntroLogoWidth = 0;
		int iAircharterIntroLogoHeight = 0;
		ArrayList alImageInfo = null;
		String sLogoUploadPath = null, sLogoViewPath = null;
		try{
			if(oFormFile!=null){
				sFileName   = oFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sFileName);
			}				
			
			iAircharterLogoWidth = Integer.parseInt(System.getProperty("AircharterLogoWidth"));
			iAircharterLogoHeight  = Integer.parseInt(System.getProperty("AircharterLogoHeight"));
			iAircharterIntroLogoWidth  = Integer.parseInt(System.getProperty("AircharterIntroLogoWidth"));
			iAircharterIntroLogoHeight  = Integer.parseInt(System.getProperty("AircharterIntroLogoHeight"));
			
			File fSrcFilePath = new File(sSrcImgPath);
			if(!fSrcFilePath.exists())
				fSrcFilePath.mkdirs();
			
			if(!oFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,sImageName+sImageCateType+"."+sImageType);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}  
				
				//Image Upload path
				sLogoUploadPath=sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\AirlineLogo\\Logo\\";
				
				//Used to create the folder to hold the logo of the owner, if it is not exists.
				File fLogoPath = new File(sLogoUploadPath);
				if(!fLogoPath .exists())
					fLogoPath .mkdirs();
		
				//Scale Orginal image
				/*ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLogoUploadPath,sImageName+"_"+sImageCateType,sImageType,178,52);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLogoUploadPath,sImageName+"_"+sImageCateType+"_intro",sImageType,364,108);
				*/
				
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLogoUploadPath,sImageName+"_"+sImageCateType,sImageType,iAircharterLogoWidth,iAircharterLogoHeight);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLogoUploadPath,sImageName+"_"+sImageCateType+"_intro",sImageType,iAircharterIntroLogoWidth,iAircharterIntroLogoHeight);
				
				sLogoViewPath = sImageViewPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/AirlineLogo/Logo/";
				
				alImageInfo = new ArrayList();
				alImageInfo.add(0,sImageType);
				alImageInfo.add(1,sLogoViewPath+sImageName+"_"+sImageCateType+"."+sImageType);
				alImageInfo.add(2,sLogoViewPath+sImageName+"_"+sImageCateType+"_intro"+"."+sImageType);
			}
		
		}catch(Exception e){
			logger.info("ImageScale:createAirlienLogoImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		
		logger.info("Image Scaling:createAirlienLogoImage::Exit");
		
		return alImageInfo;
	}

	
	public static ArrayList createAdvertisementApplication(FormFile oFormFile,String sImageViewPath,String sImageUploadPath,String sImageName,String sImageCateType,String sSubFolderName,String sCateFolderName,String sSrcImgPath,String p_sOwnerType,String p_sOwnerId) throws Exception{
		logger.info("Image Scaling:createProductImage::Enter");
		String sFileName = null,sImageType = null;
		ArrayList alImageInfo = null;
		try{
			if(oFormFile!=null){
				sFileName   = oFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sFileName);
			}				
			sSrcImgPath = sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\Swf";
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSwfPath = new File(sSrcImgPath);
				if(!fSwfPath .exists())
					fSwfPath .mkdirs();

			if(!oFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,sImageName+"."+sImageType);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}  

				alImageInfo = new ArrayList();
				alImageInfo.add(0,sImageType);
				alImageInfo.add(1,fImagePath.getAbsolutePath());
			}	
			logger.info("Image Scaling:createProductImage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:createProductImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	
	
	public static ArrayList moveProductImage(String sImageUploadPath,String sImageCatePath,String sOldCateFolderName,String sCateFolderName,String sImageName,String sImgType, String p_sOwnerType, String p_sOwnerId ) throws Exception{
		logger.info("Image Scaling:moveProductImage::Enter");
		String sFileName = null;
		ArrayList alImageInfo = null;
		String sSrcImgFilePath = null,sDesImgFilePath =null,sDesImgFile = null,sImageViewPath = null,sThumbImgPath=null;
		try{
			sImageViewPath = System.getProperty("ImageViewPath").trim();
			
			//Move Images File 	
			sSrcImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sOldCateFolderName+"/Images/"+sImageName+"image."+sImgType;
			sDesImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Images";
			sDesImgFile = sImageName+"image."+sImgType;			
			FileUtil.moveFile(sSrcImgFilePath,sDesImgFilePath,sDesImgFile);
			
			sSrcImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sOldCateFolderName+"/Thumb/"+sImageName+"thumb."+sImgType;
			sDesImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Thumb";
			sDesImgFile = sImageName+"thumb."+sImgType;			
			FileUtil.moveFile(sSrcImgFilePath,sDesImgFilePath,sDesImgFile);
			
			sSrcImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sOldCateFolderName+"/Large/"+sImageName+"large."+sImgType;
			sDesImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Large";
			sDesImgFile = sImageName+"large."+sImgType;			
			FileUtil.moveFile(sSrcImgFilePath,sDesImgFilePath,sDesImgFile);
			
			sSrcImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sOldCateFolderName+"/Med/"+sImageName+"med."+sImgType;
			sDesImgFilePath =sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Med";
			sDesImgFile = sImageName+"med."+sImgType;			
			FileUtil.moveFile(sSrcImgFilePath,sDesImgFilePath,sDesImgFile);
			
			//Image View path			
			sThumbImgPath=sImageViewPath+"SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Thumb/";

			alImageInfo = new ArrayList();
			alImageInfo.add(0,sImgType);
			alImageInfo.add(1,sThumbImgPath+sImageName+"thumb."+sImgType);
			logger.info("Image Scaling:moveProductImage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:moveProductImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	/*public static ArrayList moveProductImage(String sImageViewPath,String sImageUploadPath,String sImageName,String sImageCateType,String sSubFolderName,String sCateFolderName,String sSrcImgPath,String sImageType) throws Exception{
		String sFileName = null;
		ArrayList alImageInfo = null;
		String sProdImgPath = null,sThumbImgPath =null,sMedImgPath = null,sLargeImgPath = null;
		try{
						
				//Image Upload path
				sProdImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sSubFolderName+"\\"+sCateFolderName+"\\Images\\";
				sThumbImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sSubFolderName+"\\"+sCateFolderName+"\\Thumb\\";
				sMedImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sSubFolderName+"\\"+sCateFolderName+"\\Med\\";
				sLargeImgPath=sImageUploadPath+"\\SkyBuyPics\\"+sSubFolderName+"\\"+sCateFolderName+"\\Large\\";

				//Scale Orginal image
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sProdImgPath,sImageName+"image",sImageType,397,442);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sThumbImgPath,sImageName+"thumb",sImageType,79,79);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sMedImgPath,sImageName+"med",sImageType,300,400);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLargeImgPath,sImageName+"large",sImageType,600,800);


				//Image View path			
				sThumbImgPath=sImageViewPath+"SkyBuyPics/"+sSubFolderName+"/"+sCateFolderName+"/Thumb/";

				alImageInfo = new ArrayList();
				alImageInfo.add(0,sImageType);
				alImageInfo.add(1,sThumbImgPath+sImageName+"thumb."+sImageType);
			
		}catch(Exception e){
			logger.info("ImageScale:scale::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
*/
	public static void deleteProductImage(String sImageUploadPath,String sImageCatePath,String sCateFolderName,String sImageName,String sImgType, String p_sOwnerType, String p_sOwnerId) throws Exception{
		logger.info("Image Scaling:deleteProductImage::Enter");
		ArrayList<String> alSrcFilepath = null;
		try{
			alSrcFilepath = new ArrayList<String>();
			alSrcFilepath.add(0,sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Images/"+sImageName+"image."+sImgType);
			alSrcFilepath.add(1,sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Thumb/"+sImageName+"thumb."+sImgType);
			alSrcFilepath.add(2,sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Large/"+sImageName+"large."+sImgType);
			alSrcFilepath.add(3,sImageUploadPath+"/SkyBuyPics/"+p_sOwnerType+"_"+p_sOwnerId+"/"+sImageCatePath+"/"+sCateFolderName+"/Med/"+sImageName+"thumb."+sImgType);
			
			for(int  i=0;i<alSrcFilepath.size();i++){
				File oFile = new File((String)alSrcFilepath.get(i));
				if(oFile.exists())
					oFile.delete();
			}
			logger.info("Image Scaling:deleteProductImage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:deleteProductImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}


	}
	
	
	public static ArrayList createCoverflowImage(String sFilePath,String sImageUploadPath) throws Exception{
		logger.info("Image Scaling:createCoverflowImage::Enter");
		
		String sImageName = null,sImageType = null;
		int iCoverImageWidth = 0;
		int iCoverImageHeight = 0;
		String sCoverImagePath = null;
		ArrayList alImageInfo = null;
		byte[] baCoverImage = null;
		File fCoverImage = null;
		//String sProdImgPath = null,sThumbImgPath =null,sMedImgPath = null,sLargeImgPath = null,sCoverFlowImgPath =null;
		try{
			if(sFilePath!=null){
				fCoverImage = new File(sFilePath);
				sImageName = fCoverImage.getName();
				if(sImageName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sImageName);

			}		

			//Image Upload path
			sCoverImagePath=sImageUploadPath+"/SkyBuyPics/CoverImages/";


			File fProdPath = new File(sCoverImagePath);
			if(!fProdPath.exists())
				fProdPath.mkdirs();

			iCoverImageWidth = Integer.parseInt(System.getProperty("CoverImageWidth"));
			iCoverImageHeight  = Integer.parseInt(System.getProperty("CoverImageHeight"));

			//old Scale 
			/*ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sProdImgPath,sImageName+"image",sImageType,397,442);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sThumbImgPath,sImageName+"thumb",sImageType,79,79);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sMedImgPath,sImageName+"med",sImageType,300,400);
				ImageScale.scale(sSrcImgPath,sImageName+sImageCateType,sImageType,sLargeImgPath,sImageName+"large",sImageType,600,800);*/

			//Scale Orginal image
			ImageScale.scaleForCoverflow(sFilePath,sCoverImagePath,sImageName,iCoverImageWidth,iCoverImageHeight);

			alImageInfo = new ArrayList();
			alImageInfo.add(0,sImageType);
			alImageInfo.add(1,sCoverImagePath+sImageName+sImageType);

			logger.info("Image Scaling:createCoverflowImage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:createCoverflowImage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	
	
	public static void scaleForCoverflow(String p_sSrcPath,String p_sDestPath,String p_sDestImageName,int p_iWidth,int p_iHeight )throws Exception{
		String sImageScalingPath=null,sSrcFile,sDestFile;

		try{
			logger.info("Image Scaling:scaleForCoverflow::Enter");
			sImageScalingPath=System.getProperty("ImageScalingPath").trim();
			sImageScalingPath = sImageScalingPath==null?"":sImageScalingPath.trim();
			 
			//if(p_sSrcPath!=null && p_sSrcPath.length()>0 && p_sSrcImageName!=null && p_sSrcImageName.length()>0 && p_sSrcImageType!=null && p_sSrcImageType.length()>0 && p_sDestPath!=null && p_sDestPath.length()>0 && p_sDestImageName!=null && p_sDestImageName.length()>0 && p_sDestImageType!=null && p_sDestImageType.length()>0 && p_iWidth!=0 && p_iHeight!=0){
				Runtime r = Runtime.getRuntime ();
				Process p =null;
				sSrcFile=p_sSrcPath;
				sDestFile=p_sDestPath+p_sDestImageName;
				String sCmd="cmd /c "+sImageScalingPath+"/convert "+sSrcFile+" -resize "+p_iWidth+"x"+p_iHeight 
				+"^ -gravity center -extent "+p_iWidth+"x"+p_iHeight+" "+sDestFile;
				System.out.print(sCmd);
				p = r.exec (sCmd);
				p.waitFor();


			//}	
			logger.info("Image Scaling:scaleForCoverflow::Exit");
		}catch(Exception e){
			logger.info("ImageScale:scaleForCoverflow::Exception "+e.getMessage());
			e.printStackTrace();
		}


	}
	
	public static ArrayList createAboutMeFile(FormFile oFormFile,String sImageUploadPath,String p_sFileName,String p_sOwnerType,String p_sOwnerId) throws Exception{
		logger.info("Image Scaling:createAboutMeFile::Enter");
		String sFileName = null,sFileType = null;
		ArrayList alImageInfo = null;
		String sSrcImgPath = null;
		String sProdImgPath = null,sThumbImgPath =null,sMedImgPath = null,sLargeImgPath = null;
		try{
			if(oFormFile!=null){
				sFileName   = oFormFile.getFileName();
				if(sFileName != null)
					sFileType 	   = ImageScale.tokeniseImgType(sFileName);
			}				
			sSrcImgPath = sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\About";

			//Used to create the folder to hold the About package of the owner, if it is not exists.
			File fAboutPath = new File(sSrcImgPath);
			if(!fAboutPath .exists())
				fAboutPath .mkdirs();

			if(!oFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,p_sFileName+"."+sFileType);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}  

				alImageInfo = new ArrayList();
				alImageInfo.add(0,sFileType);
				alImageInfo.add(1,fImagePath.getAbsolutePath());
			}	
			logger.info("Image Scaling:createAboutMeFile::Exit");
		}catch(Exception e){
			logger.info("ImageScale:createAboutMeFile::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	
	public static boolean uploadSkyBuyCatalogue(FormFile oCatalogueFormFile,String sImageViewPath,String sImageUploadPath,String sSrcImgPath,String p_sFileName) {
		logger.info("Image Scaling:uploadSkyBuyCatalogue::Entry");

		boolean bSkybuyCatalogue = false;
		try{
			/*if(oCatalogueFormFile!=null){
				sFileName   = oCatalogueFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sFileName);
			}*/				
			sSrcImgPath = sImageUploadPath+"\\ECatalogue";
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSwfPath = new File(sSrcImgPath);
				if(!fSwfPath .exists())
					fSwfPath .mkdirs();

			if(!oCatalogueFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,p_sFileName);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oCatalogueFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
					bSkybuyCatalogue = true;
				}  
			}	
			logger.info("Image Scaling:uploadSkyBuyCatalogue::Exit");
		}catch(Exception e){
			logger.info("ImageScale:uploadSkyBuyCatalogue::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return bSkybuyCatalogue;
	}
	
	public static boolean uploadWelcomePage(FormFile oWelcomePageFormFile,String sImageViewPath,String sImageUploadPath,String sSrcImgPath,String p_sFileName) {
		logger.info("Image Scaling:uploadWelcomePage::Entry");
		
		boolean bSkybuyWelcome = false;
		try{
			/*if(oWelcomePageFormFile!=null){
				sFileName   = oWelcomePageFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = ImageScale.tokeniseImgType(sFileName);
			}*/				
			sSrcImgPath = sImageUploadPath+"\\ECatalogue";
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSwfPath = new File(sSrcImgPath);
				if(!fSwfPath .exists())
					fSwfPath .mkdirs();

			if(!oWelcomePageFormFile.equals("")){  
				//Create original Image
				File fImagePath = new File(sSrcImgPath,p_sFileName);
				//If file does not exists create file                      
				if(fImagePath.exists())
					fImagePath.delete();
				if(!fImagePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
					oFileOutputStream.write(oWelcomePageFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
					bSkybuyWelcome = true;
				}  
			}	
			logger.info("Image Scaling:uploadWelcomePage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:uploadWelcomePage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return bSkybuyWelcome;
	}
	
	public static List uploadSkyBuyInstallationPackage(FormFile oCatalogueFormFile,String sImageViewPath,String sImageUploadPath,String sSrcImgPath,String p_sFileName, String p_sUploadType, String p_sAdminVersion, String p_sCatalogueVersion) {
		logger.info("Image Scaling:uploadSkyBuyInstallationPackage::Entry");
		
		Boolean bIsInstallationPackageUploaded = false;
		String sFileType = "";
		String sFileName = "";
		File fImagePath = null;
		List alImageInfo = new ArrayList();
		try{
			
			if(p_sFileName != null) {
				sFileType = ImageScale.tokeniseImgType(p_sFileName);
				sFileName = ImageScale.tokeniseFileName(p_sFileName);
				sFileType = sFileType == null?"":sFileType.trim();
				sFileName = sFileName == null?"":sFileName.trim();
			}
			sSrcImgPath = sImageUploadPath+"\\InstallationPackage";
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSwfPath = new File(sSrcImgPath);
				if(!fSwfPath .exists())
					fSwfPath .mkdirs();

			if(!oCatalogueFormFile.equals("")){  
				//Create original Image
				if("All".equalsIgnoreCase(p_sUploadType)) {
					fImagePath = new File(sSrcImgPath,sFileName+"_"+p_sAdminVersion+"_"+p_sCatalogueVersion+"."+sFileType);
				}else if("Admin".equalsIgnoreCase(p_sUploadType)) {
					fImagePath = new File(sSrcImgPath,sFileName+"_"+p_sAdminVersion+"."+sFileType);
				}else if("ShoppingCart".equalsIgnoreCase(p_sUploadType)) {
					fImagePath = new File(sSrcImgPath,sFileName+"_"+p_sCatalogueVersion+"."+sFileType);
				}
				//If file does not exists create file
				if(fImagePath != null) {
					if(fImagePath.exists())
						fImagePath.delete();
					if(!fImagePath.exists()){
						FileOutputStream oFileOutputStream = new FileOutputStream(fImagePath);
						oFileOutputStream.write(oCatalogueFormFile.getFileData());					
						oFileOutputStream.flush();
						oFileOutputStream.close();
						bIsInstallationPackageUploaded = true;
					}  
				}
				alImageInfo.add(0,bIsInstallationPackageUploaded);
				alImageInfo.add(1,fImagePath.getName());
			}
			logger.info("Image Scaling:uploadSkyBuyInstallationPackage::Exit");
		}catch(Exception e){
			logger.info("ImageScale:uploadSkyBuyInstallationPackage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
	
	
	public static ArrayList createSpecialProductSWF(FormFile oFormFile,String sImageViewPath,String sImageUploadPath,String sSpecialProdName,String p_sOwnerType,String p_sOwnerId) throws Exception{
		logger.info("Image Scaling:createSpecialProductSWF::Enter");
		String sFileName = null,sSpecialProdType = null,sSrcSpecialProdPath = null;
		ArrayList alSpelProdInfo = null;
		try{
			if(oFormFile!=null){
				sFileName   = oFormFile.getFileName();
				if(sFileName != null)
					sSpecialProdType 	   = ImageScale.tokeniseImgType(sFileName);
			}				
			sSrcSpecialProdPath = sImageUploadPath+"\\SkyBuyPics\\"+p_sOwnerType+"_"+p_sOwnerId+"\\SpecialProdSwf";
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSwfPath = new File(sSrcSpecialProdPath);
				if(!fSwfPath .exists())
					fSwfPath .mkdirs();

			if(!oFormFile.equals("")){  
				//Create original Image
				File fSpecialProdPath = new File(sSrcSpecialProdPath,sSpecialProdName+"."+sSpecialProdType);
				//If file does not exists create file                      
				if(fSpecialProdPath.exists())
					fSpecialProdPath.delete();
				if(!fSpecialProdPath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fSpecialProdPath);
					oFileOutputStream.write(oFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}  

				alSpelProdInfo = new ArrayList();
				alSpelProdInfo.add(0,sSpecialProdType);
				alSpelProdInfo.add(1,fSpecialProdPath.getAbsolutePath());
			}	
			logger.info("Image Scaling:createSpecialProductSWF::Exit");
		}catch(Exception e){
			logger.info("ImageScale:createSpecialProductSWF::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alSpelProdInfo;
	}
}





