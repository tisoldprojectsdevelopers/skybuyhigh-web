package com.sbh.dao;

import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.util.CCUtils.getMaskCCNo;
import static com.sbh.util.Utils.convertToNameFormat;
import static com.sbh.util.Utils.dateAndTimeStampConversion;
import static com.sbh.util.Utils.dateStampConversion;
import static com.sbh.util.Utils.decrypt;
import static com.sbh.util.Utils.encrypt;
import static com.sbh.util.Utils.getPhoneFormat;
import static com.sbh.util.Utils.getPrivateKeyFromString;
import static com.sbh.util.Utils.getPublicKeyFromString;
import static com.sbh.util.Utils.getRandomNo;
import static com.sbh.util.Utils.mergeAddressLines;
import static com.sbh.util.Utils.mergeName;

import java.awt.Dimension;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.zefer.pd4ml.PD4Constants;
import org.zefer.pd4ml.PD4ML;

import com.sbh.client.vo.AirlineAdvtVO;
import com.sbh.email.Email;
import com.sbh.forms.OrderItemForm;
import com.sbh.forms.OrderReturnForm;
import com.sbh.forms.SearchOrderForm;
import com.sbh.util.DBConnection;
import com.sbh.vo.AirlineVO;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CustPayTxnVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.KeyVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.OrderReturnDetailsVO;
import com.sbh.vo.OrderTrackingDetailsVO;
import com.sbh.vo.PaymentInformationVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.VendorVO;

public class OrderDAO {
	private static Logger logger = LogManager.getLogger(OrderDAO.class);

	public static ArrayList getOrderStatus(LoginVO oLoginVO) throws Exception{	
		logger.info("OrderDAO::getOrderStatus::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ArrayList alOrderStatusInfo = new ArrayList();

		String sQry="{call usp_sbh_get_order_item_status(?,?,?,?,?,?,?)}";

		logger.info("OrderDAO::getOrderStatus::SQL QUERY "+sQry);	
		logger.info("OrderDAO::getOrderStatus::User Type :  "+oLoginVO.getUserType());	
		logger.info("OrderDAO::getOrderStatus::Ref Id :  "+oLoginVO.getRefId());	

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);	
			cstmt.registerOutParameter(2, Types.INTEGER);	
			cstmt.registerOutParameter(3, Types.INTEGER);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.registerOutParameter(5, Types.INTEGER);

			cstmt.setString(6, oLoginVO.getUserType());
			cstmt.setString(7, oLoginVO.getRefId());
			cstmt.execute();

			alOrderStatusInfo.add(0,cstmt.getInt(1));
			alOrderStatusInfo.add(1,cstmt.getInt(2));
			alOrderStatusInfo.add(2,cstmt.getInt(3));
			alOrderStatusInfo.add(3,cstmt.getInt(4));
			alOrderStatusInfo.add(4,cstmt.getInt(5));

		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::getOrderStatus::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("OrderDAO::getOrderStatus::EXIT");
		return alOrderStatusInfo;
	}
	public static ArrayList getOrderDetails(String p_sOrderStatus,LoginVO oLoginVO) throws Exception{	
		logger.info("OrderDAO::getOrderDetails::ENTER");
		
		ArrayList alOrderDetails = new ArrayList();
		com.sbh.vo.OrderDetailsVO oOrderDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_order_details(?,?,?)}";
		logger.info("OrderDAO::getOrderDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::getOrderDetails::Order Status "+p_sOrderStatus);			
		logger.info("OrderDAO::getOrderDetails::User Type"+oLoginVO.getUserType());	
		logger.info("OrderDAO::getOrderDetails::Ref Id "+oLoginVO.getRefId());
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderStatus);	
			cstmt.setString(2,oLoginVO.getUserType());	
			cstmt.setString(3,oLoginVO.getRefId());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oOrderDetailsVO=new com.sbh.vo.OrderDetailsVO();
				oOrderDetailsVO.setOrderId(rs.getString("order_id"));
				oOrderDetailsVO.setCustId(rs.getString("cust_id"));
				oOrderDetailsVO.setCustName(rs.getString("cust_name"));
				oOrderDetailsVO.setCustEmail(rs.getString("cust_email"));
				oOrderDetailsVO.setCustPhone(rs.getString("cust_phone"));
				oOrderDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderDetailsVO.setFlightNo(rs.getString("flight_no"));	
				oOrderDetailsVO.setAirName(rs.getString("air_name"));
				String sOrderStatus = rs.getString("order_status");
				sOrderStatus = sOrderStatus==null?"":sOrderStatus.trim();
				if(sOrderStatus.equalsIgnoreCase("C"))
					oOrderDetailsVO.setOrderStatus("Completed");
				else if(sOrderStatus.equalsIgnoreCase("P"))
					oOrderDetailsVO.setOrderStatus("Pending");
				else if(sOrderStatus.equalsIgnoreCase("F"))
					oOrderDetailsVO.setOrderStatus("InComplete");

				/*Date  dOrderDt = rs.getDate("order_dt");
				if(dOrderDt!= null)
					oOrderDetailsVO.setOrderDt(sdfOutput.format(dOrderDt));					
				else*/
				oOrderDetailsVO.setOrderDt(rs.getString("order_dt"));

				alOrderDetails.add(oOrderDetailsVO);
			}
			logger.info("OrderDAO::getOrderDetails::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getOrderDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		return alOrderDetails;
	}
	public static List<OrderItemDetailsVO> getOrderItemDetails(String p_sOrderId,LoginVO oLoginVO) throws Exception{	
		logger.info("OrderDAO::getOrderItemDetails::ENTER");
		
		List<OrderItemDetailsVO> alOrderItemDetails = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_order_item_details(?,?,?)}";
		logger.debug("OrderDAO::getOrderItemDetails::SQL QUERY "+sQry);
		logger.debug("OrderDAO::getOrderItemDetails::Order Id "+p_sOrderId);			
		logger.debug("OrderDAO::getOrderItemDetails::User Type "+oLoginVO.getUserType());	
		logger.debug("OrderDAO::getOrderItemDetails::Ref Id "+oLoginVO.getRefId());	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderId);	
			cstmt.setString(2,oLoginVO.getUserType());	
			cstmt.setString(3,oLoginVO.getRefId());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oOrderItemDetailsVO=new OrderItemDetailsVO();
				oOrderItemDetailsVO.setVendorName(rs.getString("owner_name"));
				oOrderItemDetailsVO.setCustFirstName(rs.getString("cust_name"));
				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));
				oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				/*Date  dOrderCreatedDt = rs.getDate("order_dt");
				if(dOrderCreatedDt!= null)
					oOrderItemDetailsVO.setOrderCreatedDt(sdfOutput.format(dOrderCreatedDt));					
				else*/
				oOrderItemDetailsVO.setOrderCreatedDt(rs.getString("order_dt"));
				oOrderItemDetailsVO.setItemName(rs.getString("item_name"));		
				oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
				alOrderItemDetails.add(oOrderItemDetailsVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getOrderItemDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("OrderDAO::getOrderItemDetails::EXIT");
		return alOrderItemDetails;
	}

	public static List<OrderItemDetailsVO> getSearchOrder(SearchOrderForm oSearchOrderForm,String sOwnerType,String sOwnerId) throws Exception{	
		logger.info("OrderDAO::getSearchOrder::ENTER");
		
		List<OrderItemDetailsVO> alOrderItemDetails = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemDetailsVO=null;
		double dQuantity;
		double dSbhPrice;
		Double dPayAmount;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_order_details_by_ownertype(?,?,?,?,?,?,?,?)}";
		logger.info("OrderDAO::getSearchOrder::SQL QUERY "+sQry);
		logger.info("OrderDAO::getSearchOrder::Search By "+oSearchOrderForm.getSearchBy());			
		logger.info("OrderDAO::getSearchOrder::Search Value "+oSearchOrderForm.getSearchValue());			
		logger.info("OrderDAO::getSearchOrder::Category "+oSearchOrderForm.getCategory());			
		logger.info("OrderDAO::getSearchOrder::Order date "+oSearchOrderForm.getOrderFromDate());			
		logger.info("OrderDAO::getSearchOrder::Order Status "+oSearchOrderForm.getOrderStatus());			
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,oSearchOrderForm.getSearchBy());		
			cstmt.setString(2,oSearchOrderForm.getSearchValue());		
			cstmt.setString(3,oSearchOrderForm.getCategory());		
			cstmt.setString(4,oSearchOrderForm.getOrderFromDate());	
			cstmt.setString(5,oSearchOrderForm.getOrderToDate());	
			cstmt.setString(6,oSearchOrderForm.getOrderStatus());
			cstmt.setString(7,sOwnerType);
			cstmt.setString(8,sOwnerId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oOrderItemDetailsVO=new OrderItemDetailsVO();
				oOrderItemDetailsVO.setCustFirstName(rs.getString("cust_fname")+" "+rs.getString("cust_lname"));
				oOrderItemDetailsVO.setCustPhone(getPhoneFormat(rs.getString("cust_phone")));
				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));
				oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oOrderItemDetailsVO.setAirName(rs.getString("air_name"));
				//oOrderItemDetailsVO.setFlightNo(rs.getString("flight_no"));
				oOrderItemDetailsVO.setOrderId(rs.getString("order_id"));
				oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderItemDetailsVO.setOrderCreatedDt(rs.getString("order_dt"));		
				oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));		
				oOrderItemDetailsVO.setItemName(rs.getString("item_name"));		
				oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));		
				String sChargeType = rs.getString("charge_type");
				sChargeType = sChargeType == null?"":sChargeType.trim();
				oOrderItemDetailsVO.setChargeType(sChargeType);
				oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
				dQuantity=Double.parseDouble(rs.getString("qty"));
				dSbhPrice=Double.parseDouble(rs.getString("sbh_price"));
				dPayAmount=dQuantity*dSbhPrice;
				oOrderItemDetailsVO.setPayAmount(numberFormat.format(dPayAmount));

				String sShipmentStatus = rs.getString("shipment_status");
				sShipmentStatus = sShipmentStatus==null?"N":sShipmentStatus.trim();
				oOrderItemDetailsVO.setShipmentStatus(sShipmentStatus);
				
				alOrderItemDetails.add(oOrderItemDetailsVO);
			}
			logger.info("OrderDAO::getSearchOrder::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getSearchOrder::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		return alOrderItemDetails;
	}
	public static List<OrderItemDetailsVO> getSearchOrderSummary(SearchOrderForm oSearchOrderForm,String sOwnerType,String sOwnerId) throws Exception{	
		logger.info("OrderDAO::getSearchOrderSummary::ENTER");		
		List<OrderItemDetailsVO> alOrderItemDetails = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_order_summary_details_by_ownertype(?,?,?,?,?,?,?,?)}";
		logger.info("OrderDAO::getSearchOrderSummary::SQL QUERY "+sQry);
		logger.info("OrderDAO::getSearchOrderSummary::Search By "+oSearchOrderForm.getSearchBy());			
		logger.info("OrderDAO::getSearchOrderSummary::Search Value "+oSearchOrderForm.getSearchValue());			
		logger.info("OrderDAO::getSearchOrderSummary::Category "+oSearchOrderForm.getCategory());			
		logger.info("OrderDAO::getSearchOrderSummary::Order date "+oSearchOrderForm.getOrderFromDate());			
		logger.info("OrderDAO::getSearchOrderSummary::Order Status "+oSearchOrderForm.getOrderStatus());			

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,oSearchOrderForm.getSearchBy());		
			cstmt.setString(2,oSearchOrderForm.getSearchValue());		
			cstmt.setString(3,oSearchOrderForm.getCategory());		
			cstmt.setString(4,oSearchOrderForm.getOrderFromDate());	
			cstmt.setString(5,oSearchOrderForm.getOrderToDate());	
			cstmt.setString(6,oSearchOrderForm.getOrderStatus());
			cstmt.setString(7,sOwnerType);
			cstmt.setString(8,sOwnerId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oOrderItemDetailsVO=new OrderItemDetailsVO();

				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));				
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));
				oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));					
				oOrderItemDetailsVO.setItemName(rs.getString("item_name"));		
				oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
				oOrderItemDetailsVO.setVendorName(rs.getString("owner_name"));
				oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
				
				alOrderItemDetails.add(oOrderItemDetailsVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getSearchOrderSummary::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("OrderDAO::getSearchOrderSummary::EXIT");
		return alOrderItemDetails;
	}
	public static OrderItemDetailsVO getCustOrderItemDetails(String p_sOrderItemId) throws Exception{	
		logger.info("OrderDAO::getCustOrderItemDetails::ENTER");		
		OrderItemDetailsVO oOrderItemDetailsVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDBData = null;
		String sCvv = null, sCvvStatus = null;
		String sQry="{call usp_sbh_get_cust_order_item_details(?)}";
		logger.info("OrderDAO::getCustOrderItemDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::getCustOrderItemDetails::Order Status "+p_sOrderItemId);			

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);			
			cstmt.execute();
			oOrderItemDetailsVO=new OrderItemDetailsVO();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				
				oOrderItemDetailsVO.setCustId(rs.getString("cust_id"));
				oOrderItemDetailsVO.setCustFirstName(rs.getString("cust_fname"));
				oOrderItemDetailsVO.setCustLastName(rs.getString("cust_lname"));
				oOrderItemDetailsVO.setCustEmail(rs.getString("cust_email"));
				oOrderItemDetailsVO.setCustPhone(getPhoneFormat(rs.getString("cust_phone")));
				oOrderItemDetailsVO.setCustAddress1(rs.getString("cust_address1"));
				oOrderItemDetailsVO.setCustAddress2(rs.getString("cust_address2"));
				oOrderItemDetailsVO.setCustCity(rs.getString("cust_city"));
				oOrderItemDetailsVO.setCustState(rs.getString("cust_state"));
				oOrderItemDetailsVO.setCustCountry(rs.getString("cust_country"));
				oOrderItemDetailsVO.setCustZip(rs.getString("cust_zip"));				
				oOrderItemDetailsVO.setCustShipFirstName(rs.getString("cust_ship_fname"));
				oOrderItemDetailsVO.setCustShipLastName(rs.getString("cust_ship_lname"));
				oOrderItemDetailsVO.setCustShipEmail(rs.getString("cust_ship_email"));
				//oOrderItemDetailsVO.setCustShipPhone(rs.getString("cust_ship_phone"));
				oOrderItemDetailsVO.setCustShipAddress1(rs.getString("cust_ship_address1"));
				oOrderItemDetailsVO.setCustShipAddress2(rs.getString("cust_ship_address2"));
				oOrderItemDetailsVO.setCustShipPhone(getPhoneFormat(rs.getString("cust_ship_phone")));
				oOrderItemDetailsVO.setCustShipCity(rs.getString("cust_ship_city"));
				oOrderItemDetailsVO.setCustShipState(rs.getString("cust_ship_state"));
				oOrderItemDetailsVO.setCustShipCountry(rs.getString("cust_ship_country"));
				oOrderItemDetailsVO.setCustShipZip(rs.getString("cust_ship_zip"));				
				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));
				oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oOrderItemDetailsVO.setAirName(rs.getString("air_name"));
				oOrderItemDetailsVO.setFlightNo(rs.getString("flight_no"));
				oOrderItemDetailsVO.setOrderId(rs.getString("order_id"));
				oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderItemDetailsVO.setOrderCreatedDt(rs.getString("order_dt"));		
				oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));				
				oOrderItemDetailsVO.setOwnerId(rs.getString("owner_id"));
				oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
				oOrderItemDetailsVO.setVendorName(rs.getString("owner_name"));
				oOrderItemDetailsVO.setKeyRefId(rs.getString("key_ref_id"));
				sDBData = rs.getString("item_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setItemName(sDBData);
				
				sDBData = rs.getString("brand_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setBrandName(sDBData);

				oOrderItemDetailsVO.setProdId(rs.getString("prod_id"));
				oOrderItemDetailsVO.setMainImgPath(rs.getString("main_img_path"));
				oOrderItemDetailsVO.setMainImgType(rs.getString("main_img_type"));
				oOrderItemDetailsVO.setMainImgCaption(rs.getString("main_img_caption"));
				oOrderItemDetailsVO.setTravelDate(rs.getString("travel_date"));
				oOrderItemDetailsVO.setReasonForReject(rs.getString("reject_reason"));
				
				sDBData = rs.getString("shipment_status");
				sDBData = sDBData == null?"N":sDBData.trim();
				oOrderItemDetailsVO.setShipmentStatus(sDBData);
				
				sDBData = rs.getString("cancel_reason");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCancelReason(sDBData);

				sDBData = rs.getString("color");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setColor(sDBData);

				sDBData = rs.getString("size");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setSize(sDBData);
				
				sDBData = rs.getString("all_size");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setAllSizes(sDBData);
				
				sDBData = rs.getString("all_color");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setAllColors(sDBData);
				
				sDBData = rs.getString("short_desc");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setShortDesc(sDBData);
				
				sDBData = rs.getString("special_inst");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setSpecialInst(sDBData);
				
				sDBData = rs.getString("personal_info");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setPersonalInfo(sDBData);
				
				sDBData = rs.getString("is_special_prod");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setIsSpecialProd(sDBData);
			}
			cstmt.getMoreResults();
			rs=cstmt.getResultSet();
			while(rs.next())
			{
				oOrderItemDetailsVO.setPaymentId(rs.getString("payment_id"));
				oOrderItemDetailsVO.setCCNo(rs.getString("cc_no"));
				oOrderItemDetailsVO.setCreditCardNoLFD(rs.getString("cc_no_lfd"));
				
				String sCCFName = decryptInputData(rs.getString("fname"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				oOrderItemDetailsVO.setCardFname(convertToNameFormat(sCCFName));
				
				String sCardType = decryptInputData(rs.getString("card_type"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				oOrderItemDetailsVO.setCardType(sCardType);
				
				String sExpiryDate = decryptInputData(rs.getString("exp_dt"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				oOrderItemDetailsVO.setExpDt(sExpiryDate);
				
				if(oOrderItemDetailsVO.getCreditCardNoLFD()!=null){
					oOrderItemDetailsVO.setMaskCCNo(oOrderItemDetailsVO.getCreditCardNoLFD());
				}
				sCvv = rs.getString("cvv");
				sCvv = sCvv == null?"":sCvv.trim();
				if(sCvv.trim().length() > 0) {
					oOrderItemDetailsVO.setCvv(sCvv);
				}else {
					oOrderItemDetailsVO.setCvv("");
				}

				sCvvStatus = rs.getString("cvv_status");
				sCvvStatus = sCvvStatus == null?"":sCvvStatus.trim();
				oOrderItemDetailsVO.setCvvStatus(sCvvStatus);

			}
			cstmt.getMoreResults();
			rs=cstmt.getResultSet();
			while(rs.next()) {
				oOrderItemDetailsVO.setReasonForFailure(rs.getString("txn_msg"));
			}
			cstmt.getMoreResults();
			rs=cstmt.getResultSet();
			while(rs.next()) {
				oOrderItemDetailsVO.setRmaStatus(rs.getString("rma_status"));
				oOrderItemDetailsVO.setRmaGeneratedNo(rs.getString("rma_Number"));
				oOrderItemDetailsVO.setReturnReason(rs.getString("reason"));
				oOrderItemDetailsVO.setVendorComments(rs.getString("comments"));
				oOrderItemDetailsVO.setChargeBackAmount(rs.getString("charge_back_amount"));
				oOrderItemDetailsVO.setAdminComments(rs.getString("admin_comments"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getCustOrderItemDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("OrderDAO::getCustOrderItemDetails::EXIT");
		return oOrderItemDetailsVO;
	}

	public static int updateCustOrderItemDetails(OrderItemForm oOrderItemForm,String p_sUserType, String isCreditCardChanged) throws Exception{	
		logger.info("OrderDAO::updateCustOrderItemDetails::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_cust_order_item_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.info("OrderDAO::updateCustOrderItemDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::updateCustOrderItemDetails::Cust Id "+oOrderItemForm.getCustId());		
		logger.info("OrderDAO::updateCustOrderItemDetails::Order Item Id "+oOrderItemForm.getOrderItemId());
		logger.info("OrderDAO::updateCustOrderItemDetails::Cust Zip "+oOrderItemForm.getCustZip());
		logger.info("OrderDAO::updateCustOrderItemDetails::Cust Ship Zip "+oOrderItemForm.getCustShipZip());
		logger.info("OrderDAO::updateCustOrderItemDetails::Quantity "+oOrderItemForm.getQty());
		logger.info("OrderDAO::updateCustOrderItemDetails::Cust Id "+oOrderItemForm.getCustId());
		logger.info("OrderDAO::updateCustOrderItemDetails::Order Item Status "+ oOrderItemForm.getOrderItemStatus());
		logger.info("OrderDAO::updateCustOrderItemDetails::Travel Date "+oOrderItemForm.getTravelDate());
		logger.info("OrderDAO::updateCustOrderItemDetails::CVV Status "+oOrderItemForm.getCvvStatus());
		logger.info("OrderDAO::updateCustOrderItemDetails::Color "+oOrderItemForm.getColor());
		logger.info("OrderDAO::updateCustOrderItemDetails::Size "+oOrderItemForm.getSize());
		logger.info("OrderDAO::updateCustOrderItemDetails::User Type "+p_sUserType);
		logger.info("OrderDAO::updateCustOrderItemDetails::Is Credit Card changed "+isCreditCardChanged);

		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,oOrderItemForm.getCustFirstName());
			cstmt.setString(3,oOrderItemForm.getCustLastName());
			cstmt.setString(4,oOrderItemForm.getCustAddress1());
			cstmt.setString(5,oOrderItemForm.getCustCity());
			cstmt.setString(6,oOrderItemForm.getCustState());	
			cstmt.setString(7,oOrderItemForm.getCustZip());
			cstmt.setString(8,oOrderItemForm.getCustEmail());
			cstmt.setString(9,oOrderItemForm.getCustPhone());
			cstmt.setString(10,oOrderItemForm.getCustShipFirstName());
			cstmt.setString(11,oOrderItemForm.getCustShipLastName());
			cstmt.setString(12,oOrderItemForm.getCustShipAddress1());
			cstmt.setString(13,oOrderItemForm.getCustShipPhone());
			cstmt.setString(14,oOrderItemForm.getCustShipCity());
			cstmt.setString(15,oOrderItemForm.getCustShipState());	
			cstmt.setString(16,oOrderItemForm.getCustShipZip());
			cstmt.setString(17,oOrderItemForm.getCustShipEmail());
			cstmt.setString(18,oOrderItemForm.getQty());
			cstmt.setString(19,oOrderItemForm.getCustId());
			cstmt.setString(20,oOrderItemForm.getOrderItemId());
			cstmt.setString(21,p_sUserType);
			cstmt.setString(22, oOrderItemForm.getOrderItemStatus());
			cstmt.setString(23, oOrderItemForm.getTravelDate());
			cstmt.setString(24,oOrderItemForm.getCustAddress2());
			cstmt.setString(25,oOrderItemForm.getCustShipAddress2());
			cstmt.setString(26,isCreditCardChanged);
			cstmt.setString(27,oOrderItemForm.getCvv());
			cstmt.setString(28,oOrderItemForm.getCvvStatus());
			cstmt.setString(29,oOrderItemForm.getColor());
			cstmt.setString(30,oOrderItemForm.getSize());
			cstmt.setString(31,oOrderItemForm.getCreditCardNo());
			cstmt.setString(32,oOrderItemForm.getCreditCardLFD());
			
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("updateCustOrderItemDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("updateCustOrderItemDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		return iIsUpdate;
	}

	public static List<ProductDetailsVO> getMostPopularMerchandizeDetails(ArrayList<CategoryVO> alCategory,LoginVO oLoginVO,String p_sScheme) throws Exception{	
		logger.info("OrderDAO::getMostPopularMerchandizeDetails::ENTER");		
		List<ProductDetailsVO> alProdDetails = new ArrayList<ProductDetailsVO>();
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sCateFolderName=null;
		String sImageViewPath = null;
		String sQry="{call usp_sbh_get_most_popular_merchandize(?,?)}";		 
		logger.info("OrderDAO::getMostPopularMerchandizeDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::getMostPopularMerchandizeDetails::SQL QUERY "+oLoginVO.getUserType());	
		logger.info("OrderDAO::getMostPopularMerchandizeDetails::SQL QUERY "+oLoginVO.getRefId());
		
		try{
			if("http".equalsIgnoreCase(p_sScheme)){
				sImageViewPath=System.getProperty("ImageViewPath").trim();
			}else{
				sImageViewPath=System.getProperty("SecureImageViewPath").trim();
			}
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,oLoginVO.getUserType());	
			cstmt.setString(2,oLoginVO.getRefId());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));

				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("Vendor")){
					if(alCategory!=null && alCategory.size()>0){
						for(CategoryVO oCategoryVO: alCategory){
							if(oCategoryVO.getCateId()!=null){
								if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
									sCateFolderName = oCategoryVO.getCateName();
							}					
						}
					}
				}
				else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("Airline")){
					sCateFolderName = "PrivateJetJaunts";					
				}
				oProductDetailsVO.setMainImgPath(sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType());
				alProdDetails.add(oProductDetailsVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getMostPopularMerchandizeDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("OrderDAO::getMostPopularMerchandizeDetails::EXIT");
		return alProdDetails;
	}
	public static String getorderPlacedTotAmount(LoginVO oLoginVO) throws Exception{	
		logger.info("OrderDAO::getorderPlacedTotAmount::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		
		String sTotalAmount=null;

		String sQry="{call usp_sbh_get_order_placed_tot_amt(?,?,?)}";		 
		logger.info("OrderDAO::getorderPlacedTotAmount::SQL QUERY "+sQry);					
		logger.info("OrderDAO::getorderPlacedTotAmount::User Type "+oLoginVO.getUserType());	
		logger.info("OrderDAO::getorderPlacedTotAmount::Ref Id "+oLoginVO.getRefId());	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,oLoginVO.getUserType());	
			cstmt.setString(2,oLoginVO.getRefId());	
			cstmt.registerOutParameter(3, Types.VARCHAR);			
			cstmt.execute();

			sTotalAmount = cstmt.getString(3);			
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getorderPlacedTotAmount::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("OrderDAO::getorderPlacedTotAmount::EXIT");
		return sTotalAmount;
	}

	public static List<OrderItemDetailsVO> getVendorOrderItemDetails(String p_sSelectedProcess) throws Exception{	
		logger.info("OrderDAO::getVendorOrderItemDetails::ENTER");		
		OrderItemDetailsVO oOrderItemDetailsVO=null;
		String sDBData = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_vend_order_item_details(?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.info("OrderDAO::getVendorOrderItemDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::getVendorOrderItemDetails::Selected Process "+p_sSelectedProcess);

		List<OrderItemDetailsVO> alOrderItemDetails = new ArrayList<OrderItemDetailsVO>();

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sSelectedProcess);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()) {				
				oOrderItemDetailsVO=new OrderItemDetailsVO();
				
				sDBData = rs.getString("cust_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustId(sDBData);
				
				sDBData = rs.getString("cust_fname");
				sDBData = sDBData == null?"":convertToNameFormat(sDBData.trim());
				oOrderItemDetailsVO.setCustFirstName(sDBData);
				
				sDBData = rs.getString("cust_lname");
				sDBData = sDBData == null?"":convertToNameFormat(sDBData.trim());
				oOrderItemDetailsVO.setCustLastName(sDBData);
				
				sDBData = rs.getString("cust_email");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustEmail(sDBData);
				
				sDBData = rs.getString("cust_phone");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustPhone(sDBData);
				
				sDBData = rs.getString("cust_ship_phone");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustShipPhone(sDBData);
				
				sDBData = rs.getString("cust_address1");
				sDBData = sDBData == null?"":convertToNameFormat(sDBData.trim());
				oOrderItemDetailsVO.setCustAddress1(sDBData);
				
				sDBData = rs.getString("cust_address2");
				sDBData = sDBData == null?"":convertToNameFormat(sDBData.trim());
				oOrderItemDetailsVO.setCustAddress2(sDBData);
				
				sDBData = rs.getString("cust_city");
				sDBData = sDBData == null?"":convertToNameFormat(sDBData.trim());
				oOrderItemDetailsVO.setCustCity(sDBData);
				
				sDBData = rs.getString("cust_state");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustState(sDBData);
				
				sDBData = rs.getString("cust_country");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustCountry(sDBData);
				
				sDBData = rs.getString("cust_zip");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustZip(sDBData);	

				String sShipFName = rs.getString("cust_ship_fname");
				sShipFName = sShipFName ==null?"":sShipFName.trim();
				oOrderItemDetailsVO.setCustShipFirstName(convertToNameFormat(sShipFName));

				String sShipLName = rs.getString("cust_ship_lname");
				sShipLName = sShipLName ==null?"":sShipLName.trim();
				oOrderItemDetailsVO.setCustShipLastName(convertToNameFormat(sShipLName));

				sDBData = rs.getString("cust_ship_email");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustShipEmail(sDBData);
				//oOrderItemDetailsVO.setCustShipPhone(rs.getString("cust_ship_phone"));
				String sShipAddr1 = rs.getString("cust_ship_address1");
				sShipAddr1 = sShipAddr1 ==null?"":sShipAddr1.trim();				
				oOrderItemDetailsVO.setCustShipAddress1(convertToNameFormat(sShipAddr1));

				String sShipAddr2 = rs.getString("cust_ship_address2");
				sShipAddr2 = sShipAddr2 ==null?"":sShipAddr2.trim();
				oOrderItemDetailsVO.setCustShipAddress2(convertToNameFormat(sShipAddr2));

				String sShipCity = rs.getString("cust_ship_city");
				sShipCity = sShipCity ==null?"":sShipCity.trim();
				oOrderItemDetailsVO.setCustShipCity(convertToNameFormat(sShipCity));

				sDBData = rs.getString("cust_ship_state");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustShipState(sDBData);
				
				sDBData = rs.getString("cust_ship_country");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustShipCountry(sDBData);
				
				sDBData = rs.getString("cust_ship_zip");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustShipZip(sDBData);				
				
				sDBData = rs.getString("prod_code");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setProdCode(sDBData);
				
				sDBData = rs.getString("status");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOrderItemStatus(sDBData);
				
				sDBData = rs.getString("cate_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCateId(sDBData);
				
				sDBData = rs.getString("qty");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setQty(sDBData);
				
				sDBData = rs.getString("vend_price");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setVendPrice(sDBData);
				
				sDBData = rs.getString("sbh_price");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setSbhPrice(sDBData);
				
				sDBData = rs.getString("air_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirName(sDBData);
				
				sDBData = rs.getString("flight_no");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setFlightNo(sDBData);
				
				sDBData = rs.getString("order_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOrderId(sDBData);
				
				sDBData = rs.getString("cust_trans_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustTransId(sDBData);
				
				sDBData = rs.getString("order_dt");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOrderCreatedDt(sDBData);
				
				sDBData = rs.getString("order_item_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOrderItemId(sDBData);
				
				
				String sOwnerId = rs.getString("owner_id");
				sOwnerId= sOwnerId==null?"":sOwnerId.trim();
				oOrderItemDetailsVO.setOwnerId(sOwnerId);
				
				sDBData = rs.getString("owner_type");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerType(sDBData);
				
				sDBData = rs.getString("owner_email");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerEmail(sDBData);
				
				sDBData = rs.getString("owner_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setVendorName(sDBData);
				
				sDBData = rs.getString("owner_contact_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerContactName(sDBData);
				
				sDBData = rs.getString("item_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setItemName(sDBData);
				
				sDBData = rs.getString("brand_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setBrandName(sDBData);
				
				sDBData = rs.getString("prod_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setProdId(sDBData);
				
				sDBData = rs.getString("main_img_path");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setMainImgPath(sDBData);
				
				sDBData = rs.getString("main_img_type");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setMainImgType(sDBData);
				
				sDBData = rs.getString("main_img_caption");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setMainImgCaption(sDBData);
				
				sDBData = rs.getString("cc_no");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCCNo(sDBData);
				
				sDBData = rs.getString("key_ref_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setKeyRefId(sDBData);
				
				/*sDBData = rs.getString("cc_no_status");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCCNoStatus(sDBData);*/
				
				sDBData =  decryptInputData(rs.getString("cc_no_fname"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCardFname(sDBData);
				
				
				
				sDBData = decryptInputData(rs.getString("card_type"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCardType(sDBData);
				
				sDBData = rs.getString("amount");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setPayAmount(sDBData);
				
				sDBData =  decryptInputData(rs.getString("exp_dt"), "CLIENT", oOrderItemDetailsVO.getKeyRefId());
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setExpDt(sDBData);
				
				sDBData = rs.getString("customer_service_phoneno");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCustServicePhone(sDBData);	
				
				sDBData = rs.getString("travel_date");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setTravelDate(sDBData);
				
				sDBData = rs.getString("charge_type");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setChargeType(sDBData);
				
				sDBData = rs.getString("cvv");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setCvv(sDBData);
				
				sDBData = rs.getString("po_pct");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setPoPct(sDBData);
				
				sDBData = rs.getString("rma_number");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setRmaGeneratedNo(sDBData);
				
				sDBData = rs.getString("rma_status");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setRmaStatus(sDBData);
				
				sDBData = rs.getString("vendor_comments");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setVendorComments(sDBData);
				
				sDBData = rs.getString("cust_return_qty");
				sDBData = sDBData == null?"":sDBData.trim();
				oOrderItemDetailsVO.setReturnQuantity(sDBData);
				
				sDBData = rs.getString("owner_addr1");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerAddr1(sDBData);

				sDBData = rs.getString("owner_addr2");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerAddr2(sDBData);

				sDBData = rs.getString("owner_city");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerCity(sDBData);

				sDBData = rs.getString("owner_state");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerState(sDBData);

				sDBData = rs.getString("owner_zip");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerZip(sDBData);

				sDBData = rs.getString("owner_country");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerCountry(sDBData);

				sDBData = rs.getString("owner_phone");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerPhone(sDBData);

				sDBData = rs.getString("owner_phone_ext");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerPhoneExt(sDBData);


				sDBData = rs.getString("preauth_cc");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setPreAuthCC(sDBData);


				sDBData = rs.getString("invoice_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setInvoiceNo(sDBData);
				
				sDBData = rs.getString("po_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setPoNo(sDBData);
				
				
				Date dDate = rs.getDate("po_date");
				if(dDate!=null){
					String sDate = sdfOutput.format(dDate);
					oOrderItemDetailsVO.setPoDate(sDate);
				}else{
					oOrderItemDetailsVO.setPoDate("");
				}
				
				sDBData = rs.getString("cancel_reason");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setCancelReason(sDBData);
				
				sDBData = rs.getString("shipment_status");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setShipmentStatus(sDBData);
				
				sDBData = rs.getString("owner_login_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerLoginId(sDBData);

				sDBData = rs.getString("cust_spl_inst");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setSpecialInst(sDBData);
				
				sDBData = rs.getString("cust_personal_info");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setPersonalInfo(sDBData);
				
				sDBData = rs.getString("air_contact_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirContactName(sDBData);
				
				sDBData = rs.getString("air_addr1");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirAddr1(sDBData);
				
				sDBData = rs.getString("air_addr2");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirAddr2(sDBData);
				
				sDBData = rs.getString("air_city");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirCity(sDBData);
				
				sDBData = rs.getString("air_state");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirState(sDBData);
				
				sDBData = rs.getString("air_phone");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirPhone(sDBData);
				
				sDBData = rs.getString("air_zip");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirZip(sDBData);
				
				sDBData = rs.getString("air_user_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirUserId(sDBData);
				
				sDBData = rs.getString("air_email");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirEmail(sDBData);
				
				sDBData = rs.getString("air_comm_pct");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setAirCommPct(sDBData);
				
				sDBData = rs.getString("size");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setSize(sDBData);
				
				sDBData = rs.getString("color");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setColor(sDBData);
				
				sDBData = rs.getString("cust_spl_inst");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setSpecialInst(sDBData);
				
				sDBData = rs.getString("cust_personal_info");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setPersonalInfo(sDBData);
				
				sDBData = rs.getString("owner_secondary_email");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setOwnerSecondaryEmail(sDBData);
				
				sDBData = rs.getString("cc_no_lfd");
				sDBData = sDBData==null?"":sDBData.trim();
				oOrderItemDetailsVO.setCreditCardNoLFD(sDBData);
				
				alOrderItemDetails.add(oOrderItemDetailsVO);		
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getVendorOrderItemDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("OrderDAO::getVendorOrderItemDetails::EXIT");
		return alOrderItemDetails;
	}


	public static boolean sendOrderItemsEmailToVendor(OrderItemDetailsVO oOrderItemDetailsVO) throws Exception{
		logger.info("OrderDAO:sendOrderItemsEmailToVendor:ENTER");
		
		String sEmailContent="";
		boolean bSentEmail=true;
		String sToAddr="";
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null;
		String sSkyBuyLogoPath = null,sEmailCaption =null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String  sEmailTemplateId =null,sFromEmailAddr=null,sSecondaryEmail = "";
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
				sEmailTemplateId=oBundle.getString("email.order.vendor.placement");
			else
				sEmailTemplateId=oBundle.getString("email.order.airline.placement");
			
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Heading}}",sEmailCaption);
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);


			sToAddr=oOrderItemDetailsVO.getOwnerEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();	

			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}

			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingEmail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			if(oOrderItemDetailsVO.getSpecialInst()!=null && !oOrderItemDetailsVO.getSpecialInst().equalsIgnoreCase("")){
				hmTags.put("{{SpecialInst}}","<tr align='center' ><td colspan='2'  align='left' valign='top'  style='border:1px solid #000;'>"+oOrderItemDetailsVO.getSpecialInst()+"</td></tr>");
			}else{
				hmTags.put("{{SpecialInst}}",oOrderItemDetailsVO.getSpecialInst());
			}
			
			if(oOrderItemDetailsVO.getPersonalInfo()!=null && !oOrderItemDetailsVO.getPersonalInfo().equalsIgnoreCase("")){
				hmTags.put("{{PersonalMsg}}","<tr align='center' ><td colspan='2'  align='left' valign='top'  style='border:1px solid #000;'>"+oOrderItemDetailsVO.getPersonalInfo()+"</td></tr>");
			}else{
				hmTags.put("{{PersonalMsg}}",oOrderItemDetailsVO.getPersonalInfo());
			}
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));
			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			double dPoPct=Double.parseDouble(oOrderItemDetailsVO.getPoPct());
		
			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td align=left><b>Brand Name:&nbsp;</b>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
			}	
			sItemDetails = sItemDetails + "<tr><td align=left><b>Item Name:&nbsp;</b>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";	
			sItemDetails = sItemDetails + "<tr><td align=left><b>Item Code:&nbsp;</b>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left><b>Size:&nbsp;</b>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails + "<tr><td align=left><b>Color:&nbsp;</b>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left><b>Travel Date:&nbsp;</b>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	

			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			
			hmTags.put("{{ItemName}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{ItemDetails}}",sItemDetails);
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());

			double dPOAmt = (dSbhPrice*(dPoPct/100));
			hmTags.put("{{RetailPrice}}",numberFormat.format(dPOAmt));
			hmTags.put("{{RetailPriceTotalAmount}}",numberFormat.format(dQty*dPOAmt));	
			hmTags.put("{{VendorName}}",convertToNameFormat(oOrderItemDetailsVO.getVendorName()));
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			/*if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}*/

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			oEmail.sendEmail(oEmailLogVo);
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderItemsEmailToVendor:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendOrderItemsEmailToVendor:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}

	public static boolean sendOrderItemsFailureEmail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason,long p_lOrderNumber,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendOrderItemsFailureEmail:ENTER");
		
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sEmailContent="";	
		String sToAddr="";		
		HashMap<String,String> hmTags = new HashMap<String, String>();	
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.order.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	

			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			if(p_lOrderNumber==0){
				hmTags.put("{{OrderNumber}}","NA");
			}else{
				hmTags.put("{{OrderNumber}}",p_lOrderNumber+"");
			}
			hmTags.put("{{AirName}}",oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			/*sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
			
			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();



			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			/*if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}*/


			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderItemsFailureEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendOrderItemsFailureEmail:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}
	public static boolean sendPlaceOrderFailureEmailToCustomer(com.sbh.client.vo.OrderDetailsVO p_oOrderDetailsVO, com.sbh.client.vo.OrderItemsVO p_oOrderItemsVO, String p_sRemoteHost, String p_sDeviceId) 
	throws Exception {
		boolean bEmailSent = false;
		logger.info("OrderDAO::sendPlaceOrderFailureEmailToCustomer:ENTRY");
		
		String sEmailContent="";	
		String sToAddr="";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.place.order.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	

			hmTags.put("{{AirName}}",p_oOrderDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",p_oOrderDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",p_oOrderDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",p_oOrderDetailsVO.getOrderDt());
			hmTags.put("{{CustomerTransId}}",p_oOrderDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderDetailsVO.getCustBillFname() +" "+p_oOrderDetailsVO.getCustBillLname()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderDetailsVO.getCustBillFname()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderDetailsVO.getCustBillLname()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderDetailsVO.getCustBillPhone())));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderDetailsVO.getCustBillAddr1(), p_oOrderDetailsVO.getCustBillAddr2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderDetailsVO.getCustBillCity()));
			hmTags.put("{{BillingState}}",p_oOrderDetailsVO.getCustBillState());
			hmTags.put("{{BillingZip}}",p_oOrderDetailsVO.getCustBillZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderDetailsVO.getCustShipFname()+" "+p_oOrderDetailsVO.getCustShipLname()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",p_oOrderDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderDetailsVO.getCustShipAddr1(), p_oOrderDetailsVO.getCustShipAddr2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",p_oOrderDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",p_oOrderDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{BillingMail}}",p_oOrderDetailsVO.getCustShipEmail());
			hmTags.put("{{CCNo}}","");
			hmTags.put("{{ExpDate}}",decryptInputData(p_oOrderDetailsVO.getExpDt(), "CLIENT", p_oOrderDetailsVO.getKeyRefId()));
			String sCardType = decryptInputData(p_oOrderDetailsVO.getCardType(), "CLIENT", p_oOrderDetailsVO.getKeyRefId());
			
			if("VC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","Visa Card");
			}else if("MC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","Master Card");
			}else if("AC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","American Express");
			}
			String CCFName = "";
			if(p_oOrderDetailsVO.getCardFname() != null && p_oOrderDetailsVO.getCardFname().trim().length() > 0)
				CCFName = decryptInputData(p_oOrderDetailsVO.getCardFname(), "CLIENT", p_oOrderDetailsVO.getKeyRefId());
			else 
				CCFName = "";
			
			hmTags.put("{{CCFName}}",convertToNameFormat(CCFName));
			
			
			double dQty=  0;
			if(p_oOrderItemsVO.getQty()!=null && p_oOrderItemsVO.getQty().trim().length() > 0)
				dQty=Double.parseDouble(p_oOrderItemsVO.getQty());
			
			double dSbhPrice= 0;
			if(p_oOrderItemsVO.getSbhPrice() != null && p_oOrderItemsVO.getSbhPrice().trim().length() > 0)
				dSbhPrice= Double.parseDouble(p_oOrderItemsVO.getSbhPrice());
			
			OrderItemDetailsVO oOrderItemDetailsVO = getProductDetails(p_oOrderItemsVO.getProdId());
			if("VENDOR".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if("VENDOR".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
				if(p_oOrderItemsVO.getProdSize()!= null && p_oOrderItemsVO.getProdSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemsVO.getProdSize()+"</td></tr>";
				if(p_oOrderItemsVO.getProdColor()!= null && p_oOrderItemsVO.getProdColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemsVO.getProdColor()+"</td></tr>";
			}	
			if("AIRLINE".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			
			hmTags.put("{{Category}}",p_oOrderItemsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",p_oOrderItemsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			
			sToAddr=p_oOrderDetailsVO.getCustBillEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();



			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sRemoteHost+"@"+p_sDeviceId,p_sDeviceId,"");
			System.out.print("EmailContent "+sEmailContent);

		}catch(Exception e){		
			e.printStackTrace();
			bEmailSent=false;
			logger.error("OrderDAO:sendPlaceOrderFailureEmailToCustomer:Exception "+e.getMessage());
			throw e;
		}
		
		logger.info("OrderDAO::sendPlaceOrderFailureEmailToCustomer:EXIT");
		return bEmailSent;
	}
	public static boolean sendOrderItemsRejectionEmail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason,long p_lOrderNumber,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendOrderItemsRejectionEmail::ENTER");

		String sEmailContent="";	
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails ="";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.order.Rejected");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			if(p_lOrderNumber==0){
				hmTags.put("{{OrderNumber}}","NA");
			}else{
				hmTags.put("{{OrderNumber}}",p_lOrderNumber+"");
			}
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
			
			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));		
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId, p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.info("OrderDAO::sendOrderItemsRejectionEmail::Exception "+e.getMessage());
			throw e;
		}	
		
		logger.info("OrderDAO::sendOrderItemsRejectionEmail::EXIT");
		return bSentEmail;
	}

	public static boolean sendOrderItemsFailureEmailToVendor(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason,long p_lOrderNumber,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendOrderItemsFailureEmailToVendor:ENTER");

		String sEmailContent="";	
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails ="", sSecondaryEmail = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.vendor.order.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	

			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{VendorName}}",oOrderItemDetailsVO.getVendorName());
			if(p_lOrderNumber==0){
				hmTags.put("{{OrderNumber}}","NA");
			}else{
				hmTags.put("{{OrderNumber}}",p_lOrderNumber+"");
			}
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{VendorContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingEmail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}
			
			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td align=left><b>Brand Name:&nbsp;</b>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
			}	
			sItemDetails = sItemDetails + "<tr><td align=left><b>Item Name:&nbsp;</b>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";	
			sItemDetails = sItemDetails + "<tr><td align=left><b>Item Code:&nbsp;</b>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left><b>Size:&nbsp;</b>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails + "<tr><td align=left><b>Color:&nbsp;</b>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left><b>Travel Date:&nbsp;</b>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	

			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());

			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			double dPoPct=Double.parseDouble(oOrderItemDetailsVO.getPoPct());
			double dPOAmt = (dSbhPrice*(dPoPct/100));
			hmTags.put("{{RetailPrice}}",numberFormat.format(dPOAmt));
			hmTags.put("{{RetailPriceTotalAmount}}",numberFormat.format(dQty*dPOAmt));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");

			sToAddr=oOrderItemDetailsVO.getOwnerEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			/*if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}*/

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));

			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));		
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderItemsFailureEmailToVendor:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendOrderItemsFailureEmailToVendor:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}
	public static boolean sendOrderItemsSuccessEmail(OrderItemDetailsVO oOrderItemDetailsVO,byte[] p_baPdf, long p_lOrderNumber,File p_oFile,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendOrderItemsSuccessEmail::ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="";		
		HashMap<String,String> hmTags = new HashMap<String, String>();
		double dSkyBuyPriceTotal=0.00;
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		int iStartIndex=0,iEndIndex = 0;
		String sBccAddr="",sCcAddr="";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.order.invoice");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);


			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{CustomerPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(mergeName(oOrderItemDetailsVO.getCustShipFirstName(),oOrderItemDetailsVO.getCustShipLastName())));
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	

			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",p_lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");


			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			oEmailLogVo.setAttachmentFile(p_oFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(p_baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId,p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderItemsSuccessEmail::EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO::sendOrderItemsSuccessEmail::Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}
	/*public static boolean sendOrderItemsSuccessEmailToVendor(OrderItemDetailsVO oOrderItemDetailsVO,long p_lOrderNumber,File p_oFile) throws Exception{
		logger.info("MerchandizeDAO:sendOrderItemsSuccessEmail:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap hmTags = new HashMap();
		double dVendorPriceTotal=0.00,dSkyBuyPriceTotal=0.00;
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId=null,sEmailContent=null;
		String sURL = null;
		EmailVO oEmailVO =null;
		int iStartIndex=0,iEndIndex = 0;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.vendor.order.success");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);


			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			if("vendor".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())) {
				sURL = oBundle.getString("skbuyhigh.vendor.url");
			} else if("airline".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())) {
				sURL = oBundle.getString("skbuyhigh.airline.url");
			}
			sURL = sURL == null ? "":sURL.trim();
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
			hmTags.put("{{URL}}",sURL);


			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{VendorContactName}}",oOrderItemDetailsVO.getOwnerContactName());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",getMaskCCNo(oOrderItemDetailsVO.getCreditCardNoLFD()));
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	

			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",p_lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");


			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();


			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sAdminEmailAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			oEmailLogVo.setAttachmentFile(p_oFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

	 *//** Insert Email Log Details **//*
			EmailDAO.insertEmailLogDetails(oEmailLogVo);
			System.out.print("EmailContent "+sEmailContent);

			logger.info("MerchandizeDAO::sendOrderItemsSuccessEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("MerchandizeDAO:sendOrderItemsSuccessEmail:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}*/
	public static void updateOrderItemStatus(String p_sOrderItemId,String p_sOrderStatus,byte[] p_bBlobObj,String p_sLoginId) throws Exception{	
		logger.info("OrderDAO::updateOrderItemStatus::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		

		String sQry="{call usp_sbh_upd_order_item_status(?,?,?,?)}";		 
		logger.info("OrderDAO::updateOrderItemStatus::SQL QUERY "+sQry);				
		logger.info("OrderDAO::updateOrderItemStatus::Order Item Id "+p_sOrderItemId);				
		logger.info("OrderDAO::updateOrderItemStatus::Order Status "+p_sOrderStatus);				
		logger.info("OrderDAO::updateOrderItemStatus::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setString(2,p_sOrderStatus);
			cstmt.setBytes(3, p_bBlobObj);
			cstmt.setString(4,p_sLoginId);
			cstmt.executeUpdate();

			logger.info("OrderDAO::updateOrderItemStatus::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::updateOrderItemStatus::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}	
	}
	public static void insertOrderAuditDetails(String p_sOrderItemId, String p_sOrder_Id,String p_sCustTransId,String p_sMsg,String p_sCreateId,Connection p_con,String p_sHostName,String p_sReqeustFrom) throws Exception{	
		logger.info("OrderDAO::insertOrderAuditDetails::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_order_audit_msg(?,?,?,?,?,?,?)}";
		logger.info("OrderDAO::insertOrderAuditDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::insertOrderAuditDetails::Order Item Id : "+p_sOrderItemId);
		logger.info("OrderDAO::insertOrderAuditDetails::Order Id : "+p_sOrder_Id);
		logger.info("OrderDAO::insertOrderAuditDetails::Cust trans Id :"+p_sCustTransId);
		logger.info("OrderDAO::insertOrderAuditDetails::Msg :"+p_sMsg);
		logger.info("OrderDAO::insertOrderAuditDetails::Create Id :"+p_sCreateId);


		try{
			if(p_con==null){				
				con = DBConnection.getSQL2005Connection();
				cstmt=con.prepareCall(sQry);
			}		
			else{
				cstmt=p_con.prepareCall(sQry);
			}   
			cstmt.setString(1,p_sOrderItemId);
			cstmt.setString(2,p_sOrder_Id);
			cstmt.setString(3,p_sCustTransId);
			cstmt.setString(4,p_sMsg);
			cstmt.setString(5,p_sCreateId);			
			cstmt.setString(6,p_sHostName);
			cstmt.setString(7,p_sReqeustFrom);
			cstmt.execute();			
			logger.info("OrderDAO::insertOrderAuditDetails::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::insertOrderAuditDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();

		}

	}
	public static List<OrderItemDetailsVO> getOrderItemDetailsStatus(String p_sSelectedProcess) {
		logger.info("OrderDAO::getOrderItemDetails::ENTRY");

		String sQuery = "{call usp_sbh_get_order_item_details_status(?)}";
		List<OrderItemDetailsVO> orderItemDetailsList = new ArrayList<OrderItemDetailsVO>();
		OrderItemDetailsVO oOrderItemDetailsVO;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		logger.info("OrderDAO::insertOrderAuditDetails::SQL QUERY "+sQuery);
		logger.info("OrderDAO::getOrderItemDetails::Selected Process"+p_sSelectedProcess);
		try {
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_sSelectedProcess);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()) {	
				oOrderItemDetailsVO = new OrderItemDetailsVO();

				oOrderItemDetailsVO.setPaymentTxnRefId(rs.getString("pay_txn_ref_id"));
				oOrderItemDetailsVO.setCustFirstName(convertToNameFormat(rs.getString("cust_fname"))+" "+convertToNameFormat(rs.getString("cust_lname")));
//				oOrderItemDetailsVO.setCustEmail(rs.getString("cust_email"));
				oOrderItemDetailsVO.setCustPhone(rs.getString("cust_phone"));
				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setItemName(rs.getString("item_name"));
				oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
//				oOrderItemDetailsVO.setOrderCreatedDt(rs.getString("order_dt"));
				oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));
				oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
				oOrderItemDetailsVO.setOwnerId(rs.getString("owner_id"));
				oOrderItemDetailsVO.setPreAuthCC(rs.getString("preauth_cc"));
				oOrderItemDetailsVO.setChargeType(rs.getString("charge_type"));

				orderItemDetailsList.add(oOrderItemDetailsVO);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::getOrderItemDetails::Exception"+exception.getMessage());

		}
		logger.info("OrderDAO::getOrderItemDetails::EXIT");

		return orderItemDetailsList;
	}
	
	public static OrderItemDetailsVO getOrderItemDetailStatus(String p_sOrderItemId) throws SQLException {
		logger.info("OrderDAO::getOrderItemDetailStatus::ENTRY");

		String sQuery = "{call usp_sbh_get_order_item_details_status(?)}";
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		logger.info("OrderDAO::getOrderItemDetailStatus::SQL QUERY "+sQuery);
		logger.info("OrderDAO::getOrderItemDetailStatus::OrderItem Id"+ p_sOrderItemId);
		
		try {
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_sOrderItemId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()) {	
				oOrderItemDetailsVO = new OrderItemDetailsVO();

				oOrderItemDetailsVO.setPaymentTxnRefId(rs.getString("pay_txn_ref_id"));
				oOrderItemDetailsVO.setCustFirstName(convertToNameFormat(rs.getString("cust_fname"))+" "+convertToNameFormat(rs.getString("cust_lname")));
//				oOrderItemDetailsVO.setCustEmail(rs.getString("cust_email"));
				oOrderItemDetailsVO.setCustPhone(rs.getString("cust_phone"));
				oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
				oOrderItemDetailsVO.setQty(rs.getString("qty"));
				oOrderItemDetailsVO.setItemName(rs.getString("item_name"));
				oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
//				oOrderItemDetailsVO.setOrderCreatedDt(rs.getString("order_dt"));
				oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));
				oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
				oOrderItemDetailsVO.setOwnerId(rs.getString("owner_id"));
				oOrderItemDetailsVO.setPreAuthCC(rs.getString("preauth_cc"));
				oOrderItemDetailsVO.setChargeType(rs.getString("charge_type"));

			}
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::getOrderItemDetailStatus::Exception"+exception.getMessage());

		}finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		logger.info("OrderDAO::getOrderItemDetailStatus::EXIT");

		return oOrderItemDetailsVO;
	}
	
	public static PaymentInformationVO getCreditCardInformation(String p_sOrderItemId) 
	throws Exception{
		logger.info("OrderDAO::getCreditCardInformation::ENTRY");
		
		String sQry="{call usp_sbh_get_cust_credit_card_details(?)}";
		String sDBData = "";
		PaymentInformationVO paymentInfoVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		logger.info("OrderDAO::getCreditCardInformation::SQL QUERY "+sQry);
		logger.info("OrderDAO::getCreditCardInformation::OrderItem Id"+ p_sOrderItemId);
		
		try {
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sOrderItemId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				paymentInfoVO = new PaymentInformationVO();
				while(rs.next()) {

					paymentInfoVO.setCustFirstName(convertToNameFormat(rs.getString("cust_bill_fname")));
					paymentInfoVO.setCustLastName(convertToNameFormat(rs.getString("cust_bill_lname")));
					paymentInfoVO.setCustAddress1(rs.getString("cust_bill_addr1"));
					sDBData = rs.getString("cust_bill_addr2");
					sDBData = sDBData == null?"":sDBData.trim();
					paymentInfoVO.setCustAddress2(sDBData);
					paymentInfoVO.setCustPhone(rs.getString("cust_phone"));
					paymentInfoVO.setCustEmail(rs.getString("cust_bill_email"));
					paymentInfoVO.setCustCity(rs.getString("cust_bill_city"));
					paymentInfoVO.setCustState(rs.getString("cust_bill_state"));
					paymentInfoVO.setCustCountry(rs.getString("cust_bill_country"));
					paymentInfoVO.setCustZip(rs.getString("cust_bill_zip"));
					paymentInfoVO.setKeyRefId(rs.getString("key_ref_id"));
					paymentInfoVO.setCardType(decryptInputData(rs.getString("card_type"),"CLIENT",paymentInfoVO.getKeyRefId()));
					
					String sCCFName = decryptInputData(rs.getString("fname"),"CLIENT",paymentInfoVO.getKeyRefId());
					paymentInfoVO.setCreditCardHolderName(convertToNameFormat(sCCFName));
					
					paymentInfoVO.setCreditCardNo(rs.getString("cc_no"));
					paymentInfoVO.setCreditCardNoLFD(rs.getString("cc_no_lfd"));
					
					String expiryDate =  decryptInputData(rs.getString("exp_dt"),"CLIENT",paymentInfoVO.getKeyRefId());
					paymentInfoVO.setExpMonth(getMonth(expiryDate));
					
					paymentInfoVO.setExpYear(getYear(expiryDate));
					paymentInfoVO.setOrderItemId(p_sOrderItemId);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getCreditCardInformation::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("OrderDAO::getCreditCardInformation::EXIT");
		return paymentInfoVO;
	}
	public static KeyVO encryptData(String p_sInputData, String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::encryptData::ENTRY");
		
		String sEncryptedData = "";
		KeyVO oKeyVO = getPublicKeyToEncryptUserCredential(p_sRequestType, p_sRefId);
		if(oKeyVO != null) {
			PublicKey oPublicKey = getPublicKeyFromString(oKeyVO.getPublicKey());
			sEncryptedData = encrypt(p_sInputData, oPublicKey);
			oKeyVO.setEncryptedData(sEncryptedData);
		}
		logger.info("OrderDAO::encryptData::EXIT");
		return oKeyVO;
	}
	public static String encryptInputData(String p_sInputData, String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::encryptInputData::ENTRY");
		
		String sEncryptedData = "";
		KeyVO oKeyVO = getPublicKey(p_sRequestType, p_sRefId);
		if(oKeyVO != null) {
			PublicKey oPublicKey = getPublicKeyFromString(oKeyVO.getPublicKey());
			sEncryptedData = encrypt(p_sInputData, oPublicKey);
		}
		logger.info("OrderDAO::encryptInputData::EXIT");
		return sEncryptedData;
	}
	public static String decryptInputData(String p_sInputData, String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::decryptInputData::ENTRY");
		
		String sDecryptedData = "";
		KeyVO oKeyVO = getPrivateKey(p_sRequestType,p_sRefId);
		PrivateKey oPrivateKey = getPrivateKeyFromString(oKeyVO.getPrivateKey());
		
		sDecryptedData = decrypt(p_sInputData, oPrivateKey);
		
		logger.info("OrderDAO::decryptInputData::EXIT");
		return sDecryptedData;
	}
	public static String decryptData(String p_sInputData, String p_sRefId) throws Exception {
		logger.info("OrderDAO::decryptInputData::ENTRY");
		
		String sDecryptedData = "";
		String sPrivateKey = "";
		String sRefId = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		KeyVO oKeyVO = null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_private_key_by_refId(?)}";

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sRefId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oKeyVO = new KeyVO();
					sPrivateKey = rs.getString("checksum_one");
					sPrivateKey = sPrivateKey == null?"":sPrivateKey.trim();
					sRefId = rs.getString("id");
					sRefId = sRefId == null?"":sRefId.trim();
					
					oKeyVO.setPrivateKey(sPrivateKey);
					oKeyVO.setRefId(sRefId);
				}
			}
			if(oKeyVO != null) {
				PrivateKey oPrivateKey = getPrivateKeyFromString(oKeyVO.getPrivateKey());
				sDecryptedData = decrypt(p_sInputData, oPrivateKey);
			}
		}catch(Exception e) {
			logger.debug("OrderDAO::getPrivateKey::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		
		logger.info("OrderDAO::decryptInputData::EXIT");
		return sDecryptedData;
	}
	/*public static String decryptUserCredential(String p_sInputData) throws Exception {
		logger.info("OrderDAO::decryptInputData::ENTRY");
		
		PrivateKey oPrivateKey = getPrivateKeyFromString(getPrivateKeyToDecryptUserCredential());
		String sDecryptedData = "";
		sDecryptedData = decrypt(p_sInputData, oPrivateKey);
		
		logger.info("OrderDAO::decryptInputData::EXIT");
		return sDecryptedData;
	}*/
	public static KeyVO getPublicKey(String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::getPublicKey::ENTRY");
		
		String sPublicKey = "";
		String sRefId = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		KeyVO oKeyVO = null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_private_key(?,?)}";

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sRequestType);
			cstmt.setString(2, p_sRefId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oKeyVO = new KeyVO();
					sPublicKey = rs.getString("checksum_two");
					sPublicKey = sPublicKey == null?"":sPublicKey.trim();
					sRefId = rs.getString("id");
					sRefId = sRefId == null?"":sRefId.trim();
					
					oKeyVO.setPublicKey(sPublicKey);
					oKeyVO.setRefId(sRefId);
				}
			}
		}catch(Exception e) {
			logger.debug("OrderDAO::getPublicKey::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getPublicKey::EXIT");
		return oKeyVO;
	}
	public static KeyVO getPublicKeyToEncryptUserCredential(String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::getPublicKey::ENTRY");
		
		String sPublicKey = "";
		String sRefId = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		KeyVO oKeyVO = null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_public_key(?,?)}";

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sRequestType);
			cstmt.setString(2, p_sRefId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oKeyVO = new KeyVO();
					sPublicKey = rs.getString("checksum_two");
					sPublicKey = sPublicKey == null?"":sPublicKey.trim();
					sRefId = rs.getString("id");
					sRefId = sRefId == null?"":sRefId.trim();
					
					oKeyVO.setPublicKey(sPublicKey);
					oKeyVO.setRefId(sRefId);
				}
			}
		}catch(Exception e) {
			logger.debug("OrderDAO::getPublicKey::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getPublicKey::EXIT");
		return oKeyVO;
	}
	public static KeyVO getPrivateKey(String p_sRequestType, String p_sRefId) throws Exception {
		logger.info("OrderDAO::getPrivateKey::ENTRY");
		
		String sPrivateKey = "";
		String sRefId = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		KeyVO oKeyVO = null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_private_key(?,?)}";

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sRequestType);
			cstmt.setString(2, p_sRefId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oKeyVO = new KeyVO();
					sPrivateKey = rs.getString("checksum_one");
					sPrivateKey = sPrivateKey == null?"":sPrivateKey.trim();
					sRefId = rs.getString("id");
					sRefId = sRefId == null?"":sRefId.trim();
					
					oKeyVO.setPrivateKey(sPrivateKey);
					oKeyVO.setRefId(sRefId);
				}
			}
		}catch(Exception e) {
			logger.debug("OrderDAO::getPrivateKey::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getPrivateKey::EXIT");
		return oKeyVO;
	}
	/*private static String getPrivateKeyToDecryptUserCredential() throws Exception {
		logger.info("OrderDAO::getPrivateKey::ENTRY");
		
		String sPrivateKey = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_user_private_key()}";

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					sPrivateKey = sPrivateKey == null?"":sPrivateKey.trim();
				}
			}
		}catch(Exception e) {
			logger.debug("OrderDAO::getPrivateKey::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getPrivateKey::EXIT");
		return sPrivateKey;
	}*/
	public static void updateCreditCardDetails(OrderItemForm p_oOrderItemForm,String p_sLoginId) throws Exception {

		logger.info("OrderDAO::updateCreditCardDetails::ENTRY");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_update_credit_card_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		logger.info("OrderDAO::updateCreditCardDetails::SQL QUERY: "+sQry);
		logger.info("OrderDAO::updateCreditCardDetails::SQL QUERY: "+p_oOrderItemForm.getOrderItemId());
		logger.info("OrderDAO::updateCreditCardDetails::SQL QUERY: "+p_oOrderItemForm.getCustZip());
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oOrderItemForm.getCustFirstName());
			cstmt.setString(3,p_oOrderItemForm.getCustLastName());
			cstmt.setString(4,p_oOrderItemForm.getCustAddress1());
			cstmt.setString(5,p_oOrderItemForm.getCustCity());
			cstmt.setString(6,p_oOrderItemForm.getCustState());	
			cstmt.setString(7,p_oOrderItemForm.getCustZip());
			cstmt.setString(8,p_oOrderItemForm.getCustEmail());
			cstmt.setString(9,p_oOrderItemForm.getCustPhone());
			cstmt.setString(10, p_oOrderItemForm.getCardType());
			cstmt.setString(11, p_oOrderItemForm.getCreditCardHolderName());
			cstmt.setString(12, p_oOrderItemForm.getCreditCardNo());
			cstmt.setString(13, p_oOrderItemForm.getCvv());
			cstmt.setString(14, p_oOrderItemForm.getExpDate());
			cstmt.setString(15, p_oOrderItemForm.getOrderItemId());
			cstmt.setString(16,p_oOrderItemForm.getCustAddress2());
			cstmt.setString(17,p_sLoginId);
			cstmt.setString(18,p_oOrderItemForm.getCreditCardLFD());
			
			cstmt.executeUpdate();
		}catch(Exception e) {
			logger.debug("OrderDAO::updateCreditCardDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("OrderDAO::updateCreditCardDetails::EXIT");

	}
	/*public static void addOrderTrackingDetails(OrderItemForm p_oOrderItemForm)
	throws Exception {
		logger.info("OrderDAO::addOrderTrackingDetails::ENTRY");

		String sQuery="{call usp_sbh_add_order_tracking_details(?,?)}";
		OrderTrackingDetailsVO orderTrackingDetailsVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_oOrderItemForm.getOrderItemId());
			cstmt.setString(2, p_oOrderItemForm.getOrderTrackingDetail());
			cstmt.executeUpdate();
		}catch(Exception e){
			logger.debug("OrderDAO::addOrderTrackingDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::addOrderTrackingDetails::EXIT");
	}*/
	public static void addOrderTrackingDetails(OrderItemForm p_oOrderItemForm, String p_sLoginId)
	throws Exception {
		logger.info("OrderDAO::addOrderTrackingDetails::ENTRY");

		String sQuery="{call usp_sbh_add_order_tracking_details(?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		logger.info("OrderDAO::addOrderTrackingDetails::SQL QUERY: "+sQuery);
		logger.info("OrderDAO::addOrderTrackingDetails::Order Item Id: "+p_oOrderItemForm.getOrderItemId());
		logger.info("OrderDAO::addOrderTrackingDetails::Order Tracking detail: "+p_oOrderItemForm.getOrderTrackingDetail());
		logger.info("OrderDAO::addOrderTrackingDetails::Shipment Status: "+p_oOrderItemForm.getShipmentStatus());
		logger.info("OrderDAO::addOrderTrackingDetails::Login Id: "+p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_oOrderItemForm.getOrderItemId());
			cstmt.setString(2, p_oOrderItemForm.getOrderTrackingDetail());
			cstmt.setString(3, p_oOrderItemForm.getShipmentStatus());
			cstmt.setString(4, p_sLoginId);
			
			cstmt.executeUpdate();
		}catch(Exception e){
			logger.debug("OrderDAO::addOrderTrackingDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::addOrderTrackingDetails::EXIT");
	}
	public static List<OrderTrackingDetailsVO> getOrderTrackingDetail(String p_sOrderItemId) 
	throws Exception {
		logger.info("OrderDAO::getOrderTrackingDetails::ENTRY");

		String sQuery="{call usp_sbh_get_order_tracking_details(?)}";
		List<OrderTrackingDetailsVO> orderTrackingList = null;
		OrderTrackingDetailsVO orderTrackingDetailsVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		logger.info("OrderDAO::getOrderTrackingDetail::SQL QUERY: "+sQuery);
		logger.info("OrderDAO::getOrderTrackingDetail::Order Item Id: "+p_sOrderItemId);
		
		try {
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_sOrderItemId);
			rs=cstmt.executeQuery();
			if(rs != null) {
				orderTrackingList = new ArrayList<OrderTrackingDetailsVO>();
				while(rs.next()) {
					orderTrackingDetailsVO = new OrderTrackingDetailsVO();
					orderTrackingDetailsVO.setCreate_dt(dateStampConversion(rs.getString("create_dt")));
					String sTrackingDetails = rs.getString("tracking_details");
					sTrackingDetails =sTrackingDetails ==null?"":sTrackingDetails.trim();
					orderTrackingDetailsVO.setTrackingDetails(sTrackingDetails);

					orderTrackingDetailsVO.setTrackingId(rs.getString("tracking_id"));
					orderTrackingDetailsVO.setOrderItemId(rs.getString("order_item_id"));
					
					String sDBData = rs.getString("shipment_status");
					sDBData = sDBData==null?"N":sDBData.trim();
					orderTrackingDetailsVO.setShipmentStatus(sDBData);
					
					orderTrackingList.add(orderTrackingDetailsVO);
				}
			}
		}catch(Exception e) {
			logger.info("OrderDAO::getOrderTrackingDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getOrderTrackingDetails::EXIT");
		return orderTrackingList;
	}

	public static void updateRejectReason(OrderItemForm p_oOrderItemForm, OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId) 
	throws Exception {
		logger.info("OrderDAO::updateRejectReason::ENTRY");

		String sQuery="{call usp_sbh_upd_txn_reject_reason(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		double sSbhPrice = 0.00;
		int sQuantity = 0;
		
		logger.info("OrderDAO::getOrderTrackingDetail::SQL QUERY: "+sQuery);
		logger.info("OrderDAO::getOrderTrackingDetail::Order Item Id: "+p_oOrderItemForm.getOrderItemId());
		logger.info("OrderDAO::getOrderTrackingDetail::Reject Reason: "+p_oOrderItemForm.getRejectReason());
		logger.info("OrderDAO::getOrderTrackingDetail::Login Id: "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_oOrderItemForm.getOrderItemId());
			cstmt.setString(2, p_oOrderItemForm.getRejectReason());
			cstmt.setString(3, p_oOrderItemDetailsVO.getCustId());
			
			if(p_oOrderItemDetailsVO.getQty() != null) {
				sQuantity = Integer.parseInt(p_oOrderItemDetailsVO.getQty());
			}
			if(p_oOrderItemDetailsVO.getSbhPrice() != null) {
				sSbhPrice = Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
			}
			sSbhPrice = sSbhPrice * sQuantity;
			
			cstmt.setString(4, sSbhPrice+"");
			cstmt.setString(5, p_oOrderItemDetailsVO.getCustAddress1());
			cstmt.setString(6, p_oOrderItemDetailsVO.getCustAddress2());
			cstmt.setString(7, p_oOrderItemDetailsVO.getCustCity());
			cstmt.setString(8, p_oOrderItemDetailsVO.getCustState());
			cstmt.setString(9, p_oOrderItemDetailsVO.getCustZip());
			cstmt.setString(10, p_oOrderItemDetailsVO.getCustCountry());
			cstmt.setString(11, p_oOrderItemDetailsVO.getCCNo());
			cstmt.setString(12, encryptInputData(p_oOrderItemDetailsVO.getExpDt(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(13, encryptInputData(p_oOrderItemDetailsVO.getCardFname(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(14, encryptInputData(p_oOrderItemDetailsVO.getCardType(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(15, p_sLoginId);
			
			cstmt.executeUpdate();
		}catch(Exception e){
			logger.debug("OrderDAO::updateRejectReason::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::updateRejectReason::EXIT");

	}
	public static String getOrderItemStatus(String p_oOrderItemId) throws Exception{
		logger.info("OrderDAO::getOrderItemStatus::ENTRY");
		
		String status=null;
		String sQuery="{call usp_sbh_get_order_items_status(?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		logger.info("OrderDAO::getOrderTrackingDetail::SQL QUERY: "+sQuery);
		logger.info("OrderDAO::getOrderTrackingDetail::Order Item Id: "+p_oOrderItemId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_oOrderItemId);
			rs = cstmt.executeQuery();
			while(rs.next()) {
				status=rs.getString("status");
			}
		}catch(Exception e){
			logger.debug("OrderDAO::getOrderItemStatus::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getOrderItemStatus::EXIT");

		return status;

	}
	public static String getPayTxnRefId(String p_sOrderItemId) throws Exception{	
		logger.info("OrderDAO::getPayTxnRefId::ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sPayTxnRefId =null;
		String sQry="{call usp_sbh_get_pay_txn_ref_id(?)}";
		logger.info("OrderDAO::getPayTxnRefId::SQL QUERY "+sQry);
		logger.info("OrderDAO::getPayTxnRefId::Product Key: "+p_sOrderItemId);			

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs.next()){				
				sPayTxnRefId = rs.getString("pay_txn_ref_id");
				sPayTxnRefId = sPayTxnRefId ==null?"":sPayTxnRefId.trim();
			}
			logger.info("OrderDAO::getPayTxnRefId::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::getPayTxnRefId::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return sPayTxnRefId;
	}
	public static List<CustPayTxnVO> getTxnFailureDetails(String p_sOrderItemId) throws Exception{	
		logger.info("OrderDAO::getTxnFailureDetails::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_txn_failure_details(?)}";
		logger.info("OrderDAO::getTxnFailureDetails::SQL QUERY "+sQry);
		logger.info("OrderDAO::getTxnFailureDetails::Product Key: "+p_sOrderItemId);
		CustPayTxnVO oCustPayTxnVO=null;
		List<CustPayTxnVO> alTxnFailureDetails = new ArrayList<CustPayTxnVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oCustPayTxnVO = new CustPayTxnVO();
				oCustPayTxnVO.setCustTransId(rs.getString("cust_trans_id"));
				oCustPayTxnVO.setOrderItemId(rs.getString("order_item_id"));
				oCustPayTxnVO.setOrderItemStatus(rs.getString("status"));
				oCustPayTxnVO.setPaymentTxnRefId(rs.getString("pay_txn_ref_id"));
				oCustPayTxnVO.setTransactionType(rs.getString("txn_type"));
				oCustPayTxnVO.setTransactionStatus(rs.getString("txn_status"));
				oCustPayTxnVO.setTransactionId(rs.getString("pay_txn_id"));
				oCustPayTxnVO.setKeyRefId(rs.getString("key_ref_id"));
				String sCardType = decryptInputData(rs.getString("card_type"),"CLIENT",oCustPayTxnVO.getKeyRefId());
				oCustPayTxnVO.setCardType(sCardType);
				
				String sCardHolder = decryptInputData(rs.getString("cardholder_name"),"CLIENT",oCustPayTxnVO.getKeyRefId());
				oCustPayTxnVO.setCreditCardHolderName(sCardHolder);
				
				oCustPayTxnVO.setCreditCardNo(rs.getString("cc_no"));
				oCustPayTxnVO.setCreditCardLFD(rs.getString("cc_no_lfd"));
				
				String sExpiryDate = decryptInputData(rs.getString("exp_dt"),"CLIENT",oCustPayTxnVO.getKeyRefId());
				oCustPayTxnVO.setExpiryDate(sExpiryDate);
				
				oCustPayTxnVO.setCustFirstName(rs.getString("cust_bill_fname"));
				oCustPayTxnVO.setCustLastName(rs.getString("cust_bill_lname"));
				oCustPayTxnVO.setCustAddress1(rs.getString("cust_bill_addr1"));
				oCustPayTxnVO.setCustAddress2(rs.getString("cust_bill_addr2"));
				oCustPayTxnVO.setCustCity(rs.getString("cust_bill_city"));
				oCustPayTxnVO.setCustState(rs.getString("cust_bill_state"));
				oCustPayTxnVO.setCustCountry(rs.getString("cust_bill_country"));
				oCustPayTxnVO.setCustZip(rs.getString("cust_bill_zip"));
				oCustPayTxnVO.setTransactionStatus(rs.getString("txn_status"));

				String sTxnType = rs.getString("txn_msg");
				sTxnType = sTxnType ==null?"":sTxnType.trim();
				oCustPayTxnVO.setTxnMsg(sTxnType);
				oCustPayTxnVO.setTransactionDate(rs.getString("create_dt"));				
				alTxnFailureDetails.add(oCustPayTxnVO);
			}
			logger.info("OrderDAO::getTxnFailureDetails::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::getTxnFailureDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alTxnFailureDetails;
	}
	public static void sendTrackingDetailsEmailToCustomer(OrderItemForm p_oOrderItemForm,String p_sLoginId,String p_sReqeustFrom) throws Exception {
		logger.info("OrderDAO::sendTrackingDetailsEmailToCustomer::ENTER");

		boolean bEmailSent=false;
		String sEmailTemplateId;
		String sToAddress=null;
		String sEmailSubject=null;
		String sEmailContent=null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sAdminAddr;
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO;
		String sBccAddr="",sTrackingDetails="",sEmailOptionalContent="",sItemDetails ="";
		List<OrderItemDetailsVO> alOrderItemDetailsList;
		List<OrderTrackingDetailsVO> lOrderTrackingList = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		sEmailTemplateId=oBundle.getString("email.order.tracking.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();	
		
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			alOrderItemDetailsList=getVendorOrderItemDetails(p_oOrderItemForm.getOrderItemId());
			lOrderTrackingList = getOrderTrackingDetail(p_oOrderItemForm.getOrderItemId());	

		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendTrackingDetailsEmailToCustomer::EXCEPTION");
			throw exception;
		}
		if(lOrderTrackingList!=null && lOrderTrackingList.size()>0){
			for(OrderTrackingDetailsVO oOrderTrackingDetailsVO: lOrderTrackingList ){						
				sTrackingDetails += "<tr><td align=left bgcolor=#FFFFFF>"+oOrderTrackingDetailsVO.getTrackingDetails()+"</td><td nowrap=nowrap bgcolor=#FFFFFF align=center>"+oOrderTrackingDetailsVO.getCreate_dt()+"</td></tr>";
			}						
		}
		hmTags.put("{{TrackingDetails}}",sTrackingDetails);	
		
		//hmTags.put("{{TrackingDetails}}",p_oOrderItemForm.getOrderTrackingDetail());
		for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetailsList) {
//			sCCAddress = oOrderItemDetailsVO.getOwnerEmail();
			sToAddress = oOrderItemDetailsVO.getCustEmail();

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());			
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	

			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			

			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailOptionalContent = oEmailVO.getEmailOptionalContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();

			if(sTrackingDetails.trim().length()>0){
				sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();					
				sEmailOptionalContent=replaceTagValues(hmTags,sEmailOptionalContent,null,null);
				sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();
				hmTags.put("{{TrackingDetailsTable}}",sEmailOptionalContent);	
				// System.out.println("sEmailOptionalContent:"+sEmailOptionalContent);

			}else{
				hmTags.put("{{TrackingDetailsTable}}","");	
			}
			
			
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}

		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

		sAdminAddr = oEmailVO.getAdminEmailAddress();
		sAdminAddr = sAdminAddr==null?"":sAdminAddr.trim();

		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddress));
		
		/*if(sCcAddr!=null && sCcAddr.trim().length()>0){
			sCcAddr =sCCAddress+","+sCcAddr;						
		}else{
			sCcAddr =sCCAddress;
		}
		oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));*/
		
		if(sBccAddr!=null && sBccAddr.trim().length()>0){
			sBccAddr =sAdminAddr+","+sBccAddr;						
		}else{
			sBccAddr =sAdminAddr;
		}
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sToAddress);
//		oEmailLogVo.setEmailCcAddr(sCCAddress);

		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendTrackingDetailsEmailToCustomer::EXIT");

	}
	
	public static void sendTrackingDetailsEmailToVendor(OrderItemForm p_oOrderItemForm,String p_sLoginId,String p_sReqeustFrom) throws Exception {
		logger.info("OrderDAO::sendTrackingDetailsEmailToVendor::ENTER");

		boolean bEmailSent=false;
		String sEmailTemplateId;
		String sToAddress=null;
		String sEmailSubject=null;
		String sEmailContent=null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sAdminAddr;
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO;
		String sBccAddr="",sSecondaryEmail ="",sTrackingDetails="",sEmailOptionalContent="",sItemDetails ="";
		List<OrderItemDetailsVO> alOrderItemDetailsList;
		List<OrderTrackingDetailsVO> lOrderTrackingList = null;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		sEmailTemplateId=oBundle.getString("email.order.tracking.details.vendor");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();	
		
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			alOrderItemDetailsList=getVendorOrderItemDetails(p_oOrderItemForm.getOrderItemId());
			lOrderTrackingList = getOrderTrackingDetail(p_oOrderItemForm.getOrderItemId());	

		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendTrackingDetailsEmailToVendor::EXCEPTION");
			throw exception;
		}
		if(lOrderTrackingList!=null && lOrderTrackingList.size()>0){
			for(OrderTrackingDetailsVO oOrderTrackingDetailsVO: lOrderTrackingList ){						
				sTrackingDetails += "<tr><td align=left bgcolor=#FFFFFF>"+oOrderTrackingDetailsVO.getTrackingDetails()+"</td><td nowrap=nowrap bgcolor=#FFFFFF align=center>"+oOrderTrackingDetailsVO.getCreate_dt()+"</td></tr>";
			}						
		}
		hmTags.put("{{TrackingDetails}}",sTrackingDetails);	
		
		//hmTags.put("{{TrackingDetails}}",p_oOrderItemForm.getOrderTrackingDetail());
		for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetailsList) {
			sToAddress = oOrderItemDetailsVO.getOwnerEmail();
			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddress.contains(sSecondaryEmail)){
					sToAddress = sToAddress + ","+sSecondaryEmail;
				}
			}
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			double dPoPct = Double.parseDouble(oOrderItemDetailsVO.getPoPct());
			
			dSbhPrice = (dSbhPrice * dPoPct) / 100;
			
			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());			
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{POPrice}}",numberFormat.format(dSbhPrice));
			hmTags.put("{{POPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	

			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{POPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			

			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailOptionalContent = oEmailVO.getEmailOptionalContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();

			if(sTrackingDetails.trim().length()>0){
				sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();					
				sEmailOptionalContent=replaceTagValues(hmTags,sEmailOptionalContent,null,null);
				sEmailOptionalContent=sEmailOptionalContent==null?"":sEmailOptionalContent.trim();
				hmTags.put("{{TrackingDetailsTable}}",sEmailOptionalContent);	
				// System.out.println("sEmailOptionalContent:"+sEmailOptionalContent);

			}else{
				hmTags.put("{{TrackingDetailsTable}}","");	
			}
			
			
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}

		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

		sAdminAddr = oEmailVO.getAdminEmailAddress();
		sAdminAddr = sAdminAddr==null?"":sAdminAddr.trim();

		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddress));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sToAddress);
		oEmailLogVo.setEmailBccAddr(sBccAddr);

		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendTrackingDetailsEmailToVendor::EXIT");

	}

	/*public static void sendTrackingDetailsEmail(OrderItemForm p_oOrderItemForm) throws Exception {
		logger.info("OrderDAO::sendTrackingDetailsEmail::ENTER");

		boolean bEmailSent=false;
		String sTrackingDetails;
		String sEmailTemplateId;
		String sCCAddress=null;
		String sToAddress=null;
		String sEmailSubject=null;
		String sEmailContent=null;
		String sFromEmailAddr;
		String sCustomerId=null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sAdminAddr;
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		int iStartIndex=0,iEndIndex = 0;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO;
		String sBccAddr="",sCcAddr="";
		List<OrderItemDetailsVO> alOrderItemDetailsList;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		sEmailTemplateId=oBundle.getString("email.order.tracking.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			alOrderItemDetailsList=getVendorOrderItemDetails(p_oOrderItemForm.getOrderItemId());

		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendTrackingDetailsEmail::EXCEPTION");
			throw exception;
		}
		hmTags.put("{{TrackingDetails}}",p_oOrderItemForm.getOrderTrackingDetail());
		for(OrderItemDetailsVO oOrderItemDetailsVO:alOrderItemDetailsList) {
			sCCAddress = oOrderItemDetailsVO.getOwnerEmail();
			sToAddress = oOrderItemDetailsVO.getCustEmail();
			sCustomerId = oOrderItemDetailsVO.getCustId();

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{AirName}}",convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerPhone}}",getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(),oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(),oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",getMaskCCNo(oOrderItemDetailsVO.getCreditCardNoLFD()));
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",convertToNameFormat(oOrderItemDetailsVO.getCardFname()));
			hmTags.put("{{CCLName}}",convertToNameFormat(oOrderItemDetailsVO.getCardLname()));

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	

			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");

			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}


			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}

		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

		sAdminAddr = oEmailVO.getAdminEmailAddress();
		sAdminAddr = sAdminAddr==null?"":sAdminAddr.trim();

		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddress));
		
		if(sCcAddr!=null && sCcAddr.trim().length()>0){
			sCcAddr =sCCAddress+","+sCcAddr;						
		}else{
			sCcAddr =sCCAddress;
		}
		oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
		
		if(sBccAddr!=null && sBccAddr.trim().length()>0){
			sBccAddr =sAdminAddr+","+sBccAddr;						
		}else{
			sBccAddr =sAdminAddr;
		}
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));
		oEmailLogVo.setEmailToAddr(sToAddress);
		oEmailLogVo.setEmailCcAddr(sCCAddress);

		*//** Insert Email Log Details *
		 * @throws Exception *//*
		EmailDAO.insertEmailLogDetails(oEmailLogVo);
		logger.info("OrderDAO::sendTrackingDetailsEmail::EXIT");

	}*/
	public static boolean isCreditCardDetailsChanged(PaymentInformationVO p_oPaymentInfoVO, OrderItemForm p_oOrderItemForm) throws Exception { 
		boolean isCreditCardChanged=false;

		if(p_oPaymentInfoVO != null && p_oOrderItemForm != null) {
			if(p_oOrderItemForm.getCardType() != null) {
				if(!p_oOrderItemForm.getCardType().equalsIgnoreCase(p_oPaymentInfoVO.getCardType())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCreditCardNo() != null) {
				if(!p_oOrderItemForm.getCreditCardNo().equalsIgnoreCase(decryptInputData(p_oPaymentInfoVO.getCreditCardNo(),"CLIENT",p_oPaymentInfoVO.getKeyRefId()))) {
					return true;
				}
			}
			if(p_oOrderItemForm.getExpMonth() != null) {
				if(!p_oOrderItemForm.getExpMonth().equalsIgnoreCase(p_oPaymentInfoVO.getExpMonth())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getExpYear() != null) {
				if(!p_oOrderItemForm.getExpYear().equalsIgnoreCase(p_oPaymentInfoVO.getExpYear())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCreditCardHolderName() != null) {
				if(!p_oOrderItemForm.getCreditCardHolderName().equalsIgnoreCase(p_oPaymentInfoVO.getCreditCardHolderName())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustAddress1() != null) {
				if(!p_oOrderItemForm.getCustAddress1().equalsIgnoreCase(p_oPaymentInfoVO.getCustAddress1())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustAddress2() != null) {
				if(!p_oOrderItemForm.getCustAddress2().equalsIgnoreCase(p_oPaymentInfoVO.getCustAddress2())) {
					return true;
				}
			}else if(p_oPaymentInfoVO.getCustAddress2() != null){
				if(!p_oPaymentInfoVO.getCustAddress2().equalsIgnoreCase(p_oOrderItemForm.getCustAddress2())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustCity() != null) {
				if(!p_oOrderItemForm.getCustCity().equalsIgnoreCase(p_oPaymentInfoVO.getCustCity())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustState() != null) {
				if(!p_oOrderItemForm.getCustState().equalsIgnoreCase(p_oPaymentInfoVO.getCustState())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustZip() != null) {
				if(!p_oOrderItemForm.getCustZip().equalsIgnoreCase(p_oPaymentInfoVO.getCustZip())) {
					return true;
				}
			}
		}

		return isCreditCardChanged;

	}
	
	public static String insertOrderItemAuditMessage(PaymentInformationVO p_oPaymentInfoVO, OrderItemForm p_oOrderItemForm) throws Exception { 
		String sModifiedFields = "";
		if(p_oPaymentInfoVO != null && p_oOrderItemForm != null) {
			if(p_oOrderItemForm.getCardType() != null) {
				if(!p_oOrderItemForm.getCardType().equalsIgnoreCase(p_oPaymentInfoVO.getCardType())) {
					sModifiedFields += "Card Type|";
				}
			}
			if(p_oOrderItemForm.getCreditCardNo() != null && p_oPaymentInfoVO.getCreditCardNo() != null && p_oPaymentInfoVO.getCreditCardNo().trim().length()>0) {
				if(!p_oOrderItemForm.getCreditCardNo().equalsIgnoreCase(decryptInputData(p_oPaymentInfoVO.getCreditCardNo(),"CLIENT", p_oPaymentInfoVO.getKeyRefId()))) {
					sModifiedFields += "Credit Card No|";
				}
			}else if(p_oOrderItemForm.getCreditCardNo() != null && (p_oPaymentInfoVO.getCreditCardNo() == null || p_oPaymentInfoVO.getCreditCardNo().trim().length()==0)) {
				sModifiedFields += "Credit Card No|";
			}
			if(p_oOrderItemForm.getExpMonth() != null) {
				if(!p_oOrderItemForm.getExpMonth().equalsIgnoreCase(p_oPaymentInfoVO.getExpMonth())) {
					sModifiedFields += "Exp Month|";
				}
			}
			if(p_oOrderItemForm.getExpYear() != null) {
				if(!p_oOrderItemForm.getExpYear().equalsIgnoreCase(p_oPaymentInfoVO.getExpYear())) {
					sModifiedFields += "Exp Year|";
				}
			}
			if(p_oOrderItemForm.getCreditCardHolderName() != null) {
				if(!p_oOrderItemForm.getCreditCardHolderName().equalsIgnoreCase(p_oPaymentInfoVO.getCreditCardHolderName())) {
					sModifiedFields += "Card Holder Name|";
				}
			}
			if(p_oOrderItemForm.getCustAddress1() != null) {
				if(!p_oOrderItemForm.getCustAddress1().equalsIgnoreCase(p_oPaymentInfoVO.getCustAddress1())) {
					sModifiedFields += "Billing Address1|";
				}
			}
			if(p_oOrderItemForm.getCustAddress2() != null) {
				if(!p_oOrderItemForm.getCustAddress2().equalsIgnoreCase(p_oPaymentInfoVO.getCustAddress2())) {
					sModifiedFields += "Billing Address2|";
				}
			}else if(p_oPaymentInfoVO.getCustAddress2() != null){
				if(!p_oPaymentInfoVO.getCustAddress2().equalsIgnoreCase(p_oOrderItemForm.getCustAddress2())) {
					sModifiedFields += "Billing Address2|";
				}
			}
			if(p_oOrderItemForm.getCustCity() != null) {
				if(!p_oOrderItemForm.getCustCity().equalsIgnoreCase(p_oPaymentInfoVO.getCustCity())) {
					sModifiedFields += "Billing City|";
				}
			}
			if(p_oOrderItemForm.getCustState() != null) {
				if(!p_oOrderItemForm.getCustState().equalsIgnoreCase(p_oPaymentInfoVO.getCustState())) {
					sModifiedFields += "Billing State|";
				}
			}
			if(p_oOrderItemForm.getCustZip() != null) {
				if(!p_oOrderItemForm.getCustZip().equalsIgnoreCase(p_oPaymentInfoVO.getCustZip())) {
					sModifiedFields += "Billing Zip|";
				}
			}
			if(p_oOrderItemForm.getCustPhone() != null) {
				String phoneFormat = getPhoneFormat(p_oOrderItemForm.getCustPhone());
				if(!phoneFormat.equals(p_oPaymentInfoVO.getCustPhone())) {
					sModifiedFields += "Billing Phone|";
				}
			}
			if(p_oOrderItemForm.getCustEmail() != null) {
				if(!p_oOrderItemForm.getCustEmail().equalsIgnoreCase(p_oPaymentInfoVO.getCustEmail())) {
					sModifiedFields += "Billing Email|";
				}
			}
			if(p_oOrderItemForm.getCvv()!= null) {
				sModifiedFields += "CVV|";
			}
			
		}

		return sModifiedFields;
	}

	public static boolean isOrderItemDetailChanged(OrderItemDetailsVO p_oOrderItemDetailsVO, OrderItemForm p_oOrderItemForm) 
		throws Exception {
		boolean isOrderItemDetailChanged = false;
	
		if(p_oOrderItemDetailsVO != null && p_oOrderItemForm != null) {
	
			if(p_oOrderItemForm.getQty() != null) {
				if(!p_oOrderItemForm.getQty().equalsIgnoreCase(p_oOrderItemDetailsVO.getQty())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getTravelDate() != null) {
				if(!p_oOrderItemForm.getTravelDate().equalsIgnoreCase(p_oOrderItemDetailsVO.getTravelDate())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipAddress1() != null) {
				if(!p_oOrderItemForm.getCustShipAddress1().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipAddress1())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipAddress2() != null) {
				if(!p_oOrderItemForm.getCustShipAddress2().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipAddress2())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipCity() != null) {
				if(!p_oOrderItemForm.getCustShipCity().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipCity())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipState() != null) {
				if(!p_oOrderItemForm.getCustShipState().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipState())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipCountry() != null) {
				if(!p_oOrderItemForm.getCustShipCountry().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipCountry())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipEmail() != null) {
				if(!p_oOrderItemForm.getCustShipEmail().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipEmail())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipFirstName() != null) {
				if(!p_oOrderItemForm.getCustShipFirstName().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipFirstName())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipLastName() != null) {
				if(!p_oOrderItemForm.getCustShipLastName().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipLastName())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipPhone() != null) {
				String phoneFormat = getPhoneFormat(p_oOrderItemForm.getCustShipPhone());
				if(!phoneFormat.equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipPhone())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCustShipZip() != null) {
				if(!p_oOrderItemForm.getCustShipZip().equalsIgnoreCase(p_oOrderItemDetailsVO.getCustShipZip())) {
					return true;
				}
			}
			if(p_oOrderItemForm.getCvv() != null) {
				if(!p_oOrderItemForm.getCvv().equalsIgnoreCase(p_oOrderItemDetailsVO.getCvv())) {
					return true;
				}
			}
		}
		return isOrderItemDetailChanged;
	}
	
	public static String modifiedOrderDetails(OrderItemDetailsVO p_oOrderItemDetailsVO, OrderItemForm p_oOrderItemForm) 
		throws Exception {
		String sModifiedFields = "";
	
		if(p_oOrderItemDetailsVO != null && p_oOrderItemForm != null) {
	
			if(p_oOrderItemForm.getQty() != null) {
				if(!p_oOrderItemForm.getQty().equalsIgnoreCase(p_oOrderItemDetailsVO.getQty())) {
					sModifiedFields += "Qty|";
				}
			}
			if(p_oOrderItemForm.getTravelDate() != null) {
				if(!p_oOrderItemForm.getTravelDate().equalsIgnoreCase(p_oOrderItemDetailsVO.getTravelDate())) {
					sModifiedFields += "Travel Date|";
				}
			}
			if(p_oOrderItemForm.getCustShipAddress1() != null) {
				if(!p_oOrderItemForm.getCustShipAddress1().equals(p_oOrderItemDetailsVO.getCustShipAddress1())) {
					sModifiedFields += "Shipping Address1|";
				}
			}
			if(p_oOrderItemForm.getCustShipAddress2() != null && p_oOrderItemForm.getCustShipAddress2().trim().length()>0) {
				if(!p_oOrderItemForm.getCustShipAddress2().equals(p_oOrderItemDetailsVO.getCustShipAddress2())) {
					sModifiedFields += "Shipping Address2|";
				}
			}else if(p_oOrderItemDetailsVO.getCustShipAddress2() != null && p_oOrderItemDetailsVO.getCustShipAddress2().trim().length()>0) {
				if(!p_oOrderItemDetailsVO.getCustShipAddress2().equals(p_oOrderItemForm.getCustShipAddress2())) {
					sModifiedFields += "Shipping Address2|";
				}
			}
			if(p_oOrderItemForm.getCustShipCity() != null) {
				if(!p_oOrderItemForm.getCustShipCity().equals(p_oOrderItemDetailsVO.getCustShipCity())) {
					sModifiedFields += "Shipping City|";
				}
			}
			if(p_oOrderItemForm.getCustShipState() != null) {
				if(!p_oOrderItemForm.getCustShipState().equals(p_oOrderItemDetailsVO.getCustShipState())) {
					sModifiedFields += "Shipping State|";
				}
			}
			if(p_oOrderItemForm.getCustShipCountry() != null) {
				if(!p_oOrderItemForm.getCustShipCountry().equals(p_oOrderItemDetailsVO.getCustShipCountry())) {
					sModifiedFields += "Shipping Country|";
				}
			}
			if(p_oOrderItemForm.getCustShipEmail() != null) {
				if(!p_oOrderItemForm.getCustShipEmail().equals(p_oOrderItemDetailsVO.getCustShipEmail())) {
					sModifiedFields += "Shipping Email|";
				}
			}
			if(p_oOrderItemForm.getCustShipFirstName() != null) {
				if(!p_oOrderItemForm.getCustShipFirstName().equals(p_oOrderItemDetailsVO.getCustShipFirstName())) {
					sModifiedFields += "Shipping First Name|";
				}
			}
			if(p_oOrderItemForm.getCustShipLastName() != null) {
				if(!p_oOrderItemForm.getCustShipLastName().equals(p_oOrderItemDetailsVO.getCustShipLastName())) {
					sModifiedFields += "Shipping Last Name|";
				}
			}
			if(p_oOrderItemForm.getCustShipPhone() != null) {
				String phoneFormat = getPhoneFormat(p_oOrderItemForm.getCustShipPhone());
				if(!phoneFormat.equals(p_oOrderItemDetailsVO.getCustShipPhone())) {
					sModifiedFields += "Shipping Phone|";
				}
			}
			if(p_oOrderItemForm.getCustShipZip() != null) {
				if(!p_oOrderItemForm.getCustShipZip().equals(p_oOrderItemDetailsVO.getCustShipZip())) {
					sModifiedFields += "Shipping Zip|";
				}
			}
			if(p_oOrderItemForm.getCvv() != null) {
				if(!p_oOrderItemForm.getCvv().equals(p_oOrderItemDetailsVO.getCvv())) {
					sModifiedFields += "CVV|";
				}
			}
			
			//if(p_oOrderItemDetailsVO.getCCNo() == null || "".equalsIgnoreCase(p_oOrderItemDetailsVO.getCCNo())) {
				if(p_oOrderItemForm.getCreditCardNo()!=null &&p_oOrderItemForm.getCreditCardNo().trim().length()>0 
						&& p_oOrderItemForm.getCreditCardNo().trim().length()<20)	
					sModifiedFields += "Credit Card No|";
				
			//}
		}
		return sModifiedFields;
	}



	public static boolean updateCvvStatus(String p_sCustId,String p_sStatus,String p_sLoginId)
	throws Exception {
		logger.info("OrderDAO::updateCvvStatus::ENTRY");

		String sQuery="{call usp_sbh_upd_cvv_status(?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		boolean bIsUpdate = false;
		int iRowCount = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.setString(1, p_sCustId);
			cstmt.setString(2, p_sStatus);
			cstmt.setString(3, p_sLoginId);
			cstmt.executeUpdate();
			iRowCount = cstmt.getInt(4);

			if(iRowCount>0)
				bIsUpdate = true;
			else
				bIsUpdate = false;

		}catch(Exception e){
			logger.debug("OrderDAO::updateCvvStatus::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("OrderDAO::updateCvvStatus::EXIT");
		return bIsUpdate;
	}
	
	public static boolean addAirCommissionNoticePdf(OrderItemDetailsVO p_oOrderItemDetailsVO,double p_dAirCommission,byte[] p_bBlobObj,String p_sStatus, String p_sLoginId) throws Exception{	
		logger.info("OrderDAO::addAirCommissionNoticePdf::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		
		boolean bIsUpdate = false;
		String sQry="{call usp_sbh_add_air_comm_details(?,?,?,?,?,?)}";		 
		logger.info("OrderDAO::addAirCommissionNoticePdf::SQL QUERY "+sQry);				
		logger.info("OrderDAO::addAirCommissionNoticePdf::Order Item Id "+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("OrderDAO::addAirCommissionNoticePdf::Qty "+p_oOrderItemDetailsVO.getQty());	
		logger.info("OrderDAO::addAirCommissionNoticePdf::Air Comm Pct "+p_oOrderItemDetailsVO.getAirCommPct());
		try{
			con = DBConnection.getSQL2005Connection();

			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_oOrderItemDetailsVO.getOrderItemId());			
			cstmt.setString(2,p_oOrderItemDetailsVO.getQty());
			cstmt.setString(3,p_oOrderItemDetailsVO.getAirCommPct());
			cstmt.setBytes(4, p_bBlobObj);
			cstmt.setString(5, p_sStatus);
			cstmt.setString(6, p_sLoginId);
			cstmt.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::addAirCommissionNoticePdf::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("OrderDAO::addAirCommissionNoticePdf::EXIT");
		return bIsUpdate;
	}
	
	public static boolean sendAirlineCommissionEMail(OrderItemDetailsVO oOrderItemDetailsVO,String sFilePath, String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendAirlineCommissionEMail:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		byte[] baPdf = null;
		File oPdfFile = null;
		File oHtmlFile = null;
		String sBccAddr="",sCcAddr="",sSecondaryEmail ="";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.airline.commission");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			
			hmTags.put("{{AirName}}", convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}", oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{AirContactName}}", convertToNameFormat(oOrderItemDetailsVO.getAirContactName()));
			hmTags.put("{{AirAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getAirAddr1(),oOrderItemDetailsVO.getAirAddr2())));
			hmTags.put("{{AirCity}}", convertToNameFormat(oOrderItemDetailsVO.getAirCity()));
			hmTags.put("{{AirState}}", oOrderItemDetailsVO.getAirState());
			hmTags.put("{{AirZip}}",oOrderItemDetailsVO.getAirZip());
			hmTags.put("{{AirCountry}}","USA");
			hmTags.put("{{AirPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getAirPhone()));
			hmTags.put("{{AirUserId}}", oOrderItemDetailsVO.getAirUserId());

			hmTags.put("{{CustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{OrderConfNo}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));			
			
			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			
			double dAirCommission = (Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())*(Double.parseDouble(oOrderItemDetailsVO.getAirCommPct())/100));
			
					
			hmTags.put("{{AirCommPct}}",oOrderItemDetailsVO.getAirCommPct());
			hmTags.put("{{AirlineCommission}}",numberFormat.format(dAirCommission));			
			hmTags.put("{{TotalCommission}}",numberFormat.format(dQty*dAirCommission));	
	
			
			sToAddr=oOrderItemDetailsVO.getAirEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			//To Generate PO PDF
			String sHTMLFileName = sFilePath+"CommissionNotice_"+oOrderItemDetailsVO.getOrderItemId()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+"CommissionNotice_"+oOrderItemDetailsVO.getOrderItemId()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			oPdfFile = new File(pdfFileName);
			oHtmlFile = new File(sHTMLFileName);


			addAirCommissionNoticePdf(oOrderItemDetailsVO,dAirCommission,baPdf,"A",p_sLoginId);


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("airline");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendAirlineCommissionEMail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendAirlineCommissionEMail:Exception "+e.getMessage());
			throw e;
		}	finally{
			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
		}

		return bSentEmail;
	}
	
	
	public static boolean sendCanceledAirlineCommissionEMail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sCanceledReason,String sFilePath,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendCanceledAirlineCommissionEMail:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		byte[] baPdf = null;
		File oPdfFile = null;
		File oHtmlFile = null;
		String sBccAddr="",sCcAddr="", sSecondaryEmail = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.canceled.airline.commission");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			hmTags.put("{{ReasonForCancel}}", p_sCanceledReason);
			hmTags.put("{{AirName}}", convertToNameFormat(oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}", oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{AirContactName}}", convertToNameFormat(oOrderItemDetailsVO.getAirContactName()));
			hmTags.put("{{AirAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getAirAddr1(),oOrderItemDetailsVO.getAirAddr2())));
			hmTags.put("{{AirCity}}", convertToNameFormat(oOrderItemDetailsVO.getAirCity()));
			hmTags.put("{{AirState}}", oOrderItemDetailsVO.getAirState());
			hmTags.put("{{AirZip}}",oOrderItemDetailsVO.getAirZip());
			hmTags.put("{{AirCountry}}","USA");
			hmTags.put("{{AirPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getAirPhone()));
			hmTags.put("{{AirUserId}}", oOrderItemDetailsVO.getAirUserId());

			hmTags.put("{{CustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{OrderConfNo}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));			
			
			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			
			double dAirCommission = (Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())*(Double.parseDouble(oOrderItemDetailsVO.getAirCommPct())/100));
			
					
			hmTags.put("{{AirCommPct}}",oOrderItemDetailsVO.getAirCommPct());
			hmTags.put("{{AirlineCommission}}",numberFormat.format(dAirCommission));			
			hmTags.put("{{TotalCommission}}",numberFormat.format(dQty*dAirCommission));	
	
			
			sToAddr=oOrderItemDetailsVO.getAirEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			


			//To Generate Canceled Airline Commission PDF
			String sHTMLFileName = sFilePath+"CanceledCommissionNotice_"+oOrderItemDetailsVO.getOrderItemId()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+"CanceledCommissionNotice_"+oOrderItemDetailsVO.getOrderItemId()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			oPdfFile = new File(pdfFileName);
			oHtmlFile = new File(sHTMLFileName);


			addAirCommissionNoticePdf(oOrderItemDetailsVO,dAirCommission,baPdf,"C",p_sLoginId);


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			//oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("airline");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");

			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendCanceledAirlineCommissionEMail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendCanceledAirlineCommissionEMail:Exception "+e.getMessage());
			throw e;
		}	finally{
			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
		}

		return bSentEmail;
	}
	

	public static boolean createAndSendVendorPO(OrderItemDetailsVO oOrderItemDetailsVO,String sFilePath,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::createAndSendVendorPO::ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sPurchaseContactName = null;
		String sEmailTemplateId=null,sEmailContent=null,sEmailPOPdfContent = null,sEmailPDFTemplateId = null;
		EmailVO oEmailVO =null;
		EmailVO oEmailPOPdfVO =null;
		byte[] baPdf = null;
		double dPOAmt = 0.00;
		File oPdfFile = null;
		File oHtmlFile = null;
		String sBccAddr="",sCcAddr="",sSecondaryEmail = "";
		String sScissorPath = null;
		String sCutMarkPath = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			if(oOrderItemDetailsVO.getPoPct() != null) {
				dPOAmt = Double.parseDouble(oOrderItemDetailsVO.getPoPct());
			}
			dPOAmt = (Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())*(dPOAmt/100));

			addPartnerPODetails(oOrderItemDetailsVO, p_sLoginId);

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.vendor.po");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			sEmailPDFTemplateId=oBundle.getString("email.vendor.po.pdf");
			sEmailPDFTemplateId=sEmailPDFTemplateId==null?"":sEmailPDFTemplateId.trim();		

			sPurchaseContactName = oBundle.getString("skybuyContactName");
			sPurchaseContactName=sPurchaseContactName==null?"":sPurchaseContactName.trim();	

			sScissorPath = System.getProperty("ScissorImagePath").trim();
			sScissorPath=sScissorPath==null?"":sScissorPath.trim();	
			
			sCutMarkPath  = System.getProperty("CutmarkImagePath").trim();;
			sCutMarkPath=sCutMarkPath==null?"":sCutMarkPath.trim();	
			
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			oEmailPOPdfVO = EmailDAO.getEmailContentDetails(sEmailPDFTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
			hmTags.put("{{Scissor}}",sScissorPath);	
			hmTags.put("{{CutMark}}",sCutMarkPath);	
			


			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerType()));
			hmTags.put("{{OwnerId}}",oOrderItemDetailsVO.getOwnerLoginId());
			hmTags.put("{{PoNo}}",oOrderItemDetailsVO.getPoNo());
			hmTags.put("{{PurchaseDeptContactName}}",sPurchaseContactName);
			if(VENDOR.equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getVendorName());
			}else{
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getAirName());
			}

			hmTags.put("{{OwnerContactName}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getOwnerAddr1(),oOrderItemDetailsVO.getOwnerAddr2())));
			hmTags.put("{{OwnerCity}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerCity()));
			hmTags.put("{{OwnerState}}", oOrderItemDetailsVO.getOwnerState());
			hmTags.put("{{OwnerZip}}",oOrderItemDetailsVO.getOwnerZip());
			hmTags.put("{{OwnerCountry}}",oOrderItemDetailsVO.getOwnerCountry());
			hmTags.put("{{OwnerPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getOwnerPhone()));

			hmTags.put("{{OrderConfNo}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{PoDate}}",oOrderItemDetailsVO.getPoDate());
			hmTags.put("{{ShippingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustShipPhone()));
			hmTags.put("{{ShippingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());

			hmTags.put("{{BillingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(),oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());

			
			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			//double dSbhPrice=dPOAmt;

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{PoPrice}}",numberFormat.format(dPOAmt));
			hmTags.put("{{PoTotalAmount}}",numberFormat.format(dQty*dPOAmt));	
			hmTags.put("{{VendorName}}",convertToNameFormat(oOrderItemDetailsVO.getVendorName()));
			//hmTags.put("{{SizeAndColor}}",oOrderItemDetailsVO.getSize()+" "+oOrderItemDetailsVO.getColor());
			hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());
			hmTags.put("{{ShortDesc}}",oOrderItemDetailsVO.getShortDesc());
			hmTags.put("{{SpecialInst}}",oOrderItemDetailsVO.getSpecialInst());
			hmTags.put("{{PersonalMsg}}",oOrderItemDetailsVO.getPersonalInfo());
			 
			
			
			
			
			/*dPOPriceTotal = dPOPriceTotal+dQty*dPOAmt;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dPOPriceTotal));*/

			//hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			/*if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");*/


			sToAddr=oOrderItemDetailsVO.getOwnerEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			if(oEmailPOPdfVO!=null){
				sEmailPOPdfContent = oEmailPOPdfVO.getEmailContent();
			}

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			/*if(isDifferentBillingAndShipping(oOrderItemDetailsVO)true)
			iStartIndex = sEmailContent.indexOf("<div id=CustomerBillingAddressStart></div>");
			iEndIndex = sEmailContent.indexOf("<div id=CustomerBillingAddressEnd></div>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
				String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
				sEmailContent = sSubStringFirst+sSubStringLast;
			}
			
			
			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}*/

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			sEmailPOPdfContent=sEmailPOPdfContent==null?"":sEmailPOPdfContent.trim();
			/*if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailPOPdfContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailPOPdfContent.substring(iEndIndex,sEmailPOPdfContent.length());
					sEmailPOPdfContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailPOPdfContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailPOPdfContent.substring(iEndIndex,sEmailPOPdfContent.length());
					sEmailPOPdfContent = sSubStringFirst+sSubStringLast;
				}	
			}*/
			sEmailPOPdfContent=replaceTagValues(hmTags,sEmailPOPdfContent,null,null);
			sEmailPOPdfContent=sEmailPOPdfContent==null?"":sEmailPOPdfContent.trim();


			//To Generate PO PDF
			String sHTMLFileName = sFilePath+oOrderItemDetailsVO.getPoNo()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailPOPdfContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			Dimension format = PD4Constants.A4;
			//pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageSize(format);
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1050);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+oOrderItemDetailsVO.getPoNo()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			oPdfFile = new File(pdfFileName);
			oHtmlFile = new File(sHTMLFileName);

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			addOrderItemPOPdf(oOrderItemDetailsVO.getOrderItemId(),baPdf,"A",oEmailLogVo.getEmailStatus(),p_sLoginId);

			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::createAndSendVendorPO::EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.info("OrderDAO::createAndSendVendorPO::Exception "+e.getMessage());
			throw e;
		}	finally{
			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
		}

		return bSentEmail;
	}
	
	
	public static boolean createAndSendAirlinePO(OrderItemDetailsVO oOrderItemDetailsVO,String sFilePath, String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:createAndSendAirlinePO:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sPurchaseContactName = null;
		String sEmailTemplateId=null,sEmailContent=null,sEmailPOPdfContent = null,sEmailPDFTemplateId = null;
		EmailVO oEmailVO =null;
		EmailVO oEmailPOPdfVO =null;
		byte[] baPdf = null;
		double dPOAmt = 0.00;
		File oPdfFile = null;
		File oHtmlFile = null;
		int iStartIndex=0,iEndIndex = 0;
		String sBccAddr="",sCcAddr="";
		String sScissorPath = null;
		String sCutMarkPath = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			if(oOrderItemDetailsVO.getPoPct() != null) {
				dPOAmt = Double.parseDouble(oOrderItemDetailsVO.getPoPct());
			}
			dPOAmt = (Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())*(dPOAmt/100));

			addPartnerPODetails(oOrderItemDetailsVO, p_sLoginId);

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.airline.po");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			
 
			sEmailPDFTemplateId=oBundle.getString("email.airline.po.pdf");
			sEmailPDFTemplateId=sEmailPDFTemplateId==null?"":sEmailPDFTemplateId.trim();		

			sPurchaseContactName = oBundle.getString("skybuyContactName");
			sPurchaseContactName=sPurchaseContactName==null?"":sPurchaseContactName.trim();	

			sScissorPath = System.getProperty("ScissorImagePath").trim();
			sScissorPath=sScissorPath==null?"":sScissorPath.trim();	
			
			sCutMarkPath  = System.getProperty("CutmarkImagePath").trim();;
			sCutMarkPath=sCutMarkPath==null?"":sCutMarkPath.trim();	
			
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			oEmailPOPdfVO = EmailDAO.getEmailContentDetails(sEmailPDFTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
			hmTags.put("{{Scissor}}",sScissorPath);	
			hmTags.put("{{CutMark}}",sCutMarkPath);	
			


			//hmTags.put("{{Reason}}",p_sReason);
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				hmTags.put("{{OwnerType}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerType()));
			}else{
				hmTags.put("{{OwnerType}}","Air Charter");
			}
			hmTags.put("{{OwnerId}}",oOrderItemDetailsVO.getOwnerLoginId());
			//hmTags.put("{{OwnerContactName}}","Marvin Wind");
			hmTags.put("{{PoNo}}",oOrderItemDetailsVO.getPoNo());
			hmTags.put("{{PurchaseDeptContactName}}",sPurchaseContactName);
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getVendorName());
			}else{
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getAirName());
			}

			hmTags.put("{{OwnerContactName}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getOwnerAddr1(),oOrderItemDetailsVO.getOwnerAddr2())));
			hmTags.put("{{OwnerCity}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerCity()));
			hmTags.put("{{OwnerState}}", oOrderItemDetailsVO.getOwnerState());
			hmTags.put("{{OwnerZip}}",oOrderItemDetailsVO.getOwnerZip());
			hmTags.put("{{OwnerCountry}}",oOrderItemDetailsVO.getOwnerCountry());
			hmTags.put("{{OwnerPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getOwnerPhone()));

			hmTags.put("{{OrderConfNo}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{PoDate}}",oOrderItemDetailsVO.getPoDate());
			hmTags.put("{{ShippingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustShipPhone()));
			hmTags.put("{{ShippingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","USA");
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());

			hmTags.put("{{BillingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(),oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","USA");
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());

			
			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			//double dSbhPrice=dPOAmt;

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{PoPrice}}",numberFormat.format(dPOAmt));
			hmTags.put("{{PoTotalAmount}}",numberFormat.format(dQty*dPOAmt));	
			hmTags.put("{{VendorName}}",convertToNameFormat(oOrderItemDetailsVO.getVendorName()));
			hmTags.put("{{SizeAndColor}}",oOrderItemDetailsVO.getSize()+" "+oOrderItemDetailsVO.getColor());
			hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());
			hmTags.put("{{ShortDesc}}",oOrderItemDetailsVO.getShortDesc());
			hmTags.put("{{SpecialInst}}",oOrderItemDetailsVO.getSpecialInst());
			hmTags.put("{{PersonalMsg}}",oOrderItemDetailsVO.getPersonalInfo());
			 
			
			
			
			
			/*dPOPriceTotal = dPOPriceTotal+dQty*dPOAmt;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dPOPriceTotal));*/

			//hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			/*if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");*/


			sToAddr=oOrderItemDetailsVO.getOwnerEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			String sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			if(oEmailPOPdfVO!=null){
				sEmailPOPdfContent = oEmailPOPdfVO.getEmailContent();
			}

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			if(/*isDifferentBillingAndShipping(oOrderItemDetailsVO)*/true)
			iStartIndex = sEmailContent.indexOf("<div id=CustomerBillingAddressStart></div>");
			iEndIndex = sEmailContent.indexOf("<div id=CustomerBillingAddressEnd></div>");
			if(iStartIndex > 0 && iEndIndex > 0) {
				String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
				String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
				sEmailContent = sSubStringFirst+sSubStringLast;
			}
			
			
			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			sEmailPOPdfContent=sEmailPOPdfContent==null?"":sEmailPOPdfContent.trim();
			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailPOPdfContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailPOPdfContent.substring(iEndIndex,sEmailPOPdfContent.length());
					sEmailPOPdfContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailPOPdfContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailPOPdfContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailPOPdfContent.substring(iEndIndex,sEmailPOPdfContent.length());
					sEmailPOPdfContent = sSubStringFirst+sSubStringLast;
				}	
			}
			sEmailPOPdfContent=replaceTagValues(hmTags,sEmailPOPdfContent,null,null);
			sEmailPOPdfContent=sEmailPOPdfContent==null?"":sEmailPOPdfContent.trim();


			//To Generate PO PDF
			String sHTMLFileName = sFilePath+oOrderItemDetailsVO.getPoNo()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailPOPdfContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			Dimension format = PD4Constants.A4;
			//pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageSize(format);
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1050);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+oOrderItemDetailsVO.getPoNo()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			oPdfFile = new File(pdfFileName);
			oHtmlFile = new File(sHTMLFileName);



			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");

			addOrderItemPOPdf(oOrderItemDetailsVO.getOrderItemId(),baPdf,"A",oEmailLogVo.getEmailStatus(),p_sLoginId);

			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::createAndSendAirlinePO:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:createAndSendAirlinePO:Exception "+e.getMessage());
			throw e;
		}	finally{
			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
		}

		return bSentEmail;
	}




	public static boolean addPartnerPODetails(OrderItemDetailsVO oOrderItemDetailsVO, String p_sLoginId)
	throws Exception {
		logger.info("OrderDAO::addPartnerPODetails::ENTRY");

		String sQuery="{call usp_sbh_add_partner_po_details(?,?,?,?,?,?,?,?)}";
		logger.info("OrderDAO::addPartnerPODetails::Query   "+sQuery);
		logger.info("OrderDAO::addPartnerPODetails::Order Item Id   "+oOrderItemDetailsVO.getOrderItemId());
		logger.info("OrderDAO::addPartnerPODetails::Quantity   "+oOrderItemDetailsVO.getQty());
		logger.info("OrderDAO::addPartnerPODetails::PO Number   "+oOrderItemDetailsVO.getPoNo());
		logger.info("OrderDAO::addPartnerPODetails::Login ID   "+p_sLoginId);
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		boolean bIsUpdate = false;
		int iRowCount = 0;
		String sPONo = null;
		Date dPODate = null;
		SimpleDateFormat sdfOutput = new SimpleDateFormat  ("MM/dd/yyyy") ; 
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			
			cstmt.setString(1, oOrderItemDetailsVO.getOrderItemId());
			cstmt.setString(2, oOrderItemDetailsVO.getQty());
			cstmt.setString(3, oOrderItemDetailsVO.getPoPct());
			cstmt.setString(4, oOrderItemDetailsVO.getPoNo());
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.registerOutParameter(6, Types.DATE);
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.setString(8, p_sLoginId);
			
			cstmt.executeUpdate();
			
			sPONo = cstmt.getString(5);
			dPODate = cstmt.getDate(6);
			iRowCount = cstmt.getInt(7);
			
			oOrderItemDetailsVO.setPoNo(sPONo);
			if(dPODate!=null)
				oOrderItemDetailsVO.setPoDate(sdfOutput.format(dPODate));
			else
				oOrderItemDetailsVO.setPoDate("");

			if(iRowCount>0)
				bIsUpdate = true;
			else
				bIsUpdate = false;

		}catch(Exception e){
			logger.debug("OrderDAO::addPartnerPODetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("OrderDAO::addPartnerPODetails::EXIT");
		return bIsUpdate;
	}

	public static void addCustomerInvoiceDetails(OrderItemDetailsVO oOrderItemDetailsVO, String p_sLoginId)
	throws Exception {
		logger.info("OrderDAO::addCustomerInvoiceDetails::ENTRY");

		String sQuery="{call usp_sbh_add_cust_invoice_details(?,?,?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sInvoiceNo = null;

		try{

			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, oOrderItemDetailsVO.getCustTransId());
			cstmt.setString(2, oOrderItemDetailsVO.getOrderItemId());
			cstmt.setString(3, oOrderItemDetailsVO.getSbhPrice());
			cstmt.setString(4, oOrderItemDetailsVO.getQty());
			cstmt.setString(5, oOrderItemDetailsVO.getInvoiceNo());
			cstmt.registerOutParameter(6, Types.VARCHAR);
			cstmt.setString(7, p_sLoginId);
			cstmt.executeUpdate();		
			sInvoiceNo = cstmt.getString(6);		

			oOrderItemDetailsVO.setInvoiceNo(sInvoiceNo);			
		}catch(Exception e){
			logger.debug("OrderDAO::addCustomerInvoiceDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("OrderDAO::addCustomerInvoiceDetails::EXIT");

	}

	/*public static boolean addOrderItemPOPdf(String p_sOrderItemId,byte[] p_bBlobObj) throws Exception{	
		logger.info("addOrderItemPOPdf:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		
		int iRowCount = 0;
		boolean bIsUpdate = false;
		String sQry="{call usp_sbh_add_order_item_po_pdf(?,?,?)}";		 
		logger.debug("addOrderItemPOPdf:SQL QUERY "+sQry);				
		logger.debug("addOrderItemPOPdf:Order Item Id "+p_sOrderItemId);				
		try{
			con = DBConnection.getSQL2005Connection();

			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setBytes(2, p_bBlobObj);
			cstmt.registerOutParameter(3, Types.INTEGER);
			cstmt.executeUpdate();
			iRowCount = cstmt.getInt(3);
			if(iRowCount>0)
				bIsUpdate = true;
			else
				bIsUpdate = false;
			logger.info("addOrderItemPOPdf:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("addOrderItemPOPdf:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		return bIsUpdate;
	}*/

	public static boolean addOrderItemPOPdf(String p_sOrderItemId,byte[] p_bBlobObj,String p_sStatus,String p_sIsEmailSent, String p_sLoginId) throws Exception{	
		logger.info("OrderDAO::addOrderItemPOPdf::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null; 
		int iRowCount = 0;
		boolean bIsUpdate = false;
		String sQry="{call usp_sbh_add_order_item_po_pdf(?,?,?,?,?,?)}";		 
		logger.info("OrderDAO::addOrderItemPOPdf::SQL QUERY "+sQry);				
		logger.info("OrderDAO::addOrderItemPOPdf::Order Item Id "+p_sOrderItemId);				
		logger.info("OrderDAO::addOrderItemPOPdf::Status "+p_sStatus);
		logger.info("OrderDAO::addOrderItemPOPdf::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();

			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setBytes(2, p_bBlobObj);
			cstmt.setString(3, p_sStatus);
			cstmt.setString(4, p_sIsEmailSent);
			cstmt.registerOutParameter(5, Types.INTEGER);
			cstmt.setString(6, p_sLoginId);
			cstmt.executeUpdate();
			iRowCount = cstmt.getInt(5);
			if(iRowCount>0)
				bIsUpdate = true;
			else
				bIsUpdate = false;
			logger.info("OrderDAO::addOrderItemPOPdf::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::addOrderItemPOPdf::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		return bIsUpdate;
	}

	/*public static byte[] getOrderItemEmailPdf(String p_sOrderItemId,String p_sEmail) throws Exception{	
		logger.info("getOrderItemEmailPdf:ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		byte[] baPdf = null;
		String sQry="{call usp_sbh_get_order_item_emails(?,?)}";
		logger.debug("getOrderItemEmailPdf:SQL QUERY "+sQry);
		logger.debug("getOrderItemEmailPdf:Order Item Id: "+p_sOrderItemId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setString(2,p_sEmail);	
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){				
					baPdf = rs.getBytes("pdf");
				}
			}
			logger.info("getOrderItemEmailPdf:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("getOrderItemEmailPdf:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return baPdf;
	}*/
	
	
	public static byte[] getOrderItemEmailPdf(String p_sOrderItemId,String p_sEmail,String p_sStatus) throws Exception{	
		logger.info("OrderDAO::getOrderItemEmailPdf::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		byte[] baPdf = null;
		String sQry="{call usp_sbh_get_order_item_emails(?,?,?)}";
		logger.info("OrderDAO::getOrderItemEmailPdf::SQL QUERY "+sQry);
		logger.info("OrderDAO::getOrderItemEmailPdf::Order Item Id: "+p_sOrderItemId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setString(2,p_sEmail);
			cstmt.setString(3,p_sStatus);
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){				
					baPdf = rs.getBytes("pdf");
				}
			}
			logger.info("OrderDAO::getOrderItemEmailPdf::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getOrderItemEmailPdf::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return baPdf;
	}

	
	
	public static boolean createAndSendCancelledPO(OrderItemDetailsVO oOrderItemDetailsVO,String sReasonForCancel,String sFilePath,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:createAndSendCancelledPO:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sPurchaseContactName = null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		double dPOAmt;
		byte[] baPdf;
		int iStartIndex=0,iEndIndex = 0;
		String sBccAddr="",sCcAddr="";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			dPOAmt = (Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())*(Double.parseDouble(oOrderItemDetailsVO.getPoPct())/100));

			addPartnerPODetails(oOrderItemDetailsVO,p_sLoginId);

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.vendor.cancelled.po");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			sPurchaseContactName = oBundle.getString("skybuyContactName");
			sPurchaseContactName=sPurchaseContactName==null?"":sPurchaseContactName.trim();	

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	



			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerType}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerType()));
			hmTags.put("{{OwnerId}}",oOrderItemDetailsVO.getOwnerLoginId());
			//hmTags.put("{{OwnerContactName}}","Marvin Wind");
			hmTags.put("{{PoNo}}",oOrderItemDetailsVO.getPoNo());
			hmTags.put("{{PurchaseDeptContactName}}",sPurchaseContactName);
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getVendorName());
			}else{
				hmTags.put("{{OwnerName}}",oOrderItemDetailsVO.getAirName());
			}

			hmTags.put("{{OwnerContactName}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getOwnerAddr1(),oOrderItemDetailsVO.getOwnerAddr2())));
			hmTags.put("{{OwnerCity}}", convertToNameFormat(oOrderItemDetailsVO.getOwnerCity()));
			hmTags.put("{{OwnerState}}", oOrderItemDetailsVO.getOwnerState());
			hmTags.put("{{OwnerZip}}",oOrderItemDetailsVO.getOwnerZip());
			hmTags.put("{{OwnerCountry}}",oOrderItemDetailsVO.getOwnerCountry());
			hmTags.put("{{OwnerPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getOwnerPhone()));

			hmTags.put("{{OrderConfNo}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{PoDate}}",oOrderItemDetailsVO.getPoDate());
			hmTags.put("{{ShippingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustShipPhone()));
			hmTags.put("{{ShippingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","USA");
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());

			hmTags.put("{{BillingCustomerName}}", convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}", AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}", convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(),oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}", convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","USA");
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			//double dSbhPrice=dPOAmt;

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{PoPrice}}",numberFormat.format(dPOAmt));
			hmTags.put("{{PoTotalAmount}}",numberFormat.format(dQty*dPOAmt));
			hmTags.put("{{ReasonForCancel}}",sReasonForCancel);

			/*dPOPriceTotal = dPOPriceTotal+dQty*dPOAmt;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dPOPriceTotal));*/

			//hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");


			sToAddr=oOrderItemDetailsVO.getOwnerEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			String sSecondaryEmail = oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			if(oOrderItemDetailsVO.getOwnerType()!=null && oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}

			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			String sHTMLFileName = sFilePath+oOrderItemDetailsVO.getPoNo()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+oOrderItemDetailsVO.getPoNo()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			File oPdfFile = new File(pdfFileName);
			File oHtmlFile = new File(sHTMLFileName);
			

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			//oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailAttachment(baPdf);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
			
			addOrderItemPOPdf(oOrderItemDetailsVO.getOrderItemId(),baPdf,"C",oEmailLogVo.getEmailStatus(),p_sLoginId);

			if(oPdfFile.exists()) {
				oPdfFile.delete();
			}
			if(oHtmlFile.exists()) {
				oHtmlFile.delete();
			}
			
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::createAndSendCancelledPO:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:createAndSendCancelledPO:Exception "+e.getMessage());
			throw e;
		}finally{
			
		}

		return bSentEmail;
	}
	
	
	public static boolean createAndSendCancelledInvoice(OrderItemDetailsVO p_oOrderItemDetailsVO,String sCancelReason,String sFilePath, String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:createAndSendCancelledInvoice:ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="";		
		HashMap<String, String> o_HMTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		List<OrderTrackingDetailsVO> alOrderTrackingList = null;
		byte[] baPdf;
		int iStartIndex=0,iEndIndex = 0;
		String sTrackingDetails = "";
		String sBccAddr="",sCcAddr="",sItemDetails = "",sTrackingDetailStatus = "";
		String sEmailInvoicePdfOptionalContent ="";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.order.cencelled.invoice");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			alOrderTrackingList = getOrderTrackingDetail(p_oOrderItemDetailsVO.getOrderItemId());
			
			addCustomerInvoiceDetails(p_oOrderItemDetailsVO,p_sLoginId);
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			o_HMTags.put("{{Logo}}", sSkyBuyLogoPath);
			o_HMTags.put("{{Date}}",dateFormat(new Date()));
			o_HMTags.put("{{OCN}}",p_oOrderItemDetailsVO.getCustTransId());
			
			o_HMTags.put("{{Address1}}",sSkyBuyAddr1);
			o_HMTags.put("{{Address2}}",sSkyBuyAddr2);
			o_HMTags.put("{{Phone}}",sSkyBuyPh);	
			
			o_HMTags.put("{{ReasonForCancel}}",sCancelReason);
			o_HMTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
			o_HMTags.put("{{InvoiceNo}}",p_oOrderItemDetailsVO.getInvoiceNo());
			o_HMTags.put("{{AirName}}",p_oOrderItemDetailsVO.getAirName());
			o_HMTags.put("{{FlightNo}}",p_oOrderItemDetailsVO.getFlightNo());
			o_HMTags.put("{{OrderItemId}}", p_oOrderItemDetailsVO.getOrderItemId());
			o_HMTags.put("{{OrderId}}",p_oOrderItemDetailsVO.getOrderId());
			o_HMTags.put("{{OrderDate}}",p_oOrderItemDetailsVO.getOrderCreatedDt());
			o_HMTags.put("{{CustomerTransId}}",p_oOrderItemDetailsVO.getCustTransId());
			o_HMTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName()));
			o_HMTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()));
			o_HMTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustLastName()));
			o_HMTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustPhone())));
			o_HMTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustAddress1(), p_oOrderItemDetailsVO.getCustAddress2())));
			o_HMTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustCity()));
			o_HMTags.put("{{BillingState}}",p_oOrderItemDetailsVO.getCustState());
			o_HMTags.put("{{BillingZip}}",p_oOrderItemDetailsVO.getCustZip());
			o_HMTags.put("{{BillingCountry}}","US");
			o_HMTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName()));
			o_HMTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustShipPhone())));
			o_HMTags.put("{{ShippingMail}}",p_oOrderItemDetailsVO.getCustShipEmail());
			o_HMTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustShipAddress1(), p_oOrderItemDetailsVO.getCustShipAddress2())));
			o_HMTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipCity()));
			o_HMTags.put("{{ShippingState}}",p_oOrderItemDetailsVO.getCustShipState());
			o_HMTags.put("{{ShippingZip}}",p_oOrderItemDetailsVO.getCustShipZip());
			o_HMTags.put("{{ShippingCountry}}","US");
			o_HMTags.put("{{BillingMail}}",p_oOrderItemDetailsVO.getCustEmail());
			o_HMTags.put("{{CCNo}}",p_oOrderItemDetailsVO.getCreditCardNoLFD());
			o_HMTags.put("{{ExpDate}}",p_oOrderItemDetailsVO.getExpDt());
			String sCCNumber = p_oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			o_HMTags.put("{{CCNumber}}",sCCNumber);
			o_HMTags.put("{{CCFName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCardFname()));
			o_HMTags.put("{{CCLName}}",p_oOrderItemDetailsVO.getCardLname());

			if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				o_HMTags.put("{{CardType}}","Visa Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				o_HMTags.put("{{CardType}}","Master Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				o_HMTags.put("{{CardType}}","American Express");
			}	

			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(p_oOrderItemDetailsVO.getSize()!= null && p_oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(p_oOrderItemDetailsVO.getColor()!= null && p_oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(p_oOrderItemDetailsVO.getTravelDate()!= null && p_oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			o_HMTags.put("{{ItemDetails}}",sItemDetails);
			o_HMTags.put("{{OrderDate}}", p_oOrderItemDetailsVO.getOrderCreatedDt());

			double dQty=Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());

			o_HMTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
			o_HMTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice())));
			o_HMTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
			o_HMTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getCustServicePhone()));


			if(alOrderTrackingList!=null && alOrderTrackingList.size()>0){
				for(OrderTrackingDetailsVO oOrderTrackingDetailsVO: alOrderTrackingList ){						
					sTrackingDetails += "<tr><td align=left bgcolor=#FFFFFF>"+oOrderTrackingDetailsVO.getTrackingDetails()+"</td><td nowrap=nowrap bgcolor=#FFFFFF align=center>"+oOrderTrackingDetailsVO.getCreate_dt()+"</td></tr>";
				}						
			}else{
				sTrackingDetailStatus = "<br/>Please check back with us after 24 hours to get your shipping details and tracking number.";
			}

			o_HMTags.put("{{TrackingDetails}}",sTrackingDetails);	
			o_HMTags.put("{{TrackingDetailStatus}}",sTrackingDetailStatus);


			if(oEmailVO!=null){
				sEmailInvoicePdfOptionalContent = oEmailVO.getEmailOptionalContent();
			}				

			
			if(sTrackingDetails.trim().length()>0){
				sEmailInvoicePdfOptionalContent=sEmailInvoicePdfOptionalContent==null?"":sEmailInvoicePdfOptionalContent.trim();					
				sEmailInvoicePdfOptionalContent=replaceTagValues(o_HMTags,sEmailInvoicePdfOptionalContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				o_HMTags.put("{{TrackingDetailsTable}}",sEmailInvoicePdfOptionalContent);	
				// System.out.println("sEmailInvoicePdfOptionalContent:"+sEmailInvoicePdfOptionalContent);

			}else{
				o_HMTags.put("{{TrackingDetailsTable}}","");	
			}
			
			sToAddr=p_oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			if(p_oOrderItemDetailsVO.getOwnerType()!=null && p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				iStartIndex = sEmailContent.indexOf("<div id=TravelDateColStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateColEnd>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}

				iStartIndex = sEmailContent.indexOf("<div id=TravelDateTagStart></div>");
				iEndIndex = sEmailContent.indexOf("<div id=TravelDateTagEnd></div>");
				if(iStartIndex > 0 && iEndIndex > 0) {
					String sSubStringFirst = sEmailContent.substring(0,iStartIndex);
					String sSubStringLast = sEmailContent.substring(iEndIndex,sEmailContent.length());
					sEmailContent = sSubStringFirst+sSubStringLast;
				}	
			}
			sEmailContent=replaceTagValues(o_HMTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
			
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			//oEmailLogVo.setAttachmentFile(p_oFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(p_oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
			
			String sHTMLFileName = sFilePath+p_oOrderItemDetailsVO.getInvoiceNo()+".html";

			BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
			ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
			oBufWriter.write(sEmailContent);
			oBufWriter.close();

			PD4ML pd4ml = new PD4ML();
			pd4ml.useTTF( "java:/fonts", true );
			pd4ml.setPageSize(new Dimension(615,790));
			pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
			pd4ml.setHtmlWidth(1230);

			pd4ml.enableDebugInfo();

			pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

			baPdf=oByteArrayOutputStream.toByteArray();

			String pdfFileName=sFilePath+p_oOrderItemDetailsVO.getInvoiceNo()+".pdf";
			FileOutputStream fileos = new FileOutputStream(pdfFileName);
			fileos.write(baPdf);
			fileos.close();

			updateOrderItemStatus(p_oOrderItemDetailsVO.getOrderItemId(),"O",baPdf,p_sLoginId);
			
			/*File oPdfFile = new File(pdfFileName);
			File oHtmlFile = new File(sHTMLFileName);*/
			
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO:createAndSendCancelledInvoice:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:createAndSendCancelledInvoice:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}
	
	public static boolean updateCancelReason(OrderItemDetailsVO oOrderItemDetailsVO,String sCancelReason,String p_sLoginId) throws Exception{	
		logger.info("OrderDAO::updateCancelReason::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		
		int iUpdate;
		boolean bUpdate = false;
		String sQry="{call usp_sbh_upd_cancel_order_item(?,?,?,?)}";		 
		logger.info("OrderDAO::updateCancelReason::SQL QUERY "+sQry);				
		logger.info("OrderDAO::updateCancelReason::Order Item Id "+oOrderItemDetailsVO.getOrderItemId());				
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,oOrderItemDetailsVO.getOrderItemId());	
			cstmt.setString(2,sCancelReason);
			cstmt.setString(3,p_sLoginId);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.executeUpdate();
			iUpdate = cstmt.getInt(4);
			if(iUpdate>0){
				bUpdate = true;
			}else{
				bUpdate = false;
			}
			logger.info("OrderDAO::updateCancelReason::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::updateCancelReason::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}	
		return bUpdate;
	}
	 
	public static List<OrderReturnDetailsVO> getSearchOrderToReturn(OrderReturnForm p_oOrderReturnForm, String p_sOwnerType,String p_sOwnerId) throws Exception{	
		logger.info("OrderDAO::getSearchOrderToReturn::ENTER");
		
		List<OrderReturnDetailsVO> alOrderItemDetails = new ArrayList<OrderReturnDetailsVO>();
		OrderReturnDetailsVO oOrderReturnDetailsVO=null;
		double dQuantity = 0.00;
		double dSbhPrice = 0.00;
		Double dPayAmount;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_order_return_details(?,?,?,?,?,?,?,?)}";
		
		logger.info("OrderDAO::getSearchOrderToReturn::SQL QUERY "+sQry);
		logger.info("OrderDAO::getSearchOrderToReturn::Search By "+p_oOrderReturnForm.getSearchBy());			
		logger.info("OrderDAO::getSearchOrderToReturn::Search Value "+p_oOrderReturnForm.getSearchValue());			
		logger.info("OrderDAO::getSearchOrderToReturn::Category "+p_oOrderReturnForm.getCategory());			
		logger.info("OrderDAO::getSearchOrderToReturn::Order date "+p_oOrderReturnForm.getOrderFromDate());			
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oOrderReturnForm.getSearchBy());		
			cstmt.setString(2,p_oOrderReturnForm.getSearchValue());		
			cstmt.setString(3,p_oOrderReturnForm.getCategory());
			cstmt.setString(4,p_oOrderReturnForm.getOrderStatus());
			cstmt.setString(5,p_oOrderReturnForm.getOrderFromDate());	
			cstmt.setString(6,p_oOrderReturnForm.getOrderToDate());	
			cstmt.setString(7,p_sOwnerType);
			cstmt.setString(8,p_sOwnerId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oOrderReturnDetailsVO=new OrderReturnDetailsVO();
				oOrderReturnDetailsVO.setCustFirstName(rs.getString("cust_bill_fname")+" "+rs.getString("cust_bill_lname"));
				oOrderReturnDetailsVO.setCustPhone(getPhoneFormat(rs.getString("cust_bill_phone")));
				oOrderReturnDetailsVO.setProdCode(rs.getString("prod_code"));
				oOrderReturnDetailsVO.setOrderItemStatus(rs.getString("status"));
				oOrderReturnDetailsVO.setCategoryId(rs.getString("cate_id"));
				oOrderReturnDetailsVO.setQuantity(rs.getString("qty"));
				oOrderReturnDetailsVO.setVendPrice(rs.getString("vend_price"));
				oOrderReturnDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oOrderReturnDetailsVO.setVendorName(rs.getString("owner_name"));
				oOrderReturnDetailsVO.setFlightNo(rs.getString("flight_no"));
				oOrderReturnDetailsVO.setOrderId(rs.getString("order_id"));
				oOrderReturnDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
				oOrderReturnDetailsVO.setOrderCreatedDt(dateStampConversion(rs.getString("order_dt")));		
				oOrderReturnDetailsVO.setOrderItemId(rs.getString("order_item_id"));
				oOrderReturnDetailsVO.setReturnId(rs.getString("return_id"));
				oOrderReturnDetailsVO.setItemName(rs.getString("item_name"));		
				oOrderReturnDetailsVO.setBrandName(rs.getString("brand_name"));		
				oOrderReturnDetailsVO.setOwnerType(rs.getString("owner_type"));
				oOrderReturnDetailsVO.setStatus(rs.getString("status"));
				oOrderReturnDetailsVO.setVendorReturnQuantity(rs.getString("vend_return_qty"));
				oOrderReturnDetailsVO.setReturnQuantity(rs.getString("cust_return_qty"));
				
				if(rs.getString("vend_return_qty") != null)
					dQuantity=Double.parseDouble(rs.getString("vend_return_qty"));
				else
					dQuantity = 0.00;
				if(rs.getString("sbh_price") != null)
					dSbhPrice=Double.parseDouble(rs.getString("sbh_price"));
				
				dPayAmount=dQuantity*dSbhPrice;
				oOrderReturnDetailsVO.setPayAmount(numberFormat.format(dPayAmount));
				
				alOrderItemDetails.add(oOrderReturnDetailsVO);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getSearchOrderToReturn::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getSearchOrderToReturn::EXIT");
		
		return alOrderItemDetails;
	}


	
	public static boolean sendChargeBackFailureEmail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason ,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendChargeBackFailureEmail:ENTER");

		String sEmailContent="";	
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="", sItemDetails ="";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		double chargeBackAmount = 0;
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.credit.chargeback.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			double qty = Double.parseDouble(oOrderItemDetailsVO.getQty());
			if(oOrderItemDetailsVO.getChargeBackPrice()!=null)
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getChargeBackPrice());
			else
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			
			chargeBackAmount = qty*chargeBackAmount;
			
			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{RMANumber}}",oOrderItemDetailsVO.getRmaGeneratedNo());
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{ChargeBackAmount}}",numberFormat.format(chargeBackAmount));
			hmTags.put("{{AirName}}",oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerName}}",oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",oOrderItemDetailsVO.getCardLname());

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());		
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
		
			
			
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);

			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();		
			

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("admin");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendChargeBackFailureEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendChargeBackFailureEmail:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}

	
	/*public static boolean sendOrderCancelFailureEmail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason) throws Exception{
		logger.info("OrderDAO:sendOrderCancelFailureEmail:ENTER");

		String sEmailContent="",sOutput=null;	
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sBCCAddr = "";		
		HashMap hmTags = new HashMap();	
		double chargeBackAmount = 0;
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sOrderFailureEmailContet =null;
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.order.cancel.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			double qty = Double.parseDouble(oOrderItemDetailsVO.getQty());
			if(oOrderItemDetailsVO.getChargeBackAmount()!=null)
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getChargeBackAmount());
			else
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			
			chargeBackAmount = qty*chargeBackAmount;
			
			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{ChargeBackAmount}}",numberFormat.format(chargeBackAmount));
			hmTags.put("{{AirName}}",oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerName}}",oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{CustomerFName}}",oOrderItemDetailsVO.getCustFirstName());
			hmTags.put("{{CustomerLName}}",oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{CustomerPhone}}",oOrderItemDetailsVO.getCustPhone());
			hmTags.put("{{BillingAddress}}",mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2()));
			hmTags.put("{{BillingCity}}",oOrderItemDetailsVO.getCustCity());
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingAddress}}",mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2()));
			hmTags.put("{{ShippingCity}}",oOrderItemDetailsVO.getCustShipCity());
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",getMaskCCNo(oOrderItemDetailsVO.getCreditCardNoLFD()));
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",oOrderItemDetailsVO.getCardLname());

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");

			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();
			
			

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			oEmailLogVo.setOwnerType("admin");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));
			oEmailLogVo.setEmailToAddr(sAdminEmailAddr);

			*//** Insert Email Log Details **//*
			EmailDAO.insertEmailLogDetails(oEmailLogVo);
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderCancelFailureEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendOrderCancelFailureEmail:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}*/
	
	public static boolean sendOrderCancelFailureEmail(OrderItemDetailsVO oOrderItemDetailsVO,String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO:sendOrderCancelFailureEmail:ENTER");

		String sEmailContent="";	
		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sBCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		double chargeBackAmount = 0;
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null,sItemDetails="";
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.order.cancel.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			double qty = Double.parseDouble(oOrderItemDetailsVO.getQty());
			if(oOrderItemDetailsVO.getChargeBackPrice()!=null)
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getChargeBackPrice());
			else
				chargeBackAmount = Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());
			
			chargeBackAmount = qty*chargeBackAmount;
			
			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{OwnerContactName}}",convertToNameFormat(oOrderItemDetailsVO.getOwnerContactName()));
			hmTags.put("{{OwnerType}}",oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{ChargeBackAmount}}",numberFormat.format(chargeBackAmount));
			hmTags.put("{{AirName}}",oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",oOrderItemDetailsVO.getCustTransId());
			
			hmTags.put("{{CustomerName}}",oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()+" "+oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustAddress1(), oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipFirstName()+" "+oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(oOrderItemDetailsVO.getCustShipAddress1(), oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");			
			
			hmTags.put("{{CCNo}}",oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",oOrderItemDetailsVO.getExpDt());
			String sCCNumber = oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
			if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",oOrderItemDetailsVO.getCardLname());

			double dQty=Double.parseDouble(oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(oOrderItemDetailsVO.getSbhPrice());

			hmTags.put("{{OrderItemId}}",oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{Qty}}",oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(oOrderItemDetailsVO.getCustServicePhone()));		
			
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(oOrderItemDetailsVO.getSize()!= null && oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(oOrderItemDetailsVO.getColor()!= null && oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(oOrderItemDetailsVO.getTravelDate()!= null && oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);

			sToAddr=oOrderItemDetailsVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();
			
			

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sBCCAddr = oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBCCAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("admin");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
			oEmailLogVo.setEmailBccAddr(sBCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
			System.out.print("EmailContent "+sEmailContent);

			logger.info("OrderDAO::sendOrderCancelFailureEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.error("OrderDAO:sendOrderCancelFailureEmail:Exception "+e.getMessage());
			throw e;
		}	

		return bSentEmail;
	}
	
	public static OrderReturnDetailsVO updateRMANumberStatus(OrderReturnForm p_oOrderReturnForm, String p_sOwnerName,String p_sOwnerType) 
	throws Exception {
		logger.info("OrderDAO::updateRMANumberStatus::ENTRY");
	
		String sQuery="{call usp_sbh_upd_rma_status(?,?,?,?,?,?,?,?,?)}";
		OrderReturnDetailsVO oOrderReturnDetailsVO = new OrderReturnDetailsVO();
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int iRowCount = 0;
		String sOrderItemId = null;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.setString(3, p_oOrderReturnForm.getRmaNumber());
			cstmt.setString(4, p_sOwnerName);
			cstmt.setString(5, p_sOwnerType);
			cstmt.setString(6, p_oOrderReturnForm.getReturnStatus());
			cstmt.setString(7, p_oOrderReturnForm.getReturnComments());
			cstmt.setString(8, p_oOrderReturnForm.getReturnQuantity());
			cstmt.setString(9, p_oOrderReturnForm.getAuthoriseSignatory());
			cstmt.executeUpdate();
			
			iRowCount = cstmt.getInt(1);
			sOrderItemId = cstmt.getString(2);
			
		}catch(Exception e){
			logger.info("OrderDAO::updateRMANumberStatus::EXCEPTION"+e.getMessage());
			throw e;
		}
		
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
	
		}
		oOrderReturnDetailsVO.setRowCount(iRowCount);
		oOrderReturnDetailsVO.setOrderItemId(sOrderItemId);
		
		logger.info("OrderDAO::updateRMANumberStatus::EXIT");
		
		return oOrderReturnDetailsVO;
	}
	
	public static String getRejectReason(String p_sOrderItemId) 
	throws Exception {
		logger.info("OrderDAO::getRejectReason::ENTRY");
		
		String sReturnReason = null;
		String sQuery="{call usp_sbh_get_return_reason(?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		logger.info("OrderDAO::getRejectReason::SQL Query  "+sQuery);
		logger.info("OrderDAO::getRejectReason::Order Item Id  "+p_sOrderItemId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_sOrderItemId);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sReturnReason = rs.getString("comments");
				}
			}
		}catch(Exception e){
			logger.debug("OrderDAO::getRejectReason::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
	
		}
		
		logger.info("OrderDAO::getRejectReason::EXIT");
		return sReturnReason;
	}
	
	/*public static int getCustomerReturnQuantity(String p_sRMANumber) throws Exception {
		logger.info("OrderDAO::sCustomerReturnQuantity::ENTRY");
		
		int iCustomerReturnQuantity = 0;
		String sCustomerReturnQuantity = null;
		String sQuery="{call usp_sbh_get_cust_return_quantity(?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, p_sRMANumber);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sCustomerReturnQuantity = rs.getString("cust_return_qty");
					if(sCustomerReturnQuantity != null) {
						iCustomerReturnQuantity = Integer.parseInt(sCustomerReturnQuantity);
					}
				}
			}
		}catch(Exception e){
			logger.debug("OrderDAO::sCustomerReturnQuantity::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
	
		}
		
		logger.info("OrderDAO::sCustomerReturnQuantity::EXIT");
		return iCustomerReturnQuantity;
	}*/

	public static OrderItemDetailsVO getReturnOrderItemDetails(String rma_number) 
	throws Exception {
		logger.info("OrderDAO::getReturnOrderItemDetails::ENTRY");
		
		String sQuery="{call usp_sbh_get_return_order_item_details(?)}";
		Connection con=null;
		String sSbh_Price = null;
		String sPo_Pct = null;
		double dSbh_Price=0.00;
		double dPo_Pct=0.00;
		double dChargeBackAmount = 0.00;
		double dQty = 0.00;
		String sDBData = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		CallableStatement cstmt=null;
		ResultSet rs=null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		logger.info("OrderDAO::getReturnOrderItemDetails::SQL Query  "+sQuery);
		logger.info("OrderDAO::getReturnOrderItemDetails::RMA Number  "+rma_number);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, rma_number);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oOrderItemDetailsVO = new OrderItemDetailsVO();
					oOrderItemDetailsVO.setCustId(rs.getString("cust_id"));
					oOrderItemDetailsVO.setCustFirstName(convertToNameFormat(rs.getString("cust_bill_fname")));
					oOrderItemDetailsVO.setCustLastName(convertToNameFormat(rs.getString("cust_bill_lname")));
					oOrderItemDetailsVO.setCustEmail(rs.getString("cust_bill_email"));
					oOrderItemDetailsVO.setCustPhone(rs.getString("cust_bill_phone"));
					oOrderItemDetailsVO.setCustAddress1(rs.getString("cust_bill_addr1"));
					oOrderItemDetailsVO.setCustAddress2(rs.getString("cust_bill_addr2"));
					oOrderItemDetailsVO.setCustCity(rs.getString("cust_bill_city"));
					oOrderItemDetailsVO.setCustState(rs.getString("cust_bill_state"));
					oOrderItemDetailsVO.setCustCountry(rs.getString("cust_bill_country"));
					oOrderItemDetailsVO.setCustZip(rs.getString("cust_bill_zip"));				
					oOrderItemDetailsVO.setCustShipFirstName(convertToNameFormat(rs.getString("cust_ship_fname")));
					oOrderItemDetailsVO.setCustShipLastName(convertToNameFormat(rs.getString("cust_ship_lname")));
					oOrderItemDetailsVO.setCustShipEmail(rs.getString("cust_ship_email"));
					oOrderItemDetailsVO.setCustShipPhone(rs.getString("cust_ship_phone"));
					oOrderItemDetailsVO.setCustShipAddress1(rs.getString("cust_ship_addr1"));
					oOrderItemDetailsVO.setCustShipAddress2(rs.getString("cust_ship_addr2"));
					oOrderItemDetailsVO.setCustShipCity(rs.getString("cust_ship_city"));
					oOrderItemDetailsVO.setCustShipState(rs.getString("cust_ship_state"));
					oOrderItemDetailsVO.setCustShipCountry(rs.getString("cust_ship_country"));
					oOrderItemDetailsVO.setCustShipZip(rs.getString("cust_ship_zip"));				
					oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
					oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
					oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
					oOrderItemDetailsVO.setQty(rs.getString("qty"));
					oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));
					oOrderItemDetailsVO.setOwnerLoginId(rs.getString("owner_login_id"));
					oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));
					oOrderItemDetailsVO.setAirName(rs.getString("air_name"));
					oOrderItemDetailsVO.setFlightNo(rs.getString("flight_no"));
					oOrderItemDetailsVO.setOrderId(rs.getString("order_id"));
					oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
					oOrderItemDetailsVO.setOrderCreatedDt(dateStampConversion(rs.getString("order_dt")));		
					oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));	
					String sOwnerId = rs.getString("owner_id");
					sOwnerId= sOwnerId==null?"":sOwnerId.trim();
					oOrderItemDetailsVO.setOwnerId(sOwnerId);
					oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
					oOrderItemDetailsVO.setOwnerEmail(rs.getString("owner_email"));				
					oOrderItemDetailsVO.setVendorName(rs.getString("owner_name"));
					oOrderItemDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));
					oOrderItemDetailsVO.setItemName(rs.getString("prod_title"));
					oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
					oOrderItemDetailsVO.setProdId(rs.getString("prod_id"));
					oOrderItemDetailsVO.setMainImgPath(rs.getString("main_img_path"));
					oOrderItemDetailsVO.setMainImgType(rs.getString("main_img_type"));
					oOrderItemDetailsVO.setMainImgCaption(rs.getString("main_img_caption"));
					oOrderItemDetailsVO.setCCNo(rs.getString("cc_no"));
					oOrderItemDetailsVO.setMaskCCNo(getMaskCCNo(rs.getString("cc_no")));
					oOrderItemDetailsVO.setKeyRefId(rs.getString("key_ref_id"));
					
					//oOrderItemDetailsVO.setCCNoStatus(rs.getString("cc_no_status"));
					oOrderItemDetailsVO.setCardFname(decryptInputData(rs.getString("fname"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));
					oOrderItemDetailsVO.setCardLname(rs.getString("lname"));
					oOrderItemDetailsVO.setCardType(decryptInputData(rs.getString("card_type"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));
					oOrderItemDetailsVO.setPayAmount(rs.getString("pay_amount"));		
					oOrderItemDetailsVO.setExpDt(decryptInputData(rs.getString("exp_dt"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));	
					oOrderItemDetailsVO.setTravelDate(dateAndTimeStampConversion(rs.getString("travel_date")));
					oOrderItemDetailsVO.setRmaGeneratedNo(rs.getString("rma_number"));	
					oOrderItemDetailsVO.setReturnReason(rs.getString("return_reason"));
					oOrderItemDetailsVO.setRmaStatus(rs.getString("return_status"));
					oOrderItemDetailsVO.setVendorComments(rs.getString("vendor_comments"));
					oOrderItemDetailsVO.setAdminComments(rs.getString("admin_comments"));
					oOrderItemDetailsVO.setChargeBackPrice(rs.getString("charge_back_amount"));
					oOrderItemDetailsVO.setReturnId(rs.getString("return_id"));
					oOrderItemDetailsVO.setReturnQuantity(rs.getString("vend_return_qty"));
					oOrderItemDetailsVO.setCustReturnQuantity(rs.getString("cust_return_qty"));
					oOrderItemDetailsVO.setRmaAuthorizeSignatory(rs.getString("authorise_signatory"));
					
					if(rs.getString("sbh_price") != null && rs.getString("sbh_price") != "") {
						dChargeBackAmount = Double.parseDouble(rs.getString("sbh_price"));
						if(rs.getString("vend_return_qty") != null && rs.getString("vend_return_qty") != "") {
							dQty = Double.parseDouble(rs.getString("vend_return_qty"));
							dChargeBackAmount = dChargeBackAmount * dQty;
							oOrderItemDetailsVO.setChargeBackAmount(dChargeBackAmount+"");
						}
					}
					oOrderItemDetailsVO.setPoPct(rs.getString("po_pct"));
					oOrderItemDetailsVO.setAirAddr1(rs.getString("air_addr1"));
					oOrderItemDetailsVO.setAirAddr2(rs.getString("air_addr2"));
					oOrderItemDetailsVO.setAirCity(rs.getString("air_city"));
					oOrderItemDetailsVO.setAirEmail(rs.getString("air_email1"));
					oOrderItemDetailsVO.setAirCommPct(rs.getString("air_comm_pct"));
					oOrderItemDetailsVO.setAirZip(rs.getString("air_zip"));
					oOrderItemDetailsVO.setAirContactName(rs.getString("air_contact_name1"));
					oOrderItemDetailsVO.setAirState(rs.getString("air_state"));
					oOrderItemDetailsVO.setAirPhone(rs.getString("air_phone1"));
					
					
					String sShipmentStatus = rs.getString("shipment_status");
					sShipmentStatus = sShipmentStatus==null?"N":sShipmentStatus.trim();
					oOrderItemDetailsVO.setShipmentStatus(sShipmentStatus);
					
					sDBData = rs.getString("owner_addr1");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerAddr1(sDBData);
					
					sDBData = rs.getString("color");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setColor(sDBData);

					sDBData = rs.getString("size");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setSize(sDBData);
					
					sDBData = rs.getString("all_size");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setAllSizes(sDBData);
					
					sDBData = rs.getString("all_color");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setAllColors(sDBData);
					
					sDBData = rs.getString("owner_addr2");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerAddr2(sDBData);

					sDBData = rs.getString("owner_city");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerCity(sDBData);

					sDBData = rs.getString("owner_state");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerState(sDBData);

					sDBData = rs.getString("owner_zip");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerZip(sDBData);

					sDBData = rs.getString("owner_country");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerCountry(sDBData);

					sDBData = rs.getString("owner_phone");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerPhone(sDBData);

					sDBData = rs.getString("owner_phone_ext");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerPhoneExt(sDBData);
					
					sSbh_Price = rs.getString("sbh_price");
					sPo_Pct = rs.getString("po_pct");
					if(sSbh_Price != null && sPo_Pct != null) {
						dSbh_Price = Double.parseDouble(rs.getString("sbh_price"));
						dPo_Pct = Double.parseDouble(rs.getString("po_pct"));
						dSbh_Price = (dSbh_Price*dPo_Pct)/100;
						oOrderItemDetailsVO.setPoPrice(numberFormat.format(dSbh_Price));
					}
				}
			}
		}catch(Exception e){
			logger.debug("OrderDAO::getReturnOrderItemDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
	
		}
		
		logger.info("OrderDAO::getReturnOrderItemDetails::EXIT");
		
		return oOrderItemDetailsVO;
	}
	
	public static OrderItemDetailsVO getReturnOrderItemDetail(String returnId) 
	throws Exception {
		logger.info("OrderDAO::getReturnOrderItemDetail::ENTRY");
		
		String sQuery="{call usp_sbh_get_return_order_item_detail(?)}";
		Connection con=null;
		String sSbh_Price = null;
		String sPo_Pct = null;
		double dSbh_Price=0.00;
		double dPo_Pct=0.00;
		double dChargeBackAmount = 0.00;
		double dQty = 0.00;
		String sDBData = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		CallableStatement cstmt=null;
		ResultSet rs=null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		logger.info("OrderDAO::getReturnOrderItemDetail::SQL Query  "+sQuery);
		logger.info("OrderDAO::getReturnOrderItemDetail::RMA Number  "+returnId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQuery);
			cstmt.setString(1, returnId);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oOrderItemDetailsVO = new OrderItemDetailsVO();
					oOrderItemDetailsVO.setCustId(rs.getString("cust_id"));
					oOrderItemDetailsVO.setCustFirstName(convertToNameFormat(rs.getString("cust_bill_fname")));
					oOrderItemDetailsVO.setCustLastName(convertToNameFormat(rs.getString("cust_bill_lname")));
					oOrderItemDetailsVO.setCustEmail(rs.getString("cust_bill_email"));
					oOrderItemDetailsVO.setCustPhone(rs.getString("cust_bill_phone"));
					oOrderItemDetailsVO.setCustAddress1(rs.getString("cust_bill_addr1"));
					oOrderItemDetailsVO.setCustAddress2(rs.getString("cust_bill_addr2"));
					oOrderItemDetailsVO.setCustCity(rs.getString("cust_bill_city"));
					oOrderItemDetailsVO.setCustState(rs.getString("cust_bill_state"));
					oOrderItemDetailsVO.setCustCountry(rs.getString("cust_bill_country"));
					oOrderItemDetailsVO.setCustZip(rs.getString("cust_bill_zip"));				
					oOrderItemDetailsVO.setCustShipFirstName(convertToNameFormat(rs.getString("cust_ship_fname")));
					oOrderItemDetailsVO.setCustShipLastName(convertToNameFormat(rs.getString("cust_ship_lname")));
					oOrderItemDetailsVO.setCustShipEmail(rs.getString("cust_ship_email"));
					oOrderItemDetailsVO.setCustShipPhone(rs.getString("cust_ship_phone"));
					oOrderItemDetailsVO.setCustShipAddress1(rs.getString("cust_ship_addr1"));
					oOrderItemDetailsVO.setCustShipAddress2(rs.getString("cust_ship_addr2"));
					oOrderItemDetailsVO.setCustShipCity(rs.getString("cust_ship_city"));
					oOrderItemDetailsVO.setCustShipState(rs.getString("cust_ship_state"));
					oOrderItemDetailsVO.setCustShipCountry(rs.getString("cust_ship_country"));
					oOrderItemDetailsVO.setCustShipZip(rs.getString("cust_ship_zip"));				
					oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
					oOrderItemDetailsVO.setOrderItemStatus(rs.getString("status"));
					oOrderItemDetailsVO.setCateId(rs.getString("cate_id"));
					oOrderItemDetailsVO.setQty(rs.getString("qty"));
					oOrderItemDetailsVO.setVendPrice(rs.getString("vend_price"));			
					oOrderItemDetailsVO.setSbhPrice(rs.getString("sbh_price"));
					oOrderItemDetailsVO.setAirName(rs.getString("air_name"));
					oOrderItemDetailsVO.setFlightNo(rs.getString("flight_no"));
					oOrderItemDetailsVO.setOwnerLoginId(rs.getString("owner_login_id"));
					oOrderItemDetailsVO.setOrderId(rs.getString("order_id"));
					oOrderItemDetailsVO.setCustTransId(rs.getString("cust_trans_id"));
					oOrderItemDetailsVO.setOrderCreatedDt(dateStampConversion(rs.getString("order_dt")));		
					oOrderItemDetailsVO.setOrderItemId(rs.getString("order_item_id"));	
					String sOwnerId = rs.getString("owner_id");
					sOwnerId= sOwnerId==null?"":sOwnerId.trim();
					oOrderItemDetailsVO.setOwnerId(sOwnerId);
					oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
					oOrderItemDetailsVO.setOwnerEmail(rs.getString("owner_email"));				
					oOrderItemDetailsVO.setVendorName(rs.getString("owner_name"));
					oOrderItemDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));
					oOrderItemDetailsVO.setItemName(rs.getString("prod_title"));
					oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
					oOrderItemDetailsVO.setProdId(rs.getString("prod_id"));
					oOrderItemDetailsVO.setMainImgPath(rs.getString("main_img_path"));
					oOrderItemDetailsVO.setMainImgType(rs.getString("main_img_type"));
					oOrderItemDetailsVO.setMainImgCaption(rs.getString("main_img_caption"));
					oOrderItemDetailsVO.setCCNo(rs.getString("cc_no"));
					oOrderItemDetailsVO.setMaskCCNo(getMaskCCNo(rs.getString("cc_no")));
					oOrderItemDetailsVO.setKeyRefId(rs.getString("key_ref_id"));
					
					//oOrderItemDetailsVO.setCCNoStatus(rs.getString("cc_no_status"));
					oOrderItemDetailsVO.setCardFname(decryptInputData(rs.getString("fname"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));
					oOrderItemDetailsVO.setCardLname(rs.getString("lname"));
					oOrderItemDetailsVO.setCardType(decryptInputData(rs.getString("card_type"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));
					oOrderItemDetailsVO.setPayAmount(rs.getString("pay_amount"));		
					oOrderItemDetailsVO.setExpDt(decryptInputData(rs.getString("exp_dt"),"CLIENT",oOrderItemDetailsVO.getKeyRefId()));	
					oOrderItemDetailsVO.setTravelDate(dateAndTimeStampConversion(rs.getString("travel_date")));
					oOrderItemDetailsVO.setRmaGeneratedNo(rs.getString("rma_number"));	
					oOrderItemDetailsVO.setReturnReason(rs.getString("return_reason"));
					oOrderItemDetailsVO.setRmaStatus(rs.getString("return_status"));
					oOrderItemDetailsVO.setVendorComments(rs.getString("vendor_comments"));
					oOrderItemDetailsVO.setAdminComments(rs.getString("admin_comments"));
					oOrderItemDetailsVO.setChargeBackPrice(rs.getString("sbh_price"));
					oOrderItemDetailsVO.setReturnQuantity(rs.getString("vend_return_qty"));
					oOrderItemDetailsVO.setCustReturnQuantity(rs.getString("cust_return_qty"));
					oOrderItemDetailsVO.setReturnId(rs.getString("return_id"));
					oOrderItemDetailsVO.setPoPct(rs.getString("po_pct"));
					oOrderItemDetailsVO.setAirAddr1(rs.getString("air_addr1"));
					oOrderItemDetailsVO.setAirAddr2(rs.getString("air_addr2"));
					oOrderItemDetailsVO.setAirCity(rs.getString("air_city"));
					oOrderItemDetailsVO.setAirEmail(rs.getString("air_email1"));
					oOrderItemDetailsVO.setAirCommPct(rs.getString("air_comm_pct"));
					oOrderItemDetailsVO.setAirZip(rs.getString("air_zip"));
					oOrderItemDetailsVO.setAirContactName(rs.getString("air_contact_name1"));
					oOrderItemDetailsVO.setAirState(rs.getString("air_state"));
					oOrderItemDetailsVO.setAirPhone(rs.getString("air_phone1"));
					oOrderItemDetailsVO.setRmaAuthorizeSignatory(rs.getString("authorise_signatory"));
					
					if(rs.getString("sbh_price") != null && rs.getString("sbh_price") != "") {
						dChargeBackAmount = Double.parseDouble(rs.getString("sbh_price"));
						if(rs.getString("vend_return_qty") != null && rs.getString("vend_return_qty") != "") {
							dQty = Double.parseDouble(rs.getString("vend_return_qty"));
							dChargeBackAmount = dChargeBackAmount * dQty;
							oOrderItemDetailsVO.setChargeBackAmount(dChargeBackAmount+"");
						}
					}
					oOrderItemDetailsVO.setPoPct(rs.getString("po_pct"));
					
					String sShipmentStatus = rs.getString("shipment_status");
					sShipmentStatus = sShipmentStatus==null?"N":sShipmentStatus.trim();
					oOrderItemDetailsVO.setShipmentStatus(sShipmentStatus);
					
					sDBData = rs.getString("color");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setColor(sDBData);

					sDBData = rs.getString("size");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setSize(sDBData);
					
					sDBData = rs.getString("all_size");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setAllSizes(sDBData);
					
					sDBData = rs.getString("all_color");
					sDBData = sDBData == null?"":sDBData.trim();
					oOrderItemDetailsVO.setAllColors(sDBData);
					
					sDBData = rs.getString("owner_addr1");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerAddr1(sDBData);

					sDBData = rs.getString("owner_addr2");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerAddr2(sDBData);

					sDBData = rs.getString("owner_city");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerCity(sDBData);

					sDBData = rs.getString("owner_state");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerState(sDBData);

					sDBData = rs.getString("owner_zip");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerZip(sDBData);

					sDBData = rs.getString("owner_country");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerCountry(sDBData);

					sDBData = rs.getString("owner_phone");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerPhone(sDBData);

					sDBData = rs.getString("owner_phone_ext");
					sDBData = sDBData==null?"":sDBData.trim();
					oOrderItemDetailsVO.setOwnerPhoneExt(sDBData);

					sSbh_Price = rs.getString("sbh_price");
					sPo_Pct = rs.getString("po_pct");
					if(sSbh_Price != null && sPo_Pct != null) {
						dSbh_Price = Double.parseDouble(rs.getString("sbh_price"));
						dPo_Pct = Double.parseDouble(rs.getString("po_pct"));
						dSbh_Price = (dSbh_Price*dPo_Pct)/100;
						oOrderItemDetailsVO.setPoPrice(numberFormat.format(dSbh_Price));
					}
				}
			}
		}catch(Exception e){
			logger.info("OrderDAO::getReturnOrderItemDetail::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
	
		}
		
		logger.info("OrderDAO::getReturnOrderItemDetail::EXIT");
		
		return oOrderItemDetailsVO;
	}

	

	
	public static void updateReturnDetail(OrderReturnForm p_oOrderReturnForm, String p_sReturnId,String p_sLoginId) 
		throws Exception {
		logger.info("OrderDAO::updateReturnDetail::ENTER");
		
		Connection con=null;		
		CallableStatement cstmt=null;		
	
		String sQry="{call usp_sbh_upd_order_return_quantity(?,?,?,?)}";		 
		logger.info("OrderDAO::updateReturnDetail::SQL QUERY "+sQry);				
		logger.info("OrderDAO::updateReturnDetail::Order Return Quantity"+p_oOrderReturnForm.getReturnQuantity());
		logger.info("OrderDAO::updateReturnDetail::Order Return Quantity"+p_oOrderReturnForm.getAdminComments());
		logger.info("OrderDAO::updateReturnDetail::Order Return Id "+p_sReturnId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_oOrderReturnForm.getReturnQuantity());	
			cstmt.setString(2,p_sReturnId);
			cstmt.setString(3,p_oOrderReturnForm.getAdminComments());
			cstmt.setString(4,p_sLoginId);
			cstmt.executeUpdate();
	
			logger.info("OrderDAO::updateReturnDetail::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::updateReturnDetail::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("OrderDAO::updateReturnDetail::EXIT");
	}

	public static void updateOrderItemStatusAndReturnStatus(String p_sOrderItemId,String p_sOrderStatus, String p_sReturnStatus, String p_sAmount, String p_sAdminComments, String p_sLoginId) throws Exception{	
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;		

		String sQry="{call usp_sbh_upd_order_item_and_return_status(?,?,?,?,?,?)}";		 
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::SQL QUERY "+sQry);				
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::Order Item Id "+p_sOrderItemId);				
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::Order Status "+p_sOrderStatus);
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::Order Status "+p_sReturnStatus);
		logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::Order Status "+p_sAmount);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sOrderItemId);	
			cstmt.setString(2,p_sOrderStatus);
			cstmt.setString(3, p_sReturnStatus);
			cstmt.setString(4, p_sAmount);
			cstmt.setString(5, p_sAdminComments);
			cstmt.setString(6, p_sLoginId);
			cstmt.executeUpdate();

			logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::updateOrderItemStatusAndReturnStatus::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}	
	}
		
	public static boolean sendRMAApprovedEmailToAdmin(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMAApprovedEmailToAdmin::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sEmailBccList = null;
		String sFromEmailAddr=null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailTo=null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails ="";
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.confirmation.to.admin");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMAApprovedEmailToAdmin::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
			
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
				
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
	
				sEmailTo = p_oOrderItemDetails.getOwnerEmail();
				sEmailTo = sEmailTo==null?"":sEmailTo.trim();
				String sSecondaryEmail = p_oOrderItemDetails.getOwnerSecondaryEmail();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sEmailTo.contains(sSecondaryEmail)){
						sEmailTo = sEmailTo + ","+sSecondaryEmail;
					}
				}
				
				p_sReason = p_sReason==null?"":p_sReason.trim();
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
				hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetails.getSbhPrice())));
				hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));

				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
				hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE"))
					hmTags.put("{{TravelDate}}",p_oOrderItemDetails.getTravelDate());		
				
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sEmailTo));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("ADMIN");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sEmailTo);
		oEmailLogVo.setEmailBccAddr(sEmailBccList);
		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendRMAApprovedEmailToAdmin::EXIT");
		
		return bEmailSent;
		
	}
	public static boolean sendRMAApprovedEmailToCustomer(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason, String p_sReturnQuantity,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMAApprovedEmailToCustomer::ENTRY");
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sTOAddress = null;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr=null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sEmailBccList = null;
		String sVendorReturnQuantity = null;
		int iCustomerReturnQuantity = 0;
		int iVendorReturnQuantity = 0;
		String sItemDetails = "";
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.confirmation.to.customer");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMAApprovedEmailToCustomer::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
				
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
				
				if(p_oOrderItemDetails.getReturnQuantity() != null) {
					iCustomerReturnQuantity = Integer.parseInt(p_oOrderItemDetails.getReturnQuantity());
					if(p_sReturnQuantity != null) {
						iVendorReturnQuantity = Integer.parseInt(p_sReturnQuantity);
					}
					if(iCustomerReturnQuantity == iVendorReturnQuantity) {
						sVendorReturnQuantity = "all are";
					}else if(iVendorReturnQuantity == 1){
						sVendorReturnQuantity = "1 is";
					}else {
						sVendorReturnQuantity = iVendorReturnQuantity+"are";
					}
				}
				p_sReason = p_sReason==null?"":p_sReason.trim();
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{VendorReturnQuantity}}",sVendorReturnQuantity);
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
				hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetails.getSbhPrice())));
				hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));

				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
				hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE"))
					hmTags.put("{{TravelDate}}",p_oOrderItemDetails.getTravelDate());		
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sTOAddress = p_oOrderItemDetails.getCustEmail();
				sTOAddress = sTOAddress==null?"":sTOAddress.trim();
				
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sTOAddress));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("ADMIN");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sTOAddress);
		oEmailLogVo.setEmailBccAddr(sEmailBccList);
		
		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendRMAApprovedEmailToCustomer::EXIT");
		
		return bEmailSent;
		
	}
	public static boolean sendRMAApprovedEmailToVendor(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMAApprovedEmailToVendor::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr = null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailBccList = null;
		String sEmailTo=null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails =""; 
			
		long lOrderNumber=getRandomNo();
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.confirmation.to.vendor");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMAApprovedEmailToVendor::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
				
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
	
				
				sEmailTo = p_oOrderItemDetails.getOwnerEmail();
				sEmailTo = sEmailTo==null?"":sEmailTo.trim();
	
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
	
				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
			
				
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sEmailTo));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("VENDOR");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sEmailTo);
		oEmailLogVo.setEmailBccAddr(sEmailBccList);
		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendRMAApprovedEmailToVendor::EXIT");
		return bEmailSent;
		
	}

	public static boolean sendRMARejectedEmailToAdmin(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMARejectedEmailToAdmin::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr = null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailBccList = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails ="";
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.rejection.to.admin");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMARejectedEmailToAdmin::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
	
	
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");			
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
				hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetails.getSbhPrice())));
				hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
	
				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
				hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
			
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("ADMIN");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
		oEmailLogVo.setEmailToAddr(sEmailBccList);
		
		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendRMARejectedEmailToAdmin::EXIT");
		return bEmailSent;
		
	}
	public static boolean sendRMARejectedEmailToCustomer(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMARejectedEmailToCustomer::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sTOAddress = null;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr=null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailBccList = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails ="";
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.rejection.to.customer");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMARejectedEmailToCustomer::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
				
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
				
				p_sReason = p_sReason==null?"":p_sReason.trim();
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");			
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
				hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetails.getSbhPrice())));
				hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));

				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
				hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE"))
					hmTags.put("{{TravelDate}}",p_oOrderItemDetails.getTravelDate());		
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sTOAddress = p_oOrderItemDetails.getCustEmail();
				sTOAddress = sTOAddress==null?"":sTOAddress.trim();
				
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sTOAddress));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("ADMIN");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sTOAddress);
		oEmailLogVo.setEmailBccAddr(sEmailBccList);

		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendRMARejectedEmailToCustomer::EXIT");
		
		return bEmailSent;
		
	}
	public static boolean sendRMARejectedEmailToVendor(OrderItemDetailsVO p_oOrderItemDetails, String p_sReason,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("OrderDAO::sendRMARejectedEmailToVendor::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr = null;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailBccList = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails = "";
		String sEmailTo=null;
		long lOrderNumber=getRandomNo();
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.order.return.rejection.to.vendor");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.info("OrderDAO::sendRMARejectedEmailToVendor::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetails != null) {
				sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
				sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
				sFromEmailAddr=System.getProperty("email.from.address").trim();
				sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
				sSkyBuyAddr1=oBundle.getString("skybuy.address1");
				sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
				sSkyBuyAddr2=oBundle.getString("skybuy.address2");
				sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
				sSkyBuyPh=oBundle.getString("skybuy.phone");
				sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
				//Header
				hmTags.put("{{Logo}}",sSkyBuyLogoPath);
				hmTags.put("{{Date}}",dateFormat(new Date()));
				//SkyBuy Address
				hmTags.put("{{Address1}}",sSkyBuyAddr1);
				hmTags.put("{{Address2}}",sSkyBuyAddr2);
				hmTags.put("{{Phone}}",sSkyBuyPh);	
	
			
				sEmailTo = p_oOrderItemDetails.getOwnerEmail();
				sEmailTo = sEmailTo==null?"":sEmailTo.trim();

				String sSecondaryEmail = p_oOrderItemDetails.getOwnerSecondaryEmail();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sEmailTo.contains(sSecondaryEmail)){
						sEmailTo = sEmailTo + ","+sSecondaryEmail;
					}
				}
				
				hmTags.put("{{Reason}}",p_sReason);
				hmTags.put("{{CustomerName}}",p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName());
				hmTags.put("{{RMANumber}}",p_oOrderItemDetails.getRmaGeneratedNo());
				hmTags.put("{{OwnerType}}",p_oOrderItemDetails.getOwnerType());
				hmTags.put("{{OwnerContactName}}",p_oOrderItemDetails.getOwnerContactName());
				hmTags.put("{{AirName}}",p_oOrderItemDetails.getAirName());
				hmTags.put("{{FlightNo}}",p_oOrderItemDetails.getFlightNo());
				hmTags.put("{{OrderId}}",p_oOrderItemDetails.getOrderId());
				hmTags.put("{{OrderDate}}",p_oOrderItemDetails.getOrderCreatedDt());
				hmTags.put("{{CustomerTransId}}",p_oOrderItemDetails.getCustTransId());
				hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()+" "+p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetails.getCustFirstName()));
				hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetails.getCustLastName()));
				hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustPhone())));
				hmTags.put("{{BillingMail}}",p_oOrderItemDetails.getCustEmail());
				hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustAddress1(), p_oOrderItemDetails.getCustAddress2())));
				hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustCity()));
				hmTags.put("{{BillingState}}",p_oOrderItemDetails.getCustState());
				hmTags.put("{{BillingZip}}",p_oOrderItemDetails.getCustZip());
				hmTags.put("{{BillingCountry}}","USA");
				hmTags.put("{{AuthoriseSignatory}}",convertToNameFormat(p_oOrderItemDetails.getRmaAuthorizeSignatory()));
				hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetails.getCustShipFirstName()+" "+p_oOrderItemDetails.getCustShipLastName()));
				hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetails.getCustShipPhone())));
				hmTags.put("{{ShippingMail}}",p_oOrderItemDetails.getCustShipEmail());
				hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetails.getCustShipAddress1(), p_oOrderItemDetails.getCustShipAddress2())));
				hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetails.getCustShipCity()));
				hmTags.put("{{ShippingState}}",p_oOrderItemDetails.getCustShipState());
				hmTags.put("{{ShippingZip}}",p_oOrderItemDetails.getCustShipZip());
				hmTags.put("{{ShippingCountry}}","USA");			
				hmTags.put("{{CCNo}}",p_oOrderItemDetails.getCreditCardNoLFD());
				hmTags.put("{{ExpDate}}",p_oOrderItemDetails.getExpDt());
				String sCCNumber = p_oOrderItemDetails.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				hmTags.put("{{CCNumber}}",sCCNumber);
	
				if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("VC")){
					hmTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("MC")){
					hmTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetails.getCardType().equalsIgnoreCase("AC")){
					hmTags.put("{{CardType}}","American Express");
				}
	
				hmTags.put("{{CCFName}}",p_oOrderItemDetails.getCardFname());
				hmTags.put("{{CCLName}}",p_oOrderItemDetails.getCardLname());
	
				double dQty=Double.parseDouble(p_oOrderItemDetails.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetails.getSbhPrice());
	
				hmTags.put("{{OrderItemId}}",p_oOrderItemDetails.getOrderItemId());
				hmTags.put("{{VendorName}}",p_oOrderItemDetails.getVendorName());
				hmTags.put("{{Qty}}",p_oOrderItemDetails.getQty());
	
				double dPOPct = Double.parseDouble(p_oOrderItemDetails.getPoPct());
				double dPOPrice = (dSbhPrice*dPOPct)/100.00;
				hmTags.put("{{POPrice}}",numberFormat.format(dPOPrice));
	
				hmTags.put("{{POPriceTotal}}",numberFormat.format(dQty*dPOPrice));
				hmTags.put("{{OrderNumber}}",lOrderNumber+"");
				hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetails.getCustServicePhone()));
			
				
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetails.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetails.getSize()!= null && p_oOrderItemDetails.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getSize()+"</td></tr>";
					if(p_oOrderItemDetails.getColor()!= null && p_oOrderItemDetails.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetails.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetails.getTravelDate()!= null && p_oOrderItemDetails.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetails.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				if(oEmailVO != null) {
					sEmailContent = oEmailVO.getEmailContent();
					sEmailSubject = oEmailVO.getEmailSubject();
					sEmailBccList = oEmailVO.getEmailBccList();
				}
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
				sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			}
		
		EmailLogVO oEmailLogVo=new EmailLogVO();
		Email oEmail=new Email();

		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sEmailTo));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccList));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);

		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("VENDOR");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sEmailTo);
		oEmailLogVo.setEmailBccAddr(sEmailBccList);

		/** Insert Email Log Details **/
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		logger.info("OrderDAO::sendRMARejectedEmailToVendor::EXIT");
		return bEmailSent;
		
	}

	
	public static boolean sendOrderChargeBackSuccessEmailToCustomer(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sReqeustFrom) 
		throws Exception {
		
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToCustomer::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sCustomerEmailAddr = null;
		String sEmailBccAddr = null;
		String sItemDetails = "";
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		double dSkyBuyPriceTotal=0.00;
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.chargeback.approved.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToCustomer::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetailsVO != null) {
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			double qty = Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double chargeBackAmount = Double.parseDouble(p_oOrderItemDetailsVO.getChargeBackPrice());
			chargeBackAmount = qty*chargeBackAmount;
			
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
	
			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{CustomerName}}",p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{RMANumber}}",p_oOrderItemDetailsVO.getRmaGeneratedNo());
			hmTags.put("{{VendorComments}}",p_oOrderItemDetailsVO.getVendorComments());
			hmTags.put("{{AdminComments}}",p_oOrderItemDetailsVO.getAdminComments());
			hmTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",p_oOrderItemDetailsVO.getOwnerContactName());
			hmTags.put("{{AirName}}",p_oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",p_oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",p_oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",p_oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",p_oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",p_oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustAddress1(), p_oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",p_oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",p_oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",p_oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustShipAddress1(), p_oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",p_oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",p_oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CCNo}}",p_oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",p_oOrderItemDetailsVO.getExpDt());
			if(p_oOrderItemDetailsVO.getChargeBackPrice() != null) {
				hmTags.put("{{ChargeBackPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetailsVO.getChargeBackPrice())));
			}
			hmTags.put("{{ChargeBackAmount}}",numberFormat.format(chargeBackAmount));
			
			
			String sCCNumber = p_oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
	
			if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}
	
			hmTags.put("{{CCFName}}",p_oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",p_oOrderItemDetailsVO.getCardLname());
	
			double dQty=Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
	
			hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{PartnerName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
	
			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getCustServicePhone()));
		
			
			
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(p_oOrderItemDetailsVO.getSize()!= null && p_oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(p_oOrderItemDetailsVO.getColor()!= null && p_oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(p_oOrderItemDetailsVO.getTravelDate()!= null && p_oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			sEmailBccAddr = sEmailBccAddr == null?"":sEmailBccAddr.trim();
			sAdminEmailAddr = sEmailBccAddr+","+sAdminEmailAddr;
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		sCustomerEmailAddr = p_oOrderItemDetailsVO.getCustEmail();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToCustomer::EXCEPTION");
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sCustomerEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sAdminEmailAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sCustomerEmailAddr);
		oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToCustomer::EXIT");
		return bEmailSent;
	}

	public static boolean sendOrderChargeBackSuccessEmailToAirline(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sReqeustFrom) 
		throws Exception {
	
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToAirline::ENTRY");
	
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sAirlineEmailAddr = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sEmailBccAddr = null;
		double dAirCommision = 0.00;
		double dSbhPrice = 0.00;
		double dQty = 0.00;
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.chargeback.commission.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToAirline::EXCEPTION");
			throw exception;
		}
		
		if(p_oOrderItemDetailsVO != null) {
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
			
			hmTags.put("{{ReasonForReturn}}",p_oOrderItemDetailsVO.getReturnReason());
			hmTags.put("{{AirName}}", convertToNameFormat(p_oOrderItemDetailsVO.getAirName()));
			hmTags.put("{{FlightNo}}", p_oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{AirContactName}}", convertToNameFormat(p_oOrderItemDetailsVO.getAirContactName()));
			hmTags.put("{{AirAddress}}", convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getAirAddr1(),p_oOrderItemDetailsVO.getAirAddr2())));
			hmTags.put("{{AirCity}}", convertToNameFormat(p_oOrderItemDetailsVO.getAirCity()));
			hmTags.put("{{AirState}}", p_oOrderItemDetailsVO.getAirState());
			hmTags.put("{{AirZip}}",p_oOrderItemDetailsVO.getAirZip());
			hmTags.put("{{AirCountry}}","USA");
			hmTags.put("{{AirPhone}}", AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getAirPhone()));
			hmTags.put("{{AirUserId}}", p_oOrderItemDetailsVO.getAirUserId());
			hmTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{Date}}",dateFormat(new Date()));
			hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{CustomerName}}",p_oOrderItemDetailsVO.getCustFirstName()+' '+p_oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",p_oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
			hmTags.put("{{OrderConfNo}}",p_oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{SkyBuyPrice}}",p_oOrderItemDetailsVO.getSbhPrice());
			
			if(p_oOrderItemDetailsVO.getAirCommPct() != null)
				dAirCommision = Double.parseDouble(p_oOrderItemDetailsVO.getAirCommPct());
			
			if(p_oOrderItemDetailsVO.getSbhPrice() != null)
				dSbhPrice = Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
			
			dAirCommision = (dSbhPrice * dAirCommision)/100;
			hmTags.put("{{AirlineCommission}}",dAirCommision+"");
			
			dQty = Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			dAirCommision = dAirCommision * dQty;
			
			hmTags.put("{{TotalCommission}}",dAirCommision+"");
	
		
			sAirlineEmailAddr = p_oOrderItemDetailsVO.getAirEmail();
			sAirlineEmailAddr = sAirlineEmailAddr == null?"":sAirlineEmailAddr.trim();
			
			String sSecondaryEmail = p_oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sAirlineEmailAddr.contains(sSecondaryEmail)){
					sAirlineEmailAddr = sAirlineEmailAddr + ","+sSecondaryEmail;
				}
			}
			
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			if(sEmailBccAddr != null && sEmailBccAddr.trim().length() > 0) {
				sEmailBccAddr = sAdminEmailAddr+","+sEmailBccAddr;
			}else {
				sEmailBccAddr = sAdminEmailAddr;
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToAirline::EXCEPTION");
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
		sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAirlineEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sAirlineEmailAddr);
		oEmailLogVo.setEmailBccAddr(sEmailBccAddr);
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToAirline::EXIT");
		return bEmailSent;
	}


	
	public static boolean sendUpdateReturnOrderDetail(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sReqeustFrom) throws Exception {
		logger.info("OrderDAO::sendUpdateReturnOrderDetail::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sCustomerEmailAddr = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		String sItemDetails="";
		String sOwnerEmailAddr = null;
		String sEmailBccAddr = null;
		long lOrderNumber=getRandomNo();
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.updated.return.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendUpdateReturnOrderDetail::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetailsVO != null) {
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	

			hmTags.put("{{CustomerName}}",p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{RMANumber}}",p_oOrderItemDetailsVO.getRmaGeneratedNo());
//			hmTags.put("{{VendorComments}}",p_oOrderItemDetailsVO.getVendorComments());
			hmTags.put("{{AdminComments}}",p_oOrderItemDetailsVO.getAdminComments());
			hmTags.put("{{OwnerName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{Qty}}",p_oOrderItemDetailsVO.getReturnQuantity());
			hmTags.put("{{OwnerContactName}}",p_oOrderItemDetailsVO.getOwnerContactName());
			hmTags.put("{{AirName}}",p_oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",p_oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",p_oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",p_oOrderItemDetailsVO.getOrderCreatedDt());
			hmTags.put("{{CustomerTransId}}",p_oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{CustomerFName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustFirstName()));
			hmTags.put("{{CustomerLName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustLastName()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustPhone())));
			hmTags.put("{{BillingMail}}",p_oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{BillingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustAddress1(), p_oOrderItemDetailsVO.getCustAddress2())));
			hmTags.put("{{BillingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustCity()));
			hmTags.put("{{BillingState}}",p_oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",p_oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",p_oOrderItemDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",convertToNameFormat(mergeAddressLines(p_oOrderItemDetailsVO.getCustShipAddress1(), p_oOrderItemDetailsVO.getCustShipAddress2())));
			hmTags.put("{{ShippingCity}}",convertToNameFormat(p_oOrderItemDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",p_oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",p_oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");			
			hmTags.put("{{CCNo}}",p_oOrderItemDetailsVO.getCreditCardNoLFD());
			hmTags.put("{{ExpDate}}",p_oOrderItemDetailsVO.getExpDt());
			String sCCNumber = p_oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);

			if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}

			hmTags.put("{{CCFName}}",p_oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",p_oOrderItemDetailsVO.getCardLname());

			hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{PartnerName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",p_oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",p_oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{ReturnQty}}",p_oOrderItemDetailsVO.getCustReturnQuantity());
			hmTags.put("{{RMAApprovedQty}}",p_oOrderItemDetailsVO.getReturnQuantity());
			
			hmTags.put("{{OrderNumber}}",lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getCustServicePhone()));
			
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getBrandName()+"</td></tr>";
				 
			}	
			sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getItemName()+"</td></tr>";
			sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getProdCode()+"</td></tr>";
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(p_oOrderItemDetailsVO.getSize()!= null && p_oOrderItemDetailsVO.getSize().trim().length()>0)
						sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getSize()+"</td></tr>";
				if(p_oOrderItemDetailsVO.getColor()!= null && p_oOrderItemDetailsVO.getColor().trim().length()>0)
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getColor()+"</td></tr>";
			}	
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				if(p_oOrderItemDetailsVO.getTravelDate()!= null && p_oOrderItemDetailsVO.getTravelDate().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
			}	
			
			sItemDetails = sItemDetails == null?"":sItemDetails.trim();
			hmTags.put("{{ItemDetails}}",sItemDetails);
			
			sOwnerEmailAddr = p_oOrderItemDetailsVO.getOwnerEmail();
			sOwnerEmailAddr = sOwnerEmailAddr == null?"":sOwnerEmailAddr.trim();
			
			String sSecondaryEmail = p_oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sOwnerEmailAddr.contains(sSecondaryEmail)){
					sOwnerEmailAddr = sOwnerEmailAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			if(sEmailBccAddr != null && sEmailBccAddr.trim().length() > 0) {
				sEmailBccAddr = sAdminEmailAddr+","+sEmailBccAddr;
			}else {
				sEmailBccAddr = sAdminEmailAddr;
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		sCustomerEmailAddr = p_oOrderItemDetailsVO.getCustEmail();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendUpdateReturnOrderDetail::EXCEPTION");
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sOwnerEmailAddr));
		oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCustomerEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sCustomerEmailAddr);
		oEmailLogVo.setEmailBccAddr(sEmailBccAddr);
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendUpdateReturnOrderDetail::EXIT");
		return bEmailSent;
	}


	public static boolean sendOrderChargeBackSuccessEmailToVendor(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sReqeustFrom) 
		throws Exception {
		
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToVendor::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sVendorEmailAddr = null;
		String sEmailBccAddr = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		double dPOPriceTotal=0.00;
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.chargeback.po.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToVendor::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetailsVO != null) {
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			double qty = Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double chargeBackAmount = Double.parseDouble(p_oOrderItemDetailsVO.getChargeBackPrice());
			chargeBackAmount = qty*chargeBackAmount;
			
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
	
			hmTags.put("{{OwnerContactName}}",p_oOrderItemDetailsVO.getOwnerContactName());
			hmTags.put("{{OwnerAddress}}",mergeAddressLines(p_oOrderItemDetailsVO.getOwnerAddr1(), p_oOrderItemDetailsVO.getOwnerAddr2()));
			hmTags.put("{{OwnerCity}}",p_oOrderItemDetailsVO.getOwnerCity());
			hmTags.put("{{OwnerState}}",p_oOrderItemDetailsVO.getOwnerState());
			hmTags.put("{{OwnerZip}}",p_oOrderItemDetailsVO.getOwnerZip());
			hmTags.put("{{OwnerPhone}}",p_oOrderItemDetailsVO.getOwnerPhone());
			hmTags.put("{{OwnerId}}",p_oOrderItemDetailsVO.getOwnerLoginId());
			hmTags.put("{{OwnerName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{Date}}",dateFormat(new Date()));
			hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",p_oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
			hmTags.put("{{OrderConfNo}}",p_oOrderItemDetailsVO.getCustTransId());
	
			double dQty = Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double dPOpercentage = Double.parseDouble(p_oOrderItemDetailsVO.getPoPct());
			double dSbhPrice = Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
			double dPOPrice = (dSbhPrice * dPOpercentage)/100;
			
			dPOPriceTotal = dPOPriceTotal+dQty*dPOPrice;		
			
			hmTags.put("{{POPrice}}", numberFormat.format(dPOPrice));
			hmTags.put("{{POTotalAmount}}", numberFormat.format(dPOPriceTotal));
			
			//Get the Partner Email Address.
			sVendorEmailAddr = p_oOrderItemDetailsVO.getOwnerEmail();
			sVendorEmailAddr = sVendorEmailAddr == null?"":sVendorEmailAddr.trim();
			
			String sSecondaryEmail = p_oOrderItemDetailsVO.getOwnerSecondaryEmail();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sVendorEmailAddr.contains(sSecondaryEmail)){
					sVendorEmailAddr = sVendorEmailAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			if(sEmailBccAddr != null && sEmailBccAddr.trim().length() > 0) {
				sEmailBccAddr = sAdminEmailAddr+","+sEmailBccAddr;
			}else {
				sEmailBccAddr = sAdminEmailAddr;
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackSuccessEmailToVendor::EXCEPTION");
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sVendorEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sVendorEmailAddr);
		oEmailLogVo.setEmailBccAddr(sEmailBccAddr);
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendOrderChargeBackSuccessEmailToVendor::EXIT");
		return bEmailSent;
	}
	
	public static boolean sendOrderChargeBackRejectEmail(OrderItemDetailsVO p_oOrderItemDetailsVO, String p_sReason,String p_sLoginId,String p_sReqeustFrom) 
	throws Exception {
	
		logger.info("OrderDAO::sendOrderChargeBackRejectEmail::ENTRY");
		
		boolean bEmailSent = false;
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sEmailTemplateId;
		String sEmailSubject = null;
		String sEmailContent = null;
		String sFromEmailAddr;
		String sSkyBuyLogoPath;
		String sAdminEmailAddr = null;
		String sEmailBccAddr = null;
		String sCustomerEmailAddr = null;
		String sSkyBuyAddr1;
		String sSkyBuyAddr2;
		String sSkyBuyPh;
		long lOrderNumber=getRandomNo();
		double dSkyBuyPriceTotal=0.00;
		EmailLogVO oEmailLogVo = null;
		Email oEmail = null;
		
		NumberFormat numberFormat=NumberFormat.getCurrencyInstance(Locale.US);
		HashMap<String, String> hmTags = new HashMap<String, String>();
		EmailVO oEmailVO = new EmailVO();
		
		sEmailTemplateId=oBundle.getString("email.chargeback.rejected.details");
		sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackRejectEmail::EXCEPTION");
			throw exception;
		}
		if(p_oOrderItemDetailsVO != null) {
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
	
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
	
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
	
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
	
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	
	
			hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{RMANumber}}",p_oOrderItemDetailsVO.getRmaGeneratedNo());
			hmTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
			hmTags.put("{{OwnerContactName}}",p_oOrderItemDetailsVO.getOwnerContactName());
			hmTags.put("{{AirName}}",p_oOrderItemDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",p_oOrderItemDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",p_oOrderItemDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",dateStampConversion(p_oOrderItemDetailsVO.getOrderCreatedDt()));
			hmTags.put("{{CustomerTransId}}",p_oOrderItemDetailsVO.getCustTransId());
			hmTags.put("{{CustomerFName}}",p_oOrderItemDetailsVO.getCustFirstName());
			hmTags.put("{{CustomerLName}}",p_oOrderItemDetailsVO.getCustLastName());
			hmTags.put("{{CustomerPhone}}",getPhoneFormat(p_oOrderItemDetailsVO.getCustPhone()));
			hmTags.put("{{BillingAddress}}",mergeAddressLines(p_oOrderItemDetailsVO.getCustAddress1(), p_oOrderItemDetailsVO.getCustAddress2()));
			hmTags.put("{{BillingCity}}",p_oOrderItemDetailsVO.getCustCity());
			hmTags.put("{{BillingState}}",p_oOrderItemDetailsVO.getCustState());
			hmTags.put("{{BillingZip}}",p_oOrderItemDetailsVO.getCustZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingAddress}}",mergeAddressLines(p_oOrderItemDetailsVO.getCustShipAddress1(), p_oOrderItemDetailsVO.getCustShipAddress2()));
			hmTags.put("{{ShippingCity}}",p_oOrderItemDetailsVO.getCustShipCity());
			hmTags.put("{{ShippingState}}",p_oOrderItemDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",p_oOrderItemDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{CustomerMail}}",p_oOrderItemDetailsVO.getCustEmail());
			hmTags.put("{{CCNo}}",p_oOrderItemDetailsVO.getCreditCardNoLFD());
			String sCCNumber = p_oOrderItemDetailsVO.getCreditCardNoLFD();
			sCCNumber = sCCNumber==null?"":sCCNumber.trim();
			/*if(sCCNumber.trim().length()>0){
				if(sCCNumber.length()>0)
					sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
			}*/
			hmTags.put("{{CCNumber}}",sCCNumber);
	
			if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
				hmTags.put("{{CardType}}","Visa Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
				hmTags.put("{{CardType}}","Master Card");
			}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
				hmTags.put("{{CardType}}","American Express");
			}
	
			hmTags.put("{{CCFName}}",p_oOrderItemDetailsVO.getCardFname());
			hmTags.put("{{CCLName}}",p_oOrderItemDetailsVO.getCardLname());
	
			double dQty=Double.parseDouble(p_oOrderItemDetailsVO.getQty());
			double dSbhPrice=Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
	
			hmTags.put("{{OrderItemId}}",p_oOrderItemDetailsVO.getOrderItemId());
			hmTags.put("{{PartnerName}}",p_oOrderItemDetailsVO.getVendorName());
			hmTags.put("{{ProdCode}}",p_oOrderItemDetailsVO.getProdCode());
			hmTags.put("{{ProdTitle}}",p_oOrderItemDetailsVO.getItemName());
			hmTags.put("{{Category}}",p_oOrderItemDetailsVO.getCateId());
			hmTags.put("{{Status}}","Pending");
			hmTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
			hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice())));
			hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
	
			dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;		
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));
			hmTags.put("{{OrderNumber}}",lOrderNumber+"");
			hmTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getCustServicePhone()));
			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE"))
				hmTags.put("{{TravelDate}}",p_oOrderItemDetailsVO.getTravelDate());		
			else
				hmTags.put("{{TravelDate}}","N/A");
			
			if(oEmailVO != null) {
				sEmailContent = oEmailVO.getEmailContent();
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailBccAddr = oEmailVO.getEmailBccList();
			}
			if(sEmailBccAddr != null && sEmailBccAddr.trim().length() > 0) {
				sEmailBccAddr = sAdminEmailAddr+","+sEmailBccAddr;
			}else {
				sEmailBccAddr = sAdminEmailAddr;
			}
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
		}
		sCustomerEmailAddr = p_oOrderItemDetailsVO.getCustEmail();
		try {
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("OrderDAO::sendOrderChargeBackRejectEmail::EXCEPTION");
			throw exception;
		}
		sFromEmailAddr=System.getProperty("email.from.address").trim();
		sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
		
		oEmailLogVo = new EmailLogVO();
		oEmail = new Email();
		
		oEmailLogVo.setEmailFrom(sFromEmailAddr);
		oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sCustomerEmailAddr));
		oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sEmailBccAddr));
		oEmailLogVo.setEmailSubject(sEmailSubject);
		oEmailLogVo.setEmailText(sEmailContent);
		try {
			bEmailSent = oEmail.sendEmail(oEmailLogVo);
		}catch(Exception e) {
			e.printStackTrace();
			bEmailSent = false;
		}
		
		if(bEmailSent)				
			oEmailLogVo.setEmailStatus("Y");
		else
			oEmailLogVo.setEmailStatus("N");				
		/*oEmailLogVo.setOwnerType("customer");		
		oEmailLogVo.setOwnerId(Long.parseLong(sCustomerId));*/
		oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
		oEmailLogVo.setEmailToAddr(sCustomerEmailAddr);
		oEmailLogVo.setEmailBccAddr(sEmailBccAddr);
		EmailDAO.insertEmailLogDetails(oEmailLogVo, p_sLoginId, p_sReqeustFrom,"");
		
		logger.info("OrderDAO::sendOrderChargeBackRejectEmail::EXIT");
		return bEmailSent;
	}
	
	
	public static ArrayList getAirlineAdvertisement(SearchOrderForm p_oSearchForm,String p_sOwnerType,String p_sOwnerId) throws Exception{	
		logger.info("OrderDAO::getAirlineAdvertisement::ENTER");
		
		ArrayList alAirlineAdvt = new ArrayList();
		AirlineAdvtVO oAirlineAdvtVO=null;
		String sDBData = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_air_advt_by_ownertype(?,?,?,?,?,?)}";
		logger.info("OrderDAO::getAirlineAdvertisement::SQL QUERY "+sQry);
		logger.info("OrderDAO::getAirlineAdvertisement::Search BY "+p_oSearchForm.getSearchBy());	
		logger.info("OrderDAO::getAirlineAdvertisement::Search Value "+p_oSearchForm.getSearchValue());
		logger.info("OrderDAO::getAirlineAdvertisement::Order From Date "+p_oSearchForm.getOrderFromDate());
		logger.info("OrderDAO::getAirlineAdvertisement::Order To Date "+p_oSearchForm.getOrderToDate());
		logger.info("OrderDAO::getAirlineAdvertisement::Owner Type "+p_sOwnerType);	
		logger.info("OrderDAO::getAirlineAdvertisement::Owner Id "+p_sOwnerId);	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_oSearchForm.getSearchBy());	
			cstmt.setString(2, p_oSearchForm.getSearchValue());
			cstmt.setString(3, p_oSearchForm.getOrderFromDate());
			cstmt.setString(4, p_oSearchForm.getOrderToDate());
			cstmt.setString(5, p_sOwnerType);	
			cstmt.setString(6, p_sOwnerId);	
			rs=cstmt.executeQuery();
			while(rs.next()){				
				oAirlineAdvtVO = new AirlineAdvtVO();

				sDBData = rs.getString("advt_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setAdvtId(sDBData);
				
				sDBData = rs.getString("owner_type");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setOwnerType(sDBData);
				
				sDBData = rs.getString("owner_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setOwnerId(sDBData);
				
				sDBData = rs.getString("catg_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setCatgId(sDBData);
				
				sDBData = rs.getString("prod_code");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setProdCode(sDBData);
				
				sDBData = rs.getString("cust_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setCustName(sDBData);
				
				sDBData = rs.getString("cust_email");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setCustEmail(sDBData);
				
				sDBData = rs.getString("cust_phone");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setCustPhone(getPhoneFormat(sDBData));
				
				sDBData = rs.getString("flight_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setFlightNo(sDBData);
				
				sDBData = rs.getString("air_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setAirId(sDBData);
				
				sDBData = rs.getString("owner_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setAirName(sDBData);
				
				sDBData = rs.getString("order_dt");
				sDBData = sDBData==null?"":sDBData.trim();
				oAirlineAdvtVO.setOrderDt(sDBData);
				
				
				 
				alAirlineAdvt.add(oAirlineAdvtVO);
			}
			logger.info("OrderDAO::getAirlineAdvertisement::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::getAirlineAdvertisement::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alAirlineAdvt;
	}
	
	public static byte[] getOrderItemImage(String p_sOrderItemId) throws Exception{	
		logger.info("OrderDAO::getOrderItemImage::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		byte[] baPdf = null;
		String sQry="{call usp_sbh_get_order_item_image(?)}";
		logger.info("OrderDAO::getOrderItemImage::SQL QUERY "+sQry);
		logger.info("OrderDAO::getOrderItemImage::Order Item Id: "+p_sOrderItemId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderItemId);	
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){				
					baPdf = rs.getBytes("image");
				}
			}
			logger.info("OrderDAO::getOrderItemImage::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("OrderDAO::getOrderItemImage::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return baPdf;
	}
	

	public static boolean getOrderReport(String p_sSearchType,String p_sSearchValue, String p_sFromDate, String p_sToDate,String  p_sMonth ,String p_sYear,String p_sLoginUser,String p_sUserId ) throws Exception{ //SearchOrderForm p_oSearchForm,String p_sOwnerType,String p_sOwnerId	
		logger.info("OrderDAO::getOrderReport::ENTER");		
		boolean bIsExist = false;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		String sQry="{call usp_sbh_get_order_report_birt(?,?,?,?,?,?,?,?)}";
		String sCanceledQry="{call usp_sbh_get_order_report_for_canceled_birt(?,?,?,?,?,?,?,?)}";
		
		logger.debug("OrderDAO::getOrderReport:SQL QUERY "+sQry);
		logger.debug("OrderDAO::getOrderReport:Search Type "+p_sSearchType);	
		logger.debug("OrderDAO::getOrderReport:Search Value "+p_sSearchValue);
		logger.debug("OrderDAO::getOrderReport:Month "+ p_sMonth);
		logger.debug("OrderDAO::getOrderReport:Year "+p_sYear);
		logger.debug("OrderDAO::getOrderReport:From Date "+p_sFromDate);
		logger.debug("OrderDAO::getOrderReport:To Date "+p_sToDate);
		logger.debug("OrderDAO::getOrderReport:Login User "+p_sLoginUser);	
		logger.debug("OrderDAO::getOrderReport:User Id "+p_sUserId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sSearchType);	
			cstmt.setString(2, p_sSearchValue);
			cstmt.setString(3, p_sMonth);
			cstmt.setString(4, p_sYear);
			cstmt.setString(5, p_sFromDate);
			cstmt.setString(6, p_sToDate);
			cstmt.setString(7, p_sLoginUser);	
			cstmt.setString(8, p_sUserId);	
			rs=cstmt.executeQuery();
			while(rs.next()){				
				bIsExist = true;
				break;
			}
			
			
			cstmt=con.prepareCall(sCanceledQry);
			cstmt.setString(1, p_sSearchType);	
			cstmt.setString(2, p_sSearchValue);
			cstmt.setString(3, p_sMonth);
			cstmt.setString(4, p_sYear);
			cstmt.setString(5, p_sFromDate);
			cstmt.setString(6, p_sToDate);
			cstmt.setString(7, p_sLoginUser);	
			cstmt.setString(8, p_sUserId);	
			rs=cstmt.executeQuery();
			while(rs.next()){				
				bIsExist = true;
				break;
			}
			
			
			
			logger.info("OrderDAO::getOrderReport:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getOrderReport:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return bIsExist;
	}
	
	
	public static boolean getAirlineCommReport(String p_sAirId, String p_sFromDate, String p_sToDate,String  p_sMonth ,String p_sYear,String p_sLoginUser,String p_sUserId ) throws Exception{ //SearchOrderForm p_oSearchForm,String p_sOwnerType,String p_sOwnerId	
		logger.info("OrderDAO::getAirlineCommReport::ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		boolean bIsExist = false;
		String sQry="{call usp_sbh_get_air_comm_report_birt(?,?,?,?,?,?,?)}";
		String sCanceledQry="{call usp_sbh_get_canceled_air_comm_report_birt(?,?,?,?,?,?,?)}";
		logger.debug("OrderDAO::getAirlineCommReport:SQL QUERY "+sQry);
		logger.debug("OrderDAO::getAirlineCommReport:CANCELED QUERY "+sCanceledQry);
		logger.debug("OrderDAO::getAirlineCommReport:Air Id "+p_sAirId);	
		logger.debug("OrderDAO::getAirlineCommReport:Month "+p_sMonth);
		logger.debug("OrderDAO::getAirlineCommReport:Year "+p_sYear);
		logger.debug("OrderDAO::getAirlineCommReport:From Date "+ p_sFromDate);
		logger.debug("OrderDAO::getAirlineCommReport:To Date "+p_sToDate);
		logger.debug("OrderDAO::getAirlineCommReport:Login User "+p_sLoginUser);	
		logger.debug("OrderDAO::getAirlineCommReport:User Id "+p_sUserId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sAirId);	
			cstmt.setString(2, p_sMonth);
			cstmt.setString(3, p_sYear);
			cstmt.setString(4, p_sFromDate);
			cstmt.setString(5, p_sToDate);
			cstmt.setString(6, p_sLoginUser);	
			cstmt.setString(7, p_sUserId);	
			rs=cstmt.executeQuery();
			while(rs.next()){				
				bIsExist = true;
				break;
				
			}
			
			cstmt=con.prepareCall(sCanceledQry);
			cstmt.setString(1, p_sAirId);	
			cstmt.setString(2, p_sMonth);
			cstmt.setString(3, p_sYear);
			cstmt.setString(4, p_sFromDate);
			cstmt.setString(5, p_sToDate);
			cstmt.setString(6, p_sLoginUser);	
			cstmt.setString(7, p_sUserId);	
			rs=cstmt.executeQuery();
			while(rs.next()){				
				bIsExist = true;
				break;
				
			}
			
			logger.info("OrderDAO::getAirlineCommReport:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getAirlineCommReport:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return bIsExist;
	}
	
	
	public static ArrayList getAllAirlineNames() throws Exception{	
		logger.info("OrderDAO::getAllAirlineNames::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDBData = null;
		AirlineVO oAirlineVO = null;
		ArrayList alAirline = new ArrayList();
		String sQry="{call usp_sbh_get_all_airline_details}";
		logger.debug("OrderDAO::getAllAirlineNames::SQL QUERY "+sQry);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){				
					oAirlineVO = new AirlineVO();
					
					sDBData = rs.getString("air_id");
					sDBData = sDBData == null?"":sDBData.trim();
					oAirlineVO.setAirId(sDBData);
					
					sDBData = rs.getString("air_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oAirlineVO.setAirName(sDBData);
					
					alAirline.add(oAirlineVO);
				}
			}
			logger.info("OrderDAO::getAllAirlineNames::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getAllAirlineNames::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alAirline;
	}
	
	public static ArrayList getAllVendorNames() throws Exception{	
		logger.info("OrderDAO::getAllVendorNames::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDBData = null;
		VendorVO oVendorVO = null;
		ArrayList alVendor = new ArrayList();
		String sQry="{call usp_sbh_get_all_vendor_details}";
		logger.debug("OrderDAO::getAllVendorNames::SQL QUERY "+sQry);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){				
					oVendorVO = new VendorVO();
					
					sDBData = rs.getString("vend_id");
					sDBData = sDBData == null?"":sDBData.trim();
					oVendorVO.setVendId(sDBData);
					
					sDBData = rs.getString("vend_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oVendorVO.setVendName(sDBData);
					
					alVendor.add(oVendorVO);
				}
			}
			logger.info("OrderDAO::getAllVendorNames::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getAllVendorNames::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alVendor;
	}
	
	public static OrderItemDetailsVO getProductDetails(String p_sProdId) throws Exception {
		logger.info("OrderDAO::getProductDetails::ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		OrderItemDetailsVO oOrderItemDetailsVO = null;
		String sQry="{call usp_sbh_get_product_details(?)}";
		logger.debug("OrderDAO::getProductDetails::SQL QUERY "+sQry);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProdId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs!=null){
				while(rs.next()){
					oOrderItemDetailsVO = new OrderItemDetailsVO();
					oOrderItemDetailsVO.setSize(rs.getString("size"));
					oOrderItemDetailsVO.setProdCode(rs.getString("prod_code"));
					oOrderItemDetailsVO.setOwnerType(rs.getString("owner_type"));
					oOrderItemDetailsVO.setBrandName(rs.getString("brand_name"));
					oOrderItemDetailsVO.setItemName(rs.getString("prod_title"));
					oOrderItemDetailsVO.setColor(rs.getString("color"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("OrderDAO::getProductDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("OrderDAO::getProductDetails::EXIT");
		return oOrderItemDetailsVO;
	}
	public static String replaceTagValues(HashMap p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	
	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}	
	public static double round(double d, int decimalPlace){
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	public static float round(float Rval, int Rpl) {
		float p = (float)Math.pow(10,Rpl);
		Rval = Rval * p;
		float tmp = Math.round(Rval);
		return (float)tmp/p;
	}
	public static String getMonth(String p_sExpDate) {
		String month="";

		if(p_sExpDate != null) {
			int startIndex=p_sExpDate.indexOf("/");
			month=p_sExpDate.substring(0,startIndex);
		}

		return month;
	}
	public static String getYear(String p_sExpDate) {
		String year="";

		if(p_sExpDate != null) {
			int startIndex=p_sExpDate.indexOf("/");
			year=p_sExpDate.substring(startIndex+1, p_sExpDate.trim().length());
		}

		return year;
	}
	
}
