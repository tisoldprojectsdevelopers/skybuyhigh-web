package com.sbh.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.util.DBConnection;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;

public class EmailDAO {

	private static Logger logger = LogManager.getLogger(EmailDAO.class);
	
	
	public static EmailVO getEmailContentDetails(String p_sEmailTemplateId) throws Exception{	
		logger.info("EmailDAO::getEmailContentDetails:ENTER");		
		EmailVO oEmailVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		String sQry="{call usp_sbh_get_email_template(?)}";
		logger.debug("EmailDAO::getEmailContentDetails:SQL QUERY "+sQry);
		logger.debug("EmailDAO::getEmailContentDetails:Email Template Id : "+p_sEmailTemplateId);			
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sEmailTemplateId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oEmailVO=new EmailVO();
				oEmailVO.setEmailSubject(rs.getString("email_subject"));
				oEmailVO.setEmailContent(rs.getString("email_content"));
				oEmailVO.setEmailOptionalContent(rs.getString("email_optional_content"));	
				
				String sCcList = rs.getString("email_cc_list");
				sCcList = sCcList ==null?"":sCcList.trim();
				oEmailVO.setEmailCcList(sCcList);
				
				String sBccList = rs.getString("email_bcc_list");
				sBccList = sBccList ==null?"":sBccList.trim();
				oEmailVO.setEmailBccList(sBccList);
				
				String sAdminEmailAddress = rs.getString("admin_email_address");
				sAdminEmailAddress = sAdminEmailAddress ==null?"":sAdminEmailAddress.trim();
				oEmailVO.setAdminEmailAddress(sAdminEmailAddress);
				
			}
			logger.info("EmailDAO::getEmailContentDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailDAO::getEmailContentDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		return oEmailVO;
	}
	public static void insertEmailLogDetails(EmailLogVO p_oEmailLogVo, String p_sLoginId, String p_sRequestFrom,String p_sDeviceId) throws Exception{	
		logger.info("EmailDAO::insertEmailLogDetails:ENTER");		
		EmailVO oEmailVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		String sQry="{call usp_sbh_add_email_log(?,?,?,?,?,?,?,?,?,?,?)}";
		logger.debug("EmailDAO::insertEmailLogDetails:SQL QUERY "+sQry);
				
		logger.debug("EmailDAO::insertEmailLogDetails:Email From: "+p_oEmailLogVo.getEmailFrom());
		logger.debug("EmailDAO::insertEmailLogDetails:Email To: "+p_oEmailLogVo.getEmailToAddr());
		logger.debug("EmailDAO::insertEmailLogDetails:Email CC Addr: "+p_oEmailLogVo.getEmailCcAddr());
		logger.debug("EmailDAO::insertEmailLogDetails:Email Subject: "+p_oEmailLogVo.getEmailSubject());
		logger.debug("EmailDAO::insertEmailLogDetails:Email Subject: "+p_oEmailLogVo.getEmailStatus());
		logger.debug("EmailDAO::insertEmailLogDetails:Email Template Id: "+p_oEmailLogVo.getEmailTemplateId());
		logger.debug("EmailDAO::insertEmailLogDetails:Login Id: "+p_sLoginId);
		logger.debug("EmailDAO::insertEmailLogDetails:Request From: "+p_sRequestFrom);
		logger.debug("EmailDAO::insertEmailLogDetails:Device Id: "+p_sDeviceId);
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oEmailLogVo.getEmailFrom());
			cstmt.setString(2,p_oEmailLogVo.getEmailToAddr());
			cstmt.setString(3,p_oEmailLogVo.getEmailCcAddr());
			cstmt.setString(4,p_oEmailLogVo.getEmailSubject());
			cstmt.setString(5,p_oEmailLogVo.getEmailText());
			cstmt.setString(6,p_oEmailLogVo.getEmailStatus());
			cstmt.setString(7,p_oEmailLogVo.getEmailTemplateId());
			cstmt.setBytes(8,p_oEmailLogVo.getEmailAttachment());
			cstmt.setString(9,p_sLoginId);
			cstmt.setString(10,p_sRequestFrom);
			cstmt.setString(11,p_sDeviceId);
			
			cstmt.execute();			
			logger.info("EmailDAO::insertEmailLogDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailDAO::insertEmailLogDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
						
		}
		
	}
}
