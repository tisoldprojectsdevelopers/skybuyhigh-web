package com.sbh.dao;


import static com.sbh.util.Utils.convertToNameFormat;
import static com.sbh.util.Utils.dateAndTimeStampConversion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.forms.AirlineRegForm;
import com.sbh.forms.SearchAirlineForm;
import com.sbh.util.DBConnection;
import com.sbh.vo.AirlineRegVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerRegistrationCommentsVO;
public class AirlineDAO {
	private static Logger logger = LogManager.getLogger(AirlineDAO.class);
	
	public static int addAirlineDetails(AirlineRegForm p_oAirlineRegForm, String p_sLoginId, String p_sRefId) throws Exception{	
		logger.info("AirlineDAO::addAirlineDetails:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_airline_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		logger.debug("AirlineDAO::addAirlineDetails:SQL QUERY "+sQry);			
		logger.debug("AirlineDAO::addAirlineDetails:Vendor Id: "+p_oAirlineRegForm.getAirName());		
		logger.debug("AirlineDAO::addAirlineDetails:Airline Contact Name: "+p_oAirlineRegForm.getAirContactName());	
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline User Id: "+p_oAirlineRegForm.getAirUserId().trim());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Addr1 "+p_oAirlineRegForm.getAirAddr1());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Addr2 "+p_oAirlineRegForm.getAirAddr2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline City "+p_oAirlineRegForm.getAirCity());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline State "+p_oAirlineRegForm.getAirState());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Country "+p_oAirlineRegForm.getAirCountry());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Zip "+p_oAirlineRegForm.getAirZip());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Phone "+p_oAirlineRegForm.getAirPhone1());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Phone Ext 1 "+p_oAirlineRegForm.getAirPhoneExt1());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Phone 2 "+p_oAirlineRegForm.getAirPhone2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Phone Ext 2 "+p_oAirlineRegForm.getAirPhoneExt2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Email "+p_oAirlineRegForm.getAirEmail());	
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Fax "+p_oAirlineRegForm.getAirFax());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Comments "+p_oAirlineRegForm.getAirComments());	
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Return Policy "+p_oAirlineRegForm.getAirReturnPolicy());	
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Contact Name 2 "+p_oAirlineRegForm.getAirContactName2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Customer Service Email "+p_oAirlineRegForm.getCustServiceEmail());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Customer Service Phone "+p_oAirlineRegForm.getCustServicePhoneno());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Customer Service Phone Ext "+p_oAirlineRegForm.getCustServicePhonenoext());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Mobile1 "+p_oAirlineRegForm.getAirMobile1());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Mobile2 "+p_oAirlineRegForm.getAirMobile2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Email2 "+p_oAirlineRegForm.getAirEmail2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Airline Status "+p_oAirlineRegForm.getAirStatus());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Charge Type "+p_oAirlineRegForm.getChargeType());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return Addr1 "+p_oAirlineRegForm.getReturnAddr1());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return Addr2 "+p_oAirlineRegForm.getReturnAddr2());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return City "+p_oAirlineRegForm.getReturnCity());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return State "+p_oAirlineRegForm.getReturnState());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return Country "+p_oAirlineRegForm.getReturnCountry());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return Zip "+p_oAirlineRegForm.getReturnZip());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Return Days "+p_oAirlineRegForm.getReturnDays());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Login Id "+p_sLoginId);
		logger.debug("AirlineDAO::addAirlineDetails:Key Reference Id "+p_sRefId);	
			
		
		int iAirId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.setString(2,p_oAirlineRegForm.getAirName().trim());
			cstmt.setString(3,CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName()));
			cstmt.setString(4,p_oAirlineRegForm.getAirUserId().trim());
			cstmt.setString(5,p_oAirlineRegForm.getAirPassword().trim());
			cstmt.setString(6,p_oAirlineRegForm.getAirAddr1());
			cstmt.setString(7,p_oAirlineRegForm.getAirAddr2());
			cstmt.setString(8,p_oAirlineRegForm.getAirCity());
			cstmt.setString(9,p_oAirlineRegForm.getAirState());
			cstmt.setString(10,p_oAirlineRegForm.getAirCountry());
			cstmt.setString(11,p_oAirlineRegForm.getAirZip());
			cstmt.setString(12,p_oAirlineRegForm.getAirPhone1());
			cstmt.setString(13,p_oAirlineRegForm.getAirPhoneExt1());
			cstmt.setString(14,p_oAirlineRegForm.getAirPhone2());
			cstmt.setString(15,p_oAirlineRegForm.getAirPhoneExt2());
			cstmt.setString(16,p_oAirlineRegForm.getAirEmail());	
			cstmt.setString(17,p_oAirlineRegForm.getAirFax());
			cstmt.setString(18,p_oAirlineRegForm.getAirComments());	
			cstmt.setString(19,p_oAirlineRegForm.getAirReturnPolicy());	
			cstmt.setString(20,p_oAirlineRegForm.getAirContactName2());
			cstmt.setString(21,p_oAirlineRegForm.getCustServiceEmail());
			cstmt.setString(22,p_oAirlineRegForm.getCustServicePhoneno());
			cstmt.setString(23,p_oAirlineRegForm.getCustServicePhonenoext());
			cstmt.setString(24,p_oAirlineRegForm.getAirMobile1());
			cstmt.setString(25,p_oAirlineRegForm.getAirMobile2());
			cstmt.setString(26,p_oAirlineRegForm.getAirEmail2());
			cstmt.setString(27,p_oAirlineRegForm.getAirStatus());
			cstmt.setString(28,p_oAirlineRegForm.getChargeType());
			cstmt.setString(29,p_oAirlineRegForm.getReturnAddr1());
			cstmt.setString(30,p_oAirlineRegForm.getReturnAddr2());
			cstmt.setString(31,p_oAirlineRegForm.getReturnCity());
			cstmt.setString(32,p_oAirlineRegForm.getReturnState());
			cstmt.setString(33,p_oAirlineRegForm.getReturnCountry());
			cstmt.setString(34,p_oAirlineRegForm.getReturnZip());
			cstmt.setString(35,p_oAirlineRegForm.getReturnDays());
			cstmt.setString(36,p_oAirlineRegForm.getPoPct());
			cstmt.setString(37,p_oAirlineRegForm.getAirCommPct());
			cstmt.setString(38,p_sLoginId);
			cstmt.setString(39,p_sRefId);
			cstmt.execute();
			iAirId=cstmt.getInt(1);
			
			logger.info("AirlineDAO::addVendorDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::addVendorDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return iAirId;
	}

	
		public static List<AirlineRegVO> getAirlineDetails(SearchAirlineForm p_oSearchAirlineForm) throws Exception{	
		logger.info("AirlineDAO::getAirlineDetails:ENTER");		
		AirlineRegVO oAirlineRegVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_airline_details(?,?)}";
		
		logger.debug("AirlineDAO::getAirlineDetails:SQL QUERY "+sQry);
		logger.debug("AirlineDAO::getAirlineDetails:Search By "+p_oSearchAirlineForm.getAirlineSearchBy());
		logger.debug("AirlineDAO::getAirlineDetails:Search Type "+p_oSearchAirlineForm.getAirlineSearchValue());	
		
		ResultSet rs=null;
		List<AirlineRegVO> alAirlineInfo=new ArrayList<AirlineRegVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchAirlineForm.getAirlineSearchBy());
			cstmt.setString(2,p_oSearchAirlineForm.getAirlineSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oAirlineRegVO=new AirlineRegVO();
				oAirlineRegVO.setAirId(rs.getString("air_id"));
				oAirlineRegVO.setAirName(rs.getString("air_name"));
				String sAirlineContactName = rs.getString("air_contact_name1");
				sAirlineContactName = sAirlineContactName ==null?"":sAirlineContactName.trim();
				oAirlineRegVO.setAirContactName(CatalogueDAO.toTitleCase(sAirlineContactName));
				oAirlineRegVO.setAirAddr1(rs.getString("air_addr1"));	
				oAirlineRegVO.setAirCity(rs.getString("air_city"));	
				oAirlineRegVO.setAirState(rs.getString("air_state"));
				oAirlineRegVO.setAirCountry(rs.getString("air_country"));
				oAirlineRegVO.setAirZip(rs.getString("air_zip"));
				oAirlineRegVO.setAirStatus(rs.getString("air_status"));
				/*oAirlineRegVO.setAirPhone1(rs.getString("air_phone1"));
				oAirlineRegVO.setAirPhoneExt1(rs.getString("air_phone_Ext1"));
				oAirlineRegVO.setAirPhone2(rs.getString("air_phone2"));
				oAirlineRegVO.setAirPhoneExt2(rs.getString("air_phone_ext2"));
				oAirlineRegVO.setAirEmail(rs.getString("air_email1"));*/
				oAirlineRegVO.setAirFax(rs.getString("air_fax"));
				oAirlineRegVO.setAirComments(rs.getString("air_comments"));
				oAirlineRegVO.setChargeType(rs.getString("charge_type"));

				alAirlineInfo.add(oAirlineRegVO);
			}
			logger.info("AirlineDAO::getAirlineDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::getAirlineDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return alAirlineInfo;
	}
		public static AirlineRegVO getAirlineDetailsById(String p_sAirId, String p_sUserTableId) throws Exception{	
			logger.info("AirlineDAO::getAirlineDetailsById:ENTER");		
			AirlineRegVO oAirlineRegVO=null;
			
			Connection con=null;		
			CallableStatement cstmt=null;
			ResultSet rs=null;
			String sQry="{call usp_sbh_get_airline_by_id(?,?)}";
//			SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
			logger.debug("AirlineDAO::getAirlineDetailsById:SQL QUERY "+sQry);
			logger.debug("AirlineDAO::getAirlineDetailsById:Airline Code "+p_sAirId);			
			logger.debug("AirlineDAO::getAirlineDetailsById:User Table Id "+p_sUserTableId);			
		
			try{
				con = DBConnection.getSQL2005Connection();
				cstmt=con.prepareCall(sQry);
				cstmt.setString(1,p_sAirId);	
				cstmt.setString(2,p_sUserTableId);	
				cstmt.execute();
				rs = cstmt.getResultSet();
				while(rs.next()){				
					oAirlineRegVO=new AirlineRegVO();
					oAirlineRegVO.setAirId(rs.getString("air_id"));
					oAirlineRegVO.setAirName(rs.getString("air_name"));
					String sAirlineContactName = rs.getString("air_contact_name1");
					sAirlineContactName = sAirlineContactName ==null?"":sAirlineContactName.trim();
					oAirlineRegVO.setAirContactName(CatalogueDAO.toTitleCase(sAirlineContactName));
					oAirlineRegVO.setAirAddr1(rs.getString("air_addr1"));	
					oAirlineRegVO.setAirAddr2(rs.getString("air_addr2"));	
					oAirlineRegVO.setAirCity(rs.getString("air_city"));	
					oAirlineRegVO.setAirState(rs.getString("air_state"));
					oAirlineRegVO.setAirCountry(rs.getString("air_country"));
					oAirlineRegVO.setAirZip(rs.getString("air_zip"));
					oAirlineRegVO.setAirPhone1(rs.getString("air_phone1"));
					oAirlineRegVO.setAirPhoneExt1(rs.getString("air_phone_ext1"));
					oAirlineRegVO.setAirPhone2(rs.getString("air_phone2"));
					oAirlineRegVO.setAirPhoneExt2(rs.getString("air_phone_ext2"));				
					oAirlineRegVO.setAirEmail(rs.getString("air_email1"));
					oAirlineRegVO.setAirFax(rs.getString("air_fax"));
					oAirlineRegVO.setAirComments(rs.getString("air_comments"));			
					oAirlineRegVO.setAirUserId(rs.getString("user_id"));			
					oAirlineRegVO.setAirPassword(rs.getString("password"));		
					oAirlineRegVO.setAirReturnPolicy(rs.getString("air_return_policy"));
					oAirlineRegVO.setAirStatus(rs.getString("air_status"));
					oAirlineRegVO.setKeyRefId(rs.getString("key_ref_id"));
					String sAirlineContactNamee = rs.getString("air_contact_name2");
					sAirlineContactNamee = sAirlineContactNamee ==null?"":sAirlineContactNamee.trim();
					oAirlineRegVO.setAirContactName2(CatalogueDAO.toTitleCase(sAirlineContactNamee));
					
					oAirlineRegVO.setCustServiceEmail(rs.getString("cust_service_email"));
					oAirlineRegVO.setCustServicePhoneno(rs.getString("cust_service_phoneno"));
					oAirlineRegVO.setCustServicePhonenoext(rs.getString("cust_service_phoneno_ext"));
					
					oAirlineRegVO.setAirMobile1(rs.getString("air_mobile1"));
					oAirlineRegVO.setAirMobile2(rs.getString("air_mobile2"));
					oAirlineRegVO.setAirEmail2(rs.getString("air_email2"));
					
					oAirlineRegVO.setChargeType(rs.getString("charge_type"));
					
					oAirlineRegVO.setReturnAddr1(rs.getString("air_return_addr1"));	
					oAirlineRegVO.setReturnAddr2(rs.getString("air_return_addr2"));	
					oAirlineRegVO.setReturnCity(rs.getString("air_return_city"));
					oAirlineRegVO.setReturnState(rs.getString("air_return_state"));
					oAirlineRegVO.setReturnCountry(rs.getString("air_return_country"));
					oAirlineRegVO.setReturnZip(rs.getString("air_return_zip"));
					oAirlineRegVO.setReturnDays(rs.getString("air_return_days"));
					
					oAirlineRegVO.setAirlineLogoPath(rs.getString("air_logo_path"));
					oAirlineRegVO.setAirlineLogoType(rs.getString("air_logo_type"));
					
					String sPoPct = rs.getString("po_pct");
					sPoPct = sPoPct ==null?"":sPoPct.trim();
					oAirlineRegVO.setPoPct(sPoPct);
					
					String sCommPct = rs.getString("air_comm_pct");
					sCommPct = sCommPct ==null?"":sCommPct.trim();
					oAirlineRegVO.setAirCommPct(sCommPct);
					
					String sAboutMePath = rs.getString("about_me_path");
					sAboutMePath =sAboutMePath ==null?"":sAboutMePath.trim();
					oAirlineRegVO.setAboutMePath(sAboutMePath);
					
					String sAboutMeType = rs.getString("about_me_type");
					sAboutMeType =sAboutMeType ==null?"":sAboutMeType.trim();
					oAirlineRegVO.setAboutMeType(sAboutMeType);
				}
				logger.info("AirlineDAO::getAirlineDetailsById:EXIT");
			}catch(Exception e){
				e.printStackTrace();
				logger.error("AirlineDAO::getAirlineDetailsById:Exception "+e.getMessage());
				throw e;
			}
			finally{
				if(con!=null)
					con.close();
				if(cstmt!=null)
					cstmt.close();
				if(rs!=null)
					rs.close();
				
			}
			return oAirlineRegVO;
		}
		public static int updateAirlineDetails(AirlineRegForm p_oAirlineRegForm,String p_sUsertype, String p_sLoginUserId, String p_sUserTableId, String p_sRefId) throws Exception{	
		logger.info("AirlineDAO::updateAirlineDetails:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_airline_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		
		logger.debug("AirlineDAO::updateAirlineDetails:SQL QUERY "+sQry);
		logger.debug("AirlineDAO::updateAirlineDetails:Air Id "+p_oAirlineRegForm.getAirId());	
		logger.debug("AirlineDAO::updateAirlineDetails:Air Name "+p_oAirlineRegForm.getAirName());		
		logger.debug("AirlineDAO::updateAirlineDetails:Air Contact Name "+p_oAirlineRegForm.getAirContactName());		
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline User Id: "+p_oAirlineRegForm.getAirUserId().trim());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Addr1 "+p_oAirlineRegForm.getAirAddr1());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Addr2 "+p_oAirlineRegForm.getAirAddr2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline City "+p_oAirlineRegForm.getAirCity());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline State "+p_oAirlineRegForm.getAirState());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Country "+p_oAirlineRegForm.getAirCountry());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Zip "+p_oAirlineRegForm.getAirZip());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Phone "+p_oAirlineRegForm.getAirPhone1());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Phone Ext 1 "+p_oAirlineRegForm.getAirPhoneExt1());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Phone 2 "+p_oAirlineRegForm.getAirPhone2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Phone Ext 2 "+p_oAirlineRegForm.getAirPhoneExt2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Email "+p_oAirlineRegForm.getAirEmail());	
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Fax "+p_oAirlineRegForm.getAirFax());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Comments "+p_oAirlineRegForm.getAirComments());	
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Return Policy "+p_oAirlineRegForm.getAirReturnPolicy());	
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Contact Name 2 "+p_oAirlineRegForm.getAirContactName2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Customer Service Email "+p_oAirlineRegForm.getCustServiceEmail());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Customer Service Phone "+p_oAirlineRegForm.getCustServicePhoneno());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Customer Service Phone Ext "+p_oAirlineRegForm.getCustServicePhonenoext());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Mobile1 "+p_oAirlineRegForm.getAirMobile1());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Mobile2 "+p_oAirlineRegForm.getAirMobile2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Email2 "+p_oAirlineRegForm.getAirEmail2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Airline Status "+p_oAirlineRegForm.getAirStatus());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Charge Type "+p_oAirlineRegForm.getChargeType());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return Addr1 "+p_oAirlineRegForm.getReturnAddr1());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return Addr2 "+p_oAirlineRegForm.getReturnAddr2());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return City "+p_oAirlineRegForm.getReturnCity());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return State "+p_oAirlineRegForm.getReturnState());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return Country "+p_oAirlineRegForm.getReturnCountry());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return Zip "+p_oAirlineRegForm.getReturnZip());
		logger.debug("AirlineDAO::updateAirlineDetails:Airline Return Days "+p_oAirlineRegForm.getReturnDays());
		logger.debug("AirlineDAO::addAirlineDetails:Airline Login Id "+p_sLoginUserId);
		logger.debug("AirlineDAO::addAirlineDetails:Key Reference Id "+p_sRefId);
		
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oAirlineRegForm.getAirName().trim());
			cstmt.setString(3,CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName()));			
			cstmt.setString(4,p_oAirlineRegForm.getAirAddr1());
			cstmt.setString(5,p_oAirlineRegForm.getAirAddr2());
			cstmt.setString(6,p_oAirlineRegForm.getAirCity());
			cstmt.setString(7,p_oAirlineRegForm.getAirState());
			cstmt.setString(8,p_oAirlineRegForm.getAirCountry());
			cstmt.setString(9,p_oAirlineRegForm.getAirZip());
			cstmt.setString(10,p_oAirlineRegForm.getAirPhone1());
			cstmt.setString(11,p_oAirlineRegForm.getAirPhoneExt1());
			cstmt.setString(12,p_oAirlineRegForm.getAirPhone2());
			cstmt.setString(13,p_oAirlineRegForm.getAirPhoneExt2());			
			cstmt.setString(14,p_oAirlineRegForm.getAirEmail());	
			cstmt.setString(15,p_oAirlineRegForm.getAirFax());
			cstmt.setString(16,p_oAirlineRegForm.getAirComments());	
			cstmt.setString(17,p_oAirlineRegForm.getAirId());	
			cstmt.setString(18,p_oAirlineRegForm.getAirUserId());	
			cstmt.setString(19,p_oAirlineRegForm.getAirPassword().trim());	
			cstmt.setString(20,p_sUsertype);
			cstmt.setString(21,p_oAirlineRegForm.getAirReturnPolicy());
			cstmt.setString(22,CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName2()));
			cstmt.setString(23,p_oAirlineRegForm.getCustServiceEmail());
			cstmt.setString(24,p_oAirlineRegForm.getCustServicePhoneno());
			cstmt.setString(25,p_oAirlineRegForm.getCustServicePhonenoext());
			cstmt.setString(26,p_oAirlineRegForm.getAirMobile1());
			cstmt.setString(27,p_oAirlineRegForm.getAirMobile2());
			cstmt.setString(28,p_oAirlineRegForm.getAirEmail2());
			cstmt.setString(29,p_oAirlineRegForm.getAirStatus());
			cstmt.setString(30,p_oAirlineRegForm.getChargeType());
			cstmt.setString(31,p_oAirlineRegForm.getReturnAddr1());
			cstmt.setString(32,p_oAirlineRegForm.getReturnAddr2());
			cstmt.setString(33,p_oAirlineRegForm.getReturnCity());
			cstmt.setString(34,p_oAirlineRegForm.getReturnState());
			cstmt.setString(35,p_oAirlineRegForm.getReturnCountry());
			cstmt.setString(36,p_oAirlineRegForm.getReturnZip());
			cstmt.setString(37,p_oAirlineRegForm.getReturnDays());
			cstmt.setString(38,p_oAirlineRegForm.getPoPct());
			cstmt.setString(39,p_oAirlineRegForm.getAirCommPct());
			cstmt.setString(40,p_sLoginUserId);
			cstmt.setString(41,p_sUserTableId);
			cstmt.setString(42,p_sRefId);
			
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);
			
			logger.info("AirlineDAO::updateAirlineDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::updateAirlineDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return iIsUpdate;
	}
	public static ArrayList validateAirlineCode(String p_sAirName,String p_sAirUserId,String p_sAirId,String p_sMode) throws Exception{	
		logger.info("AirlineDAO::validateAirlineCode:ENTER");		
			
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
//		int iDeviceIdCount = -1;
		ArrayList alAirlineRegStatus = new ArrayList();
		
		String sQry="{call usp_sbh_validate_airline(?,?,?,?,?,?)}";
	
		logger.debug("AirlineDAO::validateAirlineCode:SQL QUERY "+sQry);	
		logger.debug("AirlineDAO::validateAirlineCode:Air Name:"+p_sAirName);		
		logger.debug("AirlineDAO::validateAirlineCode:Air User Id:"+p_sAirUserId);	
		logger.debug("AirlineDAO::validateAirlineCode:Air Id:"+p_sAirId);	
		logger.debug("AirlineDAO::validateAirlineCode:Mode:"+p_sMode);	
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);	
			cstmt.registerOutParameter(2, Types.INTEGER);	
			cstmt.setString(3,p_sAirName);	
			cstmt.setString(4,p_sAirUserId);	
			cstmt.setString(5,p_sAirId);	
			cstmt.setString(6,p_sMode);	
			cstmt.execute();
			
			alAirlineRegStatus.add(0,cstmt.getInt(1));
			alAirlineRegStatus.add(1,cstmt.getInt(2));
			
			
			logger.info("AirlineDAO::validateAirlineCode:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::validateAirlineCode:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return alAirlineRegStatus;
	}
	public static String getInsertedAirlineDetails(AirlineRegForm p_oAirlineRegForm) throws Exception{	
		logger.info("AirlineDAO::getInsertedAirlineDetails:ENTER");			
		String sInsertedUserDetails = "";
		try{
			if(p_oAirlineRegForm!=null){
				sInsertedUserDetails = "User Id:"+p_oAirlineRegForm.getAirUserId().trim()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Password:"+p_oAirlineRegForm.getAirPassword().trim()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Name:"+p_oAirlineRegForm.getAirName()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName())+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Charge Type:"+p_oAirlineRegForm.getChargeType()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Airline Status:"+p_oAirlineRegForm.getAirStatus()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Company Address:"+p_oAirlineRegForm.getAirAddr1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Additonal Address:"+p_oAirlineRegForm.getAirAddr2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"City:"+p_oAirlineRegForm.getAirCity()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"State:"+p_oAirlineRegForm.getAirState()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Country:"+p_oAirlineRegForm.getAirCountry()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Zip:"+p_oAirlineRegForm.getAirZip()+"|";					
				sInsertedUserDetails = sInsertedUserDetails+"Phone1:"+p_oAirlineRegForm.getAirPhone1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Ext1:"+p_oAirlineRegForm.getAirPhoneExt1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Mobile1:"+p_oAirlineRegForm.getAirMobile1()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Email:"+p_oAirlineRegForm.getAirEmail()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Fax:"+p_oAirlineRegForm.getAirFax()+"|";
				
				sInsertedUserDetails = sInsertedUserDetails+"Contact Name2:"+p_oAirlineRegForm.getAirContactName2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Phone2:"+p_oAirlineRegForm.getAirPhone2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Ext2:"+p_oAirlineRegForm.getAirPhoneExt2()+"|";			
				sInsertedUserDetails = sInsertedUserDetails+"Mobile2:"+p_oAirlineRegForm.getAirMobile2()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"Email2:"+p_oAirlineRegForm.getAirEmail2()+"|";	
			
				sInsertedUserDetails = sInsertedUserDetails+"Return Policy:"+p_oAirlineRegForm.getAirReturnPolicy()+"|";				
				sInsertedUserDetails = sInsertedUserDetails+"Comments:"+p_oAirlineRegForm.getAirComments().trim()+"|";
			
				sInsertedUserDetails = sInsertedUserDetails+"Cust Service Phoneno:"+p_oAirlineRegForm.getCustServicePhoneno()+"|";
				sInsertedUserDetails = sInsertedUserDetails+"CustService Phoneno Ext:"+p_oAirlineRegForm.getCustServicePhonenoext()+"|";				
				sInsertedUserDetails = sInsertedUserDetails+"Cust Service Email:"+p_oAirlineRegForm.getCustServiceEmail()+"|";					


			}
			// System.out.println("AirlineDAO::getInsertedAirlineDetails:"+sInsertedUserDetails);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::getInsertedAirlineDetails:Exception "+e.getMessage());
			throw e;
		}
		

		logger.info("getInsertedAirlineDetails:EXIT");
		return sInsertedUserDetails;

	}
	
	
	public static void getUpdatedUserDetails(AirlineRegForm p_oAirlineRegForm,AirlineRegVO p_oAirlineRegVO,LoginVO p_oLoginVO, String p_sRefId) throws Exception{	
		logger.info("AirlineDAO::getUpdatedUserDetails:ENTER");			
		
		String sCurrentUserDetails = "",sPreviousUserDetails = "" ,sModifiedFields ="";		
	
		
		//get Current User details
		if(p_oAirlineRegForm!=null){
			
			
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				sCurrentUserDetails = "User Id:"+p_oAirlineRegForm.getAirUserId().trim()+"|";	
				//sCurrentUserDetails = sCurrentUserDetails+"Password:"+p_oAirlineRegForm.getAirPassword().trim()+"|";
			}
			sCurrentUserDetails = sCurrentUserDetails+"Name:"+p_oAirlineRegForm.getAirName()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName())+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Charge Type:"+p_oAirlineRegForm.getChargeType()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Airline Status:"+p_oAirlineRegForm.getAirStatus()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Company Address:"+p_oAirlineRegForm.getAirAddr1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Additonal Address:"+p_oAirlineRegForm.getAirAddr2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"City:"+p_oAirlineRegForm.getAirCity()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"State:"+p_oAirlineRegForm.getAirState()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Country:"+p_oAirlineRegForm.getAirCountry()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Zip:"+p_oAirlineRegForm.getAirZip()+"|";					
			sCurrentUserDetails = sCurrentUserDetails+"Phone1:"+p_oAirlineRegForm.getAirPhone1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Ext1:"+p_oAirlineRegForm.getAirPhoneExt1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Mobile1:"+p_oAirlineRegForm.getAirMobile1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Email:"+p_oAirlineRegForm.getAirEmail()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Fax:"+p_oAirlineRegForm.getAirFax()+"|";
			
			sCurrentUserDetails = sCurrentUserDetails+"Contact Name2:"+p_oAirlineRegForm.getAirContactName2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Phone2:"+p_oAirlineRegForm.getAirPhone2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Ext2:"+p_oAirlineRegForm.getAirPhoneExt2()+"|";			
			sCurrentUserDetails = sCurrentUserDetails+"Mobile2:"+p_oAirlineRegForm.getAirMobile2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Email2:"+p_oAirlineRegForm.getAirEmail2()+"|";	
		
			sCurrentUserDetails = sCurrentUserDetails+"Return Policy:"+p_oAirlineRegForm.getAirReturnPolicy()+"|";				
			sCurrentUserDetails = sCurrentUserDetails+"Comments:"+p_oAirlineRegForm.getAirComments().trim()+"|";
		
			sCurrentUserDetails = sCurrentUserDetails+"Cust Service Phoneno:"+p_oAirlineRegForm.getCustServicePhoneno()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"CustService Phoneno Ext:"+p_oAirlineRegForm.getCustServicePhonenoext()+"|";				
			sCurrentUserDetails = sCurrentUserDetails+"Cust Service Email:"+p_oAirlineRegForm.getCustServiceEmail()+"|";					


			sCurrentUserDetails = sCurrentUserDetails+"Return Company Address:"+p_oAirlineRegForm.getReturnAddr1()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Additonal Address:"+p_oAirlineRegForm.getReturnAddr2()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return City:"+p_oAirlineRegForm.getReturnCity()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return State:"+p_oAirlineRegForm.getReturnState()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Country:"+p_oAirlineRegForm.getReturnCountry()+"|";
			sCurrentUserDetails = sCurrentUserDetails+"Return Zip:"+p_oAirlineRegForm.getReturnZip()+"|";			
			sCurrentUserDetails = sCurrentUserDetails+"Return Days:"+p_oAirlineRegForm.getReturnDays()+"|";
			
		}
		
		//get Previous user details
		if(p_oAirlineRegVO!=null){
			
			
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				sPreviousUserDetails = "User Id:"+p_oAirlineRegVO.getAirUserId().trim()+"|";
				//sPreviousUserDetails = sPreviousUserDetails+"Password:"+p_oAirlineRegVO.getAirPassword().trim()+"|";
			}
			sPreviousUserDetails = sPreviousUserDetails+"Name:"+p_oAirlineRegVO.getAirName()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Contact Name:"+CatalogueDAO.toTitleCase(p_oAirlineRegVO.getAirContactName())+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Charge Type:"+p_oAirlineRegVO.getChargeType()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Airline Status:"+p_oAirlineRegVO.getAirStatus()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Company Address:"+p_oAirlineRegVO.getAirAddr1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Additonal Address:"+p_oAirlineRegVO.getAirAddr2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"City:"+p_oAirlineRegVO.getAirCity()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"State:"+p_oAirlineRegVO.getAirState()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Country:"+p_oAirlineRegVO.getAirCountry()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Zip:"+p_oAirlineRegVO.getAirZip()+"|";					
			sPreviousUserDetails = sPreviousUserDetails+"Phone1:"+p_oAirlineRegVO.getAirPhone1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Ext1:"+p_oAirlineRegVO.getAirPhoneExt1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Mobile1:"+p_oAirlineRegVO.getAirMobile1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Email:"+p_oAirlineRegVO.getAirEmail()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Fax:"+p_oAirlineRegVO.getAirFax()+"|";
			
			sPreviousUserDetails = sPreviousUserDetails+"Contact Name2:"+p_oAirlineRegVO.getAirContactName2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Phone2:"+p_oAirlineRegVO.getAirPhone2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Ext2:"+p_oAirlineRegVO.getAirPhoneExt2()+"|";			
			sPreviousUserDetails = sPreviousUserDetails+"Mobile2:"+p_oAirlineRegVO.getAirMobile2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Email2:"+p_oAirlineRegVO.getAirEmail2()+"|";	
		
			sPreviousUserDetails = sPreviousUserDetails+"Return Policy:"+p_oAirlineRegVO.getAirReturnPolicy()+"|";				
			sPreviousUserDetails = sPreviousUserDetails+"Comments:"+p_oAirlineRegVO.getAirComments().trim()+"|";
		
			sPreviousUserDetails = sPreviousUserDetails+"Cust Service Phoneno:"+p_oAirlineRegVO.getCustServicePhoneno()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"CustService Phoneno Ext:"+p_oAirlineRegVO.getCustServicePhonenoext()+"|";				
			sPreviousUserDetails = sPreviousUserDetails+"Cust Service Email:"+p_oAirlineRegVO.getCustServiceEmail()+"|";					

			
			sPreviousUserDetails = sPreviousUserDetails+"Return Company Address:"+p_oAirlineRegVO.getReturnAddr1()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Additonal Address:"+p_oAirlineRegVO.getReturnAddr2()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return City:"+p_oAirlineRegVO.getReturnCity()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return State:"+p_oAirlineRegVO.getReturnState()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Country:"+p_oAirlineRegVO.getReturnCountry()+"|";
			sPreviousUserDetails = sPreviousUserDetails+"Return Zip:"+p_oAirlineRegVO.getReturnZip()+"|";			
			sPreviousUserDetails = sPreviousUserDetails+"Return Days:"+p_oAirlineRegVO.getReturnDays()+"|";

		}
		
			
		//get modified fields name
		
		if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
			if(!p_oAirlineRegForm.getAirUserId().equals(p_oAirlineRegVO.getAirUserId())){		
				sModifiedFields =sModifiedFields+"User Id|";	
			}
			
			if(!p_oAirlineRegForm.getPoPct().equals(p_oAirlineRegVO.getPoPct())){		
				sModifiedFields =sModifiedFields+"PO Percentage|";	
			}
			
			if(!p_oAirlineRegForm.getAirCommPct().equals(p_oAirlineRegVO.getAirCommPct())){		
				sModifiedFields =sModifiedFields+"Commission Percentage|";	
			}
			
		}	
		if(p_oAirlineRegForm.getAirPassword()!=null){
			if(!OrderDAO.decryptInputData(p_oAirlineRegForm.getAirPassword(),"SERVER",p_sRefId).equals(p_oAirlineRegVO.getAirPassword())){		
				sModifiedFields =sModifiedFields+"Password|";	
			}
		}
		if(!p_oAirlineRegForm.getAirName().equals(p_oAirlineRegVO.getAirName())){		
			sModifiedFields =sModifiedFields+"Name|";	
		}
		if(!CatalogueDAO.toTitleCase(p_oAirlineRegForm.getAirContactName()).equals(p_oAirlineRegVO.getAirContactName())){		
			sModifiedFields =sModifiedFields+"Contact Name|";	
		}
		if(!p_oAirlineRegForm.getChargeType().equals(p_oAirlineRegVO.getChargeType())){		
			sModifiedFields =sModifiedFields+"Charge Type|";	
		}
		if(!p_oAirlineRegForm.getAirStatus().equals(p_oAirlineRegVO.getAirStatus())){		
			sModifiedFields =sModifiedFields+"Airline Status|";	
		}
		if(!p_oAirlineRegForm.getAirAddr1().equals(p_oAirlineRegVO.getAirAddr1())){		
			sModifiedFields =sModifiedFields+"Company Address|";	
		}
		if(!p_oAirlineRegForm.getAirAddr2().equals(p_oAirlineRegVO.getAirAddr2())){		
			sModifiedFields =sModifiedFields+"Additonal Address|";	
		}
		if(!p_oAirlineRegForm.getAirCity().equals(p_oAirlineRegVO.getAirCity())){		
			sModifiedFields =sModifiedFields+"City|";	
		}
		if(!p_oAirlineRegForm.getAirState().equals(p_oAirlineRegVO.getAirState())){		
			sModifiedFields =sModifiedFields+"State|";	
		}
		if(!p_oAirlineRegForm.getAirCountry().equals(p_oAirlineRegVO.getAirCountry())){		
			sModifiedFields =sModifiedFields+"Country|";	
		}
		if(!p_oAirlineRegForm.getAirZip().equals(p_oAirlineRegVO.getAirZip())){		
			sModifiedFields =sModifiedFields+"Zip|";	
		}
		if(!p_oAirlineRegForm.getAirPhone1().equals(p_oAirlineRegVO.getAirPhone1())){		
			sModifiedFields =sModifiedFields+"Phone1|";	
		}
		if(!p_oAirlineRegForm.getAirPhoneExt1().equals(p_oAirlineRegVO.getAirPhoneExt1())){		
			sModifiedFields =sModifiedFields+"Ext1|";	
		}
		if(!p_oAirlineRegForm.getAirMobile1().equals(p_oAirlineRegVO.getAirMobile1())){		
			sModifiedFields =sModifiedFields+"Mobile1|";	
		}
		if(!p_oAirlineRegForm.getAirEmail().equals(p_oAirlineRegVO.getAirEmail())){		
			sModifiedFields =sModifiedFields+"Email|";	
		}
		if(!p_oAirlineRegForm.getAirFax().equals(p_oAirlineRegVO.getAirFax())){		
			sModifiedFields =sModifiedFields+"Fax|";	
		}
		
		
		
		if(!p_oAirlineRegForm.getAirContactName2().equals(p_oAirlineRegVO.getAirContactName2())){		
			sModifiedFields =sModifiedFields+"Contact Name2|";	
		}
		if(!p_oAirlineRegForm.getAirPhone2().equals(p_oAirlineRegVO.getAirPhone2())){		
			sModifiedFields =sModifiedFields+"Phone2|";	
		}
		if(!p_oAirlineRegForm.getAirPhoneExt2().equals(p_oAirlineRegVO.getAirPhoneExt2())){		
			sModifiedFields =sModifiedFields+"Ext2|";	
		}
		if(!p_oAirlineRegForm.getAirMobile2().equals(p_oAirlineRegVO.getAirMobile2())){		
			sModifiedFields =sModifiedFields+"Mobile2|";	
		}
		if(!p_oAirlineRegForm.getAirEmail2().equals(p_oAirlineRegVO.getAirEmail2())){		
			sModifiedFields =sModifiedFields+"Email2|";	
		}
		if(!p_oAirlineRegForm.getAirReturnPolicy().equals(p_oAirlineRegVO.getAirReturnPolicy())){		
			sModifiedFields =sModifiedFields+"Return Policy|";	
		}
		if(!p_oAirlineRegForm.getAirComments().trim().equals(p_oAirlineRegVO.getAirComments())){		
			sModifiedFields =sModifiedFields+"Comments|";	
		}
		if(!p_oAirlineRegForm.getCustServicePhoneno().trim().equals(p_oAirlineRegVO.getCustServicePhoneno())){		
			sModifiedFields =sModifiedFields+"Cust Service Phoneno|";	
		}
		if(!p_oAirlineRegForm.getCustServicePhonenoext().trim().equals(p_oAirlineRegVO.getCustServicePhonenoext())){		
			sModifiedFields =sModifiedFields+"CustService Phoneno Ext|";	
		}
		if(!p_oAirlineRegForm.getCustServiceEmail().trim().equals(p_oAirlineRegVO.getCustServiceEmail())){		
			sModifiedFields =sModifiedFields+"Cust Service Email|";	
		}
		
		if(!p_oAirlineRegForm.getReturnAddr1().trim().equals(p_oAirlineRegVO.getReturnAddr1())){		
			sModifiedFields =sModifiedFields+"Return Company Address|";	
		}
		
		if(!p_oAirlineRegForm.getReturnAddr2().trim().equals(p_oAirlineRegVO.getReturnAddr2())){		
			sModifiedFields =sModifiedFields+"Return Additional Address|";	
		}
		
		if(!p_oAirlineRegForm.getReturnCity().trim().equals(p_oAirlineRegVO.getReturnCity())){		
			sModifiedFields =sModifiedFields+"Return City|";	
		}
		
		if(!p_oAirlineRegForm.getReturnState().trim().equals(p_oAirlineRegVO.getReturnState())){		
			sModifiedFields =sModifiedFields+"Return State|";	
		}
		
		if(!p_oAirlineRegForm.getReturnCountry().trim().equals(p_oAirlineRegVO.getReturnCountry())){		
			sModifiedFields =sModifiedFields+"Return Country|";	
		}
		
		if(!p_oAirlineRegForm.getReturnZip().trim().equals(p_oAirlineRegVO.getReturnZip())){		
			sModifiedFields =sModifiedFields+"Return Zip|";	
		}
		
		if(!p_oAirlineRegForm.getReturnDays().trim().equals(p_oAirlineRegVO.getReturnDays())){		
			sModifiedFields =sModifiedFields+"Return Days|";	
		}
		
		if(p_oAirlineRegForm.getAboutMe()!=null && !p_oAirlineRegForm.getAboutMe().getFileName().equals("")){
			sModifiedFields =sModifiedFields+"About Me Swf|";	
		}
		
		if(p_oAirlineRegForm.getAirlineLogo()!=null && !p_oAirlineRegForm.getAirlineLogo().getFileName().equals("")){
			sModifiedFields =sModifiedFields+"Air Charter Logo|";	
		}
		//update user audit details
		VendorDAO.insertUserAuditDetails(p_oAirlineRegForm.getAirId(),"airline",sCurrentUserDetails, sPreviousUserDetails,sModifiedFields,p_oAirlineRegForm.getAirComments(),p_oLoginVO.getUserId(),"AIRLINE UPDATED");


		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("AirlineDAO::getUpdatedUserDetails:EXIT");
		

	}
	
	public static int updateAirlineLogoDetails(AirlineRegVO p_oAirlineRegVO, String p_sAirId, String p_sUpdaterOwnerType, String p_sOwnerType, String p_sOwnerId) 
		throws Exception {
		
		logger.info("AirlineDAO::updateAirlineLogoDetails:ENTER");
		
		int iRowCount = 0;
		String sQry="{call usp_sbh_upd_airline_logo_details(?,?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
	
		logger.debug("AirlineDAO::updateAirlineLogoDetails:SQL QUERY "+sQry);	
		logger.debug("AirlineDAO::updateAirlineLogoDetails:Air Logo Type: "+p_oAirlineRegVO.getAirlineLogoType());
		logger.debug("AirlineDAO::updateAirlineLogoDetails:Air Id: "+p_sAirId);
		logger.debug("AirlineDAO::updateAirlineLogoDetails:Owner Type: "+p_sOwnerType);
		logger.debug("AirlineDAO::updateAirlineLogoDetails:Update Owner Type: "+p_sUpdaterOwnerType);
		logger.debug("AirlineDAO::updateAirlineLogoDetails:Owner Id: "+p_sOwnerId);
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			cstmt.registerOutParameter(1, Types.INTEGER);	
			cstmt.setString(2, p_oAirlineRegVO.getAirlineLogoType());	
			cstmt.setString(3, p_sAirId);
			cstmt.setString(4, p_sUpdaterOwnerType);
			cstmt.setString(5, p_sOwnerType);
			cstmt.setString(6, p_sOwnerId);
			
			cstmt.execute();
			
			iRowCount = cstmt.getInt(1);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::updateAirlineLogoDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		
		logger.info("AirlineDAO::updateAirlineLogoDetails:EXIT");
		
		return iRowCount;
	}
	
	public static List<PartnerRegistrationCommentsVO> getAirlineComments(String p_sOwnerId, String p_sOwnerType) throws Exception{	
		logger.info("AirlineDAO::getAirlineComments:ENTER");		
		PartnerRegistrationCommentsVO oPartnerRegistrationCommentsVO=null;
		
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_partner_comments(?,?)}";
		
		logger.debug("AirlineDAO::getAirlineComments:SQL QUERY "+sQry);
		logger.debug("AirlineDAO::getAirlineComments:Search By "+p_sOwnerId);
		logger.debug("AirlineDAO::getAirlineComments:Search Type "+p_sOwnerType);	
		
		ResultSet rs=null;
		List<PartnerRegistrationCommentsVO> alAirlineComments=new ArrayList<PartnerRegistrationCommentsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOwnerId);
			cstmt.setString(2,p_sOwnerType);	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				
				oPartnerRegistrationCommentsVO=new PartnerRegistrationCommentsVO();
				oPartnerRegistrationCommentsVO.setOwnerId(rs.getString("owner_id"));
				oPartnerRegistrationCommentsVO.setOwnerType(convertToNameFormat(rs.getString("owner_type")));
				oPartnerRegistrationCommentsVO.setComments(rs.getString("comments"));
				oPartnerRegistrationCommentsVO.setCreateDate(dateAndTimeStampConversion(rs.getString("sbh_create_dt")));	
				oPartnerRegistrationCommentsVO.setCreateId(convertToNameFormat(rs.getString("sbh_create_id")));	

				alAirlineComments.add(oPartnerRegistrationCommentsVO);
			}
			logger.info("AirlineDAO::getAirlineComments:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("AirlineDAO::getAirlineComments:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return alAirlineComments;
	}
	
	public static int updatePartnerAboutMeDetails(String p_sOwnerType, String p_sOwnerId, String p_sAboutMeType, String p_sUpdateId) 
	throws Exception {
	
	logger.info("AirlineDAO::updatePartnerAboutMeDetails:ENTER");
	
	int iRowCount = 0;
	String sQry="{call usp_sbh_upd_partner_aboutme_details(?,?,?,?,?)}";
	Connection con=null;		
	CallableStatement cstmt=null;
	ResultSet rs=null;

	logger.debug("AirlineDAO::updatePartnerAboutMeDetails:SQL QUERY "+sQry);	
	logger.debug("AirlineDAO::updatePartnerAboutMeDetails:Owner Type: "+p_sOwnerType);
	logger.debug("AirlineDAO::updatePartnerAboutMeDetails:Owner Id: "+p_sOwnerId);
	logger.debug("AirlineDAO::updatePartnerAboutMeDetails:About Me Type: "+p_sAboutMeType);
	logger.debug("AirlineDAO::updatePartnerAboutMeDetails:Update Id: "+p_sUpdateId);

	try{
		con = DBConnection.getSQL2005Connection();
		cstmt=con.prepareCall(sQry);
		
		cstmt.registerOutParameter(1, Types.INTEGER);	
		cstmt.setString(2, p_sOwnerType);	
		cstmt.setString(3, p_sOwnerId);
		cstmt.setString(4, p_sAboutMeType);
		cstmt.setString(5, p_sUpdateId);
		
		cstmt.execute();
		
		iRowCount = cstmt.getInt(1);
		
	}catch(Exception e){
		e.printStackTrace();
		logger.error("AirlineDAO::updatePartnerAboutMeDetails:Exception "+e.getMessage());
		throw e;
	}
	finally{
		if(con!=null)
			con.close();
		if(cstmt!=null)
			cstmt.close();
		if(rs!=null)
			rs.close();
		
	}
	
	logger.info("AirlineDAO::updatePartnerAboutMeDetails:EXIT");
	
	return iRowCount;
}
	
	public static String getPhoneFormat(String p_sInputFormat)throws Exception{
		String p_sDigit1=null,p_sDigit2=null,p_sDigit3=null,p_sFormat="";
		if(p_sInputFormat!=null && p_sInputFormat.trim().length()==10){
			p_sDigit1=p_sInputFormat.substring(0,3);
			p_sDigit2=p_sInputFormat.substring(3,6);
			p_sDigit3=p_sInputFormat.substring(6,10);
			p_sFormat=p_sDigit1+"-"+p_sDigit2+"-"+p_sDigit3;				
		}
		else if(p_sInputFormat!=null && p_sInputFormat.trim().length()==14){
			p_sDigit1=p_sInputFormat.substring(1,4);
			p_sDigit2=p_sInputFormat.substring(6,9);
			p_sDigit3=p_sInputFormat.substring(10,14);
			p_sFormat=p_sDigit1+"-"+p_sDigit2+"-"+p_sDigit3;		
		}
		else{
			p_sFormat=p_sInputFormat;
		}
		return p_sFormat;

	} 
	
}
