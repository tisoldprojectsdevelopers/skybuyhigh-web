package com.sbh.dao;

import java.net.InetAddress;
import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.forms.DeviceRegForm;
import com.sbh.forms.SearchDeviceForm;
import com.sbh.util.DBConnection;
import com.sbh.util.KeyUtils;
import com.sbh.util.Utils;
import com.sbh.vo.AirlineVO;
import com.sbh.vo.DeviceAttendanceVO;
import com.sbh.vo.DeviceDetailsVO;
import com.sbh.vo.DeviceRegVO;
import com.sbh.vo.ProductDetailsVO;

public class DeviceRegDAO {
	
	private static Logger logger = LogManager.getLogger(DeviceRegDAO.class);

	public static int addDeviceDetails(DeviceRegForm p_oDeviceRegForm,String p_sProdKey, String p_sLoginId,String p_sKeyRefId) throws Exception{	
		logger.info("DeviceRegDAO::addDeviceDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_device_details(?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::addDeviceDetails:SQL QUERY "+sQry);			
		logger.debug("DeviceRegDAO::addDeviceDetails:Air Id "+p_oDeviceRegForm.getAirId());	
		logger.debug("DeviceRegDAO::addDeviceDetails:device Type Id "+p_oDeviceRegForm.getDeviceType());	
		logger.debug("DeviceRegDAO::addDeviceDetails:Device Serial No  "+p_oDeviceRegForm.getDeviceSerialNo());	
		logger.debug("DeviceRegDAO::addDeviceDetails:Device Model  "+p_oDeviceRegForm.getDeviceModel());	
		

		int iDeviceId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.setString(2,p_oDeviceRegForm.getDeviceType());
			cstmt.setString(3,p_oDeviceRegForm.getDeviceSerialNo());
			cstmt.setString(4,p_oDeviceRegForm.getDeviceModel());	
			cstmt.setString(5,p_oDeviceRegForm.getDeviceCode());	
			cstmt.setString(6,p_oDeviceRegForm.getProductNo());
			cstmt.setString(7,p_sProdKey);
			cstmt.setString(8,p_oDeviceRegForm.getDevicePassword());
			cstmt.setString(9,p_sKeyRefId);
			cstmt.setString(10, p_sLoginId);
			cstmt.execute();
			iDeviceId=cstmt.getInt(1);

			logger.info("DeviceRegDAO::addDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::addDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iDeviceId;
	}
	
	public static int addAirDeviceDetails(String p_sAirId,int p_sdeviceId, String p_sLoginId) throws Exception{	
		logger.info("DeviceRegDAO::addAirDeviceDetails:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_airline_device(?,?,?,?)}";

		logger.debug("DeviceRegDAO::addAirDeviceDetails:SQL QUERY "+sQry);			
		logger.debug("DeviceRegDAO::addAirDeviceDetails:Air Id "+p_sAirId);	
		logger.debug("DeviceRegDAO::addAirDeviceDetails:device Id "+p_sdeviceId);	
		logger.debug("DeviceRegDAO::addAirDeviceDetails:Login Id "+p_sLoginId);
		
		int iDeviceId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.setInt(2,p_sdeviceId);
			cstmt.setString(3,p_sAirId);
			cstmt.setString(4,p_sLoginId);
			cstmt.execute();
			iDeviceId=cstmt.getInt(1);
			logger.info("DeviceRegDAO::addAirDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::addAirDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iDeviceId;
	}
	
	public static int updateAirDeviceDetails(String p_sAirId,int p_sdeviceId,String p_sStatus,String p_sLoginId) throws Exception{	
		logger.info("DeviceRegDAO::updateAirDeviceDetails:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_airline_device(?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::updateAirDeviceDetails:SQL QUERY "+sQry);			
		logger.debug("DeviceRegDAO::updateAirDeviceDetails:Air Id: "+p_sAirId);	
		logger.debug("DeviceRegDAO::updateAirDeviceDetails:device Id: "+p_sdeviceId);	
		logger.debug("DeviceRegDAO::updateAirDeviceDetails:Status: "+p_sStatus);
		logger.debug("DeviceRegDAO::updateAirDeviceDetails:Login Id: "+p_sLoginId);
		
		int iDeviceId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.setInt(2,p_sdeviceId);
			cstmt.setString(3,p_sAirId);
			cstmt.setString(4,p_sStatus);
			cstmt.setString(5,p_sLoginId);
			
			cstmt.execute();
			iDeviceId=cstmt.getInt(1);
			logger.info("DeviceRegDAO::updateAirDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::updateAirDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iDeviceId;
	}
	
	
	
	public static ArrayList checkDeviceRegDetails(String p_sAirId,String p_sMacAddr,String p_sProductKey, String p_sLoginId) throws Exception{	
		logger.info("DeviceRegDAO::checkDeviceRegDetails:ENTER");		
			
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int iUserIdCount = -1,iDeviceIdCount = -1;		
		ArrayList alDeviceRegStatus  =  new ArrayList();
		String sQry="{call usp_sbh_add_device_reg_details(?,?,?,?,?,?,?,?,?)}";
		logger.debug("DeviceRegDAO::checkDeviceRegDetails:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkDeviceRegDetails:Air Id "+p_sAirId);
		logger.debug("DeviceRegDAO::checkDeviceRegDetails:Mac Address: "+p_sMacAddr);
		logger.debug("DeviceRegDAO::checkDeviceRegDetails:Login Id: "+p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);			
			cstmt.registerOutParameter(2, Types.VARCHAR);				
			cstmt.registerOutParameter(3, Types.VARCHAR);	
			cstmt.registerOutParameter(4, Types.VARCHAR);	
			cstmt.registerOutParameter(5, Types.VARCHAR);	
			cstmt.setString(6,p_sAirId);		
			cstmt.setString(7,p_sMacAddr);
			cstmt.setString(8,p_sProductKey);		
			cstmt.setString(9,p_sLoginId);
			cstmt.execute();
			
			alDeviceRegStatus.add(0,cstmt.getInt(1));
			alDeviceRegStatus.add(1,cstmt.getString(2));
			alDeviceRegStatus.add(2,cstmt.getString(3));
			alDeviceRegStatus.add(3,cstmt.getString(4));
			alDeviceRegStatus.add(4,cstmt.getString(5));
			logger.info("DeviceRegDAO::checkDeviceRegDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkDeviceRegDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return alDeviceRegStatus;
	}
	
	public static ArrayList getDeviceDetails(SearchDeviceForm oSearchDeviceForm,String p_sAirId) throws Exception{	
		logger.info("DeviceRegDAO::getDeviceDetails:ENTER");		
		DeviceRegVO oDeviceRegVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_device_details(?,?,?)}";

		logger.debug("DeviceRegDAO::getDeviceDetails:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::getDeviceDetails:Search By "+oSearchDeviceForm.getDeviceSearchBy());
		logger.debug("DeviceRegDAO::getDeviceDetails:Search Type "+oSearchDeviceForm.getDeviceSearchValue());	
		logger.debug("DeviceRegDAO::getDeviceDetails:Air Id "+p_sAirId);	

		ResultSet rs=null;
		ArrayList alDeviceInfo=new ArrayList();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,oSearchDeviceForm.getDeviceSearchBy());
			cstmt.setString(2,oSearchDeviceForm.getDeviceSearchValue());	
			cstmt.setString(3,p_sAirId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oDeviceRegVO=new DeviceRegVO();
				oDeviceRegVO.setDeviceId(rs.getString("device_id"));
				oDeviceRegVO.setAirName(rs.getString("air_name"));
				oDeviceRegVO.setDeviceType(rs.getString("device_type_id"));	
				oDeviceRegVO.setDeviceSerialNo(rs.getString("device_serial_no"));	
				oDeviceRegVO.setMacAddr(rs.getString("mac_addr1"));
				oDeviceRegVO.setMacDesc(rs.getString("mac_desc1"));
				oDeviceRegVO.setProcessorId(rs.getString("processor_id"));
				oDeviceRegVO.setDeviceModel(rs.getString("device_model"));
				//oDeviceRegVO.setFlightNo(rs.getString("flight_no"));
				oDeviceRegVO.setProductKey(rs.getString("product_key"));
				oDeviceRegVO.setProductNo(rs.getString("product_no"));
				oDeviceRegVO.setIsActive(rs.getString("is_active"));
				oDeviceRegVO.setStatus(rs.getString("device_status").trim());
				
				String sAirId = rs.getString("air_id");
				sAirId = sAirId ==null?"":sAirId.trim();
				oDeviceRegVO.setAirId(sAirId);
				
				String sDeviceCode = rs.getString("device_code");
				sDeviceCode = sDeviceCode ==null?"":sDeviceCode.trim();
				oDeviceRegVO.setDeviceCode(sDeviceCode);
				
				alDeviceInfo.add(oDeviceRegVO);
			}
			logger.info("DeviceRegDAO::getDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alDeviceInfo;
	}
	public static boolean isDeviceIdExist(String p_sDeviceId) throws Exception{	
		logger.info("DeviceRegDAO::isDeviceIdExist:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		String sDeviceId = null;
		boolean dIsExist = false;
		String sQry="{call usp_sbh_get_device_id(?)}";

		logger.debug("DeviceRegDAO::isDeviceIdExist:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::isDeviceIdExist:Search By "+p_sDeviceId);
		ResultSet rs=null;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sDeviceId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				sDeviceId = rs.getString("device_code");
				dIsExist = true;
			}
			logger.info("DeviceRegDAO::isDeviceIdExist:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::isDeviceIdExist:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return dIsExist;
	}
	public static DeviceRegVO getDeviceDetailsById(String p_sDeviceId) throws Exception{	
		logger.info("DeviceRegDAO::getDeviceDetailsById:ENTER");		
		DeviceRegVO oDeviceRegVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sData = null;
		String sKeyRefId = null;
		String sPassword = null;
		String sQry="{call usp_sbh_get_device_by_id(?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.debug("DeviceRegDAO::getDeviceDetailsById:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::getDeviceDetailsById:Airline Code "+p_sDeviceId);			

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sDeviceId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oDeviceRegVO=new DeviceRegVO();
				oDeviceRegVO.setDeviceId(rs.getString("device_id"));
				sData = rs.getString("air_id");
				sData = sData == null?"":sData.trim();
				oDeviceRegVO.setAirId(sData);
				oDeviceRegVO.setAirName(rs.getString("air_name"));
				oDeviceRegVO.setDeviceType(rs.getString("device_type_id"));	
				oDeviceRegVO.setDeviceSerialNo(rs.getString("device_serial_no"));	
				oDeviceRegVO.setMacAddr(rs.getString("mac_addr1"));
				oDeviceRegVO.setMacDesc(rs.getString("mac_desc1"));
				oDeviceRegVO.setProcessorId(rs.getString("processor_id"));
				oDeviceRegVO.setDeviceModel(rs.getString("device_model"));
				oDeviceRegVO.setProductKey(rs.getString("product_key"));
				oDeviceRegVO.setProductNo(rs.getString("product_no"));
				oDeviceRegVO.setIsActive(rs.getString("is_active"));
				oDeviceRegVO.setAdminVersion(rs.getString("admin_version"));
				oDeviceRegVO.setCatalogueVersion(rs.getString("catalogue_version"));
				
				sData = rs.getString("device_status");
				sData = sData == null?"":sData.trim();
				oDeviceRegVO.setStatus(sData);
				
				sData = rs.getString("device_code");
				sData = sData == null?"":sData.trim();
				oDeviceRegVO.setDeviceCode(sData);
				
				sData = rs.getString("air_userId");
				sData = sData == null?"":sData.trim();
				oDeviceRegVO.setAirUserId(sData);
				
				sData = rs.getString("air_userId");
				sData = sData == null?"":sData.trim();
				oDeviceRegVO.setAirUserId(sData);
				
				sKeyRefId = rs.getString("key_ref_id");
				sKeyRefId = sKeyRefId == null?"":sKeyRefId.trim();
				
				sPassword = rs.getString("device_password");
				sPassword = sPassword == null?"":sPassword.trim();
				if(!"".equalsIgnoreCase(sKeyRefId)){
					sPassword = OrderDAO.decryptInputData(sPassword, "SERVER", sKeyRefId);
					oDeviceRegVO.setDevicePassword(sPassword);
				}else{
					oDeviceRegVO.setDevicePassword("");
				}
			}
			logger.info("DeviceRegDAO::getDeviceDetailsById:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getDeviceDetailsById:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oDeviceRegVO;
	}
	
	public static Map<String, List<DeviceDetailsVO>> getDeviceHistoryById(String p_sDeviceId) throws Exception{	
		logger.info("DeviceRegDAO::getDeviceHistoryById:ENTER");		
		
		Map<String, List<DeviceDetailsVO>> mDeviceDetailHistory = new HashMap<String, List<DeviceDetailsVO>>();
		
		List<DeviceDetailsVO> alDeviceRegDetailsVO = null;
		List<DeviceDetailsVO> alDeviceAllocateDetailsVO = null;
		List<DeviceDetailsVO> alCtlgDownloadDetailsVO = null;
		List<DeviceDetailsVO> alOrderPlaceDetailsVO = null;
		List<DeviceDetailsVO> alPackageDownloadDetailsVO = null;
		
		DeviceDetailsVO oDeviceRegDetailsVO=null;
		DeviceDetailsVO oDeviceAllocateDetailsVO=null;
		DeviceDetailsVO oCtlgDownloadDetailsVO=null;
		DeviceDetailsVO oOrderPlaceDetailsVO=null;
		DeviceDetailsVO oPackageDownloadDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_device_history_by_id(?)}";
		logger.debug("DeviceRegDAO::getDeviceHistoryById:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::getDeviceHistoryById:Airline Code "+p_sDeviceId);			

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sDeviceId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			alDeviceRegDetailsVO = new ArrayList<DeviceDetailsVO>();
			while(rs.next()) {				
				oDeviceRegDetailsVO=new DeviceDetailsVO();
				oDeviceRegDetailsVO.setDeviceRegId(rs.getString("id"));
				oDeviceRegDetailsVO.setDeviceRegDeviceId(rs.getString("device_id"));
				oDeviceRegDetailsVO.setDeviceRegAirId(rs.getString("air_id"));
				oDeviceRegDetailsVO.setDeviceRegAirName(rs.getString("air_name"));
				oDeviceRegDetailsVO.setDeviceRegActivity(rs.getString("activity"));
				oDeviceRegDetailsVO.setDeviceRegMacAddress(rs.getString("mac_address"));
				oDeviceRegDetailsVO.setDeviceRegUpdateDate(Utils.dateStampConversion(rs.getString("sbh_create_dt")));
				oDeviceRegDetailsVO.setDeviceRegUpdateId(rs.getString("sbh_create_id"));
				
				alDeviceRegDetailsVO.add(oDeviceRegDetailsVO);
			}
			mDeviceDetailHistory.put("DeviceRegDetails", alDeviceRegDetailsVO);
			if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				alDeviceAllocateDetailsVO = new ArrayList<DeviceDetailsVO>();
				while(rs.next()) {
					oDeviceAllocateDetailsVO=new DeviceDetailsVO();
					oDeviceAllocateDetailsVO.setDeviceAllocateId(rs.getString("id"));
					oDeviceAllocateDetailsVO.setDeviceAllocateDeviceId(rs.getString("device_id"));
					oDeviceAllocateDetailsVO.setDeviceAllocateAirId(rs.getString("air_id"));
					oDeviceAllocateDetailsVO.setDeviceAllocateAirName(rs.getString("air_name"));
					oDeviceAllocateDetailsVO.setDeviceAllocateActivity(rs.getString("activity"));
//					oDeviceAllocateDetailsVO.setDeviceAllocateMacAddress(rs.getString("mac_address"));
					oDeviceAllocateDetailsVO.setDeviceAllocateUpdateDate(Utils.dateStampConversion(rs.getString("sbh_create_dt")));
					oDeviceAllocateDetailsVO.setDeviceAllocateUpdateId(rs.getString("sbh_create_id"));
					
					alDeviceAllocateDetailsVO.add(oDeviceAllocateDetailsVO);
				}
				mDeviceDetailHistory.put("DeviceAllocateDetails", alDeviceAllocateDetailsVO);
			}
			if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				alCtlgDownloadDetailsVO = new ArrayList<DeviceDetailsVO>();
				while(rs.next()) {
					oCtlgDownloadDetailsVO=new DeviceDetailsVO();
					oCtlgDownloadDetailsVO.setCtlgDownloadId(rs.getString("id"));
					oCtlgDownloadDetailsVO.setCtlgDownloadDeviceId(rs.getString("device_id"));
					oCtlgDownloadDetailsVO.setCtlgDownloadAirId(rs.getString("air_id"));
					oCtlgDownloadDetailsVO.setCtlgDownloadAirName(rs.getString("air_name"));
					oCtlgDownloadDetailsVO.setCtlgDownloadActivity(rs.getString("activity"));
					oCtlgDownloadDetailsVO.setCtlgDownloadMacAddress(rs.getString("mac_address"));
					oCtlgDownloadDetailsVO.setCtlgDownloadUpdateDate(Utils.dateStampConversion(rs.getString("sbh_create_dt")));
					oCtlgDownloadDetailsVO.setCtlgDownloadUpdateId(rs.getString("sbh_create_id"));
					
					alCtlgDownloadDetailsVO.add(oCtlgDownloadDetailsVO);
				}
				mDeviceDetailHistory.put("CatalogueDownloadDetails", alCtlgDownloadDetailsVO);
			}
			if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				alOrderPlaceDetailsVO = new ArrayList<DeviceDetailsVO>();
				while(rs.next()) {
					oOrderPlaceDetailsVO = new DeviceDetailsVO();
					oOrderPlaceDetailsVO.setOrderPlaceId(rs.getString("id"));
					oOrderPlaceDetailsVO.setOrderPlaceDeviceId(rs.getString("device_id"));
					oOrderPlaceDetailsVO.setOrderPlaceAirId(rs.getString("air_id"));
					oOrderPlaceDetailsVO.setOrderPlaceAirName(rs.getString("air_name"));
					oOrderPlaceDetailsVO.setOrderPlaceFlightNo(rs.getString("flight_no"));
					oOrderPlaceDetailsVO.setOrderPlaceTotalOrders(rs.getString("total_orders"));
					oOrderPlaceDetailsVO.setOrderPlaceActivity(rs.getString("activity"));
					oOrderPlaceDetailsVO.setOrderPlaceMacAddress(rs.getString("mac_address"));
					oOrderPlaceDetailsVO.setOrderPlaceUpdateDate(Utils.dateStampConversion(rs.getString("sbh_create_dt")));
					oOrderPlaceDetailsVO.setOrderPlaceUpdateId(rs.getString("sbh_create_id"));
					
					alOrderPlaceDetailsVO.add(oOrderPlaceDetailsVO);
				}
				mDeviceDetailHistory.put("OrderPlacedDetails", alOrderPlaceDetailsVO);
			}
			if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				alPackageDownloadDetailsVO = new ArrayList<DeviceDetailsVO>();
				while(rs.next()) {
					oPackageDownloadDetailsVO = new DeviceDetailsVO();
					oPackageDownloadDetailsVO.setPackageDownloadId(rs.getString("id"));
					oPackageDownloadDetailsVO.setPackageDownloadDeviceId(rs.getString("device_id"));
					oPackageDownloadDetailsVO.setPackageDownloadAdminVersion(rs.getString("admin_version"));
					oPackageDownloadDetailsVO.setPackageDownloadCatalogueVersion(rs.getString("catalogue_version"));
					oPackageDownloadDetailsVO.setPackageDownloadType(rs.getString("package_type"));
					oPackageDownloadDetailsVO.setPackageDownloadActivity(rs.getString("activity"));
					oPackageDownloadDetailsVO.setPackageDownloadMacAddress(rs.getString("mac_address"));
					oPackageDownloadDetailsVO.setPackageDownloadDate(Utils.dateStampConversion(rs.getString("sbh_update_dt")));
					oPackageDownloadDetailsVO.setPackageDownloadUpdateId(rs.getString("sbh_update_id"));
					
					alPackageDownloadDetailsVO.add(oPackageDownloadDetailsVO);
				}
				mDeviceDetailHistory.put("PackageDownloadDetails", alPackageDownloadDetailsVO);
			}
			
			logger.info("DeviceRegDAO::getDeviceHistoryById:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getDeviceHistoryById:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return mDeviceDetailHistory;
	}
	
	
	public static int updateDeviceDetails(DeviceRegForm p_oDeviceRegForm,String p_sKeyRefId,String p_sLoginId) throws Exception{	
		logger.info("DeviceRegDAO::updateDeviceDetails:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_device_details(?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::updateDeviceDetails:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::updateDeviceDetails:Device Id: "+p_oDeviceRegForm.getDeviceId());
		logger.debug("DeviceRegDAO::updateDeviceDetails:Device Type: "+p_oDeviceRegForm.getDeviceType());
		logger.debug("DeviceRegDAO::updateDeviceDetails:Device Serial No: "+p_oDeviceRegForm.getDeviceSerialNo());
		logger.debug("DeviceRegDAO::updateDeviceDetails:Device Model: "+p_oDeviceRegForm.getDeviceModel());			
		logger.debug("DeviceRegDAO::updateDeviceDetails:Status: "+p_oDeviceRegForm.getStatus());			
		logger.debug("DeviceRegDAO::updateDeviceDetails:Air Id: "+p_oDeviceRegForm.getAirId());
		logger.debug("DeviceRegDAO::updateDeviceDetails:Product No: "+p_oDeviceRegForm.getProductNo());
		logger.debug("DeviceRegDAO::updateDeviceDetails:Login Id: "+p_sLoginId);

		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oDeviceRegForm.getDeviceId());
			cstmt.setString(3,p_oDeviceRegForm.getDeviceType());
			cstmt.setString(4,p_oDeviceRegForm.getDeviceSerialNo());
			cstmt.setString(5,p_oDeviceRegForm.getDeviceModel());			
			cstmt.setString(6,p_oDeviceRegForm.getStatus());			
			cstmt.setString(7,p_oDeviceRegForm.getAirId());
			cstmt.setString(8,p_oDeviceRegForm.getProductNo());
			cstmt.setString(9,p_oDeviceRegForm.getDevicePassword());
			cstmt.setString(10,p_sKeyRefId);
			cstmt.setString(11,p_sLoginId);
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("DeviceRegDAO::updateDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::updateDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}
	
	public static int updateDeviceDetails(String p_sProductKey,String p_sProcessorId,String p_sMacAddress,String p_sMacDesc) throws Exception{	
		logger.info("DeviceRegDAO::updateDeviceDetails:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_device_mac_details(?,?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::updateDeviceDetails:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::updateDeviceDetails:Mac Address: "+p_sMacAddress);	
		logger.debug("DeviceRegDAO::updateDeviceDetails:Processor Id: "+p_sProcessorId);	
		logger.debug("DeviceRegDAO::updateDeviceDetails:Mac Desc: "+p_sMacDesc);

		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sProductKey);
			cstmt.setString(3,p_sProcessorId);
			cstmt.setString(4,p_sMacAddress);
			cstmt.setString(5,p_sMacDesc);
			cstmt.setString(6,InetAddress.getLocalHost().getHostName());
			
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("DeviceRegDAO::updateDeviceDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::updateDeviceDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}
	public static List<AirlineVO> getAirlineCode() throws Exception{	
		logger.info("DeviceRegDAO::getAirlineCode:ENTER");		
		List<AirlineVO> alAirlineCode = new ArrayList<AirlineVO>();
		AirlineVO oAirlineVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_airline_code()}";		
		logger.debug("DeviceRegDAO::getAirlineCode:SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				oAirlineVO = new AirlineVO();
				oAirlineVO.setAirId(rs.getString("air_id"));
				oAirlineVO.setAirCode(rs.getString("air_name"));
				oAirlineVO.setAirUserId(rs.getString("air_user_id"));
				alAirlineCode.add(oAirlineVO);				
			}
			logger.info("DeviceRegDAO::getAirlineCode:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getAirlineCode:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alAirlineCode;
	}
	
	public static void deleteDevice(String p_sDeviceId, String p_sLoginId) throws Exception{	
		logger.info("DeviceRegDAO::deleteDevice:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_delete_device(?,?)}";

		logger.debug("DeviceRegDAO::deleteDevice:SQL QUERY "+sQry);	
		logger.debug("DeviceRegDAO::deleteDevice:Device Id :"+p_sDeviceId);	
		logger.debug("DeviceRegDAO::deleteDevice:Login Id :"+ p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sDeviceId);
			cstmt.setString(2, p_sLoginId);
			cstmt.executeUpdate();			

			logger.info("DeviceRegDAO::deleteDevice:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::deleteDevice:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
	}

	public static String getProductKey(DeviceRegForm p_oDeviceRegForm) throws Exception {
		logger.info("DeviceRegDAO::getProductKey:ENTER");
		String sKey = null;
		String sInput = null;
		String sDateTime;
		try{
			SimpleDateFormat sdfDate = new SimpleDateFormat  ("yyyyMMddhhmmssSS") ;  
			Date dCurrDate = new Date();
			sDateTime=sdfDate.format(dCurrDate);
			if(p_oDeviceRegForm.getAirId()!= null && p_oDeviceRegForm.getDeviceSerialNo()!=null){
				sInput = p_oDeviceRegForm.getAirId()+p_oDeviceRegForm.getDeviceSerialNo();
				sInput = sInput + sDateTime;
			}
			byte[] plainText = sInput.getBytes("UTF8");
			// Get a message digest object using the MD2 algorithm  BOUNCY
			// MessageDigest messageDigest = MessageDigest.getInstance("MD2");
			//
			// Get a message digest object using the MD5 algorithm
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			//
			// Get a message digest object using the SHA-1 algorithm
			// MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");                        
			//
			// Get a message digest object using the SHA-256 algorithm
			// MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			//
			// Get a message digest object using the SHA-384 algorithm
			// MessageDigest messageDigest = MessageDigest.getInstance("SHA-384");
			//
			// Get a message digest object using the SHA-512 algorithm
			// MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");              
			//
			// Print out the provider used
			// // System.out.println( "\n" + messageDigest.getProvider().getInfo() );
			//
			// Calculate the digest and print it out
			messageDigest.update( plainText);
			byte[] bDig = new String( messageDigest.digest(), "UTF8").getBytes();
			int iLen = KeyUtils.toString(bDig).length();
			sKey = KeyUtils.toString(bDig).toUpperCase();
			/*if (iLen<32){
				for(int i = iLen;i<32;i++){
					sKey = "0"+sKey;
				}
			}*/

			logger.info("DeviceRegDAO::getProductKey:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getProductKey:Exception "+e.getMessage());
			throw e;
		}
		return sKey;
	}
	public static boolean validateDevice(String p_sProductKey,String p_sAirId,String p_sMacAddress,String p_sProcessName) throws Exception{	
		logger.info("DeviceRegDAO::validateDevice:ENTER");		

		Connection con=null;	
		CallableStatement cstmt=null;	
		boolean bStatus = false;

		String sQry="{call usp_sbh_validate_device(?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::validateDevice:SQL QUERY "+sQry);	
		logger.debug("DeviceRegDAO::validateDevice:Air Id "+p_sAirId);
		logger.debug("DeviceRegDAO::validateDevice:Mac Address "+p_sMacAddress);
		logger.debug("DeviceRegDAO::validateDevice:Process Name "+p_sProcessName);
				
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProductKey);	
			cstmt.setString(2, p_sAirId);	
			cstmt.setString(3,p_sMacAddress);		
			cstmt.setString(4,p_sProcessName);		
			cstmt.registerOutParameter(5,Types.CHAR);		
			
			cstmt.execute();

			String sStatus = cstmt.getString(5);
			if (sStatus!= null && sStatus.equalsIgnoreCase("T"))
				bStatus = true;		
			
			logger.info("DeviceRegDAO::validateDevice:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::validateDevice:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
		}
		return bStatus;
	}
	public static ArrayList getDeviceStatus(String p_sProductKey,String p_sMacAddress) throws Exception{	
		logger.info("DeviceRegDAO::getDeviceStatus:ENTER");		

		Connection con=null;	
		CallableStatement cstmt=null;	
		ArrayList alDeviceStatus = new ArrayList();

		String sQry="{call usp_sbh_get_device_status(?,?,?,?)}";

		logger.debug("DeviceRegDAO::getDeviceStatus:SQL QUERY "+sQry);	
		logger.debug("DeviceRegDAO::getDeviceStatus:Mac Address "+p_sMacAddress);
				
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProductKey);	
			cstmt.setString(2, p_sMacAddress);	
			
			
			cstmt.registerOutParameter(3,Types.CHAR);		
			cstmt.registerOutParameter(4,Types.CHAR);	
			cstmt.execute();

			String sStatus = cstmt.getString(3);
			String sAirlineStatus = cstmt.getString(4);
			sStatus = sStatus == null?"":sStatus.trim();
			sAirlineStatus = sAirlineStatus ==null?"":sAirlineStatus.trim();	
			alDeviceStatus.add(0,sStatus);
			alDeviceStatus.add(1,sAirlineStatus);
			logger.info("DeviceRegDAO::getDeviceStatus:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getDeviceStatus:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
		}
		return alDeviceStatus;
	}
	public static String checkProdKey(String p_sProdKey) throws Exception{	
		logger.info("DeviceRegDAO::checkProdKey:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDeviceStatus ="";
		String sQry="{call usp_sbh_check_prod_key(?)}";
		logger.debug("DeviceRegDAO::checkProdKey:SQL QUERY "+sQry);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdKey);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs.next()){				
				sDeviceStatus = rs.getString("device_status");
				sDeviceStatus = sDeviceStatus ==null?"":sDeviceStatus.trim();
			}
			logger.info("DeviceRegDAO::checkProdKey:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkProdKey:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return sDeviceStatus;
	}
	
	public static String checkAirlineStatus(String p_sAirId,String p_sProductKey) throws Exception{	
		logger.info("DeviceRegDAO::checkAirlineStatus:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		String sAirlineStatus = null;
		String sQry="{call usp_sbh_check_airline_status(?,?,?)}";
		logger.debug("DeviceRegDAO::checkAirlineStatus:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkAirlineStatus:Air Id: "+p_sAirId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sAirId);	
			cstmt.setString(2,p_sProductKey);	
			cstmt.registerOutParameter(3, Types.VARCHAR);				
			cstmt.execute();
			sAirlineStatus = cstmt.getString(3);		
			logger.info("DeviceRegDAO::checkAirlineStatus:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkAirlineStatus:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return sAirlineStatus;
	}
	public static ArrayList getAirlineId(String p_sProductKey) throws Exception{	
		logger.info("DeviceRegDAO::getAirlineId:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		String sAirId = null,sAirName = null;;
		ArrayList alAirlineDetails = new ArrayList();
		String sQry="{call usp_sbh_get_airline_id(?,?,?)}";
		logger.debug("DeviceRegDAO::getAirlineId:SQL QUERY "+sQry);
	

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1,Types.VARCHAR);	
			cstmt.registerOutParameter(2,Types.VARCHAR);	
			cstmt.setString(3,p_sProductKey);				
			cstmt.execute();
			sAirId = cstmt.getString(1);
			sAirName = cstmt.getString(2);
			alAirlineDetails.add(0, sAirId);
			alAirlineDetails.add(1, sAirName);
			logger.info("DeviceRegDAO::getAirlineId:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getAirlineId:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return alAirlineDetails;
	}
	
	public static boolean checkAirlineCode(String p_sAirId,String p_sProductkey) throws Exception{	
		logger.info("DeviceRegDAO::checkAirlineCode:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		String sAirlineCdeStatus = null;
		boolean bAirCodeStatus =false;
		String sQry="{call usp_sbh_check_alirline_code(?,?,?)}";
		logger.debug("DeviceRegDAO::checkAirlineCode:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkAirlineCode:Air Id: "+p_sAirId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sAirId);	
			cstmt.setString(2,p_sProductkey);	
			cstmt.registerOutParameter(3, Types.VARCHAR);				
			cstmt.execute();
			sAirlineCdeStatus = cstmt.getString(3);		
			
			if(sAirlineCdeStatus!=null && sAirlineCdeStatus.equals("T"))
				bAirCodeStatus =true;
			else
				bAirCodeStatus =false;
			
			logger.info("DeviceRegDAO::checkAirlineCode:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkAirlineCode:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return bAirCodeStatus;
	}
	
	public static boolean checkAirlineProductKey(String p_sAirId,String p_sProdKey) throws Exception{	
		logger.info("DeviceRegDAO::checkAirlineProductKey:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		String sAirlineUserIdStatus ="";
		boolean bAirProdKey = false;
		String sQry="{call usp_sbh_check_airline_prod_key(?,?,?)}";
		logger.debug("DeviceRegDAO::checkAirline:ProductKey:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkAirline:ProductKey:Air Id: "+p_sAirId);	
		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);	
			cstmt.setString(1,p_sAirId);	
			cstmt.setString(2,p_sProdKey);	
			cstmt.registerOutParameter(3, Types.VARCHAR);
			cstmt.execute();
			sAirlineUserIdStatus = cstmt.getString(3);
			
			if(sAirlineUserIdStatus!=null && sAirlineUserIdStatus.equals("T"))
				bAirProdKey =true;
			else
				bAirProdKey =false;
			
			logger.info("DeviceRegDAO::checkAirlineProductKey:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkAirlineProductKey:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return bAirProdKey;
	}
	public static boolean checkMacAddress(String p_sProdKey,String p_sMacAddr) throws Exception{	
		logger.info("DeviceRegDAO::checkMacAddress:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		ResultSet rs = null;
		boolean bStatus = false;
		String sQry="{call usp_sbh_check_mac_address(?,?,?)}";
		
		logger.debug("DeviceRegDAO::checkMacAddress:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkMacAddress:Air Id : "+p_sProdKey);	
		logger.debug("DeviceRegDAO::checkMacAddress:Device Id : "+p_sMacAddr);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdKey);		
			cstmt.setString(2,p_sMacAddr);
			cstmt.registerOutParameter(3, Types.INTEGER);	
			cstmt.execute();
			int iCount = cstmt.getInt(3);
			if(iCount>0)
				bStatus = true;
			
			logger.info("DeviceRegDAO::checkMacAddress:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkMacAddress:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return bStatus;
	}	
	public static ArrayList checkAirlineDeviceStatus(String p_sAirId,String p_sDeviceId,String p_sprodKey) throws Exception{	
		logger.info("DeviceRegDAO::checkAirlineDeviceStatus:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		ArrayList alAirlineDeviceStatus =new ArrayList();
		String sQry="{call usp_sbh_check_alirline_device_status(?,?,?,?,?)}";
		
		logger.debug("DeviceRegDAO::checkAirlineDeviceStatus:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::checkAirlineDeviceStatus:Air Id : "+p_sAirId);	
		logger.debug("DeviceRegDAO::checkAirlineDeviceStatus:Device Id : "+p_sDeviceId);	
		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sAirId);		
			cstmt.setString(2,p_sDeviceId);	
			cstmt.setString(3,p_sprodKey);	
			cstmt.registerOutParameter(4, Types.VARCHAR);//device status from device_reg table
			cstmt.registerOutParameter(5, Types.VARCHAR);//air_device_status from air_device_reg table
			cstmt.execute();
			alAirlineDeviceStatus.add(0, cstmt.getString(4));
			alAirlineDeviceStatus.add(1, cstmt.getString(5));
			
			logger.info("DeviceRegDAO::checkAirlineDeviceStatus:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::checkAirlineDeviceStatus:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return alAirlineDeviceStatus;
	}
	
	
	public static ArrayList updateMacAddress(String p_sProdKey,String p_sMacAddr,String p_sMacDesc,String p_sProcessorId) throws Exception{	
		logger.info("DeviceRegDAO::updateMacAddress:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;	
		ArrayList alAirlineDeviceStatus =new ArrayList();
		String sQry="{call usp_sbh_upd_mac_addr(?,?,?,?,?)}";
		
		logger.debug("DeviceRegDAO::updateMacAddress:SQL QUERY "+sQry);
		logger.debug("DeviceRegDAO::updateMacAddress:Mac Address : "+p_sMacAddr);	
		logger.debug("DeviceRegDAO::updateMacAddress:Processor Id : "+p_sProcessorId);	
		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdKey);		
			cstmt.setString(2,p_sMacAddr);	
			cstmt.setString(3,p_sMacDesc);	
			cstmt.setString(4,p_sProcessorId);	
			cstmt.setString(5,InetAddress.getLocalHost().getHostName());
			cstmt.execute();
			logger.info("DeviceRegDAO::updateMacAddress:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::updateMacAddress:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			

		}
		return alAirlineDeviceStatus;
	}
	
	public static void insertDeviceAuditDetails(String p_sDeviceId,String p_sAirId,String p_sProductKey,String p_sCurrentDeviceDetails,String p_sPreviousDeviceDetails,String p_sModifiedFields,String p_sActivity, String p_sLoginId) throws Exception{
		logger.info("DeviceRegDAO::insertDeviceAuditDetails:ENTER");
		
		Connection con=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_device_audit(?,?,?,?,?,?,?,?)}";
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Device id: "+p_sDeviceId);	
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Air id: "+p_sAirId);	
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Activity: "+p_sActivity);	
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Current Device Details: "+p_sCurrentDeviceDetails);
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Previous Device Details: "+p_sPreviousDeviceDetails);
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Modified Fields: "+p_sModifiedFields);
		logger.debug("DeviceRegDAO::insertDeviceAuditDetails:Login Id: "+p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.setString(1,p_sDeviceId);
			cstmt.setString(2,p_sAirId);			
			cstmt.setString(3,p_sProductKey);
			cstmt.setString(4,p_sActivity);
			cstmt.setString(5,p_sCurrentDeviceDetails);
			cstmt.setString(6,p_sPreviousDeviceDetails);
			cstmt.setString(7,p_sModifiedFields);
			cstmt.setString(8,p_sLoginId);
			cstmt.execute();
			

			logger.info("DeviceRegDAO::insertDeviceAuditDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::insertDeviceAuditDetails "+e.getMessage());
			throw e;
		}

		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}		
	}
	public static int insertDeviceLogDetails(String p_sAirId,String p_sDeviceId,String p_sFlightNo,String p_sActivity,int p_sTotalOrders,String p_sMacAddress, String p_sOrderConfNo, String p_sLoginId, String p_sRequestFrom) throws Exception{
		logger.info("DeviceRegDAO::insertDeviceLogDetails:ENTER");
		
		int iInsert = 0;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_device_log_details(?,?,?,?,?,?,?,?,?,?)}";
		logger.info("DeviceRegDAO::insertDeviceLogDetails:QUERY "+sQry);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Air Id: "+p_sAirId);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Device Id: "+p_sDeviceId);			
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Flight No: "+p_sFlightNo);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Activity: "+p_sActivity);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Total Orders: "+p_sTotalOrders+"");
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Mac Address: "+p_sMacAddress);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Login Id: "+p_sLoginId);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Request From: "+p_sRequestFrom);
		logger.debug("DeviceRegDAO::insertDeviceLogDetails:Request From: "+p_sOrderConfNo);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,p_sDeviceId);			
			cstmt.setString(4,p_sFlightNo);
			cstmt.setString(5,p_sActivity);
			cstmt.setInt(6,p_sTotalOrders);
			cstmt.setString(7,p_sMacAddress);
			cstmt.setString(8,p_sLoginId);
			cstmt.setString(9,p_sRequestFrom);
			cstmt.setString(10,p_sOrderConfNo);
			
			cstmt.execute();
			iInsert=cstmt.getInt(1);

			logger.info("DeviceRegDAO::insertDeviceLogDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::insertDeviceLogDetails "+e.getMessage());
			throw e;
		}

		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
			return iInsert;
	}
	
	public static int insertAutoUpdateDeviceLogDetails(String p_sAirId,String p_sDeviceId,String p_sActivity, String p_sAdminVersion, String p_sCatalogueVersion, 
			String p_sAutoUpdateVersion, String p_sSchemaVersion, String p_sUpdatesDownloaded, String p_sMacAddress, String p_sLoginId, String p_sRequestFrom) throws Exception {
		logger.info("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:ENTER");
		
		int iInsert = 0;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_auto_update_device_log_details(?,?,?,?,?,?,?,?,?,?,?,?)}";
		logger.info("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:QUERY "+sQry);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Air Id: "+p_sAirId);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Device Id: "+p_sDeviceId);			
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Activity: "+p_sActivity);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Admin Version: "+p_sAdminVersion);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Catalogue Version: "+p_sCatalogueVersion);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Auto Update Version: "+p_sAutoUpdateVersion);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Updates Downloaded: "+p_sUpdatesDownloaded);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Mac Address: "+p_sMacAddress);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Request From: "+p_sLoginId);
		logger.debug("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:Request From: "+p_sRequestFrom);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,p_sDeviceId);			
			cstmt.setString(4,p_sActivity);
			cstmt.setString(5,p_sAdminVersion);
			cstmt.setString(6,p_sCatalogueVersion);
			cstmt.setString(7,p_sAutoUpdateVersion);
			cstmt.setString(8,p_sUpdatesDownloaded);
			cstmt.setString(9,p_sSchemaVersion);
			cstmt.setString(10,p_sMacAddress);
			cstmt.setString(11,p_sLoginId);
			cstmt.setString(12,p_sRequestFrom);
			
			cstmt.execute();
			iInsert=cstmt.getInt(1);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::insertAutoUpdateDeviceLogDetails "+e.getMessage());
			throw e;
		}

		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("DeviceRegDAO::insertAutoUpdateDeviceLogDetails:EXIT");
		return iInsert;
	}
	
	public static String getInsertedDeviceDetails(DeviceRegForm p_oDeviceRegForm) throws Exception{	
		logger.info("DeviceRegDAO::getInsertedDeviceDetails:ENTER");			
		String sInsertedDeviceDetails = "";
		try{
			if(p_oDeviceRegForm!=null){
				sInsertedDeviceDetails = "Air Id:"+p_oDeviceRegForm.getAirId()+"|";
				sInsertedDeviceDetails = sInsertedDeviceDetails+"Device Type:"+p_oDeviceRegForm.getDeviceType()+"|";
				sInsertedDeviceDetails = sInsertedDeviceDetails+"Device Serial No:"+p_oDeviceRegForm.getDeviceSerialNo()+"|";
				sInsertedDeviceDetails = sInsertedDeviceDetails+"Device Model:"+p_oDeviceRegForm.getDeviceModel()+"|";
				}
		// System.out.println("getInsertedDeviceDetails:"+sInsertedDeviceDetails);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getInsertedDeviceDetails:Exception "+e.getMessage());
			throw e;
		}

		logger.info("DeviceRegDAO::getInsertedDeviceDetails:EXIT");
		return sInsertedDeviceDetails;

	}
	
	public static void getUpdatedDeviceDetails(DeviceRegForm p_oDeviceRegForm,DeviceRegVO p_oDeviceRegVO, String p_sLoginId) throws Exception{	
		logger.info("getInsertedItemDetails:ENTER");			
		String sCurrentDeviceDetails = "",sPreviousDeviceDetails = "" ,sModifiedFields ="";		

		//get Current prod details
		if(p_oDeviceRegForm!=null){
			sCurrentDeviceDetails = "Air Id:"+p_oDeviceRegForm.getAirId()+"|";
			sCurrentDeviceDetails = sCurrentDeviceDetails+"Device Type:"+p_oDeviceRegForm.getDeviceType()+"|";
			sCurrentDeviceDetails = sCurrentDeviceDetails+"Device Serial No:"+p_oDeviceRegForm.getDeviceSerialNo()+"|";
			sCurrentDeviceDetails = sCurrentDeviceDetails+"Device Model:"+p_oDeviceRegForm.getDeviceModel()+"|";
			sCurrentDeviceDetails = sCurrentDeviceDetails+"Device Status:"+p_oDeviceRegForm.getStatus()+"|";
			}
		
		//get Previous prod details
		if(p_oDeviceRegVO!=null){
			sPreviousDeviceDetails = "Air Id:"+p_oDeviceRegVO.getAirId()+"|";
			sPreviousDeviceDetails = sPreviousDeviceDetails+"Device Type:"+p_oDeviceRegVO.getDeviceType()+"|";
			sPreviousDeviceDetails = sPreviousDeviceDetails+"Device Serial No:"+p_oDeviceRegVO.getDeviceSerialNo()+"|";
			sPreviousDeviceDetails = sPreviousDeviceDetails+"Device Model:"+p_oDeviceRegVO.getDeviceModel()+"|";
			sPreviousDeviceDetails = sPreviousDeviceDetails+"Device Status:"+p_oDeviceRegVO.getStatus()+"|";
			}
		
		
		
		//get modified fields name
		if(!p_oDeviceRegForm.getAirId().equals(p_oDeviceRegVO.getAirId())){		
			sModifiedFields ="Air Id|";			
		}
		if(!p_oDeviceRegForm.getDeviceType().equals(p_oDeviceRegVO.getDeviceType())){				
			sModifiedFields =sModifiedFields+"Device Type|";
		}			
		if(!p_oDeviceRegForm.getDeviceSerialNo().equals(p_oDeviceRegVO.getDeviceSerialNo())){		
			sModifiedFields =sModifiedFields+"Device Serial No|";
		}	
		if(!p_oDeviceRegForm.getDeviceModel().equals(p_oDeviceRegVO.getDeviceModel())){				
			sModifiedFields =sModifiedFields+"Device Model|";
		}

		if(!p_oDeviceRegForm.getStatus().equals(p_oDeviceRegVO.getStatus())){				
			sModifiedFields =sModifiedFields+"Device Status|";
		}
		
		DeviceRegDAO.insertDeviceAuditDetails(p_oDeviceRegForm.getDeviceId(), p_oDeviceRegForm.getAirId(), p_oDeviceRegVO.getProductKey(),sCurrentDeviceDetails,sPreviousDeviceDetails,sModifiedFields,"DEVICE UPDATED",p_sLoginId);

		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("DeviceRegDAO::getInsertedItemDetails:EXIT");
		

	}
/* Device Attendance */
	
	public static List<DeviceAttendanceVO> getDeviceAttendance(String p_sSearchType,String p_sSearchValue,String p_sActivity,String p_sFromDate,String p_sToDate) throws Exception {
		logger.info("DeviceRegDAO::getDeviceAttendance::ENTRY");

		String sQuery = "{call usp_sbh_get_device_attendance(?,?,?,?,?)}";
		logger.info("DeviceRegDAO::getDeviceAttendance::QUERY "+sQuery);
		logger.debug("DeviceRegDAO::getDeviceAttendance:Search Type: "+ p_sSearchType);
		logger.debug("DeviceRegDAO::getDeviceAttendance:Search Value: "+  p_sSearchValue);
		logger.debug("DeviceRegDAO::getDeviceAttendance:Activity: "+  p_sActivity);
		logger.debug("DeviceRegDAO::getDeviceAttendance:From Date: "+  p_sFromDate);
		logger.debug("DeviceRegDAO::getDeviceAttendance:To Date: "+  p_sToDate);
		Connection connection = null;		
		CallableStatement callStatement = null;
		String sDBData = null;
		String sDownloadType = null;
		ResultSet rs=null;
		List<DeviceAttendanceVO> alDeviceAttendance = new ArrayList<DeviceAttendanceVO>();
		DeviceAttendanceVO oDeviceAttendanceVO = null;
		try{
			connection = DBConnection.getSQL2005Connection();
			callStatement=connection.prepareCall(sQuery);
			callStatement.setString(1, p_sSearchType);
			callStatement.setString(2, p_sSearchValue);
			callStatement.setString(3, p_sActivity);
			callStatement.setString(4, p_sFromDate);
			callStatement.setString(5, p_sToDate);
			
			rs = callStatement.executeQuery();
			while(rs.next()) {
				oDeviceAttendanceVO = new DeviceAttendanceVO();

				sDBData = rs.getString("sbh_create_dt");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setDate(sDBData);					
				
				sDBData = rs.getString("id");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setId(sDBData);
				
				sDBData = rs.getString("activity");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setActivity(sDBData);

				sDBData = rs.getString("air_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setAirlineName(sDBData);

				sDBData = rs.getString("flight_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setFlightNo(sDBData);
				
				sDBData = rs.getString("device_serial_no");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setDeviceSerialNo(sDBData);
				
				sDBData = rs.getString("device_code");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setDeviceCode(sDBData);

				sDBData = rs.getString("status");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setStatus(sDBData);
				
				sDBData = rs.getString("total_orders");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setTotalOrders(sDBData);

				sDBData = rs.getString("cust_trans_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceAttendanceVO.setCustTransId(sDBData);
				
				sDBData = rs.getString("download_type");
				sDBData = sDBData==null?"":sDBData.trim();

				sDownloadType = rs.getString("download_type1");
				sDownloadType = sDownloadType==null?"":sDownloadType.trim();
				
				oDeviceAttendanceVO.setActivityDownloaded(sDBData+sDownloadType);

				
				alDeviceAttendance.add(oDeviceAttendanceVO);
				
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::getDeviceAttendance::Exception "+e.getMessage());
		}finally{
			if(connection!=null)
				connection.close();
			if(callStatement!=null)
				callStatement.close();
			if(rs!=null)
				rs.close();
		}
		logger.info("DeviceRegDAO::getDeviceAttendance::EXIT");

		return alDeviceAttendance;
	}
	
	
	public static ArrayList updateNewMacAddress(String p_sProductKey,String p_sMacAddress,String p_sNewMacAddress) throws Exception{	
		logger.info("DeviceRegDAO::updateNewMacAddress:ENTER");		

		Connection con=null;	
		CallableStatement cstmt=null;	
		ArrayList alDeviceStatus = new ArrayList();
		String sStatus = null,sDeviceStatus = null,sAirlineStatus = null; 
		String sQry="{call usp_sbh_upd_new_mac_addr(?,?,?,?,?,?,?)}";

		logger.debug("DeviceRegDAO::updateNewMacAddress:SQL QUERY "+sQry);	
		logger.debug("DeviceRegDAO::updateNewMacAddress:Mac Address "+p_sMacAddress);
		logger.debug("DeviceRegDAO::updateNewMacAddress:New Mac Address "+p_sNewMacAddress);
				
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProductKey);	
			cstmt.setString(2, p_sMacAddress);	
			cstmt.setString(3, p_sNewMacAddress);
			
			cstmt.registerOutParameter(4,Types.VARCHAR);	
			cstmt.registerOutParameter(5,Types.VARCHAR);
			cstmt.registerOutParameter(6,Types.VARCHAR);
			cstmt.setString(7, InetAddress.getLocalHost().getHostName());
			cstmt.execute();

			sStatus = cstmt.getString(4);
			sDeviceStatus = cstmt.getString(5);
			sAirlineStatus = cstmt.getString(6);
			alDeviceStatus.add(0,sStatus); 
			alDeviceStatus.add(1,sDeviceStatus);
			alDeviceStatus.add(2,sAirlineStatus);
			
			
			logger.info("DeviceRegDAO::updateNewMacAddress:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::updateNewMacAddress:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
		}
		return alDeviceStatus;
	}
}
