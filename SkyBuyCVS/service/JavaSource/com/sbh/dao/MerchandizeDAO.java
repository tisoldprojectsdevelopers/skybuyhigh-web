package com.sbh.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.email.Email;
import com.sbh.forms.PartnerPrioritizeForm;
import com.sbh.forms.SearchMerchandisePriorityForm;
import com.sbh.forms.SearchMerchandizeForm;
import com.sbh.forms.SetPriorityMerchandizeForm;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.CommentsVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.PartnerVO;
import com.sbh.vo.ProductDetailsVO;

public class MerchandizeDAO {
	private static Logger logger = LogManager.getLogger(MerchandizeDAO.class);

	public static SearchMerchandizeForm getMerchandizeDetails(SearchMerchandizeForm p_oSearchMerchandizeForm,String p_sOwnerType,String p_sMerchandizeAction) throws Exception{	
		logger.info("MerchandizeDAO:getMerchandizeDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		List<ProductDetailsVO> alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_merchandize_details(?,?,?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getMerchandizeDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getMerchandizeDetails:Owner "+p_sOwnerType);		
		logger.debug("MerchandizeDAO:getMerchandizeDetails:Search By "+p_oSearchMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getMerchandizeDetails:Search Value "+p_oSearchMerchandizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getMerchandizeDetails:Category "+p_oSearchMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO:getMerchandizeDetails:ProductType "+p_oSearchMerchandizeForm.getProductType());
		logger.debug("MerchandizeDAO:getMerchandizeDetails:EffectiveDate "+p_oSearchMerchandizeForm.getEffectiveDate());
		logger.debug("MerchandizeDAO:getMerchandizeDetails:MerchandizeAction "+p_sMerchandizeAction);

		// System.out.println("MerchandizeDAO:getMerchandizeDetails:Owner "+p_sOwnerType);	
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:Search By "+p_oSearchMerchandizeForm.getSearchBy());
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:Search Value "+p_oSearchMerchandizeForm.getSearchValue());	
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:Category "+p_oSearchMerchandizeForm.getCategory());
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:ProductType "+p_oSearchMerchandizeForm.getProductType());
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:EffectiveDate "+p_oSearchMerchandizeForm.getEffectiveDate());
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:prod Status "+p_oSearchMerchandizeForm.getProdStatus());
		// System.out.println("MerchandizeDAO:getMerchandizeDetails:MerchandizeAction "+p_sMerchandizeAction);



		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOwnerType);
			cstmt.setString(2,p_oSearchMerchandizeForm.getSearchBy());	
			cstmt.setString(3,p_oSearchMerchandizeForm.getSearchValue());
			cstmt.setString(4,p_oSearchMerchandizeForm.getCategory());
			cstmt.setString(5,p_oSearchMerchandizeForm.getEffectiveDate());
			cstmt.setString(6,p_oSearchMerchandizeForm.getProdStatus());
			cstmt.setString(7,p_sMerchandizeAction);
			cstmt.setString(8,p_oSearchMerchandizeForm.getProductType());
			cstmt.execute();
			alMerchandizeInfo=new ArrayList<ProductDetailsVO>();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				/*String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();	
				// System.out.println("Prod Id : sValidFromDate:"+oProductDetailsVO.getProdId()+":"+sValidFromDate);
				 */
				/*oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sSecondaryEmail = rs.getString("secondary_email_id");
				sSecondaryEmail = sSecondaryEmail==null?"":sSecondaryEmail.trim();				
				oProductDetailsVO.setSecondaryEmailId(sSecondaryEmail);

				String sSize = rs.getString("size");
				sSize = sSize==null?"":sSize.trim();				
				oProductDetailsVO.setSize(sSize);

				String sColor = rs.getString("color");
				sColor = sColor==null?"":sColor.trim();				
				oProductDetailsVO.setColor(sColor);

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sIsAdvt = rs.getString("is_advt");
				sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
				oProductDetailsVO.setIsAdvt(sIsAdvt);

				String sIsSpecialProd = rs.getString("is_special_prod");
				sIsSpecialProd = sIsSpecialProd==null?"":sIsSpecialProd.trim();				
				oProductDetailsVO.setIsSpecialProd(sIsSpecialProd);

				String sReturnPolicy = rs.getString("return_policy");
				sReturnPolicy = sReturnPolicy==null?"":sReturnPolicy.trim();				
				oProductDetailsVO.setReturnPolicy(sReturnPolicy);

				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_oSearchMerchandizeForm.setRecordSet(alMerchandizeInfo);
			logger.info("MerchandizeDAO:getMerchandizeDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oSearchMerchandizeForm;
	}

	public static SearchMerchandizeForm getEditSearchMerchandizeDetails(SearchMerchandizeForm p_oSearchMerchandizeForm) throws Exception{	
		logger.info("MerchandizeDAO:getEditSearchMerchandizeDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_search_merchandize_details(?,?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:SQL QUERY "+sQry);

		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Search By: "+p_oSearchMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Search Value: "+p_oSearchMerchandizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Category: "+p_oSearchMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:EffectiveDate: "+p_oSearchMerchandizeForm.getEffectiveDate());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Upload From Date: "+p_oSearchMerchandizeForm.getUploadFromDate());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Upload To Date: "+p_oSearchMerchandizeForm.getUploadToDate());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Product Status: "+p_oSearchMerchandizeForm.getProdStatus());
		logger.debug("MerchandizeDAO:getEditSearchMerchandizeDetails:Sbh Status: "+p_oSearchMerchandizeForm.getSbhStatus());


		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1,p_oSearchMerchandizeForm.getSearchBy());	
			cstmt.setString(2,p_oSearchMerchandizeForm.getSearchValue());
			cstmt.setString(3,p_oSearchMerchandizeForm.getCategory());
			cstmt.setString(4,p_oSearchMerchandizeForm.getUploadFromDate());
			cstmt.setString(5,p_oSearchMerchandizeForm.getUploadToDate());
			cstmt.setString(6,p_oSearchMerchandizeForm.getProdStatus());
			cstmt.setString(7,p_oSearchMerchandizeForm.getSbhStatus());

			cstmt.execute();
			alMerchandizeInfo=new ArrayList();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				String sSecondaryEmail = rs.getString("secondary_email_id");
				sSecondaryEmail = sSecondaryEmail==null?"":sSecondaryEmail.trim();
				oProductDetailsVO.setSecondaryEmailId(sSecondaryEmail);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));	

				String sReturnPolicy = rs.getString("return_policy");
				sReturnPolicy = sReturnPolicy==null?"":sReturnPolicy.trim();
				oProductDetailsVO.setReturnPolicy(sReturnPolicy);


				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_oSearchMerchandizeForm.setRecordSet(alMerchandizeInfo);
			logger.info("MerchandizeDAO:getEditSearchMerchandizeDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getEditSearchMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oSearchMerchandizeForm;
	}
	
	
	public static SearchMerchandizeForm getLastWeekUpdatedMerchandizeDetails(SearchMerchandizeForm p_oSearchMerchandizeForm) throws Exception{	
		logger.info("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_last_week_merchandize_details(?,?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:SQL QUERY "+sQry);

		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Search By: "+p_oSearchMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Search Value: "+p_oSearchMerchandizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Category: "+p_oSearchMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:EffectiveDate: "+p_oSearchMerchandizeForm.getEffectiveDate());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Upload From Date: "+p_oSearchMerchandizeForm.getUploadFromDate());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Upload To Date: "+p_oSearchMerchandizeForm.getUploadToDate());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Product Status: "+p_oSearchMerchandizeForm.getProdStatus());
		logger.debug("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Sbh Status: "+p_oSearchMerchandizeForm.getSbhStatus());


		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1,p_oSearchMerchandizeForm.getSearchBy());	
			cstmt.setString(2,p_oSearchMerchandizeForm.getSearchValue());
			cstmt.setString(3,p_oSearchMerchandizeForm.getCategory());
			cstmt.setString(4,p_oSearchMerchandizeForm.getUploadFromDate());
			cstmt.setString(5,p_oSearchMerchandizeForm.getUploadToDate());
			cstmt.setString(6,p_oSearchMerchandizeForm.getProdStatus());
			cstmt.setString(7,p_oSearchMerchandizeForm.getSbhStatus());

			cstmt.execute();
			alMerchandizeInfo=new ArrayList();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				String sSecondaryEmail = rs.getString("secondary_email_id");
				sSecondaryEmail = sSecondaryEmail==null?"":sSecondaryEmail.trim();
				oProductDetailsVO.setSecondaryEmailId(sSecondaryEmail);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));	

				String sReturnPolicy = rs.getString("return_policy");
				sReturnPolicy = sReturnPolicy==null?"":sReturnPolicy.trim();
				oProductDetailsVO.setReturnPolicy(sReturnPolicy);


				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_oSearchMerchandizeForm.setRecordSet(alMerchandizeInfo);
			logger.info("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getLastWeekUpdatedMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oSearchMerchandizeForm;
	}
	
	
	public static HashMap updateMerchandizeStatus(List<ProductDetailsVO> p_alMerchandizeDetails,LoginVO p_oLoginVO) throws Exception{
		logger.info("MerchandizeDAO::updateMerchandizeStatus:ENTER");
		ProductDetailsVO oProductDetailsVO=null;
		ArrayList<ProductDetailsVO> alProductDetails=null;
		int iIsUpdate=-1;
		HashMap p_hmMerchandizeDetails=null;
		String sSbhProdStatusDesc = "";
		try{	
			p_hmMerchandizeDetails=new HashMap();
			for(int i=0;i<p_alMerchandizeDetails.size();i++){
				oProductDetailsVO=(ProductDetailsVO)p_alMerchandizeDetails.get(i);
				String sProdId = oProductDetailsVO.getProdId();
				String sSbhPrice = oProductDetailsVO.getSbhPrice();
				String sSbhProdStatus = oProductDetailsVO.getSbhProdStatus();
				String sSbhComments = oProductDetailsVO.getSbhComment();

				sProdId = sProdId == null?"":sProdId.trim();
				if(sSbhPrice==null || sSbhPrice.trim()=="")
					sSbhPrice="0";
				sSbhProdStatus = sSbhProdStatus == null?"":sSbhProdStatus.trim();
				sSbhComments = sSbhComments == null?"":sSbhComments.trim();	

				if(sSbhProdStatus.equalsIgnoreCase("A"))
					sSbhProdStatusDesc = "PRODUCT ACCEPTED";
				else if(sSbhProdStatus.equalsIgnoreCase("R"))
					sSbhProdStatusDesc = "PRODUCT REJECTED";

				iIsUpdate=MerchandizeDAO.updateMerchandizeDetails(sProdId,sSbhPrice,sSbhProdStatus,sSbhComments,p_oLoginVO.getUserId());

				//update product audit details
				if(sSbhProdStatus.equalsIgnoreCase("A") || sSbhProdStatus.equalsIgnoreCase("R"))
					CatalogueDAO.insertProdAuditDetails(sProdId, p_oLoginVO.getRefId(),  p_oLoginVO.getUserType(),"SkyBuy Price :"+sSbhPrice+"|", null,"SkyBuy Price|",sSbhComments,p_oLoginVO.getUserId(),sSbhProdStatusDesc);

				if(iIsUpdate>0){
					if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("A")){
						if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_A")){
							alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_A");
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}else{
							alProductDetails=new ArrayList<ProductDetailsVO>();
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}
					}else if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
						if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
							if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_R")){
								alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_R");
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}else{
								alProductDetails=new ArrayList<ProductDetailsVO>();
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}
						}
					}
				}
			}	logger.info("MerchandizeDAO::updateMerchandizeStatus:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("MerchandizeDAO:updateMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}	


		return p_hmMerchandizeDetails;
	}
	public static int updateMerchandizeDetails(String p_sProdId,String p_sSbhPrice,String p_sSbhProdStatus,String p_sSbhComments,String p_sUpdateId) throws Exception{	
		logger.info("MerchandizeDAO::updateMerchandizeDetails:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_merchandize_details(?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO::updateMerchandizeDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Product Id "+p_sProdId);		
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Sbh Price "+p_sSbhPrice);		
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Sbh Prod Status "+p_sSbhProdStatus);		
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Comments "+p_sSbhComments);	
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Upadte Id "+p_sUpdateId);	
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);

			cstmt.setString(2,p_sProdId);
			p_sSbhPrice=p_sSbhPrice==null?"0":p_sSbhPrice.trim();

			if(p_sSbhPrice.charAt(0)=='$'){
				cstmt.setString(3,p_sSbhPrice.substring(1,p_sSbhPrice.length()));
			}else{
				cstmt.setString(3,p_sSbhPrice);
			}

			cstmt.setString(4,p_sSbhProdStatus);
			cstmt.setString(5,p_sSbhComments);
			cstmt.setString(6,p_sUpdateId);

			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("MerchandizeDAO::updateMerchandizeDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO::updateMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}
	public static boolean sendEmail(ArrayList p_alMerchandizeDeatils,HashMap p_hmTags, String p_sTagName, String p_sReplaceText,
			String sProdStatus,String p_sLoginId,String p_sReqeustFrom, List<CategoryVO> p_alCategory) throws Exception{
		logger.info("MerchandizeDAO:sendEmail:ENTER");
		String sEmailContent="";
		String sMerchandizeDetails="";
		String sCategory = null;
		String sToAddr="",sAdminEmailAddr=null,sEmailSubject=null,sEmailTemplateId = null,sFromEmailAddr=null,sEmailProdDetails=null;
		ProductDetailsVO oProductDetailsVO=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sSecondaryEmail = "";
		String sEmailSendTo = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");


			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			oProductDetailsVO=(ProductDetailsVO)p_alMerchandizeDeatils.get(0);
			if(oProductDetailsVO!=null){
				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
					if(sProdStatus.equalsIgnoreCase("A")){
						if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
							sEmailTemplateId =oBundle.getString("email.checkout.admin.accept");
						}else{
							sEmailTemplateId =oBundle.getString("email.vendor.prod.accept");
						}
					}else{
						if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
							sEmailTemplateId =oBundle.getString("email.checkout.admin.reject");
						}else{
							sEmailTemplateId =oBundle.getString("email.vendor.prod.reject");
						}
					}
				}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
					if(sProdStatus.equalsIgnoreCase("A")){
						if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
							sEmailTemplateId =oBundle.getString("email.airline.prod.accept");
						}else{
							sEmailTemplateId =oBundle.getString("email.airline.package.accept");
						}
					}else{
						if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
							sEmailTemplateId =oBundle.getString("email.airline.prod.reject");
						}else{
							sEmailTemplateId =oBundle.getString("email.airline.package.reject");
						}
					}
				}

				oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

				sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
				sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

				if(oProductDetailsVO!=null){
					sToAddr=oProductDetailsVO.getEmailId();
					sToAddr=sToAddr==null?"":sToAddr.trim();
					if(sToAddr!=null && !sToAddr.equalsIgnoreCase("")){
						sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
						if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
							if(!sToAddr.contains(sSecondaryEmail)){
								sToAddr = sToAddr + ","+sSecondaryEmail;
							}
						}
					}
					
					sEmailSendTo = System.getProperty("AcceptRejectMailTo");
					if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
						sToAddr = sAdminEmailAddr;
					}
					
					sCategory = Utils.convertToCategoryName(p_alCategory, oProductDetailsVO.getCateId());

					if(sToAddr==null || sToAddr.equalsIgnoreCase("")){
						sToAddr=oProductDetailsVO.getVendorEmail();
						sToAddr=sToAddr==null?"":sToAddr.trim();

						sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
						if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
							if(!sToAddr.contains(sSecondaryEmail)){
								sToAddr = sToAddr + ","+sSecondaryEmail;
							}
						}

					}
					//sToAddr = sToAddr+","+sAdminEmailAddr;

					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
						p_hmTags.put("{{Owner}}","Vendor");						
						p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());	
					}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
						p_hmTags.put("{{Owner}}","Air Charter");
						p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());	
					}

					p_hmTags.put("{{Name}}",Utils.toTitleCase(oProductDetailsVO.getOwnerName()));
				}			

				if(oEmailVO!=null){
					sEmailSubject=oEmailVO.getEmailSubject();
					sEmailContent=oEmailVO.getEmailContent();
					sEmailProdDetails = oEmailVO.getEmailOptionalContent();
					sCcAddr=oEmailVO.getEmailCcList();
					sBccAddr=oEmailVO.getEmailBccList();
				}
				sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

				sEmailProdDetails=sEmailProdDetails==null?"":sEmailProdDetails.trim();
				for(int i=0;i<p_alMerchandizeDeatils.size();i++){
					String sSplInsturction ="";
					oProductDetailsVO=(ProductDetailsVO)p_alMerchandizeDeatils.get(i);

					// System.out.println("prod Img path:"+oProductDetailsVO.getMainImgPath());
					//Prod Details
					p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
					p_hmTags.put("{{ProdCode}}",oProductDetailsVO.getProdCode());
					p_hmTags.put("{{Brand}}",oProductDetailsVO.getBrandName());
					p_hmTags.put("{{ItemName}}",oProductDetailsVO.getProdTitle());
					p_hmTags.put("{{ShortDesc}}",oProductDetailsVO.getShortDesc());
					p_hmTags.put("{{Category}}",sCategory);
					p_hmTags.put("{{FullDescription}}",oProductDetailsVO.getLongDesc());
					p_hmTags.put("{{ReturnPolicy}}",oProductDetailsVO.getReturnPolicy());
					p_hmTags.put("{{Comments}}",oProductDetailsVO.getComments());
					if(oProductDetailsVO.getSbhPrice()!=null && !"".equalsIgnoreCase(oProductDetailsVO.getSbhPrice())){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsVO.getSbhPrice())));
					}else{
						p_hmTags.put("{{SbhPrice}}","");
					}
					p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getEffectiveDate());
					p_hmTags.put("{{InShop}}",oProductDetailsVO.getInShopStatus());
					p_hmTags.put("{{Comment}}",oProductDetailsVO.getSbhComment());
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
						/*String sValidFrom = oProductDetailsVO.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsVO.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();*/
						String sInstrustion = oProductDetailsVO.getInstructions();
						sInstrustion = sInstrustion == null?"":sInstrustion.trim();
						p_hmTags.put("{{SplInstruction}}",sInstrustion);
						/*if(sValidFrom.trim().length()>0)
						 sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						 sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

						/*if(sInstrustion.trim().length()>0)
							sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

						p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/
					}
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
						String sSize = oProductDetailsVO.getSize();
						String sColor = oProductDetailsVO.getColor();					

						if(sSize!=null && sSize.trim().length()>0)
							p_hmTags.put("{{Size}}",sSize);
						else
							p_hmTags.put("{{Size}}","N/A");
						if(sColor!=null && sColor.trim().length()>0)
							p_hmTags.put("{{Color}}",sColor);
						else
							p_hmTags.put("{{Color}}","N/A");
					}	

					sMerchandizeDetails=sMerchandizeDetails+replaceTagValues(p_hmTags,sEmailProdDetails,null,null);
				}

				p_hmTags.put("{{ProdDetails}}",sMerchandizeDetails);

				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
				sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();

				EmailLogVO oEmailLogVo=new EmailLogVO();
				Email oEmail=new Email();
				oEmailLogVo.setEmailFrom(sFromEmailAddr);
				oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
				if(sCcAddr!=null && sCcAddr.trim().length()>0){
					sCcAddr =sAdminEmailAddr+","+sCcAddr;						
				}else{
					sCcAddr =sAdminEmailAddr;
				}
				oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
				oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
				oEmailLogVo.setEmailSubject(sEmailSubject);			
				oEmailLogVo.setEmailText(sEmailContent);

				System.out.print("EmailContent "+sEmailContent);

				boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
				if(bEmailSent)				
					oEmailLogVo.setEmailStatus("Y");
				else
					oEmailLogVo.setEmailStatus("N");
				/*oEmailLogVo.setOwnerId(0);
				oEmailLogVo.setOwnerType("Admin");*/
				oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
				oEmailLogVo.setEmailToAddr(sToAddr);
				oEmailLogVo.setEmailCcAddr("");

				/** Insert Email Log Details **/
				EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			}
			logger.info("MerchandizeDAO::sendEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("MerchandizeDAO:sendEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}


	public static boolean addProductComments(String p_sOwnerId,String p_sOwnerType,String 

			p_sProdId,String p_sComments,String p_sUserId) throws Exception {

		logger.info("MerchandizeDAO::addProductComments::ENTRY");
		ProductDetailsVO oProductDetailsVO=null;
		boolean bIsInsert = false;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_product_comments(?,?,?,?,?,?)}";
		logger.info("MerchandizeDAO::addProductComments::QUERY "+sQry);
		logger.info("MerchandizeDAO::addProductComments::Owner Id: "+p_sOwnerId);
		logger.info("MerchandizeDAO::addProductComments::Owner Type: "+p_sOwnerType);
		logger.info("MerchandizeDAO::addProductComments::Prod Id: "+p_sProdId);
		logger.info("MerchandizeDAO::addProductComments::Comments: "+p_sComments);
		logger.info("MerchandizeDAO::addProductComments::User Id: "+p_sUserId);
		int iIsInsert = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sOwnerId);
			cstmt.setString(3,p_sOwnerType);
			cstmt.setString(4,p_sProdId);
			cstmt.setString(5,p_sComments);
			cstmt.setString(6,p_sUserId);
			iIsInsert=cstmt.executeUpdate();
			if(iIsInsert > 0){
				bIsInsert = true;
			}else{
				bIsInsert = false;
			}
			logger.info("MerchandizeDAO::addProductComments::EXIT");
		}catch(Exception e) {


			logger.debug("MerchandizeDAO::addProductComments::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}


		return bIsInsert;
	}


	public static List<CommentsVO> getProductComments(String p_sOwnerId,String 

			p_sOwnerType,String p_sProdId,String p_sUserId) throws Exception{	
		logger.info("MerchandizeDAO::getProductComments:ENTER");		
		List<CommentsVO> alProductComments = new ArrayList<CommentsVO>();
		CommentsVO oCommentsVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDBData = null;
		String sQry="{call usp_sbh_get_product_comments(?,?,?,?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy  hh:mm"  ) ; 
		logger.debug("MerchandizeDAO::getProductComments:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO::getProductComments:Owner Id: "+p_sOwnerId);	
		logger.debug("MerchandizeDAO::getProductComments:Owner Type: "+p_sOwnerType);	
		logger.debug("MerchandizeDAO::getProductComments:Prod Id: "+p_sProdId);
		logger.debug("MerchandizeDAO::getProductComments:User Id: "+p_sUserId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOwnerId);	
			cstmt.setString(2,p_sOwnerType);	
			cstmt.setString(3,p_sProdId);
			cstmt.setString(4,p_sUserId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oCommentsVO = new CommentsVO();

				sDBData = rs.getString("comments");
				sDBData = sDBData==null?"":sDBData.trim();
				oCommentsVO.setComments(sDBData);

				if(rs.getTimestamp("updt_dt")!=null){
					sDBData=sdfOutput.format(rs.getTimestamp("updt_dt"));
					sDBData = sDBData==null?"":sDBData.trim();
					oCommentsVO.setDate(sDBData);
				}else{
					oCommentsVO.setDate("");
				}

				sDBData = rs.getString("user_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oCommentsVO.setUserId(sDBData);


				alProductComments.add(oCommentsVO);
			}
			logger.info("MerchandizeDAO::getProductComments:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO::getProductComments:Exception"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alProductComments;
	}


	public static boolean addAdvertisementComments(String p_sOwnerId,String p_sOwnerType,String 

			p_sProdId,String p_sComments,String p_sUserId) throws Exception {

		logger.info("MerchandizeDAO::addAdvertisementComments::ENTRY");
		ProductDetailsVO oProductDetailsVO=null;
		boolean bIsInsert = false;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_advertisement_comments(?,?,?,?,?,?)}";
		logger.info("MerchandizeDAO::addAdvertisementComments::QUERY "+sQry);
		logger.info("MerchandizeDAO::addAdvertisementComments::Owner Id: "+p_sOwnerId);
		logger.info("MerchandizeDAO::addAdvertisementComments::Owner Type: "+p_sOwnerType);
		logger.info("MerchandizeDAO::addAdvertisementComments::Prod Id: "+p_sProdId);
		logger.info("MerchandizeDAO::addAdvertisementComments::Comments: "+p_sComments);
		logger.info("MerchandizeDAO::addAdvertisementComments::User Id: "+p_sUserId);
		int iIsInsert = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sOwnerId);
			cstmt.setString(3,p_sOwnerType);
			cstmt.setString(4,p_sProdId);
			cstmt.setString(5,p_sComments);
			cstmt.setString(6,p_sUserId);
			iIsInsert=cstmt.executeUpdate();
			if(iIsInsert > 0){
				bIsInsert = true;
			}else{
				bIsInsert = false;
			}
			logger.info("MerchandizeDAO::addAdvertisementComments::EXIT");
		}catch(Exception e) {

			logger.debug("MerchandizeDAO::addAdvertisementComments::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}


		return bIsInsert;
	}


	public static SearchMerchandizeForm getAirlineAdvtDetails(SearchMerchandizeForm p_oSearchMerchandizeForm,String p_sOwnerType,String p_sMerchandizeAction) throws Exception{	
		logger.info("MerchandizeDAO:getAirlineAdvtDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_airline_advt_details(?,?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:Owner "+p_sOwnerType);		
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:Search By "+p_oSearchMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:Search Value "+p_oSearchMerchandizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:Category "+p_oSearchMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:EffectiveDate "+p_oSearchMerchandizeForm.getEffectiveDate());
		logger.debug("MerchandizeDAO:getAirlineAdvtDetails:MerchandizeAction "+p_sMerchandizeAction);





		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOwnerType);
			cstmt.setString(2,p_oSearchMerchandizeForm.getSearchBy());	
			cstmt.setString(3,p_oSearchMerchandizeForm.getSearchValue());
			cstmt.setString(4,p_oSearchMerchandizeForm.getCategory());
			cstmt.setString(5,p_oSearchMerchandizeForm.getEffectiveDate());
			cstmt.setString(6,p_oSearchMerchandizeForm.getProdStatus());
			cstmt.setString(7,p_sMerchandizeAction);
			cstmt.execute();
			alMerchandizeInfo=new ArrayList();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				/*	String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();	
				// System.out.println("Prod Id : sValidFromDate:"+oProductDetailsVO.getProdId()+":"+sValidFromDate);
				 */	
				/*oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sOwnerContactName = rs.getString("owner_contact_name");
				sOwnerContactName = sOwnerContactName==null?"":sOwnerContactName.trim();				
				oProductDetailsVO.setOwnerContactName(sOwnerContactName);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));


				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_oSearchMerchandizeForm.setRecordSet(alMerchandizeInfo);
			logger.info("MerchandizeDAO:getAirlineAdvtDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getAirlineAdvtDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oSearchMerchandizeForm;
	}



	public static HashMap updateAirlineAdvtStatus(List<ProductDetailsVO> p_alMerchandizeDetails,LoginVO p_oLoginVO) throws Exception{
		logger.info("MerchandizeDAO::updateAirlineAdvtStatus:ENTER");
		ProductDetailsVO oProductDetailsVO=null;
		ArrayList<ProductDetailsVO> alProductDetails=null;
		int iIsUpdate=-1;
		HashMap p_hmMerchandizeDetails=null;
		String sSbhProdStatusDesc = "";
		try{	
			p_hmMerchandizeDetails=new HashMap();
			for(int i=0;i<p_alMerchandizeDetails.size();i++){
				oProductDetailsVO=(ProductDetailsVO)p_alMerchandizeDetails.get(i);
				String sProdId = oProductDetailsVO.getProdId();
				String sSbhPrice = oProductDetailsVO.getSbhPrice();
				String sSbhProdStatus = oProductDetailsVO.getSbhProdStatus();
				String sSbhComments = oProductDetailsVO.getSbhComment();

				sProdId = sProdId == null?"":sProdId.trim();
				if(sSbhPrice==null || sSbhPrice.trim()=="")
					sSbhPrice="0";
				sSbhProdStatus = sSbhProdStatus == null?"":sSbhProdStatus.trim();
				sSbhComments = sSbhComments == null?"":sSbhComments.trim();	

				if(sSbhProdStatus.equalsIgnoreCase("A"))
					sSbhProdStatusDesc = "PRODUCT ACCEPTED";
				else if(sSbhProdStatus.equalsIgnoreCase("R"))
					sSbhProdStatusDesc = "PRODUCT REJECTED";

				iIsUpdate=MerchandizeDAO.updateAirlineAdvtDetails(sProdId,sSbhProdStatus,sSbhComments,p_oLoginVO.getUserId());

				//update product audit details
				if(sSbhProdStatus.equalsIgnoreCase("A") || sSbhProdStatus.equalsIgnoreCase("R"))
					CatalogueDAO.insertProdAuditDetails(sProdId, p_oLoginVO.getRefId(),  p_oLoginVO.getUserType(),"SkyBuy Price :"+sSbhPrice+"|", null,"SkyBuy Price|",sSbhComments,p_oLoginVO.getUserId(),sSbhProdStatusDesc);

				if(iIsUpdate>0){
					if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("A")){
						if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_A")){
							alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_A");
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}else{
							alProductDetails=new ArrayList<ProductDetailsVO>();
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}
					}else if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
						if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
							if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_R")){
								alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_R");
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}else{
								alProductDetails=new ArrayList<ProductDetailsVO>();
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}
						}
					}
				}
			}	logger.info("MerchandizeDAO::updateAirlineAdvtStatus:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("MerchandizeDAO:updateAirlineAdvtStatus:Exception "+e.getMessage());
			throw e;
		}	


		return p_hmMerchandizeDetails;
	}

	private static int updateAirlineAdvtDetails(String p_sProdId,String p_sSbhProdStatus,String p_sSbhComments,String p_sUpdateId) throws Exception{	
		logger.info("MerchandizeDAO::updateAirlineAdvtDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_airline_advt_details(?,?,?,?,?)}";

		logger.debug("MerchandizeDAO::updateAirlineAdvtDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO::updateAirlineAdvtDetails:Product Id "+p_sProdId);		
		logger.debug("MerchandizeDAO::updateAirlineAdvtDetails:Sbh Prod Status "+p_sSbhProdStatus);		
		logger.debug("MerchandizeDAO::updateAirlineAdvtDetails:Comments "+p_sSbhComments);	
		logger.debug("MerchandizeDAO::updateAirlineAdvtDetails:Upadte Id "+p_sUpdateId);	
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);

			cstmt.setString(2,p_sProdId);
			cstmt.setString(3,p_sSbhProdStatus);
			cstmt.setString(4,p_sSbhComments);
			cstmt.setString(5,p_sUpdateId);

			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("MerchandizeDAO::updateAirlineAdvtDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO::updateAirlineAdvtDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}

	public static ArrayList getAdvertisementCatalogueDetails(SearchMerchandizeForm p_oSearchMerchandizeForm,String p_sOwnerId,String p_sOwnerType) throws Exception{	
		logger.info("MerchandizeDAO::getAdvertisementCatalogueDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_advertisement_details(?,?,?,?,?,?,?,?,?)}";

		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Category: "+p_oSearchMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Upload From Date: "+p_oSearchMerchandizeForm.getUploadFromDate());	
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Upload To Date: "+p_oSearchMerchandizeForm.getUploadToDate());		
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Prod Status: "+p_oSearchMerchandizeForm.getProdStatus());			
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Sbh Status: "+p_oSearchMerchandizeForm.getSbhStatus());			
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Owner Type: "+p_sOwnerType);
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Owner Id: "+p_sOwnerId);
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Search By: "+p_oSearchMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO::getAdvertisementCatalogueDetails:Search Value: "+p_oSearchMerchandizeForm.getSearchValue());	
		ResultSet rs=null;
		ArrayList alCatalogueInfo=new ArrayList();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchMerchandizeForm.getCategory());
			cstmt.setString(2,p_oSearchMerchandizeForm.getUploadFromDate());	
			cstmt.setString(3,p_oSearchMerchandizeForm.getUploadToDate());		
			cstmt.setString(4,p_oSearchMerchandizeForm.getProdStatus());			
			cstmt.setString(5,p_oSearchMerchandizeForm.getSbhStatus());			
			cstmt.setString(6,p_sOwnerType);
			cstmt.setString(7,p_sOwnerId);
			cstmt.setString(8,p_oSearchMerchandizeForm.getSearchBy());
			cstmt.setString(9,p_oSearchMerchandizeForm.getSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));

				/*oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	*/

				String sShortDesc = rs.getString("short_desc");
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDesc = sShortDesc.replaceAll(" ","&nbsp;");
				oProductDetailsVO.setShortDesc(sShortDesc);	

				String sLongDesc = rs.getString("long_desc");
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll(" ","&nbsp;");	
				oProductDetailsVO.setLongDesc(sLongDesc);	

				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else if(rs.getString("in_shop_status").equalsIgnoreCase("I")){
					oProductDetailsVO.setInShopStatus("InActive");
				}
				oProductDetailsVO.setSbhProdStatus(rs.getString("sbh_prod_status"));

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));


				alCatalogueInfo.add(oProductDetailsVO);

			}

			p_oSearchMerchandizeForm.setRecordSet(alCatalogueInfo);
			logger.info("MerchandizeDAO::getAdvertisementCatalogueDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO::getAdvertisementCatalogueDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alCatalogueInfo;
	}

	public static ArrayList getMerchandizeDetailsWithPriority(SearchMerchandisePriorityForm oSearchMerchandisePriorityForm) throws Exception{	
		logger.info("MerchandizeDAO:getMerchandizeDetailsWithPriority:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null,sImageUploadPath = null,sOriginalImgPath = null;
		sImageUploadPath = System.getProperty("ImageUploadPath"); 
		sImageUploadPath = sImageUploadPath==null?"":sImageUploadPath.trim();
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_merchandize_cf_priority(?,?,?,?)}";

		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:Search By: "+oSearchMerchandisePriorityForm.getSearchBy());	
		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:Search Value: "+oSearchMerchandisePriorityForm.getSearchValue());
		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:Category:"+oSearchMerchandisePriorityForm.getCategory());
		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:Coverflow Status: "+oSearchMerchandisePriorityForm.getCoverflowStatus());

		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1,oSearchMerchandisePriorityForm.getSearchBy());	
			cstmt.setString(2,oSearchMerchandisePriorityForm.getSearchValue());
			cstmt.setString(3,oSearchMerchandisePriorityForm.getCategory());
			cstmt.setString(4,oSearchMerchandisePriorityForm.getCoverflowStatus());

			cstmt.execute();
			alMerchandizeInfo=new ArrayList();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;

						sOriginalImgPath = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+sImageType;
						oProductDetailsVO.setOrignalImagePath(sOriginalImgPath);
						oProductDetailsVO.setOrignalImageType(sImageType);
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				String sCoverflowStatus = rs.getString("coverflow_status");
				sCoverflowStatus = sCoverflowStatus==null?"":sCoverflowStatus.trim();
				oProductDetailsVO.setCoverflowStatus(sCoverflowStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));	


				alMerchandizeInfo.add(oProductDetailsVO);

			}
			oSearchMerchandisePriorityForm.setRecordSet(alMerchandizeInfo);
			logger.info("MerchandizeDAO:getMerchandizeDetailsWithPriority:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getMerchandizeDetailsWithPriority:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alMerchandizeInfo;
	}

	public static ArrayList getAllMerchandizeDetailsWithPriority() throws Exception{	
		logger.info("MerchandizeDAO:getAllMerchandizeDetailsWithPriority:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_merchandize_cf_priority(?,?,?,?)}";

		logger.debug("MerchandizeDAO:getAllMerchandizeDetailsWithPriority:SQL QUERY "+sQry);


		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1,"All");	
			cstmt.setString(2,"");
			cstmt.setString(3,"All");
			cstmt.setString(4,"All");

			cstmt.execute();
			alMerchandizeInfo=new ArrayList();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				String sCoverflowStatus = rs.getString("coverflow_status");
				sCoverflowStatus = sCoverflowStatus==null?"":sCoverflowStatus.trim();
				oProductDetailsVO.setCoverflowStatus(sCoverflowStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));	


				alMerchandizeInfo.add(oProductDetailsVO);

			}

			logger.info("MerchandizeDAO:getAllMerchandizeDetailsWithPriority:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getAllMerchandizeDetailsWithPriority:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alMerchandizeInfo;
	}


	public static int updateMerchandizePriority(String p_sProdId,String p_sCoverflowStatus,String p_sUserId) throws Exception{	
		logger.info("MerchandizeDAO::updateMerchandizeDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_cf_merchadise_priority(?,?,?)}";

		logger.debug("MerchandizeDAO::updateMerchandizeDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Product Id "+p_sProdId);		
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:Coverflow Status "+p_sCoverflowStatus);		
		logger.debug("MerchandizeDAO::updateMerchandizeDetails:User Id "+p_sUserId);	
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdId);
			cstmt.setString(2,p_sCoverflowStatus);
			cstmt.setString(3,p_sUserId);

			cstmt.executeUpdate();


			logger.info("MerchandizeDAO::updateMerchandizeDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO::updateMerchandizeDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}

	public static SetPriorityMerchandizeForm getProductDetailsToSetMerchandizePriority(SetPriorityMerchandizeForm p_sSetPriorityMerchandizeForm) throws Exception {
		logger.info("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:ENTER");

		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		List<ProductDetailsVO> alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_product_details_to_set_merchandize_priority(?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Search By: "+p_sSetPriorityMerchandizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Search Value: "+p_sSetPriorityMerchandizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Category: "+p_sSetPriorityMerchandizeForm.getCategory());
		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Prod Status: "+p_sSetPriorityMerchandizeForm.getProdStatus());
		logger.debug("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Sbh Status: "+p_sSetPriorityMerchandizeForm.getSbhStatus());

		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sSetPriorityMerchandizeForm.getSearchBy());	
			cstmt.setString(2,p_sSetPriorityMerchandizeForm.getSearchValue());
			cstmt.setString(3,p_sSetPriorityMerchandizeForm.getCategory());
			cstmt.setString(4,p_sSetPriorityMerchandizeForm.getProdStatus());
			cstmt.setString(5,p_sSetPriorityMerchandizeForm.getSbhStatus());

			cstmt.execute();
			alMerchandizeInfo=new ArrayList<ProductDetailsVO>();
			rs = cstmt.getResultSet();
//			int counter = 0;

			while(rs.next()){

				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));
				oProductDetailsVO.setPriorityOfMerchandize(rs.getString("priority_of_merchandize"));
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));	

				/*
				 * This value is assigned in the productDetailsVO is because the default value of
				 * priorityOfMerchandize is zero. Since it is zero the client page contains the 
				 * name of the text box is same for all. So differentiate the dynamically generated
				 * name, the value is set in a variable name counter in ProductDetailsVO.  

				counter++;
				oProductDetailsVO.setCounter(counter+"");*/

				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_sSetPriorityMerchandizeForm.setRecordSet(alMerchandizeInfo);

			logger.info("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getProductDetailsToSetMerchandizePriority:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_sSetPriorityMerchandizeForm;
	}
	public static List<ProductDetailsVO> getAllMerchandizeDetailsWithPriorityOfMerchandize() throws Exception{	
		logger.info("MerchandizeDAO:getAllMerchandizeDetailsWithPriorityOfMerchandize:ENTER");

		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		List<ProductDetailsVO> alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_all_merchandize_priority(?,?,?,?,?)}";

		logger.debug("MerchandizeDAO:getAllMerchandizeDetailsWithPriorityOfMerchandize:SQL QUERY "+sQry);


		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1,"All");	
			cstmt.setString(2,"");
			cstmt.setString(3,"All");
			cstmt.setString(4,"All");
			cstmt.setString(5,"All");

			cstmt.execute();
			alMerchandizeInfo=new ArrayList<ProductDetailsVO>();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);

				String sCoverflowStatus = rs.getString("coverflow_status");
				sCoverflowStatus = sCoverflowStatus==null?"":sCoverflowStatus.trim();
				oProductDetailsVO.setCoverflowStatus(sCoverflowStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setPriorityOfMerchandize(rs.getString("priority_of_merchandize"));


				alMerchandizeInfo.add(oProductDetailsVO);

			}

			logger.info("MerchandizeDAO:getAllMerchandizeDetailsWithPriorityOfMerchandize:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getAllMerchandizeDetailsWithPriorityOfMerchandize:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alMerchandizeInfo;
	}
	public static void updatePriorityOfMerchandise(ProductDetailsVO p_oProductDetailsVO,String p_sLoginId) throws Exception {

		logger.info("MerchandizeDAO:updatePriorityOfMerchandise:ENTER");

		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_upd_merchandize_priority(?,?,?)}";
		logger.debug("MerchandizeDAO:updatePriorityOfMerchandise:SQL QUERY:"+sQry);
		logger.debug("MerchandizeDAO:updatePriorityOfMerchandise:Prod Id:"+p_oProductDetailsVO.getProdId());	
		logger.debug("MerchandizeDAO:updatePriorityOfMerchandise:Priority Of Merchandise:"+p_oProductDetailsVO.getPriorityOfMerchandize());
		logger.debug("MerchandizeDAO:updatePriorityOfMerchandise:Login Id:"+p_sLoginId);
		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oProductDetailsVO.getProdId());	
			cstmt.setString(2,p_oProductDetailsVO.getPriorityOfMerchandize());
			cstmt.setString(3,p_sLoginId);

			cstmt.executeUpdate();

			logger.info("MerchandizeDAO:updatePriorityOfMerchandise:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:updatePriorityOfMerchandise:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}

	public static ArrayList getCoverflowMerchandise() throws Exception{	
		logger.info("MerchandizeDAO:getCoverflowMerchandise:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null,sImageUploadPath = null,sOriginalImgPath = null;
		sImageUploadPath = System.getProperty("ImageUploadPath"); 
		sImageUploadPath = sImageUploadPath==null?"":sImageUploadPath.trim();
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		ArrayList<ProductDetailsVO> alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_coverflow_merchandise}";

		logger.debug("MerchandizeDAO:getMerchandizeDetailsWithPriority:SQL QUERY "+sQry);

		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			alMerchandizeInfo=new ArrayList<ProductDetailsVO>();
			rs = cstmt.executeQuery();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				/*oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));*/
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				/*oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"0.00":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();*/

				sOriginalImgPath = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+"jpg";
				oProductDetailsVO.setOrignalImagePath(sOriginalImgPath);
				oProductDetailsVO.setOrignalImageType(sImageType);

				/*if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;

						sOriginalImgPath = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+sImageType;
						oProductDetailsVO.setOrignalImagePath(sOriginalImgPath);
						oProductDetailsVO.setOrignalImageType(sImageType);
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				String sComments = rs.getString("sbh_comments");
			    sComments = sComments==null?"":sComments.trim();
			    oProductDetailsVO.setSbhComment(sComments);

			    String sCoverflowStatus = rs.getString("coverflow_status");
			    sCoverflowStatus = sCoverflowStatus==null?"":sCoverflowStatus.trim();
			    oProductDetailsVO.setCoverflowStatus(sCoverflowStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));*/	


				alMerchandizeInfo.add(oProductDetailsVO);

			}

			logger.info("MerchandizeDAO:getCoverflowMerchandise:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getCoverflowMerchandise:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alMerchandizeInfo;
	}

	public static PartnerPrioritizeForm getPartnerDetails(PartnerPrioritizeForm p_oPrioritizeForm) throws Exception {
		logger.info("MerchandizeDAO:getPartnerDetails:ENTER");

		PartnerVO oPartnerVO=null;
		List<PartnerVO> alPatnerInfo=new ArrayList<PartnerVO>();
		Connection con=null;		
		String sDBData = null;
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_partner_details(?,?,?)}";

		logger.debug("MerchandizeDAO:getPartnerDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getPartnerDetails:Search By: "+p_oPrioritizeForm.getSearchBy());
		logger.debug("MerchandizeDAO:getPartnerDetails:Search Value: "+p_oPrioritizeForm.getSearchValue());
		logger.debug("MerchandizeDAO:getPartnerDetails:Status : "+p_oPrioritizeForm.getStatus());

		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oPrioritizeForm.getSearchBy());	
			cstmt.setString(2,p_oPrioritizeForm.getSearchValue());
			cstmt.setString(3,p_oPrioritizeForm.getStatus());
			cstmt.execute();
			rs = cstmt.getResultSet();

			while(rs.next()){

				oPartnerVO=new PartnerVO();
				
				sDBData = rs.getString("owner_type");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerType(sDBData);
				
				sDBData = rs.getString("owner_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerId(sDBData);
				
				sDBData = rs.getString("owner_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerName(sDBData);
				
				sDBData = rs.getString("owner_contact_name1");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerContactName(sDBData);
				
				sDBData = rs.getString("owner_phone");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerPhone(sDBData);
				
				sDBData = rs.getString("owner_phone_ext1");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerPhoneExt(sDBData);
				
				sDBData = rs.getString("owner_email1");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerEmail(sDBData);
				
				sDBData = rs.getString("owner_fax");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerFax(sDBData);
				
				sDBData = rs.getString("owner_mobile1");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerMobile(sDBData);
				
				sDBData = rs.getString("owner_addr1");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerAddress1(sDBData);
				
				sDBData = rs.getString("owner_addr2");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerAddress2(sDBData);
				
				sDBData = rs.getString("owner_city");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerCity(sDBData);
				
				sDBData = rs.getString("owner_state");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerState(sDBData);
				
				sDBData = rs.getString("owner_country");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerCountry(sDBData);
				
				sDBData = rs.getString("owner_zip");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerZip(sDBData);
				
				sDBData = rs.getString("owner_status");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerStatus(sDBData);
				
				sDBData = rs.getString("owner_charge_type");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerChargeType(sDBData);
				
				sDBData = rs.getString("owner_po_pct");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerPOPct(sDBData);
				
				sDBData = rs.getString("owner_priority");
				sDBData = sDBData == null?"":sDBData.trim();
				oPartnerVO.setOwnerPriority(sDBData);
				
				alPatnerInfo.add(oPartnerVO);

			}
			p_oPrioritizeForm.setRecordSet(alPatnerInfo);

			logger.info("MerchandizeDAO:getPartnerDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getPartnerDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oPrioritizeForm;
	}
	
	public static HashMap<String,HashMap> getPartnerPriorityDetails() throws Exception {
		logger.info("MerchandizeDAO:getPartnerPriorityDetails:ENTER");

		PartnerVO oPartnerVO=null;
		List<PartnerVO> alPatnerInfo=new ArrayList<PartnerVO>();
		Connection con=null;		
		String sDBData = null;
		CallableStatement cstmt=null;
		int iOwnerId;
		int iPriority;
		HashMap<Integer,Integer> hmVendorInfo = new HashMap<Integer,Integer>();
		HashMap<Integer,Integer> hmAirlineInfo = new HashMap<Integer,Integer>();
		
		HashMap<String,HashMap> hmPartnerPriorityInfo = new HashMap<String,HashMap>();
		
		String sQry="{call usp_sbh_get_partner_details(?,?,?)}";

		logger.debug("MerchandizeDAO:getPartnerPriorityDetails:SQL QUERY "+sQry);
		logger.debug("MerchandizeDAO:getPartnerPriorityDetails:Search By: "+"ALL");
		logger.debug("MerchandizeDAO:getPartnerPriorityDetails:Search Value: "+"");
		logger.debug("MerchandizeDAO:getPartnerPriorityDetails:Status : "+"ALL");

		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,"ALL");	
			cstmt.setString(2,"");
			cstmt.setString(3,"ALL");
			cstmt.execute();
			rs = cstmt.getResultSet();

			while(rs.next()){
				
				sDBData = rs.getString("owner_type");
				sDBData = sDBData == null?"":sDBData.trim();
				
				if(sDBData.equalsIgnoreCase("VENDOR")){
					hmVendorInfo.put(rs.getInt("owner_id"), rs.getInt("owner_priority"));
				}else if(sDBData.equalsIgnoreCase("AIRLINE")){
					hmAirlineInfo.put(rs.getInt("owner_id"), rs.getInt("owner_priority"));
				}
			}
			
			hmPartnerPriorityInfo.put("VENDOR", hmVendorInfo);
			hmPartnerPriorityInfo.put("AIRLINE", hmAirlineInfo);
			
			logger.info("MerchandizeDAO:getPartnerPriorityDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:getPartnerPriorityDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return hmPartnerPriorityInfo;
	}
	
	public static boolean updatePartnerPriority(PartnerVO p_oPartnerVO,String p_sLoginId) throws Exception {

		logger.info("MerchandizeDAO:updatePartnerPriority:ENTER");
		boolean bStatus = true;
		Connection con=null;		
		CallableStatement cstmt=null;
		int rowCount;
		String sQry="{call usp_sbh_upd_partner_priority(?,?,?,?,?)}";
		logger.debug("MerchandizeDAO:updatePartnerPriority:SQL QUERY:"+sQry);
		logger.debug("MerchandizeDAO:updatePartnerPriority:Owner Type:"+p_oPartnerVO.getOwnerType());	
		logger.debug("MerchandizeDAO:updatePartnerPriority:Owner Id:"+p_oPartnerVO.getOwnerId());
		logger.debug("MerchandizeDAO:updatePartnerPriority:Priority :"+p_oPartnerVO.getOwnerPriority());
		logger.debug("MerchandizeDAO:updatePartnerPriority:Login Id:"+p_sLoginId);
		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oPartnerVO.getOwnerType());	
			cstmt.setString(2,p_oPartnerVO.getOwnerId());
			cstmt.setString(3,p_oPartnerVO.getOwnerPriority());
			cstmt.setString(4,p_sLoginId);
			cstmt.registerOutParameter(5, Types.INTEGER);
			cstmt.executeUpdate();
			
			rowCount = cstmt.getInt(5);
			if(rowCount<=0){
				bStatus = false;
			}
			logger.info("MerchandizeDAO:updatePartnerPriority:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("MerchandizeDAO:updatePartnerPriority:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return bStatus;
	}
	
	public static String replaceTagValues(HashMap p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	
	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}



}