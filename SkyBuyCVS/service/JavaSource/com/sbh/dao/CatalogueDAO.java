package com.sbh.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.email.Email;
import com.sbh.forms.ProductDetailsForm;
import com.sbh.forms.SearchCatalogueForm;
import com.sbh.forms.SearchMerchandizeForm;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.LoginVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.VendorVO;

public class CatalogueDAO {

	private static Logger logger = LogManager.getLogger(CatalogueDAO.class);

	public static int addProductDetails(ProductDetailsForm p_oProductDetailsForm,LoginVO p_oLoginVO,String p_sMode) throws Exception{	
		logger.info("CatalogueDAO::addProductDetails:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;
		String sPreAuth = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_product_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::addProductDetails:SQL QUERY "+sQry);

		logger.debug("CatalogueDAO::addProductDetails:User Ref Id: "+p_oLoginVO.getRefId());
		logger.debug("CatalogueDAO::addProductDetails:User Type: "+p_oLoginVO.getUserType());
		logger.debug("CatalogueDAO::addProductDetails:Catg Id: "+p_oProductDetailsForm.getCateId());
		logger.debug("CatalogueDAO::addProductDetails:Product Code: "+p_oProductDetailsForm.getProdCode().trim());
		logger.debug("CatalogueDAO::addProductDetails:Product Title: "+p_oProductDetailsForm.getProdTitle().trim());
		logger.debug("CatalogueDAO::addProductDetails:Brand Name: "+p_oProductDetailsForm.getBrandName());
		logger.debug("CatalogueDAO::addProductDetails:Short Desc: "+p_oProductDetailsForm.getShortDesc().trim());
		logger.debug("CatalogueDAO::addProductDetails:Long Desc: "+p_oProductDetailsForm.getLongDesc().trim());
		logger.debug("CatalogueDAO::addProductDetails:Vend Price: "+p_oProductDetailsForm.getVendPrice().trim());
		logger.debug("CatalogueDAO::addProductDetails:In-Shop Status: "+p_oProductDetailsForm.getInShopStatus());	
		logger.debug("CatalogueDAO::addProductDetails:User Name: "+p_oLoginVO.getUserId().trim());	
		logger.debug("CatalogueDAO::addProductDetails:Comments: "+p_oProductDetailsForm.getSbhComment().trim());
//		logger.debug("CatalogueDAO::addProductDetails:Valid From Date: "+p_oProductDetailsForm.getValidFromDate().trim());
//		logger.debug("CatalogueDAO::addProductDetails:Valid To Date: "+p_oProductDetailsForm.getValidToDate().trim());
		logger.debug("CatalogueDAO::addProductDetails:Instruction: "+p_oProductDetailsForm.getInstructions());
		logger.debug("CatalogueDAO::addProductDetails:Product Id "+p_oProductDetailsForm.getProdId());
		logger.debug("CatalogueDAO::addProductDetails:Color: "+p_oProductDetailsForm.getColor());
		logger.debug("CatalogueDAO::addProductDetails:Size: "+p_oProductDetailsForm.getSize());
		logger.debug("CatalogueDAO::addProductDetails:Pre-Auth: "+sPreAuth);
		logger.debug("CatalogueDAO::addProductDetails:Mode: "+p_sMode);		
		int iProdId = 0;
		try{
			if(p_oProductDetailsForm.getPreAuthCC()!=null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase(""))
				sPreAuth = p_oProductDetailsForm.getPreAuthCC();
			else
				sPreAuth = "N";

			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oLoginVO.getRefId());
			cstmt.setString(3,p_oLoginVO.getUserType());
			cstmt.setString(4,p_oProductDetailsForm.getCateId());
			cstmt.setString(5,p_oProductDetailsForm.getProdCode().trim());
			cstmt.setString(6,p_oProductDetailsForm.getProdTitle().trim());
			cstmt.setString(7,p_oProductDetailsForm.getBrandName());
			cstmt.setString(8,p_oProductDetailsForm.getShortDesc().trim());
			cstmt.setString(9,p_oProductDetailsForm.getLongDesc().trim());
			cstmt.setString(10,p_oProductDetailsForm.getVendPrice().trim());
			cstmt.setString(11,p_oProductDetailsForm.getInShopStatus());	
			cstmt.setString(12,p_oLoginVO.getUserId().trim());	
			cstmt.setString(13,p_oProductDetailsForm.getSbhComment().trim());
//			cstmt.setString(14,p_oProductDetailsForm.getValidFromDate().trim());
//			cstmt.setString(15,p_oProductDetailsForm.getValidToDate().trim());
			cstmt.setString(14,p_oProductDetailsForm.getInstructions());
			cstmt.setString(15,p_oProductDetailsForm.getProdId());
			cstmt.setString(16,p_oProductDetailsForm.getColor());
			cstmt.setString(17,p_oProductDetailsForm.getSize());
			cstmt.setString(18,sPreAuth);
			cstmt.setString(19,p_sMode);

			cstmt.execute();
			iProdId=cstmt.getInt(1);

			logger.info("CatalogueDAO::addProductDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::addProductDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iProdId;
	}

	public static int updateProductImage(ProductDetailsVO p_oProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::updateProductImage:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_product_image(?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::updateProductImage:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateProductImage:Product Id "+p_oProductDetailsVO.getProdId());	
		logger.debug("CatalogueDAO::updateProductImage:Main Image Type: "+p_oProductDetailsVO.getMainImgType());
		logger.debug("CatalogueDAO::updateProductImage:View1 Image Type: "+p_oProductDetailsVO.getView1ImgType());
		logger.debug("CatalogueDAO::updateProductImage:View2 Image Type: "+p_oProductDetailsVO.getView2ImgType());
		logger.debug("CatalogueDAO::updateProductImage:View3 Image Type: "+p_oProductDetailsVO.getView3ImgType());
		logger.debug("CatalogueDAO::updateProductImage:Main Image Cap: "+p_oProductDetailsVO.getMainImgCap());
		logger.debug("CatalogueDAO::updateProductImage:View1 Image Cap: "+p_oProductDetailsVO.getView1ImgCap());
		logger.debug("CatalogueDAO::updateProductImage:View2 Image Cap: "+p_oProductDetailsVO.getView2ImgCap());		
		logger.debug("CatalogueDAO::updateProductImage:View3 Image Cap: "+p_oProductDetailsVO.getView3ImgCap());
		logger.debug("CatalogueDAO::updateProductImage:In-Shop Status: "+p_oProductDetailsVO.getInShopStatus().trim());
		logger.debug("CatalogueDAO::updateProductImage:User Name: "+p_oLoginVO.getUserId().trim());		
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oProductDetailsVO.getProdId().trim());
			cstmt.setString(3,p_oProductDetailsVO.getMainImgType());
			cstmt.setString(4,p_oProductDetailsVO.getView1ImgType());
			cstmt.setString(5,p_oProductDetailsVO.getView2ImgType());
			cstmt.setString(6,p_oProductDetailsVO.getView3ImgType());
			cstmt.setString(7,p_oProductDetailsVO.getMainImgCap());
			cstmt.setString(8,p_oProductDetailsVO.getView1ImgCap());
			cstmt.setString(9,p_oProductDetailsVO.getView2ImgCap());		
			cstmt.setString(10,p_oProductDetailsVO.getView3ImgCap());
			cstmt.setString(11,p_oProductDetailsVO.getInShopStatus().trim());
			cstmt.setString(12,p_oLoginVO.getUserId().trim());



			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("CatalogueDAO::updateProductImage:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateProductImage:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}	
	public static void updateProductStatus(String p_sProdId,String p_sCretaeId) throws Exception{	
		logger.info("CatalogueDAO::updateProductStatus:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_product_status(?,?)}";

		logger.debug("CatalogueDAO::updateProductStatus:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateProductStatus:Product Id "+p_sProdId);	
		logger.debug("CatalogueDAO::updateProductStatus:Create Id "+p_sCretaeId);	
//		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);		
			cstmt.setString(1,p_sProdId.trim());
			cstmt.setString(2,p_sCretaeId.trim());

			cstmt.executeUpdate();


			logger.info("CatalogueDAO::updateProductStatus:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateProductStatus:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}	

	public static List<ProductDetailsVO> getCatalogueDetails(SearchCatalogueForm p_oSearchCatalogueForm,String p_sOwnerId,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getCatalogueDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_catalogue_details(?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::getCatalogueDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getCatalogueDetails:Category: "+p_oSearchCatalogueForm.getCategory());
		logger.debug("CatalogueDAO::getCatalogueDetails:Upload From Date: "+p_oSearchCatalogueForm.getUploadFromDate());	
		logger.debug("CatalogueDAO::getCatalogueDetails:Upload To Date: "+p_oSearchCatalogueForm.getUploadToDate());		
		logger.debug("CatalogueDAO::getCatalogueDetails:Product Status: "+p_oSearchCatalogueForm.getProdStatus());			
		logger.debug("CatalogueDAO::getCatalogueDetails:Sbh Product Status: "+p_oSearchCatalogueForm.getSbhProdStatus());			
		logger.debug("CatalogueDAO::getCatalogueDetails:Owner Type "+p_sOwnerType);
		logger.debug("CatalogueDAO::getCatalogueDetails:Owner Id "+p_sOwnerId);
		logger.debug("CatalogueDAO::getCatalogueDetails:Search By: "+p_oSearchCatalogueForm.getSearchBy());
		logger.debug("CatalogueDAO::getCatalogueDetails:Search Value: "+p_oSearchCatalogueForm.getSearchValue());	

		ResultSet rs=null;
		List<ProductDetailsVO> alCatalogueInfo=new ArrayList<ProductDetailsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchCatalogueForm.getCategory());
			cstmt.setString(2,p_oSearchCatalogueForm.getUploadFromDate());	
			cstmt.setString(3,p_oSearchCatalogueForm.getUploadToDate());		
			cstmt.setString(4,p_oSearchCatalogueForm.getProdStatus());			
			cstmt.setString(5,p_oSearchCatalogueForm.getSbhProdStatus());			
			cstmt.setString(6,p_sOwnerType);
			cstmt.setString(7,p_sOwnerId);
			cstmt.setString(8,p_oSearchCatalogueForm.getSearchBy());
			cstmt.setString(9,p_oSearchCatalogueForm.getSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				/*oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	*/

				String sShortDesc = rs.getString("short_desc");
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDesc = sShortDesc.replaceAll(" ","&nbsp;");
				oProductDetailsVO.setShortDesc(sShortDesc);	

				String sLongDesc = rs.getString("long_desc");
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll(" ","&nbsp;");	
				oProductDetailsVO.setLongDesc(sLongDesc);	

				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Yes");
				}else if(rs.getString("in_shop_status").equalsIgnoreCase("I")){
					oProductDetailsVO.setInShopStatus("No");
				}
				if(rs.getString("sbh_prod_status").equalsIgnoreCase("N")){
					oProductDetailsVO.setSbhProdStatus("Pending");
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setSbhProdStatus("Accepted");
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("R")){
					oProductDetailsVO.setSbhProdStatus("Rejected");
				}
				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));


				alCatalogueInfo.add(oProductDetailsVO);

			}
			logger.info("CatalogueDAO::getCatalogueDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getCatalogueDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alCatalogueInfo;
	}
	public static ProductDetailsVO getProductDetails(String p_sProdId,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getProductDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sDBData = null;
		String sQry="{call usp_sbh_get_product_by_id(?,?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.debug("CatalogueDAO::getProductDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getProductDetails:Product Id "+p_sProdId);	
		logger.debug("CatalogueDAO::getProductDetails:Owner Type "+p_sOwnerType);	


		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdId);	
			cstmt.setString(2,p_sOwnerType);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setSeqId(rs.getString("seq_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));

				sDBData = rs.getString("prod_title");
				sDBData = sDBData == null?"":sDBData.trim();
				oProductDetailsVO.setProdTitle(sDBData);	

				sDBData = rs.getString("brand_name");
				sDBData = sDBData == null?"":sDBData.trim();
				oProductDetailsVO.setBrandName(sDBData);	

				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));

				String sShortDesc = rs.getString("short_desc");			
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();			
				sShortDesc = sShortDesc.replaceAll("<br/>","\n");	
				sShortDesc = sShortDesc.replaceAll("<br>","\n");	
				//sShortDesc = sShortDesc.replaceAll(" ","\s");	
				oProductDetailsVO.setShortDesc(sShortDesc);	



				String sLongDesc = rs.getString("long_desc");
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll("<br/>","\n");	
				sLongDesc = sLongDesc.replaceAll("<br>","\n");	
				//sLongDesc = sLongDesc.replaceAll("<br/>","\n");	
				oProductDetailsVO.setLongDesc(sLongDesc);	


				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));	
				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));	
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));	
				oProductDetailsVO.setMainImgCap(rs.getString("main_img_caption"));	
				oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));	
				oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));	
				oProductDetailsVO.setView1ImgCap(rs.getString("view1_img_caption"));	
				oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));	
				oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));	
				oProductDetailsVO.setView2ImgCap(rs.getString("view2_img_caption"));	
				oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));	
				oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));
				oProductDetailsVO.setView3ImgCap(rs.getString("view3_img_caption"));	
				oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
				oProductDetailsVO.setSbhProdStatus(rs.getString("sbh_prod_status"));
				oProductDetailsVO.setCategoryDate(sdfOutput.format(rs.getDate("sbh_create_dt")));	
				oProductDetailsVO.setVendorEmail(rs.getString("email"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));		
				oProductDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));	
				String sCcomments = rs.getString("sbh_comments");
				sCcomments = sCcomments==null?"":sCcomments.trim();
				oProductDetailsVO.setSbhComment(sCcomments);	

				String sSecondaryEmail = rs.getString("secondary_email");
				sSecondaryEmail = sSecondaryEmail == null?"":sSecondaryEmail.trim();
				oProductDetailsVO.setSecondaryEmailId(sSecondaryEmail);

				/*String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
				oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sNoimagePath=System.getProperty("noImagePath").trim();
				sNoimagePath=sNoimagePath==null?"":sNoimagePath.trim();
				oProductDetailsVO.setNoImgPath(sNoimagePath);

				String sPoPct = rs.getString("po_pct");
				sPoPct = sPoPct==null?"":sPoPct.trim();				
				oProductDetailsVO.setPoPct(sPoPct);

				String sPreAuthCC = rs.getString("preauth_cc");
				sPreAuthCC = sPreAuthCC==null?"":sPreAuthCC.trim();				
				oProductDetailsVO.setPreAuthCC(sPreAuthCC);

				String sColor = rs.getString("color");
				sColor = sColor==null?"":sColor.trim();				
				oProductDetailsVO.setColor(sColor);

				String sSize = rs.getString("size");
				sSize = sSize==null?"":sSize.trim();				
				oProductDetailsVO.setSize(sSize);

				String sSwfPath = rs.getString("swf_path");
				sSwfPath = sSwfPath==null?"":sSwfPath.trim();				
				oProductDetailsVO.setSwfPath(sSwfPath);

				String sSwfType = rs.getString("swf_type");
				sSwfType = sSwfType==null?"":sSwfType.trim();				
				oProductDetailsVO.setSwfType(sSwfType);


				String sIsAdvt = rs.getString("is_advt");
				sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
				oProductDetailsVO.setIsAdvt(sIsAdvt);

				String sSbhProdStatus = rs.getString("sbh_prod_status");
				sSbhProdStatus = sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				
				String sSpecialProdSwfType = rs.getString("prod_swf_type");
				sSpecialProdSwfType = sSpecialProdSwfType==null?"":sSpecialProdSwfType.trim();				
				oProductDetailsVO.setSpecialProdType(sSpecialProdSwfType);

				String sSpecialProdSwfPath = rs.getString("prod_swf_path");
				sSpecialProdSwfPath = sSpecialProdSwfPath==null?"":sSpecialProdSwfPath.trim();				
				oProductDetailsVO.setSpecialProductPath(sSpecialProdSwfPath);

				String sIsSplProd = rs.getString("is_special_prod");
				sIsSplProd = sIsSplProd==null?"":sIsSplProd.trim();				
				oProductDetailsVO.setIsSpecialProd(sIsSplProd);
				
				String sReturnPolicy = rs.getString("return_policy");
				sReturnPolicy = sReturnPolicy==null?"":sReturnPolicy.trim();				
				oProductDetailsVO.setReturnPolicy(sReturnPolicy);

			}
			logger.info("CatalogueDAO::getProductDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getProductDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oProductDetailsVO;
	}
	public static int updateProductDetails(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO p_oProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::updateProductDetails:ENTER");		
//		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_product_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::updateProductDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateProductDetails:Product Id: "+p_oProductDetailsForm.getProdId());			
		logger.debug("CatalogueDAO::updateProductDetails:Product Title: "+p_oProductDetailsForm.getProdTitle().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Brand Name: "+p_oProductDetailsForm.getBrandName().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Short Desc: "+p_oProductDetailsForm.getShortDesc().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Long Desc: "+p_oProductDetailsForm.getLongDesc().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Category Id: "+p_oProductDetailsForm.getCateId().trim());
		logger.debug("CatalogueDAO::updateProductDetails:InShop Status: "+p_oProductDetailsForm.getInShopStatus().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Vendor Price: "+p_oProductDetailsForm.getVendPrice().trim());			
		logger.debug("CatalogueDAO::updateProductDetails:Main Image Type: "+p_oProductDetailsVO.getMainImgType().trim());
		logger.debug("CatalogueDAO::updateProductDetails:View1 Image Type: "+p_oProductDetailsVO.getView1ImgType());
		logger.debug("CatalogueDAO::updateProductDetails:View2 Image Type: "+p_oProductDetailsVO.getView2ImgType());
		logger.debug("CatalogueDAO::updateProductDetails:View3 Image Type: "+p_oProductDetailsVO.getView3ImgType());
		logger.debug("CatalogueDAO::updateProductDetails:Main Image Cap: "+p_oProductDetailsVO.getMainImgCap());
		logger.debug("CatalogueDAO::updateProductDetails:View1 Image Cap: "+p_oProductDetailsVO.getView1ImgCap());
		logger.debug("CatalogueDAO::updateProductDetails:View2 Image Cap: "+p_oProductDetailsVO.getView2ImgCap());		
		logger.debug("CatalogueDAO::updateProductDetails:View3 Image Cap: "+p_oProductDetailsVO.getView3ImgCap());
		logger.debug("CatalogueDAO::updateProductDetails:User Id: "+p_oLoginVO.getUserId().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Sbh Price: "+p_oProductDetailsForm.getSbhPrice());
		logger.debug("CatalogueDAO::updateProductDetails:User Type: "+p_oLoginVO.getUserType().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Comments: "+p_oProductDetailsForm.getSbhComment().trim());
//		logger.debug("CatalogueDAO::updateProductDetails:Valid From Date: "+p_oProductDetailsForm.getValidFromDate().trim());
//		logger.debug("CatalogueDAO::updateProductDetails:Valid To Date: "+p_oProductDetailsForm.getValidToDate().trim());
		logger.debug("CatalogueDAO::updateProductDetails:Instructions: "+p_oProductDetailsForm.getInstructions());
		logger.debug("CatalogueDAO::updateProductDetails:Color: "+p_oProductDetailsForm.getColor());
		logger.debug("CatalogueDAO::updateProductDetails:Size: "+p_oProductDetailsForm.getSize());
		logger.debug("CatalogueDAO::updateProductDetails:Product Code: "+p_oProductDetailsForm.getProdCode());

		p_oProductDetailsVO.setMainImgType(p_oProductDetailsVO.getMainImgType()==null?"":p_oProductDetailsVO.getMainImgType().trim());
		String sPreAuth = null;
		int iIsUpdate = 0;
		try{
			if(p_oProductDetailsForm.getPreAuthCC()!=null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase(""))
				sPreAuth = p_oProductDetailsForm.getPreAuthCC();
			else
				sPreAuth = "N";
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oProductDetailsForm.getProdId().trim());
			cstmt.setString(3,p_oProductDetailsForm.getProdTitle().trim());
			cstmt.setString(4,p_oProductDetailsForm.getBrandName().trim());
			cstmt.setString(5,p_oProductDetailsForm.getShortDesc().trim());
			cstmt.setString(6,p_oProductDetailsForm.getLongDesc().trim());
			cstmt.setString(7,p_oProductDetailsForm.getCateId().trim());
			cstmt.setString(8,p_oProductDetailsForm.getInShopStatus().trim());
			cstmt.setString(9,p_oProductDetailsForm.getVendPrice().trim());			
			cstmt.setString(10,p_oProductDetailsVO.getMainImgType().trim());
			cstmt.setString(11,p_oProductDetailsVO.getView1ImgType());
			cstmt.setString(12,p_oProductDetailsVO.getView2ImgType());
			cstmt.setString(13,p_oProductDetailsVO.getView3ImgType());
			cstmt.setString(14,p_oProductDetailsVO.getMainImgCap());
			cstmt.setString(15,p_oProductDetailsVO.getView1ImgCap());
			cstmt.setString(16,p_oProductDetailsVO.getView2ImgCap());		
			cstmt.setString(17,p_oProductDetailsVO.getView3ImgCap());
			cstmt.setString(18,p_oLoginVO.getUserId().trim());
			cstmt.setString(19,p_oProductDetailsForm.getSbhPrice());
			cstmt.setString(20,p_oLoginVO.getUserType().trim());
			cstmt.setString(21,p_oProductDetailsForm.getSbhComment().trim());
//			cstmt.setString(22,p_oProductDetailsForm.getValidFromDate().trim());
//			cstmt.setString(23,p_oProductDetailsForm.getValidToDate().trim());
			cstmt.setString(22,p_oProductDetailsForm.getInstructions());
			cstmt.setString(23,p_oProductDetailsForm.getColor());
			cstmt.setString(24,p_oProductDetailsForm.getSize());
			cstmt.setString(25,p_oProductDetailsForm.getPoPct());
			cstmt.setString(26,sPreAuth);
			cstmt.setString(27,p_oProductDetailsForm.getProdCode());

			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("CatalogueDAO::updateProductDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateProductDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}

	public static List<VendorVO> getVendor() throws Exception{	
		logger.info("CatalogueDAO::getVendor:ENTER");		
		List<VendorVO> alVendor = new ArrayList<VendorVO>();
		VendorVO oVendorVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_vendor()}";		
		logger.debug("CatalogueDAO::getVendor:SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				oVendorVO = new VendorVO();
				oVendorVO.setVendId(rs.getString("vend_id"));
				oVendorVO.setVendName(rs.getString("vend_name"));

				alVendor.add(oVendorVO);				
			}
			logger.info("CatalogueDAO::getVendor:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getVendor:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alVendor;
	}
	public static ArrayList getCatalogueStatus(String p_sOwnerId ,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getCatalogueStatus:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ArrayList alOrderStatusInfo = new ArrayList();

		String sQry="{call usp_sbh_get_catalogue_status(?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::getCatalogueStatus:SQL QUERY "+sQry);	
		logger.debug("CatalogueDAO::getCatalogueStatus:Owner Id"+p_sOwnerId);
		logger.debug("CatalogueDAO::getCatalogueStatus:Owner Type"+p_sOwnerType);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sOwnerId);
			cstmt.setString(2, p_sOwnerType);
			cstmt.registerOutParameter(3, Types.INTEGER);	
			cstmt.registerOutParameter(4, Types.INTEGER);	
			cstmt.registerOutParameter(5, Types.INTEGER);
			cstmt.registerOutParameter(6, Types.INTEGER);
			cstmt.execute();

			alOrderStatusInfo.add(0,cstmt.getInt(3));
			alOrderStatusInfo.add(1,cstmt.getInt(4));
			alOrderStatusInfo.add(2,cstmt.getInt(5));
			alOrderStatusInfo.add(3,cstmt.getInt(6));


			logger.info("CatalogueDAO::getCatalogueStatus:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getCatalogueStatus:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		return alOrderStatusInfo;
	}

	public static void deleteProduct(String p_sProdId ,String p_sOwnerName) throws Exception{	
		logger.info("CatalogueDAO::deleteProduct:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_delete_product(?,?)}";

		logger.debug("CatalogueDAO::deleteProduct:SQL QUERY "+sQry);	
		logger.debug("CatalogueDAO::deleteProduct:Prod Id :"+p_sProdId);	
		logger.debug("CatalogueDAO::deleteProduct:Owner Name : "+p_sOwnerName);	


		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProdId);
			cstmt.setString(2, p_sOwnerName);
			cstmt.executeUpdate();			

			logger.info("CatalogueDAO::deleteProduct:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::deleteProduct:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}

	}
	public static List<ProductDetailsVO> getCatalogueStatusDetails(String p_sOrderStatus,String p_sOwnerId,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getCatalogueStatusDetails:ENTER");		

		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		List<ProductDetailsVO> alCatalogueInfo=new ArrayList<ProductDetailsVO>();

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_catalogue_status_details(?,?,?)}";
//		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.debug("CatalogueDAO::getCatalogueStatusDetails:SQL QUERY: "+sQry);
		logger.debug("CatalogueDAO::getCatalogueStatusDetails:Order Status: "+p_sOrderStatus);
		logger.debug("CatalogueDAO::getCatalogueStatusDetails:Owner Id: "+p_sOwnerId);	
		logger.debug("CatalogueDAO::getCatalogueStatusDetails:Owner Type: "+p_sOwnerType);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sOrderStatus);	
			cstmt.setString(2,p_sOwnerId);	
			cstmt.setString(3,p_sOwnerType);	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	
				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oProductDetailsVO.setEmailId(rs.getString("email_id"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
					sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
					sImagePath=sViewPath+sImagePath;
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				
				alCatalogueInfo.add(oProductDetailsVO);
			}
			logger.info("CatalogueDAO::getCatalogueStatusDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getCatalogueStatusDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alCatalogueInfo;
	}
	public static boolean sendProdStatusByEmail(ProductDetailsForm oProductDetailsForm,ProductDetailsVO oProductDetailsVO,LoginVO oLoginVO, String p_sLoginId, 
			String p_sReqeustFrom, String p_sCategory) throws Exception{
		logger.info("CatalogueDAO:sendProdStatusByEmail:ENTER");
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null,sEmailFromAddr = null,sEmailTemplateId=null;			
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sSplInsturction="";
		EmailVO oEmailVO =null;
		Map<String,String> p_hmTags =new HashMap<String, String>();
		String sSecondaryEmail = "",sBccAddr="",sCcAddr="",sSkyBuyURL="",sToEmail = "";
//		String sSizeAndColor ="";
		String sEmailSendTo = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ProductDetailsVO oNewProductDetailsVO = null;
			oNewProductDetailsVO = CatalogueDAO.getProductDetails(oProductDetailsVO.getProdId(),oLoginVO.getUserType());

			
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			String sNoImagePath=System.getProperty("noImagePath").trim();
			sNoImagePath = sNoImagePath==null?"":sNoImagePath.trim();	

			if(oProductDetailsForm!=null){
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A")){
					if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
						sEmailTemplateId=oBundle.getString("email.checkout.admin.active.templateId");
					}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						sEmailTemplateId=oBundle.getString("email.item.vendor.active.templateId");
					}else{
						if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
							sEmailTemplateId=oBundle.getString("email.item.airline.active.templateId");
						}else{
							sEmailTemplateId=oBundle.getString("email.package.airline.active.templateId");
						}
					}

				}else if(oProductDetailsVO.getOwnerType()!=null){
					if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
						sEmailTemplateId=oBundle.getString("email.checkout.admin.inactive.templateId");
					}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						sEmailTemplateId=oBundle.getString("email.item.vendor.inactive.templateId");
					}else{
						if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
							sEmailTemplateId=oBundle.getString("email.item.airline.inactive.templateId");
						}else{
							sEmailTemplateId=oBundle.getString("email.package.airline.inactive.templateId");
						}

					}
				}else{
					if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){					
						sEmailTemplateId=oBundle.getString("email.item.vendor.inactive.templateId");
					}else{
						if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
							sEmailTemplateId=oBundle.getString("email.item.airline.inactive.templateId");
						}else{
							sEmailTemplateId=oBundle.getString("email.package.airline.inactive.templateId");
						}

					}
				}

			}

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();			

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);


			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oLoginVO.getUserName()));	
					if(oLoginVO.getEmail()!=null){
						sToEmail = oLoginVO.getEmail();	
					}
				}else{
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oProductDetailsVO.getOwnerName()));	
					sToEmail = oProductDetailsVO.getVendorEmail();


				}

				sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sToEmail.contains(sSecondaryEmail)){
						sToEmail = sToEmail + ","+sSecondaryEmail;
					}
				}
				
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					sEmailSendTo = System.getProperty("ProductStatusChangeByVendorMailTo");
				}else{
					sEmailSendTo = System.getProperty("ProductStatusChangeByAdminMailTo");
				}
				sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
				if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
					sToEmail = sAdminEmailAddr;
				}
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");
					p_hmTags.put("{{ContactName}}",oNewProductDetailsVO.getOwnerContactName());
					sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
					p_hmTags.put("{{ContactName}}",oNewProductDetailsVO.getOwnerContactName());
					sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
				}else if(oLoginVO.getUserType().equalsIgnoreCase("admin")){
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						p_hmTags.put("{{OwnerType}}","Vendor");	
						p_hmTags.put("{{ContactName}}",oNewProductDetailsVO.getOwnerContactName());
						sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
					}else{
						p_hmTags.put("{{OwnerType}}","Air Charter");
						p_hmTags.put("{{ContactName}}",oNewProductDetailsVO.getOwnerContactName());
						sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
					}

				}
				p_hmTags.put("{{URL}}",sSkyBuyURL);

			}
			if(oProductDetailsForm!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsForm.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsForm.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsForm.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",oProductDetailsForm.getShortDesc());
				p_hmTags.put("{{Category}}",p_sCategory);
				p_hmTags.put("{{FullDescription}}",oProductDetailsForm.getLongDesc());
				p_hmTags.put("{{Comments}}",oProductDetailsForm.getSbhComment());
				p_hmTags.put("{{ReturnPolicy}}",oNewProductDetailsVO.getReturnPolicy());
				p_hmTags.put("{{AdminURL}}",System.getProperty("SkybuyhighAdminURL"));
				if("ADMIN".equalsIgnoreCase(oLoginVO.getUserType())) {
					if(oProductDetailsForm.getSbhPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getSbhPrice())));
					}
				}else {
					if(oProductDetailsForm.getVendPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getVendPrice())));
					}
				}
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");

				if((oProductDetailsVO.getOwnerType()!=null && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor"))){
					String sSize = oProductDetailsForm.getSize();
					String sColor = oProductDetailsForm.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}else if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					String sSize = oProductDetailsForm.getSize();
					String sColor = oProductDetailsForm.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	

				if(oProductDetailsVO.getOwnerType()!=null && oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsForm.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsForm.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();*/
					String sInstrustion = oProductDetailsForm.getInstructions();
					sInstrustion = sInstrustion == null?"":sInstrustion.trim();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/
				}else if(oLoginVO.getUserType()!=null && oLoginVO.getUserType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsForm.getValidFromDate();
					String sValidTo = oProductDetailsForm.getValidToDate();*/
					String sInstrustion = oProductDetailsForm.getInstructions();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/
				}
			}
			if(oProductDetailsVO!=null){			
				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				else
					p_hmTags.put("{{ProdImagePath}}",sNoImagePath);
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());
			}

			if(oEmailVO!=null){
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();			

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToEmail));
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			System.out.print("EmailContent "+sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");

			/*if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				oEmailLogVo.setOwnerType(oLoginVO.getUserType());
				oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));
			}else{
				oEmailLogVo.setOwnerType(oProductDetailsVO.getOwnerType());		
				oEmailLogVo.setOwnerId(Long.parseLong(oProductDetailsVO.getOwnerId()));
			}*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(oLoginVO.getEmail());
			oEmailLogVo.setEmailCcAddr("");

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");



			logger.info("CatalogueDAO::sendProdStatusByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:sendProdStatusByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}

	public static boolean sendEditProdInfoByEmail(ProductDetailsForm oProductDetailsForm,ProductDetailsVO oProductDetailsVO,ProductDetailsVO oOldProductDetailsVO,LoginVO oLoginVO, String p_sLoginId, String p_sReqeustFrom,
			String p_sCategory) throws Exception{
		logger.info("CatalogueDAO:sendEditProdInfoByEmail:ENTER");
		/*String sEmailContetnHeader=null,sEmailContentDate=null,sEmailContetntOrderDeatilsHeader=null;
		String sEmailContentDateName =null,sEmailContetntProdDeatilsHeader = null,sEmailContetntProdDeatilsBody =null;
		String sEmailContentProdInfo=null,sEmailContentProdInfoFooter=null,sEmailContentFooter=null;*/
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null,sEmailContetntProdDeatilsInfo="";
//		String sOrderDetails="",sEmailContetnStyle=null ,sInShopStatus = null ,sOldInShopStatus =null;
		String sSplInsturction ="";
		String sSkyBuyLogoPath = null,sEmailCaption =null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sEmailTemplateId =null,sEmailFromAddr=null;
		String sBccAddr="",sCcAddr="",sSecondaryEmail = "",sToEmail = "";
		boolean bEditProduct = false;
		Map<String, String> p_hmTags =new HashMap<String, String>();	
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		String sEmailSendTo = null;
		String sAdminURL = null;
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
				sEmailTemplateId=oBundle.getString("email.editcheckout.admin.templateId");
			}else if(oLoginVO.getUserType().equalsIgnoreCase("VENDOR")){
				sEmailTemplateId=oBundle.getString("email.edititem.vendor.templateId");					
			}else if(oLoginVO.getUserType().equalsIgnoreCase("AIRLINE")){
				if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
					sEmailTemplateId=oBundle.getString("email.edititem.airline.templateId");
				}else{
					sEmailTemplateId=oBundle.getString("email.editpackage.airline.templateId");
				}
			}else if(oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
					sEmailTemplateId=oBundle.getString("email.admin.edititem.vendor.templateId");
				}else{
					if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
						sEmailTemplateId=oBundle.getString("email.admin.edititem.airline.templateId");
					}else{
						sEmailTemplateId=oBundle.getString("email.admin.editpackage.airline.templateId");
					}
				}
			}			
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			String sNoImagePath=System.getProperty("noImagePath").trim();
			sNoImagePath = sNoImagePath==null?"":sNoImagePath.trim();

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sAdminURL = System.getProperty("SkybuyhighAdminURL");
			sAdminURL=sAdminURL==null?"":sAdminURL.trim();
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			p_hmTags.put("{{Heading}}",sEmailCaption);
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{AdminURL}}",sAdminURL);


			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",oLoginVO.getUserName());	
					if(oLoginVO.getEmail()!=null){
						sToEmail = oLoginVO.getEmail();	
					}
				}else{
					p_hmTags.put("{{ContactName}}",Utils.toTitleCase((oProductDetailsVO.getOwnerContactName())));	
					p_hmTags.put("{{Name}}",oProductDetailsVO.getOwnerName());	
					sToEmail = oProductDetailsVO.getVendorEmail();	


				}

				sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sToEmail.contains(sSecondaryEmail)){
						sToEmail = sToEmail + ","+sSecondaryEmail;
					}
				}
				
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					sEmailSendTo = System.getProperty("EditProductByVendorMailTo");
				}else{
					sEmailSendTo = System.getProperty("EditProductByAdminMailTo");
				}
				sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
				if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
					sToEmail = sAdminEmailAddr;
				}
				
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");				
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
				}else if(oLoginVO.getUserType().equalsIgnoreCase("admin")){
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						p_hmTags.put("{{OwnerType}}","Vendor");	
					}else{
						p_hmTags.put("{{OwnerType}}","Air Charter");	
					}
				}

			}				

			if(oProductDetailsForm!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsForm.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsForm.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsForm.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",oProductDetailsForm.getShortDesc());
				p_hmTags.put("{{FullDescription}}",oProductDetailsForm.getLongDesc());
				p_hmTags.put("{{ReturnPolicy}}",oProductDetailsVO.getReturnPolicy());
				p_hmTags.put("{{Category}}",p_sCategory);
				p_hmTags.put("{{Comments}}",oProductDetailsForm.getSbhComment());
				
				if("ADMIN".equalsIgnoreCase(oLoginVO.getUserType())) {
					if(oProductDetailsForm.getSbhPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getSbhPrice())));
					}
				}else {
					if(oProductDetailsForm.getVendPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getVendPrice())));
					}
				}
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");

				if("vendor".equalsIgnoreCase(oProductDetailsVO.getOwnerType())){
					String sSize = oProductDetailsForm.getSize();
					String sColor = oProductDetailsForm.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	


				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsForm.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsForm.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();*/
					String sInstrustion = oProductDetailsForm.getInstructions();
					sInstrustion = sInstrustion == null?"":sInstrustion.trim();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/
				}

			}
			if(oProductDetailsVO!=null){	

				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				else
					p_hmTags.put("{{ProdImagePath}}",sNoImagePath);

				//p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());
			}	

			if(oEmailVO!=null){
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();

			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			if( oProductDetailsForm.getProdTitle()!=null && !oProductDetailsForm.getProdTitle().equals(oOldProductDetailsVO.getProdTitle()) ){				
				bEditProduct =true;	
				sEmailContetntProdDeatilsInfo = "<b>Item Name</b>";				
			}

			if( oProductDetailsForm.getInShopStatus()!=null && !oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){						
				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0)
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Item Status</b>";
				else
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Item Status</b>";
			}

			if(oProductDetailsForm.getProdCode()!=null && !oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())  ){						
				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0)
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Item Code</b>";
				else
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Item Code</b>";
			}

			if(("VENDOR".equalsIgnoreCase(oProductDetailsVO.getOwnerType())) || ("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt()) && "AIRLINE".equalsIgnoreCase(oProductDetailsVO.getOwnerType()))){
				if("N".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd()) && (oLoginVO.getUserType().equalsIgnoreCase("VENDOR") || oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))){
					if(!oProductDetailsForm.getCateId().equals(oOldProductDetailsVO.getCateId())){				
						bEditProduct =true;	
						if(sEmailContetntProdDeatilsInfo.trim().length()>0)
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Category</b>";
						else
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Category</b>";

					}	
				}

				if(oProductDetailsForm.getBrandName()!=null && !oProductDetailsForm.getBrandName().equals(oOldProductDetailsVO.getBrandName()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){				
					bEditProduct =true;	
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Brand Name</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Brand Name</b>";

				}			

				String sPreAuthCC = null;
				if(oProductDetailsForm.getPreAuthCC()!= null && !oProductDetailsForm.getPreAuthCC().equalsIgnoreCase("")){
					sPreAuthCC = oProductDetailsForm.getPreAuthCC();
				}else{
					sPreAuthCC = "N";
					oProductDetailsForm.setPreAuthCC("N");
				}
				if(!sPreAuthCC.equals(oOldProductDetailsVO.getPreAuthCC())){	
					bEditProduct =true;
					if(sEmailContetntProdDeatilsInfo.trim().length()>0){
						sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Pre-Authorize Credit Card</b>";
					}else{				
						sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Pre-Authorize Credit Card</b>";
					}
				}
				if(oProductDetailsForm.getSize()!=null){
					if(!oProductDetailsForm.getSize().equals(oOldProductDetailsVO.getSize()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){	
						bEditProduct =true;
						if(sEmailContetntProdDeatilsInfo.trim().length()>0){
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Size</b>";
						}else{				
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Size</b>";
						}
					}
				}
				if(oProductDetailsForm.getColor()!=null){
					if(!oProductDetailsForm.getColor().equals(oOldProductDetailsVO.getColor()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){	
						bEditProduct =true;
						if(sEmailContetntProdDeatilsInfo.trim().length()>0){
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Color</b>";
						}else{				
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Color</b>";
						}
					}
				}

				/*if(!oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){				
				bEditProduct =true;

				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+",<b>Retail Price</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Retail Price</b>";
				}else{				
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+",<b>Price</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Price</b>";
				}
			}*/
				if(oProductDetailsForm.getSbhPrice()!=null && oOldProductDetailsVO.getSbhPrice()!=null){
					if(!oProductDetailsForm.getSbhPrice().equals(oOldProductDetailsVO.getSbhPrice())){				
						if(sEmailContetntProdDeatilsInfo.trim().length()>0)
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>SkyBuy<sup>High</sup> Price</b>";
						else
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>SkyBuy<sup>High</sup> Price</b>";
						bEditProduct =true;
					}
				}
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					if(oProductDetailsForm.getVendPrice()!=null && oOldProductDetailsVO.getVendPrice()!=null){
						if(!oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){				
							if(sEmailContetntProdDeatilsInfo.trim().length()>0)
								sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>SkyBuy<sup>High</sup> Price</b>";
							else
								sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>SkyBuy<sup>High</sup> Price</b>";
							bEditProduct =true;
						}
					}
				}

				String sShortDesc =oOldProductDetailsVO.getShortDesc();
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDesc = sShortDesc.replaceAll("\n","<br>");
				oOldProductDetailsVO.setShortDesc(sShortDesc);


				String ssShortDesc =oProductDetailsForm.getShortDesc();
				ssShortDesc = ssShortDesc ==null?"":ssShortDesc.trim();
				ssShortDesc = ssShortDesc.replaceAll("&nbsp;"," ");
				oProductDetailsForm.setShortDesc(ssShortDesc);

				String sLongDesc = oOldProductDetailsVO.getLongDesc();
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll("\n","<br>");
				oOldProductDetailsVO.setLongDesc(sLongDesc);


				String ssLongDesc =oProductDetailsForm.getLongDesc();
				ssLongDesc = ssLongDesc ==null?"":ssLongDesc.trim();
				ssLongDesc = ssLongDesc.replaceAll("&nbsp;"," ");
				oProductDetailsForm.setLongDesc(ssLongDesc);

				if(!oProductDetailsForm.getShortDesc().equals(oOldProductDetailsVO.getShortDesc())){				
					// System.out.println("new value:"+oProductDetailsForm.getShortDesc());
					// System.out.println("old value:"+oOldProductDetailsVO.getShortDesc());
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Title</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Title</b>";
					bEditProduct =true;
				}

				if(!oProductDetailsForm.getLongDesc().equals(oOldProductDetailsVO.getLongDesc())){				
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Full Description</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Full Description</b>";
					bEditProduct =true;
				}
			}

			if(oProductDetailsForm.getAdvtMainImgPath()!=null && !oProductDetailsForm.getAdvtMainImgPath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Image</b>";
				}				
			}

			if(oProductDetailsForm.getAdvertisementPath()!=null && !oProductDetailsForm.getAdvertisementPath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Advertisement</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Advertisement</b>";
				}				
			}

			if(oProductDetailsForm.getUploadMainImagePath()!=null && !oProductDetailsForm.getUploadMainImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Main Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Main Image</b>";
				}				
			}


			if(oProductDetailsForm.getUploadView1ImagePath()!=null && !oProductDetailsForm.getUploadView1ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View1 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View1 Image</b>";
				}
			}
			if(oProductDetailsForm.getUploadView2ImagePath()!=null && !oProductDetailsForm.getUploadView2ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View2 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View2 Image</b>";
				}
			}
			if(oProductDetailsForm.getUploadView3ImagePath()!=null && !oProductDetailsForm.getUploadView3ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View3 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View3 Image</b>";
				}
			}

			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd()) && oProductDetailsForm.getSpecialProdPath()!=null && !oProductDetailsForm.getSpecialProdPath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Checkout Product</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Checkout Product</b>";
				}				
			}

			System.out.print("EditProdDeatil_EmailContent"+sEmailContetntProdDeatilsInfo);


			if(sEmailContetntProdDeatilsInfo.trim().length()>0){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){	
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"  have been modified ";
				}else{
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"  have been modified ";
				}
			}

			p_hmTags.put("{{ModifiedFields}}",sEmailContetntProdDeatilsInfo);

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			if(bEditProduct){
				EmailLogVO oEmailLogVo=new EmailLogVO();
				Email oEmail=new Email();
				oEmailLogVo.setEmailFrom(sEmailFromAddr);
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){					
					oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
					if(sCcAddr!=null && sCcAddr.trim().length()>0){
						sCcAddr =sToEmail+","+sCcAddr;						
					}else{
						sCcAddr =sToEmail;
					}

					oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
					oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));					
				}else{
					oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToEmail));
					if(sCcAddr!=null && sCcAddr.trim().length()>0){
						sCcAddr =sAdminEmailAddr+","+sCcAddr;
					}else{
						sCcAddr =sAdminEmailAddr;
					}
					oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
					oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));			
				}
				oEmailLogVo.setEmailSubject(sEmailSubject);
				oEmailLogVo.setEmailText(sEmailContent);


				boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
				if(bEmailSent)				
					oEmailLogVo.setEmailStatus("Y");
				else
					oEmailLogVo.setEmailStatus("N");


				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){					
					oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
					oEmailLogVo.setEmailCcAddr(oLoginVO.getEmail());
					/*oEmailLogVo.setOwnerType(oLoginVO.getUserType());
					oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));*/
				}else{
					oEmailLogVo.setEmailToAddr(oLoginVO.getEmail());
					oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);
					/*oEmailLogVo.setOwnerType(oProductDetailsVO.getOwnerType());
					oEmailLogVo.setOwnerId(Long.parseLong(oProductDetailsVO.getOwnerId()));*/

				}
				oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
				/** Insert Email Log Details **/
				EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			}
			System.out.print("EmailContent "+sEmailContent);

			logger.info("CatalogueDAO::sendEditProdInfoByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:sendEditProdInfoByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}

	/*public static boolean sendEditProdInfoByEmail(ProductDetailsForm oProductDetailsForm,ProductDetailsVO oProductDetailsVO,ProductDetailsVO oOldProductDetailsVO,LoginVO oLoginVO, String p_sShortDesc) throws Exception{
		logger.info("CatalogueDAO:sendEditProdInfoByEmail:ENTER");
		String sEmailContetnHeader=null,sEmailContentDate=null,sEmailContetntOrderDeatilsHeader=null;
		String sEmailContentDateName =null,sEmailContetntProdDeatilsHeader = null,sEmailContetntProdDeatilsBody =null;
		String sEmailContentProdInfo=null,sEmailContentProdInfoFooter=null,sEmailContentFooter=null;
		String sEmailContent="",sOutput=null,sEmailSubject=null,sAdminEmailAddr=null,sEmailContetntProdDeatilsInfo="";
		String sOrderDetails="",sEmailContetnStyle=null ,sInShopStatus = null ,sOldInShopStatus =null,sSplInsturction ="";
		String sSkyBuyLogoPath = null,sEmailCaption =null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sEmailTemplateId =null,sEmailFromAddr=null;
		String sBccAddr="",sCcAddr="",sSizeAndColor ="";
		boolean bEditProduct = false;
		HashMap p_hmTags =new HashMap();	
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);

		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
				sEmailTemplateId=oBundle.getString("email.editcheckout.admin.templateId");
			}else if(oLoginVO.getUserType().equalsIgnoreCase("VENDOR")){
				sEmailTemplateId=oBundle.getString("email.edititem.vendor.templateId");					
			}else if(oLoginVO.getUserType().equalsIgnoreCase("AIRLINE")){
				if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
					sEmailTemplateId=oBundle.getString("email.edititem.airline.templateId");
				}else{
					sEmailTemplateId=oBundle.getString("email.editpackage.airline.templateId");
				}
			}else if(oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
					sEmailTemplateId=oBundle.getString("email.admin.edititem.vendor.templateId");
				}else{
					if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
						sEmailTemplateId=oBundle.getString("email.admin.edititem.airline.templateId");
					}else{
						sEmailTemplateId=oBundle.getString("email.admin.editpackage.airline.templateId");
					}

				}

			}				
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			String sNoImagePath=System.getProperty("noImagePath").trim();
			sNoImagePath = sNoImagePath==null?"":sNoImagePath.trim();

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminEmailAddr= p_sAdminEmailAddress;
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			p_hmTags.put("{{Heading}}",sEmailCaption);
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);


			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",oLoginVO.getUserName());	
					if(oLoginVO.getEmail()!=null){
						oLoginVO.setEmail(oLoginVO.getEmail());	
					}
				}else{
					p_hmTags.put("{{ContactName}}",Utils.toTitleCase((oProductDetailsVO.getOwnerContactName())));	
					p_hmTags.put("{{Name}}",oProductDetailsVO.getOwnerName());	
					oLoginVO.setEmail(oProductDetailsVO.getVendorEmail());	
				}
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");				
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
				}else if(oLoginVO.getUserType().equalsIgnoreCase("admin")){
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						p_hmTags.put("{{OwnerType}}","Vendor");	
					}else{
						p_hmTags.put("{{OwnerType}}","Air Charter");	
					}
				}

			}				

			if(oProductDetailsForm!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsForm.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsForm.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsForm.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",p_sShortDesc);
				p_hmTags.put("{{Comments}}",oProductDetailsForm.getSbhComment());
				if(oProductDetailsForm.getVendPrice()!=null){
					p_hmTags.put("{{VendorPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getVendPrice())));
				}
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");

				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
					String sSize = oProductDetailsVO.getSize();
					String sColor = oProductDetailsVO.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	


				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
					String sValidFrom = oProductDetailsForm.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsForm.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();
					String sInstrustion = oProductDetailsForm.getInstructions();
					sInstrustion = sInstrustion == null?"":sInstrustion.trim();
					if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;

					if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);
				}

			}
			if(oProductDetailsVO!=null){	

				if(oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0)
					p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				else
					p_hmTags.put("{{ProdImagePath}}",sNoImagePath);

				//p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());
			}	

			if(oEmailVO!=null){
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();

			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			if(!oProductDetailsForm.getProdTitle().equals(oOldProductDetailsVO.getProdTitle())){				
				bEditProduct =true;	
				sEmailContetntProdDeatilsInfo = "<b>Item Name</b>";				
			}

			if(!oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){						
				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0)
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Item Status</b>";
				else
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Item Status</b>";
			}

			if(!oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())){						
				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0)
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Item Code</b>";
				else
					sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Item Code</b>";
			}

			if(("VENDOR".equalsIgnoreCase(oProductDetailsVO.getOwnerType())) || ("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt()) && "AIRLINE".equalsIgnoreCase(oProductDetailsVO.getOwnerType()))){
				if(oLoginVO.getUserType().equalsIgnoreCase("VENDOR") || oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					if(!oProductDetailsForm.getCateId().equals(oOldProductDetailsVO.getCateId())){				
						bEditProduct =true;	
						if(sEmailContetntProdDeatilsInfo.trim().length()>0)
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Category</b>";
						else
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Category</b>";

					}	
				}

				if(!oProductDetailsForm.getBrandName().equals(oOldProductDetailsVO.getBrandName()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){				
					bEditProduct =true;	
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Brand Name</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Brand Name</b>";

				}			

				String sPreAuthCC = null;
				if(oProductDetailsForm.getPreAuthCC()!= null && !oProductDetailsForm.getPreAuthCC().equalsIgnoreCase("")){
					sPreAuthCC = oProductDetailsForm.getPreAuthCC();
				}else{
					sPreAuthCC = "N";
					oProductDetailsForm.setPreAuthCC("N");
				}
				if(!sPreAuthCC.equals(oOldProductDetailsVO.getPreAuthCC())){	
					bEditProduct =true;
					if(sEmailContetntProdDeatilsInfo.trim().length()>0){
						sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Pre-Authorize Credit Card</b>";
					}else{				
						sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Pre-Authorize Credit Card</b>";
					}
				}
				if(oProductDetailsForm.getSize()!=null){
					if(!oProductDetailsForm.getSize().equals(oOldProductDetailsVO.getSize()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){	
						bEditProduct =true;
						if(sEmailContetntProdDeatilsInfo.trim().length()>0){
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Size</b>";
						}else{				
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Size</b>";
						}
					}
				}
				if(oProductDetailsForm.getColor()!=null){
					if(!oProductDetailsForm.getColor().equals(oOldProductDetailsVO.getColor()) && oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){	
						bEditProduct =true;
						if(sEmailContetntProdDeatilsInfo.trim().length()>0){
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Color</b>";
						}else{				
							sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Color</b>";
						}
					}
				}

				/*if(!oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){				
				bEditProduct =true;

				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+",<b>Retail Price</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Retail Price</b>";
				}else{				
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+",<b>Price</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Price</b>";
				}
			}*/
	/*	if(oProductDetailsForm.getSbhPrice()!=null && oOldProductDetailsVO.getSbhPrice()!=null){
					if(!oProductDetailsForm.getSbhPrice().equals(oOldProductDetailsVO.getSbhPrice())){				
						if(sEmailContetntProdDeatilsInfo.trim().length()>0)
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>SkyBuy<sup>High</sup> Price</b>";
						else
							sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>SkyBuy<sup>High</sup> Price</b>";
						bEditProduct =true;
					}
				}
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					if(oProductDetailsForm.getVendPrice()!=null && oOldProductDetailsVO.getVendPrice()!=null){
						if(!oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){				
							if(sEmailContetntProdDeatilsInfo.trim().length()>0)
								sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>SkyBuy<sup>High</sup> Price</b>";
							else
								sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>SkyBuy<sup>High</sup> Price</b>";
							bEditProduct =true;
						}
					}
				}

				String sShortDesc =oOldProductDetailsVO.getShortDesc();
				//sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDesc = sShortDesc.replaceAll("\n","<br>");
				oOldProductDetailsVO.setShortDesc(sShortDesc);


				String ssShortDesc =oProductDetailsForm.getShortDesc();
				ssShortDesc = ssShortDesc ==null?"":ssShortDesc.trim();
				ssShortDesc = ssShortDesc.replaceAll("&nbsp;"," ");
				oProductDetailsForm.setShortDesc(ssShortDesc);

				String sLongDesc = oOldProductDetailsVO.getLongDesc();
				//sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll("\n","<br>");
				oOldProductDetailsVO.setLongDesc(sLongDesc);


				String ssLongDesc =oProductDetailsForm.getLongDesc();
				ssLongDesc = ssLongDesc ==null?"":ssLongDesc.trim();
				ssLongDesc = ssLongDesc.replaceAll("&nbsp;"," ");
				oProductDetailsForm.setLongDesc(ssLongDesc);

				if(!oProductDetailsForm.getShortDesc().equals(oOldProductDetailsVO.getShortDesc())){				
					// System.out.println("new value:"+oProductDetailsForm.getShortDesc());
					// System.out.println("old value:"+oOldProductDetailsVO.getShortDesc());
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Title</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Title</b>";
					bEditProduct =true;
				}

				if(!oProductDetailsForm.getLongDesc().equals(oOldProductDetailsVO.getLongDesc())){				
					if(sEmailContetntProdDeatilsInfo.trim().length()>0)
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+", <b>Full Description</b>";
					else
						sEmailContetntProdDeatilsInfo = sEmailContetntProdDeatilsInfo+"<b>Full Description</b>";
					bEditProduct =true;
				}
			}

			if(oProductDetailsForm.getAdvtMainImgPath()!=null && !oProductDetailsForm.getAdvtMainImgPath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Image</b>";
				}				
			}

			if(oProductDetailsForm.getAdvertisementPath()!=null && !oProductDetailsForm.getAdvertisementPath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Advertisement</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Advertisement</b>";
				}				
			}

			if(oProductDetailsForm.getUploadMainImagePath()!=null && !oProductDetailsForm.getUploadMainImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>Main Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>Main Image</b>";
				}				
			}
			if(oProductDetailsForm.getUploadView1ImagePath()!=null && !oProductDetailsForm.getUploadView1ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View1 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View1 Image</b>";
				}
			}
			if(oProductDetailsForm.getUploadView2ImagePath()!=null && !oProductDetailsForm.getUploadView2ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View2 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View2 Image</b>";
				}
			}
			if(oProductDetailsForm.getUploadView3ImagePath()!=null && !oProductDetailsForm.getUploadView3ImagePath().getFileName().equals("")){

				bEditProduct =true;
				if(sEmailContetntProdDeatilsInfo.trim().length()>0){
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+", <b>View3 Image</b>";
				}else{				
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"<b>View3 Image</b>";
				}
			}
			System.out.print("EditProdDeatil_EmailContent"+sEmailContetntProdDeatilsInfo);


			if(sEmailContetntProdDeatilsInfo.trim().length()>0){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){	
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"  have been modified ";
				}else{
					sEmailContetntProdDeatilsInfo =sEmailContetntProdDeatilsInfo+"  have been modified ";
				}
			}

			p_hmTags.put("{{ModifiedFields}}",sEmailContetntProdDeatilsInfo);

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			if(bEditProduct){
				EmailLogVO oEmailLogVo=new EmailLogVO();
				Email oEmail=new Email();
				oEmailLogVo.setEmailFrom(sEmailFromAddr);
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){					
					oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));
					if(sCcAddr!=null && sCcAddr.trim().length()>0){
						sCcAddr =oLoginVO.getEmail()+","+sCcAddr;						
					}else{
						sCcAddr =oLoginVO.getEmail();
					}

					oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
					oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));					
				}else{
					oEmailLogVo.setEmailTo(oEmail.convertToStringArray(oLoginVO.getEmail()));
					if(sCcAddr!=null && sCcAddr.trim().length()>0){
						sCcAddr =sAdminEmailAddr+","+sCcAddr;
					}else{
						sCcAddr =sAdminEmailAddr;
					}
					oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
					oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));			
				}
				oEmailLogVo.setEmailSubject(sEmailSubject);
				oEmailLogVo.setEmailText(sEmailContent);


				boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
				if(bEmailSent)				
					oEmailLogVo.setEmailStatus("Y");
				else
					oEmailLogVo.setEmailStatus("N");


				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){					
					oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
					oEmailLogVo.setEmailCcAddr(oLoginVO.getEmail());
					oEmailLogVo.setOwnerType(oLoginVO.getUserType());
					oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));
				}else{
					oEmailLogVo.setEmailToAddr(oLoginVO.getEmail());
					oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);
					oEmailLogVo.setOwnerType(oProductDetailsVO.getOwnerType());
					oEmailLogVo.setOwnerId(Long.parseLong(oProductDetailsVO.getOwnerId()));

				}
				/** Insert Email Log Details **/
	/*EmailDAO.insertEmailLogDetails(oEmailLogVo);

			}
			System.out.print("EmailContent "+sEmailContent);

			logger.info("CatalogueDAO::sendEditProdInfoByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:sendEditProdInfoByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}*/
	public static boolean sendIncompleteProdInfoByEmail(ProductDetailsForm oProductDetailsForm,ProductDetailsVO oProductDetailsVO,LoginVO oLoginVO,String p_sLoginId, String p_sReqeustFrom,String p_sCategory) throws Exception{
		logger.info("CatalogueDAO:sendIncompleteProdInfoByEmail:ENTER");
		/*String sEmailContetnHeader=null,sEmailContentDate=null,sEmailContetntOrderDeatilsHeader=null;
		String sEmailContentDateName =null,sEmailContetntProdDeatilsHeader = null,sEmailContetntProdDeatilsBody =null;
		String sEmailContentProdInfo=null,sEmailContentProdInfoFooter=null,sEmailContentFooter=null;*/
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null;
//		String sOrderDetails="",sEmailContetnStyle=null;
//		ArrayList alOrderItemInfo =null;
//		OrderItemsVO oOrderItemsVO = null;
		Map<String, String> p_hmTags =new HashMap<String, String>();
		String sBccAddr="",sCcAddr="",sSecondaryEmail = "", sToEmail = "";
		EmailVO oEmailVO =null;
//		double dVendorPriceTotal=0.00,dSkyBuyPriceTotal=0.00;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId =null ,sEmailFromAddr =null,sSplInsturction = "";
		String sEmailSendTo = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			ProductDetailsVO oNewProductDetailsVO = null;
			oNewProductDetailsVO = CatalogueDAO.getProductDetails(oProductDetailsVO.getProdId(),oLoginVO.getUserType());

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");


			String sNoImagePath=System.getProperty("noImagePath").trim();
			sNoImagePath = sNoImagePath==null?"":sNoImagePath.trim();	

			
			if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
				sEmailTemplateId = oBundle.getString("email.additem.incomplte.vendor.templateId");		
			}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
				sEmailTemplateId = oBundle.getString("email.additem.incomplete.airline.templateId");
			}
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();

			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);			

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();


			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header

			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{ReturnPolicy}}",oNewProductDetailsVO.getReturnPolicy());


			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oLoginVO.getUserName()));	
					if(oLoginVO.getEmail()!=null){
						sToEmail = oLoginVO.getEmail();	
					}
				}else{
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oProductDetailsVO.getOwnerName()));	
					sToEmail = oProductDetailsVO.getVendorEmail();	


				}

				sSecondaryEmail = oNewProductDetailsVO.getSecondaryEmailId();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sToEmail.contains(sSecondaryEmail)){
						sToEmail = sToEmail + ","+sSecondaryEmail;
					}
				}

				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					sEmailSendTo = System.getProperty("AddProductByAdminMailTo");
				}else{
					sEmailSendTo = System.getProperty("AddProductByVendorMailTo");
				}
				sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
				if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
					sToEmail = sAdminEmailAddr;
				}
				
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");				
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
				}

			}
			if(oProductDetailsForm!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsForm.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsForm.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsForm.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",oProductDetailsForm.getShortDesc());
				p_hmTags.put("{{Category}}",p_sCategory);
				p_hmTags.put("{{FullDescription}}",oProductDetailsForm.getLongDesc());
				if("ADMIN".equalsIgnoreCase(oLoginVO.getUserType())) {
					if(oProductDetailsForm.getSbhPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getSbhPrice())));
					}
				}else {
					if(oProductDetailsForm.getVendPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getVendPrice())));
					}
				}
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");

				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					String sSize = oProductDetailsVO.getSize();
					String sColor = oProductDetailsVO.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	


				if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsForm.getValidFromDate();
					String sValidTo = oProductDetailsForm.getValidToDate();*/
					String sInstrustion = oProductDetailsForm.getInstructions();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/

				}
			}
			if(oProductDetailsVO!=null){	
				p_hmTags.put("{{ProdImagePath}}",sNoImagePath);
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());
			}				

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}				

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));

			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sToEmail+","+sCcAddr;						
			}else{
				sCcAddr =sToEmail;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	

			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			/*if( oLoginVO.getRefId()!=null)			
				oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));
			else
				oEmailLogVo.setOwnerId(0);
			oEmailLogVo.setOwnerType(oLoginVO.getUserType());*/	
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
			oEmailLogVo.setEmailCcAddr(oLoginVO.getEmail());

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			System.out.print("EmailContent "+sEmailContent);

			logger.info("CatalogueDAO::sendIncompleteProdInfoByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:sendIncompleteProdInfoByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	public static boolean sendAddProdInfoByEmail(ProductDetailsForm oProductDetailsForm,ProductDetailsVO oProductDetailsVO,LoginVO oLoginVO,String p_sLoginId,String p_sReqeustFrom,String p_sCategory) throws Exception{
		logger.info("CatalogueDAO:sendAddProdInfoByEmail:ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null;
		Map<String, String> p_hmTags =new HashMap<String, String>();
		EmailVO oEmailVO =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId =null ,sEmailFromAddr =null,sSplInsturction = "";
		String sBccAddr="",sCcAddr="",sToEmail = "",sSecondaryEmail = "";
		String sEmailSendTo = null;
		String sAdminURL = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{

			ProductDetailsVO oNewProductDetailsVO = null;
			oNewProductDetailsVO = CatalogueDAO.getProductDetails(oProductDetailsVO.getProdId(),oLoginVO.getUserType());

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
				sEmailTemplateId = oBundle.getString("email.addcheckout.admin.templateId");	
			}
			else if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
				sEmailTemplateId = oBundle.getString("email.additem.vendor.templateId");		
			}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
				if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
					sEmailTemplateId = oBundle.getString("email.additem.airline.templateId");
				}else{
					sEmailTemplateId = oBundle.getString("email.addpackage.airline.templateId");
				}
			}
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();

			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);			

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();


			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminURL =System.getProperty("SkybuyhighAdminURL");
			sAdminURL=sAdminURL==null?"":sAdminURL.trim();
			
			sAdminEmailAddr= oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header

			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			p_hmTags.put("{{AdminURL}}",sAdminURL);

			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oLoginVO.getUserName()));	
					if(oLoginVO.getEmail()!=null){
						sToEmail = oLoginVO.getEmail();	
					}
				}else{
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oProductDetailsVO.getOwnerName()));	
					sToEmail = oProductDetailsVO.getVendorEmail();


				}

				sSecondaryEmail = oNewProductDetailsVO.getSecondaryEmailId();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sToEmail.contains(sSecondaryEmail)){
						sToEmail = sToEmail + ","+sSecondaryEmail;
					}
				}

				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					sEmailSendTo = System.getProperty("AddProductByAdminMailTo");
				}else{
					sEmailSendTo = System.getProperty("AddProductByVendorMailTo");
				}
				sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
				if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
					sToEmail = sAdminEmailAddr;
				}
				
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");				
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
				}

			}
			if(oProductDetailsForm!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsForm.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsForm.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsForm.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",oProductDetailsForm.getShortDesc());
				p_hmTags.put("{{FullDescription}}",oProductDetailsForm.getLongDesc());
				p_hmTags.put("{{Category}}",p_sCategory);
				p_hmTags.put("{{ReturnPolicy}}",oNewProductDetailsVO.getReturnPolicy());
				
				if("ADMIN".equalsIgnoreCase(oLoginVO.getUserType())) {
					if(oProductDetailsForm.getSbhPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getSbhPrice())));
					}
				}else {
					if(oProductDetailsForm.getVendPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsForm.getVendPrice())));
					}
				}
				if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsForm.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");

				if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd()) || oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					String sSize = oProductDetailsVO.getSize();
					String sColor = oProductDetailsVO.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	




				if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsForm.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsForm.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();*/
					String sInstrustion = oProductDetailsForm.getInstructions();
					sInstrustion = sInstrustion == null?"":sInstrustion.trim();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/

				}
			}
			if(oProductDetailsVO!=null){		
				p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());
			}				

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}				

			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);

			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
				if(sCcAddr!=null && sCcAddr.trim().length()>0){
					sCcAddr =sToEmail+","+sCcAddr;						
				}else{
					sCcAddr =sToEmail;
				}
				oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sCcAddr));
				oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sAdminEmailAddr));
			}else{
				oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sAdminEmailAddr));

				if(sCcAddr!=null && sCcAddr.trim().length()>0){
					sCcAddr =sToEmail+","+sCcAddr;						
				}else{
					sCcAddr =sToEmail;
				}
				oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));

			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			/*if( oLoginVO.getRefId()!=null && !"".equalsIgnoreCase(oLoginVO.getRefId()))			
				oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));
			else
				oEmailLogVo.setOwnerId(0);
			oEmailLogVo.setOwnerType(oLoginVO.getUserType());*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sAdminEmailAddr);
			oEmailLogVo.setEmailCcAddr(oLoginVO.getEmail());

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

			System.out.print("EmailContent "+sEmailContent);

			logger.info("CatalogueDAO::sendAddProdInfoByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:sendAddProdInfoByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	public static boolean sendDeletedProductsByEmail(ProductDetailsVO oProductDetailsVO,LoginVO oLoginVO, String p_sLoginId, 
			String p_sReqeustFrom, String p_sCategory) throws Exception{
		logger.info("CatalogueDAO:sendDeletedProductsByEmail:ENTER");
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null;
		Map<String, String> p_hmTags =new HashMap<String, String>();
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailFromAddr =null,sEmailTemplateId=null,sSplInsturction = "";
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sSkyBuyURL="",sSecondaryEmail = "", sToEmail = "";
		String sEmailSendTo = null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
				sEmailTemplateId=oBundle.getString("email.deletecheckout.admin.templateId");	
			}else if(oLoginVO.getUserType().equalsIgnoreCase("VENDOR")){
				sEmailTemplateId=oBundle.getString("email.deleteitem.vendor.templateId");					
			}else if(oLoginVO.getUserType().equalsIgnoreCase("AIRLINE")){
				if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
					sEmailTemplateId=oBundle.getString("email.deleteitem.airline.templateId");
				}else{
					sEmailTemplateId=oBundle.getString("email.deletepackage.airline.templateId");
				}
			}
			else if(oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				if("Y".equalsIgnoreCase(oProductDetailsVO.getIsSpecialProd())){
					sEmailTemplateId=oBundle.getString("email.deletecheckout.admin.templateId");	
				}else if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
					sEmailTemplateId=oBundle.getString("email.deleteitem.vendor.templateId");		
				}else{
					if("N".equalsIgnoreCase(oProductDetailsVO.getIsAdvt())){
						sEmailTemplateId=oBundle.getString("email.deleteitem.airline.templateId");
					}else{
						sEmailTemplateId=oBundle.getString("email.deletepackage.airline.templateId");
					}
				}					
			}		

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);


			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sAdminEmailAddr= oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);

			if(oLoginVO!=null){
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oLoginVO.getUserName()));	
					if(oLoginVO.getEmail()!=null){
						sToEmail = oLoginVO.getEmail();	
					}
				}else{
					p_hmTags.put("{{Name}}",Utils.toTitleCase(oProductDetailsVO.getOwnerName()));	
					sToEmail = oProductDetailsVO.getVendorEmail();
				}

				sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
				if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
					if(!sToEmail.contains(sSecondaryEmail)){
						sToEmail = sToEmail + ","+sSecondaryEmail;
					}
				}
				
				if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
					sEmailSendTo = System.getProperty("AddProductByAdminMailTo");
				}else{
					sEmailSendTo = System.getProperty("AddProductByVendorMailTo");
				}
				sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
				if("ADMIN".equalsIgnoreCase(sEmailSendTo)){
					sToEmail = sAdminEmailAddr;
				}
				if(oLoginVO.getUserType().equalsIgnoreCase("vendor")){
					p_hmTags.put("{{OwnerType}}","Vendor");
					p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());
					sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
				}else if(oLoginVO.getUserType().equalsIgnoreCase("airline")){
					p_hmTags.put("{{OwnerType}}","Air Charter");	
					p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());
					sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
				}else if(oLoginVO.getUserType().equalsIgnoreCase("admin")){
					if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){					
						p_hmTags.put("{{OwnerType}}","Vendor");	
						p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());
						sSkyBuyURL=System.getProperty("SkybuyhighVendorURL");
					}else{
						p_hmTags.put("{{OwnerType}}","Air Charter");	
						p_hmTags.put("{{ContactName}}",oProductDetailsVO.getOwnerContactName());
						sSkyBuyURL=System.getProperty("SkybuyhighAirlineURL").trim();
					}					
				}
				p_hmTags.put("{{URL}}",sSkyBuyURL);

			}
			if(oProductDetailsVO!=null){
				p_hmTags.put("{{ProdCode}}",oProductDetailsVO.getProdCode());
				p_hmTags.put("{{Brand}}",oProductDetailsVO.getBrandName());
				p_hmTags.put("{{ItemName}}",oProductDetailsVO.getProdTitle());
				p_hmTags.put("{{ShortDesc}}",oProductDetailsVO.getShortDesc());
				p_hmTags.put("{{Category}}",p_sCategory);
				p_hmTags.put("{{FullDescription}}",oProductDetailsVO.getLongDesc());
				p_hmTags.put("{{ReturnPolicy}}",oProductDetailsVO.getReturnPolicy());
				
				if("ADMIN".equalsIgnoreCase(oLoginVO.getUserType())) {
					if(oProductDetailsVO.getSbhPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsVO.getSbhPrice())));
					}
				}else {
					if(oProductDetailsVO.getVendPrice()!=null){
						p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsVO.getVendPrice())));
					}
				}
				/*if(oProductDetailsVO.getVendPrice()!=null){
					p_hmTags.put("{{SbhPrice}}",numberFormat.format(Double.parseDouble(oProductDetailsVO.getVendPrice())));
				}*/
				if(oProductDetailsVO.getInShopStatus().equalsIgnoreCase("A"))
					p_hmTags.put("{{InShop}}","Active");
				else if(oProductDetailsVO.getInShopStatus().equalsIgnoreCase("I"))
					p_hmTags.put("{{InShop}}","Inactive");
				else
					p_hmTags.put("{{InShop}}","Deleted");
				p_hmTags.put("{{ProdImagePath}}",oProductDetailsVO.getMainImgPath());
				p_hmTags.put("{{EffectiveDate}}",oProductDetailsVO.getCategoryDate());

				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("vendor")){
					String sSize = oProductDetailsVO.getSize();
					String sColor = oProductDetailsVO.getColor();					
					if(sSize.trim().length()>0)
						p_hmTags.put("{{Size}}",sSize);
					else
						p_hmTags.put("{{Size}}","N/A");
					if(sColor.trim().length()>0)
						p_hmTags.put("{{Color}}",sColor);
					else
						p_hmTags.put("{{Color}}","N/A");
				}	


				if(oProductDetailsVO.getOwnerType().equalsIgnoreCase("airline")){
					/*String sValidFrom = oProductDetailsVO.getValidFromDate();
					sValidFrom = sValidFrom == null?"":sValidFrom.trim();
					String sValidTo = oProductDetailsVO.getValidToDate();
					sValidTo = sValidTo == null?"":sValidTo.trim();*/
					String sInstrustion = oProductDetailsVO.getInstructions();
					sInstrustion = sInstrustion == null?"":sInstrustion.trim();
					p_hmTags.put("{{SplInstruction}}",sInstrustion);
					/*if(sValidFrom.trim().length()>0)
						sSplInsturction ="Valid From : " + sValidFrom;
					if(sValidTo.trim().length()>0)
						sSplInsturction =sSplInsturction+"&nbsp;&nbsp; To : " + sValidTo;*/

					/*if(sInstrustion.trim().length()>0)
						sSplInsturction =sSplInsturction+"<br/><br/>" + sInstrustion;					

					p_hmTags.put("{{SplInstruction}}",sSplInsturction);*/

				}
			}		



			if(oEmailVO!=null){
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();			

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToEmail));
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;
			}else{
				sCcAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));		
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			System.out.print("EmailContent "+sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");

			/*if(!oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				oEmailLogVo.setOwnerType(oLoginVO.getUserType());
				oEmailLogVo.setOwnerId(Long.parseLong(oLoginVO.getRefId()));
			}else{
				oEmailLogVo.setOwnerType(oProductDetailsVO.getOwnerType());	
				oEmailLogVo.setOwnerId(Long.parseLong(oProductDetailsVO.getOwnerId()));
			}*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(oLoginVO.getEmail());
			oEmailLogVo.setEmailCcAddr("");

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");



			logger.info("CatalogueDAO::sendDeletedProductsByEmail:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO::sendDeletedProductsByEmail:Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}


	public static void insertProdAuditDetails(String p_sProdId, String p_sOwnerId,String p_sOwnerType,String p_sCurrentProdDetails,String p_sPreviousProdDetails,String p_sModifiedFields,String p_sComments,String p_sCreateId,String p_sProcess) throws Exception{	
		logger.info("CatalogueDAO::insertProdAuditDetails:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_prod_audit_msg(?,?,?,?,?,?,?,?,?)}";
		logger.debug("CatalogueDAO::insertProdAuditDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Prod Id : "+p_sProdId);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Owner Id : "+p_sOwnerId);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Owner Type :"+p_sOwnerType);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Currnet Prod Details :"+p_sCurrentProdDetails);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Prev Product Details :"+p_sPreviousProdDetails);
		logger.debug("CatalogueDAO::insertProdAuditDetails:Modified Fields :"+p_sModifiedFields);
		logger.debug("CatalogueDAO::insertProdAuditDetails: Msg :"+p_sComments);
		logger.debug("CatalogueDAO::insertProdAuditDetails: Create Id :"+p_sCreateId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			 
			cstmt.setString(1,p_sProdId);
			cstmt.setString(2,p_sProcess);
			cstmt.setString(3,p_sOwnerId);
			cstmt.setString(4,p_sOwnerType);
			cstmt.setString(5,p_sCurrentProdDetails);
			cstmt.setString(6,p_sPreviousProdDetails);
			cstmt.setString(7,p_sModifiedFields);
			cstmt.setString(8,p_sComments);
			cstmt.setString(9,p_sCreateId);			
			cstmt.execute();			
			logger.info("CatalogueDAO::insertProdAuditDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::insertProdAuditDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();						
		}

	}
	public static String getInsertedItemDetails(ProductDetailsForm p_oProductDetailsForm,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getInsertedItemDetails:ENTER");			
		String sInsertedItemDetails = "";
		try{
			if(p_oProductDetailsForm!=null){
				sInsertedItemDetails = "Category Id:"+p_oProductDetailsForm.getCateId()+"|";
				sInsertedItemDetails = sInsertedItemDetails+"Item Name:"+p_oProductDetailsForm.getProdTitle()+"|";
				sInsertedItemDetails = sInsertedItemDetails+"Brand Name:"+p_oProductDetailsForm.getBrandName()+"|";
				sInsertedItemDetails = sInsertedItemDetails+"Item Code:"+p_oProductDetailsForm.getProdCode()+"|";
				sInsertedItemDetails = sInsertedItemDetails+"Item Status:"+p_oProductDetailsForm.getInShopStatus()+"|";
				if(p_sOwnerType.equalsIgnoreCase("AIRLINE")){
					/*sInsertedItemDetails = sInsertedItemDetails+"Valid From Date:"+p_oProductDetailsForm.getValidFromDate()+"|";
					sInsertedItemDetails = sInsertedItemDetails+"Valid To Date:"+p_oProductDetailsForm.getValidToDate()+"|";*/
					sInsertedItemDetails = sInsertedItemDetails+"Special Instruction:"+p_oProductDetailsForm.getInstructions()+"|";
				}
				if(p_sOwnerType.equalsIgnoreCase("VENDOR"))
					sInsertedItemDetails = sInsertedItemDetails+"Retail Price:"+p_oProductDetailsForm.getVendPrice()+"|";
				else
					sInsertedItemDetails = sInsertedItemDetails+"Price:"+p_oProductDetailsForm.getVendPrice()+"|";

				sInsertedItemDetails = sInsertedItemDetails+"Short Desc:"+p_oProductDetailsForm.getShortDesc()+"|";
				sInsertedItemDetails = sInsertedItemDetails+"Long Desc:"+p_oProductDetailsForm.getLongDesc()+"|";	
				if(p_sOwnerType.equalsIgnoreCase("ADMIN"))
					sInsertedItemDetails = sInsertedItemDetails+"Skybuy Price:"+p_oProductDetailsForm.getSbhPrice()+"|";


			}
			// System.out.println("CatalogueDAO::getInsertedItemDetails:"+sInsertedItemDetails);
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getInsertedItemDetails:Exception "+e.getMessage());
			throw e;
		}


		logger.info("CatalogueDAO::getInsertedItemDetails:EXIT");
		return sInsertedItemDetails;

	}

	public static String getUpdatedItemDetails(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO oOldProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::getUpdatedItemDetails:ENTER");			
		String sCurrentProdDetails = "",sPreviousProdDetails = "" ,sModifiedFields ="";		

		//get Current prod details
		if(p_oProductDetailsForm!=null){
			sCurrentProdDetails = "Category Id:"+p_oProductDetailsForm.getCateId()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Name:"+p_oProductDetailsForm.getProdTitle()+"|";
			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
				sCurrentProdDetails = sCurrentProdDetails+"Brand Name:"+p_oProductDetailsForm.getBrandName()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Code:"+p_oProductDetailsForm.getProdCode()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Status:"+p_oProductDetailsForm.getInShopStatus()+"|";

			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				/*sCurrentProdDetails = sCurrentProdDetails+"Valid From Date:"+p_oProductDetailsForm.getValidFromDate()+"|";
				sCurrentProdDetails = sCurrentProdDetails+"Valid To Date:"+p_oProductDetailsForm.getValidToDate()+"|";*/
				sCurrentProdDetails = sCurrentProdDetails+"Special Instruction:"+p_oProductDetailsForm.getInstructions()+"|";
			}

			if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR"))
				sCurrentProdDetails = sCurrentProdDetails+"Retail Price:"+p_oProductDetailsForm.getVendPrice()+"|";
			else
				sCurrentProdDetails = sCurrentProdDetails+"Price:"+p_oProductDetailsForm.getVendPrice()+"|";

			sCurrentProdDetails = sCurrentProdDetails+"Short Desc:"+p_oProductDetailsForm.getShortDesc()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Long Desc:"+p_oProductDetailsForm.getLongDesc()+"|";	
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
				sCurrentProdDetails = sCurrentProdDetails+"Skybuy Price:"+p_oProductDetailsForm.getSbhPrice()+"|";
		}

		//get Previous prod details
		if(oOldProductDetailsVO!=null){
			sPreviousProdDetails = "Category Id:"+oOldProductDetailsVO.getCateId()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Name:"+oOldProductDetailsVO.getProdTitle()+"|";
			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
				sPreviousProdDetails = sPreviousProdDetails+"Brand Name:"+oOldProductDetailsVO.getBrandName()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Code:"+oOldProductDetailsVO.getProdCode()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Status:"+oOldProductDetailsVO.getInShopStatus()+"|";

			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				/*sPreviousProdDetails = sPreviousProdDetails+"Valid From Date:"+oOldProductDetailsVO.getValidFromDate()+"|";
				sPreviousProdDetails = sPreviousProdDetails+"Valid To Date:"+oOldProductDetailsVO.getValidToDate()+"|";*/
				sPreviousProdDetails = sPreviousProdDetails+"Special Instruction:"+oOldProductDetailsVO.getInstructions()+"|";
			}

			if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR"))
				sPreviousProdDetails = sPreviousProdDetails+"Retail Price:"+oOldProductDetailsVO.getVendPrice()+"|";
			else
				sPreviousProdDetails = sPreviousProdDetails+"Price:"+oOldProductDetailsVO.getVendPrice()+"|";

			sPreviousProdDetails = sPreviousProdDetails+"Short Desc:"+oOldProductDetailsVO.getShortDesc()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Long Desc:"+oOldProductDetailsVO.getLongDesc()+"|";	
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
				sPreviousProdDetails = sPreviousProdDetails+"Skybuy Price:"+oOldProductDetailsVO.getSbhPrice()+"|";
		}



		//get modified fields name
		if(p_oProductDetailsForm.getProdTitle()!=null && oOldProductDetailsVO.getProdTitle()!=null){
			if(!p_oProductDetailsForm.getProdTitle().equals(oOldProductDetailsVO.getProdTitle())){		
				sModifiedFields ="Item Name|";			
			}
		}
		if(p_oProductDetailsForm.getProdCode()!=null && oOldProductDetailsVO.getProdCode()!=null){
			if(!p_oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())){		
				sModifiedFields ="Item Code|";			
			}
		}
		if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR") || p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
			if(!p_oProductDetailsForm.getCateId().equals(oOldProductDetailsVO.getCateId())){			
				sModifiedFields =sModifiedFields+"Category Id|";	
			}	
		}
		if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR") && !p_oProductDetailsForm.getBrandName().equals(oOldProductDetailsVO.getBrandName())){				
			sModifiedFields =sModifiedFields+"Brand Name|";
		}			
		if(!p_oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){		
			sModifiedFields =sModifiedFields+"Item Status|";
		}

		if(!p_oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){	
			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sModifiedFields =sModifiedFields+"Retail Price|";
			}else{				
				sModifiedFields =sModifiedFields+"Price|";
			}
		}
		String sNewPreAuthCC = null;
		String sOldPreAuthCC = null;
		if(p_oProductDetailsForm.getPreAuthCC()!= null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase("")){
			sNewPreAuthCC = p_oProductDetailsForm.getPreAuthCC();
		}else{
			sNewPreAuthCC = "N";
		}
		if(oOldProductDetailsVO.getPreAuthCC()!= null && !oOldProductDetailsVO.getPreAuthCC().equalsIgnoreCase("")){
			sOldPreAuthCC = oOldProductDetailsVO.getPreAuthCC();
		}else{
			sOldPreAuthCC = "N";
		}
		if(!sNewPreAuthCC.equals(sOldPreAuthCC)){	
			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
			}else{				
				sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
			}
		}

		if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
			/*if(!p_oProductDetailsForm.getValidFromDate().equals(oOldProductDetailsVO.getValidFromDate())){		
				sModifiedFields =sModifiedFields+"Valid From Date|";
			}
			if(!p_oProductDetailsForm.getValidToDate().equals(oOldProductDetailsVO.getValidToDate())){		
				sModifiedFields =sModifiedFields+"Valid To Date|";
			}*/
			if(!p_oProductDetailsForm.getInstructions().equals(oOldProductDetailsVO.getInstructions())){		
				sModifiedFields =sModifiedFields+"Special Instruction|";
			}
		}

		if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
			if(!p_oProductDetailsForm.getColor().equals(oOldProductDetailsVO.getColor())){		
				sModifiedFields =sModifiedFields+"Color|";
			}
			if(!p_oProductDetailsForm.getSize().equals(oOldProductDetailsVO.getSize())){		
				sModifiedFields =sModifiedFields+"Size|";
			}

		}


		String sShortDesc =oOldProductDetailsVO.getShortDesc();
		//sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
		sShortDesc = sShortDesc.replaceAll("\n","<br>");
		oOldProductDetailsVO.setShortDesc(sShortDesc);


		String ssShortDesc =p_oProductDetailsForm.getShortDesc();
		ssShortDesc = ssShortDesc ==null?"":ssShortDesc.trim();
		ssShortDesc = ssShortDesc.replaceAll("&nbsp;"," ");
		p_oProductDetailsForm.setShortDesc(ssShortDesc);

		String sLongDesc = oOldProductDetailsVO.getLongDesc();
		//sLongDesc = sLongDesc==null?"":sLongDesc.trim();
		sLongDesc = sLongDesc.replaceAll("\n","<br>");
		oOldProductDetailsVO.setLongDesc(sLongDesc);


		String ssLongDesc =p_oProductDetailsForm.getLongDesc();
		ssLongDesc = ssLongDesc ==null?"":ssLongDesc.trim();
		ssLongDesc = ssLongDesc.replaceAll("&nbsp;"," ");
		p_oProductDetailsForm.setLongDesc(ssLongDesc);

		if(!p_oProductDetailsForm.getShortDesc().equals(oOldProductDetailsVO.getShortDesc())){				
			// System.out.println("new value:"+p_oProductDetailsForm.getShortDesc());
			// System.out.println("old value:"+oOldProductDetailsVO.getShortDesc());			
			sModifiedFields =sModifiedFields+"Short Desc|";
		}

		if(!p_oProductDetailsForm.getLongDesc().equals(oOldProductDetailsVO.getLongDesc())){				
			sModifiedFields =sModifiedFields+"Long Desc|";
		}
		if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
			if(!p_oProductDetailsForm.getSbhPrice().equals(oOldProductDetailsVO.getSbhPrice()))			
				sModifiedFields = sModifiedFields+"Skybuy Price|";

		//update product audit details
		//CatalogueDAO.insertProdAuditDetails(p_oProductDetailsForm.getProdId(), p_oLoginVO.getRefId(),p_oLoginVO.getUserType(),sCurrentProdDetails, sPreviousProdDetails,sModifiedFields,p_oProductDetailsForm.getSbhComment(),p_oLoginVO.getUserName(),"PRODUCT UPDATED");


		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("CatalogueDAO::getUpdatedItemDetails:EXIT");

		return sModifiedFields;

	}

	public static String insertProductAuditTrail(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO oOldProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::insertProductAuditTrail:ENTER");			
		String sCurrentProdDetails = "",sPreviousProdDetails = "" ,sModifiedFields ="";		
		try{
			//get Current prod details
			if(p_oProductDetailsForm!=null){
				sCurrentProdDetails = "Category Id:"+p_oProductDetailsForm.getCateId()+"|";
				sCurrentProdDetails = sCurrentProdDetails+"Item Name:"+p_oProductDetailsForm.getProdTitle()+"|";
				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
					sCurrentProdDetails = sCurrentProdDetails+"Brand Name:"+p_oProductDetailsForm.getBrandName()+"|";
				sCurrentProdDetails = sCurrentProdDetails+"Item Code:"+p_oProductDetailsForm.getProdCode()+"|";
				sCurrentProdDetails = sCurrentProdDetails+"Item Status:"+p_oProductDetailsForm.getInShopStatus()+"|";

				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
					/*sCurrentProdDetails = sCurrentProdDetails+"Valid From Date:"+p_oProductDetailsForm.getValidFromDate()+"|";
					sCurrentProdDetails = sCurrentProdDetails+"Valid To Date:"+p_oProductDetailsForm.getValidToDate()+"|";*/
					sCurrentProdDetails = sCurrentProdDetails+"Special Instruction:"+p_oProductDetailsForm.getInstructions()+"|";
				}

				if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR"))
					sCurrentProdDetails = sCurrentProdDetails+"Retail Price:"+p_oProductDetailsForm.getVendPrice()+"|";
				else
					sCurrentProdDetails = sCurrentProdDetails+"Price:"+p_oProductDetailsForm.getVendPrice()+"|";

				sCurrentProdDetails = sCurrentProdDetails+"Short Desc:"+p_oProductDetailsForm.getShortDesc()+"|";
				sCurrentProdDetails = sCurrentProdDetails+"Long Desc:"+p_oProductDetailsForm.getLongDesc()+"|";	
				if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
					sCurrentProdDetails = sCurrentProdDetails+"Skybuy Price:"+p_oProductDetailsForm.getSbhPrice()+"|";
			}

			//get Previous prod details
			if(oOldProductDetailsVO!=null){
				sPreviousProdDetails = "Category Id:"+oOldProductDetailsVO.getCateId()+"|";
				sPreviousProdDetails = sPreviousProdDetails+"Item Name:"+oOldProductDetailsVO.getProdTitle()+"|";
				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
					sPreviousProdDetails = sPreviousProdDetails+"Brand Name:"+oOldProductDetailsVO.getBrandName()+"|";
				sPreviousProdDetails = sPreviousProdDetails+"Item Code:"+oOldProductDetailsVO.getProdCode()+"|";
				sPreviousProdDetails = sPreviousProdDetails+"Item Status:"+oOldProductDetailsVO.getInShopStatus()+"|";

				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
					/*sPreviousProdDetails = sPreviousProdDetails+"Valid From Date:"+oOldProductDetailsVO.getValidFromDate()+"|";
					sPreviousProdDetails = sPreviousProdDetails+"Valid To Date:"+oOldProductDetailsVO.getValidToDate()+"|";*/
					sPreviousProdDetails = sPreviousProdDetails+"Special Instruction:"+oOldProductDetailsVO.getInstructions()+"|";
				}

				if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR"))
					sPreviousProdDetails = sPreviousProdDetails+"Retail Price:"+oOldProductDetailsVO.getVendPrice()+"|";
				else
					sPreviousProdDetails = sPreviousProdDetails+"Price:"+oOldProductDetailsVO.getVendPrice()+"|";

				sPreviousProdDetails = sPreviousProdDetails+"Short Desc:"+oOldProductDetailsVO.getShortDesc()+"|";
				sPreviousProdDetails = sPreviousProdDetails+"Long Desc:"+oOldProductDetailsVO.getLongDesc()+"|";	
				if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
					sPreviousProdDetails = sPreviousProdDetails+"Skybuy Price:"+oOldProductDetailsVO.getSbhPrice()+"|";
			}



			//get modified fields name
			if(p_oProductDetailsForm.getProdTitle()!=null && oOldProductDetailsVO.getProdTitle()!=null){
				if(!p_oProductDetailsForm.getProdTitle().equals(oOldProductDetailsVO.getProdTitle())){		
					sModifiedFields ="Item Name|";			
				}
			}
			if(p_oProductDetailsForm.getProdCode()!=null && oOldProductDetailsVO.getProdCode()!=null){
				if(!p_oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())){		
					sModifiedFields ="Item Code|";			
				}
			}
			if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR") || p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN")){
				if(!p_oProductDetailsForm.getCateId().equals(oOldProductDetailsVO.getCateId())){			
					sModifiedFields =sModifiedFields+"Category Id|";	
				}	
			}
			if(p_oLoginVO.getUserType().equalsIgnoreCase("VENDOR") && !p_oProductDetailsForm.getBrandName().equals(oOldProductDetailsVO.getBrandName())){				
				sModifiedFields =sModifiedFields+"Brand Name|";
			}			
			if(!p_oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){		
				sModifiedFields =sModifiedFields+"Item Status|";
			}

			if(!p_oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice())){	
				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
					sModifiedFields =sModifiedFields+"Retail Price|";
				}else{				
					sModifiedFields =sModifiedFields+"Price|";
				}
			}
			String sNewPreAuthCC = null;
			String sOldPreAuthCC = null;
			if(p_oProductDetailsForm.getPreAuthCC()!= null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase("")){
				sNewPreAuthCC = p_oProductDetailsForm.getPreAuthCC();
			}else{
				sNewPreAuthCC = "N";
			}
			if(oOldProductDetailsVO.getPreAuthCC()!= null && !oOldProductDetailsVO.getPreAuthCC().equalsIgnoreCase("")){
				sOldPreAuthCC = oOldProductDetailsVO.getPreAuthCC();
			}else{
				sOldPreAuthCC = "N";
			}
			if(!sNewPreAuthCC.equals(sOldPreAuthCC)){	
				if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
					sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
				}else{				
					sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
				}
			}

			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
				/*if(!p_oProductDetailsForm.getValidFromDate().equals(oOldProductDetailsVO.getValidFromDate())){		
					sModifiedFields =sModifiedFields+"Valid From Date|";
				}
				if(!p_oProductDetailsForm.getValidToDate().equals(oOldProductDetailsVO.getValidToDate())){		
					sModifiedFields =sModifiedFields+"Valid To Date|";
				}*/
				if(!p_oProductDetailsForm.getInstructions().equals(oOldProductDetailsVO.getInstructions())){		
					sModifiedFields =sModifiedFields+"Special Instruction|";
				}
			}

			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				if(!p_oProductDetailsForm.getColor().equals(oOldProductDetailsVO.getColor())){		
					sModifiedFields =sModifiedFields+"Color|";
				}
				if(!p_oProductDetailsForm.getSize().equals(oOldProductDetailsVO.getSize())){		
					sModifiedFields =sModifiedFields+"Size|";
				}

			}


			String sShortDesc =oOldProductDetailsVO.getShortDesc();
			//sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
			sShortDesc = sShortDesc.replaceAll("\n","<br>");
			oOldProductDetailsVO.setShortDesc(sShortDesc);


			String ssShortDesc =p_oProductDetailsForm.getShortDesc();
			ssShortDesc = ssShortDesc ==null?"":ssShortDesc.trim();
			ssShortDesc = ssShortDesc.replaceAll("&nbsp;"," ");
			p_oProductDetailsForm.setShortDesc(ssShortDesc);

			String sLongDesc = oOldProductDetailsVO.getLongDesc();
			//sLongDesc = sLongDesc==null?"":sLongDesc.trim();
			sLongDesc = sLongDesc.replaceAll("\n","<br>");
			oOldProductDetailsVO.setLongDesc(sLongDesc);


			String ssLongDesc =p_oProductDetailsForm.getLongDesc();
			ssLongDesc = ssLongDesc ==null?"":ssLongDesc.trim();
			ssLongDesc = ssLongDesc.replaceAll("&nbsp;"," ");
			p_oProductDetailsForm.setLongDesc(ssLongDesc);

			if(!p_oProductDetailsForm.getShortDesc().equals(oOldProductDetailsVO.getShortDesc())){				
				// System.out.println("new value:"+p_oProductDetailsForm.getShortDesc());
				// System.out.println("old value:"+oOldProductDetailsVO.getShortDesc());			
				sModifiedFields =sModifiedFields+"Short Desc|";
			}

			if(!p_oProductDetailsForm.getLongDesc().equals(oOldProductDetailsVO.getLongDesc())){				
				sModifiedFields =sModifiedFields+"Long Desc|";
			}
			if(p_oLoginVO.getUserType().equalsIgnoreCase("ADMIN"))
				if(!p_oProductDetailsForm.getSbhPrice().equals(oOldProductDetailsVO.getSbhPrice()))			
					sModifiedFields = sModifiedFields+"Skybuy Price|";

			if(p_oProductDetailsForm.getUploadMainImagePath()!=null && !p_oProductDetailsForm.getUploadMainImagePath().getFileName().equals("")){
				sModifiedFields = sModifiedFields+"Main Image|";
			}
			if(p_oProductDetailsForm.getUploadView1ImagePath()!=null && !p_oProductDetailsForm.getUploadView1ImagePath().getFileName().equals("")){
				sModifiedFields = sModifiedFields+"View1 Image|";
			}
			if(p_oProductDetailsForm.getUploadView2ImagePath()!=null && !p_oProductDetailsForm.getUploadView2ImagePath().getFileName().equals("")){
				sModifiedFields = sModifiedFields+"View2 Image|";
			}
			if(p_oProductDetailsForm.getUploadView3ImagePath()!=null && !p_oProductDetailsForm.getUploadView3ImagePath().getFileName().equals("")){
				sModifiedFields = sModifiedFields+"View3 Image|";
			}
			
			if(p_oProductDetailsForm.getUploadMainImgCap()!=null && !"".equalsIgnoreCase(p_oProductDetailsForm.getUploadMainImgCap())){
				if(!p_oProductDetailsForm.getUploadMainImgCap().equals(oOldProductDetailsVO.getMainImgCap())){				
					sModifiedFields =sModifiedFields+"Main Image Caption|";
				}
			}else if(oOldProductDetailsVO.getMainImgCap()!=null && !"".equalsIgnoreCase(oOldProductDetailsVO.getMainImgCap())){
				if(!oOldProductDetailsVO.getMainImgCap().equals(p_oProductDetailsForm.getUploadMainImgCap())){				
					sModifiedFields =sModifiedFields+"Main Image Caption|";
				}
			}
			
			if(p_oProductDetailsForm.getUploadView1ImgCap()!=null && !"".equalsIgnoreCase(p_oProductDetailsForm.getUploadView1ImgCap())){
				if(!p_oProductDetailsForm.getUploadView1ImgCap().equals(oOldProductDetailsVO.getView1ImgCap())){				
					sModifiedFields =sModifiedFields+"View1 Image Caption|";
				}
			}else if(oOldProductDetailsVO.getView1ImgCap()!=null && !"".equalsIgnoreCase(oOldProductDetailsVO.getView1ImgCap())){
				if(!oOldProductDetailsVO.getView1ImgCap().equals(p_oProductDetailsForm.getUploadView1ImgCap())){				
					sModifiedFields =sModifiedFields+"View1 Image Caption|";
				}
			}
			
			if(p_oProductDetailsForm.getUploadView2ImgCap()!=null && !"".equalsIgnoreCase(p_oProductDetailsForm.getUploadView2ImgCap())){
				if(!p_oProductDetailsForm.getUploadView2ImgCap().equals(oOldProductDetailsVO.getView2ImgCap())){				
					sModifiedFields =sModifiedFields+"View2 Image Caption|";
				}
			}else if(oOldProductDetailsVO.getView2ImgCap()!=null && !"".equalsIgnoreCase(oOldProductDetailsVO.getView2ImgCap())){
				if(!oOldProductDetailsVO.getView2ImgCap().equals(p_oProductDetailsForm.getUploadView2ImgCap())){				
					sModifiedFields =sModifiedFields+"View2 Image Caption|";
				}
			}
			
			if(p_oProductDetailsForm.getUploadView3ImgCap()!=null && !"".equalsIgnoreCase(p_oProductDetailsForm.getUploadView3ImgCap())){
				if(!p_oProductDetailsForm.getUploadView3ImgCap().equals(oOldProductDetailsVO.getView3ImgCap())){				
					sModifiedFields =sModifiedFields+"View3 Image Caption|";
				}
			}else if(oOldProductDetailsVO.getView3ImgCap()!=null && !"".equalsIgnoreCase(oOldProductDetailsVO.getView3ImgCap())){
				if(!oOldProductDetailsVO.getView3ImgCap().equals(p_oProductDetailsForm.getUploadView3ImgCap())){				
					sModifiedFields =sModifiedFields+"View3 Image Caption|";
				}
			}
			//update product audit details
			CatalogueDAO.insertProdAuditDetails(p_oProductDetailsForm.getProdId(), p_oLoginVO.getRefId(),p_oLoginVO.getUserType(),sCurrentProdDetails, sPreviousProdDetails,sModifiedFields,p_oProductDetailsForm.getSbhComment(),p_oLoginVO.getUserId(),"PRODUCT UPDATED");

		}catch(Exception e){
			logger.info("CatalogueDAO::insertProductAuditTrail:Exception:"+e.getMessage());
		}
		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("CatalogueDAO::insertProductAuditTrail:EXIT");

		return sModifiedFields;

	}
	public static String replaceTagValues(Map<String,String> p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	
	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}
	public static String toTitleCase(String s) {
		char[] chars = s.trim().toLowerCase().toCharArray();
		boolean found = false;

		for (int i=0; i<chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i])) {
				found = false;
			}
		}

		return String.valueOf(chars);
	}

	public static int addAdvertisementDetails(ProductDetailsForm p_oProductDetailsForm,LoginVO p_oLoginVO,String p_sMode, String p_sOwnerType) throws Exception{
		logger.info("CatalogueDAO::addAdvertisementDetails:ENTER");	

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_advertisement_details(?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::addAdvertisementDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::addAdvertisementDetails:Product Id: "+p_oProductDetailsForm.getProdId());	
		logger.debug("CatalogueDAO::addAdvertisementDetails:Ref Id: "+p_oLoginVO.getRefId());
		logger.debug("CatalogueDAO::addAdvertisementDetails:User Type: "+p_oLoginVO.getUserType());
		logger.debug("CatalogueDAO::addAdvertisementDetails:Product Code: "+p_oProductDetailsForm.getProdCode().trim());
		logger.debug("CatalogueDAO::addAdvertisementDetails:Product Title: "+p_oProductDetailsForm.getProdTitle().trim());
		logger.debug("CatalogueDAO::addAdvertisementDetails:InShop Status: "+p_oProductDetailsForm.getInShopStatus());
		logger.debug("CatalogueDAO::addAdvertisementDetails:Mode: "+p_sMode);
		logger.debug("CatalogueDAO::addAdvertisementDetails:User Id "+p_oLoginVO.getUserId());		
		int iProdId = 0;
		try{

			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setInt(2,20);
			cstmt.setString(3,p_oLoginVO.getRefId());
			cstmt.setString(4,p_oLoginVO.getUserType());
			cstmt.setString(5,p_oProductDetailsForm.getProdCode().trim());
			cstmt.setString(6,p_oProductDetailsForm.getProdTitle().trim());
			cstmt.setString(7,p_oProductDetailsForm.getLongDesc().trim());
			cstmt.setString(8,p_oProductDetailsForm.getProdId());
			cstmt.setString(9,p_oProductDetailsForm.getInShopStatus());
			cstmt.setString(10,"Y");
			cstmt.setString(11,p_sMode);
			cstmt.setString(12,p_oLoginVO.getUserId());

			cstmt.execute();
			iProdId=cstmt.getInt(1);

		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::addAdvertisementDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("CatalogueDAO::addAdvertisementDetails:EXIT");
		return iProdId;
	}

	public static List<ProductDetailsVO> getAdvertisementCatalogueDetails(SearchCatalogueForm p_oSearchCatalogueForm,String p_sOwnerId,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getAdvertisementCatalogueDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sImagePath=null,sImageType=null,sViewPath=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_advertisement_details(?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Category: "+p_oSearchCatalogueForm.getCategory());
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Owner Id: "+p_sOwnerId);	
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Upload From Date: "+p_oSearchCatalogueForm.getUploadFromDate());	
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Upload To Date: "+p_oSearchCatalogueForm.getUploadToDate());		
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Prod Status: "+p_oSearchCatalogueForm.getProdStatus());			
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Sbh Prod Status: "+p_oSearchCatalogueForm.getSbhProdStatus());			
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Owner Type: "+p_sOwnerType);
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Search By: "+p_oSearchCatalogueForm.getSearchBy());
		logger.debug("CatalogueDAO::getAdvertisementCatalogueDetails:Search Value: "+p_oSearchCatalogueForm.getSearchValue());	

		ResultSet rs=null;
		List<ProductDetailsVO> alCatalogueInfo=new ArrayList<ProductDetailsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchCatalogueForm.getCategory());
			cstmt.setString(2,p_oSearchCatalogueForm.getUploadFromDate());	
			cstmt.setString(3,p_oSearchCatalogueForm.getUploadToDate());		
			cstmt.setString(4,p_oSearchCatalogueForm.getProdStatus());			
			cstmt.setString(5,p_oSearchCatalogueForm.getSbhProdStatus());			
			cstmt.setString(6,p_sOwnerType);
			cstmt.setString(7,p_sOwnerId);
			cstmt.setString(8,p_oSearchCatalogueForm.getSearchBy());
			cstmt.setString(9,p_oSearchCatalogueForm.getSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				/*oProductDetailsVO.setShortDesc(rs.getString("short_desc"));	
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));	*/

				String sShortDesc = rs.getString("short_desc");
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();
				sShortDesc = sShortDesc.replaceAll(" ","&nbsp;");
				oProductDetailsVO.setShortDesc(sShortDesc);	

				String sLongDesc = rs.getString("long_desc");
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll(" ","&nbsp;");	
				oProductDetailsVO.setLongDesc(sLongDesc);	

				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				sImagePath=rs.getString("main_img_path");
				sImageType=rs.getString("main_img_type");
				sImagePath=sImagePath==null?"":sImagePath.trim();
				sImageType=sImageType==null?"":sImageType.trim();
				if(sImageType.trim().length()>0){
					if(oProductDetailsVO.getCateName()!=null && oProductDetailsVO.getOwnerId()!=null && oProductDetailsVO.getProdId()!=null){			
						sImagePath=sImagePath+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+sImageType;
						sImagePath=sViewPath+sImagePath;
					}
				}else{
					sImagePath=System.getProperty("noImagePath").trim();
					sImagePath = sImagePath==null?"":sImagePath.trim();
				}
				oProductDetailsVO.setMainImgPath(sImagePath);
				oProductDetailsVO.setMainImgType(sImageType);
				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Yes");
				}else if(rs.getString("in_shop_status").equalsIgnoreCase("I")){
					oProductDetailsVO.setInShopStatus("No");
				}
				if(rs.getString("sbh_prod_status").equalsIgnoreCase("N")){
					oProductDetailsVO.setSbhProdStatus("Pending");
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setSbhProdStatus("Accepted");
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("R")){
					oProductDetailsVO.setSbhProdStatus("Rejected");
				}
				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));


				alCatalogueInfo.add(oProductDetailsVO);

			}
			logger.info("CatalogueDAO::getAdvertisementCatalogueDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getAdvertisementCatalogueDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alCatalogueInfo;
	}

	public static ProductDetailsVO getAdvertisementDetails(String p_sProdId) throws Exception{	
		logger.info("CatalogueDAO::getAdvertisementDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_advertisement_by_id(?)}";
		SimpleDateFormat sdfOutput = new SimpleDateFormat  (  "MM/dd/yyyy"  ) ; 
		logger.debug("CatalogueDAO::getAdvertisementDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getAdvertisementDetails:Product Id "+p_sProdId);	


		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sProdId);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));	
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));	
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));

				String sShortDesc = rs.getString("short_desc");			
				sShortDesc = sShortDesc ==null?"":sShortDesc.trim();			
				sShortDesc = sShortDesc.replaceAll("<br/>","\n");	
				sShortDesc = sShortDesc.replaceAll("<br>","\n");	
				//sShortDesc = sShortDesc.replaceAll(" ","\s");	
				oProductDetailsVO.setShortDesc(sShortDesc);	



				String sLongDesc = rs.getString("long_desc");
				sLongDesc = sLongDesc==null?"":sLongDesc.trim();
				sLongDesc = sLongDesc.replaceAll("<br/>","\n");	
				sLongDesc = sLongDesc.replaceAll("<br>","\n");	
				//sLongDesc = sLongDesc.replaceAll("<br/>","\n");	
				oProductDetailsVO.setLongDesc(sLongDesc);	


				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));	
				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));	
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));	
				oProductDetailsVO.setMainImgCap(rs.getString("main_img_caption"));	
				oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));	
				oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));	
				oProductDetailsVO.setView1ImgCap(rs.getString("view1_img_caption"));	
				oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));	
				oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));	
				oProductDetailsVO.setView2ImgCap(rs.getString("view2_img_caption"));	
				oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));	
				oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));
				oProductDetailsVO.setView3ImgCap(rs.getString("view3_img_caption"));	
				oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));	
				oProductDetailsVO.setCategoryDate(sdfOutput.format(rs.getDate("sbh_create_dt")));	
				oProductDetailsVO.setVendorEmail(rs.getString("email"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));		
				oProductDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));	
				String sCcomments = rs.getString("sbh_comments");
				sCcomments = sCcomments==null?"":sCcomments.trim();
				oProductDetailsVO.setSbhComment(sCcomments);	

				/*String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
				oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sNoimagePath=System.getProperty("noImagePath").trim();
				sNoimagePath=sNoimagePath==null?"":sNoimagePath.trim();
				oProductDetailsVO.setNoImgPath(sNoimagePath);

				String sPoPct = rs.getString("po_pct");
				sPoPct = sPoPct==null?"":sPoPct.trim();				
				oProductDetailsVO.setPoPct(sPoPct);

				String sPreAuthCC = rs.getString("preauth_cc");
				sPreAuthCC = sPreAuthCC==null?"":sPreAuthCC.trim();				
				oProductDetailsVO.setPreAuthCC(sPreAuthCC);

				String sColor = rs.getString("color");
				sColor = sColor==null?"":sColor.trim();				
				oProductDetailsVO.setColor(sColor);

				String sSize = rs.getString("size");
				sSize = sSize==null?"":sSize.trim();				
				oProductDetailsVO.setSize(sSize);

			}
			logger.info("CatalogueDAO::getAdvertisementDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getAdvertisementDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oProductDetailsVO;
	}

	public static void deleteAdvertisement(String p_sProdId ,String p_sOwnerName) throws Exception{	
		logger.info("CatalogueDAO::deleteAdvertisement:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_delete_advertisement(?,?)}";

		logger.debug("CatalogueDAO::deleteAdvertisement:SQL QUERY "+sQry);	
		logger.debug("CatalogueDAO::deleteAdvertisement:Prod Id :"+p_sProdId);	
		logger.debug("CatalogueDAO::deleteAdvertisement:Owner Name : "+p_sOwnerName);	


		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProdId);
			cstmt.setString(2, p_sOwnerName);
			cstmt.executeUpdate();			

			logger.info("CatalogueDAO::deleteAdvertisement:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::deleteAdvertisement:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}

	}

	public static String getUpdatedAdvertisementDetails(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO oOldProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::getUpdatedAdvertisementDetails:ENTER");			
		String sCurrentProdDetails = "",sPreviousProdDetails = "" ,sModifiedFields ="";		

		//get Current prod details
		if(p_oProductDetailsForm!=null){
			sCurrentProdDetails = sCurrentProdDetails+"Item Name:"+p_oProductDetailsForm.getProdTitle()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Code:"+p_oProductDetailsForm.getProdCode()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Status:"+p_oProductDetailsForm.getInShopStatus()+"|";
			
			
		}

		//get Previous prod details
		if(oOldProductDetailsVO!=null){
			sPreviousProdDetails = sPreviousProdDetails+"Item Name:"+oOldProductDetailsVO.getProdTitle()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Code:"+oOldProductDetailsVO.getProdCode()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Status:"+oOldProductDetailsVO.getInShopStatus()+"|";
			
		}

		//get modified fields name
		if(!p_oProductDetailsForm.getProdTitle().equals(oOldProductDetailsVO.getProdTitle())){		
			sModifiedFields ="Item Name|";			
		}
		if(!p_oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())){		
			sModifiedFields ="Item Code|";			
		}
				
		if(!p_oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){		
			sModifiedFields =sModifiedFields+"Item Status|";
		}
		
		if(p_oProductDetailsForm.getAdvertisementPath()!=null && !p_oProductDetailsForm.getAdvertisementPath().getFileName().equals("")){
			sModifiedFields =sModifiedFields+"Advertisement Swf|";
		}
		
		if(p_oProductDetailsForm.getAdvtMainImgPath()!=null && !p_oProductDetailsForm.getAdvtMainImgPath().getFileName().equals("")){
			sModifiedFields =sModifiedFields+"Advertisement Main Image|";
		}

		
		//update product audit details
		CatalogueDAO.insertProdAuditDetails(p_oProductDetailsForm.getProdId(), p_oLoginVO.getRefId(),p_oLoginVO.getUserType(),sCurrentProdDetails, sPreviousProdDetails,sModifiedFields,p_oProductDetailsForm.getSbhComment(),p_oLoginVO.getUserId(),"PRODUCT UPDATED");


		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("CatalogueDAO::getUpdatedAdvertisementDetails:EXIT");

		return sModifiedFields;

	}

	public static int updateAdvertisementDetails(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO p_oProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::updateAdvertisementDetails:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_advertisement_details(?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::updateAdvertisementDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateAdvertisementDetails:Product Id: "+p_oProductDetailsForm.getProdId());			
		logger.debug("CatalogueDAO::updateAdvertisementDetails:prod title:"+p_oProductDetailsForm.getProdTitle());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:brand name:"+p_oProductDetailsForm.getBrandName());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:short desc:"+p_oProductDetailsForm.getShortDesc());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:long desc:"+p_oProductDetailsForm.getLongDesc());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:cate id:"+p_oProductDetailsForm.getCateId());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:in shop status:"+p_oProductDetailsForm.getInShopStatus());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:vend price:"+p_oProductDetailsForm.getVendPrice());
		logger.debug("CatalogueDAO::updateAdvertisementDetails:user name:"+p_oLoginVO.getUserId());		
		logger.debug("CatalogueDAO::updateAdvertisementDetails:user type:"+p_oLoginVO.getUserType());
		p_oProductDetailsVO.setMainImgType(p_oProductDetailsVO.getMainImgType()==null?"":p_oProductDetailsVO.getMainImgType().trim());
//		String sPreAuth = null;
		int iIsUpdate = 0;
		try{
//			if(p_oProductDetailsForm.getPreAuthCC()!=null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase(""))
//				sPreAuth = p_oProductDetailsForm.getPreAuthCC();
//			else
//				sPreAuth = "N";
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setInt(2,20);
			cstmt.setString(3,p_oLoginVO.getRefId());
			cstmt.setString(4,p_oLoginVO.getUserType());
			cstmt.setString(5,p_oProductDetailsForm.getProdCode().trim());
			cstmt.setString(6,p_oProductDetailsForm.getProdTitle().trim());
			cstmt.setString(7,p_oProductDetailsForm.getProdId());
			cstmt.setString(8,p_oProductDetailsForm.getInShopStatus());
			cstmt.setString(9,"Y");
			cstmt.setString(10,p_oLoginVO.getUserId());
			cstmt.setString(11,p_oProductDetailsForm.getSbhComment());
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("CatalogueDAO::updateAdvertisementDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateAdvertisementDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}

	public static int updateAdvertisementImage(ProductDetailsVO p_oProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::updateAdvertisementImage:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_advertisement_image(?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::updateAdvertisementImage:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateAdvertisementImage:Product Id: "+p_oProductDetailsVO.getProdId());	
		logger.debug("CatalogueDAO::updateAdvertisementImage:Main Image Type: "+p_oProductDetailsVO.getMainImgType());
		logger.debug("CatalogueDAO::updateAdvertisementImage:Advertisement Type: "+p_oProductDetailsVO.getAdvertisementType());
		logger.debug("CatalogueDAO::updateAdvertisementImage:User Name: "+p_oLoginVO.getUserId().trim());

		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oProductDetailsVO.getProdId().trim());
			cstmt.setString(3,p_oProductDetailsVO.getMainImgType());
			cstmt.setString(4,p_oProductDetailsVO.getView1ImgType());
			cstmt.setString(5,p_oProductDetailsVO.getView2ImgType());
			cstmt.setString(6,p_oProductDetailsVO.getView3ImgType());
			cstmt.setString(7,p_oProductDetailsVO.getMainImgCap());
			cstmt.setString(8,p_oProductDetailsVO.getView1ImgCap());
			cstmt.setString(9,p_oProductDetailsVO.getView2ImgCap());		
			cstmt.setString(10,p_oProductDetailsVO.getView3ImgCap());
			cstmt.setString(11,p_oProductDetailsVO.getAdvertisementType());
			cstmt.setString(12,p_oLoginVO.getUserId().trim());

			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("CatalogueDAO::updateAdvertisementImage:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateAdvertisementImage:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}

	public static boolean isProdCodeExist(String p_sOwnerType, String p_sOwnerId, String p_sProdCode, String p_sProdId) throws Exception{	
		logger.info("CatalogueDAO::isProdCodeExist:ENTER");		
		
		boolean bIsExist = false;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_prod_code_by_owner_id(?,?,?,?)}";

		logger.debug("CatalogueDAO::isProdCodeExist:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::isProdCodeExist:Owner Type "+p_sOwnerType);			
		logger.debug("CatalogueDAO::isProdCodeExist:Owner Id "+p_sOwnerId);
		logger.debug("CatalogueDAO::isProdCodeExist:Prod Code "+p_sProdCode);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1, p_sOwnerType);
			cstmt.setString(2,p_sOwnerId);
			cstmt.setString(3,p_sProdCode);
			cstmt.setString(4, p_sProdId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){	
				bIsExist = true;
			}

			logger.info("CatalogueDAO::isProdCodeExist:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::isProdCodeExist:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return bIsExist;
	}

	public static String getProductStatus(String p_sProdId) throws Exception {
		logger.info("CatalogueDAO::getProductStatus:ENTER");

		String sProductStatus = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_product_status(?)}";

		logger.debug("CatalogueDAO::getProductStatus:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getProductStatus:Owner Type "+p_sProdId);			
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1, p_sProdId);
			rs = cstmt.executeQuery();

			while(rs.next()) {
				sProductStatus = rs.getString("sbh_prod_status");
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::isProdCodeExist:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return sProductStatus;
	}




	public static int saveSpecialProdDetails(ProductDetailsForm p_oProductDetailsForm,LoginVO p_oLoginVO,String p_sMode) throws Exception{
		logger.info("CatalogueDAO::saveSpecialProdDetails:ENTER");	

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_special_product_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::saveSpecialProdDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Product Id: "+p_oProductDetailsForm.getProdId());	
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Product Code:"+p_oProductDetailsForm.getProdCode().trim());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Vendor Proce:"+p_oProductDetailsForm.getVendPrice());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:In Shop Status:"+p_oProductDetailsForm.getInShopStatus());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Sbh Price:"+p_oProductDetailsForm.getSbhPrice());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Color:"+p_oProductDetailsForm.getColor());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Size:"+p_oProductDetailsForm.getSize());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Comments:"+p_oProductDetailsForm.getSbhComment());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Mode:"+p_sMode);
		logger.debug("CatalogueDAO::saveSpecialProdDetails:User Name:"+p_oLoginVO.getUserId());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Owner Id:"+p_oProductDetailsForm.getOwnerId());
		logger.debug("CatalogueDAO::saveSpecialProdDetails:Brand Name:"+p_oProductDetailsForm.getBrandName());
		int iProdId = 0;
		String sPreAuth = null;
		try{
			if(p_oProductDetailsForm.getPreAuthCC()!=null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase(""))
				sPreAuth = p_oProductDetailsForm.getPreAuthCC();
			else
				sPreAuth = "N";

			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oProductDetailsForm.getProdId());
			cstmt.setString(3,p_oProductDetailsForm.getProdCode().trim());
			cstmt.setString(4,p_oProductDetailsForm.getVendPrice());
			cstmt.setString(5,p_oProductDetailsForm.getInShopStatus());
			cstmt.setString(6,p_oProductDetailsForm.getSbhPrice());
			cstmt.setString(7,p_oProductDetailsForm.getColor());
			cstmt.setString(8,p_oProductDetailsForm.getSize());
			cstmt.setString(9,sPreAuth);
			cstmt.setString(10,p_oProductDetailsForm.getSbhComment());
			cstmt.setString(11,p_sMode);
			cstmt.setString(12,p_oLoginVO.getUserId());
			cstmt.setString(13,p_oProductDetailsForm.getOwnerId());
			cstmt.setString(14,p_oProductDetailsForm.getPoPct());
			cstmt.setString(15,p_oProductDetailsForm.getBrandName());
			cstmt.execute();
			iProdId=cstmt.getInt(1);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::saveSpecialProdDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}

		logger.info("CatalogueDAO::saveSpecialProdDetails:EXIT");
		return iProdId;
	}

	public static List<ProductDetailsVO> getSpecialProductDetails(SearchMerchandizeForm p_oSearchCatalogueForm,String p_sOwnerId,String p_sOwnerType) throws Exception{	
		logger.info("CatalogueDAO::getSpecialProductDetails:ENTER");
		
		ProductDetailsVO oProductDetailsVO=null;
		String sViewPath=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_special_prod_details(?,?,?,?,?,?,?,?,?)}";

		logger.debug("CatalogueDAO::getSpecialProductDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getSpecialProductDetails:Category: "+p_oSearchCatalogueForm.getCategory());
		logger.debug("CatalogueDAO::getSpecialProductDetails:Upload From Date:"+p_oSearchCatalogueForm.getUploadFromDate());	
		logger.debug("CatalogueDAO::getSpecialProductDetails:Upload To Date: "+p_oSearchCatalogueForm.getUploadToDate());		
		logger.debug("CatalogueDAO::getSpecialProductDetails:Prod Status: "+p_oSearchCatalogueForm.getProdStatus());			
		logger.debug("CatalogueDAO::getSpecialProductDetails:Sbh Status: "+p_oSearchCatalogueForm.getSbhStatus());			
		logger.debug("CatalogueDAO::getSpecialProductDetails:Owner Type: "+p_sOwnerType);
		logger.debug("CatalogueDAO::getSpecialProductDetails:Owner Id: "+p_sOwnerId);
		logger.debug("CatalogueDAO::getSpecialProductDetails:Search By: "+p_oSearchCatalogueForm.getSearchBy());
		logger.debug("CatalogueDAO::getSpecialProductDetails:Search Value: "+p_oSearchCatalogueForm.getSearchValue());			

		ResultSet rs=null;
		List<ProductDetailsVO> alCatalogueInfo=new ArrayList<ProductDetailsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchCatalogueForm.getCategory());
			cstmt.setString(2,p_oSearchCatalogueForm.getUploadFromDate());	
			cstmt.setString(3,p_oSearchCatalogueForm.getUploadToDate());		
			cstmt.setString(4,p_oSearchCatalogueForm.getProdStatus());			
			cstmt.setString(5,p_oSearchCatalogueForm.getSbhStatus());			
			cstmt.setString(6,p_sOwnerType);
			cstmt.setString(7,p_sOwnerId);
			cstmt.setString(8,p_oSearchCatalogueForm.getSearchBy());
			cstmt.setString(9,p_oSearchCatalogueForm.getSearchValue());	
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));

				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));

				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));

				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else if(rs.getString("in_shop_status").equalsIgnoreCase("I")){
					oProductDetailsVO.setInShopStatus("InActive");
				}
				if(rs.getString("sbh_prod_status").equalsIgnoreCase("N")){
					oProductDetailsVO.setSbhProdStatus(rs.getString("sbh_prod_status"));
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setSbhProdStatus(rs.getString("sbh_prod_status"));
				}else if (rs.getString("sbh_prod_status").equalsIgnoreCase("R")){
					oProductDetailsVO.setSbhProdStatus(rs.getString("sbh_prod_status"));
				}
				String sComments = rs.getString("sbh_comments");
				sComments = sComments==null?"":sComments.trim();
				oProductDetailsVO.setSbhComment(sComments);
				oProductDetailsVO.setAdminApprovalDt(rs.getString("admin_approval_dt"));
				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));


				alCatalogueInfo.add(oProductDetailsVO);

			}
			logger.info("CatalogueDAO::getSpecialProductDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getSpecialProductDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		return alCatalogueInfo;
	}



	public static void deleteSpecialProduct(String p_sProdId ,String p_sOwnerName) throws Exception{	
		logger.info("CatalogueDAO::deleteSpecialProduct:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_delete_advertisement(?,?)}";

		logger.debug("CatalogueDAO::deleteSpecialProduct:SQL QUERY "+sQry);	
		logger.debug("CatalogueDAO::deleteSpecialProduct:Prod Id :"+p_sProdId);	
		logger.debug("CatalogueDAO::deleteSpecialProduct:Owner Name : "+p_sOwnerName);	


		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sProdId);
			cstmt.setString(2, p_sOwnerName);
			cstmt.executeUpdate();			

			logger.info("CatalogueDAO::deleteSpecialProduct:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::deleteSpecialProduct:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}

	}

	public static String getUpdatedSpecialProductDetails(ProductDetailsForm p_oProductDetailsForm,ProductDetailsVO oOldProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::getUpdatedSpecialProductDetails:ENTER");			
		String sCurrentProdDetails = "",sPreviousProdDetails = "" ,sModifiedFields ="";		

		//get Current prod details
		if(p_oProductDetailsForm!=null){
			sCurrentProdDetails = sCurrentProdDetails+"Item Code:"+p_oProductDetailsForm.getProdCode()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Item Status:"+p_oProductDetailsForm.getInShopStatus()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Brand Name:"+p_oProductDetailsForm.getBrandName()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Color:"+p_oProductDetailsForm.getColor()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Size:"+p_oProductDetailsForm.getSize()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Pre-Auth:"+p_oProductDetailsForm.getPreAuthCC()==null?"N":p_oProductDetailsForm.getPreAuthCC().trim()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Retail Price:"+p_oProductDetailsForm.getVendPrice()+"|";
			sCurrentProdDetails = sCurrentProdDetails+"Skybuy Price:"+p_oProductDetailsForm.getSbhPrice()+"|";
		}

		//get Previous prod details
		if(oOldProductDetailsVO!=null){
			sPreviousProdDetails = sPreviousProdDetails+"Item Code:"+oOldProductDetailsVO.getProdCode()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Item Status:"+oOldProductDetailsVO.getInShopStatus()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Brand Name:"+oOldProductDetailsVO.getBrandName()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Color:"+oOldProductDetailsVO.getColor()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Size:"+oOldProductDetailsVO.getSize()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Pre-Auth:"+oOldProductDetailsVO.getPreAuthCC()==null?"N":oOldProductDetailsVO.getPreAuthCC().trim()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Retail Price:"+oOldProductDetailsVO.getVendPrice()+"|";
			sPreviousProdDetails = sPreviousProdDetails+"Skybuy Price:"+oOldProductDetailsVO.getSbhPrice()+"|";
			
			
		}

		//get modified fields name

		
		String sNewPreAuthCC = null;
		String sOldPreAuthCC = null;
		if(p_oProductDetailsForm.getPreAuthCC()!= null && !p_oProductDetailsForm.getPreAuthCC().equalsIgnoreCase("")){
			sNewPreAuthCC = p_oProductDetailsForm.getPreAuthCC();
		}else{
			sNewPreAuthCC = "N";
		}
		if(oOldProductDetailsVO.getPreAuthCC()!= null && !oOldProductDetailsVO.getPreAuthCC().equalsIgnoreCase("")){
			sOldPreAuthCC = oOldProductDetailsVO.getPreAuthCC();
		}else{
			sOldPreAuthCC = "N";
		}
		if(!sNewPreAuthCC.equals(sOldPreAuthCC)){	
			if(oOldProductDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
				sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
			}else{				
				sModifiedFields =sModifiedFields+"Pre-Authorize Credit Card|";
			}
		}

		if(!p_oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){		
			sModifiedFields =sModifiedFields+"Item Status|";
		}

		if(!p_oProductDetailsForm.getProdCode().equals(oOldProductDetailsVO.getProdCode())){		
			sModifiedFields =sModifiedFields+"Item Code|";
		}

		if(!p_oProductDetailsForm.getInShopStatus().equals(oOldProductDetailsVO.getInShopStatus())){		
			sModifiedFields =sModifiedFields+"Item Status|";
		}

		if(!p_oProductDetailsForm.getColor().equals(oOldProductDetailsVO.getColor())){		
			sModifiedFields =sModifiedFields+"Color|";
		}
		
		if(!p_oProductDetailsForm.getSize().equals(oOldProductDetailsVO.getSize())){		
			sModifiedFields =sModifiedFields+"Size|";
		}
		
		if(!p_oProductDetailsForm.getBrandName().equals(oOldProductDetailsVO.getBrandName())){		
			sModifiedFields =sModifiedFields+"Brand Name|";
		}

		
		if(p_oProductDetailsForm.getSbhPrice()!=null && oOldProductDetailsVO.getSbhPrice()!=null){
			if(!p_oProductDetailsForm.getSbhPrice().equals(oOldProductDetailsVO.getSbhPrice()))			
				sModifiedFields = sModifiedFields+"Skybuy Price|";
		}
	
		
		if(p_oProductDetailsForm.getVendPrice()!=null && oOldProductDetailsVO.getVendPrice()!=null){
			if(!p_oProductDetailsForm.getVendPrice().equals(oOldProductDetailsVO.getVendPrice()))			
				sModifiedFields = sModifiedFields+"Vendor Price|";
		}
		
		if(p_oProductDetailsForm.getSpecialProdPath()!=null && !p_oProductDetailsForm.getSpecialProdPath().getFileName().equals("")){
			sModifiedFields = sModifiedFields+"Checkout Product Swf|";
		}
		//update product audit details
		CatalogueDAO.insertProdAuditDetails(p_oProductDetailsForm.getProdId(), p_oLoginVO.getRefId(),p_oLoginVO.getUserType(),sCurrentProdDetails, sPreviousProdDetails,sModifiedFields,p_oProductDetailsForm.getSbhComment(),p_oLoginVO.getUserId(),"PRODUCT UPDATED");


		// System.out.println("updated fields Details:"+sModifiedFields);

		logger.info("CatalogueDAO::getUpdatedSpecialProductDetails:EXIT");

		return sModifiedFields;

	}


	public static int updateSpecialProdSwf(ProductDetailsVO p_oProductDetailsVO,LoginVO p_oLoginVO) throws Exception{	
		logger.info("CatalogueDAO::updateSpecialProdSwf:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_upd_special_prod_swf(?,?,?,?)}";

		logger.debug("CatalogueDAO::updateSpecialProdSwf:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::updateSpecialProdSwf:Product Id: "+p_oProductDetailsVO.getProdId());	
		logger.debug("CatalogueDAO::updateSpecialProdSwf:Special Prod Type: "+p_oProductDetailsVO.getSpecialProdType());
		logger.debug("CatalogueDAO::updateSpecialProdSwf:User Name: "+p_oLoginVO.getUserId().trim());		
		int iIsUpdate = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_oProductDetailsVO.getProdId().trim());
			cstmt.setString(3,p_oProductDetailsVO.getSpecialProdType());
			cstmt.setString(4,p_oLoginVO.getUserId().trim());
			cstmt.executeUpdate();
			iIsUpdate=cstmt.getInt(1);

			logger.info("CatalogueDAO::updateSpecialProdSwf:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::updateSpecialProdSwf:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return iIsUpdate;
	}


	public static SearchMerchandizeForm getNewSpecialProdDetails(SearchMerchandizeForm p_oSearchSplProdForm) throws Exception{	
		logger.info("CatalogueDAO:getNewSpecialProdDetails:ENTER");		
		ProductDetailsVO oProductDetailsVO=null;
		String sViewPath=null,sSbhProdStatus=null;
		sViewPath=System.getProperty("ImageViewPath");
		sViewPath=sViewPath==null?"":sViewPath.trim();
		List<ProductDetailsVO> alMerchandizeInfo=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_new_special_prod_details(?,?,?,?)}";

		logger.debug("CatalogueDAO:getNewSpecialProdDetails:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO:getNewSpecialProdDetails:Search By "+p_oSearchSplProdForm.getSearchBy());
		logger.debug("CatalogueDAO:getNewSpecialProdDetails:Search Value "+p_oSearchSplProdForm.getSearchValue());
		logger.debug("CatalogueDAO:getNewSpecialProdDetails:EffectiveDate "+p_oSearchSplProdForm.getEffectiveDate());

		// System.out.println("CatalogueDAO:getNewSpecialProdDetails:Search By "+p_oSearchSplProdForm.getSearchBy());
		// System.out.println("CatalogueDAO:getNewSpecialProdDetails:Search Value "+p_oSearchSplProdForm.getSearchValue());	
		// System.out.println("CatalogueDAO:getNewSpecialProdDetails:EffectiveDate "+p_oSearchSplProdForm.getEffectiveDate());
		// System.out.println("CatalogueDAO:getNewSpecialProdDetails:prod Status "+p_oSearchSplProdForm.getProdStatus());



		ResultSet rs=null;

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_oSearchSplProdForm.getSearchBy());	
			cstmt.setString(2,p_oSearchSplProdForm.getSearchValue());
			cstmt.setString(3,p_oSearchSplProdForm.getEffectiveDate());
			cstmt.setString(4,p_oSearchSplProdForm.getProdStatus());

			cstmt.execute();
			alMerchandizeInfo=new ArrayList<ProductDetailsVO>();
			rs = cstmt.getResultSet();

			while(rs.next()){				
				oProductDetailsVO=new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));				
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setCateName(rs.getString("cate_name"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setOwnerContactName(rs.getString("owner_contact_name"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));

				String sSBHprice = rs.getString("sbh_price");
				sSBHprice = sSBHprice==null?"":sSBHprice.trim();
				oProductDetailsVO.setSbhPrice(sSBHprice);
				oProductDetailsVO.setEmailId(rs.getString("email_id"));

				if(rs.getString("in_shop_status").equalsIgnoreCase("A")){
					oProductDetailsVO.setInShopStatus("Active");
				}else{
					oProductDetailsVO.setInShopStatus("InActive");
				}
				sSbhProdStatus=rs.getString("sbh_prod_status");
				sSbhProdStatus=sSbhProdStatus==null?"":sSbhProdStatus.trim();
				oProductDetailsVO.setSbhProdStatus(sSbhProdStatus);

				oProductDetailsVO.setEffectiveDate(rs.getString("effective_dt"));
				/*String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();	
				// System.out.println("Prod Id : sValidFromDate:"+oProductDetailsVO.getProdId()+":"+sValidFromDate);
*/
				/*oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/


				String sSize = rs.getString("size");
				sSize = sSize==null?"":sSize.trim();				
				oProductDetailsVO.setSize(sSize);

				String sColor = rs.getString("color");
				sColor = sColor==null?"":sColor.trim();				
				oProductDetailsVO.setColor(sColor);

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sIsAdvt = rs.getString("is_advt");
				sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
				oProductDetailsVO.setIsAdvt(sIsAdvt);

				String sIsSplProd = rs.getString("is_special_prod");
				sIsSplProd = sIsSplProd==null?"":sIsSplProd.trim();				
				oProductDetailsVO.setIsSpecialProd(sIsSplProd);

				alMerchandizeInfo.add(oProductDetailsVO);

			}
			p_oSearchSplProdForm.setRecordSet(alMerchandizeInfo);
			logger.info("CatalogueDAO:getNewSpecialProdDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO:getNewSpecialProdDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return p_oSearchSplProdForm;
	}

	public static String getDeviceType(String p_sDeviceId) throws Exception {
		logger.info("CatalogueDAO::getDeviceType:ENTER");

		String sDeviceType = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_device_type(?)}";

		logger.debug("CatalogueDAO::getDeviceType:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::getDeviceType:Owner Type "+p_sDeviceId);			
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);

			cstmt.setString(1, p_sDeviceId);
			rs = cstmt.executeQuery();

			while(rs.next()) {
				sDeviceType = rs.getString("device_type_name");
				sDeviceType = sDeviceType == null?"":sDeviceType.trim();
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::getDeviceType:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("CatalogueDAO::getDeviceType:EXIT");
		return sDeviceType;
	}
	public static Map<String, List<ProductDetailsVO>> updateSpecialProductStatus(ArrayList p_alMerchandizeDetails,LoginVO p_oLoginVO) throws Exception{
		logger.info("CatalogueDAO::updateSpecialProductStatus:ENTER");
		ProductDetailsVO oProductDetailsVO=null;
		List<ProductDetailsVO> alProductDetails=null;
		int iIsUpdate=-1;
		Map<String, List<ProductDetailsVO>> p_hmMerchandizeDetails=null;
		String sSbhProdStatusDesc = "";
		try{	
			p_hmMerchandizeDetails=new HashMap<String, List<ProductDetailsVO>>();
			for(int i=0;i<p_alMerchandizeDetails.size();i++){
				oProductDetailsVO=(ProductDetailsVO)p_alMerchandizeDetails.get(i);
				String sProdId = oProductDetailsVO.getProdId();
				String sSbhPrice = oProductDetailsVO.getSbhPrice();
				String sSbhProdStatus = oProductDetailsVO.getSbhProdStatus();
				String sSbhComments = oProductDetailsVO.getSbhComment();

				sProdId = sProdId == null?"":sProdId.trim();
				if(sSbhPrice==null || sSbhPrice.trim()=="")
					sSbhPrice="0";
				sSbhProdStatus = sSbhProdStatus == null?"":sSbhProdStatus.trim();
				sSbhComments = sSbhComments == null?"":sSbhComments.trim();	

				if(sSbhProdStatus.equalsIgnoreCase("A"))
					sSbhProdStatusDesc = "PRODUCT ACCEPTED";
				else if(sSbhProdStatus.equalsIgnoreCase("R"))
					sSbhProdStatusDesc = "PRODUCT REJECTED";

				iIsUpdate=MerchandizeDAO.updateMerchandizeDetails(sProdId,sSbhPrice,sSbhProdStatus,sSbhComments,p_oLoginVO.getUserId());

				//update product audit details
				if(sSbhProdStatus.equalsIgnoreCase("A") || sSbhProdStatus.equalsIgnoreCase("R"))
					CatalogueDAO.insertProdAuditDetails(sProdId, p_oLoginVO.getRefId(),  p_oLoginVO.getUserType(),"SkyBuy Price :"+sSbhPrice+"|", null,"SkyBuy Price|",sSbhComments,p_oLoginVO.getUserId(),sSbhProdStatusDesc);

				if(iIsUpdate>0){
					if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("A")){
						if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_A")){
							alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_A");
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}else{
							alProductDetails=new ArrayList<ProductDetailsVO>();
							alProductDetails.add(oProductDetailsVO);
							p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_A", alProductDetails);
						}
					}else if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
						if(oProductDetailsVO.getSbhProdStatus().equalsIgnoreCase("R")){
							if(p_hmMerchandizeDetails.containsKey(oProductDetailsVO.getOwnerId()+"_R")){
								alProductDetails=(ArrayList)p_hmMerchandizeDetails.get(oProductDetailsVO.getOwnerId()+"_R");
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}else{
								alProductDetails=new ArrayList<ProductDetailsVO>();
								alProductDetails.add(oProductDetailsVO);
								p_hmMerchandizeDetails.put(oProductDetailsVO.getOwnerId()+"_R", alProductDetails);
							}
						}
					}
				}
			}	logger.info("CatalogueDAO::updateSpecialProductStatus:EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("CatalogueDAO:updateSpecialProductStatus:Exception "+e.getMessage());
			throw e;
		}	


		return p_hmMerchandizeDetails;
	}
	
	
	
	public static String validateDevicePassword(String p_sDeviceId,String p_sUserId,String p_sPassword) throws Exception{	
		logger.info("CatalogueDAO::validateDevicePassword:ENTER");
		String sStatus = "FALSE";
		Connection con=null;
		String sDeviceCode = null;
		String sDevicePassword = null;
		String sKeyRefId = null;
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_get_device_password(?,?,?,?)}";

		logger.debug("CatalogueDAO::validateDevicePassword:SQL QUERY "+sQry);
		logger.debug("CatalogueDAO::validateDevicePassword:Category: "+p_sDeviceId);
		
		ResultSet rs=null;
		List<ProductDetailsVO> alCatalogueInfo=new ArrayList<ProductDetailsVO>();
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sDeviceId);
			cstmt.registerOutParameter(2, Types.VARCHAR);	
			cstmt.registerOutParameter(3, Types.VARCHAR);	
			cstmt.registerOutParameter(4, Types.VARCHAR);	
			cstmt.execute();
			
			sDeviceCode = cstmt.getString(2);
			sDeviceCode = sDeviceCode == null?"":sDeviceCode.trim();
			
			sDevicePassword = cstmt.getString(3);
			sDevicePassword = sDevicePassword == null?"":sDevicePassword.trim();
			
			sKeyRefId = cstmt.getString(4);
			sKeyRefId = sKeyRefId == null?"":sKeyRefId.trim();
			
			if(!"".equalsIgnoreCase(sKeyRefId)){
				sDevicePassword = OrderDAO.decryptInputData(sDevicePassword, "SERVER", sKeyRefId);
			}
			
			if(p_sUserId.equalsIgnoreCase(sDeviceCode) && p_sPassword.equals(sDevicePassword)){
				sStatus = "TRUE";
			}
			logger.info("CatalogueDAO::validateDevicePassword:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("CatalogueDAO::validateDevicePassword:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		return sStatus;
	}
}
