/**
 * 
 */
package com.sbh.dao;
 
import static com.sbh.util.Utils.dateFormat;
import static com.sbh.util.Utils.findAppropiateDeviceModel;
import static com.sbh.util.Utils.replaceTagValues;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.email.Email;
import com.sbh.util.DBConnection;
import com.sbh.vo.DeviceTypesVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailSchedulerVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.ProductDetailVO;

/**
 * @author Thapovan
 *
 */
public class EmailSchedulerDAO {
	private static Logger logger = LogManager.getLogger(UploadSkyBuyCatalogueDAO.class);
	private static final String SCHEDULER_EMAIL_SEND_TO_AIRLINE="AIRLINE";
	
	public static List<EmailSchedulerVO> getDeviceLastUpdatedDetails() throws Exception {
		logger.info("EmailSchedulerDAO::getDeviceDetails::ENTER");
		
		String sQry="{call usp_sbh_get_device_last_updated_details()}";
		String sDBData = "";
		List<EmailSchedulerVO> alEmailSchedulerVO = new ArrayList<EmailSchedulerVO>();
		EmailSchedulerVO oEmailSchedulerVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		try {
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()){	
					oEmailSchedulerVO = new EmailSchedulerVO();
					oEmailSchedulerVO.setAirCharterName(rs.getString("air_name"));
					oEmailSchedulerVO.setAirId(rs.getString("air_id"));
					oEmailSchedulerVO.setContactName(rs.getString("air_contact_name"));
					oEmailSchedulerVO.setDeviceId(rs.getString("device_id"));	
					oEmailSchedulerVO.setDeviceCode(rs.getString("device_code"));
					oEmailSchedulerVO.setDeviceModel(rs.getString("device_model"));	
					oEmailSchedulerVO.setDeviceSerialNo(rs.getString("device_serial_no"));				
					oEmailSchedulerVO.setDeviceType(rs.getString("device_type_id"));
					oEmailSchedulerVO.setProductNo(rs.getString("product_no"));
					
					sDBData = rs.getString("device_status");
					sDBData = sDBData == null?"":sDBData.trim();
					oEmailSchedulerVO.setStatus(sDBData);
					oEmailSchedulerVO.setAirContactEmail(rs.getString("air_email"));
					oEmailSchedulerVO.setAirContactSecondaryEmail(rs.getString("air_email_secondary"));
					oEmailSchedulerVO.setLastUpdatedDate(rs.getString("last_update_dt"));
					oEmailSchedulerVO.setLastDownloadDate(rs.getString("last_dowloaded_dt"));
//					oEmailSchedulerVO.setCatalogueUpdatedDate(rs.getString("catalogue_updated_date"));
					oEmailSchedulerVO.setSwfUpdateDate(rs.getString("swf_updated_date"));
					oEmailSchedulerVO.setWelcomepageUpdateDate(rs.getString("welcomepage_updated_date"));
					oEmailSchedulerVO.setAdminUpdatedDate(rs.getString("admin_updated_dt"));
					oEmailSchedulerVO.setShoppingcartUpdateDate(rs.getString("shoppingcart_updated_dt"));
					
					sDBData = rs.getString("current_admin_version");
					sDBData = sDBData == null?"":sDBData.trim();
					oEmailSchedulerVO.setCurrentAdminVersion(sDBData);
					
					sDBData = rs.getString("current_catalogue_version");
					sDBData = sDBData == null?"":sDBData.trim();
					oEmailSchedulerVO.setCurrentCatalogueVersion(sDBData);
					
					sDBData = rs.getString("latest_admin_version");
					sDBData = sDBData == null?"":sDBData.trim();
					oEmailSchedulerVO.setLatestAdminVersion(sDBData);
					
					sDBData = rs.getString("latest_catalogue_version");
					sDBData = sDBData == null?"":sDBData.trim();
					oEmailSchedulerVO.setLatestCatalogueVersion(sDBData);
					
					alEmailSchedulerVO.add(oEmailSchedulerVO);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.debug("EmailSchedulerDAO::getDeviceDetails:SQL QUERY "+sQry);
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		
		logger.info("EmailSchedulerDAO::getDeviceDetails::EXIT");
		return alEmailSchedulerVO;
	}
	
	
	public static List<EmailSchedulerVO> getDeviceLatestUpdatedDetails() throws Exception {
		logger.info("EmailSchedulerDAO::getDeviceLatestUpdatedDetails::ENTER");
		
		String sQry="{call usp_sbh_get_device_latest_update_details()}";
		String sDBData = "";
		List<EmailSchedulerVO> alDeviceDetails = new ArrayList<EmailSchedulerVO>();
		EmailSchedulerVO oEmailSchedulerVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
				
		logger.debug("EmailSchedulerDAO::getDeviceLatestUpdatedDetails::SQL QUERY "+sQry);		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				oEmailSchedulerVO = new EmailSchedulerVO();
				sDBData = rs.getString("device_status");
				sDBData = sDBData == null?"":sDBData.trim();
				oEmailSchedulerVO.setStatus(sDBData);
				oEmailSchedulerVO.setDeviceId(rs.getString("device_id")==null?"":rs.getString("device_id").trim());
				oEmailSchedulerVO.setDeviceCode(rs.getString("device_code")==null?"":rs.getString("device_code").trim());
				oEmailSchedulerVO.setDeviceType(rs.getString("device_type_id")==null?"":rs.getString("device_type_id").trim());
				oEmailSchedulerVO.setProductNo(rs.getString("product_no")==null?"":rs.getString("product_no").trim());
				oEmailSchedulerVO.setDeviceModel(rs.getString("device_model")==null?"":rs.getString("device_model").trim());
				oEmailSchedulerVO.setLastUpdatedDate(rs.getString("last_updated_dt")==null?"":rs.getString("last_updated_dt").trim());
				oEmailSchedulerVO.setDeviceSerialNo(rs.getString("device_serial_no")==null?"":rs.getString("device_serial_no").trim());	
				oEmailSchedulerVO.setAirId(rs.getString("air_id")==null?"":rs.getString("air_id").trim());
				oEmailSchedulerVO.setAirCharterName(rs.getString("air_name")==null?"":rs.getString("air_name").trim());
				oEmailSchedulerVO.setContactName(rs.getString("air_contact_name")==null?"":rs.getString("air_contact_name").trim());
				oEmailSchedulerVO.setAirContactEmail(rs.getString("air_email")==null?"":rs.getString("air_email").trim());
				oEmailSchedulerVO.setAirContactSecondaryEmail(rs.getString("air_email_secondary")==null?"":rs.getString("air_email_secondary").trim());
				alDeviceDetails.add(oEmailSchedulerVO);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailSchedulerDAO::getDeviceLatestUpdatedDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
		}
		
		logger.info("EmailSchedulerDAO::getDeviceLatestUpdatedDetails::EXIT");
		return alDeviceDetails;
	}
	
	public static List<DeviceTypesVO> getDeviceTypes() throws Exception {
		logger.info("EmailSchedulerDAO::getDeviceTypes::ENTER");
		
		List<DeviceTypesVO> alDeviceType = new ArrayList<DeviceTypesVO>();
		DeviceTypesVO oDeviceTypesVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_device_types()}";		
		logger.debug("EmailSchedulerDAO::getDeviceTypes::SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				oDeviceTypesVO = new DeviceTypesVO();
				oDeviceTypesVO.setDeviceId(rs.getString("device_type_id")==null?"":rs.getString("device_type_id").trim());
				oDeviceTypesVO.setDeviceName(rs.getString("device_type_name")==null?"":rs.getString("device_type_name").trim());

				alDeviceType.add(oDeviceTypesVO);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailSchedulerDAO::getDeviceTypes::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("EmailSchedulerDAO::getDeviceTypes::EXIT");
		return alDeviceType;
	}
	
	public static List<ProductDetailVO> getProductDetails(String p_sLastDownloadedCateDt,String p_sAirId) throws Exception{	
		logger.info("EmailSchedulerDAO::getProductDetails::ENTER");		
		
		String sQry="{call usp_sbh_get_catalogue_by_date(?,?,?,?,?,?,?,?,?)}";
		String sDBData = "";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		
		ProductDetailVO oProductDetailVO = null;
		List<ProductDetailVO> alProductDetailVO = new ArrayList<ProductDetailVO>();
		
		logger.info("EmailSchedulerDAO::getProductDetails::Qry "+sQry);	
		logger.info("EmailSchedulerDAO::getProductDetails::Last Downloaded Date  "+p_sLastDownloadedCateDt);	
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);						
			cstmt.setString(1,p_sLastDownloadedCateDt);	
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,"");
			cstmt.registerOutParameter(4, Types.VARCHAR);	
			cstmt.registerOutParameter(5, Types.INTEGER);	
			cstmt.registerOutParameter(6, Types.INTEGER);
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.registerOutParameter(8, Types.INTEGER);
			cstmt.registerOutParameter(9, Types.INTEGER);
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs != null) {
				while(rs.next()) {
					oProductDetailVO = new ProductDetailVO();
					sDBData = rs.getString("prod_id");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setProdId(sDBData);
					
					sDBData = rs.getString("cate_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setCategoryName(sDBData);
					
					sDBData = rs.getString("owner_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setOwnerName(sDBData);
					
					sDBData = rs.getString("owner_type");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setOwnerType(sDBData);
					
					sDBData = rs.getString("prod_code");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setProdCode(sDBData);
					
					sDBData = rs.getString("prod_title");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setProdTitle(sDBData);
					
					sDBData = rs.getString("sbh_update_dt");
					sDBData = sDBData == null?"":sDBData.trim();
					oProductDetailVO.setUpdatedDate(sDBData);
					
					alProductDetailVO.add(oProductDetailVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("EmailSchedulerDAO::getProductDetails::EXCEPTION"+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("EmailSchedulerDAO::getProductDetails::EXIT");
		return alProductDetailVO;
	}
	public static void sendEmailToAirlineAboutUpdateAvailable(EmailSchedulerVO p_oSendDeviceDetailsEmailVO, String p_sUpdatesAvailable, String p_sUpdate, List<ProductDetailVO> p_alProductDetails,String p_sLoginId,String p_sReqeustFrom) {
		logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutUpdateAvailable::ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId =null ,sEmailFromAddr =null;
		String sBccAddr="",sCcAddr="";
		String sVendorEmailAdditionalContent = "";
		String sAirlineEmailAdditionalContent = "";
		String sVendorProductDetials = "";
		String sAirlineProductDetials = "";
		String sSendContactEmail = null;
		String sAirlineSecondaryContactEmail = null;
		String sStatus = null;
		String sDeviceType = null;
		String sEmailSendTo = null;
		EmailVO oEmailVO =null;
		List<DeviceTypesVO> alDeviceType = null;
		HashMap<String, String> p_hmTags =new HashMap<String, String>();
		
		try {
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId = oBundle.getString("email.device.updated.details.templateId");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
			
			sEmailSendTo = System.getProperty("SchedulerEmailTo");
			sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
					
			
			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);			
			alDeviceType = getDeviceTypes();
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();


			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header

			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			
			p_hmTags.put("{{AirCharterName}}", p_oSendDeviceDetailsEmailVO.getAirCharterName());
			p_hmTags.put("{{ContactName}}", p_oSendDeviceDetailsEmailVO.getContactName());
			p_hmTags.put("{{DeviceId}}", p_oSendDeviceDetailsEmailVO.getDeviceCode());
			
			sDeviceType = findAppropiateDeviceModel(alDeviceType, p_oSendDeviceDetailsEmailVO.getDeviceType());
			p_hmTags.put("{{DeviceType}}", sDeviceType);
			p_hmTags.put("{{DeviceSerialNo}}", p_oSendDeviceDetailsEmailVO.getDeviceSerialNo());
			p_hmTags.put("{{ProductNo}}", p_oSendDeviceDetailsEmailVO.getProductNo());
			p_hmTags.put("{{DeviceModel}}", p_oSendDeviceDetailsEmailVO.getDeviceModel());
			if("A".equalsIgnoreCase(p_oSendDeviceDetailsEmailVO.getStatus().trim())) {
				sStatus = "Active";
			}else if("AL".equalsIgnoreCase(p_oSendDeviceDetailsEmailVO.getStatus())) {
				sStatus = "Allocated";
			}
			p_hmTags.put("{{Status}}", sStatus);
			p_hmTags.put("{{Update}}", p_sUpdate);
			
			p_hmTags.put("{{LastUpdatedDate}}", p_oSendDeviceDetailsEmailVO.getLastUpdatedDate());
			p_hmTags.put("{{UpdateAvailable}}", p_sUpdatesAvailable);
			
			
			
			sAirlineSecondaryContactEmail = p_oSendDeviceDetailsEmailVO.getAirContactSecondaryEmail();
			sAirlineSecondaryContactEmail = sAirlineSecondaryContactEmail == null?"":sAirlineSecondaryContactEmail.trim();
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sVendorEmailAdditionalContent = oEmailVO.getEmailOptionalContent();
				sAirlineEmailAdditionalContent = oEmailVO.getEmailOptionalContent();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			if(SCHEDULER_EMAIL_SEND_TO_AIRLINE.equalsIgnoreCase(sEmailSendTo)) {
				sCcAddr=sAdminEmailAddr;
				if(sAirlineSecondaryContactEmail != null && sAirlineSecondaryContactEmail.trim().length() > 0) {
					sCcAddr=sAdminEmailAddr+", "+sAirlineSecondaryContactEmail;
				}
				sSendContactEmail = p_oSendDeviceDetailsEmailVO.getAirContactEmail();
				sSendContactEmail = sSendContactEmail == null?"":sSendContactEmail.trim();
			}else {
				sSendContactEmail = sAdminEmailAddr;
			}
			if(p_alProductDetails != null && !p_alProductDetails.isEmpty()) {
				for(ProductDetailVO oProductDetailVO:p_alProductDetails) {
					if(oProductDetailVO != null) {
						if("Vendor".equalsIgnoreCase(oProductDetailVO.getOwnerType())) {
							sVendorProductDetials += "<tr><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getProdCode()+"</td><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getOwnerName()+"</td>" +
									"<td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getProdTitle()+"</td><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getCategoryName()+"</td></tr>";
						}else if("Airline".equalsIgnoreCase(oProductDetailVO.getOwnerType())) {
							sAirlineProductDetials += "<tr><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getProdCode()+"</td><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getOwnerName()+"</td>" +
									"<td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getProdTitle()+"</td><td style=padding:5px;  bgcolor=#FFFFFF>"+oProductDetailVO.getCategoryName()+"</td></tr>";
						}
					}
				}
			}
			sVendorProductDetials = sVendorProductDetials==null?"":sVendorProductDetials.trim();
			if(sVendorProductDetials.trim().length() > 0) {
				p_hmTags.put("{{AllProductDetails}}", sVendorProductDetials);
				p_hmTags.put("{{UpdateType}}", "Gift Catalogue");
				p_hmTags.put("{{PackageType}}", "Merchandise");
				sVendorEmailAdditionalContent=sVendorEmailAdditionalContent==null?"":sVendorEmailAdditionalContent.trim();
				sVendorEmailAdditionalContent=replaceTagValues(p_hmTags,sVendorEmailAdditionalContent,null,null);
				sVendorEmailAdditionalContent=sVendorEmailAdditionalContent==null?"":sVendorEmailAdditionalContent.trim();
			}else {
				sVendorEmailAdditionalContent = "";
			}
			sAirlineProductDetials = sAirlineProductDetials==null?"":sAirlineProductDetials.trim();
			if(sAirlineProductDetials.trim().length() > 0) {
				p_hmTags.put("{{AllProductDetails}}", sAirlineProductDetials);
				p_hmTags.put("{{UpdateType}}", "AirCharter Package");
				p_hmTags.put("{{PackageType}}", "Package");
				sAirlineEmailAdditionalContent=sAirlineEmailAdditionalContent==null?"":sAirlineEmailAdditionalContent.trim();
				sAirlineEmailAdditionalContent=replaceTagValues(p_hmTags,sAirlineEmailAdditionalContent,null,null);
				sAirlineEmailAdditionalContent=sAirlineEmailAdditionalContent==null?"":sAirlineEmailAdditionalContent.trim();
			}else {
				sAirlineEmailAdditionalContent= "";
			}
			p_hmTags.put("{{ProductDetails}}", sVendorEmailAdditionalContent+sAirlineEmailAdditionalContent);
			
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sSendContactEmail));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			
			/*oEmailLogVo.setOwnerId(0);
			oEmailLogVo.setOwnerType("Admin");*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sSendContactEmail);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

//			System.out.print("EmailContent "+sEmailContent);

		}catch(Exception e) {
			e.printStackTrace();
			logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutUpdateAvailable::EXCEPTION"+e.getMessage());
		}
		logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutUpdateAvailable::EXIT");
	}
	
	public static void sendEmailToAirlineAboutDeviceUsage(EmailSchedulerVO p_oSendDeviceDetailsEmailVO,String p_sLoginId,String p_sReqeustFrom) {
		logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutDeviceUsage::ENTER");
		
		String sEmailContent="",sEmailSubject=null,sAdminEmailAddr=null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailTemplateId =null ,sEmailFromAddr =null;
		String sBccAddr="",sCcAddr="";
		String sSendContactEmail = null;
		String sAirlineSecondaryContactEmail = "";
		String sStatus = null;
		String sDeviceType = null;
		String sEmailSendTo = null;
		EmailVO oEmailVO =null;
		List<DeviceTypesVO> alDeviceType = null;
		HashMap<String, String> p_hmTags =new HashMap<String, String>();
		
		try {
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId = oBundle.getString("email.device.usage.by.airline.templateId");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();
			
			sEmailSendTo = System.getProperty("SchedulerEmailDeviceUsageSendTo");
			sEmailSendTo = sEmailSendTo == null?"":sEmailSendTo.trim();
			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);			
			alDeviceType = getDeviceTypes();
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();


			sEmailFromAddr=System.getProperty("email.from.address").trim();
			sEmailFromAddr=sEmailFromAddr==null?"":sEmailFromAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header

			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			
			p_hmTags.put("{{AirCharterName}}", p_oSendDeviceDetailsEmailVO.getAirCharterName());
			p_hmTags.put("{{ContactName}}", p_oSendDeviceDetailsEmailVO.getContactName());
			p_hmTags.put("{{DeviceId}}", p_oSendDeviceDetailsEmailVO.getDeviceCode());
			
			sDeviceType = findAppropiateDeviceModel(alDeviceType, p_oSendDeviceDetailsEmailVO.getDeviceType());
			p_hmTags.put("{{DeviceType}}", sDeviceType);
			p_hmTags.put("{{DeviceSerialNo}}", p_oSendDeviceDetailsEmailVO.getDeviceSerialNo());
			p_hmTags.put("{{ProductNo}}", p_oSendDeviceDetailsEmailVO.getProductNo());
			p_hmTags.put("{{DeviceModel}}", p_oSendDeviceDetailsEmailVO.getDeviceModel());
			if(p_oSendDeviceDetailsEmailVO.getStatus() != null) {
				if("A".equalsIgnoreCase(p_oSendDeviceDetailsEmailVO.getStatus().trim())) {
					sStatus = "Active";
				}else if("AL".equalsIgnoreCase(p_oSendDeviceDetailsEmailVO.getStatus().trim())) {
					sStatus = "Allocated";
				}
			}
			p_hmTags.put("{{Status}}", sStatus);
			
			p_hmTags.put("{{LastUpdatedDate}}", p_oSendDeviceDetailsEmailVO.getLastUpdatedDate());
			
			sSendContactEmail = p_oSendDeviceDetailsEmailVO.getAirContactEmail();
			sSendContactEmail = sSendContactEmail == null?"":sSendContactEmail.trim();
			
			sAirlineSecondaryContactEmail = p_oSendDeviceDetailsEmailVO.getAirContactSecondaryEmail();
			sAirlineSecondaryContactEmail = sAirlineSecondaryContactEmail == null?"":sAirlineSecondaryContactEmail.trim();
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent = oEmailVO.getEmailContent();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			if(SCHEDULER_EMAIL_SEND_TO_AIRLINE.equalsIgnoreCase(sEmailSendTo)) {
				sCcAddr=sAdminEmailAddr;
				if(sAirlineSecondaryContactEmail != null && sAirlineSecondaryContactEmail.trim().length() > 0) {
					sCcAddr=sAdminEmailAddr+", "+sAirlineSecondaryContactEmail;
				}
				sSendContactEmail = p_oSendDeviceDetailsEmailVO.getAirContactEmail();
				sSendContactEmail = sSendContactEmail == null?"":sSendContactEmail.trim();
			}else {
				sSendContactEmail = sAdminEmailAddr;
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(p_hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			

			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sEmailFromAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sSendContactEmail));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			boolean bEmailSent = oEmail.sendEmail(oEmailLogVo);
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");
			
			/*oEmailLogVo.setOwnerId(0);
			oEmailLogVo.setOwnerType("Admin");*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sSendContactEmail);
			oEmailLogVo.setEmailCcAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId,p_sReqeustFrom,"");

//			System.out.print("EmailContent "+sEmailContent);

		}catch(Exception e) {
			e.printStackTrace();
			logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutDeviceUsage::EXCEPTION"+e.getMessage());
		}
		logger.info("EmailSchedulerDAO::sendEmailToAirlineAboutDeviceUsage::EXIT");
	}
	
	public static List<String> getAllAirlineCode() throws Exception {
		logger.info("EmailSchedulerDAO::getAllAirlineCode::ENTER");
		
		String sAirlineCode = null;
		List<String> alAirlineCode = new ArrayList<String>();
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_airline_code()}";		
		logger.debug("EmailSchedulerDAO::getAllAirlineCode::SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				sAirlineCode = rs.getString("air_id");
				alAirlineCode.add(sAirlineCode);				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailSchedulerDAO::getAllAirlineCode::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("EmailSchedulerDAO::getAllAirlineCode::EXIT");
		return alAirlineCode;
	}
}
