package com.sbh.dao;
import static com.sbh.util.Utils.dateStampConversion;
import static com.sbh.contants.SkyBuyContants.*;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.client.vo.UpdatedCatalogueDetailsVO;
import com.sbh.contants.SkyBuyContants;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.SkyBuyCatalogueUploadVO;
import com.sbh.vo.UploadInstallationPackageVO;

public class UploadSkyBuyCatalogueDAO {
	private static Logger logger = LogManager.getLogger(UploadSkyBuyCatalogueDAO.class);
	
	public static long addSkyBuyCatalogueDetails(String p_sLoginId) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyCatalogueDetails::ENTER");		
		
		long lIdentity = 0;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_skybuy_catalogue_uploaded_date(?,?)}";

		logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyCatalogueDetails::SQL QUERY "+sQry);	
		logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyCatalogueDetails::Login Id "+p_sLoginId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, p_sLoginId);
			cstmt.execute();

			lIdentity = cstmt.getInt(1);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::addSkyBuyCatalogueDetails::EXCEPTION "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyCatalogueDetails::EXIT");
		return lIdentity;
	}
	public static int updateSkyBuyCatalogueUploadedDate(String p_sUploadType, String p_sReasonForUpload, String p_sUpdateId,String p_sLoginId) throws Exception {	
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::ENTER");		
		
		int iRowCount = 0;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_upd_skybuy_catalogue_uploaded_date(?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::SQL QUERY "+sQry);	
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::Upload Type "+p_sUploadType);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::Reason For Upload "+p_sReasonForUpload);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::Update Id "+p_sUpdateId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::Login Id "+p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, p_sUploadType);
			cstmt.setString(3, p_sReasonForUpload);
			cstmt.setString(4, p_sUpdateId);
			cstmt.setString(5, p_sLoginId);
			
			cstmt.execute();

			iRowCount = cstmt.getInt(1);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyCatalogueUploadedDate::EXIT");
		return iRowCount;
	}
	
	public static List<SkyBuyCatalogueUploadVO> getSearchSkyBuyCatalogueDetails() 
		throws Exception {
		
		logger.info("UploadSkyBuyCatalogueDAO::getSearchSkyBuyCatalogueDetails::ENTER");		
		
		String sQry="{call usp_sbh_get_skybuy_catalogue_details()}";
		String sUploadType = null;
		List<SkyBuyCatalogueUploadVO> alInstallationPackage = null;
		SkyBuyCatalogueUploadVO oSkyBuyCatalogueUploadVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet resultSet = null;
	
		logger.debug("UploadSkyBuyCatalogueDAO::getSearchSkyBuyCatalogueDetails::SQL QUERY "+sQry);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			resultSet = cstmt.executeQuery();
			if(resultSet != null) {
				alInstallationPackage = new ArrayList<SkyBuyCatalogueUploadVO>();
				while(resultSet.next()) {
					oSkyBuyCatalogueUploadVO = new SkyBuyCatalogueUploadVO();
					oSkyBuyCatalogueUploadVO.setId(resultSet.getString("id"));
					oSkyBuyCatalogueUploadVO.setReasonForUpload(resultSet.getString("reason_for_upload"));
					oSkyBuyCatalogueUploadVO.setCatalogueUpdateDate(dateStampConversion(resultSet.getString("sbh_update_dt")));
					sUploadType = resultSet.getString("upload_type");
					sUploadType = sUploadType==null?"":sUploadType.trim();
					if(UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(sUploadType)) {
						sUploadType = "Welcome Page";
					}else if(UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(sUploadType)) {
						sUploadType = "E-Catalogue";
					}
					oSkyBuyCatalogueUploadVO.setUploadType(sUploadType);
//					oSkyBuyCatalogueUploadVO.setWelcomePageUpdateDate(dateStampConversion(resultSet.getString("skybuy_welcomepage_updated_dt")));
					oSkyBuyCatalogueUploadVO.setSkyBuyCataloguePath(resultSet.getString("swf_path"));
//					oSkyBuyCatalogueUploadVO.setSkyBuyCataloguePath(resultSet.getString("welcomepage_path"));
					oSkyBuyCatalogueUploadVO.setRank(resultSet.getString("rank"));
					alInstallationPackage.add(oSkyBuyCatalogueUploadVO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getSearchSkyBuyCatalogueDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(resultSet, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getSearchSkyBuyCatalogueDetails::EXIT");
		return alInstallationPackage;
	}
	
	public static SkyBuyCatalogueUploadVO getSkyBuyCatalogueInfo(String p_sPackageId) 
		throws Exception {
		
		logger.info("UploadSkyBuyCatalogueDAO::getSkyBuyCatalogueInfo::ENTER");		
		
		String sQry="{call usp_sbh_get_skybuy_catalogue_info(?)}";
		SkyBuyCatalogueUploadVO oSkyBuyCatalogueUploadVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet resultSet = null;
	
		logger.debug("UploadSkyBuyCatalogueDAO::getSkyBuyCatalogueInfo::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::getSkyBuyCatalogueInfo::Package Id "+p_sPackageId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sPackageId);
			resultSet = cstmt.executeQuery();
			if(resultSet != null) {
				oSkyBuyCatalogueUploadVO = new SkyBuyCatalogueUploadVO();
				while(resultSet.next()) {
					oSkyBuyCatalogueUploadVO.setId(resultSet.getString("id"));
					oSkyBuyCatalogueUploadVO.setReasonForUpload(resultSet.getString("reason_for_upload"));
					oSkyBuyCatalogueUploadVO.setUploadType(resultSet.getString("upload_type"));
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getSkyBuyCatalogueInfo::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(resultSet, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getSkyBuyCatalogueInfo::EXIT");
		return oSkyBuyCatalogueUploadVO;
		
	}
	
	public static long addSkyBuyInstallationUploadedDate(String p_sReasonForUpload, String p_sUploadType, String p_sVersion, String p_sLoginId) throws Exception {	
		logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::ENTER");		
		
		long lIdentity = 0;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_skybuy_installation_package_uploaded_date(?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::SQL QUERY "+sQry);	
		logger.debug("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::Reason For Upload "+p_sReasonForUpload);
		logger.debug("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::Upload Type "+p_sUploadType);
		logger.debug("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::Version "+p_sVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, p_sReasonForUpload);
			cstmt.setString(3, p_sUploadType);
			cstmt.setString(4, p_sVersion);
			cstmt.setString(5, p_sLoginId);
			cstmt.execute();

			lIdentity = cstmt.getInt(1);
			
			logger.info("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::addSkyBuyInstallationUploadedDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);			
		}
		return lIdentity;
	}
	
	public static int updateSkyBuyInstallationUploadedDate(String p_sUploadType, String p_sFileName, String p_sUpdateId,String p_sLoginId) throws Exception {	
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::ENTER");		
		
		int iRowCount = 0;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_upd_skybuy_installation_package_uploaded_date(?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Upload Type "+p_sUploadType);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::File Name "+p_sFileName);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Update Id "+p_sUpdateId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, p_sUploadType);
			cstmt.setString(3, p_sFileName);
			cstmt.setString(4, p_sUpdateId);
			cstmt.setString(5, p_sLoginId);
			cstmt.execute();

			iRowCount = cstmt.getInt(1);

		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::EXIT");
		return iRowCount;
	}
	public static int updateClientCertAccountInfo(String p_sUploadType, String p_sFileName, String p_sUsername, String p_sPassword, String p_sRefId,String p_sUpdateId,String p_sLoginId) throws Exception {	
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::ENTER");		
		
		int iRowCount = 0;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_upd_client_cert_account_info(?,?,?,?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Upload Type "+p_sUploadType);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::File Name "+p_sFileName);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Update Id "+p_sUpdateId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Login Id "+p_sLoginId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Login Id "+p_sRefId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, p_sUploadType);
			cstmt.setString(3, p_sFileName);
			cstmt.setString(4, p_sUsername);
			cstmt.setString(5, p_sPassword);
			cstmt.setString(6, p_sRefId);
			cstmt.setString(7, p_sUpdateId);
			cstmt.setString(8, p_sLoginId);
			cstmt.execute();

			iRowCount = cstmt.getInt(1);

		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateSkyBuyInstallationUploadedDate::EXIT");
		return iRowCount;
	}
	public static boolean validateVersion(String p_sCatalogueType, String p_sVersion) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::validateVersion::ENTER");		
		
		String sNewVersion = "";
		boolean bIsVersionValid = false;
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_get_is_valid_version(?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::validateVersion::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::validateVersion::AdminVersion "+p_sVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::validateVersion::CatalogueType "+p_sCatalogueType);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.CHAR);
			cstmt.setString(2, p_sVersion);
			cstmt.setString(3, p_sCatalogueType);
			cstmt.execute();

			sNewVersion = cstmt.getString(1);
			if("Y".equalsIgnoreCase(sNewVersion)) {
				bIsVersionValid = true;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::validateVersion::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::validateVersion::EXIT");
		return bIsVersionValid;
	}
	
	/*public static UpdatedCatalogueDetailsVO isAutoUpdateVersionAvailable(String p_sAutoUpdateVersion) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::isAutoUpdateVersionAvailable::ENTER");		
		
		String sDBData = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs = null;
		UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO = new UpdatedCatalogueDetailsVO();
		String sQry="{call usp_sbh_is_auto_update_version_available(?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::isAutoUpdateVersionAvailable::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::isAutoUpdateVersionAvailable::Auto Update Version "+p_sAutoUpdateVersion);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			cstmt.setString(1, p_sAutoUpdateVersion);
			
			cstmt.execute();
			if(cstmt.getResultSet() != null)
				rs = cstmt.getResultSet();
			
			if(rs != null) {
				while(rs.next()) {
					sDBData = rs.getString("auto_updated_dt");
					sDBData = sDBData == null?"":sDBData.trim();
					oUpdatedCatalogueDetailsVO.setAutoUpdateDate(sDBData);
					
					sDBData = rs.getString("version");
					sDBData = sDBData == null?"":sDBData.trim();
					oUpdatedCatalogueDetailsVO.setAutoUpdateVersion(sDBData);
					
					sDBData = rs.getString("upload_path");
					sDBData = sDBData == null?"":sDBData.trim();
					oUpdatedCatalogueDetailsVO.setAutoUpdateInstallationPath(sDBData);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::isAutoUpdateVersionAvailable::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::isAutoUpdateVersionAvailable::EXIT");
		return oUpdatedCatalogueDetailsVO;
	}*/
	
	public static UpdatedCatalogueDetailsVO isUpdatedVersion(String p_sServiceVersion, String p_sVirtualDesktopVersion, String p_sCatalogueType,
			String p_sLastDownloadDate, String p_sAirId, String p_sAutoUpdateVersion) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::isUpdatedVersion::ENTER");		
		
		String sServiceInstallationPath = "";
		String sServiceUpdateDate = "";
		String sNewServiceVersion = "";
		String sVirtualDesktopInstallationPath = "";
		String sVirtualDesktopUpdateDate = "";
		String sNewVirtualDesktopVersion = "";
		String sAutoUpdateInstallationPath = "";
		String sAutoUpdateUpdateDate = "";
		String sNewAutoUpdateVersion = "";
		String sECatalogueLastUpdateDate = ""; 
		UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO = new UpdatedCatalogueDetailsVO();
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_is_updated_version(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Admin Version "+p_sServiceVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Catalogue Version "+p_sVirtualDesktopVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Catalogue Type "+p_sCatalogueType);
		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Last Download Date "+p_sLastDownloadDate);
		logger.debug("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Air Id "+p_sAirId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.VARCHAR);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.registerOutParameter(3, Types.VARCHAR);
			cstmt.registerOutParameter(4, Types.VARCHAR);
			cstmt.registerOutParameter(5, Types.VARCHAR);
			cstmt.registerOutParameter(6, Types.VARCHAR);
			cstmt.registerOutParameter(7, Types.VARCHAR);
			cstmt.registerOutParameter(8, Types.VARCHAR);
			cstmt.registerOutParameter(9, Types.VARCHAR);
			cstmt.registerOutParameter(10, Types.VARCHAR);
			cstmt.setString(11, p_sServiceVersion);
			cstmt.setString(12, p_sVirtualDesktopVersion);
			cstmt.setString(13, p_sCatalogueType);
			cstmt.setString(14, p_sLastDownloadDate);
			cstmt.setString(15, p_sAirId);
			cstmt.setString(16, p_sAutoUpdateVersion);
			
			cstmt.execute();

			sNewServiceVersion = cstmt.getString(1);
			sNewVirtualDesktopVersion = cstmt.getString(2);
			sServiceInstallationPath = cstmt.getString(3);
			sVirtualDesktopInstallationPath = cstmt.getString(4);
			sServiceUpdateDate = cstmt.getString(5);
			sVirtualDesktopUpdateDate = cstmt.getString(6);
			sECatalogueLastUpdateDate = cstmt.getString(7);
			sNewAutoUpdateVersion = cstmt.getString(8);
			sAutoUpdateUpdateDate = cstmt.getString(9);
			sAutoUpdateInstallationPath = cstmt.getString(10);
			
			oUpdatedCatalogueDetailsVO.setServiceInstallationPath(sServiceInstallationPath == null?"":sServiceInstallationPath.trim());
			oUpdatedCatalogueDetailsVO.setVirtualDesktopInstallationPath(sVirtualDesktopInstallationPath == null?"":sVirtualDesktopInstallationPath.trim());
			oUpdatedCatalogueDetailsVO.setVirtualDesktopVersion(sNewVirtualDesktopVersion == null?"":sNewVirtualDesktopVersion.trim());
			oUpdatedCatalogueDetailsVO.setServiceVersion(sNewServiceVersion == null?"":sNewServiceVersion.trim());
			oUpdatedCatalogueDetailsVO.setServiceUpdateDate(sServiceUpdateDate == null?"":sServiceUpdateDate.trim());
			oUpdatedCatalogueDetailsVO.setVirtualDesktopUpdateDate(sVirtualDesktopUpdateDate == null?"":sVirtualDesktopUpdateDate.trim());
			/*oUpdatedCatalogueDetailsVO.setECatalogueLastUpdateDate(sECatalogueLastUpdateDate == null?"":sECatalogueLastUpdateDate.trim());
			oUpdatedCatalogueDetailsVO.setAutoUpdateVersion(sNewAutoUpdateVersion == null?"":sNewAutoUpdateVersion.trim());
			oUpdatedCatalogueDetailsVO.setAutoUpdateDate(sAutoUpdateUpdateDate == null?"":sAutoUpdateUpdateDate.trim());
			oUpdatedCatalogueDetailsVO.setAutoUpdateInstallationPath(sAutoUpdateInstallationPath == null?"":sAutoUpdateInstallationPath.trim());*/
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::isUpdatedVersion::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::isUpdatedVersion::EXIT");
		return oUpdatedCatalogueDetailsVO;
	}
	
	public static String isDownloadCatalogueAvailable(String p_sLastDownloadedCateDt, String p_sAirId, String p_sIsAirlineChanged) throws Exception{	
		logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sIsCatalogueAvailable = "CatalogueNotAvailable";
		String sDownloadCateDate = null;
		
		int iUpdatedProdCount = 0, iUpdatedAirLogoCount = 0, iUpdateReturnPolicyMsg = 0, iPartnerPriorityUpdated = 0;
		String sQry="{call usp_sbh_get_catalogue_by_date(?,?,?,?,?,?,?,?,?)}";
		
		logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::Qry "+sQry);	
		logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::Last Downloaded Date  "+p_sLastDownloadedCateDt);	
		logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::Air Charter Id  "+p_sAirId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);						
			cstmt.setString(1,p_sLastDownloadedCateDt);	
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,"");
			cstmt.setString(4,p_sIsAirlineChanged);
			
			cstmt.registerOutParameter(5, Types.VARCHAR);	
			cstmt.registerOutParameter(6, Types.INTEGER);	
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.registerOutParameter(8, Types.INTEGER);
			cstmt.registerOutParameter(9, Types.INTEGER);
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs != null) {
				while(rs.next()) {
					String sProdId = rs.getString("prod_id");
					if(sProdId != null && sProdId.trim().length() > 0) {
						sIsCatalogueAvailable = "CatalogueAvailable";
					}
				}
			}
			cstmt.getMoreResults();
			cstmt.getMoreResults();
			/*if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				while(rs.next()){	
					String sURL = rs.getString("URL");
					if(sURL!=null && !"".equalsIgnoreCase(sURL)){
						sIsCatalogueAvailable = "CatalogueAvailable";
					}
				}
			}*/
			sDownloadCateDate = cstmt.getString(5);
			iUpdatedProdCount = cstmt.getInt(6);
			iUpdatedAirLogoCount = cstmt.getInt(7);
			iUpdateReturnPolicyMsg = cstmt.getInt(8);
			iPartnerPriorityUpdated = cstmt.getInt(9);
			if(iUpdatedProdCount > 0 || iUpdatedAirLogoCount > 0 || iUpdateReturnPolicyMsg > 0 || iPartnerPriorityUpdated > 0) {
				sIsCatalogueAvailable = "CatalogueAvailable";
			}
			
			
			ArrayList<UpdatedCatalogueDetailsVO> alInstList = getInstallationPackageByDate(p_sLastDownloadedCateDt);
			if(alInstList!=null && alInstList.size()>0){
				for(UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO:alInstList){
					String sCatgType = oUpdatedCatalogueDetailsVO.getUploadType();
					if(SkyBuyContants.UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(sCatgType) || SkyBuyContants.UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(sCatgType) || SkyBuyContants.UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(sCatgType) || SkyBuyContants.UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sCatgType)){
						sIsCatalogueAvailable = "CatalogueAvailable";
						break;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::EXIT");
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);

		}
		logger.info("UploadSkyBuyCatalogueDAO::isDownloadCatalogueAvailable::EXIT");
		return sIsCatalogueAvailable;
	}
	public static void insertPackageDownloadLogDetails(String p_sDeviceId, String p_sActivity, String p_sDownloadType, 
			String p_sAdminVersion, String p_sCatalogueVersion, String p_sAutoUpdateVersion, String p_sMacAddress, String p_sLoginId) throws Exception {
		
		logger.info("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::ENTER");		
		
		Connection con=null;		
		CallableStatement cstmt=null;

		String sQry="{call usp_sbh_add_package_download_details(?,?,?,?,?,?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Device Id "+p_sDeviceId);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Activity "+p_sActivity);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Download Type "+p_sDownloadType);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Admin Version "+p_sAdminVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Catalogue Version "+p_sCatalogueVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Catalogue Version "+p_sAutoUpdateVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Mac Address "+p_sMacAddress);
		logger.debug("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Login Id "+p_sMacAddress);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			p_sDeviceId = p_sDeviceId==null?"":p_sDeviceId.trim();
			p_sActivity = p_sActivity==null?"":p_sActivity.trim();
			p_sDownloadType = p_sDownloadType==null?"":p_sDownloadType.trim();
			p_sAdminVersion = p_sAdminVersion==null?"":p_sAdminVersion.trim();
			p_sCatalogueVersion = p_sCatalogueVersion==null?"":p_sCatalogueVersion.trim();
			p_sMacAddress = p_sMacAddress==null?"":p_sMacAddress.trim();
			
			cstmt.setString(1, p_sDeviceId);
			cstmt.setString(2, p_sActivity);
			cstmt.setString(3, p_sDownloadType);
			cstmt.setString(4, p_sAdminVersion);
			cstmt.setString(5, p_sCatalogueVersion);
			cstmt.setString(6, p_sAutoUpdateVersion);	
			cstmt.setString(7, p_sMacAddress);
			cstmt.setString(8, p_sLoginId);
			
			cstmt.execute();

		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);		
		}
		logger.info("UploadSkyBuyCatalogueDAO::insertPackageDownloadLogDetails::EXIT");
	}
	
	public static int insertDeviceLogDetails(String p_sAirId,String p_sDeviceId,String p_sFlightNo,String p_sActivity,
			String p_sDownloadType,String p_sAdminVersion,String p_sCatalogueVersion,String p_sMacAddress, 
			String p_sLoginId) throws Exception{
		logger.info("DeviceRegDAO::insertDeviceLogDetails:ENTER");	
		int iInsert = 0; 
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_add_package_download_log_details(?,?,?,?,?,?,?,?,?,?)}";
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+sQry);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sAirId);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sDeviceId);			
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sFlightNo);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sActivity);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sDownloadType);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sAdminVersion);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sCatalogueVersion);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sMacAddress);
		logger.info("DeviceRegDAO::insertDeviceLogDetails::SQL QUERY "+p_sLoginId);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,p_sDeviceId);			
			cstmt.setString(4,p_sFlightNo);
			cstmt.setString(5,p_sActivity);
			cstmt.setString(6,p_sDownloadType);
			cstmt.setString(7,p_sAdminVersion);
			cstmt.setString(8,p_sCatalogueVersion);
			cstmt.setString(9,p_sMacAddress);
			cstmt.setString(10,p_sLoginId);
			
			cstmt.execute();
			iInsert=cstmt.getInt(1);

		}catch(Exception e){
			e.printStackTrace();
			logger.error("DeviceRegDAO::insertDeviceLogDetails "+e.getMessage());
			throw e;
		}

		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);

		}
		
		logger.info("DeviceRegDAO::insertDeviceLogDetails:EXIT");
		return iInsert;
	}
	
	public static List<UploadInstallationPackageVO> getSearchInstallationPackageInfo(String p_sSearchBy, String p_sSearchValue) 
		throws Exception {
		
		logger.info("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::ENTER");		
		
		String sQry="{call usp_sbh_get_installation_package_details(?,?)}";
		String sUploadType = null;
		List<UploadInstallationPackageVO> alInstallationPackage = null;
		UploadInstallationPackageVO oUploadInstallationPackageVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet resultSet = null;
		String sDBData = null;
		String sURL = null;
		logger.debug("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::Search By "+p_sSearchBy);
		logger.debug("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::Search Value "+p_sSearchValue);
		
		try{
			sURL = System.getProperty("ImageViewPath");
			sURL = sURL == null?"":sURL.trim();
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sSearchBy);
			cstmt.setString(2, p_sSearchValue);
			resultSet = cstmt.executeQuery();
			if(resultSet != null) {
				alInstallationPackage = new ArrayList<UploadInstallationPackageVO>();
				while(resultSet.next()) {
					oUploadInstallationPackageVO = new UploadInstallationPackageVO();
					oUploadInstallationPackageVO.setPackageId(resultSet.getString("id"));
					oUploadInstallationPackageVO.setReasonForUpload(resultSet.getString("reason_for_upload"));
					oUploadInstallationPackageVO.setUpdatedDate(dateStampConversion(resultSet.getString("last_updated_date")));
					sUploadType = resultSet.getString("package_type");
					sUploadType = sUploadType==null?"":sUploadType.trim();
					/*if(UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(sUploadType)) {
						sUploadType = "Auto Update";
					}else if(UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(sUploadType)) {
						sUploadType = "Shopping Cart";
					}else if(UPLOADTYPE_USERNAME.equalsIgnoreCase(sUploadType)) {
						sUploadType = "Account Info";
					}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(sUploadType)) {
						sUploadType = "Certificate";
					}*/
					oUploadInstallationPackageVO.setUploadType(sUploadType);
					sDBData = resultSet.getString("upload_path");
					sDBData = sDBData == null?"":sDBData.trim();
					oUploadInstallationPackageVO.setFilePath(sURL+sDBData);
					oUploadInstallationPackageVO.setVersion(resultSet.getString("version"));
					oUploadInstallationPackageVO.setRank(resultSet.getString("rank"));
					alInstallationPackage.add(oUploadInstallationPackageVO);
				}
			}
		}catch(Exception e){ 
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(resultSet, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getSearchInstallationPackageInfo::EXIT");
		return alInstallationPackage;
	}
	
	public static UploadInstallationPackageVO getInstallationPackageInfo(String p_sPackageId) 
		throws Exception {
		
		logger.info("UploadSkyBuyCatalogueDAO::getInstallationPackageInfo::ENTER");		
		
		String sQry="{call usp_sbh_get_installation_package_info(?)}";
		UploadInstallationPackageVO oUploadInstallationPackageVO = null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet resultSet = null;

		logger.debug("UploadSkyBuyCatalogueDAO::getInstallationPackageInfo::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::getInstallationPackageInfo::Package Id "+p_sPackageId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sPackageId);
			resultSet = cstmt.executeQuery();
			if(resultSet != null) {
				oUploadInstallationPackageVO = new UploadInstallationPackageVO();
				while(resultSet.next()) {
					oUploadInstallationPackageVO = new UploadInstallationPackageVO();
					oUploadInstallationPackageVO.setPackageId(resultSet.getString("id"));
					oUploadInstallationPackageVO.setReasonForUpload(resultSet.getString("reason_for_upload"));
					oUploadInstallationPackageVO.setUpdatedDate(dateStampConversion(resultSet.getString("last_updated_date")));
					oUploadInstallationPackageVO.setUploadType(resultSet.getString("package_type"));
					oUploadInstallationPackageVO.setVersion(resultSet.getString("version"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getInstallationPackageInfo::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(resultSet, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getInstallationPackageInfo::EXIT");
		return oUploadInstallationPackageVO;
		
	}
	
	public static String updateInstallationPackageStatus(String p_sPackageId, String p_sLoginId) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::ENTER");		
		
		String sQry="{call usp_sbh_upd_installation_package_status(?,?,?)}";
		String sReturnValue = "";
		Connection con=null;		
		CallableStatement cstmt=null;

		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::Package Id "+p_sPackageId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.CHAR);
			cstmt.setString(2, p_sPackageId);
			cstmt.setString(3, p_sLoginId);
			cstmt.execute();
			sReturnValue = cstmt.getString(1);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateInstallationPackageStatus::EXIT");
		return sReturnValue;
	}
	
	public static void updateInstallationPackageDetails(String p_sPackageId, String p_sFileName, String p_sReasonForUpdate, String p_sUploadType,String p_sLoginId) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::updateInstallationPackage::ENTER");		
		
		String sQry="{call usp_sbh_upd_installation_package_details(?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;

		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::Package Id "+p_sPackageId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::File Name "+p_sFileName);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::Reason For Update "+p_sReasonForUpdate);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::Upload Type "+p_sUploadType);
		logger.debug("UploadSkyBuyCatalogueDAO::updateInstallationPackage::Login Id "+p_sLoginId);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sPackageId);
			cstmt.setString(2, p_sFileName);
			cstmt.setString(3, p_sReasonForUpdate);
			cstmt.setString(4, p_sUploadType);
			cstmt.setString(5, p_sLoginId);
			
			cstmt.execute();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateInstallationPackage::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, con);		
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateInstallationPackage::EXIT");
	}
	
	public static void updateLatestVersionandStatus(String p_sDeviceId, String p_sAdminVersion, String p_sCatalogueVersion, String p_sLastAutoUpdateVersion) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::ENTER");		
		
		String sQry="{call usp_sbh_upd_installation_package_version(?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;

		logger.debug("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::Package Id "+p_sDeviceId);
		logger.debug("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::Package Id "+p_sAdminVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::Package Id "+p_sCatalogueVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::Package Id "+p_sLastAutoUpdateVersion);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sDeviceId);
			cstmt.setString(2, p_sAdminVersion);
			cstmt.setString(3, p_sCatalogueVersion);
			cstmt.setString(4, p_sLastAutoUpdateVersion);
			cstmt.setString(5, InetAddress.getLocalHost().getHostName());
			
			cstmt.execute();
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::Exception "+e.getMessage());
			throw e;
		}
		finally {
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::updateLatestVersionandStatus::EXIT");
	}
	
	public static Map<String,String> getLastUpdatedCatalogueId() throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::getLastUpdatedCatalogueId::ENTER");		
		
		String sQry="{call usp_sbh_get_last_updated_catalogue_Id()}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs = null;
		Map<String,String> mUpdatedSwfIds = null;
		logger.debug("UploadSkyBuyCatalogueDAO::getLastUpdatedCatalogueId::SQL QUERY "+sQry);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			rs = cstmt.executeQuery();
			if(rs != null) {
				mUpdatedSwfIds = new HashMap<String, String>();
				while(rs.next()) {
					mUpdatedSwfIds.put("CataloguePath", rs.getString("catalogue_path"));
				}
			}
			if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				while(rs.next()) {
					mUpdatedSwfIds.put("WelcomepagePath", rs.getString("welcomepage_path"));
					
				}
			}if(cstmt.getMoreResults()) {
				rs = cstmt.getResultSet();
				while(rs.next()) {
					mUpdatedSwfIds.put("PersonalshopperPath", rs.getString("personalshopper_path"));
					
				}
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getLastUpdatedCatalogueId::Exception "+e.getMessage());
			throw e;
		}
		finally {
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getLastUpdatedCatalogueId::EXIT");
		
		return mUpdatedSwfIds;
	}
	
	
	public static ArrayList<UpdatedCatalogueDetailsVO> getDownloadByProductVersion(String p_sServiceVersion,String p_sVirtualDesktopVersion,String p_sSchemaVersion) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::ENTER");		
		
		String sUploadPath = null;
		String sVersion = null;
		String sUploadType = null;
		String sUpdateDate = null;
		ArrayList<UpdatedCatalogueDetailsVO> alCatalogue = new ArrayList<UpdatedCatalogueDetailsVO>();
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs = null;
		UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO = null;
		String sQry="{call usp_sbh_get_download_by_version(?,?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::Virtual Desktop Version "+p_sVirtualDesktopVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::Service Version "+p_sServiceVersion);
		logger.debug("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::Schema Version "+p_sSchemaVersion);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			/*cstmt.setString(1, p_sAutoUpdateVersion);
			cstmt.setString(2, p_sAdminVersion);
			cstmt.setString(3, p_sShoppingCartVersion);*/
			cstmt.setString(1, p_sVirtualDesktopVersion);
			cstmt.setString(2, p_sServiceVersion);
			cstmt.setString(3, p_sSchemaVersion);
			
			rs = cstmt.executeQuery();
						
			if(rs != null) {
				while(rs.next()) {
					oUpdatedCatalogueDetailsVO = new UpdatedCatalogueDetailsVO();
					sUploadPath = rs.getString("upload_path");
					sUploadPath = sUploadPath == null?"":sUploadPath.trim();
					
					sVersion = rs.getString("version");
					sVersion = sVersion == null?"":sVersion.trim();
					
					sUploadType  = rs.getString("upload_type");
					sUploadType = sUploadType == null?"":sUploadType.trim();
					
					sUpdateDate = rs.getString("sbh_update_dt");
					sUpdateDate = sUpdateDate == null?"":sUpdateDate.trim();
					
					/*if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("ADMIN")){
						oUpdatedCatalogueDetailsVO.setAdminInstallationPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setAdminVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("AUTOUPDATE")){
						oUpdatedCatalogueDetailsVO.setAutoUpdateInstallationPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setAutoUpdateVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("SHOPPINGCART")){
						oUpdatedCatalogueDetailsVO.setCatalogueInstallationPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setCatalogueVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}else*/ if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("SCHEMA")){
						oUpdatedCatalogueDetailsVO.setSchemaPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setSchemaVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("SERVICE")){
						oUpdatedCatalogueDetailsVO.setServiceInstallationPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setServiceVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("VIRTUALDESKTOP")){
						oUpdatedCatalogueDetailsVO.setVirtualDesktopInstallationPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setVirtualDesktopVersion(sVersion);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUpdateDate);
					}
					
					alCatalogue.add(oUpdatedCatalogueDetailsVO);
					
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);			
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getDownloadByProductVersion::EXIT");
		return alCatalogue;
	}
	
	
	public static ArrayList<UpdatedCatalogueDetailsVO> getInstallationPackageByDate(String p_sLastDownloadedDate) throws Exception {
		logger.info("UploadSkyBuyCatalogueDAO::getInstallationPackageByDate::ENTER");		
		
		String sUploadPath = null;
		String sVersion = null;
		String sUploadType = null;
		String sUserName = null;
		String sPassword = null;
		String sUpdateDate = null;
		String sKeyRefId = null;
		ArrayList<UpdatedCatalogueDetailsVO> alCatalogue = new ArrayList<UpdatedCatalogueDetailsVO>();
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs = null;
		UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO = null;;
		String sQry="{call usp_sbh_get_installation_pkg_by_date(?,?)}";

		logger.debug("UploadSkyBuyCatalogueDAO::getInstallationPackageByDate::SQL QUERY "+sQry);
		logger.debug("UploadSkyBuyCatalogueDAO::getInstallationPackageByDate::Last Downloaded Date: "+p_sLastDownloadedDate);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			
			cstmt.setString(1, p_sLastDownloadedDate);
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.execute();
			if(cstmt.getResultSet() != null)
				rs = cstmt.getResultSet();
			
			if(rs != null) {
				while(rs.next()) {
					oUpdatedCatalogueDetailsVO = new UpdatedCatalogueDetailsVO();
					sUploadPath = rs.getString("upload_path");
					sUploadPath = sUploadPath == null?"":sUploadPath.trim();
					
					sVersion = rs.getString("version");
					sVersion = sVersion == null?"":sVersion.trim();
					
					sUploadType  = rs.getString("upload_type");
					sUploadType = sUploadType == null?"":sUploadType.trim();
					
					sUserName  = rs.getString("account_username");
					sUserName = sUserName == null?"":sUserName.trim();
					
					sPassword  = rs.getString("account_passcode");
					sPassword = sPassword == null?"":sPassword.trim();
					
					sUpdateDate = rs.getString("sbh_update_dt");
					sUpdateDate = sUpdateDate == null?"":sUpdateDate.trim();
					
					sKeyRefId = rs.getString("key_ref_id");
					sKeyRefId = sKeyRefId == null?"":sKeyRefId.trim();
					
					oUpdatedCatalogueDetailsVO.setKeyRefId(sKeyRefId);
					
					if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("USERNAME")){
						oUpdatedCatalogueDetailsVO.setNetworkUsername(sUserName);
						oUpdatedCatalogueDetailsVO.setNetworkPassword(sPassword);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUploadType);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase("CLIENTCERT")){
						oUpdatedCatalogueDetailsVO.setClientCertPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setClientCertPassword(sPassword);
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setUpdateDate(sUploadType);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_IPHONEDESKTOPIMAGE)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setIPhoneDesktopPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setIPhoneDesktopUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_IPHONEPERSONAL)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setIPhonePersonalShopperPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setIPhonePersonalShopperUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_IPHONEWELCOMEPAGE)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setIPhoneWelcomePagePath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setIPhoneWelcomePageUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_WELCOMEPAGE)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setWelcomePagePath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setWelcomePageUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_SKYBUYCATALOGUE)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setECataloguePath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setECatalogueUpdateDate(sUpdateDate);
					}else if(!"".equalsIgnoreCase(sUploadType) && sUploadType.equalsIgnoreCase(SkyBuyContants.UPLOADTYPE_PERSONALSHOPPER)){
						oUpdatedCatalogueDetailsVO.setUploadType(sUploadType);
						oUpdatedCatalogueDetailsVO.setPersonalShopperPath(sUploadPath);
						oUpdatedCatalogueDetailsVO.setPersonalShopperUpdateDate(sUpdateDate);
					}
					
					alCatalogue.add(oUpdatedCatalogueDetailsVO);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("UploadSkyBuyCatalogueDAO::getInstallationPackageByDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);
		}
		
		logger.info("UploadSkyBuyCatalogueDAO::getInstallationPackageByDate::EXIT");
		return alCatalogue;
	}
}
