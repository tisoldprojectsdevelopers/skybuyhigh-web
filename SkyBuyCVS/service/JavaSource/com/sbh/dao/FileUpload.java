package com.sbh.dao;

import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_ADMIN;
import static com.sbh.contants.SkyBuyContants.*;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_CLIENTCERT;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SCHEMA;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SERVICE;
import static com.sbh.contants.SkyBuyContants.UPLOADTYPE_SHOPPINGCART;
import static com.sbh.util.Utils.tokeniseFileName;
import static com.sbh.util.Utils.tokeniseImgType;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

public class FileUpload {
	private static Logger logger = LogManager.getLogger(FileUpload.class);
	
	public static boolean uploadSkyBuyCatalogue(FormFile oCatalogueFormFile, String sFileUploadPath, String p_sFolderName, String p_sFileName, String p_sDemoCatalogueFolderPath) {
		logger.info("FileUpload:uploadSkyBuyCatalogue::Entry");

		boolean bSkybuyCatalogue = false;
		String sSrcFilePath = "";
		String sDemoSrcFilePath = "";
		try{
			/*if(oCatalogueFormFile!=null){
				sFileName   = oCatalogueFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = UploadInstallationPackageandECatalogue.tokeniseImgType(sFileName);
			}*/				
			sSrcFilePath = sFileUploadPath+"\\"+p_sFolderName;
			sDemoSrcFilePath = p_sDemoCatalogueFolderPath;
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSrcSwfPath = new File(sSrcFilePath);
				if(!fSrcSwfPath .exists())
					fSrcSwfPath .mkdirs();
				
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fDemoSrcFilePath = new File(sDemoSrcFilePath);
				if(!fDemoSrcFilePath .exists())
					fDemoSrcFilePath .mkdirs();	

			if(!oCatalogueFormFile.equals("")){  
				//Create original Image
				File fFilePath = new File(sSrcFilePath,p_sFileName);
				File fDemoFilePath = new File(sDemoSrcFilePath,p_sFileName);
				//If file does not exists create file                      
				if(fFilePath.exists())
					fFilePath.delete();
				if(!fFilePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fFilePath);
					oFileOutputStream.write(oCatalogueFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
					bSkybuyCatalogue = true;
				}
				if(fDemoFilePath.exists())
					fDemoFilePath.delete();
				if(!fDemoFilePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fDemoFilePath);
					oFileOutputStream.write(oCatalogueFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
				}
			}	
			logger.info("FileUpload:uploadSkyBuyCatalogue::Exit");
		}catch(Exception e){
			logger.info("FileUpload:uploadSkyBuyCatalogue::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return bSkybuyCatalogue;
	}
	
	public static boolean uploadWelcomePage(FormFile oWelcomePageFormFile, String sFileUploadPath, String p_sFolderName, String p_sFileName) {
		logger.info("FileUpload:uploadWelcomePage::Entry");
		
		boolean bSkybuyWelcome = false;
		String sSrcFilePath = "";
		try{
			/*if(oWelcomePageFormFile!=null){
				sFileName   = oWelcomePageFormFile.getFileName();
				if(sFileName != null)
					sImageType 	   = UploadInstallationPackageandECatalogue.tokeniseImgType(sFileName);
			}*/				
			sSrcFilePath = sFileUploadPath+"\\"+p_sFolderName;
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
				File fSrcSwfPath = new File(sSrcFilePath);
				if(!fSrcSwfPath .exists())
					fSrcSwfPath .mkdirs();

			if(!oWelcomePageFormFile.equals("")){  
				//Create original Image
				File fDestFilePath = new File(sSrcFilePath,p_sFileName);
				//If file does not exists create file                      
				if(fDestFilePath.exists())
					fDestFilePath.delete();
				if(!fDestFilePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fDestFilePath);
					oFileOutputStream.write(oWelcomePageFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
					bSkybuyWelcome = true;
				}  
			}	
			logger.info("FileUpload:uploadWelcomePage::Exit");
		}catch(Exception e){
			logger.info("FileUpload:uploadWelcomePage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return bSkybuyWelcome;
	}
	
	/*
	public static List uploadSkyBuyInstallationPackage(FormFile oCatalogueFormFile, String sFileUploadPath,String sSrcFilePath,String p_sFileName, String p_sUploadType, String p_sVersion, String p_sFolderName) {
		logger.info("FileUpload:uploadSkyBuyInstallationPackage::Entry");
		
		Boolean bIsInstallationPackageUploaded = false;
		String sFileType = "";
		String sFileName = "";
		String sClientFileName = "";
		File fDestFilePath = null;
		List alImageInfo = new ArrayList();
		try{
			if(p_sFileName != null) {
				sFileType = tokeniseImgType(p_sFileName);
				sFileType = sFileType == null?"":sFileType.trim();
				sClientFileName = tokeniseFileName(p_sFileName);
				sClientFileName = sClientFileName == null?"":sClientFileName.trim();
			}
			sFileName = p_sFolderName;
			sFileName = sFileName == null?"":sFileName.trim();
			sSrcFilePath = sFileUploadPath+"\\"+sFileName;
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
			File fSwfPath = new File(sSrcFilePath);
			if(!fSwfPath .exists())
				fSwfPath .mkdirs();

			if(!oCatalogueFormFile.equals("")) {  
				//Create the file to upload.
				if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_SERVICE.equalsIgnoreCase(p_sUploadType)
						|| UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_ADMIN.equalsIgnoreCase(p_sUploadType)
						|| UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(p_sUploadType)) {
					fDestFilePath = new File(sSrcFilePath,sFileName+"_"+p_sVersion+"."+sFileType);
				}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(p_sUploadType)) {
					fDestFilePath = new File(sSrcFilePath,sClientFileName+"."+sFileType);
				}
				else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sIphondeDesktopImageName = System.getProperty(UPLOADTYPE_IPHONEDESKTOPIMAGE);
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sIphondeDesktopImageName+"."+sFileType);
				}
				//If file does not exists create file
				if(fDestFilePath != null) {
					if(fDestFilePath.exists())
						fDestFilePath.delete();
					if(!fDestFilePath.exists()){
						FileOutputStream oFileOutputStream = new FileOutputStream(fDestFilePath);
						oFileOutputStream.write(oCatalogueFormFile.getFileData());					
						oFileOutputStream.flush();
						oFileOutputStream.close();
						bIsInstallationPackageUploaded = true;
					}  
				}
				alImageInfo.add(0,bIsInstallationPackageUploaded);
				alImageInfo.add(1,fDestFilePath.getName());
			}
			logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exit");
		}catch(Exception e){
			logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}
}
*/


/*public static List uploadSkyBuyInstallationPackage(FormFile oCatalogueFormFile, String p_sFileName, String p_sUploadType, String p_sVersion, String p_sFolderName) {
	logger.info("FileUpload:uploadSkyBuyInstallationPackage::Entry");
	
	Boolean bIsInstallationPackageUploaded = false;
	String sFileType = "";
	String sFileName = "";
	String sOriginalFileName = "";
	File fDestFilePath = null;
	List alImageInfo = new ArrayList();
	String sFileUploadPath = null;
	String sInstallationPackageFilePath = null;
	String sSrcFilePath = "";
	String sStaticPath = null;
	String sAbsolutePath = null;
	try{
		sInstallationPackageFilePath = System.getProperty("InstallationPackageFilePath").trim();
		sInstallationPackageFilePath = sInstallationPackageFilePath == null?"":sInstallationPackageFilePath.trim();
		
		sFileUploadPath = System.getProperty("CatalogueFolderPath").trim();
		sFileUploadPath = sFileUploadPath + "//"+sInstallationPackageFilePath ;
		
		if(p_sFileName != null) {
			sFileType = tokeniseImgType(p_sFileName);
			sFileType = sFileType == null?"":sFileType.trim();
			sOriginalFileName = tokeniseFileName(p_sFileName);
			sOriginalFileName = sOriginalFileName == null?"":sOriginalFileName.trim();
		}
		sFileName = p_sFolderName;
		sFileName = sFileName == null?"":sFileName.trim();
		sSrcFilePath = sFileUploadPath+"\\"+sFileName;
		
		//Used to create the folder to hold the swf package of the owner, if it is not exists.
		File fSwfPath = new File(sSrcFilePath);
		if(!fSwfPath .exists())
			fSwfPath .mkdirs();

		if(!oCatalogueFormFile.equals("")) {  
			//Create the file to upload.
			if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_SERVICE.equalsIgnoreCase(p_sUploadType)
					|| UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_ADMIN.equalsIgnoreCase(p_sUploadType)
					|| UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(p_sUploadType)) {
				fDestFilePath = new File(sSrcFilePath,sFileName+"_"+p_sVersion+"."+sFileType);
			}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(p_sUploadType)) {
				fDestFilePath = new File(sSrcFilePath,sOriginalFileName+"."+sFileType);
			}
			else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(p_sUploadType)) {
				File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
				String sIphondeDesktopImageName = System.getProperty(UPLOADTYPE_IPHONEDESKTOPIMAGE);
				if(!fNewFolder .exists())
					fNewFolder .mkdirs();
				fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sIphondeDesktopImageName+"."+sFileType);
			}
			else if(UPLOADTYPE_IPHONEPERSONAL.equalsIgnoreCase(p_sUploadType)) {
				File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
				String sIphonePersonalShopper = System.getProperty(UPLOADTYPE_IPHONEPERSONAL);
				if(!fNewFolder .exists())
					fNewFolder .mkdirs();
				fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sIphonePersonalShopper+"."+sFileType);
			}
			else if(UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(p_sUploadType)) {
				File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
				String sWelcomePageFileName = System.getProperty("welcomePageFileName");
				if(!fNewFolder .exists())
					fNewFolder .mkdirs();
				fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sWelcomePageFileName);
			}else if(UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(p_sUploadType)) {
				File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
				String sEcatalogueFileName = System.getProperty("skybuyCatgFileName");
				if(!fNewFolder .exists())
					fNewFolder .mkdirs();
				fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sEcatalogueFileName);
			}
			
			sStaticPath = System.getProperty("StaticPath"); 
			sStaticPath = sStaticPath == null?"":sStaticPath.trim();
			
			if(fDestFilePath.getPath().indexOf(sStaticPath)>=0){
				sAbsolutePath = fDestFilePath.getPath();
				sAbsolutePath = sAbsolutePath.replace(sStaticPath, "");
			}
			//If file does not exists create file
			if(fDestFilePath != null) {
				if(fDestFilePath.exists())
					fDestFilePath.delete();
				if(!fDestFilePath.exists()){
					FileOutputStream oFileOutputStream = new FileOutputStream(fDestFilePath);
					oFileOutputStream.write(oCatalogueFormFile.getFileData());					
					oFileOutputStream.flush();
					oFileOutputStream.close();
					bIsInstallationPackageUploaded = true;
				}  
			}
			alImageInfo.add(0,bIsInstallationPackageUploaded);
			alImageInfo.add(1,sAbsolutePath);
		}
		logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exit");
	}catch(Exception e){
		logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exception "+e.getMessage());
		e.printStackTrace();		
	}
	return alImageInfo;
}*/
	
	
	public static List uploadSkyBuyInstallationPackage(FormFile oCatalogueFormFile, String p_sFileName, String p_sUploadType, String p_sVersion, String p_sFolderName) {
		logger.info("FileUpload:uploadSkyBuyInstallationPackage::Entry");
		
		Boolean bIsInstallationPackageUploaded = false;
		String sFileType = "";
		String sFileName = "";
		String sOriginalFileName = "";
		File fDestFilePath = null;
		List alImageInfo = new ArrayList();
		String sFileUploadPath = null;
		String sInstallationPackageFilePath = null;
		String sSrcFilePath = "";
		String sStaticPath = null;
		String sAbsolutePath = null;
		try{
			sInstallationPackageFilePath = System.getProperty("InstallationPackageFilePath").trim();
			sInstallationPackageFilePath = sInstallationPackageFilePath == null?"":sInstallationPackageFilePath.trim();
			
			sFileUploadPath = System.getProperty("CatalogueFolderPath").trim();
			sFileUploadPath = sFileUploadPath + "//"+sInstallationPackageFilePath ;
			
			if(p_sFileName != null) {
				sFileType = tokeniseImgType(p_sFileName);
				sFileType = sFileType == null?"":sFileType.trim();
				sOriginalFileName = tokeniseFileName(p_sFileName);
				sOriginalFileName = sOriginalFileName == null?"":sOriginalFileName.trim();
			}
			sFileName = p_sFolderName;
			sFileName = sFileName == null?"":sFileName.trim();
			sSrcFilePath = sFileUploadPath+"\\"+sFileName;
			
			//Used to create the folder to hold the swf package of the owner, if it is not exists.
			File fSwfPath = new File(sSrcFilePath);
			if(!fSwfPath .exists())
				fSwfPath .mkdirs();

			if(!oCatalogueFormFile.equals("")) {  
				//Create the file to upload.
				if(UPLOADTYPE_SCHEMA.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_SERVICE.equalsIgnoreCase(p_sUploadType)
						|| UPLOADTYPE_VIRTUALDESKTOP.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_ADMIN.equalsIgnoreCase(p_sUploadType)
						|| UPLOADTYPE_SHOPPINGCART.equalsIgnoreCase(p_sUploadType) || UPLOADTYPE_AUTOUDPATE.equalsIgnoreCase(p_sUploadType)) {
					fDestFilePath = new File(sSrcFilePath,sFileName+"_"+p_sVersion+"."+sFileType);
				}else if(UPLOADTYPE_CLIENTCERT.equalsIgnoreCase(p_sUploadType)) {
					fDestFilePath = new File(sSrcFilePath,sOriginalFileName+"."+sFileType);
				}
				else if(UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sIphondeDesktopImageName = System.getProperty(UPLOADTYPE_IPHONEDESKTOPIMAGE);
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sIphondeDesktopImageName+"."+sFileType);
				}
				else if(UPLOADTYPE_IPHONEPERSONAL.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sIphonePersonalShopper = System.getProperty(UPLOADTYPE_IPHONEPERSONAL);
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sIphonePersonalShopper+"."+sFileType);
				}
				else if(UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sWelcomePageFileName = System.getProperty("welcomePageFileName");
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sWelcomePageFileName);
				}else if(UPLOADTYPE_IPHONEWELCOMEPAGE.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sWelcomePageFileName = System.getProperty(UPLOADTYPE_IPHONEWELCOMEPAGE);
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sWelcomePageFileName+"."+sFileType);
				}
				
				else if(UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sEcatalogueFileName = System.getProperty("skybuyCatgFileName");
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sEcatalogueFileName);
				}
				else if(UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(p_sUploadType)) {
					File fNewFolder = new File(sSrcFilePath+"/"+p_sVersion);
					String sEcatalogueFileName = System.getProperty("personalShopperFileName");
					if(!fNewFolder .exists())
						fNewFolder .mkdirs();
					fDestFilePath = new File(sSrcFilePath+"/"+p_sVersion,sEcatalogueFileName);
				}
				
				sStaticPath = System.getProperty("StaticPath"); 
				sStaticPath = sStaticPath == null?"":sStaticPath.trim();
				
				if(fDestFilePath.getPath().indexOf(sStaticPath)>=0){
					sAbsolutePath = fDestFilePath.getPath();
					sAbsolutePath = sAbsolutePath.replace(sStaticPath, "");
					sAbsolutePath = sAbsolutePath.replaceAll("\\\\", "/");
				}
				//If file does not exists create file
				if(fDestFilePath != null) {
					if(fDestFilePath.exists())
						fDestFilePath.delete();
					if(!fDestFilePath.exists()){
						FileOutputStream oFileOutputStream = new FileOutputStream(fDestFilePath);
						oFileOutputStream.write(oCatalogueFormFile.getFileData());					
						oFileOutputStream.flush();
						oFileOutputStream.close();
						bIsInstallationPackageUploaded = true;
					}  
				}
				alImageInfo.add(0,bIsInstallationPackageUploaded);
				alImageInfo.add(1,sAbsolutePath);
			}
			logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exit");
		}catch(Exception e){
			logger.info("FileUpload:uploadSkyBuyInstallationPackage::Exception "+e.getMessage());
			e.printStackTrace();		
		}
		return alImageInfo;
	}

	
	

}
