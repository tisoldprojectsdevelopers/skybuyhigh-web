package com.sbh.dao;

import static com.sbh.contants.SkyBuyContants.CREDIT;
import static com.sbh.contants.SkyBuyContants.MODE_OF_REPAYMENT_CANCEL;
import static com.sbh.contants.SkyBuyContants.PAYMENT_APPROVED;
import static com.sbh.contants.SkyBuyContants.POST_AUTHENTICATION;
import static com.sbh.contants.SkyBuyContants.PRE_AUTHENTICATION;
import static com.sbh.contants.SkyBuyContants.SALE;
import static com.sbh.contants.SkyBuyContants.TAX_EXCEMPTION_YES;
import static com.sbh.contants.SkyBuyContants.TRANSACTION_FAILED;
import static com.sbh.contants.SkyBuyContants.TRANSACTION_SUCCESS;
import static com.sbh.dao.OrderDAO.decryptData;
import static com.sbh.dao.OrderDAO.decryptInputData;
import static com.sbh.dao.OrderDAO.encryptInputData;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.StringTokenizer;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.payment.txn.JLinkPointCreditCardTxn;
import com.sbh.util.DBConnection;
import com.sbh.util.Utils;
import com.sbh.vo.CCStoreInfoVO;
import com.sbh.vo.CustomerOrderInfo;
import com.sbh.vo.OrderItemDetails;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.PaymentResponse;


public class PaymentDAO {

	private static Logger logger = LogManager.getLogger(PaymentDAO.class);
	
	public static PaymentResponse preAuthorizePayment(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sIpAddress) throws Exception{
		logger.info("PaymentDAO::preAuthorizePayment::ENTER");
		
		double dQty=0.0;
		double dSbhPrice = 0.0;
		String sCardExpMonth = null;
		String sCardExpYear = null;
		String sStoreNumber = null;
		String sTxnMsg="";
		String sTxnStatus="";
		CustomerOrderInfo oCustomerInfo=new CustomerOrderInfo();
		JLinkPointCreditCardTxn oJLinkPointCreditCardTxn=new JLinkPointCreditCardTxn();
		PaymentResponse oPaymentResponse = null;
		
		logger.info("PaymentDAO::preAuthorizePayment::Order Confirmation No"+p_oOrderItemDetailsVO.getCustTransId());
		logger.info("PaymentDAO::preAuthorizePayment::Price"+p_oOrderItemDetailsVO.getSbhPrice());
		logger.info("PaymentDAO::preAuthorizePayment::Product Code"+p_oOrderItemDetailsVO.getProdCode());
		logger.info("PaymentDAO::preAuthorizePayment::Order Item Id"+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("PaymentDAO::preAuthorizePayment::Quantity"+p_oOrderItemDetailsVO.getQty());
		logger.info("PaymentDAO::preAuthorizePayment::Special Instructions"+p_oOrderItemDetailsVO.getSpecialInst());
		logger.info("PaymentDAO::preAuthorizePayment::Customer shipping address Zip code"+p_oOrderItemDetailsVO.getCustShipZip());
		logger.info("PaymentDAO::preAuthorizePayment::Customer billing address Zip code"+p_oOrderItemDetailsVO.getCustZip());
		logger.info("PaymentDAO::preAuthorizePayment::Login Id:"+p_sLoginId);
		logger.info("PaymentDAO::preAuthorizePayment::Ip Address"+p_sIpAddress);
		
		try {
			CCStoreInfoVO oCCStoreInfoVO = getCCStoreInfo();
			if(oCCStoreInfoVO != null){
				sStoreNumber = oCCStoreInfoVO.getStoreNumber();
			}
			sStoreNumber = sStoreNumber == null?"":sStoreNumber.trim();
			
			oCustomerInfo.setOrderType(PRE_AUTHENTICATION);
//			oCustomerInfo.setPaymentTxnRefId(p_oOrderItemDetailsVO.getOrderItemId());
			oCustomerInfo.setOrderConfirmationNo(p_oOrderItemDetailsVO.getCustTransId());
			oCustomerInfo.setStoreNumber(sStoreNumber);
			oCustomerInfo.setCardNumber(p_oOrderItemDetailsVO.getCCNo());
			if(p_oOrderItemDetailsVO.getCvv() != null && p_oOrderItemDetailsVO.getCvv().trim().length() > 0)
				oCustomerInfo.setCvv(decryptInputData(p_oOrderItemDetailsVO.getCvv(), "CLIENT", p_oOrderItemDetailsVO.getKeyRefId()));
			else 
				oCustomerInfo.setCvv(p_oOrderItemDetailsVO.getCvv());
			
			if(p_oOrderItemDetailsVO.getExpDt()!=null){
				String sExpDt = p_oOrderItemDetailsVO.getExpDt();
				StringTokenizer oStringTokenizer = new StringTokenizer(sExpDt,"/");
				if(oStringTokenizer.hasMoreTokens())
					sCardExpMonth = oStringTokenizer.nextToken();
				if(oStringTokenizer.hasMoreTokens()){
					sCardExpYear = oStringTokenizer.nextToken();
					sCardExpYear = sCardExpYear.substring(sCardExpYear.length()-2, sCardExpYear.length());
				}	
			}				

			oCustomerInfo.setCardExpMonth(sCardExpMonth);
			oCustomerInfo.setCardExpYear(sCardExpYear);

			if(p_oOrderItemDetailsVO.getQty()!=null)
				dQty= Double.parseDouble(p_oOrderItemDetailsVO.getQty());

			if(p_oOrderItemDetailsVO.getSbhPrice()!=null)
				dSbhPrice= Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());

			double dAmount =dQty*dSbhPrice;
			oCustomerInfo.setChargeTotal((dAmount)+"");

			String sAddress = p_oOrderItemDetailsVO.getCustAddress1()+p_oOrderItemDetailsVO.getCustAddress2();

			oCustomerInfo.setAddrNum(getAddrNumber(sAddress)+"");
			oCustomerInfo.setZip(p_oOrderItemDetailsVO.getCustZip());
			oCustomerInfo.setBillingAddr1(p_oOrderItemDetailsVO.getCustAddress1());
			if(p_oOrderItemDetailsVO.getCustAddress2()!=null)
				oCustomerInfo.setBillingAddr2(p_oOrderItemDetailsVO.getCustAddress2());
			else
				oCustomerInfo.setBillingAddr2("");

			oCustomerInfo.setBillingCity(p_oOrderItemDetailsVO.getCustCity());
			oCustomerInfo.setBillingState(p_oOrderItemDetailsVO.getCustState());
			oCustomerInfo.setBillingCountry(p_oOrderItemDetailsVO.getCustCountry());
			oCustomerInfo.setBillingCustName(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			oCustomerInfo.setBillingEmail(p_oOrderItemDetailsVO.getCustEmail());
			oCustomerInfo.setBillingPhone(p_oOrderItemDetailsVO.getCustPhone());
			
			//Shipping address details are set in the Customer Info bean. 
			oCustomerInfo.setShippingAddr1(p_oOrderItemDetailsVO.getCustShipAddress1());
			if(p_oOrderItemDetailsVO.getCustShipAddress2()!=null)
				oCustomerInfo.setShippingAddr2(p_oOrderItemDetailsVO.getCustShipAddress2());
			else
				oCustomerInfo.setShippingAddr2("");

			oCustomerInfo.setShippingCity(p_oOrderItemDetailsVO.getCustShipCity());
			oCustomerInfo.setShippingState(p_oOrderItemDetailsVO.getCustShipState());
			oCustomerInfo.setShippingCountry(p_oOrderItemDetailsVO.getCustShipCountry());
			oCustomerInfo.setShippingCustName(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName());
			oCustomerInfo.setShippingZip(p_oOrderItemDetailsVO.getCustShipZip());
			oCustomerInfo.setCustSpecialInstruction(p_oOrderItemDetailsVO.getSpecialInst());
			oCustomerInfo.setTaxexempt(TAX_EXCEMPTION_YES);
			oCustomerInfo.setIpAddress(p_sIpAddress);

			OrderItemDetails oOrderItemInfo=new OrderItemDetails();
			String sShortDesc = p_oOrderItemDetailsVO.getShortDesc();
			sShortDesc = sShortDesc == null?"":sShortDesc.trim();
			String sProdCode = p_oOrderItemDetailsVO.getProdCode();
			sProdCode = sProdCode == null?"":sProdCode.trim();
			oOrderItemInfo.setDescription("Prod Code:"+sProdCode);
			oOrderItemInfo.setId(p_oOrderItemDetailsVO.getOrderItemId()); 
			//oOrderItemInfo.setPrice((dQty*dSbhPrice)+"");
			oOrderItemInfo.setPrice(dSbhPrice+"");
			oOrderItemInfo.setQuantity(p_oOrderItemDetailsVO.getQty());
			oCustomerInfo.setOrderItemDetails(oOrderItemInfo);
			
			// add cust pay txns with txn type -- PREAUTH
			long lPayTxnId = addCustPayTxnDetails(p_oOrderItemDetailsVO,dAmount,PRE_AUTHENTICATION,p_sLoginId);
				if(lPayTxnId > 0) {
				oPaymentResponse=oJLinkPointCreditCardTxn.process(oCustomerInfo);       	
				p_oOrderItemDetailsVO.setPaymentTxnRefId(oPaymentResponse.getR_OrderNum());
				//update cust pay txns
				if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){
					sTxnMsg = oPaymentResponse.getR_Approved();
					sTxnStatus = TRANSACTION_SUCCESS;
				}	
				else{
					sTxnMsg = oPaymentResponse.getR_Error_Msg();
					sTxnStatus = TRANSACTION_FAILED;
				}	
	
				updateCustPayTxnDetails(lPayTxnId,sTxnStatus,sTxnMsg,p_oOrderItemDetailsVO.getPaymentTxnRefId(),oPaymentResponse.getR_Error_Code(),oPaymentResponse.getInBoundMsg(),oPaymentResponse.getOutBoundMsg(),PRE_AUTHENTICATION,p_oOrderItemDetailsVO.getReturnId(),"",p_oOrderItemDetailsVO.getOrderItemId(),p_sLoginId);
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}else {
				oPaymentResponse = new PaymentResponse();
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}

			logger.info("PaymentDAO::preAuthorizePayment:EXIT");

		} catch (Exception e) {
			logger.error("PaymentDAO::preAuthorizePayment:EXCEPTION "+e.getMessage());
			throw e;
		}
		return oPaymentResponse;
	}

	public static PaymentResponse postAuthorizePayment(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sIpAddress) throws Exception{
		logger.info("PaymentDAO::postAuthorizePayment::Enter");
		
		double dQty=0.0;
		double dSbhPrice = 0.0;
		String sCardExpMonth = null;
		String sCardExpYear = null;
		String sTxnMsg="";
		String sTxnStatus="";
		CustomerOrderInfo oCustomerInfo=new CustomerOrderInfo();
		JLinkPointCreditCardTxn oJLinkPointCreditCardTxn=new JLinkPointCreditCardTxn();
		PaymentResponse oPaymentResponse = null;
		
		logger.info("PaymentDAO::postAuthorizePayment::Order Confirmation No"+p_oOrderItemDetailsVO.getCustTransId());
		logger.info("PaymentDAO::postAuthorizePayment::Price"+p_oOrderItemDetailsVO.getSbhPrice());
		logger.info("PaymentDAO::postAuthorizePayment::Product Code"+p_oOrderItemDetailsVO.getProdCode());
		logger.info("PaymentDAO::postAuthorizePayment::Order Item Id"+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("PaymentDAO::postAuthorizePayment::Quantity"+p_oOrderItemDetailsVO.getQty());
		logger.info("PaymentDAO::postAuthorizePayment::Special Instructions"+p_oOrderItemDetailsVO.getSpecialInst());
		logger.info("PaymentDAO::postAuthorizePayment::Customer shipping address Zip code"+p_oOrderItemDetailsVO.getCustShipZip());
		logger.info("PaymentDAO::postAuthorizePayment::Customer billing address Zip code"+p_oOrderItemDetailsVO.getCustZip());
		logger.info("PaymentDAO::postAuthorizePayment::Login Id:"+p_sLoginId);
		logger.info("PaymentDAO::postAuthorizePayment::Ip Address"+p_sIpAddress);
		
		String sStoreNumber = null;
		try {
			CCStoreInfoVO oCCStoreInfoVO = getCCStoreInfo();
			if(oCCStoreInfoVO != null){
				sStoreNumber = oCCStoreInfoVO.getStoreNumber();
				/*ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			String sStoreNumber = oBundle.getString("StoreNumber");*/
			}
			sStoreNumber = sStoreNumber == null?"":sStoreNumber.trim();
			
			oCustomerInfo.setOrderType(POST_AUTHENTICATION);
			oCustomerInfo.setPaymentTxnRefId(p_oOrderItemDetailsVO.getPaymentTxnRefId());
			oCustomerInfo.setStoreNumber(sStoreNumber);
			//oCustomerInfo.setCardNumber("4111111111111111");
			oCustomerInfo.setCardNumber(p_oOrderItemDetailsVO.getCCNo());

			if(p_oOrderItemDetailsVO.getExpDt()!=null){
				String sExpDt = p_oOrderItemDetailsVO.getExpDt();
				StringTokenizer oStringTokenizer = new StringTokenizer(sExpDt,"/");
				if(oStringTokenizer.hasMoreTokens())
					sCardExpMonth = oStringTokenizer.nextToken();
				if(oStringTokenizer.hasMoreTokens())
					sCardExpYear = oStringTokenizer.nextToken();
				sCardExpYear = sCardExpYear.substring(sCardExpYear.length()-2, sCardExpYear.length());
			}				

			oCustomerInfo.setCardExpMonth(sCardExpMonth);
			oCustomerInfo.setCardExpYear(sCardExpYear);

			if(p_oOrderItemDetailsVO.getQty()!=null)
				dQty= Double.parseDouble(p_oOrderItemDetailsVO.getQty());

			if(p_oOrderItemDetailsVO.getSbhPrice()!=null)
				dSbhPrice= Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());

			Double dAmount = dQty*dSbhPrice;
			oCustomerInfo.setChargeTotal((dAmount)+"");

//			String sAddress = p_oOrderItemDetailsVO.getCustAddress1()+p_oOrderItemDetailsVO.getCustAddress2();
			String sAddress = oCustomerInfo.getBillingAddr1()+oCustomerInfo.getBillingAddr2();

			oCustomerInfo.setAddrNum(getAddrNumber(sAddress)+"");
			oCustomerInfo.setZip(p_oOrderItemDetailsVO.getCustZip());
			oCustomerInfo.setBillingAddr1(p_oOrderItemDetailsVO.getCustAddress1());
			if(p_oOrderItemDetailsVO.getCustAddress2()!=null)
				oCustomerInfo.setBillingAddr2(p_oOrderItemDetailsVO.getCustAddress2());
			else
				oCustomerInfo.setBillingAddr2("");

			oCustomerInfo.setBillingCity(p_oOrderItemDetailsVO.getCustCity());
			oCustomerInfo.setBillingState(p_oOrderItemDetailsVO.getCustState());
			oCustomerInfo.setBillingCountry(p_oOrderItemDetailsVO.getCustCountry());
			oCustomerInfo.setBillingCustName(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			oCustomerInfo.setBillingEmail(p_oOrderItemDetailsVO.getCustEmail());
			oCustomerInfo.setBillingPhone(p_oOrderItemDetailsVO.getCustPhone());
			oCustomerInfo.setShippingAddr1(p_oOrderItemDetailsVO.getCustShipAddress1());
			if(p_oOrderItemDetailsVO.getCustShipAddress2()!=null)
				oCustomerInfo.setShippingAddr2(p_oOrderItemDetailsVO.getCustShipAddress2());
			else
				oCustomerInfo.setShippingAddr2("");

			oCustomerInfo.setShippingCity(p_oOrderItemDetailsVO.getCustShipCity());
			oCustomerInfo.setShippingState(p_oOrderItemDetailsVO.getCustShipState());
			oCustomerInfo.setShippingCountry(p_oOrderItemDetailsVO.getCustShipCountry());
			oCustomerInfo.setShippingCustName(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName());
			oCustomerInfo.setShippingZip(p_oOrderItemDetailsVO.getCustShipZip());
			oCustomerInfo.setCustSpecialInstruction(p_oOrderItemDetailsVO.getSpecialInst());
			oCustomerInfo.setTaxexempt(TAX_EXCEMPTION_YES);
			oCustomerInfo.setIpAddress(p_sIpAddress);

			OrderItemDetails oOrderItemInfo=new OrderItemDetails();
			String sShortDesc = p_oOrderItemDetailsVO.getShortDesc();
			sShortDesc = sShortDesc == null?"":sShortDesc.trim();
			String sProdCode = p_oOrderItemDetailsVO.getProdCode();
			sProdCode = sProdCode == null?"":sProdCode.trim();
			oOrderItemInfo.setDescription("Prod Code:"+sProdCode);
			oOrderItemInfo.setId(p_oOrderItemDetailsVO.getOrderItemId()); 
			//oOrderItemInfo.setPrice((dQty*dSbhPrice)+"");
			oOrderItemInfo.setPrice(dSbhPrice+"");
			oOrderItemInfo.setQuantity(p_oOrderItemDetailsVO.getQty());
			oCustomerInfo.setOrderItemDetails(oOrderItemInfo);

			// add cust pay txns table with txn type - POSTAUTH
			long lPayTxnId = addCustPayTxnDetails(p_oOrderItemDetailsVO,dAmount,POST_AUTHENTICATION,p_sLoginId);

			if(lPayTxnId > 0) {
				oPaymentResponse=oJLinkPointCreditCardTxn.process(oCustomerInfo);
	
				//update cust pay txns
				if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){
					sTxnMsg = oPaymentResponse.getR_Approved();
					sTxnStatus = TRANSACTION_SUCCESS;
				}	
				else{
					sTxnMsg = oPaymentResponse.getR_Error_Msg();
					sTxnStatus = TRANSACTION_FAILED;
				}	
	
				updateCustPayTxnDetails(lPayTxnId,sTxnStatus,sTxnMsg,p_oOrderItemDetailsVO.getPaymentTxnRefId(),oPaymentResponse.getR_Error_Code(),oPaymentResponse.getInBoundMsg(),oPaymentResponse.getOutBoundMsg(),POST_AUTHENTICATION,p_oOrderItemDetailsVO.getReturnId(),"",p_oOrderItemDetailsVO.getOrderItemId(),p_sLoginId);
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}else {
				oPaymentResponse = new PaymentResponse();
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}
			logger.info("PaymentDAO::postAuthorizePayment:EXIT");

		} catch (Exception e) {
			logger.error("PaymentDAO::postAuthorizePayment:EXCEPTION "+e.getMessage());
			throw e;
		}
		return oPaymentResponse;
	}
	public static PaymentResponse salePayment(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sLoginId,String p_sIpAddress) throws Exception{
		logger.info("PaymentDAO::salePayment::Enter");
		
		double dQty=0.0;
		double dSbhPrice = 0.0;
		String sCardExpMonth = null;
		String sCardExpYear = null;
		String sStoreNumber = null;
		String sTxnMsg="";
		String sTxnStatus="";
		CustomerOrderInfo oCustomerInfo=new CustomerOrderInfo();
		JLinkPointCreditCardTxn oJLinkPointCreditCardTxn=new JLinkPointCreditCardTxn();
		PaymentResponse oPaymentResponse = null;
		
		logger.info("PaymentDAO::salePayment::Order Confirmation No"+p_oOrderItemDetailsVO.getCustTransId());
		logger.info("PaymentDAO::salePayment::Price"+p_oOrderItemDetailsVO.getSbhPrice());
		logger.info("PaymentDAO::salePayment::Product Code"+p_oOrderItemDetailsVO.getProdCode());
		logger.info("PaymentDAO::salePayment::Order Item Id"+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("PaymentDAO::salePayment::Quantity"+p_oOrderItemDetailsVO.getQty());
		logger.info("PaymentDAO::salePayment::Special Instructions"+p_oOrderItemDetailsVO.getSpecialInst());
		logger.info("PaymentDAO::salePayment::Customer shipping address Zip code"+p_oOrderItemDetailsVO.getCustShipZip());
		logger.info("PaymentDAO::salePayment::Customer billing address Zip code"+p_oOrderItemDetailsVO.getCustZip());
		logger.info("PaymentDAO::salePayment::Login Id:"+p_sLoginId);
		logger.info("PaymentDAO::salePayment::Ip Address"+p_sIpAddress);
		
		try {
			CCStoreInfoVO oCCStoreInfoVO = getCCStoreInfo();
			if(oCCStoreInfoVO != null){
				sStoreNumber = oCCStoreInfoVO.getStoreNumber();
				/*ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			String sStoreNumber = oBundle.getString("StoreNumber");*/
			}
			sStoreNumber = sStoreNumber == null?"":sStoreNumber.trim();
			
			
			oCustomerInfo.setOrderType(SALE);
//			oCustomerInfo.setPaymentTxnRefId(p_oOrderItemDetailsVO.getOrderItemId());
			oCustomerInfo.setOrderConfirmationNo(p_oOrderItemDetailsVO.getCustTransId());
			oCustomerInfo.setStoreNumber(sStoreNumber);
			//oCustomerInfo.setCardNumber("4111111111111111");
			oCustomerInfo.setCardNumber(p_oOrderItemDetailsVO.getCCNo());
			if(p_oOrderItemDetailsVO.getCvv() != null && p_oOrderItemDetailsVO.getCvv().trim().length() > 0)
				oCustomerInfo.setCvv(decryptInputData(p_oOrderItemDetailsVO.getCvv(), "CLIENT", p_oOrderItemDetailsVO.getKeyRefId()));
			else 
				oCustomerInfo.setCvv(p_oOrderItemDetailsVO.getCvv());
			if(p_oOrderItemDetailsVO.getExpDt()!=null){
				String sExpDt = p_oOrderItemDetailsVO.getExpDt();
				StringTokenizer oStringTokenizer = new StringTokenizer(sExpDt,"/");
				if(oStringTokenizer.hasMoreTokens())
					sCardExpMonth = oStringTokenizer.nextToken();
				if(oStringTokenizer.hasMoreTokens()){
					sCardExpYear = oStringTokenizer.nextToken();
					sCardExpYear = sCardExpYear.substring(sCardExpYear.length()-2, sCardExpYear.length());
				}	
			}				

			oCustomerInfo.setCardExpMonth(sCardExpMonth);
			oCustomerInfo.setCardExpYear(sCardExpYear);

			if(p_oOrderItemDetailsVO.getQty()!=null)
				dQty= Double.parseDouble(p_oOrderItemDetailsVO.getQty());

			if(p_oOrderItemDetailsVO.getSbhPrice()!=null)
				dSbhPrice= Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());

			double dAmount =dQty*dSbhPrice;
			oCustomerInfo.setChargeTotal((dAmount)+"");

//			String sAddress = p_oOrderItemDetailsVO.getCustAddress1()+p_oOrderItemDetailsVO.getCustAddress2();
			String sAddress = oCustomerInfo.getBillingAddr1()+oCustomerInfo.getBillingAddr2();

			oCustomerInfo.setAddrNum(getAddrNumber(sAddress)+"");
			oCustomerInfo.setZip(p_oOrderItemDetailsVO.getCustZip());
			oCustomerInfo.setBillingAddr1(p_oOrderItemDetailsVO.getCustAddress1());
			if(p_oOrderItemDetailsVO.getCustAddress2()!=null)
				oCustomerInfo.setBillingAddr2(p_oOrderItemDetailsVO.getCustAddress2());
			else
				oCustomerInfo.setBillingAddr2("");

			oCustomerInfo.setBillingCity(p_oOrderItemDetailsVO.getCustCity());
			oCustomerInfo.setBillingState(p_oOrderItemDetailsVO.getCustState());
			oCustomerInfo.setBillingCountry(p_oOrderItemDetailsVO.getCustCountry());
			oCustomerInfo.setBillingCustName(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			oCustomerInfo.setBillingEmail(p_oOrderItemDetailsVO.getCustEmail());
			oCustomerInfo.setBillingPhone(p_oOrderItemDetailsVO.getCustPhone());
			oCustomerInfo.setShippingAddr1(p_oOrderItemDetailsVO.getCustShipAddress1());
			if(p_oOrderItemDetailsVO.getCustShipAddress2()!=null)
				oCustomerInfo.setShippingAddr2(p_oOrderItemDetailsVO.getCustShipAddress2());
			else
				oCustomerInfo.setShippingAddr2("");

			oCustomerInfo.setShippingCity(p_oOrderItemDetailsVO.getCustShipCity());
			oCustomerInfo.setShippingState(p_oOrderItemDetailsVO.getCustShipState());
			oCustomerInfo.setShippingCountry(p_oOrderItemDetailsVO.getCustShipCountry());
			oCustomerInfo.setShippingCustName(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName());
			oCustomerInfo.setShippingZip(p_oOrderItemDetailsVO.getCustShipZip());
			oCustomerInfo.setCustSpecialInstruction(p_oOrderItemDetailsVO.getSpecialInst());
			oCustomerInfo.setTaxexempt(TAX_EXCEMPTION_YES);
			oCustomerInfo.setIpAddress(p_sIpAddress);

			OrderItemDetails oOrderItemInfo=new OrderItemDetails();
			String sShortDesc = p_oOrderItemDetailsVO.getShortDesc();
			sShortDesc = sShortDesc == null?"":sShortDesc.trim();
			String sProdCode = p_oOrderItemDetailsVO.getProdCode();
			sProdCode = sProdCode == null?"":sProdCode.trim();
			oOrderItemInfo.setDescription("Prod Code:"+sProdCode);
			oOrderItemInfo.setId(p_oOrderItemDetailsVO.getOrderItemId()); 
			//oOrderItemInfo.setPrice((dQty*dSbhPrice)+"");
			oOrderItemInfo.setPrice(dSbhPrice+"");
			oOrderItemInfo.setQuantity(p_oOrderItemDetailsVO.getQty());
			oCustomerInfo.setOrderItemDetails(oOrderItemInfo);
			
			// add cust pay txns with txn type -- SALE
			long lPayTxnId = addCustPayTxnDetails(p_oOrderItemDetailsVO,dAmount,SALE,p_sLoginId);
			if(lPayTxnId > 0) {
				oPaymentResponse=oJLinkPointCreditCardTxn.process(oCustomerInfo);       	
				p_oOrderItemDetailsVO.setPaymentTxnRefId(oPaymentResponse.getR_OrderNum());
				//update cust pay txns
				if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){
					sTxnMsg = oPaymentResponse.getR_Approved();
					sTxnStatus = TRANSACTION_SUCCESS;
				}	
				else{
					sTxnMsg = oPaymentResponse.getR_Error_Msg();
					sTxnStatus = TRANSACTION_FAILED;
				}	
	
				updateCustPayTxnDetails(lPayTxnId,sTxnStatus,sTxnMsg,p_oOrderItemDetailsVO.getPaymentTxnRefId(),oPaymentResponse.getR_Error_Code(),oPaymentResponse.getInBoundMsg(),oPaymentResponse.getOutBoundMsg(),SALE,p_oOrderItemDetailsVO.getReturnId(),"",p_oOrderItemDetailsVO.getOrderItemId(),p_sLoginId);
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}else {
				oPaymentResponse = new PaymentResponse();
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}

			logger.info("PaymentDAO::salePayment:EXIT");

		} catch (Exception e) {
			logger.error("PaymentDAO::salePayment:EXCEPTION "+e.getMessage());
			throw e;
		}
		return oPaymentResponse;
	}
	
	public static PaymentResponse creditAuthorizationToRepay(OrderItemDetailsVO p_oOrderItemDetailsVO,String p_sMode,String p_sLoginId,String p_sIpAddress) throws Exception {
		logger.info("PaymentDAO::creditAuthorizationToRepay::ENTER");
		
		long lPayTxnId = 0;
		double dAmount = 0.0;
		double dSbhPrice = 0.0;
		double dQty = 0.0;
		String sCardExpMonth = null;
		String sCardExpYear = null;
		String sStoreNumber = null;
		String sTxnMsg="";
		String sTxnStatus="";
		CustomerOrderInfo oCustomerInfo=new CustomerOrderInfo();
		JLinkPointCreditCardTxn oJLinkPointCreditCardTxn=new JLinkPointCreditCardTxn();
		PaymentResponse oPaymentResponse = null;
		
		logger.info("PaymentDAO::creditAuthorizationToRepay::Order Confirmation No"+p_oOrderItemDetailsVO.getCustTransId());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Price"+p_oOrderItemDetailsVO.getSbhPrice());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Product Code"+p_oOrderItemDetailsVO.getProdCode());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Order Item Id"+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Quantity"+p_oOrderItemDetailsVO.getQty());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Special Instructions"+p_oOrderItemDetailsVO.getSpecialInst());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Customer shipping address Zip code"+p_oOrderItemDetailsVO.getCustShipZip());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Customer billing address Zip code"+p_oOrderItemDetailsVO.getCustZip());
		logger.info("PaymentDAO::creditAuthorizationToRepay::Login Id:"+p_sLoginId);
		logger.info("PaymentDAO::creditAuthorizationToRepay::Ip Address"+p_sIpAddress);
		logger.info("PaymentDAO::creditAuthorizationToRepay::Mode of Repayment"+p_sMode);
		
		try {
			CCStoreInfoVO oCCStoreInfoVO = getCCStoreInfo();
			if(oCCStoreInfoVO != null){
				sStoreNumber = oCCStoreInfoVO.getStoreNumber();
			}
			sStoreNumber = sStoreNumber == null?"":sStoreNumber.trim();
			
			oCustomerInfo.setOrderType(CREDIT);
			oCustomerInfo.setPaymentTxnRefId(p_oOrderItemDetailsVO.getPaymentTxnRefId());
			oCustomerInfo.setStoreNumber(sStoreNumber);
			oCustomerInfo.setCardNumber(p_oOrderItemDetailsVO.getCCNo());
			if(p_oOrderItemDetailsVO.getExpDt()!=null){
				String sExpDt = p_oOrderItemDetailsVO.getExpDt();
				StringTokenizer oStringTokenizer = new StringTokenizer(sExpDt,"/");
				if(oStringTokenizer.hasMoreTokens())
					sCardExpMonth = oStringTokenizer.nextToken();
				if(oStringTokenizer.hasMoreTokens()){
					sCardExpYear = oStringTokenizer.nextToken();
					sCardExpYear = sCardExpYear.substring(sCardExpYear.length()-2, sCardExpYear.length());
				}	
			}				
	
			oCustomerInfo.setCardExpMonth(sCardExpMonth);
			oCustomerInfo.setCardExpYear(sCardExpYear);
	
			if(MODE_OF_REPAYMENT_CANCEL.equalsIgnoreCase(p_sMode)){
				dQty= Double.parseDouble(p_oOrderItemDetailsVO.getQty());
				dSbhPrice = Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());
			}else{
				dQty= Double.parseDouble(p_oOrderItemDetailsVO.getReturnQuantity());
				dSbhPrice = Double.parseDouble(p_oOrderItemDetailsVO.getChargeBackPrice());
			}
			
	
			dAmount = dQty*dSbhPrice;
			oCustomerInfo.setChargeTotal((dAmount)+"");
			
			String sAddress = oCustomerInfo.getBillingAddr1()+oCustomerInfo.getBillingAddr2();
	
			oCustomerInfo.setAddrNum(getAddrNumber(sAddress)+"");
			oCustomerInfo.setZip(p_oOrderItemDetailsVO.getCustZip());
			oCustomerInfo.setBillingAddr1(p_oOrderItemDetailsVO.getCustAddress1());
			if(p_oOrderItemDetailsVO.getCustAddress2()!=null)
				oCustomerInfo.setBillingAddr2(p_oOrderItemDetailsVO.getCustAddress2());
			else
				oCustomerInfo.setBillingAddr2("");

			oCustomerInfo.setBillingCity(p_oOrderItemDetailsVO.getCustCity());
			oCustomerInfo.setBillingState(p_oOrderItemDetailsVO.getCustState());
			oCustomerInfo.setBillingCountry(p_oOrderItemDetailsVO.getCustCountry());
			oCustomerInfo.setBillingCustName(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName());
			oCustomerInfo.setBillingEmail(p_oOrderItemDetailsVO.getCustEmail());
			oCustomerInfo.setBillingPhone(p_oOrderItemDetailsVO.getCustPhone());
			oCustomerInfo.setShippingAddr1(p_oOrderItemDetailsVO.getCustShipAddress1());
			if(p_oOrderItemDetailsVO.getCustShipAddress2()!=null)
				oCustomerInfo.setShippingAddr2(p_oOrderItemDetailsVO.getCustShipAddress2());
			else
				oCustomerInfo.setShippingAddr2("");
	
			oCustomerInfo.setShippingCity(p_oOrderItemDetailsVO.getCustShipCity());
			oCustomerInfo.setShippingState(p_oOrderItemDetailsVO.getCustShipState());
			oCustomerInfo.setShippingCountry(p_oOrderItemDetailsVO.getCustShipCountry());
			oCustomerInfo.setShippingCustName(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName());
			oCustomerInfo.setShippingZip(p_oOrderItemDetailsVO.getCustShipZip());
			oCustomerInfo.setCustSpecialInstruction(p_oOrderItemDetailsVO.getSpecialInst());
			oCustomerInfo.setTaxexempt(TAX_EXCEMPTION_YES);
			oCustomerInfo.setIpAddress(p_sIpAddress);
			
			// add cust pay txns table with txn type - POSTAUTH
			lPayTxnId = addCustPayTxnDetails(p_oOrderItemDetailsVO,dAmount,CREDIT,p_sLoginId);
			
			if(lPayTxnId > 0) {
				oPaymentResponse=oJLinkPointCreditCardTxn.process(oCustomerInfo);
		
				//update cust pay txns
				if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())) {
					sTxnMsg = oPaymentResponse.getR_Approved();
					sTxnStatus = TRANSACTION_SUCCESS;
				}	
				else {
					sTxnMsg = oPaymentResponse.getR_Error_Msg();
					sTxnStatus = TRANSACTION_FAILED;
				}	
				//Update the status and the transaction history in the Customer Pay transaction Table.s
				updateCustPayTxnDetails(lPayTxnId,sTxnStatus,sTxnMsg,p_oOrderItemDetailsVO.getPaymentTxnRefId(),oPaymentResponse.getR_Error_Code(),oPaymentResponse.getInBoundMsg(),oPaymentResponse.getOutBoundMsg(),CREDIT,p_oOrderItemDetailsVO.getReturnId(),p_sMode,p_oOrderItemDetailsVO.getOrderItemId(),p_sLoginId);
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}else {
				oPaymentResponse = new PaymentResponse();
				oPaymentResponse.setR_TxnRefId(lPayTxnId);
			}
		} catch (Exception e) {
			logger.error("PaymentDAO::creditAuthorizationToRepay:EXCEPTION "+e.getMessage());
			throw e;
		}
		
		logger.info("PaymentDAO::creditAuthorizationToRepay::EXIT");
		return oPaymentResponse;
	}
	
	
	public static long addCustPayTxnDetails(OrderItemDetailsVO p_oOrderItemDetailsVO,Double p_dAmount,String p_sTxnType,String p_sLoginId) throws Exception{	
		logger.info("PaymentDAO::addCustPayTxnDetails::ENTER");
		
		String return_id = "";
		String sQry="{call usp_sbh_add_cust_pay_txn_details(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		String txn_ref_id = "";
		CallableStatement cstmt=null;
		Connection con=null;		
		ResultSet rs=null;
		
		logger.debug("PaymentDAO::addCustPayTxnDetails:SQL QUERY "+sQry);
		logger.info("PaymentDAO::addCustPayTxnDetails::Order Confirmation No"+p_oOrderItemDetailsVO.getCustTransId());
		logger.info("PaymentDAO::addCustPayTxnDetails::Price"+p_oOrderItemDetailsVO.getSbhPrice());
		logger.info("PaymentDAO::addCustPayTxnDetails::Product Code"+p_oOrderItemDetailsVO.getProdCode());
		logger.info("PaymentDAO::addCustPayTxnDetails::Order Item Id"+p_oOrderItemDetailsVO.getOrderItemId());
		logger.info("PaymentDAO::addCustPayTxnDetails::Quantity"+p_oOrderItemDetailsVO.getQty());
		logger.info("PaymentDAO::addCustPayTxnDetails::Special Instructions"+p_oOrderItemDetailsVO.getSpecialInst());
		logger.info("PaymentDAO::addCustPayTxnDetails::Customer shipping address Zip code"+p_oOrderItemDetailsVO.getCustShipZip());
		logger.info("PaymentDAO::addCustPayTxnDetails::Customer billing address Zip code"+p_oOrderItemDetailsVO.getCustZip());
		logger.info("PaymentDAO::addCustPayTxnDetails::Login Id:"+p_sLoginId);
		logger.info("PaymentDAO::addCustPayTxnDetails::Transaction Type"+p_sTxnType);
		logger.info("PaymentDAO::addCustPayTxnDetails::Amount"+p_dAmount);
		
		long lPayTxnId = 0;
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.registerOutParameter(1, Types.BIGINT);
			cstmt.setString(2,p_oOrderItemDetailsVO.getOrderItemId());
			cstmt.setString(3,p_oOrderItemDetailsVO.getCustId());
			
			txn_ref_id = p_oOrderItemDetailsVO.getPaymentTxnRefId();
			txn_ref_id = txn_ref_id == null?"":txn_ref_id.trim();
			
			cstmt.setString(4,txn_ref_id);
			cstmt.setDouble(5,p_dAmount);
			cstmt.setString(6,p_sTxnType);
			cstmt.setString(7,encryptInputData(p_oOrderItemDetailsVO.getCCNo(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(8,encryptInputData(p_oOrderItemDetailsVO.getCardType(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(9,encryptInputData(p_oOrderItemDetailsVO.getExpDt(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(10,encryptInputData(p_oOrderItemDetailsVO.getCardFname(),"CLIENT",p_oOrderItemDetailsVO.getKeyRefId()));
			cstmt.setString(11,p_oOrderItemDetailsVO.getCustAddress1());
			cstmt.setString(12,p_oOrderItemDetailsVO.getCustAddress2());
			cstmt.setString(13,p_oOrderItemDetailsVO.getCustCity());
			cstmt.setString(14,p_oOrderItemDetailsVO.getCustState());
			cstmt.setString(15,p_oOrderItemDetailsVO.getCustCountry());
			cstmt.setString(16,p_oOrderItemDetailsVO.getCustZip());
			
			return_id = p_oOrderItemDetailsVO.getReturnId();
			return_id = return_id == null?"":return_id.trim();
			
			cstmt.setString(17,return_id);
			cstmt.setString(18,p_sLoginId);
			cstmt.executeUpdate();
			lPayTxnId=cstmt.getInt(1);

			logger.info("PaymentDAO::addCustPayTxnDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PaymentDAO::addCustPayTxnDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return lPayTxnId;
	
	}

	public static void updateCustPayTxnDetails(long p_lPayTxnId,String p_sTxnStatus,String p_sTxnMsg,String p_sPayTxnRefId, String p_sTxnMsgCode,String p_sInBoundMsg,String p_sOutBoundMsg,String p_sTxnType, String p_sReturnId,String p_sPayTxnMode, String p_sOrderItemId, String p_sLoginId) 
		throws Exception {
		logger.info("PaymentDAO::updateCustPayTxnDetails:ENTER");
		
		String sQry="{call usp_sbh_upd_cust_pay_txn_details(?,?,?,?,?,?,?,?,?,?,?,?)}";
		CallableStatement cstmt=null;
		Connection con=null;		
		ResultSet rs=null;
		
		logger.debug("PaymentDAO::updateCustPayTxnDetails::SQL QUERY "+sQry);
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Pay Txn Id :"+p_lPayTxnId);		
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Txn Status :"+p_sTxnStatus);		
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Txn Type :"+p_sTxnType);		
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Txn Msg :"+p_sTxnMsg);	
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Txn Msg Code :"+p_sTxnMsgCode);		
		logger.debug("PaymentDAO::updateCustPayTxnDetails::Tnx Ref Id :"+p_sPayTxnRefId);

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setLong(1, p_lPayTxnId);
			cstmt.setString(2,p_sTxnStatus);
			cstmt.setString(3,p_sTxnType);
			cstmt.setString(4,p_sTxnMsg);
			cstmt.setString(5,p_sTxnMsgCode);
			cstmt.setString(6,p_sInBoundMsg);
			cstmt.setString(7,p_sOutBoundMsg);
			cstmt.setString(8,p_sPayTxnRefId);
			
			p_sReturnId = p_sReturnId==null?"":p_sReturnId.trim();
			
			cstmt.setString(9,p_sReturnId);
			cstmt.setString(10,p_sPayTxnMode);
			cstmt.setString(11,p_sOrderItemId);
			cstmt.setString(12,p_sLoginId);

			cstmt.executeUpdate();


			logger.info("PaymentDAO::updateCustPayTxnDetails:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PaymentDAO::updateCustPayTxnDetails:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	
	public static CCStoreInfoVO getCCStoreInfo() throws Exception{	
		logger.info("PaymentDAO::getCCStoreInfo:ENTER");
		
		String sDBData = null;
		String sQry="{call usp_sbh_get_cc_store_info}";
		CallableStatement cstmt=null;
		CCStoreInfoVO oCCStoreInfoVO=null;
		Connection con=null;		
		ResultSet rs=null;
		
		logger.debug("PaymentDAO::getCCStoreInfo:SQL QUERY "+sQry);
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
//			cstmt.setString(1, p_sOrderItemId);
			cstmt.execute();
			rs = cstmt.getResultSet();
			if(rs.next()) {				
				oCCStoreInfoVO=new CCStoreInfoVO();
				
				sDBData = rs.getString("key_ref_id");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setKeyRefId(sDBData);
				
				sDBData = rs.getString("client_cert_path");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setClientCertificatePath(decryptData(sDBData, oCCStoreInfoVO.getKeyRefId()));
				
				sDBData = rs.getString("password");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setPassword(decryptData(sDBData, oCCStoreInfoVO.getKeyRefId()));

				sDBData = rs.getString("store_no");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setStoreNumber(decryptData(sDBData, oCCStoreInfoVO.getKeyRefId()));

				sDBData = rs.getString("host");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setHost(decryptData(sDBData, oCCStoreInfoVO.getKeyRefId()));

				sDBData = rs.getString("port");
				sDBData = sDBData == null?"":sDBData.trim();
				oCCStoreInfoVO.setPort(decryptData(sDBData, oCCStoreInfoVO.getKeyRefId()));

			}
			logger.info("PaymentDAO::getCCStoreInfo:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PaymentDAO::getCCStoreInfo:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oCCStoreInfoVO;
	}
	public static long getAddrNumber(String p_sAddr){
		String sNo="";
		long lNumber = 0;
		char c;
		if(p_sAddr!=null){
			for(int i=0;i<p_sAddr.length();i++){
				c = p_sAddr.charAt(i);
				if(Character.isDigit(c)){
					sNo = sNo+c;
				}
			}
		}
		if(sNo!=null && !sNo.equalsIgnoreCase("")){
			try{
				lNumber = Long.parseLong(sNo);
			}catch (Exception e) {
				lNumber = 0;
			}
		}
		return lNumber;
	}
}
