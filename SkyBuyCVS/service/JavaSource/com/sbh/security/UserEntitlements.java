package com.sbh.security;


import static com.sbh.dao.OrderDAO.decryptInputData;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.util.DBConnection;
import com.sbh.vo.LoginAirlineVO;
import com.sbh.vo.LoginVO;


/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserEntitlements{
	private static Logger logger = LogManager.getLogger(UserEntitlements.class);

	public static LoginVO authenticateUser (String p_sUserId, String p_sPassword,String p_sUserType) throws Exception {
		logger.info("authenticateVendorUser:ENTER");		

		LoginVO oLoginVO = null;
		try {

			oLoginVO = validateUser(p_sUserId,p_sPassword,p_sUserType);
			logger.info("authenticateVendorUser:EXIT");	
		} catch (Exception e) {
			String sMsg = "User Authentication failed: " + e.getMessage();
			logger.error("authenticateVendorUser:"+sMsg);
			throw new Exception (sMsg);
		}
		return oLoginVO;
	}
	public static LoginVO validateUser(String p_sUserId,String p_sPassword,String p_sUserType) throws Exception{	
		logger.info("validateVendorUser:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		LoginVO oLoginVO = null;
		String sUserType=null,sIsSuperUser = null;
		int iGroupId;
		String sQry="{call usp_sbh_validate_user(?,?,?,?,?,?,?,?)}";
		String sEncryptedPassword = null;
		
		logger.debug("validateVendorUser:SQL QUERY "+sQry);	
		logger.debug("validateVendorUser:User Id"+p_sUserId);		
		logger.debug("validateVendorUser:Password"+p_sPassword);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sUserId);	
			cstmt.setString(2,p_sPassword);	
			cstmt.registerOutParameter(3, Types.CHAR);	
			cstmt.registerOutParameter(4, Types.CHAR);	
			cstmt.registerOutParameter(5, Types.INTEGER);	
			cstmt.registerOutParameter(6, Types.INTEGER);	
			cstmt.registerOutParameter(7, Types.CHAR);
			cstmt.registerOutParameter(8, Types.VARCHAR);
			cstmt.execute();
			rs =cstmt.getResultSet();
			 
			if(rs!=null){
				while(rs.next()){
					oLoginVO = new LoginVO();
					String sRefId =rs.getString("ref_id");
					sRefId = sRefId == null?"":sRefId.trim();
					oLoginVO.setRefId(sRefId);
					
					String sUserName =rs.getString("user_name");
					sUserName = sUserName == null?"":sUserName.trim();
					oLoginVO.setUserName(sUserName);
					
					String sEmail =rs.getString("email");
					sEmail = sEmail == null?"":sEmail.trim();
					oLoginVO.setEmail(sEmail);
					
					String sContactName =rs.getString("contact_name");
					sContactName = sContactName == null?"":sContactName.trim();
					oLoginVO.setContactName(sContactName);
					
					oLoginVO.setUserId(p_sUserId);
					oLoginVO.setPassword(p_sPassword);
				}
			}
				
			 sUserType =cstmt.getString(3); 
			 sUserType=sUserType==null?"":sUserType.trim();
			 
			 sIsSuperUser  =cstmt.getString(4); 
			 sIsSuperUser=sIsSuperUser==null?"":sIsSuperUser.trim();
			 
			 iGroupId=cstmt.getInt(5); 
			 
			 int iId=cstmt.getInt(6); 
			 
			 sEncryptedPassword = cstmt.getString(7);
			 sEncryptedPassword = sEncryptedPassword==null?"":sEncryptedPassword.trim();
			 
			 String sKeyRefId =cstmt.getString(8);
			 sKeyRefId = sKeyRefId == null?"":sKeyRefId.trim();
			 oLoginVO.setKeyRefId(sKeyRefId);
			 
			 if(sEncryptedPassword.trim().length() > 0)
				 sEncryptedPassword = decryptInputData(sEncryptedPassword,"SERVER",oLoginVO.getKeyRefId());
			 
			 if(sEncryptedPassword != null && sEncryptedPassword.trim().length() > 0) {
				 if(sEncryptedPassword.equals(p_sPassword)) {
					 if(p_sUserType.equalsIgnoreCase(sUserType)){
						 
					 	/*if(sUserType.equalsIgnoreCase("admin") ){
							oLoginVO = new LoginVO();
							oLoginVO.setUserName(p_sUserId);
							oLoginVO.setUserId(p_sUserId);
							oLoginVO.setRefId("0");
							oLoginVO.setPassword(p_sPassword);
							oLoginVO.setUserType(sUserType.toLowerCase());
						}*/
					 	
					 	if(oLoginVO!=null){
							 oLoginVO.setUserType(sUserType.toLowerCase());
							 oLoginVO.setIsSuperUser(sIsSuperUser);
							 oLoginVO.setGroupId(iGroupId);
							 oLoginVO.setId(iId+"");
					 	}
					 }else{
						 oLoginVO=null;
					 } 
				 }else{
					 oLoginVO=null;
				 } 
			 }else{
				 oLoginVO=null;
			 } 
				
			
			logger.info("validateVendorUser:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("validateVendorUser:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oLoginVO;
	}
	public static LoginAirlineVO authenticateAirlineUser (String p_sUserId, String p_sPassword) throws Exception {
		logger.info("authenticateAirlineUser:ENTER");		

		LoginAirlineVO oLoginAirlineVO = null;
		try {

			oLoginAirlineVO = validateAirlineUser(p_sUserId,p_sPassword);
			logger.info("authenticateAirlineUser:EXIT");	
		} catch (Exception e) {
			String sMsg = "User Authentication failed: " + e.getMessage();
			logger.error("authenticateAirlineUser:"+sMsg);
			throw new Exception (sMsg);
		}
		return oLoginAirlineVO;
	}
	public static LoginAirlineVO validateAirlineUser(String p_sUserId,String p_sPassword) throws Exception{	
		logger.info("validateAirlineUser:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		LoginAirlineVO oLoginAirlineVO = null;

		String sQry="{call usp_sbh_validate_airline_userid(?,?)}";

		logger.debug("validateAirlineUser:SQL QUERY "+sQry);	
		logger.debug("validateAirlineUser:User Id"+p_sUserId);		
		logger.debug("validateAirlineUser:Password"+p_sPassword);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,p_sUserId);	
			cstmt.setString(2,p_sPassword);	
			cstmt.execute();
			rs =cstmt.getResultSet();
			while(rs.next()){
				oLoginAirlineVO = new LoginAirlineVO();
				oLoginAirlineVO.setAirId(rs.getString("air_id"));
				oLoginAirlineVO.setAirName(rs.getString("air_name"));
				oLoginAirlineVO.setAirUserId(rs.getString("air_userid"));
				oLoginAirlineVO.setAirPassword(rs.getString("air_password"));
			}

			logger.info("validateAirlineUser:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("validateAirlineUser:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return oLoginAirlineVO;
	}


	public static ArrayList buildEntitlementActions(LoginVO oLoginVO) throws Exception{	
		logger.info("buildEntitlementActions:ENTER");		

		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		ArrayList alListOfActions = new ArrayList();
		String sDBData = null;
		String sQry="{call usp_sbh_get_user_entitled_actions(?,?,?)}";

		logger.debug("buildEntitlementActions:SQL QUERY "+sQry);	
		logger.debug("buildEntitlementActions:User Type:"+oLoginVO.getUserType());		
		logger.debug("buildEntitlementActions:Group Id:"+oLoginVO.getGroupId());		
		logger.debug("buildEntitlementActions:Super User:"+oLoginVO.getIsSuperUser());

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1,oLoginVO.getUserType());	
			cstmt.setInt(2,oLoginVO.getGroupId());	
			cstmt.setString(3,oLoginVO.getIsSuperUser());	
			
			cstmt.execute();
			rs =cstmt.getResultSet();
			while(rs.next()){
				sDBData = rs.getString("action_name");
				sDBData = sDBData == null?"":sDBData.trim();
				alListOfActions.add(sDBData);
			}

			logger.info("buildEntitlementActions:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("buildEntitlementActions:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alListOfActions;
	}


}
