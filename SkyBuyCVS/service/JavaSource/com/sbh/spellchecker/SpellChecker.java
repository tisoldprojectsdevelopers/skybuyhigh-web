/**
 * 
 */
package com.sbh.spellchecker;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.plugin.SBHDelegator;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;

/**
 * @author nbvk
 *
 */
public class SpellChecker extends DispatchAction {
	private static Logger logger = LogManager.getLogger(SpellChecker.class);
	
	public ActionForward spellcheck(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("SpellChecker::spellcheck:ENTER");
		String sText = "";
		String inputLine = "";
		String sOutPut = "";
		try {
			//Format the XML that needs to be send to Google Spell
			//Checker.
			sText = request.getParameter("input");
			StringBuffer requestXML = new StringBuffer();
			requestXML.append("<spellrequest textalreadyclipped=\"0\""+
					" ignoredups=\"1\""+
			" ignoredigits=\"1\" ignoreallcaps=\"0\"><text>");
			requestXML.append(sText);
			requestXML.append("</text></spellrequest>");
			//The Google Spell Checker URL
			URL url = new URL("https://www.google.com/tbproxy/spell?lang=eng");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);

			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
			
			//System.out.println("Input String:"+requestXML.toString());
			out.write(requestXML.toString());
			out.close();

			//Get the result from Google Spell Checker
			InputStream in = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			

			while ((inputLine = br.readLine()) != null) {
				//Print out the response from Google
				//System.out.println(inputLine);
				sOutPut += inputLine;
			}
			in.close();
			response.setContentType("application/xml");
			PrintWriter pw = response.getWriter();
			//System.out.println("test:"+sOutPut);
			pw.println(sOutPut);
			pw.flush();	
		}
		catch (Exception e) {
			logger.info("SpellChecker::spellcheck:Exception:"+e.getMessage());
			
			//System.out.println("Exception "+ e);
		}
		logger.info("SpellChecker::spellcheck:EXIT");
		return null;
	}
}
