package com.sbh.email;

import java.awt.Insets;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;



import com.sbh.vo.EmailLogVO;


public class Email {
	private static Logger logger = LogManager.getLogger(Email.class);
	String sEmailTemplateId;
	public boolean sendEmail(EmailLogVO p_oLogVO) throws Exception{

		logger.info("SBHMail:sendEmail::Enter");
		boolean bSuccess = false;
		Session oSession = null;
		InternetAddress oFromAddress = null;
		InternetAddress oToAddress = null;
		InternetAddress[] oa_ToAddresses = null,oa_CcAddresses = null,oa_BccAddresses = null;
		MimeMessage oMessage=null;
		Properties oProperties = new Properties();
		ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sSMTPHost= oBundle.getString("smtp_server_host");		
		sSMTPHost = (sSMTPHost == null?"":sSMTPHost.trim());
		oProperties.put("mail.smtp.host",sSMTPHost);
		oSession = Session.getDefaultInstance(oProperties);
		oMessage = new MimeMessage(oSession);
		MimeBodyPart mbp1 = new MimeBodyPart();
		MimeBodyPart mbp = new MimeBodyPart();
		Multipart mp = new MimeMultipart();

		logger.info("SBHMail:sendEmail::Enter");

		logger.info("SBHMail:sendEmail::From Address:"+p_oLogVO.getEmailFrom());
		oFromAddress = new InternetAddress(p_oLogVO.getEmailFrom());
		oMessage.setFrom(oFromAddress);

		/*String sBcc= oBundle.getString("email.bcc.address");
		sBcc = sBcc ==null?"":sBcc.trim();
		if(p_oLogVO.getEmailBcc()!=null){
			sBcc = addToStringArray(p_oLogVO.getEmailBcc(),sBcc);
			p_oLogVO.setEmailBcc(convertToStringArray(sBcc));
		}else{
			p_oLogVO.setEmailBcc(convertToStringArray(sBcc));
		}			
*/

		if(p_oLogVO.getEmailToAddr()!= null ){
			logger.info("SBHMail:sendEmail::TOAddress:"+p_oLogVO.getEmailToAddr());
			oToAddress = new InternetAddress(p_oLogVO.getEmailToAddr());
			oMessage.setRecipient(Message.RecipientType.TO, oToAddress);
		}   
		if(p_oLogVO.getEmailTo() != null && p_oLogVO.getEmailTo().length > 0){
			logger.info("SBHMail:sendEmail::TOAddress:"+convertStringArrayToString(p_oLogVO.getEmailTo()));
			oa_ToAddresses = new InternetAddress[p_oLogVO.getEmailTo().length];
			oa_ToAddresses = addressString2InternetArray(p_oLogVO.getEmailTo());
			oMessage.setRecipients(Message.RecipientType.TO, oa_ToAddresses);
		}    

		if(p_oLogVO.getEmailCc() != null && p_oLogVO.getEmailCc().length > 0){
			logger.info("SBHMail:sendEmail::CCAddress:"+convertStringArrayToString(p_oLogVO.getEmailCc()));
			oa_CcAddresses = new InternetAddress[p_oLogVO.getEmailCc().length];
			oa_CcAddresses = addressString2InternetArray(p_oLogVO.getEmailCc());
			oMessage.setRecipients(Message.RecipientType.CC, oa_CcAddresses);
		}  
		if(p_oLogVO.getEmailBcc() != null && p_oLogVO.getEmailBcc().length > 0){
			logger.info("SBHMail:sendEmail::BccAddress:"+convertStringArrayToString(p_oLogVO.getEmailBcc()));
			oa_BccAddresses = new InternetAddress[p_oLogVO.getEmailBcc().length];
			oa_BccAddresses = addressString2InternetArray(p_oLogVO.getEmailBcc());
			oMessage.setRecipients(Message.RecipientType.BCC, oa_BccAddresses);
		}   

		oMessage.setSubject(p_oLogVO.getEmailSubject());

		/*//This is to send the attachment
		String sFileName= p_oLogVO.getAttachmentFile();
		sFileName = (sFileName==null?"":sFileName.trim());
		if(!sFileName.equals("")){

			mbp1.setContent(p_oLogVO.getEmailText(), "text/html");
			FileDataSource fds = new FileDataSource(sFileName);
			if(fds != null){
				logger.info("SBHMail:sendEmail::File attachment");
				mbp.setDataHandler(new DataHandler(fds));

				mbp.setFileName(fds.getName());
				mp.addBodyPart(mbp);
				mp.addBodyPart(mbp1);
				oMessage.setContent(mp);

			}
		}
		else{
			oMessage.setContent(p_oLogVO.getEmailText(), "text/html");
		}*/

		//This is to send the attachment
		String sFileName= p_oLogVO.getAttachmentFile();
		sFileName = (sFileName==null?"":sFileName.trim());
		if(!sFileName.equals("")){

			mbp1.setContent(p_oLogVO.getEmailText(), "text/html");
			FileDataSource fds = new FileDataSource(sFileName);
			if(fds != null){
				logger.info("SBHMail:sendEmail::File attachment");
				mbp.setDataHandler(new DataHandler(fds));

				mbp.setFileName(fds.getName());
				mbp.attachFile(fds.getFile());
				mp.addBodyPart(mbp);
				mp.addBodyPart(mbp1);
				oMessage.setContent(mp);

			}
		}else{
			oMessage.setContent(p_oLogVO.getEmailText(), "text/html");
		}
		try{
			oMessage.setSentDate(new Date());
			oMessage.saveChanges(); // implicit with send()
			Transport.send(oMessage);
			// System.out.println("Mail Sent");
			logger.info("SBHMail:sendEmail::SendSuccess ");

			bSuccess = true;
		}catch(SendFailedException oExcep){
			oExcep.printStackTrace();
			logger.error("SBHMail:sendEmail::Exception "+oExcep.getMessage());
			throw oExcep;

		}	

		logger.info("SBHMail:sendEmail::Exit");
		return bSuccess;
	}

	//Tokenise the TO and CC in the String[] array to generate the Internet Address Array
	public String[] convertToStringArray(String p_sEmailAddr){
		logger.info("SBHMail:convertToStringArray::ENTER");
		String[] p_saEmailAddresses = null;
		if(p_sEmailAddr != null){
			StringTokenizer oToken = new StringTokenizer(p_sEmailAddr,",");
			p_saEmailAddresses = new String[oToken.countTokens()];
			int index = 0;
			while(oToken.hasMoreTokens()){
				p_saEmailAddresses[index] = (String)oToken.nextToken();
				index++;
			}
		}	
		logger.info("SBHMail:convertToStringArray::EXIT");
		return p_saEmailAddresses;
	}

	//Created to form the Internet Address Arrya for To and CC by passing String[] of To and CC

	public InternetAddress[] addressString2InternetArray(String[] p_saAddresses) throws Exception{
		logger.info("SBHMail:addressString2InternetArray::ENTER");
		InternetAddress[] aRetValue = null;
		if(p_saAddresses != null && p_saAddresses.length > 0){
			aRetValue = new InternetAddress[p_saAddresses.length];
			for(int index = 0 ;index < p_saAddresses.length ; index++)
				aRetValue[index] = new InternetAddress(p_saAddresses[index]);
		}	
		logger.info("SBHMail:addressString2InternetArray::EXIT");
		return aRetValue;
	}
	public static String addToStringArray(String[] p_sArray,String sText){
		String sResult = ""; 
		if(sText!=null){
			sResult = sText;

		}
		if(p_sArray!=null){
			for(String temp: p_sArray){
				sResult += ","+temp;
			}
		}
		// System.out.println("Bcc Address:"+sResult);
		return sResult;
		
	}

	
	public String convertStringArrayToString(String p_sEmailAddr[]){
		logger.info("SBHMail:convertToStringArray::ENTER");
		String p_sEmailAddresses = "";
		int iIndex = 0;
		if(p_sEmailAddr != null){
			for(String sAddr:p_sEmailAddr){
				if(iIndex!=0){
					p_sEmailAddresses += ",";
				}
				p_sEmailAddresses += sAddr;
				iIndex = 1;
			}
			
		}	
		logger.info("SBHMail:convertToStringArray::EXIT");
		return p_sEmailAddresses;
	}
}
