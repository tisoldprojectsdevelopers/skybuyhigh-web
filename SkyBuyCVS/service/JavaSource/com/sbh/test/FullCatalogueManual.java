package com.sbh.test;


import static com.sbh.contants.SkyBuyContants.IPHONE;
import static com.sbh.contants.SkyBuyContants.ITOUCH;
import static com.sbh.dao.CatalogueDAO.getDeviceType;
import static com.sbh.util.Utils.deleteFiles;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sbh.client.vo.UpdatedCatalogueDetailsVO;
import com.sbh.dao.ImageScale;
import com.sbh.dao.OrderDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.util.GenerateCatalogueZip;
import com.sbh.util.Utils;
import com.sbh.util.generatexml.GenerateCatalogue;
import com.sbh.vo.AdminReturnPolicyVO;
import com.sbh.vo.PartnerVO;
import com.sbh.vo.ProductDetailsVO;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class FullCatalogueManual {
	private static Logger logger = LogManager.getLogger(FullCatalogueManual.class);

	public static void main(String s[]){
		logger.info("EmailScheduleValidate::generateFullCatalogue::ENTER");

		String sAirCode = null;
		String sDeviceId = "";
		String sCommaSepCoverflowProdIds = null;
		String sIsAirlineChanged = "YES";
		String sCatelogueXmlFilePath = null;
		String sLastDownloadedCateDate = "FullCatalogue";
		String sCatalogueFileName = null;
		String sCatalogueFolderPath = null;
		String sZipFolderPath = null;
		String sCatalogueZipFolder = null;
		String sCatalogueName = null;
		GenerateCatalogueZip oGenerateCatalogueZip =  null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		File oCatalogueDownloadFile = null;
		File oCatalogueXmlFile =  null;
		File oCoverflowXmlFile =  null;

		File fFullCatalogue = null;
		File fXML = null;
		File fCatalogueZip = null;
		List<String> saAirCode = null;
		ArrayList<String> alCoverflowProdIds = null;
		try {
			generateCatalogueXML("C:/StaticContent/","Admin","0","Admin","","","","DEMONSTRATIONCATALOGUE","","");
			
			sCatalogueFolderPath = "C:/StaticContent";
			sZipFolderPath = "C:/StaticContent/FullCatalogue";
			sCatalogueZipFolder = sCatalogueFolderPath +"/DownloadCatalogue/"+sAirCode;
			
			 //* Get all airline code present in the database.
			 
			saAirCode = getAllAirlineCode();
			if(saAirCode != null && !saAirCode.isEmpty()) {
				fFullCatalogue = new File(sZipFolderPath);
				if(!fFullCatalogue.exists()) {
					fFullCatalogue.mkdirs();
				}
				for(String sAirlineId:saAirCode) {
					sAirCode = sAirlineId;
					sAirCode = sAirCode==null?"":sAirCode.trim();
					sLastDownloadedCateDate = sLastDownloadedCateDate == null ?" ":sLastDownloadedCateDate.trim();
					sCatelogueXmlFilePath = sCatalogueFolderPath+"/XML/"+sAirCode;
					sCatalogueZipFolder = sCatalogueFolderPath +"/DownloadCatalogue/"+sAirCode;

					fXML = new File(sCatelogueXmlFilePath);
					if(!fXML.exists()) {
						fXML.mkdirs();
					}
					fCatalogueZip = new File(sCatalogueZipFolder);
					if(!fCatalogueZip.exists()) {
						fCatalogueZip.mkdirs();
					}

					//UploadSkyBuyCatalogueDAO.isDownloadCatalogueAvailable(sLastDownloadedCateDate,sAirCode,sIsAirlineChanged);
					
					 //* Generate the Catalogue XML file.
					 
					alCoverflowProdIds = generateCatalogueXML(sCatelogueXmlFilePath,"CLIENT",sAirCode,"","","","","",sDeviceId,"");	
					sCatalogueFileName = "SkyBuy";
					sCommaSepCoverflowProdIds = Utils.listToCommaSepertedString(alCoverflowProdIds);
					sCatalogueName = sCatalogueFileName+".zip";
					sCatalogueFileName = sCatalogueFileName+"_"+sAirCode+".zip";

					oGenerateCatalogueZip = new GenerateCatalogueZip(sZipFolderPath,sCatalogueFileName);
					
					// * Generate the Catalogue zip file.
					 
					getCatalogueDetailsByDate(sLastDownloadedCateDate,sCatalogueFolderPath,sCatalogueName,sAirCode,sDeviceId,sCommaSepCoverflowProdIds,sIsAirlineChanged,sCatelogueXmlFilePath,sCatalogueZipFolder);
					
					 //* Delete the generated XML files.
					 
					String sCataogueFileName = sCatalogueZipFolder+sCatalogueName;
					oGenerateCatalogueZip.addFile(sCatalogueZipFolder, sCatalogueName, sCatalogueName);
					oGenerateCatalogueZip.finish();

					if(fXML != null && fXML.exists()) {
						deleteFiles(fXML);
					}
					if(fCatalogueZip != null && fCatalogueZip.exists()) {
						deleteFiles(fCatalogueZip);
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("EmailScheduleValidate::generateFullCatalogue::EXCEPTION"+e.getMessage());
		}finally{
			if(oCatalogueXmlFile != null && oCatalogueXmlFile.exists())
				oCatalogueXmlFile.delete();
			if(oCatalogueXmlFile != null && oCoverflowXmlFile.exists())
				oCoverflowXmlFile.delete();
			if(oCatalogueDownloadFile != null && oCatalogueDownloadFile.exists())
				oCatalogueDownloadFile.delete();
			if(fXML != null && fXML.exists()) {
				deleteFiles(fXML);
			}
		}
	}
	public static Map<String,String> getCatalogueDetailsByDate(String p_sLastDownloadedCateDt,String p_sCatalogueFolderPath,String p_sCatalogueFileName,String p_sAirId,String p_sDeviceId,String p_sCoverflowProdIds,String p_sIsAirlinePackageDownload,String p_sCatalogueXMLFile,String p_sZipFolderPath) throws Exception{	
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::ENTER");		
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sCoverImageXML = null;
		String sAboutPartnerXML = null;
		String sDownloadCateDate = null;
		String sQry="{call usp_sbh_get_catalogue_by_date(?,?,?,?,?,?,?,?,?)}";
		GenerateCatalogueZip oGenerateCatalogueZip =  null;
		String sSwfFilePath = null,sMainSrcPath =null,sMainSrcFileName =null,sMainDestFolderAndFileName= null;
		String sSwfSrcFileName = null,sView1SrcPath = null,sView1SrcFileName= null,sView1DestFolderAndFileName=null;
		String sView2SrcPath = null,sView2SrcFileName= null,sView2DestFolderAndFileName=null;
		String sView3SrcPath = null,sView3SrcFileName= null,sView3DestFolderAndFileName=null;
		String sAirLogoPath = null,sAirLogoFileName =null,sAirLogoIntroPath = null,sAirLogoIntroFileName = null;
		String sAirLogoDestFolderAndFileName = null,sAirLogoIntroDestFolderAndFileName = null;
		String sNetworkUserName = null;
		String sNetworkPassword = null;
		String sAccountInfo = null;
		String sClientCertPath = null;
		String sClientCertPass = null;
		String sDeviceType = null;
		int iPartnerPriorityUpdated = 0;
		Map<String,String> hmReturn = new HashMap<String, String>();
		boolean bIsNewVersionFound=false;
		boolean bIsFileUpdated = false;
		String sE_CatalogueFileName = "eCatalogue";
		int iUpdatedProdCount = 0,iUpdatedAirLogoCount =0, iUpdatedReturnPolicy = 0;
		/*sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml";
		sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml";
		sE_CatalogueFileName = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_"+p_sDeviceId+".xml";*/
		sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner.xml";
		sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover.xml";
		sE_CatalogueFileName = sE_CatalogueFileName==null?"":sE_CatalogueFileName+".xml";
		
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::SQL QUERY "+sQry);	
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Last Downloaded Date:"+p_sLastDownloadedCateDt);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Catalogue Folder Path:"+p_sCatalogueFolderPath);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Cataogue File Name:"+p_sCatalogueFileName);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Air Id:"+p_sAirId);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Device Id:"+p_sDeviceId);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Cover Flow Prod Id:"+p_sCoverflowProdIds);
		
		try{
			sDeviceType = "";//getDeviceType(p_sDeviceId);
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);						
			cstmt.setString(1,p_sLastDownloadedCateDt);	
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,p_sCoverflowProdIds);
			cstmt.setString(4, p_sIsAirlinePackageDownload);
			
			cstmt.registerOutParameter(5, Types.VARCHAR);	
			cstmt.registerOutParameter(6, Types.INTEGER);	
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.registerOutParameter(8, Types.INTEGER);;
			cstmt.registerOutParameter(9, Types.INTEGER);;
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			oGenerateCatalogueZip =  new GenerateCatalogueZip(p_sZipFolderPath,p_sCatalogueFileName);
			while(rs.next()){	
				bIsNewVersionFound=true;
				// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
				String sProdId = rs.getString("prod_id");
				String sCateName = rs.getString("cate_name");
				sCateName = sCateName == null?"":sCateName.trim();
				String sOwnerId = rs.getString("owner_id");	
				String sOwnerType = rs.getString("owner_type");

				String sMainImgPath = rs.getString("main_img_path");
				sMainImgPath =sMainImgPath ==null?"":sMainImgPath.trim();
				String sMainImgType = rs.getString("main_img_type");
				sMainImgType =sMainImgType ==null?"":sMainImgType.trim();

				String sView1ImgPath = rs.getString("view1_img_path");
				sView1ImgPath =sView1ImgPath ==null?"":sView1ImgPath.trim();
				String sView1ImgType = rs.getString("view1_img_type");
				sView1ImgType =sView1ImgType ==null?"":sView1ImgType.trim();

				String sView2ImgPath = rs.getString("view2_img_path");
				sView2ImgPath =sView2ImgPath ==null?"":sView2ImgPath.trim();
				String sView2ImgType = rs.getString("view2_img_type");
				sView2ImgType =sView2ImgType ==null?"":sView2ImgType.trim();

				String sView3ImgPath = rs.getString("view3_img_path");
				sView3ImgPath =sView3ImgPath ==null?"":sView3ImgPath.trim();
				String sView3ImgType = rs.getString("view3_img_type");	
				sView3ImgType =sView3ImgType ==null?"":sView3ImgType.trim();

				String sSwfType = rs.getString("swf_type");	
				sSwfType =sSwfType ==null?"":sSwfType.trim();
				
				String sSwfPath = rs.getString("swf_path");	
				sSwfPath =sSwfPath ==null?"":sSwfPath.trim();
			
				String sIsAdvt = rs.getString("is_advt");	
				sIsAdvt =sIsAdvt ==null?"":sIsAdvt.trim();
				//To SWF file
				
				if("Y".equalsIgnoreCase(sIsAdvt) && sSwfPath.trim().length()>0 && sSwfType.trim().length()>0){
					//Swf folder
					sSwfFilePath =p_sCatalogueFolderPath+"/"+sSwfPath+"/";
					sSwfSrcFileName = sOwnerId+sProdId+"."+sSwfType;
					sMainDestFolderAndFileName = sSwfPath+"/"+sOwnerId+sProdId+"."+sSwfType;
					oGenerateCatalogueZip.addFile(sSwfFilePath, sSwfSrcFileName, sMainDestFolderAndFileName);
					
				}
				
				//To Add Special Product
				String sIsSpecialProd = rs.getString("is_special_prod");	
				sIsSpecialProd =sIsSpecialProd ==null?"":sIsSpecialProd.trim();
				
				String sProdSwfType = rs.getString("prod_swf_type");	
				sProdSwfType =sProdSwfType ==null?"":sProdSwfType.trim();
				
				String sProdSwfPath = rs.getString("prod_swf_path");	
				sProdSwfPath =sProdSwfPath ==null?"":sProdSwfPath.trim();
				
				if("Y".equalsIgnoreCase(sIsSpecialProd) && sProdSwfPath.trim().length()>0 && sProdSwfType.trim().length()>0){
					sSwfFilePath =p_sCatalogueFolderPath+"/"+sProdSwfPath+"/";
					sSwfSrcFileName = sOwnerId+sProdId+"."+sProdSwfType;
					sMainDestFolderAndFileName = sProdSwfPath+"/"+sOwnerId+sProdId+"."+sProdSwfType;
					oGenerateCatalogueZip.addFile(sSwfFilePath, sSwfSrcFileName, sMainDestFolderAndFileName);
					
				}
				
				//Main Img path
				if(sMainImgPath.trim().length()>0 && sMainImgType.trim().length()>0){
					//Image folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Images/";
					sMainSrcFileName = sOwnerId+sProdId+"image."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					// System.out.println("sMainSrcPath "+sMainSrcPath);
					// System.out.println("sMainSrcFileName "+sMainSrcFileName);
					// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
					
					//Large folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Large/";
					sMainSrcFileName = sOwnerId+sProdId+"large."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Med/";
						sMainSrcFileName = sOwnerId+sProdId+"med."+sMainImgType;
						sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					}
					//Thumb folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Thumb/";
					sMainSrcFileName = sOwnerId+sProdId+"thumb."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//CoverFlow Image
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/CoverFlow/";
						sMainSrcFileName = sOwnerId+sProdId+"."+sMainImgType;
						sMainDestFolderAndFileName = sMainImgPath+"/CoverFlow/"+sOwnerId+sProdId+"."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
						
						//Original Img 
						sMainSrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sMainSrcFileName = sOwnerId+sProdId+"main."+sMainImgType;
						sMainDestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"main."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					}
				}
				//View1 Img path
				if(sView1ImgPath.trim().length()>0 && sView1ImgType.trim().length()>0){
					//Image folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Images/";
					sView1SrcFileName = sOwnerId+sProdId+"image."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
				
					//Large folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Large/";
					sView1SrcFileName = sOwnerId+sProdId+"large."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Med folder
						sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Med/";
						sView1SrcFileName = sOwnerId+sProdId+"med."+sView1ImgType;
						sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView1ImgType;
						oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					}
					//Thumb folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Thumb/";
					sView1SrcFileName = sOwnerId+sProdId+"thumb."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView1SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView1SrcFileName = sOwnerId+sProdId+"view1."+sView1ImgType;
						sView1DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"view1."+sView1ImgType;
						oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					}
				}
				//View2 Img path
				if(sView2ImgPath.trim().length()>0 && sView2ImgType.trim().length()>0){
					//Image folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Images/";
					sView2SrcFileName = sOwnerId+sProdId+"image."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					//Large folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Large/";
					sView2SrcFileName = sOwnerId+sProdId+"large."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Med/";
						sView2SrcFileName = sOwnerId+sProdId+"med."+sView2ImgType;
						sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView2ImgType;
						oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					}
					
					//Thumb folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Thumb/";
					sView2SrcFileName = sOwnerId+sProdId+"thumb."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView2SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView2SrcFileName = sOwnerId+sProdId+"view2."+sView2ImgType;
						sView2DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/"+sOwnerId+sProdId+"view2."+sView2ImgType;
						oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					}
				}
				//View3 Img path
				if(sView3ImgPath.trim().length()>0 && sView3ImgType.trim().length()>0){
					//Image folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Images/";
					sView3SrcFileName = sOwnerId+sProdId+"image."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					
					//Large folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Large/";
					sView3SrcFileName = sOwnerId+sProdId+"large."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Med/";
						sView3SrcFileName = sOwnerId+sProdId+"med."+sView3ImgType;
						sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView3ImgType;
						oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					}
					
					//Thumb folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Thumb/";
					sView3SrcFileName = sOwnerId+sProdId+"thumb."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);		
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView3SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView3SrcFileName = sOwnerId+sProdId+"view3."+sView3ImgType;
						sView3DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"view3."+sView3ImgType;
						oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					}
				}
				// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
			}
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				while(rs.next()){	
					bIsNewVersionFound=true;
					// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
					String sProdId = rs.getString("prod_id");
//					String sCateName = rs.getString("cate_name");
					String sOwnerId = rs.getString("owner_id");	
//					String sOwnerType = rs.getString("owner_type");

					String sMainImgPath = rs.getString("main_img_path");
					sMainImgPath =sMainImgPath ==null?"":sMainImgPath.trim();
					String sMainImgType = rs.getString("main_img_type");
					sMainImgType =sMainImgType ==null?"":sMainImgType.trim();

					String sView1ImgPath = rs.getString("view1_img_path");
					sView1ImgPath =sView1ImgPath ==null?"":sView1ImgPath.trim();
					String sView1ImgType = rs.getString("view1_img_type");
					sView1ImgType =sView1ImgType ==null?"":sView1ImgType.trim();

					String sView2ImgPath = rs.getString("view2_img_path");
					sView2ImgPath =sView2ImgPath ==null?"":sView2ImgPath.trim();
					String sView2ImgType = rs.getString("view2_img_type");
					sView2ImgType =sView2ImgType ==null?"":sView2ImgType.trim();

					String sView3ImgPath = rs.getString("view3_img_path");
					sView3ImgPath =sView3ImgPath ==null?"":sView3ImgPath.trim();
					String sView3ImgType = rs.getString("view3_img_type");	
					sView3ImgType =sView3ImgType ==null?"":sView3ImgType.trim();

					String sSwfType = rs.getString("swf_type");	
					sSwfType =sSwfType ==null?"":sSwfType.trim();

					String sSwfPath = rs.getString("swf_path");	
					sSwfPath =sSwfPath ==null?"":sSwfPath.trim();

					String sIsAdvt = rs.getString("is_advt");	
					sIsAdvt =sIsAdvt ==null?"":sIsAdvt.trim();
					//To SWF file
					sMainSrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/CoverImages/";
					sMainSrcFileName = sOwnerId+sProdId+"main."+sMainImgType;
					sMainDestFolderAndFileName = "SkyBuyPics/CoverImages/"+sOwnerId+sProdId+"main."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);

					// System.out.println("sMainSrcPath "+sMainSrcPath);
					// System.out.println("sMainSrcFileName "+sMainSrcFileName);
					// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
					
					// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);

				}
			}
			// To get Vendor and Airline About Me files
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
					while(rs.next()){	
						bIsNewVersionFound=true;
						// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
						
	//					String sOwnerName = rs.getString("owner_name");
						String sOwnerId = rs.getString("owner_id");	
	//					String sOwnerType = rs.getString("owner_type");
						
						String sAboutMePath = rs.getString("about_me_path");
						sAboutMePath = sAboutMePath == null?"":sAboutMePath.trim();
						String sAboutMeType = rs.getString("about_me_type");
						sAboutMeType = sAboutMeType == null?"":sAboutMeType.trim();
											
						//To About Me SWF file
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sAboutMePath;
						sMainSrcFileName = sOwnerId+"."+sAboutMeType;
						sMainDestFolderAndFileName = sAboutMePath+"/"+sOwnerId+"."+sAboutMeType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
	
						// System.out.println("sMainSrcPath "+sMainSrcPath);
						// System.out.println("sMainSrcFileName "+sMainSrcFileName);
						// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
						
						// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
	
					}
				}
			}
			
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				int iIndex;
				if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
					while(rs.next()){	
						
						// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
						
						String sURL = rs.getString("URL");
						if(sURL!=null && !"".equalsIgnoreCase(sURL)){
							
							sURL = sURL == null?"":sURL.trim();
							sMainSrcPath =p_sCatalogueFolderPath+"/"+sURL;
							File fFile = new File(sMainSrcPath);
							if(fFile.exists()){
								bIsNewVersionFound=true;
								sMainSrcFileName = fFile.getName();
								sMainDestFolderAndFileName = sMainSrcFileName;
								iIndex = sMainSrcPath.lastIndexOf("/");
								sMainSrcPath = sMainSrcPath.substring(0, iIndex);
								oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
							
							}
						}
											
						// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
					}
				}
			}
			sDownloadCateDate = cstmt.getString(5);
			iUpdatedProdCount = cstmt.getInt(6);
			iUpdatedAirLogoCount = cstmt.getInt(7);
			iUpdatedReturnPolicy = cstmt.getInt(8);
			iPartnerPriorityUpdated = cstmt.getInt(9);
			if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
				// Airline Logo Path
				if((p_sLastDownloadedCateDt.trim().length()>0 && p_sLastDownloadedCateDt.equals("FullCatalogue")) || (iUpdatedAirLogoCount>0)){						
					bIsNewVersionFound = true;
					sAirLogoPath =p_sCatalogueFolderPath+"/SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/";
					sAirLogoFileName = p_sAirId+"_logo.jpg";
					sAirLogoDestFolderAndFileName = "SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/"+sAirLogoFileName;
					oGenerateCatalogueZip.addFile(sAirLogoPath, sAirLogoFileName, sAirLogoDestFolderAndFileName);
					
					sAirLogoIntroPath =p_sCatalogueFolderPath+"/SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/";
					sAirLogoIntroFileName = p_sAirId+"_logo_intro.jpg";
					sAirLogoIntroDestFolderAndFileName = "SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/"+sAirLogoIntroFileName;
					oGenerateCatalogueZip.addFile(sAirLogoIntroPath, sAirLogoIntroFileName, sAirLogoIntroDestFolderAndFileName);
				}
			}

			//if there is any product is deleted attach the xml file 
			if(iUpdatedProdCount>0 || iUpdatedReturnPolicy > 0 || iPartnerPriorityUpdated > 0)
				bIsNewVersionFound = true;
			
			ArrayList<UpdatedCatalogueDetailsVO> alCertAndNWPass = null;//UploadSkyBuyCatalogueDAO.getClientCertAndNWUserName(p_sLastDownloadedCateDt);
			
//			File fFolder = new File(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId);
			
		/*	if(!fFolder .exists())
				fFolder .mkdirs();*/
			// 	To get Account Information and Certificate Information
			/*for(UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO:alCertAndNWPass){
				
				if(oUpdatedCatalogueDetailsVO.getUploadType().equalsIgnoreCase("USERNAME")){
					sNetworkUserName = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkUsername(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					sNetworkPassword = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkPassword(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					
					sAccountInfo = sNetworkUserName+"|"+sNetworkPassword;
					hmReturn.put("AccountInfo", sAccountInfo);
				}
				if(oUpdatedCatalogueDetailsVO.getUploadType().equalsIgnoreCase("CLIENTCERT")){
					sClientCertPath = oUpdatedCatalogueDetailsVO.getClientCertPath();
					sClientCertPass = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getClientCertPassword(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					sClientCertPath = p_sCatalogueFolderPath +"/"+ sClientCertPath;
					File fSourceFile = new File(sClientCertPath);
					File fDestPath = new File(fFolder,fSourceFile.getName());
					
					Utils.copyfile(fSourceFile,fDestPath);
//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass/"+fSourceFile.getName());
					hmReturn.put("ClientCertPass", sClientCertPass);
					bIsFileUpdated = true;
				}
			}*/
			/*File fFolder1 = new File(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId);
			String files[] = fFolder1.list();
			if(files!=null && files.length>0){
				for(int i=0;i<files.length;i++) {
					oGenerateCatalogueZip.addFile(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId, files[i], "ClientPass_"+p_sDeviceId+"/"+files[i]);
				}
			}*/
			//Add eCatalogue xml file
			if(bIsNewVersionFound){					
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sE_CatalogueFileName, sE_CatalogueFileName);
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sCoverImageXML, sCoverImageXML);
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sAboutPartnerXML, sAboutPartnerXML);
			}
			if(bIsNewVersionFound || bIsFileUpdated) {
				oGenerateCatalogueZip.finish();	
			}
			hmReturn.put("DownloadDate", sDownloadCateDate);
			logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return hmReturn;
	}
	public static List<String> getAllAirlineCode() throws Exception {
		logger.info("EmailSchedulerDAO::getAllAirlineCode::ENTER");

		String sAirlineCode = null;
		List<String> alAirlineCode = new ArrayList<String>();
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_airline_code()}";		
		logger.debug("EmailSchedulerDAO::getAllAirlineCode::SQL QUERY "+sQry);		

		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				sAirlineCode = rs.getString("air_id");
				alAirlineCode.add(sAirlineCode);				
			}

		}catch(Exception e){
			e.printStackTrace();
			logger.error("EmailSchedulerDAO::getAllAirlineCode::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		logger.info("EmailSchedulerDAO::getAllAirlineCode::EXIT");
		return alAirlineCode;
	}
	public static Document createDocument()
    {
    	logger.info("GenerateCatalogue::createDocument:ENTER");	
        Document dom = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try
        {
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.newDocument();
        }
        catch(ParserConfigurationException pce)
        {
        	logger.error("GenerateCatalogue::createDocument:EXCEPTION "+pce.getMessage());
            // System.out.println((new StringBuilder("Error while trying to instantiate DocumentBuilder ")).append(pce).toString());
            System.exit(1);
        }
        logger.info("GenerateCatalogue::createDocument:EXIT");	
        return dom;
    }
	public static ArrayList generateCatalogueXML(String p_sDestPath,String p_sUserType,String p_sOwnerId,String p_sUserId,String p_sServerPath,String p_sProdId,String p_sCateId,String p_CatalogueType,String p_sDeviceId, String p_sSeqId)
	throws Exception {
		logger.info("GenerateCatalogue::generateCatalogueXML:ENTER");		
		ArrayList alCoverflow = null,alCoverflowProdIds= null,alPartnerDetails = null;
		Document dom = createDocument();
		Document domCover = createDocument();
		Document domPartner = createDocument();
		List<AdminReturnPolicyVO> alAdminReturnPolicyVO = getReturnPolicyMessages();
		HashMap hmProdDetails = getProductData(p_sUserType,p_sOwnerId,p_sProdId,p_sCateId,p_CatalogueType);
		alCoverflow = (ArrayList)hmProdDetails.get("Coverflow");
		hmProdDetails.remove("Coverflow");
		alPartnerDetails = (ArrayList)hmProdDetails.get("PartnerDetails");
		hmProdDetails.remove("PartnerDetails");
		createCatalogue(dom, hmProdDetails, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId,p_sProdId,p_sSeqId,alAdminReturnPolicyVO);
		alCoverflowProdIds = createCatalogueForCover(domCover, alCoverflow, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId);
		createCatalogueForPartnerDetails(domPartner, alPartnerDetails, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId);

		logger.info("GenerateCatalogue::generateCatalogueXML:Catalogue XML Generated file successfully.");	
		logger.info("GenerateCatalogue::generateCatalogueXML:EXIT");
		return alCoverflowProdIds;
	}
	private static List<AdminReturnPolicyVO> getReturnPolicyMessages() throws Exception {
		logger.info("GenerateCatalogue::getReturnPolicyMessages:ENTER");

		String sQuery = "{call usp_sbh_get_skybuy_disclaimer_message()}";
		List<AdminReturnPolicyVO> alAdminReturnPolicyVO = new ArrayList<AdminReturnPolicyVO>();
		AdminReturnPolicyVO oAdminReturnPolicyVO = null;
		CallableStatement cstmt = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			con = getSQL2005Connection();
			cstmt = con.prepareCall(sQuery);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oAdminReturnPolicyVO = new AdminReturnPolicyVO();
					oAdminReturnPolicyVO.setId(rs.getString("id"));
					oAdminReturnPolicyVO.setCode(rs.getString("code"));
					oAdminReturnPolicyVO.setText(rs.getString("text"));
					oAdminReturnPolicyVO.setStatus(rs.getString("status"));
					oAdminReturnPolicyVO.setComments(rs.getString("comments"));
					oAdminReturnPolicyVO.setCreateDate(rs.getString("sbh_create_dt"));
					oAdminReturnPolicyVO.setCreateId(rs.getString("sbh_create_id"));
					oAdminReturnPolicyVO.setCreateDate(rs.getString("sbh_update_dt"));
					oAdminReturnPolicyVO.setCreateId(rs.getString("sbh_update_id"));
					alAdminReturnPolicyVO.add(oAdminReturnPolicyVO);
				}
			}
		}catch(Exception e) {
			logger.info("GenerateCatalogue::getReturnPolicyMessages:EXCEPTION"+e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
			if(cstmt != null){
				cstmt.close();
			}
			if(con != null) {
				con.close();
			}
		}

		logger.info("GenerateCatalogue::getReturnPolicy:EXIT");	
		return alAdminReturnPolicyVO;
	}
	private static  HashMap getProductData(String p_sUserType,String p_sOwnerId,String p_sProdId,String p_Cate_Id,String p_CatalogueType)
	throws Exception {
		logger.info("GenerateCatalogue::getProductData:ENTER");	
		Connection con = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		List<ProductDetailsVO> alCoverflowProd = new ArrayList<ProductDetailsVO>();
		List<PartnerVO> alPartnerDetails = new ArrayList<PartnerVO>();
		String sProdDetailsQuery = "{call usp_sbh_get_prod_details(?,?,?)}";
		ProductDetailsVO oProductDetailsVO = null;
		HashMap hmProdDetails = new HashMap();
		ArrayList alProdDetails = null;
		String sDBData = null;
		PartnerVO oPartnerVO = null;
		try
		{
			con = getSQL2005Connection();
			cstmt = con.prepareCall(sProdDetailsQuery);
			cstmt.setString(1, p_sUserType);
			cstmt.setString(2, p_sOwnerId);
			cstmt.registerOutParameter(3, Types.VARCHAR);
			String sCateName;
			for(rs = cstmt.executeQuery(); rs.next(); hmProdDetails.put(sCateName, alProdDetails))
			{
				oProductDetailsVO = new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setSeqId(rs.getString("seq_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				sCateName = rs.getString("cate_name");
				oProductDetailsVO.setCateName(sCateName);
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));

				String sSize = rs.getString("size");
				sSize = sSize ==null?"":sSize.trim();
				oProductDetailsVO.setSize(sSize);

				String sColor = rs.getString("color");
				sColor = sColor ==null?"":sColor.trim();
				oProductDetailsVO.setColor(sColor);

				String sVendorPrice = rs.getString("vend_price");
				sVendorPrice =sVendorPrice==null?"0.00":sVendorPrice.trim();
				oProductDetailsVO.setVendPrice(sVendorPrice);

				String sSbhPrice=rs.getString("sbh_price");
				sSbhPrice = sSbhPrice ==null?sVendorPrice:sSbhPrice.trim();
				oProductDetailsVO.setSbhPrice(sSbhPrice);

				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
				oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));
				oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));
				oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));
				oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));
				oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));
				oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));

				sDBData = rs.getString("main_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();               
				oProductDetailsVO.setMainImgCap(sDBData);

				sDBData = rs.getString("view1_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView1ImgCap(sDBData);

				sDBData = rs.getString("view2_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView2ImgCap(sDBData);

				sDBData = rs.getString("view3_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView3ImgCap(sDBData);

				oProductDetailsVO.setReturnPolicy(rs.getString("return_policy"));

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
					if(p_sProdId.equals(oProductDetailsVO.getProdId()))
						oProductDetailsVO.setShowProd("Yes");
					else
						oProductDetailsVO.setShowProd("No");	
				}

				/*String sValidFromDate = rs.getString("valid_from_date");
	sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
	oProductDetailsVO.setValidFromDate(sValidFromDate);

	String sValidToDate = rs.getString("valid_to_date");
	sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
	oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sSwfPath = rs.getString("swf_path");
				sSwfPath = sSwfPath==null?"":sSwfPath.trim();				
				oProductDetailsVO.setSwfPath(sSwfPath);

				String sSwfType = rs.getString("swf_type");
				sSwfType = sSwfType==null?"":sSwfType.trim();				
				oProductDetailsVO.setSwfType(sSwfType);

				String sIsAdvt = rs.getString("is_advt");
				sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
				oProductDetailsVO.setIsAdvt(sIsAdvt);

				String sCFStatus = rs.getString("coverflow_status");
				sCFStatus = sCFStatus==null?"":sCFStatus.trim();				
				oProductDetailsVO.setCoverflowStatus(sCFStatus);

				String sProdSwfPath = rs.getString("prod_swf_path");
				sProdSwfPath = sProdSwfPath==null?"":sProdSwfPath.trim();				
				oProductDetailsVO.setSpecialProductPath(sProdSwfPath);

				String sProdSwfType = rs.getString("prod_swf_type");
				sProdSwfType = sProdSwfType==null?"":sProdSwfType.trim();				
				oProductDetailsVO.setSpecialProdType(sProdSwfType);

				String sIsSpecialProd = rs.getString("is_special_prod");
				sIsSpecialProd = sIsSpecialProd==null?"":sIsSpecialProd.trim();				
				oProductDetailsVO.setIsSpecialProd(sIsSpecialProd);

				alProdDetails = (ArrayList)hmProdDetails.get(sCateName);
				if(alProdDetails == null)
				{
					alProdDetails = new ArrayList();
					alProdDetails.add(oProductDetailsVO);
				} else
				{
					alProdDetails.add(oProductDetailsVO);
				}

			}


			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				while(rs.next()){
					oProductDetailsVO = new ProductDetailsVO();
					oProductDetailsVO.setProdId(rs.getString("prod_id"));
					oProductDetailsVO.setCateId(rs.getString("cate_id"));
					oProductDetailsVO.setSeqId(rs.getString("seq_id"));
					oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
					oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
					sCateName = rs.getString("cate_name");
					oProductDetailsVO.setCateName(sCateName);
					oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
					oProductDetailsVO.setProdCode(rs.getString("prod_code"));
					oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
					oProductDetailsVO.setProdTitle(rs.getString("prod_title"));
					oProductDetailsVO.setBrandName(rs.getString("brand_name"));
					oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
					oProductDetailsVO.setLongDesc(rs.getString("long_desc"));

					String sSize = rs.getString("size");
					sSize = sSize ==null?"":sSize.trim();
					oProductDetailsVO.setSize(sSize);

					String sColor = rs.getString("color");
					sColor = sColor ==null?"":sColor.trim();
					oProductDetailsVO.setColor(sColor);

					String sVendorPrice = rs.getString("vend_price");
					sVendorPrice =sVendorPrice==null?"0.00":sVendorPrice.trim();
					oProductDetailsVO.setVendPrice(sVendorPrice);

					String sSbhPrice=rs.getString("sbh_price");
					sSbhPrice = sSbhPrice ==null?sVendorPrice:sSbhPrice.trim();
					oProductDetailsVO.setSbhPrice(sSbhPrice);

					oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
					oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));
					oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));
					oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));
					oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));
					oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));
					oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));
					oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));

					sDBData = rs.getString("main_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();               
					oProductDetailsVO.setMainImgCap(sDBData);

					sDBData = rs.getString("view1_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView1ImgCap(sDBData);

					sDBData = rs.getString("view2_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView2ImgCap(sDBData);

					sDBData = rs.getString("view3_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView3ImgCap(sDBData);

					oProductDetailsVO.setReturnPolicy(rs.getString("return_policy"));

					if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
						if(p_sProdId.equals(oProductDetailsVO.getProdId()))
							oProductDetailsVO.setShowProd("Yes");
						else
							oProductDetailsVO.setShowProd("No");	
					}

					/*String sValidFromDate = rs.getString("valid_from_date");
		sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
		oProductDetailsVO.setValidFromDate(sValidFromDate);

		String sValidToDate = rs.getString("valid_to_date");
		sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
		oProductDetailsVO.setValidToDate(sValidToDate);*/

					String sInstructions = rs.getString("instructions");
					sInstructions = sInstructions==null?"":sInstructions.trim();				
					oProductDetailsVO.setInstructions(sInstructions);

					String sSwfPath = rs.getString("swf_path");
					sSwfPath = sSwfPath==null?"":sSwfPath.trim();				
					oProductDetailsVO.setSwfPath(sSwfPath);

					String sSwfType = rs.getString("swf_type");
					sSwfType = sSwfType==null?"":sSwfType.trim();				
					oProductDetailsVO.setSwfType(sSwfType);

					String sIsAdvt = rs.getString("is_advt");
					sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
					oProductDetailsVO.setIsAdvt(sIsAdvt);

					String sCFStatus = rs.getString("coverflow_status");
					sCFStatus = sCFStatus==null?"":sCFStatus.trim();				
					oProductDetailsVO.setCoverflowStatus(sCFStatus);

					alCoverflowProd.add(oProductDetailsVO);
				}

				if(hmProdDetails!=null){
					hmProdDetails.put("Coverflow", alCoverflowProd);
				}

			}
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();

				while(rs.next()){
					oPartnerVO = new PartnerVO();
					sDBData = rs.getString("about_me_path");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setAboutMePath(sDBData);

					sDBData = rs.getString("about_me_type");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setAboutMeType(sDBData);

					sDBData = rs.getString("owner_id");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerId(sDBData);

					sDBData = rs.getString("owner_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerName(sDBData);

					sDBData = rs.getString("owner_type");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerType(sDBData);


					alPartnerDetails.add(oPartnerVO);
				}

				if(hmProdDetails!=null){
					hmProdDetails.put("PartnerDetails", alPartnerDetails);
				}

			}

			String sCatalogueDate = cstmt.getString(3);
			sCatalogueDate = sCatalogueDate == null?"":sCatalogueDate.trim();
			// System.out.println("CatalogueDate"+sCatalogueDate);
			if(hmProdDetails!=null)
				hmProdDetails.put("CatalogueDate",sCatalogueDate);

			logger.info("GenerateCatalogue::getProductData:EXIT");	
		}
		catch(Exception e)
		{
			logger.error("GenerateCatalogue::getProductData:EXCEPTION "+e.getMessage());	
			// System.out.println((new StringBuilder("GenerateXML::loadData:Exception")).append(e.getMessage()).toString());
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return hmProdDetails;
	}

	private static  void createCatalogue(Document dom, HashMap hmProdDetails, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId, String p_sProdId, String p_sSeqId, List<AdminReturnPolicyVO> p_alAdminReturnPolicyVO)
	{
		logger.info("GenerateCatalogue::createCatalogue:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sShowActiveProduct = null;
		String sIsDemonstrationCatalogue = null;
		String sIsPreviewCatalogue = null;
		String sPartnerType = "";
		try{

			Element rootEle = dom.createElement("Categories");

			String sCatalogueDate = (String)hmProdDetails.get("CatalogueDate");
			rootEle.setAttribute("catalogueDate", sCatalogueDate);     
			hmProdDetails.remove("CatalogueDate");

			alCoverflow = (ArrayList)hmProdDetails.get("Coverflow");
			hmProdDetails.remove("Coverflow");
			//String  sImageViewPath= "";

			/*if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
sImageViewPath = System.getProperty("ImageViewPath").trim();
}		  
sImageViewPath = (sImageViewPath ==null)?"":sImageViewPath.trim();*/

			if(p_sUserType!=null && p_sUserType.trim().length()>=0) {
				if(p_sUserType.equalsIgnoreCase("CLIENT")) {  

					String sAirLogoPath =/*sImageViewPath+*/"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo.jpg";
					String sAirLogoIntroPath =/*sImageViewPath+*/"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo_intro.jpg";
					rootEle.setAttribute("AirLogoPath",sAirLogoPath);   
					rootEle.setAttribute("AirLogoIntroPath",sAirLogoIntroPath);
					String sE_CatalogueFileName = "eCatalogue";
					String sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml";
					String sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml";
					rootEle.setAttribute("PartnerDetails" ,sAboutPartnerXML);
					rootEle.setAttribute("CoverImageDetails" ,sCoverImageXML);

				}else if(p_sUserType.equalsIgnoreCase("AIRLINE")) {
					String sImageViewPath = "http://skybuyhigh.com/static/";	
					sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
					String sAirLogoPath =sImageViewPath+"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo.jpg";
					String sAirLogoIntroPath =sImageViewPath+"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo_intro.jpg";
					rootEle.setAttribute("AirLogoPath",sAirLogoPath);   
					rootEle.setAttribute("AirLogoIntroPath",sAirLogoIntroPath);
				}
			}
			if(!"CLIENT".equalsIgnoreCase(p_sUserType)) {
				if("PRODCATALOGUE".equalsIgnoreCase(p_CatalogueType)){
					rootEle.setAttribute("activeCategory" ,p_sSeqId);
					rootEle.setAttribute("activeProductId" ,p_sProdId);
					rootEle.setAttribute("activeOwnerId" ,p_sOwnerId);
					sShowActiveProduct = "Y";
					sIsDemonstrationCatalogue = "N";
					sIsPreviewCatalogue = "Y";
				}else if("PREVIEWCATALOGUE".equalsIgnoreCase(p_CatalogueType)) {
					sShowActiveProduct = "N";
					sIsDemonstrationCatalogue = "N";
					sIsPreviewCatalogue = "Y";
					if("AIRLINE".equalsIgnoreCase(p_sUserType)) {
						sPartnerType = "airline";
					}else if("VENDOR".equalsIgnoreCase(p_sUserType)) {
						sPartnerType = "vendor";
					}
				}else if("DEMONSTRATIONCATALOGUE".equalsIgnoreCase(p_CatalogueType)){
					sShowActiveProduct = "N";
					sIsDemonstrationCatalogue = "Y";
					sIsPreviewCatalogue = "N";
				}
				rootEle.setAttribute("showActiveProduct" ,sShowActiveProduct);
				rootEle.setAttribute("isDemonstrationCatalogue" ,sIsDemonstrationCatalogue);
				rootEle.setAttribute("isPreviewCatalogue" ,sIsPreviewCatalogue);
				rootEle.setAttribute("partnerType" ,sPartnerType);
			}
			dom.appendChild(rootEle);
			/*Element categoriesEle = dom.createElement("Categories");
rootEle.appendChild(categoriesEle);
Element coverflowEle = dom.createElement("Coverflow");
rootEle.appendChild(coverflowEle);*/

			Element oMessages;
			Element oMessage;
			/*
			 * NEW CHILD NODE -- RETURN POLICY.
			 * Used to add a new child node name ReturnPolicy and to add the Disclaimer
			 */
			if("CLIENT".equalsIgnoreCase(p_sUserType) || "DEMONSTRATIONCATALOGUE".equalsIgnoreCase(p_CatalogueType)) {
				oMessages = dom.createElement("Messages");
				CDATASection cDisclaimer;
				for(AdminReturnPolicyVO oAdminReturnPolicyVO:p_alAdminReturnPolicyVO) {
					oMessage = dom.createElement("Message");
					oMessage.setAttribute("type", oAdminReturnPolicyVO.getCode());
					//	disclaimer = dom.createTextNode(oAdminReturnPolicyVO.getText());
					cDisclaimer = dom.createCDATASection(oAdminReturnPolicyVO.getText());
					oMessage.appendChild(cDisclaimer);
					oMessages.appendChild(oMessage);
				}
				rootEle.appendChild(oMessages);
			}
			Element cateEle;
			for(Iterator itr = hmProdDetails.entrySet().iterator(); itr.hasNext(); rootEle.appendChild(cateEle))
			{
				java.util.Map.Entry e = (java.util.Map.Entry)itr.next();
				alProdInfo = (ArrayList)e.getValue();
				cateEle = dom.createElement("Category");
				Element prodEle;
				for(Iterator iterator = alProdInfo.iterator(); iterator.hasNext(); cateEle.appendChild(prodEle))
				{
					ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
					prodEle = createProdElement(dom, oProductDetailsVO,p_sUserType,p_CatalogueType);
					cateEle.setAttribute("id", oProductDetailsVO.getCateId());
					cateEle.setAttribute("seqId", oProductDetailsVO.getSeqId());

					//For generating Product catalogue ---- 
					/*if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
    	if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
    		cateEle.setAttribute("isActive", "Y");
    	else
    		cateEle.setAttribute("isActive", "N");
    }*/
				}
			}

			/*cateEle = dom.createElement("Category");
Element prodEle;
for(Iterator iterator = alCoverflow.iterator(); iterator.hasNext(); cateEle.appendChild(prodEle))
{
ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
prodEle = createProdElement(dom, oProductDetailsVO,p_sUserType,p_CatalogueType);
cateEle.setAttribute("id", oProductDetailsVO.getCateId());
cateEle.setAttribute("seqId", oProductDetailsVO.getSeqId());

//For generating Product catalogue ---- 
if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
	if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
		cateEle.setAttribute("isActive", "Y");
	else
		cateEle.setAttribute("isActive", "N");
}
}
coverflowEle.appendChild(cateEle);*/

			printToFile(dom, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);
			logger.info("GenerateCatalogue::createCatalogue:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogue:EXCEPTION "+e.getMessage());	
		}
	}

	private static  ArrayList createCatalogueForCover(Document domCover, ArrayList alCoverflowProd, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::createCatalogue:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sImageUploadPath = null;
		String sImageURL = null,sOriginalImageURL = null,sImageFullURL = null;
		String sImageViewPath = "";
		ArrayList alCoverflowProdIds = new ArrayList();
		int iCoverflowCount = 0;
		try{
			sImageUploadPath = "C:/StaticContent";
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			Element rootEle = domCover.createElement("Coverflow");
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
				sImageViewPath = "http://skybuyhigh.com/static/";	
				sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			}
			domCover.appendChild(rootEle);

			Element cateEle;

			for(Iterator iterator = alCoverflowProd.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
			{

				cateEle = domCover.createElement("Image");
				ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
				sImageURL =sImageViewPath+"SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
				sImageFullURL = sImageUploadPath+ "/SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
				alCoverflowProdIds.add(oProductDetailsVO.getProdId());
				//prodEle = createProdElement(domCover, oProductDetailsVO,p_sUserType,p_CatalogueType);
				cateEle.setAttribute("id", oProductDetailsVO.getCateId());
				cateEle.setAttribute("SeqId", oProductDetailsVO.getSeqId());
				cateEle.setAttribute("ProdId", oProductDetailsVO.getProdId());
				cateEle.setAttribute("coverSeqId", oProductDetailsVO.getCoverflowStatus());
				cateEle.setAttribute("ImgURL", sImageURL);
				//For generating Product catalogue ---- 
				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
					if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
						cateEle.setAttribute("isActive", "Y");
					else
						cateEle.setAttribute("isActive", "N");
				}


				File f = new File(sImageFullURL);
				if(!f.exists()){
					sOriginalImageURL = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
					ImageScale.createCoverflowImage(sOriginalImageURL, sImageUploadPath);
				}
				iCoverflowCount++;

			}
			while(iCoverflowCount > 0 && iCoverflowCount < 5){
				for(Iterator iterator = alCoverflowProd.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
				{
					if(iCoverflowCount == 5){
						break;
					}
					cateEle = domCover.createElement("Image");
					ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
					sImageURL =sImageViewPath+"SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main.jpg";
					sImageFullURL = sImageUploadPath+ "/SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main.jpg";
					alCoverflowProdIds.add(oProductDetailsVO.getProdId());
					//prodEle = createProdElement(domCover, oProductDetailsVO,p_sUserType,p_CatalogueType);
					cateEle.setAttribute("id", oProductDetailsVO.getCateId());
					cateEle.setAttribute("SeqId", oProductDetailsVO.getSeqId());
					cateEle.setAttribute("ProdId", oProductDetailsVO.getProdId());
					cateEle.setAttribute("coverSeqId", oProductDetailsVO.getCoverflowStatus());
					cateEle.setAttribute("ImgURL", sImageURL);
					//For generating Product catalogue ---- 
					if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
						if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
							cateEle.setAttribute("isActive", "Y");
						else
							cateEle.setAttribute("isActive", "N");
					}


					File f = new File(sImageFullURL);
					if(!f.exists()){
						sOriginalImageURL = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+"jpg";

						ImageScale.createCoverflowImage(sOriginalImageURL, sImageUploadPath);
					}
					iCoverflowCount++;

				}

			}



			printToFileForCover(domCover, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);
			logger.info("GenerateCatalogue::createCatalogue:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogue:EXCEPTION "+e.getMessage());	
		}

		return alCoverflowProdIds;
	}
	private static  void createCatalogueForPartnerDetails(Document domPartner, ArrayList alPartnerDetails, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::createCatalogueForPartnerDetails:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sImageUploadPath = null;
		String sAboutMeURL = null,sOriginalImageURL = null,sImageFullURL = null,sImageViewPath = "";


		try{

			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
				sImageViewPath = "http://skybuyhigh.com/static/";	
				sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			}
			sImageUploadPath = "C:/StaticContent";
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			Element rootEle = domPartner.createElement("PartnerDetails");

			domPartner.appendChild(rootEle);

			Element cateEle;

			for(Iterator iterator = alPartnerDetails.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
			{

				cateEle = domPartner.createElement("Owner");
				PartnerVO oPartnerVO = (PartnerVO)iterator.next();

				if(oPartnerVO.getAboutMePath()!=null && !oPartnerVO.getAboutMePath().equalsIgnoreCase("")){
					sAboutMeURL =sImageViewPath+oPartnerVO.getAboutMePath()+"/"+oPartnerVO.getOwnerId()+"."+oPartnerVO.getAboutMeType();
				}else{
					sAboutMeURL = "";
				}
				cateEle.setAttribute("id", oPartnerVO.getOwnerId());
				cateEle.setAttribute("type", oPartnerVO.getOwnerType());
				cateEle.setAttribute("name", oPartnerVO.getOwnerName());
				cateEle.setAttribute("aboutMeURL", sAboutMeURL);
			}



			printToFileForPartner(domPartner, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);
			logger.info("GenerateCatalogue::createCatalogueForPartnerDetails:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogueForPartnerDetails:EXCEPTION "+e.getMessage());	
		}

	}
	private static  Element createProdElement(Document dom, ProductDetailsVO oProductDetailsVO,String p_sUserType,String p_CatalogueType)
	{
		logger.info("GenerateCatalogue::createProdElement:ENTER");		
		String sView1ImageURLPath="",sView1ThumbURLPath = "",sView1MedViewPath = "",sView1LargeViewPath ="";
		String sView3MedViewPath ="",sView3LargeViewPath="",sView3ThumbURLPath="",sView3ImageURLPath="";
		String sView2LargeViewPath="",sView2ThumbURLPath="",sView2MedViewPath="",sView2ImageURLPath="";
		String sImageViewPath ="",sOriginalImageURLPath = "";
		String sSbhPrice =null;

		if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
			sImageViewPath = "http://skybuyhigh.com/static/";	    		
		}

		Element prodEle = dom.createElement("Product");
		prodEle.setAttribute("OwnerId", oProductDetailsVO.getOwnerId());
		prodEle.setAttribute("Adv", oProductDetailsVO.getIsAdvt());
		if(oProductDetailsVO.getIsAdvt().equalsIgnoreCase("Y")){
			prodEle.setAttribute("SwfPath", sImageViewPath+oProductDetailsVO.getSwfPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSwfType());
		}else if(oProductDetailsVO.getIsSpecialProd().equalsIgnoreCase("Y")){
			prodEle.setAttribute("SwfPath", sImageViewPath+oProductDetailsVO.getSpecialProductPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSpecialProdType());
		}else{
			prodEle.setAttribute("SwfPath", "");
		}

		Element prodIdEle = dom.createElement("ProductId");
		Text prodIdText = dom.createTextNode(oProductDetailsVO.getProdId());
		prodIdEle.appendChild(prodIdText);
		prodEle.appendChild(prodIdEle);

		Element prodCodeEle = dom.createElement("ProductCode");
		Text prodCodeText = dom.createTextNode(oProductDetailsVO.getProdCode());
		prodCodeEle.appendChild(prodCodeText);
		prodEle.appendChild(prodCodeEle);

		Element brandEle = dom.createElement("BRAND");
		Text brandText = dom.createTextNode(oProductDetailsVO.getBrandName());
		brandEle.appendChild(brandText);
		prodEle.appendChild(brandEle);

		Element titleEle = dom.createElement("TITLE");
		Text titleText = dom.createTextNode(oProductDetailsVO.getProdTitle());
		titleEle.appendChild(titleText);
		prodEle.appendChild(titleEle);

		/*Element shortDescEle = dom.createElement("ShortDiscription");
Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
shortDescEle.appendChild(shortDescText);
prodEle.appendChild(shortDescEle);

Element longDescEle = dom.createElement("LongDiscription");
Element htmlLongEle = dom.createElement("html");
Text longDescText = dom.createTextNode(oProductDetailsVO.getLongDesc());
htmlLongEle.appendChild(longDescText);
longDescEle.appendChild(htmlLongEle);
prodEle.appendChild(longDescEle);*/

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element shortDescEle = dom.createElement("ShortDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cShortDesc = dom.createCDATASection(oProductDetailsVO.getShortDesc());
		shortDescEle.appendChild(cShortDesc);
		prodEle.appendChild(shortDescEle);

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element longDescEle = dom.createElement("LongDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cLongDesc = dom.createCDATASection(oProductDetailsVO.getLongDesc());
		longDescEle.appendChild(cLongDesc);
		prodEle.appendChild(longDescEle);

		Element sizeEle = dom.createElement("size");
		Text sizeText = dom.createTextNode(oProductDetailsVO.getSize());
		sizeEle.appendChild(sizeText);
		prodEle.appendChild(sizeEle);

		Element colorEle = dom.createElement("color");
		Text colorText = dom.createTextNode(oProductDetailsVO.getColor());
		colorEle.appendChild(colorText);
		prodEle.appendChild(colorEle);

		/*Element captionEle = dom.createElement("Caption");
prodEle.appendChild(captionEle);*/

		Element priceEle = dom.createElement("Price");
		Text priceText;
		if(p_sUserType!=null && (p_sUserType.equalsIgnoreCase("ADMIN")|| p_sUserType.equalsIgnoreCase("CLIENT"))){
			priceText = dom.createTextNode("$"+oProductDetailsVO.getVendPrice());
		}else{
			priceText = dom.createTextNode("");
		}
		priceEle.appendChild(priceText);
		prodEle.appendChild(priceEle);


		Element skyBuyPriceEle = dom.createElement("SkyBuyPrice");
		sSbhPrice ="$"+oProductDetailsVO.getSbhPrice();
		/*  if(p_sUserType!=null && (p_sUserType.equalsIgnoreCase("ADMIN")|| p_sUserType.equalsIgnoreCase("CLIENT")))
sSbhPrice ="$"+oProductDetailsVO.getSbhPrice();
else

sSbhPrice ="$"+oProductDetailsVO.getVendPrice();*/
		Text skyBuyPriceText = dom.createTextNode(sSbhPrice);
		skyBuyPriceEle.appendChild(skyBuyPriceText);
		prodEle.appendChild(skyBuyPriceEle);

		// Starts Main Img Url // 
		Element mainImageURLEle = dom.createElement("MainImageURL");

		//Adding attribute for ConverFlowPath,Caption,originalImgpath
		String sConverFlowImageURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/CoverFlow/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getMainImgType();
		sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
		mainImageURLEle.setAttribute("CoverFlowImgPath", sConverFlowImageURLPath);
		mainImageURLEle.setAttribute("Caption", oProductDetailsVO.getMainImgCap());        
		mainImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);

		String sMainImageURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getMainImgType();
		Text mainImageURLText = dom.createTextNode(sMainImageURLPath);
		mainImageURLEle.appendChild(mainImageURLText);
		prodEle.appendChild(mainImageURLEle);

		Element mainThumbURLEle = dom.createElement("MainThumbURL");
		String sMainThumbURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType();
		Text mainThumbURLText = dom.createTextNode(sMainThumbURLPath);
		mainThumbURLEle.appendChild(mainThumbURLText);
		prodEle.appendChild(mainThumbURLEle);

		Element mainMedViewEle = dom.createElement("MainMedView");
		String sMainMedViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getMainImgType();
		Text mainMedViewText = dom.createTextNode(sMainMedViewPath);
		mainMedViewEle.appendChild(mainMedViewText);
		prodEle.appendChild(mainMedViewEle);

		Element mainLargeViewEle = dom.createElement("MainLargeView");
		String sMainLargeViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getMainImgType();
		Text mainLargeViewText = dom.createTextNode(sMainLargeViewPath);
		mainLargeViewEle.appendChild(mainLargeViewText);
		prodEle.appendChild(mainLargeViewEle);

		// Ends Main Img Url // 

		// Starts View1 Img Url // 

		Element view1ImageURLEle = dom.createElement("ViewOneImageURL");

		//Adding attribute for Caption,originalImgpath
		sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view1."+oProductDetailsVO.getView1ImgType();
		view1ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView1ImgCap());        
		view1ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);

		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1ImageURLPath = sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView1ImgType();
		else
			sView1ImageURLPath="";
		Text view1ImageURLText = dom.createTextNode(sView1ImageURLPath);
		view1ImageURLEle.appendChild(view1ImageURLText);
		prodEle.appendChild(view1ImageURLEle);

		Element view1ThumbURLEle = dom.createElement("ViewOneThumbURL");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1ThumbURLPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType();
		else
			sView1ThumbURLPath = "";
		Text view1ThumbURLText = dom.createTextNode(sView1ThumbURLPath);
		view1ThumbURLEle.appendChild(view1ThumbURLText);
		prodEle.appendChild(view1ThumbURLEle);

		Element view1MedViewEle = dom.createElement("ViewOneMedView");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1MedViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView1ImgType();
		else
			sView1MedViewPath = "";
		Text view1MedViewText = dom.createTextNode(sView1MedViewPath);
		view1MedViewEle.appendChild(view1MedViewText);
		prodEle.appendChild(view1MedViewEle);

		Element view1LargeViewEle = dom.createElement("ViewOneLargeView");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1LargeViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView1ImgType();
		else
			sView1LargeViewPath ="";
		Text view1LargeViewText = dom.createTextNode(sView1LargeViewPath);
		view1LargeViewEle.appendChild(view1LargeViewText);
		prodEle.appendChild(view1LargeViewEle);

		// Ends View1 Img Url // 


		// Starts View2 Img Url // 
		Element view2ImageURLEle = dom.createElement("ViewTwoImageURL");

		//Adding attribute for Caption,originalImgpath
		sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view2."+oProductDetailsVO.getView2ImgType();
		view2ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView2ImgCap());        
		view2ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);

		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2ImageURLPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView2ImgType();
		else
			sView2ImageURLPath ="";
		Text view2ImageURLText = dom.createTextNode(sView2ImageURLPath);
		view2ImageURLEle.appendChild(view2ImageURLText);
		prodEle.appendChild(view2ImageURLEle);

		Element view2ThumbURLEle = dom.createElement("ViewTwoThumbURL");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2ThumbURLPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType();
		else
			sView2ThumbURLPath = "";
		Text view2ThumbURLText = dom.createTextNode(sView2ThumbURLPath);
		view2ThumbURLEle.appendChild(view2ThumbURLText);
		prodEle.appendChild(view2ThumbURLEle);

		Element view2MedViewEle = dom.createElement("ViewTwoMedView");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2MedViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView2ImgType();
		else
			sView2MedViewPath = "";
		Text view2MedViewText = dom.createTextNode(sView2MedViewPath);
		view2MedViewEle.appendChild(view2MedViewText);
		prodEle.appendChild(view2MedViewEle);                 


		Element view2LargeViewEle = dom.createElement("ViewTwoLargeView");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2LargeViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView2ImgType();
		else
			sView2LargeViewPath ="";
		Text view2LargeViewText = dom.createTextNode(sView2LargeViewPath);
		view2LargeViewEle.appendChild(view2LargeViewText);
		prodEle.appendChild(view2LargeViewEle);

		// Ends View2 Img Url // 

		// Starts View3 Img Url // 
		Element view3ImageURLEle = dom.createElement("ViewThreeImageURL");

		//Adding attribute for Caption,originalImgpath
		sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view3."+oProductDetailsVO.getView3ImgType();
		view3ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView3ImgCap());        
		view3ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);

		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3ImageURLPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView3ImgType();
		else
			sView3ImageURLPath="";
		Text view3ImageURLText = dom.createTextNode(sView3ImageURLPath);
		view3ImageURLEle.appendChild(view3ImageURLText);
		prodEle.appendChild(view3ImageURLEle);

		Element view3ThumbURLEle = dom.createElement("ViewThreeThumbURL");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3ThumbURLPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType();
		else
			sView3ThumbURLPath="";
		Text view3ThumbURLText = dom.createTextNode(sView3ThumbURLPath);
		view3ThumbURLEle.appendChild(view3ThumbURLText);
		prodEle.appendChild(view3ThumbURLEle);

		Element view3MedViewEle = dom.createElement("ViewThreeMedView");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3MedViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView3ImgType();
		else
			sView3MedViewPath ="";
		Text view3MedViewText = dom.createTextNode(sView3MedViewPath);
		view3MedViewEle.appendChild(view3MedViewText);
		prodEle.appendChild(view3MedViewEle);

		Element view3LargeViewEle = dom.createElement("ViewThreeLargeView");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3LargeViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView3ImgType();
		else
			sView3LargeViewPath = "";

		Text view3LargeViewText = dom.createTextNode(sView3LargeViewPath);
		view3LargeViewEle.appendChild(view3LargeViewText);
		prodEle.appendChild(view3LargeViewEle);

		// Ends View3 Img Url // 


		// if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
		Element returnPolicyEle = dom.createElement("ReturnPolicy");
		Text returnPolicyText = dom.createTextNode(oProductDetailsVO.getReturnPolicy());
		returnPolicyEle.appendChild(returnPolicyText);
		prodEle.appendChild(returnPolicyEle);
		// }

		// add valid from date and to date and instruction

		Element instructionEle = dom.createElement("Instruction");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cInstruction= dom.createCDATASection(oProductDetailsVO.getInstructions());
		instructionEle.appendChild(cInstruction);
		prodEle.appendChild(instructionEle);

		/* Element validFromEle = dom.createElement("ValidFrom");
Text validFromText = dom.createTextNode(oProductDetailsVO.getValidFromDate());
validFromEle.appendChild(validFromText);
prodEle.appendChild(validFromEle);


Element validToEle = dom.createElement("ValidTo");
Text validToText = dom.createTextNode(oProductDetailsVO.getValidToDate());
validToEle.appendChild(validToText);
prodEle.appendChild(validToEle);*/

		/*if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
Element showProdEle = dom.createElement("ShowProd");
Text showProdText = dom.createTextNode(oProductDetailsVO.getShowProd());
showProdEle.appendChild(showProdText);
prodEle.appendChild(showProdEle);
}*/
		logger.info("GenerateCatalogue::createProdElement:EXIT");		
		return prodEle;
	}

	private static  void printToFile(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFile:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = "eCatalogue";
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFile:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFile:EXIT");	
	}

	private static  void printToFileForCover(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFileForCover:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = "eCatalogue";
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_cover_"+"Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_cover_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_cover"+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFileForCover:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFileForCover:EXIT");	
	}

	private static  void printToFileForPartner(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFileForPartner:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = "eCatalogue";
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_partner_"+"_Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_partner_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_partner"+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFileForPartner:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFileForPartner:EXIT");	
	}
	public static Connection getSQL2005Connection() throws Exception {

		Connection con = null;
		InitialContext ctx = null;
		try {
			if (ctx == null)
				ctx = new InitialContext();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(
					"jdbc:sqlserver://localhost;databaseName=skybuydata",
					"SA", "SkyBuy01");
			//		if (con != null)
			// System.out.println("Connected");

		} catch (Exception e) {
			System.err.println("getSQL2005Connection Error is " + e.toString());
		}

		return con;
	}
}

