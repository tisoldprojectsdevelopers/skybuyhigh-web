package com.sbh.test;
import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class RSAUtil {
	 private  static final String ALGORITHM = "RSA";
	 public static byte[] encrypt(byte[] text, PublicKey key) throws Exception
	    {
//		 	Security.addProvider();
	        byte[] cipherText = null;
	        try
	        {
	            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	            cipher.init(Cipher.ENCRYPT_MODE, key);
	            cipherText = cipher.doFinal(text);
	        }
	        catch (Exception e)
	        {
	        	throw e;
	        }
	        return cipherText;
	    }

	   
	    public static String encrypt(String text, PublicKey key) throws Exception
	    {
	        String encryptedText;
	        try
	        {
	            byte[] cipherText = encrypt(text.getBytes("UTF8"),key);
	            encryptedText = encodeBASE64(cipherText);
	        }
	        catch (Exception e)
	        {
	             throw e;
	        }
	        return encryptedText;
	    }

	   
	    public static byte[] decrypt(byte[] text, PrivateKey key) throws Exception
	    {
	    	//Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	        byte[] dectyptedText = null;
	        try
	        {
	        	Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	            cipher.init(Cipher.DECRYPT_MODE, key);
	            dectyptedText = cipher.doFinal(text);
	        }
	        catch (Exception e)
	        {
	            throw e;
	        }
	        return dectyptedText;

	    }

	    
	    public static String decrypt(String text, PrivateKey key) throws Exception
	    {
	        String result;
	        try
	        {
	        	byte [] byteText =decodeBASE64(text);
	        	//String byteString = new String(byteText, "ISO-8859-1");
	        	
	           byte[] dectyptedText = decrypt(byteText,key);
	           //System.out.println("Base 64 decoded text : " + new String(decodeBASE64(text)));
	            result = new String(dectyptedText, "UTF8");
	        }
	        catch (Exception e)
	        {
	            throw e;
	        }
	        return result;

	    }

	    public static String getKeyAsString(Key key)
	    {
	        byte[] keyBytes = key.getEncoded();
	        BASE64Encoder b64 = new BASE64Encoder();
	        return b64.encode(keyBytes);
	    }

	   
	    public static PrivateKey getPrivateKeyFromString(String key) throws Exception
	    {
	        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
	        keyFactory.getProvider().toString();
	        BASE64Decoder b64 = new BASE64Decoder();
	        EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(b64.decodeBuffer(key));
	        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	        return privateKey;
	    }

	  
	    public static PublicKey getPublicKeyFromString(String key) throws Exception
	    {
	        BASE64Decoder b64 = new BASE64Decoder();
	        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
	        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(b64.decodeBuffer(key));
	        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
	        return publicKey;
	    }
	    private static String encodeBASE64(byte[] bytes)
	    {
	        BASE64Encoder b64 = new BASE64Encoder();
	        return b64.encode(bytes);
	    }
	    private static byte[] decodeBASE64(String text) throws IOException
	    {
	        BASE64Decoder b64 = new BASE64Decoder();
	        return b64.decodeBuffer(text);
	    }
}
