package com.sbh.test;

	import java.io.BufferedInputStream ;
	import java.io.BufferedOutputStream ;
	import java.io.FileInputStream ;
	import java.io.FileOutputStream ;

import java.security.Key;
	import java.security.KeyStore ;
import java.security.PrivateKey;
	import java.security.PublicKey ;

	import java.security.cert.Certificate ;

import javax.crypto.Cipher ;

	public class RSAEncDecUsingKeyStore
	{
	    public static void main ( String[] args )
	        throws Exception
	    {
	        
	        String keystorefile        = "/Users/thapovan/Desktop/RSA/keystore.jks" ;
	        String storepass        = "skybuy" ;
	        String alias            = "skybuyrsakeypair";
	        KeyStore keystore = KeyStore.getInstance("JKS");

	     keystore.load ( new FileInputStream ( keystorefile ),
	storepass.toCharArray() ) ;

	        Certificate certificate = keystore.getCertificate (alias) ;
	        Key key=keystore.getKey(alias, storepass.toCharArray());
	        PublicKey publicKey = (PublicKey) certificate.getPublicKey () ;
	        System.out.println("Public Key base64 = \n"+ (new sun.misc.BASE64Encoder ()).encode(publicKey.getEncoded()));
	        PrivateKey privateKey=(PrivateKey)key;
	        String enc1=RSAUtil.encrypt("THe QuiCK bRoWN foX juMPs ovER ThE LaZY DOg.",publicKey);
	   	    //String enc1="bGHYwzJSHzHjx3zQNdPTL7akf7NRdmhLNnHXoWvvGzLZ+1pKFdsZ+bjRJHwa86NYEh4E6zmhQN2hoxmm21eNcBTM45tpDw8DyLgc4r9ut+mMwNqRvT8rRWoK4pqFfNMJPw9jZSVWWxZo5VFguvdRxtxJ43tXsRYkBOh+QAj30sDzLsErazEz/ppk+re6qyqpny6JmA1OxWAsxeAZYO5bQvITJBRz+Y2VLwZlsCWSvx1hgn7bmyjcU4jXWjfBu86JTBxvs98SA4NdXor62hB3PuEVBdpk6sRKqCqsqpOe2KFR7/QJaVLj9SPLTpr0RrnWDGr0+mgRU6Bc1ycztHk/iA==";
	        String iphoneEnc = "HFltFIfPLXiEkc3lhOOflPQ1QXSUrPAi1dZZPcWaY0wk1DD6P1aCT4668frOJBS0D+FETGpxXJZq+wORadzDt2EUkl8mlVnu2WeP9SwGMrIbCrgTwZFl6HHL5qGFnAX5rL3ZN0oiHcvp6xvmM0A2qctWTH8CJNvbTWtnPDGucv420qxR4BqXRI90ZKh9nXiqH0kn6iFPGpUB2W66IPNk1TDrxiuVODOm1oORk+f32H0jeWs+jsNiTKo8pLfJpIWpZCE3rgbZ0iXLy7VZB2qdY1yiTAszTTHEyLKQxH6hkWoukw+6nVtIRQyIsSlISIa1AelXgxA4JaVMVZODxGgxiQ==";
	        String dec1=RSAUtil.decrypt(iphoneEnc,privateKey);
	   	    System.out.println("enc1 : \n"+enc1);
	   	    System.out.print("\n dec: \n"+dec1);
	   	 //String dec=RSAUtil.decrypt(enc,privateKey);
	   	// System.out.println("dec : \n "+dec);
	   	// System.out.println("dec1 : \n "+dec1);
	       // System.out.println("Public Key :\n "+publicKey);
	        //System.out.println("Private Key : \n"+privateKey);
	      
	    }
	}

