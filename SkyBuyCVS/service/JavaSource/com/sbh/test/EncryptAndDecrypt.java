package com.sbh.test;


import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;

import com.sbh.util.Utils;

/**
 * @author nbvk
 *
 */
public class EncryptAndDecrypt {
	
	public void encryptAndDecryptFirstData() throws Exception {
		String sPublicKey = "";
		String sPrivateKey = "";
		String Client_Cert_Path = "";
		String password = "";
		String StoreNo = "";
		String Host = "";
		String PortNo = "";
		String sUpdateQuery="";
		
		Connection con=null;		
		Statement cstmt=null;
		ResultSet rs=null;
		PrivateKey oOldPrivateKey = null;
		PublicKey oNewPublicKey = null;
		List<String> alQuery = new ArrayList<String>();
		String sQry="select CONVERT(varchar(max),client_cert_path) AS client_cert_path,CONVERT(varchar(max),password) As password,CONVERT(varchar(max),store_no)As store_no,CONVERT(varchar(max),host)as host,CONVERT(varchar(max),port)as port,CONVERT(varchar(max), checksum_one) As checksum_one,CAST(checksum_two AS varchar(max)) As checksum_two from sbh_merchant_info where status='A'";

		try{
			con = getSQL2005Connection();
			cstmt=con.createStatement();
			rs=cstmt.executeQuery(sQry);
			if(rs != null) {
				while(rs.next()) {
					sPublicKey = rs.getString("checksum_two");
					sPrivateKey = rs.getString("checksum_one");
					Client_Cert_Path = rs.getString("client_cert_path");
					password = rs.getString("password");
					StoreNo = rs.getString("store_no");
					Host = rs.getString("host");
					PortNo = rs.getString("port");
//					String enc="BrHLrgDBAJztyH4mYDRhpJzOzevuKygTpqgLXtm7gLjCg2JnTyoWcwg4H/ZR5+FvCmypquvbfvqrmI0IvyQuW5M6O13i+nNKk3EMJ/ZyBE7ce9OaQreBb1ZV6erREgiZ3PLp2SbBTaWjiaso2OLD88dYBqFgpWmCJiwvK805/u+uD91Sw9gYSINXBgISoZF+h2iKhObH6kgov3hrMqS6Wztl9y6jEk2DYX1BWdTtPzesV/rap9gCJYMOyJgD+Oy4Hco4Zd1xlHzRq10WeNU6YMmFHMVGBD7eOKMVQ4XMC9Gm3D3+zbfVlPhKVd9eBDH8HvLb48vkXHo43XRzc7/qAJH14G2JO42osW7jhygLv7kTkvFM+yIgQayJnw5SULo0fdYbcdjl";
					String enc="h2fMzUrFlc//3F9Uux6knH7ojmUPowyQ/kGmqvRFAmEBHk0nen6W9C0FdaNO5jPC5mpkudsPnWyNRpZJkCJuaJ4bZRWMRl2zUfrjI0IV9Lhr2rj2PkrvByiG+coNFojHUsK8Jc+M/smN2vzvPGMxUPM2NDj3HNKLV2IGhmsYdBtHLHYHo6hDrVSIBR1hMg8Em0t8tPrrod9jg5/PY3OwbAo4ELcYMYKv01+YQBikQ0e0YRjckx3Xmtbzr7DezSYUzjtUR02Vv1xtK+OIXICBhSqRpdazWeJzu0dbLXh8Fm4DDS4KDDtwAFn5SnCYe2ixiwu4y9+o3zDL4uj9/WfHhg==";
					sPrivateKey="MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDRFFfHGrMSHv1g787+oSm5lyjrFq5gy5294o/6iqu5Tk8ks19Nuhj0ZVw5aemkEcdmSsMh962y6mArMjUmZ8Bviu3QODIGQOCSPTnhL5PZnVzSWIQUnpP8HcaeDJ0YHcpKG6f9Fxras+bH/Li5PDLOEth2w1HEQO588hxDxqs9r01ATf5w3TIXx3H3TqaP9bjil4nkCW26zWOJs7ilxi+7J1+xFiIQIzfiqUAxCxjqyO9MNcoqOOjHBkrB+tkqDd/Fm/LXg6XjT2n4udaIo/Ifcic/Vh4bN+xwfxikke/J+1K1cg1TKLNOPCOFse5fnd7RlX/A7oh26xj/oh7wrvDTAgMBAAECggEAWg+PN/6yQA4FSupBmcFGPeF1CL7+YkeTLSYYqu1sxyeKQ97YIJ0W/r9imICc2cWmxNNTdqA/vB/lSPYZmu96HxITGmv6cGMl2RhyhCOAZm64ewsR6efdCu6wjYgVazv51S5QkF6RfL+dm2urDm912vqXL6E26Ximwj9wGWnMfYbrg4zB8qihxkSNhIe+NA9WMk4Pek9yeOW/gcNWXRzPNr0LmvuvPsUzLSdXddPG4HWtgVwvKYxbvKz5PkX5AUs6KyTgAMtuAcDgfpjv1j7xHc3dEN0BC9IlkUSYfEW5wta3YwXSxEhKT4Ilk88kaluHGm2z/WgnI/QNPcMyr7+LUQKBgQD8p47KQUITZTqhefIteVsjLjMGDDqxbs6e8VgTbVpJAy3JCtqT1Q9pdHktDLhf6O3gzUGyfC8I1VFZrEJ4UsHdbfGyLlICRfd47T3+lj/Up/2crHQGtELR388g6AFMT97PErEbueWtlmKg/Li2YRybVdX/4wUL95z2SZu9mIbI9QKBgQDT2RNO9gjhJr/H1CJR6ixLALEJLk+3hhXMRXvM+Ndy+V+orU/5uRsB06PWAq+4smAgS/lUS5QelD3kmHBT3JvevJYljKuCvXlK9BMLqX4TFezVmU2NaRGQ27eyOo8YvnbjM1jN9YOHgacnCR8Dfit/+ER+Cwjy1qi3vgTs3JbVpwKBgCeXolqzi0U60ctILtlNcSi8Lqa999yiPqLNsz2oH0Pdyaj9mG8eLkIaTHrwyVvORqi5wZfUuhIqS4s/4VK1tUeb2QpRE+zjRfqfQt3IccxDXwzUQYJjcS0uY2kGsl1hDstoZHSx1voy3NXZhKCf6zm0Y8qrqrjLXr+BnXRCmKyZAoGAVJC1rXJ3LvFZ4vniTVKuJa57/Q84kSj8MJOYDMVUs7eqINo2L5hdmIgUQYzG+nJx/tJ1D9ovoVlJr6POSxuoOOyM/yY9HK5n6ayuoZwiEJpS9R0CIS3M7kdW99V6Y8U8DgvkKisUOxZdVC7G640E7kt7g9kB1HYt2VMIAkk4CMcCgYA2nQWTlgoOHUBHFe3F3tqH++3s48xFV5mQxx1Oy0ELBJdvHsuU+ZLW5++P9K1L8Yex8Q7wOFUow4bcnPO8fdN/qGxQ/jcHuSNuV11dSyZuffCDKZPd2tQ6yfFEtHjB8BNotGxK17CJT1I5tYOZ38pTSogbHkddlm5IsHegIA1rQA==";
					sPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0RRXxxqzEh79YO/O/qEpuZco6xauYMudveKP+oqruU5PJLNfTboY9GVcOWnppBHHZkrDIfetsupgKzI1JmfAb4rt0DgyBkDgkj054S+T2Z1c0liEFJ6T/B3GngydGB3KShun/Rca2rPmx/y4uTwyzhLYdsNRxEDufPIcQ8arPa9NQE3+cN0yF8dx906mj/W44peJ5Altus1jibO4pcYvuydfsRYiECM34qlAMQsY6sjvTDXKKjjoxwZKwfrZKg3fxZvy14Ol409p+LnWiKPyH3InP1YeGzfscH8YpJHvyftStXINUyizTjwjhbHuX53e0ZV/wO6IdusY/6Ie8K7w0wIDAQAB";
//					String enc1="Encrypted";
					oNewPublicKey = Utils.getPublicKeyFromString(sPublicKey);
					oOldPrivateKey = Utils.getPrivateKeyFromString(sPrivateKey);
					
					enc = Utils.decrypt(enc, oOldPrivateKey);
					System.out.println("Decrypted String..."+enc);
					enc = Utils.encrypt(enc, oNewPublicKey);
					System.out.println("Encrypted String..."+enc);
					
					/*Client_Cert_Path = Utils.encrypt(Client_Cert_Path, oNewPublicKey);
					password = Utils.encrypt(password, oNewPublicKey);
					StoreNo = Utils.encrypt(StoreNo, oNewPublicKey);
					PortNo = Utils.encrypt(PortNo, oNewPublicKey);
					Host = Utils.encrypt(Host, oNewPublicKey);*/
					
					
					// System.out.println("Encrypted String..."+ sPublicKey);
					// System.out.println("Encrypted String...MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArb2P71NxZe0cNGBF1rGnCvsOSk1kipw+UaHVUN1o3+uxVad+pvVBgDVcc1c0cSgI7mvJ/oN2Kx7F3I6AcJh3b2Yvk1SJSIPA02H43BYPEg+rMkTOE7/h0FGRE6Rgg8RCTyynUfI5f2Wpis1mY6AYSoI87ZPd9schK/xN9dwU9s9P7qn5HBfSNdmdSQHprHTLcanSFQZ+DvArRbcD1MDyyumOTKWt6T0t6f1lnL0OfwEKYvfw5oc2OZvPw+xRCVES+OWl4DeEZN6YSsJB7yCO7KvuDa1A818Ar4/rTKEi3+E88PoihOfQT3Z1k5gOqpdArqvjWo0drUXwP94aYw/x4wIDAQAB");
					// System.out.println("Encrypted String..."+ sPrivateKey);
					// System.out.println("Encrypted String...MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCtvY/vU3Fl7Rw0YEXWsacK+w5KTWSKnD5RodVQ3Wjf67FVp36m9UGANVxzVzRxKAjua8n+g3YrHsXcjoBwmHdvZi+TVIlIg8DTYfjcFg8SD6syRM4Tv+HQUZETpGCDxEJPLKdR8jl/ZamKzWZjoBhKgjztk932xyEr/E313BT2z0/uqfkcF9I12Z1JAemsdMtxqdIVBn4O8CtFtwPUwPLK6Y5Mpa3pPS3p/WWcvQ5/AQpi9/DmhzY5m8/D7FEJURL45aXgN4Rk3phKwkHvII7sq+4NrUDzXwCvj+tMoSLf4Tzw+iKE59BPdnWTmA6ql0Cuq+NajR2tRfA/3hpjD/HjAgMBAAECggEAaMzrwcVnpb/9P2rDNdo7lNgTqqY8e4/712nzLrAfztJxSeWa9EMEm72UYs67h3ZKzOagFbJe2WvalkwvFbwib0QIdqqLlP5Elj2Gy9tSg+aHse7nD+qy3+ITzPFedp0wEmoxE7E1Fa/RMTTtCjNL88XT0stbHLzJOasbq65xxt4Ly9S3ZNoRZDWW7RqPf7sSSTm7rqSraJAgSH3SKM+WrK2/g36gEQnFY9pjqcfMHXTsMED9bi8pVldvIR5B8wzJuF8+tjyRC78rtmmKi/+uDTf6BVQq9J7deTGd2MrHggwcswPzYT3P+CDgJpELWOmRgTCa21l5RT8jQC7nVPlkYQKBgQD+M4wRT1Ma9ovGISWiJNYIB7xAKTJqcP4YIQldjCxaxjWW3On1npkBKtBzU2+ypFD1D0qa54RR38YW1hWt07sxaKcS/T6bZbYxzByar63OyIpuTHG836InUJ2dqQQLJTmFuyeK2xY6QfgOYxncTAyTOahs3A8g8FEi6nzIdoFinQKBgQCu+EVIfGG5xWKK59GGlsG+zRW2wL/VQqPrdK3kM2FjBNsNk1YNnlJ8mG0PRVEM+Kak2R8/J6ze+rAaApqMZ/mg+scJfLYwQQck8yADYAUW773heodiWKDIU4d1l4nsJ6G9fAQGn9aaftcRRccMuC+wupn1mcq1R6N9OEBL95Y+fwKBgCKDWScYgA4PH1cdNQDYw35/nl2PulqdUMJx6dWbrIH+m1QKVnGUwO1ZpMejsmysxpkfJF/3ww1itoBvPK5HKwlATj7Tx2ZN3vjP36pY495JeBIVqvOb4WY5Gc1IH1ZsDW1Mxceii1gJkOZTKjWw1Zw/ndB56EvulPWKpLqK7HI5AoGAegXJA7FC0jTW8XqURCGVQsRyFT69SjQsb0OzFgfDLjQvtHsBM4QrkJsYy+KSkNJEHYkSYo//3o3EDI+uSqckj2D1OCeSQ3rGSb2IoPHw+rr9mGdETZDzqGGggaDKXjiRcr0vbZNBLgOxZ2lbVQKKt9ybDI5RsTUhzm1RYelsDW8CgYEAnHaZ8y00YjqNNxt+KvzBsp+wtmnMAvciO6gdYdOa5Hk6M1HFw2Elvbj9wb5/SBt+XmsBPt78CDi4CzI5lGy+/my5bPGcJrdnJ4llLYWECkllmJh79qmw+DGCSRxaF1/aUGHe1RlNvwhPr+p98gf8yU1HxBbWCptBdZHq65/1Vb0=");
					
					
					/*Client_Cert_Path = Utils.decrypt(Client_Cert_Path, Utils.getPrivateKeyFromString(sPrivateKey));
					password = Utils.decrypt(password, Utils.getPrivateKeyFromString(sPrivateKey));
					StoreNo = Utils.decrypt(StoreNo, Utils.getPrivateKeyFromString(sPrivateKey));
					PortNo = Utils.decrypt(PortNo, Utils.getPrivateKeyFromString(sPrivateKey));
					Host = Utils.decrypt(Host, Utils.getPrivateKeyFromString(sPrivateKey));
					
					// System.out.println("Encrypted String..."+ Client_Cert_Path);
					// System.out.println("Encrypted String..."+ password);
					// System.out.println("Encrypted String..."+ StoreNo);
					// System.out.println("Encrypted String..."+ PortNo);
					// System.out.println("Encrypted String..."+ Host);*/
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	public void decryptFirstData() throws Exception{
		String sPrivateKey = "";
		String Client_Cert_Path = "";
		String password = "";
		String StoreNo = "";
		String Host = "";
		String PortNo = "";
		String sCustBillFName = "G4O9qvMJPvasi3wQoePFLbeNZH2qlEJRLus9JGHVhQbTRyLsJLdzDuy7giLdn8ElzFU93jm3ATXFMANR1s6ZT0D+XZ87HynIXDlz8Fi5uo5JfT729ExdwYTJAe41dPl6nyXQ+d0HkcTGByTCOXP2OYifPebTYvMkVv0Ze+6nDBbeZGT4PWWD8LKB2zKIGdBsPRkWC1GqKRTY7r4CIOh1PLeEWaMTbbxBWSgKCLh0Ufqm3LlKj158KSlW5xEZFZ0Vak9dA621ioxXn52IeEyQVJqCq28mG4Eclw20YP66WtL/e8wF1GwGvKZKd7uC33T817m97d/Xt8MsO6ZDLR6BzQ==";
		Connection con=null;		
		Statement cstmt=null;
		ResultSet rs=null;
		String sQry="select client_cert_path,password,store_no,host,port,status,CAST(checksum_one AS VARCHAR(MAX)) AS checksum_one,checksum_two from sbh_firstdata_merchant_info";
		
		Client_Cert_Path = "SGFstrBR3MIwk3yHrmy9Fn73zLQt7bqXOvGB+5n9fX9/joxSy9h94zY+KOyvisU/WdQnKLls9CUS0ehMhkLg6/C/zOll9HpBpsCtXOOwu+7eHfAhWa5FUpsG89AFnHA0LLBv12U/YX3gDY2pjkH3n3RHHUpCn5GjSIxjZ06JNlRD/IRrqS6c1vnOex3y/W5bfTzVldbITccugszpeMM4KAReOV/vXWPa4+bd4Uw5VAr8dRPD6MY35xdSPZnmnjHGokiXh++OAxZBgiwQJhojYLlYKGPrbqEEPHCsz1hnLwfjysAjbTSGH7CP4oynC/cQ2MD7lmfVJ/Uzwv2DaOVmxg==";

		password = "BtX3eEsKCSYFHYjUALNdtl1F6NaYwlNwDuybjnT3v7Ii2VRdRNxwljMALslU7K6dONK+yad5gGBO6Q0pXO70LGu8Ah4iiS4bfN+tpmbtstyD6VHt8tuYnP8fse9E302gq82AFEEI1hlJo9v5GfBT4S+Snk4ZuPvdUqKv3suKdhH/XAVbErYWomICvYFHTLRU30YmmYqQPmyPDDuqRbRVfOQTUVadNKJkSXImm32iCutRjUUrYKV6Xo+2fN7skOXwelL4yv/EzHzoXpg76rFLv+aH250HICpjOfoYiLX3K6w6fOfZH/HncfG8sY1+ObgtaEcPPLrrlY7Xa+lZRTPSeA==";

		StoreNo = "oydOAkuHaPKny/VrNV3r7fTo/TSWQB43ecdFwyhHdRMaQUYgBmH+uZgXyQx/FH5Q2ud1DlP4Dau3vG7JrYa47ceLoIQifBpoQW9GiEfOR0+2ZM3Zobqu0uTGuJPT/paxJSr4tY34fMncr5dTQea8K2eTUcD7k5Fb2CwdgedsqS6YdrgUv9+cqq6vuiXCp/nfrS4l27UpDQPBdqgwc/eKQ4XnEshzQkxDAJrbe3W8XoCp0N4XBoznfyt5rb8Nnw1ok+mB8STGVc2zBdzsJBvqPb4LeBn/hXwHVhX8xPKZP/l9OCktoYcz+5F9jpbehNaMaoEj5gxwvzLeBrgCPEx5JA==";
		
		Host = "HxIDXg77Ulk9Ejn6PmiBSz6cxaK+mHsNEtB0UMotr5VGMTohNbfAg39/Qlxlojs1TGXA23zpcUU2L7aaBdiufYd+BoCTqlfP+SoIPV0KUo7296p7jYtcT4CjmplhOcOxBEjkfpyTHtgjlvAHDtEmZVlqM0jr87LB9KrkTKQh7phRmBj98xMJzEaPl0aqHlarRtPFUuPfGpg8vG27L4V6O9bHYdG6glHLV8d32P09K1JblwaXHrcmbLz5aHjry799jB9N8cXFS3mrEkWQdkxcQOHNWxL+BPXET7/Zk6awXk4tpOKitnSftME1jaaUxi+97B8JvIHW09G2ufrFbAGjlA==";
		
		PortNo = "N1kVdRUsSPW/UnaLbm9lW1kJzqsIQSiy9V0Wha23HU18rotl58t3nkZUg4rVEZFHHm/zKQOEbuKIgEXS4gGjhcNcKpOlGoM06h4k9sDtSckw5yLHieunP1BNvlJCGqMEU1oE1hh8vF3PRqJZuGJIAEBnOZs2nUqrb8u+Jbh5fSDVbPzDfNkySsTutc257oItLjJPKjeT6+4J7au0mnfnCq0YkjjQr2qhCLb8WPlZV5pxS/XRmRw54HeVcpwYt6INzGfnDpunlmOPkOQVKe1/75zC0X2imyfQ+JGfZlmQZ0Os8uyf/4NSQorsojPQ2ktF4itBfF54KKEZz0xa1AHR5g==";
		
		

		try{
			con = getSQL2005Connection();
			cstmt=con.createStatement();
			rs=cstmt.executeQuery(sQry);
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					
					Client_Cert_Path = Utils.decrypt(Client_Cert_Path, Utils.getPrivateKeyFromString(sPrivateKey));
					password = Utils.decrypt(password, Utils.getPrivateKeyFromString(sPrivateKey));
					StoreNo = Utils.decrypt(StoreNo, Utils.getPrivateKeyFromString(sPrivateKey));
					PortNo = Utils.decrypt(PortNo, Utils.getPrivateKeyFromString(sPrivateKey));
					Host = Utils.decrypt(Host, Utils.getPrivateKeyFromString(sPrivateKey));
					sCustBillFName = Utils.decrypt(sCustBillFName, Utils.getPrivateKeyFromString(sPrivateKey));
					
					// System.out.println("Client Cert Encrypted String..."+ Client_Cert_Path);
					// System.out.println("Password Encrypted String..."+ password);
					// System.out.println("Store No Encrypted String..."+ StoreNo);
					// System.out.println("Host Encrypted String..."+ Host);
					// System.out.println("Port Encrypted String..."+ PortNo);
					// System.out.println("CustBillName Encrypted String..."+ sCustBillFName);
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	
	public void encryptOldCreditCard() throws Exception {
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_encrypt_old_credit_card()}";
		String sPrivateKey = "";
		String sPublicKey = "";
		String sCVV="";
		String sCCNo="";
		String sCCNoLFD = "";
		String sUpdateQuery="";
		String sUpdateCVV = "";
		String sOrderItemId="";
		String sPaymentId="";
		List<String> alQuery = new ArrayList<String>();
		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					sPublicKey = rs.getString("checksum_two");
					sCVV = rs.getString("cvv");
					sCCNo = rs.getString("cc_no");
					sOrderItemId = rs.getString("order_item_id");
					sPaymentId = rs.getString("payment_id");
					
					// System.out.println("Credit Card No..."+sCCNo);
					// System.out.println("Credit Card CVV.."+sCVV);
					// System.out.println("PrivateKey...."+sPrivateKey);
					// System.out.println("PublicKey...."+sPublicKey);
					
					// System.out.println("Order Item Id...."+sOrderItemId);
					
					if(sCVV != null && sCVV.trim().length()>0) {
						sCVV = Utils.encrypt(sCVV, Utils.getPublicKeyFromString(sPublicKey));
						sUpdateCVV = ", cvv=convert(varbinary(max), '"+sCVV+"') ";
					}
					if(sCCNo != null && sCCNo.trim().length()>0) {
						sUpdateQuery="Update sbh_payment set cc_no=convert(varbinary(max), '"; 
						sCCNoLFD = sCCNo.substring(sCCNo.trim().length() -4, sCCNo.trim().length());
						sCCNo = Utils.encrypt(sCCNo, Utils.getPublicKeyFromString(sPublicKey));
						sUpdateQuery = sUpdateQuery+sCCNo+"'), cc_no_lfd='"+sCCNoLFD+"'"+sUpdateCVV+" where payment_id="+sPaymentId;
						alQuery.add(sUpdateQuery);
					}
					
					// System.out.println("Credit Card No..."+sCCNo);
					// System.out.println("Credit Card CVV.."+sCVV);
					sUpdateQuery=""; 
					sUpdateCVV = "";
				}
			}
			if(alQuery.size()>0) {
				Statement stmt = con.createStatement();
				for(String str:alQuery) {
					int update = stmt.executeUpdate(str);
					// System.out.println("Query..."+str);
/*					if(update == 1)
						// System.out.println("Updated Success");
//					else
						// System.out.println("Updated Failure");*/	
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	
public void decryptACreditCard() throws Exception {
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call decryptOldCreditCard(?)}";
		String sCVV="";
		String sCCNo="";
		String sOrderItemId="";
		String sPaymentId="";
		String sPrivateKey = "";
		String sPublicKey = "";
		List<String> alQuery = new ArrayList<String>();
		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, "11014");
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					sPublicKey = rs.getString("checksum_two");
					sCVV = rs.getString("fname");
					sCCNo = rs.getString("cc_no");
					sOrderItemId = rs.getString("exp_dt");
					sPaymentId = rs.getString("payment_id");
					
					sCVV = Utils.decrypt(sCVV, Utils.getPrivateKeyFromString(sPrivateKey));
					sCCNo = Utils.decrypt("F+d9OhZKQlAgbCqFeie+3jDhwnae0Evmr1XPuweNeiBe7TfwAX8GrQZUzasZBr4R6u/7Pt62bwagFwkcCN+xvMAPvLNbQ+zZm6KQw3avcZ+JRaYXnf3ZWjTyZWsPQbfQwcIklxnoudFiEdqPa3T9b26jmhb5dhTRNjlkj0qlA1srs5pMcW2YJhVcvEStcxe7t01lfPGPbTA11dnRKFMoQkPgKa8T4gHrDp8FiE4KBnHyZAmwcj5sy7IopWzv0vcbgWeyIETKN/y0Q3Tke6vkROUw0WqGqszgzFQu27VPol/QiVEE0nDwyd/XFR0I6RxKnQXB3OQGZwjcWroYs3EtJw==", Utils.getPrivateKeyFromString(sPrivateKey));
					String sCCNewNo =     "F+d9OhZKQlAgbCqFeie+3jDhwnae0Evmr1XPuweNeiBe7TfwAX8GrQZUzasZBr4R6u/7Pt62bwagFwkcCN+xvMAPvLNbQ+zZm6KQw3avcZ+JRaYXnf3ZWjTyZWsPQbfQwcIklxnoudFiEdqPa3T9b26jmhb5dhTRNjlkj0qlA1srs5pMcW2YJhVcvEStcxe7t01lfPGPbTA11dnRKFMoQkPgKa8T4gHrDp8FiE4KBnHyZAmwcj5sy7IopWzv0vcbgWeyIETKN/y0Q3Tke6vkROUw0WqGqszgzFQu27VPol/QiVEE0nDwyd/XFR0I6RxKnQXB3OQGZwjcWroYs3EtJw==";
					String sCCNewNOs =    "btq7M//ImO6qs2wA7ZxdyvsRK8J6gZybt4ONmLrT+6UgFvCZJ3mk2S9bz67N1pAE5xu02zGGGqPC\r\nXTrOlykJx5Y70XKVbJe4gtPex3kcCFB8MXBtElnkZR1J2hgyotphaVsJVdCzvKK+aYTeZ+DlnUze\r\nQYzxg5wmaCGcJxWQExGsD8Ak7H8QPBlad9rA/QNeQYqgSv5QBc++qCfAw++hhH5btadMVa6XYFSk\r\nfce8xHz6GOV4aG1H5TbTpfyRFJN0/1g9wsplbcmu/PV+hvr8TVHvnqHjKS4lr00sqPtdxHtuCkH6\r\nLjdz98+cyzdw3YvdnrDU4lNN8yjRq1Nyec28iw==";
					//"g3uM3Ez8psmFi3DHKzMPdezA69zDbwvee8820W0hkMtkyLDR4DQudTqkDJN9x5/g5fCHLysLdD7BqHVFrLeLfxHvJHhPrcSsxuB4QPy/3qG4sTvBqrldtGYGog5RtLQXttzgDjTfZq54OByWtTI1h6e/GkzL/ej95E6t7/BdX65yC2iAhnYlMa8m0IhsLzMr0HAUobptkJ+RGH0WzUOUGYQ43WaoUn1lcWJAkT/C/ddLUQnjBPt7I1FNlFC70sC/S4hHzEuIM5EflVso31M9tc+s8MndO5aa9pD4LePKX2kuHXPWqOP3NTcJpyKf4ngzlumWIkOVz+2auWZMh+327g==""
					sOrderItemId = Utils.decrypt(sOrderItemId, Utils.getPrivateKeyFromString(sPrivateKey));
					
					// System.out.println("Credit Card No..."+sCCNo);
					// System.out.println("Credit Card Exp date.."+sCVV);
					// System.out.println("Credit Card HolderName.."+sOrderItemId);
					// System.out.println("Payment Id .."+sPaymentId);
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	
	public void encryptOldCreditCardInfo() throws Exception {
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_encrypt_old_credit_card_info()}";
		String sPrivateKey = "";
		String sPublicKey = "";
		String sCCNOFName="";
		String sExpDt="";
		String sCardType = "";
		String sUpdateQuery="";
		String sUpdateCVV = "";
		String sOrderItemId="";
		String sPaymentId="";
		List<String> alQuery = new ArrayList<String>();
		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					sPublicKey = rs.getString("checksum_two");
					sCCNOFName = rs.getString("fname");
					sExpDt = rs.getString("exp_dt");
					sCardType = rs.getString("card_type");
					sPaymentId = rs.getString("payment_id");
					
					// System.out.println("Card HolderName..."+sCCNOFName);
					// System.out.println("Expiry Date.."+sExpDt);
					// System.out.println("CardType...."+sCardType);
					
					// System.out.println("Order Item Id...."+sOrderItemId);
					
					
					if(sCCNOFName != null && sCCNOFName.trim().length()>0) {
						
						sCCNOFName = Utils.encrypt(sCCNOFName, Utils.getPublicKeyFromString(sPublicKey));
						sExpDt = Utils.encrypt(sExpDt, Utils.getPublicKeyFromString(sPublicKey));
						sCardType = Utils.encrypt(sCardType, Utils.getPublicKeyFromString(sPublicKey));
						
						sUpdateQuery="Update sbh_payment set fname=convert(varbinary(max), '"+sCCNOFName+"'), exp_dt=convert(varbinary(max), '"+sExpDt+"'), card_type=convert(varbinary(max), '"+sCardType+"') where payment_id="+sPaymentId; 
						
						alQuery.add(sUpdateQuery);
					}
					
					// System.out.println("Credit Card No..."+sCCNOFName);
					// System.out.println("Credit Card CVV.."+sExpDt);
					// System.out.println("Credit Card CVV.."+sCardType);
					
					
					sUpdateQuery=""; 
					sUpdateCVV = "";
				}
			}
			if(alQuery.size()>0) {
				Statement stmt = con.createStatement();
				for(String str:alQuery) {
					int update = stmt.executeUpdate(str);
					/*if(update == 1)
						// System.out.println("Updated Success");
					else
						// System.out.println("Updated Failure");
*/				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	
	public void decryptCreditCardInfo() throws Exception {

		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_encrypt_old_credit_card_info()}";
		String sPrivateKey = "";
		String sPublicKey = "";
		String sCCNOFName="";
		String sExpDt="";
		String sCardType = "";
		String sUpdateQuery="";
		String sUpdateCVV = "";
		String sOrderItemId="";
		String sPaymentId="";
		List<String> alQuery = new ArrayList<String>();
		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPrivateKey = rs.getString("checksum_one");
					sPublicKey = rs.getString("checksum_two");
					sCCNOFName = rs.getString("fname");
					sExpDt = rs.getString("exp_dt");
					sCardType = rs.getString("card_type");
					sPaymentId = rs.getString("payment_id");
					
					// System.out.println("Card HolderName..."+sCCNOFName);
					// System.out.println("Expiry Date.."+sExpDt);
					// System.out.println("CardType...."+sCardType);
					
					// System.out.println("Order Item Id...."+sPaymentId);
					
					
					if(sCCNOFName != null && sCCNOFName.trim().length()>0) {
						
						sCCNOFName = Utils.decrypt(sCCNOFName, Utils.getPrivateKeyFromString(sPrivateKey));
						sExpDt = Utils.decrypt(sExpDt, Utils.getPrivateKeyFromString(sPrivateKey));
						sCardType = Utils.decrypt(sCardType, Utils.getPrivateKeyFromString(sPrivateKey));
						
						// System.out.println("Credit First Name...."+sCCNOFName);
						// System.out.println("Expirey date...."+sExpDt);
						// System.out.println("Card Type....."+sCardType);
					}
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	
		
	}
	public void encryptOldPassword() throws Exception {
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_encrypt_old_password()}";
		String sPublicKey = "";
		String sPrivateKey = "";
		String sNewPrivateKey = "";
		String sUpdateQuery="";
		String sUserId="";
		String sPassword="";
		List<String> alQuery = new ArrayList<String>();
		try{
			con = getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					sPublicKey = rs.getString("new_checksum_two");
					sNewPrivateKey = rs.getString("new_checksum_one");
					sPrivateKey = rs.getString("old_checksum_one");
					sUserId = rs.getString("id");
					sPassword = rs.getString("password");
					
					if(sPassword != null && sPassword.trim().length()>0) {
						sPassword = Utils.decrypt(sPassword, Utils.getPrivateKeyFromString(sPrivateKey));
						sPassword = Utils.encrypt(sPassword, Utils.getPublicKeyFromString("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv5c4Z2ERVvuy00Nc855eiP/jZPBM10txL2N3Og721/eRN9GEbtmXRGc61mkwQpOiSw8SIvTk2wqT8JAZJO8qw3Fzz4PE8eqG62KlRwmtpg6gx5rY8MOHREtQ/nfpe1ERvFsSIwvCVcNz8SCN7vgo/PigxqS7nF6fkFQxbOwu9pqlx/qboEgQcQpkMEI8gLoJH+pzLR0KkqsxNZSLsBGUNIhSy9OoFY6iAy6THxYPjFvOhujPKqW7QyCCMTnLHeYtNo8aIrHOfaXeFYM2wM7c6JvlTRXqV9hlmoPUJadYVamTbqo9lYVK8HEoYTO3kAwlPVFEAHebob+s+ONFEMt6pwIDAQAB"));
						sUpdateQuery = "Update sbh_user set password=convert(varbinary(max), '"+sPassword+"') Where id="+sUserId;
						alQuery.add(sUpdateQuery);
					}
					
					sUpdateQuery=""; 
					System.out.println(sPassword);
				
					sPassword = Utils.decrypt(sPassword, Utils.getPrivateKeyFromString(sNewPrivateKey));
					System.out.println(sPassword);
				}
			}
			if(alQuery.size()>0) {
				Statement stmt = con.createStatement();
				for(String str:alQuery) {
					/*int update = stmt.executeUpdate(str);
					if(update == 1)
						 System.out.println("Updated Success");
					else
						 System.out.println("Updated Failure");*/
				}
			}
		}catch(Exception e) {
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
	}
	public Connection getSQL2005Connection() throws Exception {

		Connection con = null;
		InitialContext ctx = null;
		try {
			if (ctx == null)
				ctx = new InitialContext();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(
					"jdbc:sqlserver://localhost;databaseName=skybuydata",
					"SA", "SkyBuy01");
//			if (con != null)
				// System.out.println("Connected");

		} catch (Exception e) {
			System.err.println("getSQL2005Connection Error is " + e.toString());
		}

		return con;
	}
	private void generateXMLFromPublicKey() throws Exception {
		String sPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv5c4Z2ERVvuy00Nc855eiP/jZPBM10txL2N3Og721/eRN9GEbtmXRGc61mkwQpOiSw8SIvTk2wqT8JAZJO8qw3Fzz4PE8eqG62KlRwmtpg6gx5rY8MOHREtQ/nfpe1ERvFsSIwvCVcNz8SCN7vgo/PigxqS7nF6fkFQxbOwu9pqlx/qboEgQcQpkMEI8gLoJH+pzLR0KkqsxNZSLsBGUNIhSy9OoFY6iAy6THxYPjFvOhujPKqW7QyCCMTnLHeYtNo8aIrHOfaXeFYM2wM7c6JvlTRXqV9hlmoPUJadYVamTbqo9lYVK8HEoYTO3kAwlPVFEAHebob+s+ONFEMt6pwIDAQAB";
		
		String sPrivateKey="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCtvY/vU3Fl7Rw0YEXWsacK+w5KTWSKnD5RodVQ3Wjf67FVp36m9UGANVxzVzRxKAjua8n+g3YrHsXcjoBwmHdvZi+TVIlIg8DTYfjcFg8SD6syRM4Tv+HQUZETpGCDxEJPLKdR8jl/ZamKzWZjoBhKgjztk932xyEr/E313BT2z0/uqfkcF9I12Z1JAemsdMtxqdIVBn4O8CtFtwPUwPLK6Y5Mpa3pPS3p/WWcvQ5/AQpi9/DmhzY5m8/D7FEJURL45aXgN4Rk3phKwkHvII7sq+4NrUDzXwCvj+tMoSLf4Tzw+iKE59BPdnWTmA6ql0Cuq+NajR2tRfA/3hpjD/HjAgMBAAECggEAaMzrwcVnpb/9P2rDNdo7lNgTqqY8e4/712nzLrAfztJxSeWa9EMEm72UYs67h3ZKzOagFbJe2WvalkwvFbwib0QIdqqLlP5Elj2Gy9tSg+aHse7nD+qy3+ITzPFedp0wEmoxE7E1Fa/RMTTtCjNL88XT0stbHLzJOasbq65xxt4Ly9S3ZNoRZDWW7RqPf7sSSTm7rqSraJAgSH3SKM+WrK2/g36gEQnFY9pjqcfMHXTsMED9bi8pVldvIR5B8wzJuF8+tjyRC78rtmmKi/+uDTf6BVQq9J7deTGd2MrHggwcswPzYT3P+CDgJpELWOmRgTCa21l5RT8jQC7nVPlkYQKBgQD+M4wRT1Ma9ovGISWiJNYIB7xAKTJqcP4YIQldjCxaxjWW3On1npkBKtBzU2+ypFD1D0qa54RR38YW1hWt07sxaKcS/T6bZbYxzByar63OyIpuTHG836InUJ2dqQQLJTmFuyeK2xY6QfgOYxncTAyTOahs3A8g8FEi6nzIdoFinQKBgQCu+EVIfGG5xWKK59GGlsG+zRW2wL/VQqPrdK3kM2FjBNsNk1YNnlJ8mG0PRVEM+Kak2R8/J6ze+rAaApqMZ/mg+scJfLYwQQck8yADYAUW773heodiWKDIU4d1l4nsJ6G9fAQGn9aaftcRRccMuC+wupn1mcq1R6N9OEBL95Y+fwKBgCKDWScYgA4PH1cdNQDYw35/nl2PulqdUMJx6dWbrIH+m1QKVnGUwO1ZpMejsmysxpkfJF/3ww1itoBvPK5HKwlATj7Tx2ZN3vjP36pY495JeBIVqvOb4WY5Gc1IH1ZsDW1Mxceii1gJkOZTKjWw1Zw/ndB56EvulPWKpLqK7HI5AoGAegXJA7FC0jTW8XqURCGVQsRyFT69SjQsb0OzFgfDLjQvtHsBM4QrkJsYy+KSkNJEHYkSYo//3o3EDI+uSqckj2D1OCeSQ3rGSb2IoPHw+rr9mGdETZDzqGGggaDKXjiRcr0vbZNBLgOxZ2lbVQKKt9ybDI5RsTUhzm1RYelsDW8CgYEAnHaZ8y00YjqNNxt+KvzBsp+wtmnMAvciO6gdYdOa5Hk6M1HFw2Elvbj9wb5/SBt+XmsBPt78CDi4CzI5lGy+/my5bPGcJrdnJ4llLYWECkllmJh79qmw+DGCSRxaF1/aUGHe1RlNvwhPr+p98gf8yU1HxBbWCptBdZHq65/1Vb0=";
		String encpt="D/2ScqkkgNnaOdNATE67xLf9yyttEstCU/r8Hy6r7bEF1c9cUJ5Ru3Yqh8vEvZUREauGb4Mj/kDL\r\nXpYDIOrH2i/AIWYmuFNjwhXQSIWNfKtyZoUnaOic79PNWkVuHdLT23ObnYIb374ugT7MPPt1zG3S\r\nO/O0/FVdYu8JskcbXPqIUNacEjzVExd04Szq8Iai4sZQYs6oAt4wnWWIa8BV2CeqgFzDfVk6MKO0\r\nkyOHDqtI22RzYGUZG3xxygV0pszxkrNgrIApEnZZ7fqAyNq8A/6Y5zb0GmxtiPu1IUNFQ4x67VFu\r\nNk7a4Rd/6IfDgAFtJ0PIp+dyLb0+un8tIsl9uA==";
		PublicKey pub = Utils.getPublicKeyFromString(sPublicKey);
		byte[] publicKeyBytes  = pub.getEncoded();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
	    RSAPublicKey rsaPublicKey = (RSAPublicKey)keyFactory.generatePublic(publicKeySpec);
	    String xml = RSAEncryptUtil.getRSAPublicKeyAsXMLString(rsaPublicKey);
	    System.out.println(".Net xml string \n "+xml);
		
	}
	private void encryptAndDecrypt() throws Exception{
		String sPrivatekey=null;
		String sPublicKey=null;
		PrivateKey oOldPrivateKey = null;
		PublicKey oNewPublicKey = null;
		
		String enc="EncryptString";
		sPrivatekey="MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDRFFfHGrMSHv1g787+oSm5lyjrFq5gy5294o/6iqu5Tk8ks19Nuhj0ZVw5aemkEcdmSsMh962y6mArMjUmZ8Bviu3QODIGQOCSPTnhL5PZnVzSWIQUnpP8HcaeDJ0YHcpKG6f9Fxras+bH/Li5PDLOEth2w1HEQO588hxDxqs9r01ATf5w3TIXx3H3TqaP9bjil4nkCW26zWOJs7ilxi+7J1+xFiIQIzfiqUAxCxjqyO9MNcoqOOjHBkrB+tkqDd/Fm/LXg6XjT2n4udaIo/Ifcic/Vh4bN+xwfxikke/J+1K1cg1TKLNOPCOFse5fnd7RlX/A7oh26xj/oh7wrvDTAgMBAAECggEAWg+PN/6yQA4FSupBmcFGPeF1CL7+YkeTLSYYqu1sxyeKQ97YIJ0W/r9imICc2cWmxNNTdqA/vB/lSPYZmu96HxITGmv6cGMl2RhyhCOAZm64ewsR6efdCu6wjYgVazv51S5QkF6RfL+dm2urDm912vqXL6E26Ximwj9wGWnMfYbrg4zB8qihxkSNhIe+NA9WMk4Pek9yeOW/gcNWXRzPNr0LmvuvPsUzLSdXddPG4HWtgVwvKYxbvKz5PkX5AUs6KyTgAMtuAcDgfpjv1j7xHc3dEN0BC9IlkUSYfEW5wta3YwXSxEhKT4Ilk88kaluHGm2z/WgnI/QNPcMyr7+LUQKBgQD8p47KQUITZTqhefIteVsjLjMGDDqxbs6e8VgTbVpJAy3JCtqT1Q9pdHktDLhf6O3gzUGyfC8I1VFZrEJ4UsHdbfGyLlICRfd47T3+lj/Up/2crHQGtELR388g6AFMT97PErEbueWtlmKg/Li2YRybVdX/4wUL95z2SZu9mIbI9QKBgQDT2RNO9gjhJr/H1CJR6ixLALEJLk+3hhXMRXvM+Ndy+V+orU/5uRsB06PWAq+4smAgS/lUS5QelD3kmHBT3JvevJYljKuCvXlK9BMLqX4TFezVmU2NaRGQ27eyOo8YvnbjM1jN9YOHgacnCR8Dfit/+ER+Cwjy1qi3vgTs3JbVpwKBgCeXolqzi0U60ctILtlNcSi8Lqa999yiPqLNsz2oH0Pdyaj9mG8eLkIaTHrwyVvORqi5wZfUuhIqS4s/4VK1tUeb2QpRE+zjRfqfQt3IccxDXwzUQYJjcS0uY2kGsl1hDstoZHSx1voy3NXZhKCf6zm0Y8qrqrjLXr+BnXRCmKyZAoGAVJC1rXJ3LvFZ4vniTVKuJa57/Q84kSj8MJOYDMVUs7eqINo2L5hdmIgUQYzG+nJx/tJ1D9ovoVlJr6POSxuoOOyM/yY9HK5n6ayuoZwiEJpS9R0CIS3M7kdW99V6Y8U8DgvkKisUOxZdVC7G640E7kt7g9kB1HYt2VMIAkk4CMcCgYA2nQWTlgoOHUBHFe3F3tqH++3s48xFV5mQxx1Oy0ELBJdvHsuU+ZLW5++P9K1L8Yex8Q7wOFUow4bcnPO8fdN/qGxQ/jcHuSNuV11dSyZuffCDKZPd2tQ6yfFEtHjB8BNotGxK17CJT1I5tYOZ38pTSogbHkddlm5IsHegIA1rQA==";
		sPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0RRXxxqzEh79YO/O/qEpuZco6xauYMudveKP+oqruU5PJLNfTboY9GVcOWnppBHHZkrDIfetsupgKzI1JmfAb4rt0DgyBkDgkj054S+T2Z1c0liEFJ6T/B3GngydGB3KShun/Rca2rPmx/y4uTwyzhLYdsNRxEDufPIcQ8arPa9NQE3+cN0yF8dx906mj/W44peJ5Altus1jibO4pcYvuydfsRYiECM34qlAMQsY6sjvTDXKKjjoxwZKwfrZKg3fxZvy14Ol409p+LnWiKPyH3InP1YeGzfscH8YpJHvyftStXINUyizTjwjhbHuX53e0ZV/wO6IdusY/6Ie8K7w0wIDAQAB";
//		String enc1="Encrypted";
		oNewPublicKey = Utils.getPublicKeyFromString(sPublicKey);
		oOldPrivateKey = Utils.getPrivateKeyFromString(sPrivatekey);
		
		enc = Utils.encrypt(enc, oNewPublicKey);
		System.out.println("Encrypted String..."+enc);
		enc = Utils.decrypt(enc, oOldPrivateKey);
		System.out.println("Decrypted String..."+enc);
		
	}
	public static void main(String args[]) {
		try {
			EncryptAndDecrypt e = new EncryptAndDecrypt();
//			e.encryptAndDecryptFirstData();
//			e.decryptFirstData();
//			e.encryptOldCreditCard();
//			e.encryptOldPassword();
//			e.encryptOldCreditCardInfo();
//			e.decryptCreditCardInfo();
//			e.decryptACreditCard();
//			e.generateXMLFromPublicKey();
			e.encryptAndDecrypt();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
