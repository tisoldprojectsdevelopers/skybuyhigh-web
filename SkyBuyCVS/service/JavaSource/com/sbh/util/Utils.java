package com.sbh.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



import javax.crypto.Cipher;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.sbh.vo.CategoryVO;
import com.sbh.vo.DeviceTypesVO;
import com.sbh.vo.ProductDetailsVO;


public class Utils {
	private static Logger logger = LogManager.getLogger(Utils.class);
	public static String getUserTypeFromURI(String p_sURI) throws Exception{
		logger.info("Utils:getUserTypeFromURI:ENTER");
		String sPath = null;
		try{

			p_sURI=p_sURI==null?"":p_sURI.trim();
			if(p_sURI.indexOf("\\")!=-1)
				p_sURI.replaceAll("\\", "/");
			StringTokenizer stPath=new StringTokenizer(p_sURI,"/");
			if(stPath.hasMoreTokens())
				sPath=stPath.nextToken();
			if(stPath.hasMoreTokens())
				sPath=stPath.nextToken();
			sPath=sPath==null?"":sPath.trim();

			logger.info("Utils:getUserTypeFromURI:EXIT");
		}
		catch(Exception e){		
			e.printStackTrace();
			logger.error("Utils:getUserTypeFromURI:Exception "+e.getMessage());
			throw e;
		}
		return sPath;
	}
	public static String convertToNameFormat(String name) {
		String returnName = "";
		if(name != null && name.trim().length() > 0)
			if(name.trim().length() > 1) {
				returnName = name.substring(0,1);
				name = name.substring(1,name.trim().length());
				returnName=returnName.toUpperCase();
				returnName=returnName+name;
			}else {
				returnName = name.toUpperCase();
			}
		return returnName;
	}

	public static String stringToDateConverter(String p_oDateObj) {
		String sFormattedDate = null;
		Date newDate = null;
		if(p_oDateObj != null) {
			SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat oFormatOldDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				newDate = oFormatOldDate.parse(p_oDateObj);
			}catch(Exception e) {
				e.printStackTrace();
			}
			if(newDate != null) {
				sFormattedDate = oFormat.format(newDate);
			}
		}
		return sFormattedDate;
	}

	public static String getPhoneFormat(String p_sInputFormat)throws Exception{
		String p_sDigit1=null,p_sDigit2=null,p_sDigit3=null,p_sDigit4=null,p_sFormat="";
		if(p_sInputFormat!=null && p_sInputFormat.trim().length()==10){
			p_sDigit1=p_sInputFormat.substring(0,3);
			p_sDigit2=p_sInputFormat.substring(3,6);
			p_sDigit3=p_sInputFormat.substring(6,10);
			p_sFormat=p_sDigit1+"-"+p_sDigit2+"-"+p_sDigit3;				
		}
		else if(p_sInputFormat!=null && p_sInputFormat.trim().length()==11){
			p_sDigit1=p_sInputFormat.substring(0,1);
			p_sDigit2=p_sInputFormat.substring(1,4);
			p_sDigit3=p_sInputFormat.substring(4,7);
			p_sDigit4=p_sInputFormat.substring(7,11);
			p_sFormat=p_sDigit1+"-"+p_sDigit2+"-"+p_sDigit3+"-"+p_sDigit4;		
		}
		else if(p_sInputFormat!=null && p_sInputFormat.trim().length()==14){
			p_sDigit1=p_sInputFormat.substring(1,4);
			p_sDigit2=p_sInputFormat.substring(6,9);
			p_sDigit3=p_sInputFormat.substring(10,14);
			p_sFormat=p_sDigit1+"-"+p_sDigit2+"-"+p_sDigit3;		
		}
		else{
			p_sFormat=p_sInputFormat;
		}
		return p_sFormat;
	} 

	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);

		return sFormattedDate;
	}

	public static String timeStampConversion(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
		String sFormattedDate = oFormat.format(p_oDateObj);

		return sFormattedDate;
	}
	public static String mergeName(String p_sFirstName, String p_sLastName) {
		String sName=null;

		if(p_sFirstName != null) {
			if(p_sLastName != null && p_sLastName.trim().length() > 0) {
				sName = p_sFirstName+" "+p_sLastName;
			}else{
				sName = p_sFirstName;
			}
		}
		return sName;
	}
	public static String mergeAddressLines(String p_sAddressFirstLine, String p_sAddressSecondLine) {
		String sAddress=null;

		if(p_sAddressFirstLine != null) {
			if(p_sAddressSecondLine != null && p_sAddressSecondLine.trim().length() > 0) {
				sAddress = p_sAddressFirstLine+"<BR>"+p_sAddressSecondLine;
			}else{
				sAddress = p_sAddressFirstLine;
			}
		}
		return sAddress;
	}

	public static String mergeAddressLine(String p_sAddressFirstLine, String p_sAddressSecondLine) {
		String sAddress=null;

		if(p_sAddressFirstLine != null) {
			if(p_sAddressSecondLine != null && p_sAddressSecondLine.trim().length() > 0) {
				sAddress = p_sAddressFirstLine+"<BR>"+p_sAddressSecondLine;
			}else{
				sAddress = p_sAddressFirstLine;
			}
		}
		return sAddress;
	}

	public static String combineAddressLines(String p_sAddressFirstLine, String p_sAddressSecondLine) {
		String sAddress=null;

		if(p_sAddressFirstLine != null) {
			if(p_sAddressSecondLine != null && p_sAddressSecondLine.trim().length() > 0) {
				sAddress = p_sAddressFirstLine+" "+p_sAddressSecondLine;
			}else{
				sAddress = p_sAddressFirstLine;
			}
		}
		return sAddress;
	}

	public static String convertToCategoryName(List<CategoryVO> category, String p_cateId) {
		String categoryName = null;
		if(category != null && p_cateId != null) {
			for(CategoryVO categoryVO:category) {
				if(p_cateId.equalsIgnoreCase(categoryVO.getCateId())) {
					categoryName = categoryVO.getCateName();
				}
			}
			if(categoryName == null) {
				if(p_cateId.equalsIgnoreCase("20")) {
					categoryName= "Private Jet Jaunts";
				}
			}
		}
		return categoryName;
	}

	public static String findAppropiateDeviceModel(List<DeviceTypesVO> p_alDeviceTypes, String p_sDeviceModelId) {
		String deviceName = null;
		if(p_alDeviceTypes != null && p_sDeviceModelId != null) {
			for(DeviceTypesVO deviceTypesVO:p_alDeviceTypes) {
				if(p_sDeviceModelId.equalsIgnoreCase(deviceTypesVO.getDeviceId())) {
					deviceName = deviceTypesVO.getDeviceName();
				}
			}
		}
		return deviceName;
	}

	public static String toTitleCase(String name) {		
		String returnName = "";
		if(name != null && name.trim().length() > 0)
			if(name.trim().length() > 1) {
				returnName = name.substring(0,1);
				name = name.substring(1,name.trim().length());
				returnName=returnName.toUpperCase();
				returnName=returnName+name;
			}else {
				returnName = name.toUpperCase();
			}
		return returnName;
	}

	public static String replaceTagValues(HashMap p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	

	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}

	public static int getRandomNo(){
		int aStart = 100000;
		int aEnd = 999999;
		Random aRandom = new Random();
		//get the range, casting to long to avoid overflow problems
		long range = (long)aEnd - (long)aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long)(range * aRandom.nextDouble());
		int randomNumber =  (int)(fraction + aStart);    
		// System.out.println(randomNumber);

		return randomNumber;
	}


	public static String dateStampConversion(String p_oDateObj) {

		String sFormattedDate=null;
		Date date=null;
		if(p_oDateObj != null) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			try {
				date = dateFormat.parse(p_oDateObj);
			}catch(ParseException e) {
				e.printStackTrace();
			}

			SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			sFormattedDate = oFormat.format(date);
		}
		return sFormattedDate;
	}

	public static Date stringToDateTimeConversion(String p_oDateObj) {

		Date date=null;
		if(p_oDateObj != null) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			try {
				date = dateFormat.parse(p_oDateObj);
			}catch(ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	/*public static Date dateTimeConversion(Date p_oDateObj) {

		Date date=null;
		if(p_oDateObj != null) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			try {
				date = dateFormat.parse(p_oDateObj.toString());
			}catch(ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}*/


	public static String dateAndTimeStampConversion(String p_oDateObj) {

		String sFormattedDate=null;
		Date date=null;
		if(p_oDateObj != null) {
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			try {
				date = dateFormat.parse(p_oDateObj);
			}catch(ParseException e) {
				e.printStackTrace();
			}

			SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
			sFormattedDate = oFormat.format(date);
		}
		return sFormattedDate;
	}
	public static ArrayList commaSepertedStringToList(String p_sInputString) {
		String[] saInputString=null;
		ArrayList alList = new ArrayList();
		if(p_sInputString != null && !p_sInputString.equalsIgnoreCase("")) {
			saInputString = p_sInputString.split(",");

			for(String sStr : saInputString){
				alList.add(sStr.trim());
			}
		}
		return alList;
	}

	public static String listToCommaSepertedString(ArrayList<String> p_alList) {
		String sInputString="";
		ArrayList alList = new ArrayList();
		if(p_alList != null && p_alList.size()>0) {
			for(String sStr : p_alList){
				if(!sInputString.equalsIgnoreCase("")){
					sInputString +=",";
				}
				sInputString += sStr; 
			}
		}
		return sInputString;
	}

	/**
	 * <Code>
	 * 		IF oProductDetailsVO.getCoverflowStatus() is present in HashMap
	 * 			RETURN true;
	 * 		ELSE 
	 * 			RETURN false;
	 * 		END IF
	 * </Code>
	 * <Reference>
	 * 		<Class> MerchandizeAction.java </Class>  
	 * 		<Method> updateMerchandizePriorityDetails</Method>
	 * </Reference>
	 * 
	 * Used to find any duplicate entry while setting the priority to vendor. Having two for loops
	 * first will start with the index zero ('0') and the second for loop start with the index ('1').
	 * Compare the first product status with the rest of the products in the list. If any matches
	 * then compare the vendor name of the both product, if it is same then return the 
	 * isAnyRecurrencePriority = 'true' else return the isAnyRecurrencePriority value as 'false' 
	 * 
	 * @param p_alList Instance of List<ProductDetailsVO> 
	 * @return isAnyRecurrence primitive type boolean.
	 */
	public static boolean isAnyDuplicatePriority(List<ProductDetailsVO> p_alList) {
		boolean bIsDuplicate = false;
		HashMap hmCheck = new HashMap();
		if(p_alList!=null && p_alList.size()>0){
			for(ProductDetailsVO oProductDetailsVO:p_alList){
				if(oProductDetailsVO.getCoverflowStatus().equals("1") || oProductDetailsVO.getCoverflowStatus().equals("2") || oProductDetailsVO.getCoverflowStatus().equals("3") || oProductDetailsVO.getCoverflowStatus().equals("4") || oProductDetailsVO.getCoverflowStatus().equals("5")){
					if(hmCheck.get(oProductDetailsVO.getCoverflowStatus())==null){
						hmCheck.put(oProductDetailsVO.getCoverflowStatus(), "value");
					}else{
						bIsDuplicate = true;
						break;
					}
				}
			}
		}

		return bIsDuplicate;
	}

	/**
	 * <Code>
	 * 		IF ProductDetailsVO.getPriorityOfMerchandize[Next] equals ProductDetailsVO.getPriorityOfMerchandize[++Next]
	 * 			IF ProductDetailsVO.getOwnerName[Next] equals ProductDetailsVO.getOwnerName[++Next]
	 * 				RETURN true;
	 * 			ELSE 
	 * 				RETURN false;
	 * 			END IF
	 * 		END IF
	 * </Code>
	 * <Reference>
	 * 		<Class> MerchandizeAction.java </Class>  
	 * 		<Method> updatePriorityOfMerchandize</Method>
	 * </Reference>
	 * 
	 * Used to find any duplicate entry while setting the priority to vendor. Having two for loops
	 * first will start with the index zero ('0') and the second for loop start with the index ('1').
	 * Compare the first product status with the rest of the products in the list. If any matches
	 * then compare the vendor name of the both product, if it is same then return the 
	 * isAnyRecurrencePriority = 'true' else return the isAnyRecurrencePriority value as 'false' 
	 * 
	 * @param p_aProductList Instance of List<ProductDetailsVO> 
	 * @return isAnyRecurrence primitive type boolean.
	 */
	public static boolean isAnyRecurrencePriority(List<ProductDetailsVO> p_aProductList) {
		boolean isAnyRecurrence = false;

		if(p_aProductList != null && p_aProductList.size() > 0) {
			for(int i=0;i<p_aProductList.size();i++) {
				for(int j=i+1;j<p_aProductList.size();j++) {
					if(p_aProductList.get(i) != null && p_aProductList.get(j) != null) {
						if(p_aProductList.get(i).getPriorityOfMerchandize() != null 
								&& !"9999".equalsIgnoreCase(p_aProductList.get(i).getPriorityOfMerchandize())
								&& !"9999".equalsIgnoreCase(p_aProductList.get(j).getPriorityOfMerchandize())
								&& p_aProductList.get(i).getPriorityOfMerchandize().equalsIgnoreCase(p_aProductList.get(j).getPriorityOfMerchandize())) {
							if(p_aProductList.get(i).getCateId() != null 
									&& p_aProductList.get(i).getCateId().equalsIgnoreCase(p_aProductList.get(j).getCateId())) {
								isAnyRecurrence = true;
								break;
							}
						}
					}
				}
				if(isAnyRecurrence) {
					break;
				}
			}
		}

		return isAnyRecurrence;
	}

	/**
	 * Used to separate the type of a particular file. For example if your file name is
	 * abc.jpg this method returns the jpg alone as string.
	 *  
	 * @param p_sFileName String
	 * @return ImageType String 
	 * @throws Exception
	 */
	public static String tokeniseImgType(String p_sFileName ) throws Exception{

		StringTokenizer stImageType=new StringTokenizer(p_sFileName,".");
		String sImageType="";
		while(stImageType.hasMoreTokens()){
			sImageType=stImageType.nextToken();
		}
		sImageType =sImageType == null ? "" : sImageType.trim();

		return sImageType;

	}

	/**
	 * Used to separate the name of a particular file. For example if your file name is
	 * abc.jpg this method returns the abc alone as a string.
	 * 
	 * @param p_sFileName String
	 * @return sImageName String 
	 * @throws Exception
	 */
	public static String tokeniseFileName(String p_sFileName ) throws Exception{

		if(p_sFileName != null){
			StringTokenizer stImageName=new StringTokenizer(p_sFileName,".");
			String sImageName="";
			if(stImageName.hasMoreTokens()){
				sImageName=stImageName.nextToken();
			}
			/*if(stImageName.hasMoreTokens()){
					sImageType=stImageName.nextToken();
				}*/
			sImageName =sImageName == null ? "" : sImageName.trim();
			//sImageType =sImageType == null ? "" : sImageType.trim();

			//return sImageName+"_org."+sImageType;
			return sImageName;
		}
		return "";
	}

	public static long diffBetweenDates(Date oldDate, Date newDate) {

		long lDateDiff = 0;
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(oldDate);  // = 3rd November
		cal2.setTime(newDate);  // = 5th November
		//cal1.add(Calendar.DATE,1);                   //(a) with (b)   = 1 day  diff
		//while(cal1.before(cal2))                     //(b) on its own = 2 days diff
		while(cal1.before(cal2) || cal1.equals(cal2))  //(c) on its own = 3 days diff
		{
			cal1.add(Calendar.DATE,1);
			lDateDiff++;
		}
		// System.out.println("Difference = " + lDateDiff + " days");

		return lDateDiff;
	}
	private  static final String ALGORITHM = "RSA";

	/**
	 * This method used to encrypt the given byte array to byte array of cipher text using RSA Algorithm.
	 *  
	 * @param text			byte[]
	 * @param key 			PublicKey
	 * @return cipherText 	byte[] 
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] text, PublicKey key) throws Exception
	{
		byte[] cipherText = null;
		try
		{
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text);
		}
		catch (Exception e)
		{
			throw e;
		}
		return cipherText;
	}


	/**
	 * This method used to encrypt the given text to cipher text using RSA Algorithm.
	 * 
	 * @param text				String
	 * @param key				PublicKey
	 * @return encryptedText 	String 
	 * @throws Exception
	 */
	public static String encrypt(String text, PublicKey key) throws Exception
	{
		String encryptedText;
		try
		{
			byte[] cipherText = encrypt(text.getBytes("UTF8"),key);
			encryptedText = encodeBASE64(cipherText);
		}
		catch (Exception e)
		{
			throw e;
		}
		return encryptedText;
	}


	/**
	 * This method used to decrypt the given byte array to byte array of cipher text using RSA Algorithm.
	 * 
	 * @param text				byte[]
	 * @param key				PrivateKey
	 * @return dectyptedText	byte[]
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] text, PrivateKey key) throws Exception
	{
		byte[] dectyptedText = null;
		try
		{
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding","SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, key);
			dectyptedText = cipher.doFinal(text);
		}
		catch (Exception e)
		{
			throw e;
		}
		return dectyptedText;

	}


	/**
	 * This method used to decrypt the given text to cipher text using RSA Algorithm.
	 * 
	 * @param text			String
	 * @param key			PrivateKey
	 * @return result 		String
	 * @throws Exception
	 */
	public static String decrypt(String text, PrivateKey key) throws Exception
	{
		String result;
		try
		{
			byte[] dectyptedText = decrypt(decodeBASE64(text),key);
			result = new String(dectyptedText, "UTF8");
		}
		catch (Exception e)
		{
			throw e;
		}
		return result;

	}

	/**
	 * This method used to convert the generated key to string using BASE64 encoder
	 * 
	 * @param key 	Key
	 * @return
	 */
	public static String getKeyAsString(Key key)
	{
		byte[] keyBytes = key.getEncoded();
		BASE64Encoder b64 = new BASE64Encoder();
		return b64.encode(keyBytes);
	}


	/**
	 * This method used to convert a string into private key using BASE64 encoder .
	 * 
	 * @param key 			String
	 * @return privateKey	PrivateKey
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKeyFromString(String key) throws Exception
	{
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		BASE64Decoder b64 = new BASE64Decoder();
		EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(b64.decodeBuffer(key));
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		return privateKey;
	}


	/**
	 * This method used to convert a string into public key using BASE64 encoder .
	 * 
	 * @param key			String
	 * @return publicKey 	PublicKey
	 * @throws Exception
	 */
	public static PublicKey getPublicKeyFromString(String key) throws Exception
	{
		BASE64Decoder b64 = new BASE64Decoder();
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(b64.decodeBuffer(key));
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
		return publicKey;
	}
	/**
	 * This method used to convert the byte array to a string.
	 *  
	 * @param bytes			byte[]
	 * @return b64			String 
	 */
	private static String encodeBASE64(byte[] bytes)
	{
		BASE64Encoder b64 = new BASE64Encoder();
		return b64.encode(bytes);
	}
	/**
	 * This method used to convert the string to byte array.
	 * 
	 * @param text		String
	 * @return b64		byte[]
	 * @throws IOException
	 */
	private static byte[] decodeBASE64(String text) throws IOException
	{
		BASE64Decoder b64 = new BASE64Decoder();
		return b64.decodeBuffer(text);
	}


	public static void copyfile(File f1, File f2){

		try{


			InputStream in = new FileInputStream(f1);


			//For Append the file.
			//

			//OutputStream out = new FileOutputStream(f2,true);
			//For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);


			byte[] buf = new byte[1024];

			int len;

			while ((len = in.read(buf)) > 0){

				out.write(buf, 0, len);

			}

			in.close();

			out.close();

			System.out.println("File copied.");

		}

		catch(FileNotFoundException ex){

			System.out.println(ex.getMessage() + " in the specified directory.");

			System.exit(0);

		}

		catch(IOException e){

			System.out.println(e.getMessage());  

		}
	}

	public static RSAPrivateKey getPrivateKeyFromFile(String fileName) {
		RSAPrivateKey privKey=null;
		InputStream inputReader = null;
		try {

			// read private key DER file
			inputReader =new FileInputStream(fileName);
			byte[] privKeyBytes = new byte[inputReader.available()];
			inputReader.read(privKeyBytes);
			inputReader.close(); 
			inputReader = null;

			KeyFactory keyFactory = KeyFactory.getInstance("RSA");


			// decode private key
			PKCS8EncodedKeySpec privSpec =new PKCS8EncodedKeySpec(privKeyBytes);
			privKey= (RSAPrivateKey) keyFactory.generatePrivate(privSpec);

			return privKey;  
		}
		catch (Exception e) {
			System.err.println("err: "+e);
			e.printStackTrace();
		}
		finally {

			if (inputReader != null) {
				try { inputReader.close(); } catch (Exception ce) {}
			}
		}
		return privKey;
	}


	public static String getDevicePassword(String p_sDevicePassword) {
		String sMask="";
		if(p_sDevicePassword!=null){
			for(int index=1;index<=p_sDevicePassword.length();index++){
				sMask=sMask+"*";
			}
		}
		return sMask;
	}

	public static double round(double d, int decimalPlace){
		// see the Javadoc about why we use a String in the constructor
		// http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}


	public static boolean isValidExpDate(int month,int year){
		boolean bStatus = false;
		Calendar sysDate = Calendar.getInstance();
		Calendar ccDate = Calendar.getInstance();
		ccDate.set(year, month, 0);
		sysDate.add(Calendar.DAY_OF_YEAR , 60);
		if(ccDate.after(sysDate)){
			bStatus = true;
		}else{
			bStatus = false;
		}
		return bStatus;
	}
	/**
	 * @param p_fInputFile File 
	 *  
	 * If it is a directory then iterate the list of files present inside 
	 * the directory and delete all the files, finally delete the directory.
	 * Else delete the file.
	 */
	public static void deleteFiles(File p_fInputFile) {
		/*
		 * If it is a directory delete all the files and delete the directory.
		 */
		if(p_fInputFile != null && p_fInputFile.isDirectory()) {
			File []fListOfFiles = p_fInputFile.listFiles();
				for(File fFile:fListOfFiles) {
					if(fFile.isDirectory()) {
						deleteFiles(fFile);
					}else{
						fFile.delete();
					}
				}
				p_fInputFile.delete();
		}else {
			/*
			 * Delete if it is a file.
			 */
			p_fInputFile.delete();
		}
	}
	
	public static void closeDBConnection(ResultSet p_objRs,CallableStatement p_objCstmt,PreparedStatement p_objPstmt,Statement p_objStmt,Connection p_objCon)throws SQLException{

			if(p_objRs!=null){

				p_objRs.close();
				p_objRs=null;
			}
			if(p_objCstmt!=null){
				p_objCstmt.close();
				p_objCstmt=null;
			}
			if(p_objPstmt!=null){
				p_objPstmt.close();
				p_objPstmt=null;
			}
			if(p_objStmt!=null){
				p_objStmt.close();
				p_objStmt=null;
			}
			if(p_objCon!=null){
				if ( !p_objCon.isClosed() ) {
					p_objCon.close();
				}    
				p_objCon=null;
			}
		}

	
	
	
}
