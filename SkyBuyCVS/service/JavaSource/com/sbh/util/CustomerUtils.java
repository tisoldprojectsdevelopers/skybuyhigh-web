/**
 * 
 */
package com.sbh.util;
import static com.sbh.dao.OrderDAO.dateFormat;
import static com.sbh.dao.OrderDAO.getOrderTrackingDetail;

import static com.sbh.util.Utils.mergeAddressLines;
import static com.sbh.util.Utils.mergeName;
import static com.sbh.util.Utils.replaceTagValues;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.actions.CustomerLoginAction;
import com.sbh.dao.AirlineDAO;
import com.sbh.dao.EmailDAO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.OrderTrackingDetailsVO;

/**
 * @author Thapovan
 *
 */
public class CustomerUtils {
	private static Logger logger = LogManager.getLogger(CustomerLoginAction.class);

	public static String createBlob(OrderItemDetailsVO p_oOrderItemDetailsVO) {


		HashMap<String, String> o_HMTags = new HashMap<String, String>();
		String sSkyBuyLogoPath =null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		String sEmailPDFTemplateId =null;
		EmailVO oEmailInvoicePdfVO =null;
		String sEmailInvoicePdfContent = null;
		List<OrderTrackingDetailsVO> lOrderTrackingList = null;
		String sTrackingDetails = "";
		String sEmailInvoicePdfOptionalContent ="";
		String sTrackingDetailStatus ="",sItemDetails ="";
		String sEmailContent = "";
		int iStartIndex=0,iEndIndex = 0;
		
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);

			if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR"))
				sEmailPDFTemplateId=oBundle.getString("email.customer.vendor.order.invoice.pdf");
			else
				sEmailPDFTemplateId=oBundle.getString("email.customer.airline.order.invoice.pdf");

			//sEmailPDFTemplateId=oBundle.getString("email.customer.order.invoice.pdf");
			sEmailPDFTemplateId=sEmailPDFTemplateId==null?"":sEmailPDFTemplateId.trim();		

			oEmailInvoicePdfVO = EmailDAO.getEmailContentDetails(sEmailPDFTemplateId);

			lOrderTrackingList = getOrderTrackingDetail(p_oOrderItemDetailsVO.getOrderItemId());			

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();

			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();

			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//SkyBuy Address
			o_HMTags.put("{{Address1}}",sSkyBuyAddr1);
			o_HMTags.put("{{Address2}}",sSkyBuyAddr2);
			o_HMTags.put("{{Phone}}",sSkyBuyPh);	


			if(p_oOrderItemDetailsVO != null) {

				o_HMTags.put("{{Logo}}", sSkyBuyLogoPath);
				o_HMTags.put("{{Date}}",dateFormat(new Date()));
				o_HMTags.put("{{OCN}}",p_oOrderItemDetailsVO.getCustTransId());
				
				o_HMTags.put("{{OwnerType}}",p_oOrderItemDetailsVO.getOwnerType());
				o_HMTags.put("{{InvoiceNo}}",p_oOrderItemDetailsVO.getInvoiceNo());
				o_HMTags.put("{{AirName}}",p_oOrderItemDetailsVO.getAirName());
				o_HMTags.put("{{FlightNo}}",p_oOrderItemDetailsVO.getFlightNo());
				o_HMTags.put("{{OrderItemId}}", p_oOrderItemDetailsVO.getOrderItemId());
				o_HMTags.put("{{OrderId}}",p_oOrderItemDetailsVO.getOrderId());
				o_HMTags.put("{{OrderDate}}",p_oOrderItemDetailsVO.getOrderCreatedDt());
				o_HMTags.put("{{CustomerTransId}}",p_oOrderItemDetailsVO.getCustTransId());
				o_HMTags.put("{{BillingCustomerName}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustFirstName()+" "+p_oOrderItemDetailsVO.getCustLastName()));
				o_HMTags.put("{{CustomerFName}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustFirstName()));
				o_HMTags.put("{{CustomerLName}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustLastName()));
				o_HMTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustPhone())));
				o_HMTags.put("{{BillingAddress}}",Utils.toTitleCase(mergeAddressLines(p_oOrderItemDetailsVO.getCustAddress1(), p_oOrderItemDetailsVO.getCustAddress2())));
				o_HMTags.put("{{BillingCity}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustCity()));
				o_HMTags.put("{{BillingState}}",p_oOrderItemDetailsVO.getCustState());
				o_HMTags.put("{{BillingZip}}",p_oOrderItemDetailsVO.getCustZip());
				o_HMTags.put("{{BillingCountry}}","US");
				o_HMTags.put("{{ShippingCustomerName}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustShipFirstName()+" "+p_oOrderItemDetailsVO.getCustShipLastName()));
				o_HMTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderItemDetailsVO.getCustShipPhone())));
				o_HMTags.put("{{ShippingMail}}",p_oOrderItemDetailsVO.getCustShipEmail());
				o_HMTags.put("{{ShippingAddress}}",Utils.toTitleCase(mergeAddressLines(p_oOrderItemDetailsVO.getCustShipAddress1(), p_oOrderItemDetailsVO.getCustShipAddress2())));
				o_HMTags.put("{{ShippingCity}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCustShipCity()));
				o_HMTags.put("{{ShippingState}}",p_oOrderItemDetailsVO.getCustShipState());
				o_HMTags.put("{{ShippingZip}}",p_oOrderItemDetailsVO.getCustShipZip());
				o_HMTags.put("{{ShippingCountry}}","US");
				o_HMTags.put("{{BillingMail}}",p_oOrderItemDetailsVO.getCustEmail());
				o_HMTags.put("{{CCNo}}",p_oOrderItemDetailsVO.getCreditCardNoLFD());
				o_HMTags.put("{{ExpDate}}",p_oOrderItemDetailsVO.getExpDt());
				
				String sCCNumber = p_oOrderItemDetailsVO.getCreditCardNoLFD();
				sCCNumber = sCCNumber==null?"":sCCNumber.trim();
				/*if(sCCNumber.trim().length()>0){
					if(sCCNumber.length()>0)
						sCCNumber = sCCNumber.substring(sCCNumber.length()-4, sCCNumber.length());
				}*/
				o_HMTags.put("{{CCNumber}}",sCCNumber);
				o_HMTags.put("{{CCFName}}",Utils.toTitleCase(p_oOrderItemDetailsVO.getCardFname()));
				o_HMTags.put("{{CCLName}}",p_oOrderItemDetailsVO.getCardLname());
				o_HMTags.put("{{CustomerUrl}}",System.getProperty("SkybuyhighCustomerURL"));
				if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("VC")){
					o_HMTags.put("{{CardType}}","Visa Card");
				}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("MC")){
					o_HMTags.put("{{CardType}}","Master Card");
				}else if(p_oOrderItemDetailsVO.getCardType().equalsIgnoreCase("AC")){
					o_HMTags.put("{{CardType}}","American Express");
				}	

				if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getBrandName()+"</td></tr>";
					 
				}	
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+p_oOrderItemDetailsVO.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getProdCode()+"</td></tr>";
				if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
					if(p_oOrderItemDetailsVO.getSize()!= null && p_oOrderItemDetailsVO.getSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getSize()+"</td></tr>";
					if(p_oOrderItemDetailsVO.getColor()!= null && p_oOrderItemDetailsVO.getColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getColor()+"</td></tr>";
				}	
				if(p_oOrderItemDetailsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
					if(p_oOrderItemDetailsVO.getTravelDate()!= null && p_oOrderItemDetailsVO.getTravelDate().trim().length()>0)
							sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+p_oOrderItemDetailsVO.getTravelDate()+"</td></tr>";
				}	
				
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				o_HMTags.put("{{ItemDetails}}",sItemDetails);
				o_HMTags.put("{{OrderDate}}", p_oOrderItemDetailsVO.getOrderCreatedDt());

				double dQty=Double.parseDouble(p_oOrderItemDetailsVO.getQty());
				double dSbhPrice=Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice());

				o_HMTags.put("{{Qty}}",p_oOrderItemDetailsVO.getQty());
				o_HMTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(p_oOrderItemDetailsVO.getSbhPrice())));
				o_HMTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
				o_HMTags.put("{{CustServicePhone}}",AirlineDAO.getPhoneFormat(p_oOrderItemDetailsVO.getCustServicePhone()));


				if(lOrderTrackingList!=null && lOrderTrackingList.size()>0){
					for(OrderTrackingDetailsVO oOrderTrackingDetailsVO: lOrderTrackingList ){						
						sTrackingDetails += "<tr><td align=left bgcolor=#FFFFFF>"+oOrderTrackingDetailsVO.getTrackingDetails()+"</td><td nowrap=nowrap bgcolor=#FFFFFF align=center>"+oOrderTrackingDetailsVO.getCreate_dt()+"</td></tr>";
					}						
				}else{
					sTrackingDetailStatus = "<br/>Please check back with us after 24 hours to get your shipping details and tracking number.";
				}

				o_HMTags.put("{{TrackingDetails}}",sTrackingDetails);	
				o_HMTags.put("{{TrackingDetailStatus}}",sTrackingDetailStatus);


				if(oEmailInvoicePdfVO!=null){
					sEmailInvoicePdfContent = oEmailInvoicePdfVO.getEmailContent();
					sEmailInvoicePdfOptionalContent = oEmailInvoicePdfVO.getEmailOptionalContent();
				}				

				
				if(sTrackingDetails.trim().length()>0){
					sEmailInvoicePdfOptionalContent=sEmailInvoicePdfOptionalContent==null?"":sEmailInvoicePdfOptionalContent.trim();					
					sEmailInvoicePdfOptionalContent=replaceTagValues(o_HMTags,sEmailInvoicePdfOptionalContent,null,null);
					sEmailContent=sEmailContent==null?"":sEmailContent.trim();
					o_HMTags.put("{{TrackingDetailsTable}}",sEmailInvoicePdfOptionalContent);	
					// System.out.println("sEmailInvoicePdfOptionalContent:"+sEmailInvoicePdfOptionalContent);

				}else{
					o_HMTags.put("{{TrackingDetailsTable}}","");	
				}
				sEmailInvoicePdfContent=sEmailInvoicePdfContent==null?"":sEmailInvoicePdfContent.trim();
				sEmailContent=replaceTagValues(o_HMTags,sEmailInvoicePdfContent,null,null);
				sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			}

		}catch(Exception exception) {
			exception.printStackTrace();
			logger.debug("CustomerUtils.createBlob.Exception"+exception);
		}
		// System.out.println("Order Pdf Content : "+sEmailContent);
		return sEmailContent;
	}
}