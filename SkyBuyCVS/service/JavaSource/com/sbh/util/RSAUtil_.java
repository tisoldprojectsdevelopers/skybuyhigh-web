/**
 * 
 */
package com.sbh.util;

import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


/**
 * @author nbvk
 *
 */
public class RSAUtil_ {
	 private  static final String ALGORITHM = "RSA";
	 
	/**
	 * This method used to encrypt the given byte array to byte array of cipher text using RSA Algorithm.
	 *  
	 * @param text			byte[]
	 * @param key 			PublicKey
	 * @return cipherText 	byte[] 
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] text, PublicKey key) throws Exception
	    {
	        byte[] cipherText = null;
	        try
	        {
	            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	            cipher.init(Cipher.ENCRYPT_MODE, key);
	            cipherText = cipher.doFinal(text);
	        }
	        catch (Exception e)
	        {
	        	throw e;
	        }
	        return cipherText;
	    }

	   
	    /**
	     * This method used to encrypt the given text to cipher text using RSA Algorithm.
	     * 
	     * @param text				String
	     * @param key				PublicKey
	     * @return encryptedText 	String 
	     * @throws Exception
	     */
	    public static String encrypt(String text, PublicKey key) throws Exception
	    {
	        String encryptedText;
	        try
	        {
	            byte[] cipherText = encrypt(text.getBytes("UTF8"),key);
	            encryptedText = encodeBASE64(cipherText);
	        }
	        catch (Exception e)
	        {
	             throw e;
	        }
	        return encryptedText;
	    }

	   
	    /**
	     * This method used to decrypt the given byte array to byte array of cipher text using RSA Algorithm.
	     * 
	     * @param text				byte[]
	     * @param key				PrivateKey
	     * @return dectyptedText	byte[]
	     * @throws Exception
	     */
	    public static byte[] decrypt(byte[] text, PrivateKey key) throws Exception
	    {
	        byte[] dectyptedText = null;
	        try
	        {
	        	Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	            cipher.init(Cipher.DECRYPT_MODE, key);
	            dectyptedText = cipher.doFinal(text);
	        }
	        catch (Exception e)
	        {
	            throw e;
	        }
	        return dectyptedText;

	    }

	    
	    /**
	     * This method used to decrypt the given text to cipher text using RSA Algorithm.
	     * 
	     * @param text			String
	     * @param key			PrivateKey
	     * @return result 		String
	     * @throws Exception
	     */
	    public static String decrypt(String text, PrivateKey key) throws Exception
	    {
	        String result;
	        try
	        {
	           byte[] dectyptedText = decrypt(decodeBASE64(text),key);
	            result = new String(dectyptedText, "UTF8");
	        }
	        catch (Exception e)
	        {
	            throw e;
	        }
	        return result;

	    }

	    /**
	     * This method used to convert the generated key to string using BASE64 encoder
	     * 
	     * @param key 	Key
	     * @return
	     */
	    public static String getKeyAsString(Key key)
	    {
	        byte[] keyBytes = key.getEncoded();
	        BASE64Encoder b64 = new BASE64Encoder();
	        return b64.encode(keyBytes);
	    }

	   
	    /**
	     * This method used to convert a string into private key using BASE64 encoder .
	     * 
	     * @param key 			String
	     * @return privateKey	PrivateKey
	     * @throws Exception
	     */
	    public static PrivateKey getPrivateKeyFromString(String key) throws Exception
	    {
	        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
	        BASE64Decoder b64 = new BASE64Decoder();
	        EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(b64.decodeBuffer(key));
	        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	        return privateKey;
	    }

	  
	    /**
	     * This method used to convert a string into public key using BASE64 encoder .
	     * 
	     * @param key			String
	     * @return publicKey 	PublicKey
	     * @throws Exception
	     */
	    public static PublicKey getPublicKeyFromString(String key) throws Exception
	    {
	        BASE64Decoder b64 = new BASE64Decoder();
	        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
	        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(b64.decodeBuffer(key));
	        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
	        return publicKey;
	    }
	    /**
	     * This method used to convert the byte array to a string.
	     *  
	     * @param bytes			byte[]
	     * @return b64			String 
	     */
	    private static String encodeBASE64(byte[] bytes)
	    {
	        BASE64Encoder b64 = new BASE64Encoder();
	        return b64.encode(bytes);
	    }
	    /**
	     * This method used to convert the string to byte array.
	     * 
	     * @param text		String
	     * @return b64		byte[]
	     * @throws IOException
	     */
	    private static byte[] decodeBASE64(String text) throws IOException
	    {
	        BASE64Decoder b64 = new BASE64Decoder();
	        return b64.decodeBuffer(text);
	    }
}

