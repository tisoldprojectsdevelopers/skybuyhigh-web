/**
 * 
 */
package com.sbh.util;
import static com.sbh.client.dao.PlaceOrderDAO.getCatalogueDetailsByDate;
import static com.sbh.dao.EmailSchedulerDAO.getAllAirlineCode;
import static com.sbh.dao.EmailSchedulerDAO.getDeviceLastUpdatedDetails;
import static com.sbh.dao.EmailSchedulerDAO.getDeviceLatestUpdatedDetails;
import static com.sbh.dao.EmailSchedulerDAO.getProductDetails;
import static com.sbh.dao.EmailSchedulerDAO.sendEmailToAirlineAboutDeviceUsage;
import static com.sbh.dao.EmailSchedulerDAO.sendEmailToAirlineAboutUpdateAvailable;
import static com.sbh.util.Utils.deleteFiles;
import static com.sbh.util.Utils.diffBetweenDates;
import static com.sbh.util.Utils.replaceTagValues;
import static com.sbh.util.Utils.stringToDateTimeConversion;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.dao.EmailDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.util.generatexml.GenerateCatalogue;
import com.sbh.vo.EmailSchedulerVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.ProductDetailVO;

/**
 * @author Thapovan
 *
 */
public class EmailScheduleValidate {
	private static Logger logger = LogManager.getLogger(EmailScheduleValidate.class);
	
	public void emailScheduleValidate() {
		logger.info("EmailScheduleValidate::emailScheduleValidate::ENTER");
		
		String sUpdate = "";
		String sEmailTemplateId=null;
		String sAdditionalContent = "";
		String sEmailContent = "";
		int iCount = 0;
		long dateDiff = 0;
		List<EmailSchedulerVO> alEmailSchedulerVO = null;
		Date dOldDate = null;
		Date dNewDate = null;
		EmailVO oEmailVO =null;
		List<ProductDetailVO> alProductDetailVO = null;
		HashMap<String, String> p_hmTags =new HashMap<String, String>();
		try {
			InetAddress oInetAddress = InetAddress.getLocalHost();
			
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			sEmailTemplateId = oBundle.getString("email.device.updated.details.additional.content.templateId");
			
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();

			//get email content details
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);	
			
			alEmailSchedulerVO = getDeviceLastUpdatedDetails();
			if(alEmailSchedulerVO != null && !alEmailSchedulerVO.isEmpty()) {
				for(EmailSchedulerVO oEmailSchedulerVO : alEmailSchedulerVO) {
					sAdditionalContent = "";
					iCount = 0;
					
					// System.out.println("Device ID:.."+oEmailSchedulerVO.getDeviceId());
					// System.out.println("Device Code:.."+oEmailSchedulerVO.getDeviceCode());
					
					dOldDate = stringToDateTimeConversion(oEmailSchedulerVO.getLastUpdatedDate());
					dNewDate = stringToDateTimeConversion(oEmailSchedulerVO.getAdminUpdatedDate());
					if(dOldDate != null && dNewDate != null) {
						dateDiff = diffBetweenDates(dOldDate, dNewDate);
						if(dateDiff > 0) {
							sAdditionalContent = appendString(sAdditionalContent, oEmailVO.getEmailOptionalContent(), oEmailSchedulerVO.getCurrentAdminVersion(), oEmailSchedulerVO.getLatestAdminVersion(),"SkyBuyHigh<sup>High</sup> Admin Application");
							iCount++;
						}
					}else if(dNewDate != null) {
						sAdditionalContent = appendString(sAdditionalContent, oEmailVO.getEmailOptionalContent(), oEmailSchedulerVO.getCurrentAdminVersion(), oEmailSchedulerVO.getLatestAdminVersion(),"SkyBuyHigh<sup>High</sup> Admin Application");
						iCount++;
					}
					
					dNewDate = stringToDateTimeConversion(oEmailSchedulerVO.getShoppingcartUpdateDate());
					if(dOldDate != null && dNewDate != null) {
						dateDiff = diffBetweenDates(dOldDate, dNewDate);
						if(dateDiff > 0) {
							sAdditionalContent = appendString(sAdditionalContent, oEmailVO.getEmailOptionalContent(), oEmailSchedulerVO.getCurrentCatalogueVersion(), oEmailSchedulerVO.getLatestCatalogueVersion(),"SkyBuyHigh<sup>High</sup> Shopping Cart");
							iCount++;
						}
					}else if(dNewDate != null) {
						sAdditionalContent = appendString(sAdditionalContent, oEmailVO.getEmailOptionalContent(), oEmailSchedulerVO.getCurrentCatalogueVersion(), oEmailSchedulerVO.getLatestCatalogueVersion(),"SkyBuyHigh<sup>High</sup> Shopping Cart");
						iCount++;
					}
					
					alProductDetailVO = getProductDetails(oEmailSchedulerVO.getLastDownloadDate(), oEmailSchedulerVO.getAirId());
					if(alProductDetailVO != null && alProductDetailVO.size() > 0) {
						sAdditionalContent = appendString(sAdditionalContent, oEmailVO.getEmailOptionalContent(), oEmailSchedulerVO.getLastDownloadDate(), "New Catalogue Available","SkyBuyHigh<sup>High</sup> Catalogue");
						iCount++;
					}
					
					if(iCount > 1) {
						sUpdate = " Updates are available";
					}else {
						sUpdate = " Update is available";
					}
					 
					if(sAdditionalContent != null && sAdditionalContent.trim().length() > 0) {
						p_hmTags.put("{{UpdateDetails}}", sAdditionalContent);
						p_hmTags.put("{{DeviceCode}}", oEmailSchedulerVO.getDeviceCode());
						sEmailContent = oEmailVO.getEmailContent();
						sEmailContent=sEmailContent==null?"":sEmailContent.trim();
						sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
						sEmailContent=sEmailContent==null?"":sEmailContent.trim();
						
						sendEmailToAirlineAboutUpdateAvailable(oEmailSchedulerVO, sEmailContent, sUpdate, alProductDetailVO,oInetAddress.getHostName(),"");
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("EmailScheduleValidate::emailScheduleValidate::EXCEPTION"+e.getMessage());
		}
		
		logger.info("EmailScheduleValidate::emailScheduleValidate::EXIT");
	}
	
	public void emailScheduleValidateDeviceUsed() {
		logger.info("EmailScheduleValidate::emailScheduleValidateDeviceUsed::ENTER");
		
		long dateDiff = 0;
		List<EmailSchedulerVO> alEmailSchedulerVO = null;
		Date dLastUpdatedDate = null;
		Date dToday = new Date();
		try {
			alEmailSchedulerVO = getDeviceLatestUpdatedDetails();
			InetAddress oInetAddress = InetAddress.getLocalHost();
			if(alEmailSchedulerVO != null && !alEmailSchedulerVO.isEmpty()) {
				for(EmailSchedulerVO oEmailSchedulerVO : alEmailSchedulerVO) {
					
					// System.out.println("Device ID:.."+oEmailSchedulerVO.getDeviceId());
					// System.out.println("Device Code:.."+oEmailSchedulerVO.getDeviceCode());
					
					dLastUpdatedDate = stringToDateTimeConversion(oEmailSchedulerVO.getLastUpdatedDate());
					if(dLastUpdatedDate != null) {
						dateDiff = diffBetweenDates(dLastUpdatedDate, dToday);
						if(dateDiff > 0) {
							sendEmailToAirlineAboutDeviceUsage(oEmailSchedulerVO,oInetAddress.getHostName(),"");
						}
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.info("EmailScheduleValidate::emailScheduleValidateDeviceUsed::EXCEPTION"+e.getMessage());
		}
		
		logger.info("EmailScheduleValidate::emailScheduleValidateDeviceUsed::EXIT");
	}
	
	public void generateFullCatalogue() {
		logger.info("EmailScheduleValidate::generateFullCatalogue::ENTER");
		
		String sAirCode = null;
		String sDeviceId = "";
		String sCommaSepCoverflowProdIds = null;
		String sIsAirlineChanged = "YES";
		String sCatelogueXmlFilePath = null;
		String sLastDownloadedCateDate = "FullCatalogue";
		String sCatalogueFileName = null;
		String sCatalogueFolderPath = null;
		String sZipFolderPath = null;
		String sCatalogueZipFolder = null;
		String sCatalogueName = null;
		GenerateCatalogueZip oGenerateCatalogueZip =  null;
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		File oCatalogueDownloadFile = null;
		File oCatalogueXmlFile =  null;
		File oCoverflowXmlFile =  null;
		
		File fFullCatalogue = null;
		File fXML = null;
		File fCatalogueZip = null;
		List<String> saAirCode = null;
		ArrayList<String> alCoverflowProdIds = null;
		try {
			sCatalogueFolderPath = System.getProperty("CatalogueFolderPath").trim();
			sZipFolderPath = System.getProperty("FullCatalogueZipFolderPath").trim();
			sCatalogueZipFolder = sCatalogueFolderPath +"/DownloadCatalogue/"+sAirCode;
			/*
			 * Get all airline code present in the database.
			 */
			saAirCode = getAllAirlineCode();
			if(saAirCode != null && !saAirCode.isEmpty()) {
				fFullCatalogue = new File(sZipFolderPath);
				if(!fFullCatalogue.exists()) {
					fFullCatalogue.mkdirs();
				}
				for(String sAirlineId:saAirCode) {
					sAirCode = sAirlineId;
					sAirCode = sAirCode==null?"":sAirCode.trim();
					sLastDownloadedCateDate = sLastDownloadedCateDate == null ?" ":sLastDownloadedCateDate.trim();
					sCatelogueXmlFilePath = sCatalogueFolderPath+"/XML/"+sAirCode;
					sCatalogueZipFolder = sCatalogueFolderPath +"/DownloadCatalogue/"+sAirCode;
					
					fXML = new File(sCatelogueXmlFilePath);
					if(!fXML.exists()) {
						fXML.mkdirs();
					}
					fCatalogueZip = new File(sCatalogueZipFolder);
					if(!fCatalogueZip.exists()) {
						fCatalogueZip.mkdirs();
					}
					
					//UploadSkyBuyCatalogueDAO.isDownloadCatalogueAvailable(sLastDownloadedCateDate,sAirCode,sIsAirlineChanged);
					/*
					 * Generate the Catalogue XML file.
					 */
					alCoverflowProdIds = oGenerateCatalogue.generateCatalogueXML(sCatelogueXmlFilePath,"CLIENT",sAirCode,"","","","","",sDeviceId,"");	
					sCatalogueFileName = System.getProperty("CatalogueFileName").trim();
					sCommaSepCoverflowProdIds = Utils.listToCommaSepertedString(alCoverflowProdIds);
					sCatalogueName = sCatalogueFileName+".zip";
					sCatalogueFileName = sCatalogueFileName+"_"+sAirCode+".zip";
					
					oGenerateCatalogueZip = new GenerateCatalogueZip(sZipFolderPath,sCatalogueFileName);
					/*
					 * Generate the Catalogue zip file.
					 */
					 getCatalogueDetailsByDate(sLastDownloadedCateDate,sCatalogueFolderPath,sCatalogueName,sAirCode,sDeviceId,sCommaSepCoverflowProdIds,sIsAirlineChanged,sCatelogueXmlFilePath,sCatalogueZipFolder,null);
					/*
					 * Delete the generated XML files.
					 */
					 String sCataogueFileName = sCatalogueZipFolder+sCatalogueName;
					 oGenerateCatalogueZip.addFile(sCatalogueZipFolder, sCatalogueName, sCatalogueName);
					 oGenerateCatalogueZip.finish();
					 
					if(fXML != null && fXML.exists()) {
						deleteFiles(fXML);
					}
					if(fCatalogueZip != null && fCatalogueZip.exists()) {
						deleteFiles(fCatalogueZip);
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("EmailScheduleValidate::generateFullCatalogue::EXCEPTION"+e.getMessage());
		}finally{
			if(oCatalogueXmlFile != null && oCatalogueXmlFile.exists())
				oCatalogueXmlFile.delete();
			if(oCatalogueXmlFile != null && oCoverflowXmlFile.exists())
				oCoverflowXmlFile.delete();
			if(oCatalogueDownloadFile != null && oCatalogueDownloadFile.exists())
				oCatalogueDownloadFile.delete();
			if(fXML != null && fXML.exists()) {
				deleteFiles(fXML);
			}
		}
		
		logger.info("EmailScheduleValidate::generateFullCatalogue::EXIT");
	}
	
	
	public void generateDemoCatalogueXML() {
		logger.info("EmailScheduleValidate::generateDemoCatalogueXML::ENTER");
		String sDemoCatalogueXMLPath = null;
		
		GenerateCatalogue oGenerateCatalogue = new GenerateCatalogue();
		
		try {
			
            sDemoCatalogueXMLPath = System.getProperty("DemoCatalogueXMLPath");
            sDemoCatalogueXMLPath = sDemoCatalogueXMLPath == null?"":sDemoCatalogueXMLPath.trim();
			/*
			 * Generate the Catalogue XML file for Demo Catalogue.
			 */
			oGenerateCatalogue.generateCatalogueXML(sDemoCatalogueXMLPath,"Admin","0","Admin",sDemoCatalogueXMLPath,"","","DEMONSTRATIONCATALOGUE","","");
			//alCoverflowProdIds = oGenerateCatalogue.generateCatalogueXML(sCatelogueXmlFilePath,"CLIENT",sAirCode,"","","","","",sDeviceId,"");	
					
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("EmailScheduleValidate::generateDemoCatalogueXML::EXCEPTION"+e.getMessage());
		}
		
		logger.info("EmailScheduleValidate::generateDemoCatalogueXML::EXIT");
	}
	private static String appendString(String p_sOriginalContent, String p_sAdditionalContent, String p_sCurrentVersion, String p_sLatestVersion, String p_sUpdateAvailableType) 
	throws Exception {
		String sReturnContent = "";
		HashMap<String, String> p_hmTags =new HashMap<String, String>();
		
		if(p_sOriginalContent != null && p_sOriginalContent.trim().length() > 0) {
			sReturnContent = p_sOriginalContent + p_sAdditionalContent;
		}else {
			sReturnContent = p_sAdditionalContent;
		}
		try {
			p_hmTags.put("{{UpdateAvailableType}}",p_sUpdateAvailableType);
			p_hmTags.put("{{CurrentVersion}}",p_sCurrentVersion);
			p_hmTags.put("{{LatestVersion}}",p_sLatestVersion);
			
			sReturnContent=sReturnContent==null?"":sReturnContent.trim();
			sReturnContent=replaceTagValues(p_hmTags,sReturnContent,null,null);
			sReturnContent=sReturnContent==null?"":sReturnContent.trim();
		}catch(Exception e) {
			throw e;
		}
		return sReturnContent;
	}
}