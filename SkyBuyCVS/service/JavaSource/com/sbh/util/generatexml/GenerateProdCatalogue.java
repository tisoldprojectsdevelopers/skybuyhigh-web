package com.sbh.util.generatexml;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sbh.util.DBConnection;
import com.sbh.vo.ProductDetailsVO;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;




public class GenerateProdCatalogue {
	private static Logger logger = LogManager.getLogger(GenerateProdCatalogue.class);	

	ArrayList<ProductDetailsVO> alProductInfo = null;
	Document dom;

	public GenerateProdCatalogue() {
		//Get a DOM object
		createDocument();
	}

	public String createXML(String p_sProdId,String p_sVendId,String p_sCateId,String p_sDestPath,String p_sOwnerType){	
		logger.info("GenerateProdCatalogue::createXML:ENTER");	
		String sProductXmlContent = null;
		try{	
			//load vendor product details
			vendorProductDetails(p_sProdId,p_sVendId,p_sCateId,p_sOwnerType);
			//Create Dom Object
			createDOMTree();

			//write the xml document to String 
			sProductXmlContent= printXmlToString();
			sProductXmlContent=sProductXmlContent.replaceAll("\"","&quot;");

			//printToFile(p_sDestPath);
		
			logger.info("GenerateProdCatalogue::createXML:EXIT");
		}
		catch(Exception e){
			logger.error("GenerateProdCatalogue::createXML:EXCEPTION "+e.getMessage());
			// System.out.println("GenerateProdCatalogue::createXML:Exception"+e.getMessage());	
		}	
		return sProductXmlContent;
	}

	/*private void loadData(){
		Connection con = null;
		ResultSet rs = null;
		CallableStatement cstmt=null;
		String sProdDetailsQuery="{call usp_sbh_get_prod_details()}";
		ProductDetailsVO oProductDetailsVO = null;
		hmProdDetails = new HashMap();		

		try{
			con=DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sProdDetailsQuery);
			rs = cstmt.executeQuery();
			while(rs.next()){
				oProductDetailsVO = new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));			
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setVendId(rs.getString("vend_id"));
				String sCateName = rs.getString("cate_name");
				oProductDetailsVO.setCateName(sCateName);
				oProductDetailsVO.setVendName(rs.getString("vend_name"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setProdStatus(rs.getString("prod_status"));
				oProductDetailsVO.setTitle(rs.getString("title"));
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));
				oProductDetailsVO.setVendPrice(rs.getString("vend_price"));
				oProductDetailsVO.setSbhPrice(rs.getString("sbh_price"));
				oProductDetailsVO.setImgPath(rs.getString("img_path"));

				alProdDetails = (ArrayList) hmProdDetails.get(sCateName);
				if(alProdDetails == null ){
					alProdDetails = new ArrayList();
					alProdDetails.add(oProductDetailsVO);					
				}else{
					alProdDetails.add(oProductDetailsVO);		
				}
				hmProdDetails.put(sCateName, alProdDetails);

			}


		}catch(Exception e){
		 // System.out.println("GenerateXML::loadData:Exception"+e.getMessage());	
		}


	}*/

	private void vendorProductDetails(String p_sProdId,String p_sVendId,String p_sCateId,String p_sOwnerType){
		logger.info("GenerateProdCatalogue::vendorProductDetails:ENTER");	

		Connection con = null;
		ResultSet rs = null;
		CallableStatement cstmt=null;
		String sProdDetailsQuery="{call usp_sbh_get_vend_prod_details(?,?,?)}";
		ProductDetailsVO oProductDetailsVO = null;		
		alProductInfo = new ArrayList();		
		try{
			con=DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sProdDetailsQuery);
			cstmt.setString(1, p_sVendId);
			cstmt.setString(2, p_sCateId);				
			cstmt.setString(3, p_sOwnerType);		

			rs = cstmt.executeQuery();
			while(rs.next()){
				oProductDetailsVO = new ProductDetailsVO();				
				oProductDetailsVO.setProdId(rs.getString("prod_id"));			
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setSeqId(rs.getString("seq_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				String sCateName = rs.getString("cate_name");
				oProductDetailsVO.setCateName(sCateName);
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));

				String sVendorPrice = rs.getString("vend_price");
				sVendorPrice =sVendorPrice==null?"0.00":sVendorPrice.trim();
				oProductDetailsVO.setVendPrice(sVendorPrice);

				String sSbhPrice=rs.getString("sbh_price");
				sSbhPrice = sSbhPrice ==null?sVendorPrice:sSbhPrice.trim();
				oProductDetailsVO.setSbhPrice(sSbhPrice);

				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
				oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));
				oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));
				oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));
				oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));
				oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));
				oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));
				if(p_sProdId.equals(oProductDetailsVO.getProdId()))
					oProductDetailsVO.setShowProd("Yes");
				else
					oProductDetailsVO.setShowProd("No");

				alProductInfo.add(oProductDetailsVO);		

			}

			logger.info("GenerateProdCatalogue::vendorProductDetails:EXIT");	
		}catch(Exception e){
			logger.error("GenerateProdCatalogue::vendorProductDetails:EXCEPTION "+e.getMessage());
			// System.out.println("GenerateXML::loadData:Exception"+e.getMessage());	
		}		

	}

	/**
	 * Using JAXP in implementation independent manner create a document object
	 * using which we create a xml tree in memory
	 */
	public void createDocument() {
		logger.info("GenerateProdCatalogue::createDocument:ENTER");	
		//get an instance of factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			//get an instance of builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//create an instance of DOM
			dom = db.newDocument();

		}catch(ParserConfigurationException pce) {
			logger.error("GenerateProdCatalogue::createDocument:EXCEPTION "+pce.getMessage());
			// System.out.println("Error while trying to instantiate DocumentBuilder " + pce);

		}
		logger.info("GenerateProdCatalogue::createDocument:EXIT");	
	}

	/**
	 * The real workhorse which creates the XML structure
	 */
	private void createDOMTree(){
		//String sImageUploadPath = System.getProperty("ImageUploadPath").trim();		
		logger.info("GenerateProdCatalogue::createDOMTree:ENTER");	
		//create the root element <Categories>
		try{
			Element rootEle = dom.createElement("Categories");
			dom.appendChild(rootEle);	
			Element cateEle = dom.createElement("Category");
			for(ProductDetailsVO oProductDetailsVO: alProductInfo){
				Element prodEle = createProdElement(oProductDetailsVO);
				cateEle.setAttribute("id", oProductDetailsVO.getCateId());
				cateEle.setAttribute("seqId", oProductDetailsVO.getSeqId());
				cateEle.appendChild(prodEle);
			}
			rootEle.appendChild(cateEle);


		}catch(Exception e) {
			logger.error("GenerateProdCatalogue::createDOMTree:EXCEPTION "+e.getMessage());

		}
		logger.info("GenerateProdCatalogue::createDOMTree:EXIT");	
	}




	private Element createProdElement(ProductDetailsVO oProductDetailsVO){

		logger.info("GenerateProdCatalogue::createProdElement:ENTER");	
		String sView1ImageURLPath="",sView1ThumbURLPath = "",sView1MedViewPath = "",sView1LargeViewPath ="";
		String sView3MedViewPath ="",sView3LargeViewPath="",sView3ThumbURLPath="",sView3ImageURLPath="";
		String sView2LargeViewPath="",sView2ThumbURLPath="",sView2MedViewPath="",sView2ImageURLPath="";

		String sImageViewPath = System.getProperty("ImageViewPath").trim();	
		
		//create Product element 
		Element prodEle = dom.createElement("Product");
		prodEle.setAttribute("OwnerId", oProductDetailsVO.getOwnerId());		

		//create Prod id element and Prod id text node and attach it to Product Element
		Element prodIdEle = dom.createElement("ProductId");
		Text prodIdText = dom.createTextNode(oProductDetailsVO.getProdId());
		prodIdEle.appendChild(prodIdText);
		prodEle.appendChild(prodIdEle);

		Element prodCodeEle = dom.createElement("ProductCode");
		Text prodCodeText = dom.createTextNode(oProductDetailsVO.getProdCode());
		prodCodeEle.appendChild(prodCodeText);
		prodEle.appendChild(prodCodeEle);

		//create brand element and brand text node and attach it to Product Element
		Element brandEle = dom.createElement("BRAND");
		Text brandText = dom.createTextNode(oProductDetailsVO.getBrandName());
		brandEle.appendChild(brandText);
		prodEle.appendChild(brandEle);	


		//create title element and title text node and attach it to Product Element
		Element titleEle = dom.createElement("TITLE");
		Text titleText = dom.createTextNode(oProductDetailsVO.getProdTitle());
		titleEle.appendChild(titleText);
		prodEle.appendChild(titleEle);

		/*//create Short Desc element and Short Desc text node and attach it to Product Element
		Element shortDescEle = dom.createElement("ShortDiscription");
		Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		shortDescEle.appendChild(shortDescText);
		prodEle.appendChild(shortDescEle);

		//create Long Desc element and Long Desc text node and attach it to Product Element
		Element longDescEle = dom.createElement("LongDiscription");
		Element htmlLongEle = dom.createElement("html");
		Text longDescText = dom.createTextNode((oProductDetailsVO.getLongDesc()));	
		htmlLongEle.appendChild(longDescText);	
		longDescEle.appendChild(htmlLongEle);
		prodEle.appendChild(longDescEle);*/

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element shortDescEle = dom.createElement("ShortDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cShortDesc = dom.createCDATASection(oProductDetailsVO.getShortDesc());
		shortDescEle.appendChild(cShortDesc);
		prodEle.appendChild(shortDescEle);

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element longDescEle = dom.createElement("LongDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cLongDesc = dom.createCDATASection(oProductDetailsVO.getLongDesc());
		longDescEle.appendChild(cLongDesc);
		prodEle.appendChild(longDescEle);


		//create Caption element and Caption text node and attach it to Product Element
		//Element captionEle = dom.createElement("Caption");
		//Text captionText = dom.createTextNode(oProductDetailsVO.getLongDesc());
		//longDescEle.appendChild(longDescText);
		//prodEle.appendChild(captionEle);

		//create Price element and Price text node and attach it to Product Element
		Element priceEle = dom.createElement("Price");
		Text priceText = dom.createTextNode("$"+oProductDetailsVO.getVendPrice());
		priceEle.appendChild(priceText);
		prodEle.appendChild(priceEle);

		//create Sky Buy Price element and Sky Buy Price text node and attach it to Product Element
		Element skyBuyPriceEle = dom.createElement("SkyBuyPrice");
		Text skyBuyPriceText = dom.createTextNode("$"+oProductDetailsVO.getSbhPrice());
		skyBuyPriceEle.appendChild(skyBuyPriceText);
		prodEle.appendChild(skyBuyPriceEle);		

		// Starts Main Img Url // 
		Element mainImageURLEle = dom.createElement("MainImageURL");
		String sMainImageURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getMainImgType();
		Text mainImageURLText = dom.createTextNode(sMainImageURLPath);
		mainImageURLEle.appendChild(mainImageURLText);
		prodEle.appendChild(mainImageURLEle);

		Element mainThumbURLEle = dom.createElement("MainThumbURL");
		String sMainThumbURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType();
		Text mainThumbURLText = dom.createTextNode(sMainThumbURLPath);
		mainThumbURLEle.appendChild(mainThumbURLText);
		prodEle.appendChild(mainThumbURLEle);

		Element mainMedViewEle = dom.createElement("MainMedView");
		String sMainMedViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getMainImgType();
		Text mainMedViewText = dom.createTextNode(sMainMedViewPath);
		mainMedViewEle.appendChild(mainMedViewText);
		prodEle.appendChild(mainMedViewEle);

		Element mainLargeViewEle = dom.createElement("MainLargeView");
		String sMainLargeViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getMainImgType();
		Text mainLargeViewText = dom.createTextNode(sMainLargeViewPath);
		mainLargeViewEle.appendChild(mainLargeViewText);
		prodEle.appendChild(mainLargeViewEle);

		// Ends Main Img Url // 
		// Starts View1 Img Url // 

		Element view1ImageURLEle = dom.createElement("ViewOneImageURL");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1ImageURLPath =sImageViewPath+ oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView1ImgType();
		else
			sView1ImageURLPath="";
		Text view1ImageURLText = dom.createTextNode(sView1ImageURLPath);
		view1ImageURLEle.appendChild(view1ImageURLText);
		prodEle.appendChild(view1ImageURLEle);

		Element view1ThumbURLEle = dom.createElement("ViewOneThumbURL");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1ThumbURLPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType();
		else
			sView1ThumbURLPath = "";
		Text view1ThumbURLText = dom.createTextNode(sView1ThumbURLPath);
		view1ThumbURLEle.appendChild(view1ThumbURLText);
		prodEle.appendChild(view1ThumbURLEle);

		Element view1MedViewEle = dom.createElement("ViewOneMedView");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1MedViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView1ImgType();
		else
			sView1MedViewPath = "";
		Text view1MedViewText = dom.createTextNode(sView1MedViewPath);
		view1MedViewEle.appendChild(view1MedViewText);
		prodEle.appendChild(view1MedViewEle);

		Element view1LargeViewEle = dom.createElement("ViewOneLargeView");
		if(oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0)
			sView1LargeViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView1ImgType();
		else
			sView1LargeViewPath ="";
		Text view1LargeViewText = dom.createTextNode(sView1LargeViewPath);
		view1LargeViewEle.appendChild(view1LargeViewText);
		prodEle.appendChild(view1LargeViewEle);

		// Ends View1 Img Url // 


		// Starts View2 Img Url // 
		Element view2ImageURLEle = dom.createElement("ViewTwoImageURL");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2ImageURLPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView2ImgType();
		else
			sView2ImageURLPath ="";
		Text view2ImageURLText = dom.createTextNode(sView2ImageURLPath);
		view2ImageURLEle.appendChild(view2ImageURLText);
		prodEle.appendChild(view2ImageURLEle);

		Element view2ThumbURLEle = dom.createElement("ViewTwoThumbURL");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2ThumbURLPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType();
		else
			sView2ThumbURLPath = "";
		Text view2ThumbURLText = dom.createTextNode(sView2ThumbURLPath);
		view2ThumbURLEle.appendChild(view2ThumbURLText);
		prodEle.appendChild(view2ThumbURLEle);

		Element view2MedViewEle = dom.createElement("ViewTwoMedView");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2MedViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView2ImgType();
		else
			sView2MedViewPath = "";
		Text view2MedViewText = dom.createTextNode(sView2MedViewPath);
		view2MedViewEle.appendChild(view2MedViewText);
		prodEle.appendChild(view2MedViewEle);                 


		Element view2LargeViewEle = dom.createElement("ViewTwoLargeView");
		if(oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0)
			sView2LargeViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView2ImgType();
		else
			sView2LargeViewPath ="";
		Text view2LargeViewText = dom.createTextNode(sView2LargeViewPath);
		view2LargeViewEle.appendChild(view2LargeViewText);
		prodEle.appendChild(view2LargeViewEle);

		// Ends View2 Img Url // 

		// Starts View3 Img Url // 
		Element view3ImageURLEle = dom.createElement("ViewThreeImageURL");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3ImageURLPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView3ImgType();
		else
			sView3ImageURLPath="";
		Text view3ImageURLText = dom.createTextNode(sView3ImageURLPath);
		view3ImageURLEle.appendChild(view3ImageURLText);
		prodEle.appendChild(view3ImageURLEle);

		Element view3ThumbURLEle = dom.createElement("ViewThreeThumbURL");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3ThumbURLPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType();
		else
			sView3ThumbURLPath="";
		Text view3ThumbURLText = dom.createTextNode(sView3ThumbURLPath);
		view3ThumbURLEle.appendChild(view3ThumbURLText);
		prodEle.appendChild(view3ThumbURLEle);

		Element view3MedViewEle = dom.createElement("ViewThreeMedView");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3MedViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView3ImgType();
		else
			sView3MedViewPath ="";
		Text view3MedViewText = dom.createTextNode(sView3MedViewPath);
		view3MedViewEle.appendChild(view3MedViewText);
		prodEle.appendChild(view3MedViewEle);

		Element view3LargeViewEle = dom.createElement("ViewThreeLargeView");
		if(oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0)
			sView3LargeViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView3ImgType();
		else
			sView3LargeViewPath = "";

		Text view3LargeViewText = dom.createTextNode(sView3LargeViewPath);
		view3LargeViewEle.appendChild(view3LargeViewText);
		prodEle.appendChild(view3LargeViewEle);

		// Ends View3 Img Url //   

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element showProdEle = dom.createElement("ShowProd");
		Text showProdText = dom.createTextNode(oProductDetailsVO.getShowProd());
		showProdEle.appendChild(showProdText);
		prodEle.appendChild(showProdEle);

		logger.info("GenerateProdCatalogue::createProdElement:EXIT");	
		return prodEle;

	}

	/**`
	 * prints the XML document to file.
	 */
	private void printToFile(String p_sDestPath){
		logger.info("GenerateProdCatalogue::printToFile:ENETR");	
		try
		{
			//print
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			String sProdCatalogueFileName = System.getProperty("ProdCatalogueFileName").trim();

			File oFile=new File(p_sDestPath+"\\"+sProdCatalogueFileName);

			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(
					new FileOutputStream(oFile), format);

			serializer.serialize(dom);		
			logger.info("GenerateProdCatalogue::printToFile:EXIT");	
		} catch(Exception ie) {
			logger.error("GenerateProdCatalogue::printToFile:EXCEPTION "+ie.getMessage());
			ie.printStackTrace();
		}
	}	

	/**
	 * prints the XML document to String.
	 */
	private String printXmlToString(){
		logger.info("GenerateProdCatalogue::printXmlToString:ENETR");	
		String sProductXmlContent =null;
		StringBuilder stringBuilder = null;

		ByteArrayOutputStream stream = 
			new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		try
		{		
			//Transformer transformer = TransformerFactory.newInstance().newTransformer();
			//transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			//initialize StreamResult with File object to save to file
			//StreamResult result = new StreamResult(new StringWriter());
			//DOMSource source = new DOMSource(dom);

			//transformer.transform(source, result);
			//  sProductXmlContent = result.getWriter().toString();
			//	// System.out.println(sProductXmlContent);


			//outputformat.setIndent(4);
			//outputformat.setIndenting(true);

			outputformat.setPreserveSpace(false);
			XMLSerializer serializer = new XMLSerializer();
			serializer.setOutputFormat(outputformat);
			serializer.setOutputByteStream(stream);
			//serializer.asDOMSerializer();
			serializer.serialize(dom.getDocumentElement());
			stringBuilder = new StringBuilder(stream.toString());
			stringBuilder.trimToSize();
			String sDocType=stringBuilder.substring(stringBuilder.indexOf("<"), stringBuilder.indexOf(">")+1);
			stringBuilder.replace(stringBuilder.indexOf("<"),stringBuilder.indexOf(">")+1,"");
			stringBuilder.trimToSize();
			stringBuilder.replace(0, stringBuilder.indexOf("<"), "");
			sProductXmlContent=stringBuilder.toString();
			sProductXmlContent=sDocType+sProductXmlContent;




			logger.info("GenerateProdCatalogue::printXmlToString:EXIT");	
		} catch(Exception ie) {
			logger.error("GenerateProdCatalogue::printXmlToString:EXCEPTION "+ie.getMessage());
			ie.printStackTrace();
		}
		return sProductXmlContent;
	}	

}


