package com.sbh.util.generatexml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sbh.dao.ImageScale;
import com.sbh.util.DBConnection;
import com.sbh.vo.AdminReturnPolicyVO;
import com.sbh.vo.PartnerVO;
import com.sbh.vo.ProductDetailsVO;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class GenerateCatalogue
{
	private static Logger logger = LogManager.getLogger(GenerateCatalogue.class);	

	public Document createDocument()
	{
		logger.info("GenerateCatalogue::createDocument:ENTER");	
		Document dom = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}
		catch(ParserConfigurationException pce)
		{
			logger.error("GenerateCatalogue::createDocument:EXCEPTION "+pce.getMessage());
			// System.out.println((new StringBuilder("Error while trying to instantiate DocumentBuilder ")).append(pce).toString());
			System.exit(1);
		}
		logger.info("GenerateCatalogue::createDocument:EXIT");	
		return dom;
	}

	public ArrayList generateCatalogueXML(String p_sDestPath,String p_sUserType,String p_sOwnerId,String p_sUserId,String p_sServerPath,String p_sProdId,String p_sCateId,String p_CatalogueType,String p_sDeviceId, String p_sSeqId)
	throws Exception {
		logger.info("GenerateCatalogue::generateCatalogueXML:ENTER");		
		ArrayList alCoverflow = null,alCoverflowProdIds= null,alPartnerDetails = null;
		Document dom = createDocument();
		Document domCover = createDocument();
		Document domPartner = createDocument();
		List<AdminReturnPolicyVO> alAdminReturnPolicyVO = getReturnPolicyMessages();
		HashMap hmProdDetails = getProductData(p_sUserType,p_sOwnerId,p_sProdId,p_sCateId,p_CatalogueType);
		alCoverflow = (ArrayList)hmProdDetails.get("Coverflow");
		hmProdDetails.remove("Coverflow");
		alPartnerDetails = (ArrayList)hmProdDetails.get("PartnerDetails");
		hmProdDetails.remove("PartnerDetails");
		createCatalogue(dom, hmProdDetails, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId,p_sProdId,p_sSeqId,alAdminReturnPolicyVO);
		alCoverflowProdIds = createCatalogueForCover(domCover, alCoverflow, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId);
		createCatalogueForPartnerDetails(domPartner, alPartnerDetails, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_sCateId,p_CatalogueType,p_sOwnerId,p_sDeviceId);

		logger.info("GenerateCatalogue::generateCatalogueXML:Catalogue XML Generated file successfully.");	
		logger.info("GenerateCatalogue::generateCatalogueXML:EXIT");
		return alCoverflowProdIds;
	}
	private List<AdminReturnPolicyVO> getReturnPolicyMessages() throws Exception {
		logger.info("GenerateCatalogue::getReturnPolicyMessages:ENTER");

		String sQuery = "{call usp_sbh_get_skybuy_disclaimer_message()}";
		List<AdminReturnPolicyVO> alAdminReturnPolicyVO = new ArrayList<AdminReturnPolicyVO>();
		AdminReturnPolicyVO oAdminReturnPolicyVO = null;
		CallableStatement cstmt = null;
		Connection con = null;
		ResultSet rs = null;

		try {
			con = DBConnection.getSQL2005Connection();
			cstmt = con.prepareCall(sQuery);
			rs = cstmt.executeQuery();
			if(rs != null) {
				while(rs.next()) {
					oAdminReturnPolicyVO = new AdminReturnPolicyVO();
					oAdminReturnPolicyVO.setId(rs.getString("id"));
					oAdminReturnPolicyVO.setCode(rs.getString("code"));
					oAdminReturnPolicyVO.setText(rs.getString("text"));
					oAdminReturnPolicyVO.setStatus(rs.getString("status"));
					oAdminReturnPolicyVO.setComments(rs.getString("comments"));
					oAdminReturnPolicyVO.setCreateDate(rs.getString("sbh_create_dt"));
					oAdminReturnPolicyVO.setCreateId(rs.getString("sbh_create_id"));
					oAdminReturnPolicyVO.setCreateDate(rs.getString("sbh_update_dt"));
					oAdminReturnPolicyVO.setCreateId(rs.getString("sbh_update_id"));
					alAdminReturnPolicyVO.add(oAdminReturnPolicyVO);
				}
			}
		}catch(Exception e) {
			logger.info("GenerateCatalogue::getReturnPolicyMessages:EXCEPTION"+e.getMessage());
		}finally {
			if(rs != null) {
				rs.close();
			}
			if(cstmt != null){
				cstmt.close();
			}
			if(con != null) {
				con.close();
			}
		}

		logger.info("GenerateCatalogue::getReturnPolicy:EXIT");	
		return alAdminReturnPolicyVO;
	}
	private HashMap getProductData(String p_sUserType,String p_sOwnerId,String p_sProdId,String p_Cate_Id,String p_CatalogueType)
	throws Exception {
		logger.info("GenerateCatalogue::getProductData:ENTER");	
		Connection con = null;
		ResultSet rs = null;
		CallableStatement cstmt = null;
		List<ProductDetailsVO> alCoverflowProd = new ArrayList<ProductDetailsVO>();
		List<PartnerVO> alPartnerDetails = new ArrayList<PartnerVO>();
		String sProdDetailsQuery = "{call usp_sbh_get_prod_details(?,?,?)}";
		logger.info("GenerateCatalogue::getProductData:Query:"+sProdDetailsQuery);
		logger.info("GenerateCatalogue::getProductData:User Type:"+p_sUserType);
		logger.info("GenerateCatalogue::getProductData:User Id:"+p_sOwnerId);
		ProductDetailsVO oProductDetailsVO = null;
		HashMap hmProdDetails = new HashMap();
		ArrayList alProdDetails = null;
		String sDBData = null;
		PartnerVO oPartnerVO = null;
		try
		{
			con = DBConnection.getSQL2005Connection();
			cstmt = con.prepareCall(sProdDetailsQuery);
			cstmt.setString(1, p_sUserType);
			cstmt.setString(2, p_sOwnerId);
			cstmt.registerOutParameter(3, Types.VARCHAR);
			String sCateName;
			for(rs = cstmt.executeQuery(); rs.next(); hmProdDetails.put(sCateName, alProdDetails))
			{
				oProductDetailsVO = new ProductDetailsVO();
				oProductDetailsVO.setProdId(rs.getString("prod_id"));
				oProductDetailsVO.setCateId(rs.getString("cate_id"));
				oProductDetailsVO.setSeqId(rs.getString("seq_id"));
				oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
				oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
				sCateName = rs.getString("cate_name");
				oProductDetailsVO.setCateName(sCateName);
				oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
				oProductDetailsVO.setProdCode(rs.getString("prod_code"));
				oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
				oProductDetailsVO.setProdTitle(rs.getString("prod_title"));
				oProductDetailsVO.setBrandName(rs.getString("brand_name"));
				oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
				oProductDetailsVO.setLongDesc(rs.getString("long_desc"));

				String sSize = rs.getString("size");
				sSize = sSize ==null?"":sSize.trim();
				oProductDetailsVO.setSize(sSize);

				String sColor = rs.getString("color");
				sColor = sColor ==null?"":sColor.trim();
				oProductDetailsVO.setColor(sColor);

				String sVendorPrice = rs.getString("vend_price");
				sVendorPrice =sVendorPrice==null?"0.00":sVendorPrice.trim();
				oProductDetailsVO.setVendPrice(sVendorPrice);

				String sSbhPrice=rs.getString("sbh_price");
				sSbhPrice = sSbhPrice ==null?sVendorPrice:sSbhPrice.trim();
				oProductDetailsVO.setSbhPrice(sSbhPrice);

				oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
				oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));
				oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));
				oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));
				oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));
				oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));
				oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));
				oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));

				sDBData = rs.getString("main_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();               
				oProductDetailsVO.setMainImgCap(sDBData);

				sDBData = rs.getString("view1_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView1ImgCap(sDBData);

				sDBData = rs.getString("view2_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView2ImgCap(sDBData);

				sDBData = rs.getString("view3_img_caption");
				sDBData = sDBData==null?"":sDBData.trim();  
				oProductDetailsVO.setView3ImgCap(sDBData);

				oProductDetailsVO.setReturnPolicy(rs.getString("return_policy"));

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
					if(p_sProdId.equals(oProductDetailsVO.getProdId()))
						oProductDetailsVO.setShowProd("Yes");
					else
						oProductDetailsVO.setShowProd("No");	
				}

				/*String sValidFromDate = rs.getString("valid_from_date");
				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
				oProductDetailsVO.setValidFromDate(sValidFromDate);

				String sValidToDate = rs.getString("valid_to_date");
				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
				oProductDetailsVO.setValidToDate(sValidToDate);*/

				String sInstructions = rs.getString("instructions");
				sInstructions = sInstructions==null?"":sInstructions.trim();				
				oProductDetailsVO.setInstructions(sInstructions);

				String sSwfPath = rs.getString("swf_path");
				sSwfPath = sSwfPath==null?"":sSwfPath.trim();				
				oProductDetailsVO.setSwfPath(sSwfPath);

				String sSwfType = rs.getString("swf_type");
				sSwfType = sSwfType==null?"":sSwfType.trim();				
				oProductDetailsVO.setSwfType(sSwfType);

				String sIsAdvt = rs.getString("is_advt");
				sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
				oProductDetailsVO.setIsAdvt(sIsAdvt);

				String sCFStatus = rs.getString("coverflow_status");
				sCFStatus = sCFStatus==null?"":sCFStatus.trim();				
				oProductDetailsVO.setCoverflowStatus(sCFStatus);

				String sProdSwfPath = rs.getString("prod_swf_path");
				sProdSwfPath = sProdSwfPath==null?"":sProdSwfPath.trim();				
				oProductDetailsVO.setSpecialProductPath(sProdSwfPath);

				String sProdSwfType = rs.getString("prod_swf_type");
				sProdSwfType = sProdSwfType==null?"":sProdSwfType.trim();				
				oProductDetailsVO.setSpecialProdType(sProdSwfType);

				String sIsSpecialProd = rs.getString("is_special_prod");
				sIsSpecialProd = sIsSpecialProd==null?"":sIsSpecialProd.trim();				
				oProductDetailsVO.setIsSpecialProd(sIsSpecialProd);

				alProdDetails = (ArrayList)hmProdDetails.get(sCateName);
				if(alProdDetails == null)
				{
					alProdDetails = new ArrayList();
					alProdDetails.add(oProductDetailsVO);
				} else
				{
					alProdDetails.add(oProductDetailsVO);
				}

			}


			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				while(rs.next()){
					oProductDetailsVO = new ProductDetailsVO();
					oProductDetailsVO.setProdId(rs.getString("prod_id"));
					oProductDetailsVO.setCateId(rs.getString("cate_id"));
					oProductDetailsVO.setSeqId(rs.getString("seq_id"));
					oProductDetailsVO.setOwnerId(rs.getString("owner_id"));
					oProductDetailsVO.setOwnerType(rs.getString("owner_type"));
					sCateName = rs.getString("cate_name");
					oProductDetailsVO.setCateName(sCateName);
					oProductDetailsVO.setOwnerName(rs.getString("owner_name"));
					oProductDetailsVO.setProdCode(rs.getString("prod_code"));
					oProductDetailsVO.setInShopStatus(rs.getString("in_shop_status"));
					oProductDetailsVO.setProdTitle(rs.getString("prod_title"));
					oProductDetailsVO.setBrandName(rs.getString("brand_name"));
					oProductDetailsVO.setShortDesc(rs.getString("short_desc"));
					oProductDetailsVO.setLongDesc(rs.getString("long_desc"));

					String sSize = rs.getString("size");
					sSize = sSize ==null?"":sSize.trim();
					oProductDetailsVO.setSize(sSize);

					String sColor = rs.getString("color");
					sColor = sColor ==null?"":sColor.trim();
					oProductDetailsVO.setColor(sColor);

					String sVendorPrice = rs.getString("vend_price");
					sVendorPrice =sVendorPrice==null?"0.00":sVendorPrice.trim();
					oProductDetailsVO.setVendPrice(sVendorPrice);

					String sSbhPrice=rs.getString("sbh_price");
					sSbhPrice = sSbhPrice ==null?sVendorPrice:sSbhPrice.trim();
					oProductDetailsVO.setSbhPrice(sSbhPrice);

					oProductDetailsVO.setMainImgPath(rs.getString("main_img_path"));
					oProductDetailsVO.setView1ImgPath(rs.getString("view1_img_path"));
					oProductDetailsVO.setView2ImgPath(rs.getString("view2_img_path"));
					oProductDetailsVO.setView3ImgPath(rs.getString("view3_img_path"));
					oProductDetailsVO.setMainImgType(rs.getString("main_img_type"));
					oProductDetailsVO.setView1ImgType(rs.getString("view1_img_type"));
					oProductDetailsVO.setView2ImgType(rs.getString("view2_img_type"));
					oProductDetailsVO.setView3ImgType(rs.getString("view3_img_type"));

					sDBData = rs.getString("main_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();               
					oProductDetailsVO.setMainImgCap(sDBData);

					sDBData = rs.getString("view1_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView1ImgCap(sDBData);

					sDBData = rs.getString("view2_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView2ImgCap(sDBData);

					sDBData = rs.getString("view3_img_caption");
					sDBData = sDBData==null?"":sDBData.trim();  
					oProductDetailsVO.setView3ImgCap(sDBData);

					oProductDetailsVO.setReturnPolicy(rs.getString("return_policy"));

					if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
						if(p_sProdId.equals(oProductDetailsVO.getProdId()))
							oProductDetailsVO.setShowProd("Yes");
						else
							oProductDetailsVO.setShowProd("No");	
					}

					/*String sValidFromDate = rs.getString("valid_from_date");
    				sValidFromDate = sValidFromDate==null?"":sValidFromDate.trim();				
    				oProductDetailsVO.setValidFromDate(sValidFromDate);

    				String sValidToDate = rs.getString("valid_to_date");
    				sValidToDate = sValidToDate==null?"":sValidToDate.trim();				
    				oProductDetailsVO.setValidToDate(sValidToDate);*/

					String sInstructions = rs.getString("instructions");
					sInstructions = sInstructions==null?"":sInstructions.trim();				
					oProductDetailsVO.setInstructions(sInstructions);

					String sSwfPath = rs.getString("swf_path");
					sSwfPath = sSwfPath==null?"":sSwfPath.trim();				
					oProductDetailsVO.setSwfPath(sSwfPath);

					String sSwfType = rs.getString("swf_type");
					sSwfType = sSwfType==null?"":sSwfType.trim();				
					oProductDetailsVO.setSwfType(sSwfType);

					String sIsAdvt = rs.getString("is_advt");
					sIsAdvt = sIsAdvt==null?"":sIsAdvt.trim();				
					oProductDetailsVO.setIsAdvt(sIsAdvt);

					String sCFStatus = rs.getString("coverflow_status");
					sCFStatus = sCFStatus==null?"":sCFStatus.trim();				
					oProductDetailsVO.setCoverflowStatus(sCFStatus);

					alCoverflowProd.add(oProductDetailsVO);
				}

				if(hmProdDetails!=null){
					hmProdDetails.put("Coverflow", alCoverflowProd);
				}

			}
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();

				while(rs.next()){
					oPartnerVO = new PartnerVO();
					sDBData = rs.getString("about_me_path");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setAboutMePath(sDBData);

					sDBData = rs.getString("about_me_type");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setAboutMeType(sDBData);

					sDBData = rs.getString("owner_id");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerId(sDBData);

					sDBData = rs.getString("owner_name");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerName(sDBData);

					sDBData = rs.getString("owner_type");
					sDBData = sDBData == null?"":sDBData.trim();
					oPartnerVO.setOwnerType(sDBData);


					alPartnerDetails.add(oPartnerVO);
				}

				if(hmProdDetails!=null){
					hmProdDetails.put("PartnerDetails", alPartnerDetails);
				}

			}

			String sCatalogueDate = cstmt.getString(3);
			sCatalogueDate = sCatalogueDate == null?"":sCatalogueDate.trim();
			// System.out.println("CatalogueDate"+sCatalogueDate);
			if(hmProdDetails!=null)
				hmProdDetails.put("CatalogueDate",sCatalogueDate);

			logger.info("GenerateCatalogue::getProductData:EXIT");	
		}
		catch(Exception e)
		{
			logger.error("GenerateCatalogue::getProductData:EXCEPTION "+e.getMessage());	
			// System.out.println((new StringBuilder("GenerateXML::loadData:Exception")).append(e.getMessage()).toString());
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return hmProdDetails;
	}

	private void createCatalogue(Document dom, HashMap hmProdDetails, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId, String p_sProdId, String p_sSeqId, List<AdminReturnPolicyVO> p_alAdminReturnPolicyVO)
	{
		logger.info("GenerateCatalogue::createCatalogue:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sShowActiveProduct = "";
		/*String sIsDemonstrationCatalogue = null;*/
		String sIsPreviewCatalogue = "";
		String sPartnerType = "";
		String sEcatgType=null;
		try{

			Element rootEle = dom.createElement("Categories");

			String sCatalogueDate = (String)hmProdDetails.get("CatalogueDate");
			rootEle.setAttribute("catalogueDate", sCatalogueDate);     
			hmProdDetails.remove("CatalogueDate");

			alCoverflow = (ArrayList)hmProdDetails.get("Coverflow");
			hmProdDetails.remove("Coverflow");
//			String  sImageViewPath= "";

			/*if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
        	sImageViewPath = System.getProperty("ImageViewPath").trim();
        }		  
        sImageViewPath = (sImageViewPath ==null)?"":sImageViewPath.trim();*/

			if(p_sUserType!=null && p_sUserType.trim().length()>=0) {
				if(p_sUserType.equalsIgnoreCase("CLIENT")) {  

					String sAirLogoPath =/*sImageViewPath+*/"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo.jpg";
					String sAirLogoIntroPath =/*sImageViewPath+*/"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo_intro.jpg";
					rootEle.setAttribute("AirLogoPath",sAirLogoPath);   
					rootEle.setAttribute("AirLogoIntroPath",sAirLogoIntroPath);
					String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
					String sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml";
					String sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml";
					rootEle.setAttribute("PartnerDetails" ,sAboutPartnerXML);
					rootEle.setAttribute("CoverImageDetails" ,sCoverImageXML);

				}else if(p_sUserType.equalsIgnoreCase("AIRLINE")) {
					String sImageViewPath = System.getProperty("ImageViewPath").trim();	
					sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
					String sAirLogoPath =sImageViewPath+"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo.jpg";
					String sAirLogoIntroPath =sImageViewPath+"SkyBuyPics/Airline_"+p_sOwnerId+"/AirlineLogo/Logo"+"/"+p_sOwnerId+"_logo_intro.jpg";
					rootEle.setAttribute("AirLogoPath",sAirLogoPath);   
					rootEle.setAttribute("AirLogoIntroPath",sAirLogoIntroPath);
				}
			}
			if(!"CLIENT".equalsIgnoreCase(p_sUserType)) {
				if("PRODCATALOGUE".equalsIgnoreCase(p_CatalogueType)){
					rootEle.setAttribute("activeCategory" ,p_sSeqId);
					rootEle.setAttribute("activeProductId" ,p_sProdId);
					rootEle.setAttribute("activeOwnerId" ,p_sOwnerId);
					sShowActiveProduct = "Y";
					/*sIsDemonstrationCatalogue = "N";*/
					sIsPreviewCatalogue = "Y";
					sEcatgType="PRODUCT_PREVIEW";
				}else if("PREVIEWCATALOGUE".equalsIgnoreCase(p_CatalogueType)) {
					/*sShowActiveProduct = "N";
					sIsDemonstrationCatalogue = "N";*/
					sIsPreviewCatalogue = "Y";
					if("AIRLINE".equalsIgnoreCase(p_sUserType)) {
						sPartnerType = "airline";
					}else if("VENDOR".equalsIgnoreCase(p_sUserType)) {
						sPartnerType = "vendor";
					}
					sEcatgType="CATALOGUE_PREVIEW";
				}else if("DEMONSTRATIONCATALOGUE".equalsIgnoreCase(p_CatalogueType)){
					/*sShowActiveProduct = "N";
					sIsDemonstrationCatalogue = "Y";
					sIsPreviewCatalogue = "N";*/
					sEcatgType="DEMO";
				}else if("WEBCATALOGUE".equalsIgnoreCase(p_CatalogueType)){
					/*sShowActiveProduct = "N";
					sIsDemonstrationCatalogue = "Y";
					sIsPreviewCatalogue = "N";*/
					sEcatgType="WEB";
				}

			}
			else{
				sEcatgType="DEVICE";
			}
			rootEle.setAttribute("eCatalogueType",sEcatgType);
			rootEle.setAttribute("showActiveProduct" ,sShowActiveProduct);
			/*rootEle.setAttribute("isDemonstrationCatalogue" ,sIsDemonstrationCatalogue);*/
			rootEle.setAttribute("isPreviewCatalogue" ,sIsPreviewCatalogue);
			rootEle.setAttribute("partnerType" ,sPartnerType);

			dom.appendChild(rootEle);
			/*Element categoriesEle = dom.createElement("Categories");
        rootEle.appendChild(categoriesEle);
        Element coverflowEle = dom.createElement("Coverflow");
        rootEle.appendChild(coverflowEle);*/

			Element oMessages;
			Element oMessage;
			/*
			 * NEW CHILD NODE -- RETURN POLICY.
			 * Used to add a new child node name ReturnPolicy and to add the Disclaimer
			 */
			if("CLIENT".equalsIgnoreCase(p_sUserType) || "DEMONSTRATIONCATALOGUE".equalsIgnoreCase(p_CatalogueType)|| "WEBCATALOGUE".equalsIgnoreCase(p_CatalogueType) )  {
				oMessages = dom.createElement("Messages");
				CDATASection cDisclaimer;
				for(AdminReturnPolicyVO oAdminReturnPolicyVO:p_alAdminReturnPolicyVO) {
					oMessage = dom.createElement("Message");
					oMessage.setAttribute("type", oAdminReturnPolicyVO.getCode());
//					disclaimer = dom.createTextNode(oAdminReturnPolicyVO.getText());
					cDisclaimer = dom.createCDATASection(oAdminReturnPolicyVO.getText());
					oMessage.appendChild(cDisclaimer);
					oMessages.appendChild(oMessage);
				}
				rootEle.appendChild(oMessages);
			}
			Element cateEle;
			for(Iterator itr = hmProdDetails.entrySet().iterator(); itr.hasNext(); rootEle.appendChild(cateEle))
			{
				java.util.Map.Entry e = (java.util.Map.Entry)itr.next();
				alProdInfo = (ArrayList)e.getValue();
				cateEle = dom.createElement("Category");
				Element prodEle;
				for(Iterator iterator = alProdInfo.iterator(); iterator.hasNext(); cateEle.appendChild(prodEle))
				{
					ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
					prodEle = createProdElement(dom, oProductDetailsVO,p_sUserType,p_CatalogueType);
					cateEle.setAttribute("id", oProductDetailsVO.getCateId());
					cateEle.setAttribute("seqId", oProductDetailsVO.getSeqId());

					//For generating Product catalogue ---- 
					/*if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
                	if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
                		cateEle.setAttribute("isActive", "Y");
                	else
                		cateEle.setAttribute("isActive", "N");
                }*/
				}
			}

			/*cateEle = dom.createElement("Category");
        Element prodEle;
        for(Iterator iterator = alCoverflow.iterator(); iterator.hasNext(); cateEle.appendChild(prodEle))
        {
            ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
            prodEle = createProdElement(dom, oProductDetailsVO,p_sUserType,p_CatalogueType);
            cateEle.setAttribute("id", oProductDetailsVO.getCateId());
            cateEle.setAttribute("seqId", oProductDetailsVO.getSeqId());

            //For generating Product catalogue ---- 
            if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
            	if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
            		cateEle.setAttribute("isActive", "Y");
            	else
            		cateEle.setAttribute("isActive", "N");
            }
        }
        coverflowEle.appendChild(cateEle);*/


			printToFile(dom, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);



			logger.info("GenerateCatalogue::createCatalogue:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogue:EXCEPTION "+e.getMessage());	
		}
	}

	private ArrayList createCatalogueForCover(Document domCover, ArrayList alCoverflowProd, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::createCatalogue:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sImageUploadPath = null;
		String sImageURL = null,sOriginalImageURL = null,sImageFullURL = null;
		String sImageViewPath = "";
		ArrayList alCoverflowProdIds = new ArrayList();
		int iCoverflowCount = 0;
		try{
			sImageUploadPath = System.getProperty("ImageUploadPath");
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			Element rootEle = domCover.createElement("Coverflow");
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
				sImageViewPath = System.getProperty("ImageViewPath").trim();	
				sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			}
			domCover.appendChild(rootEle);

			Element cateEle;

			for(Iterator iterator = alCoverflowProd.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
			{

				cateEle = domCover.createElement("Image");
				ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
				sImageURL =sImageViewPath+"SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
				sImageFullURL = sImageUploadPath+ "/SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
				alCoverflowProdIds.add(oProductDetailsVO.getProdId());
				//prodEle = createProdElement(domCover, oProductDetailsVO,p_sUserType,p_CatalogueType);
				cateEle.setAttribute("id", oProductDetailsVO.getCateId());
				cateEle.setAttribute("SeqId", oProductDetailsVO.getSeqId());
				cateEle.setAttribute("ProdId", oProductDetailsVO.getProdId());
				cateEle.setAttribute("coverSeqId", oProductDetailsVO.getCoverflowStatus());
				cateEle.setAttribute("ImgURL", sImageURL);
				//For generating Product catalogue ---- 
				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
					if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
						cateEle.setAttribute("isActive", "Y");
					else
						cateEle.setAttribute("isActive", "N");
				}


				File f = new File(sImageFullURL);
				if(!f.exists()){
					sOriginalImageURL = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
					ImageScale.createCoverflowImage(sOriginalImageURL, sImageUploadPath);
				}
				iCoverflowCount++;

			}
			while(iCoverflowCount > 0 && iCoverflowCount < 5){
				for(Iterator iterator = alCoverflowProd.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
				{
					if(iCoverflowCount == 5){
						break;
					}
					cateEle = domCover.createElement("Image");
					ProductDetailsVO oProductDetailsVO = (ProductDetailsVO)iterator.next();
					sImageURL =sImageViewPath+"SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main.jpg";
					sImageFullURL = sImageUploadPath+ "/SkyBuyPics/CoverImages/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main.jpg";
					alCoverflowProdIds.add(oProductDetailsVO.getProdId());
					//prodEle = createProdElement(domCover, oProductDetailsVO,p_sUserType,p_CatalogueType);
					cateEle.setAttribute("id", oProductDetailsVO.getCateId());
					cateEle.setAttribute("SeqId", oProductDetailsVO.getSeqId());
					cateEle.setAttribute("ProdId", oProductDetailsVO.getProdId());
					cateEle.setAttribute("coverSeqId", oProductDetailsVO.getCoverflowStatus());
					cateEle.setAttribute("ImgURL", sImageURL);
					//For generating Product catalogue ---- 
					if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
						if(p_sCateId.equalsIgnoreCase(oProductDetailsVO.getCateId()))
							cateEle.setAttribute("isActive", "Y");
						else
							cateEle.setAttribute("isActive", "N");
					}


					File f = new File(sImageFullURL);
					if(!f.exists()){
						sOriginalImageURL = sImageUploadPath+"/SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+"jpg";

						ImageScale.createCoverflowImage(sOriginalImageURL, sImageUploadPath);
					}
					iCoverflowCount++;

				}

			}



			printToFileForCover(domCover, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);

			logger.info("GenerateCatalogue::createCatalogue:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogue:EXCEPTION "+e.getMessage());	
		}

		return alCoverflowProdIds;
	}
	private void createCatalogueForPartnerDetails(Document domPartner, ArrayList alPartnerDetails, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_sCateId,String p_CatalogueType,String p_sOwnerId,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::createCatalogueForPartnerDetails:ENTER");		
		ArrayList alProdInfo = null;
		List<ProductDetailsVO> alCoverflow = null;
		String sImageUploadPath = null;
		String sAboutMeURL = null,sOriginalImageURL = null,sImageFullURL = null,sImageViewPath = "";


		try{

			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
				sImageViewPath = System.getProperty("ImageViewPath").trim();	
				sImageViewPath = sImageViewPath == null?"":sImageViewPath.trim();
			}
			sImageUploadPath = System.getProperty("ImageUploadPath");
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			Element rootEle = domPartner.createElement("PartnerDetails");

			domPartner.appendChild(rootEle);

			Element cateEle;

			for(Iterator iterator = alPartnerDetails.iterator(); iterator.hasNext(); rootEle.appendChild(cateEle))	
			{

				cateEle = domPartner.createElement("Owner");
				PartnerVO oPartnerVO = (PartnerVO)iterator.next();

				if(oPartnerVO.getAboutMePath()!=null && !oPartnerVO.getAboutMePath().equalsIgnoreCase("")){
					sAboutMeURL =sImageViewPath+oPartnerVO.getAboutMePath()+"/"+oPartnerVO.getOwnerId()+"."+oPartnerVO.getAboutMeType();
				}else{
					sAboutMeURL = "";
				}
				cateEle.setAttribute("id", oPartnerVO.getOwnerId());
				cateEle.setAttribute("type", oPartnerVO.getOwnerType());
				cateEle.setAttribute("name", oPartnerVO.getOwnerName());
				cateEle.setAttribute("aboutMeURL", sAboutMeURL);
			}



			printToFileForPartner(domPartner, p_sDestPath,p_sUserType,p_sUserId,p_sServerPath,p_CatalogueType,p_sDeviceId);


			logger.info("GenerateCatalogue::createCatalogueForPartnerDetails:EXIT");	

		}catch(Exception e){
			logger.error("GenerateCatalogue::createCatalogueForPartnerDetails:EXCEPTION "+e.getMessage());	
		}

	}
	private Element createProdElement(Document dom, ProductDetailsVO oProductDetailsVO,String p_sUserType,String p_CatalogueType)
	{
		logger.info("GenerateCatalogue::createProdElement:ENTER");		
		String sView1ImageURLPath="",sView1ThumbURLPath = "",sView1MedViewPath = "",sView1LargeViewPath ="";
		String sView3MedViewPath ="",sView3LargeViewPath="",sView3ThumbURLPath="",sView3ImageURLPath="";
		String sView2LargeViewPath="",sView2ThumbURLPath="",sView2MedViewPath="",sView2ImageURLPath="";
		String sImageViewPath ="",sOriginalImageURLPath = "";
		String sSbhPrice =null;
		String sConverFlowImageURLPath= "",sMainImageURLPath= "",sMainThumbURLPath= "",sMainMedViewPath= "",sMainLargeViewPath = "";
		if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){
			sImageViewPath = System.getProperty("ImageViewPath").trim();	    		
		}

		Element prodEle = dom.createElement("Product");
		prodEle.setAttribute("OwnerId", oProductDetailsVO.getOwnerId());
		prodEle.setAttribute("Adv", oProductDetailsVO.getIsAdvt());
		if(oProductDetailsVO.getIsAdvt().equalsIgnoreCase("Y")){
			prodEle.setAttribute("SwfPath", sImageViewPath+oProductDetailsVO.getSwfPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSwfType());
		}else if(oProductDetailsVO.getIsSpecialProd().equalsIgnoreCase("Y")){
			prodEle.setAttribute("SwfPath", sImageViewPath+oProductDetailsVO.getSpecialProductPath()+"/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getSpecialProdType());
		}else{
			prodEle.setAttribute("SwfPath", "");
		}

		Element prodIdEle = dom.createElement("ProductId");
		Text prodIdText = dom.createTextNode(oProductDetailsVO.getProdId());
		prodIdEle.appendChild(prodIdText);
		prodEle.appendChild(prodIdEle);

		Element prodCodeEle = dom.createElement("ProductCode");
		Text prodCodeText = dom.createTextNode(oProductDetailsVO.getProdCode());
		prodCodeEle.appendChild(prodCodeText);
		prodEle.appendChild(prodCodeEle);

		Element brandEle = dom.createElement("BRAND");
		Text brandText = dom.createTextNode(oProductDetailsVO.getBrandName());
		brandEle.appendChild(brandText);
		prodEle.appendChild(brandEle);

		Element titleEle = dom.createElement("TITLE");
		Text titleText = dom.createTextNode(oProductDetailsVO.getProdTitle());
		titleEle.appendChild(titleText);
		prodEle.appendChild(titleEle);

		/*Element shortDescEle = dom.createElement("ShortDiscription");
        Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
        shortDescEle.appendChild(shortDescText);
        prodEle.appendChild(shortDescEle);

        Element longDescEle = dom.createElement("LongDiscription");
        Element htmlLongEle = dom.createElement("html");
        Text longDescText = dom.createTextNode(oProductDetailsVO.getLongDesc());
        htmlLongEle.appendChild(longDescText);
        longDescEle.appendChild(htmlLongEle);
        prodEle.appendChild(longDescEle);*/

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element shortDescEle = dom.createElement("ShortDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cShortDesc = dom.createCDATASection(oProductDetailsVO.getShortDesc());
		shortDescEle.appendChild(cShortDesc);
		prodEle.appendChild(shortDescEle);

		//create Short Desc element and Short Desc text node and attach it to Product Element
		Element longDescEle = dom.createElement("LongDiscription");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cLongDesc = dom.createCDATASection(oProductDetailsVO.getLongDesc());
		longDescEle.appendChild(cLongDesc);
		prodEle.appendChild(longDescEle);

		Element sizeEle = dom.createElement("size");
		Text sizeText = dom.createTextNode(oProductDetailsVO.getSize());
		sizeEle.appendChild(sizeText);
		prodEle.appendChild(sizeEle);

		Element colorEle = dom.createElement("color");
		Text colorText = dom.createTextNode(oProductDetailsVO.getColor());
		colorEle.appendChild(colorText);
		prodEle.appendChild(colorEle);

		/*Element captionEle = dom.createElement("Caption");
        prodEle.appendChild(captionEle);*/

		Element priceEle = dom.createElement("Price");
		Text priceText;
		if(p_sUserType!=null && (p_sUserType.equalsIgnoreCase("ADMIN")|| p_sUserType.equalsIgnoreCase("CLIENT"))){
			priceText = dom.createTextNode("$"+oProductDetailsVO.getVendPrice());
		}else{
			priceText = dom.createTextNode("");
		}
		priceEle.appendChild(priceText);
		prodEle.appendChild(priceEle);


		Element skyBuyPriceEle = dom.createElement("SkyBuyPrice");
		sSbhPrice ="$"+oProductDetailsVO.getSbhPrice();
		/*  if(p_sUserType!=null && (p_sUserType.equalsIgnoreCase("ADMIN")|| p_sUserType.equalsIgnoreCase("CLIENT")))
        	sSbhPrice ="$"+oProductDetailsVO.getSbhPrice();
        else

        	sSbhPrice ="$"+oProductDetailsVO.getVendPrice();*/
		Text skyBuyPriceText = dom.createTextNode(sSbhPrice);
		skyBuyPriceEle.appendChild(skyBuyPriceText);
		prodEle.appendChild(skyBuyPriceEle);

		// Starts Main Img Url // 

		Element mainImageURLEle = dom.createElement("MainImageURL");

		if((oProductDetailsVO.getMainImgPath()!=null && oProductDetailsVO.getMainImgPath().trim().length()>0) && (oProductDetailsVO.getMainImgType()!=null && oProductDetailsVO.getMainImgType().trim().length()>0) ) {
			sConverFlowImageURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/CoverFlow/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"."+oProductDetailsVO.getMainImgType();
			sMainImageURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getMainImgType();
			sMainThumbURLPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getMainImgType();
			sMainMedViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getMainImgType();
			sMainLargeViewPath=sImageViewPath+oProductDetailsVO.getMainImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getMainImgType();
		}else{
			sConverFlowImageURLPath="";
			sMainImageURLPath="";
			sMainThumbURLPath="";
			sMainMedViewPath="";
			sMainLargeViewPath="";
		}
		if(oProductDetailsVO.getMainImgType()!=null && oProductDetailsVO.getMainImgType().trim().length()>0) 
			sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"main."+oProductDetailsVO.getMainImgType();
		else
			sOriginalImageURLPath="";

		//Adding attribute for ConverFlowPath,Caption,originalImgpath
		mainImageURLEle.setAttribute("CoverFlowImgPath", sConverFlowImageURLPath);
		mainImageURLEle.setAttribute("Caption", oProductDetailsVO.getMainImgCap());        
		mainImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);

		Text mainImageURLText = dom.createTextNode(sMainImageURLPath);
		mainImageURLEle.appendChild(mainImageURLText);
		prodEle.appendChild(mainImageURLEle);

		Element mainThumbURLEle = dom.createElement("MainThumbURL");
		Text mainThumbURLText = dom.createTextNode(sMainThumbURLPath);
		mainThumbURLEle.appendChild(mainThumbURLText);
		prodEle.appendChild(mainThumbURLEle);

		Element mainMedViewEle = dom.createElement("MainMedView");
		Text mainMedViewText = dom.createTextNode(sMainMedViewPath);
		mainMedViewEle.appendChild(mainMedViewText);
		prodEle.appendChild(mainMedViewEle);

		Element mainLargeViewEle = dom.createElement("MainLargeView");
		Text mainLargeViewText = dom.createTextNode(sMainLargeViewPath);
		mainLargeViewEle.appendChild(mainLargeViewText);
		prodEle.appendChild(mainLargeViewEle);

		// Ends Main Img Url // 

		// Starts View1 Img Url // 

		Element view1ImageURLEle = dom.createElement("ViewOneImageURL");

		//Adding attribute for Caption,originalImgpath
		if(oProductDetailsVO.getView1ImgPath()!=null && oProductDetailsVO.getView1ImgPath().trim().length()>0 && oProductDetailsVO.getView1ImgType()!=null &&  oProductDetailsVO.getView1ImgType().trim().length()>0 ){
			sView1ImageURLPath = sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView1ImgType();
			sView1ThumbURLPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView1ImgType();
			sView1MedViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView1ImgType();
			sView1LargeViewPath=sImageViewPath+oProductDetailsVO.getView1ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView1ImgType();
		}else{
			sView1ImageURLPath="";
			sView1ThumbURLPath = "";
			sView1MedViewPath = "";
			sView1LargeViewPath = "";
		}

		if(oProductDetailsVO.getView1ImgType()!=null && oProductDetailsVO.getView1ImgType().trim().length()>0){
			sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view1."+oProductDetailsVO.getView1ImgType();
		}else{
			sOriginalImageURLPath = "";
		}
		view1ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView1ImgCap());        
		view1ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);




		Text view1ImageURLText = dom.createTextNode(sView1ImageURLPath);
		view1ImageURLEle.appendChild(view1ImageURLText);
		prodEle.appendChild(view1ImageURLEle);

		Element view1ThumbURLEle = dom.createElement("ViewOneThumbURL");

		Text view1ThumbURLText = dom.createTextNode(sView1ThumbURLPath);
		view1ThumbURLEle.appendChild(view1ThumbURLText);
		prodEle.appendChild(view1ThumbURLEle);

		Element view1MedViewEle = dom.createElement("ViewOneMedView");
		Text view1MedViewText = dom.createTextNode(sView1MedViewPath);
		view1MedViewEle.appendChild(view1MedViewText);
		prodEle.appendChild(view1MedViewEle);

		Element view1LargeViewEle = dom.createElement("ViewOneLargeView");
		Text view1LargeViewText = dom.createTextNode(sView1LargeViewPath);
		view1LargeViewEle.appendChild(view1LargeViewText);
		prodEle.appendChild(view1LargeViewEle);

		// Ends View1 Img Url // 


		// Starts View2 Img Url // 
		if(oProductDetailsVO.getView2ImgPath()!=null && oProductDetailsVO.getView2ImgPath().trim().length()>0 && oProductDetailsVO.getView2ImgType()!=null &&  oProductDetailsVO.getView2ImgType().trim().length()>0 ){
			sView2ImageURLPath = sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView2ImgType();
			sView2ThumbURLPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView2ImgType();
			sView2MedViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView2ImgType();
			sView2LargeViewPath=sImageViewPath+oProductDetailsVO.getView2ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView2ImgType();
		}else{
			sView2ImageURLPath="";
			sView2ThumbURLPath = "";
			sView2MedViewPath = "";
			sView2LargeViewPath = "";
		}

		if(oProductDetailsVO.getView2ImgType()!=null && oProductDetailsVO.getView2ImgType().trim().length()>0){
			sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view2."+oProductDetailsVO.getView2ImgType();
		}else{
			sOriginalImageURLPath = "";
		}

		Element view2ImageURLEle = dom.createElement("ViewTwoImageURL");

		//Adding attribute for Caption,originalImgpath
		view2ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView2ImgCap());        
		view2ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);


		Text view2ImageURLText = dom.createTextNode(sView2ImageURLPath);
		view2ImageURLEle.appendChild(view2ImageURLText);
		prodEle.appendChild(view2ImageURLEle);

		Element view2ThumbURLEle = dom.createElement("ViewTwoThumbURL");

		Text view2ThumbURLText = dom.createTextNode(sView2ThumbURLPath);
		view2ThumbURLEle.appendChild(view2ThumbURLText);
		prodEle.appendChild(view2ThumbURLEle);

		Element view2MedViewEle = dom.createElement("ViewTwoMedView");

		Text view2MedViewText = dom.createTextNode(sView2MedViewPath);
		view2MedViewEle.appendChild(view2MedViewText);
		prodEle.appendChild(view2MedViewEle);                 


		Element view2LargeViewEle = dom.createElement("ViewTwoLargeView");

		Text view2LargeViewText = dom.createTextNode(sView2LargeViewPath);
		view2LargeViewEle.appendChild(view2LargeViewText);
		prodEle.appendChild(view2LargeViewEle);

		// Ends View2 Img Url // 

		// Starts View3 Img Url // 
		if(oProductDetailsVO.getView3ImgPath()!=null && oProductDetailsVO.getView3ImgPath().trim().length()>0 && oProductDetailsVO.getView3ImgType()!=null &&  oProductDetailsVO.getView3ImgType().trim().length()>0 ){
			sView3ImageURLPath = sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Images/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"image."+oProductDetailsVO.getView3ImgType();
			sView3ThumbURLPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Thumb/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"thumb."+oProductDetailsVO.getView3ImgType();
			sView3MedViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getView3ImgType();
			sView3LargeViewPath=sImageViewPath+oProductDetailsVO.getView3ImgPath()+"/"+oProductDetailsVO.getCateName()+"/Large/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"large."+oProductDetailsVO.getView3ImgType();
		}else{
			sView3ImageURLPath="";
			sView3ThumbURLPath = "";
			sView3MedViewPath = "";
			sView3LargeViewPath = "";
		}

		if(oProductDetailsVO.getView3ImgType()!=null && oProductDetailsVO.getView3ImgType().trim().length()>0){
			sOriginalImageURLPath=sImageViewPath+"SkyBuyPics/"+oProductDetailsVO.getOwnerType()+"_"+oProductDetailsVO.getOwnerId()+"/OriginalImg/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"view3."+oProductDetailsVO.getView3ImgType();
		}else{
			sOriginalImageURLPath = "";
		}

		Element view3ImageURLEle = dom.createElement("ViewThreeImageURL");

		//Adding attribute for Caption,originalImgpath
		view3ImageURLEle.setAttribute("Caption", oProductDetailsVO.getView3ImgCap());        
		view3ImageURLEle.setAttribute("OriginalImgPath", sOriginalImageURLPath);


		Text view3ImageURLText = dom.createTextNode(sView3ImageURLPath);
		view3ImageURLEle.appendChild(view3ImageURLText);
		prodEle.appendChild(view3ImageURLEle);

		Element view3ThumbURLEle = dom.createElement("ViewThreeThumbURL");

		Text view3ThumbURLText = dom.createTextNode(sView3ThumbURLPath);
		view3ThumbURLEle.appendChild(view3ThumbURLText);
		prodEle.appendChild(view3ThumbURLEle);

		Element view3MedViewEle = dom.createElement("ViewThreeMedView");

		Text view3MedViewText = dom.createTextNode(sView3MedViewPath);
		view3MedViewEle.appendChild(view3MedViewText);
		prodEle.appendChild(view3MedViewEle);

		Element view3LargeViewEle = dom.createElement("ViewThreeLargeView");

		Text view3LargeViewText = dom.createTextNode(sView3LargeViewPath);
		view3LargeViewEle.appendChild(view3LargeViewText);
		prodEle.appendChild(view3LargeViewEle);

		// Ends View3 Img Url // 


		// if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
		Element returnPolicyEle = dom.createElement("ReturnPolicy");
		Text returnPolicyText = dom.createTextNode(oProductDetailsVO.getReturnPolicy());
		returnPolicyEle.appendChild(returnPolicyText);
		prodEle.appendChild(returnPolicyEle);
		// }

		// add valid from date and to date and instruction

		Element instructionEle = dom.createElement("Instruction");
		//Text shortDescText = dom.createTextNode(oProductDetailsVO.getShortDesc());
		CDATASection cInstruction= dom.createCDATASection(oProductDetailsVO.getInstructions());
		instructionEle.appendChild(cInstruction);
		prodEle.appendChild(instructionEle);

		/* Element validFromEle = dom.createElement("ValidFrom");
        Text validFromText = dom.createTextNode(oProductDetailsVO.getValidFromDate());
        validFromEle.appendChild(validFromText);
        prodEle.appendChild(validFromEle);


        Element validToEle = dom.createElement("ValidTo");
        Text validToText = dom.createTextNode(oProductDetailsVO.getValidToDate());
        validToEle.appendChild(validToText);
        prodEle.appendChild(validToEle);*/

		/*if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE")){
        	Element showProdEle = dom.createElement("ShowProd");
    		Text showProdText = dom.createTextNode(oProductDetailsVO.getShowProd());
    		showProdEle.appendChild(showProdText);
    		prodEle.appendChild(showProdEle);
        }*/
		logger.info("GenerateCatalogue::createProdElement:EXIT");		
		return prodEle;
	}

	private void printToFile(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFile:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFile:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFile:EXIT");	
	}

	private void printToFileForCover(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFileForCover:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_cover_"+"Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_cover_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_cover"+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFileForCover:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFileForCover:EXIT");	
	}

	private void printToFileForPartner(Document dom, String p_sDestPath,String p_sUserType,String p_sUserId,String p_sServerPath,String p_CatalogueType,String p_sDeviceId)
	{
		logger.info("GenerateCatalogue::printToFileForPartner:ENTER");			
		try
		{
			String sE_CatalogueFileName ="";
			File oFile = null;
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);
			sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
			if(p_sUserType!=null && p_sUserType.trim().length()>=0 && !p_sUserType.equalsIgnoreCase("CLIENT")){

				if(p_CatalogueType!=null && p_CatalogueType.equals("PRODCATALOGUE"))
					sE_CatalogueFileName=sE_CatalogueFileName+"_partner_"+"_Prod_"+p_sUserId.trim();
				else
					sE_CatalogueFileName=sE_CatalogueFileName+"_partner_"+p_sUserId.trim();

				oFile = new File(p_sServerPath+"\\"+sE_CatalogueFileName+".xml");
			}else{
				oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_partner"+".xml");
				//oFile = new File(p_sDestPath+"\\"+sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml");
			}
			if(oFile.exists())
				oFile.delete();
			XMLSerializer serializer = new XMLSerializer(new FileOutputStream(oFile), format);
			serializer.serialize(dom);
		}
		catch(IOException ie)
		{
			logger.error("GenerateCatalogue::printToFileForPartner:EXCEPTION "+ie.getMessage());		
			ie.printStackTrace();
		}
		logger.info("GenerateCatalogue::printToFileForPartner:EXIT");	
	}
}