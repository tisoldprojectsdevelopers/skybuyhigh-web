package com.sbh.util;

import java.io.ByteArrayOutputStream;

public class KeyUtils
{
	static private final String Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
	/**
	 * Return length many bytes of the passed in byte array as a hex string.
	 *
	 * @param data the bytes to be converted.
	 * @param length the number of bytes in the data block to be converted.
	 * @return a hex representation of length bytes of data.
	 */
	public static String convertBaToHex(byte[] data) {
       
        StringBuffer result = null;
        try {
            result = new StringBuffer();
            for (int i=0; i<data.length; i++) {
                int tmp = data[i];
                if (tmp < 0) tmp +=256;
                String tmpStr = Integer.toHexString(tmp);
                result.append((tmpStr.length()==1)?"0"+tmpStr:tmpStr);
            }
        } catch (Exception e) {
        	e.printStackTrace();
        }
        String hex = result.toString();
        return hex;
    }
	
	
	public static String toString(byte [] b) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		for (int i = 0; i < (b.length + 2) / 3; i++) {
			short [] s = new short[3];
			short [] t = new short[4];
			for (int j = 0; j < 3; j++) {
				if ((i * 3 + j) < b.length)
					s[j] = (short) (b[i*3+j] & 0xFF);
				else
					s[j] = -1;
			}
			
			t[0] = (short) (s[0] >> 2);
			if (s[1] == -1)
				t[1] = (short) (((s[0] & 0x3) << 4));
			else
				t[1] = (short) (((s[0] & 0x3) << 4) + (s[1] >> 4));
			if (s[1] == -1)
				t[2] = t[3] = 61;
			else if (s[2] == -1) {
				t[2] = (short) (((s[1] & 0xF) << 2));
				t[3] = 61;
			}
			else {
				t[2] = (short) (((s[1] & 0xF) << 2) + (s[2] >> 6));
				t[3] = (short) (s[2] & 0x3C);
			}
			for (int j = 0; j < 4; j++)
				os.write(Base64.charAt(t[j]));
		}
		return new String(os.toByteArray());
	}
	
}