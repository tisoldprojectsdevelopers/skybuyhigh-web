package com.sbh.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FileUtil {
	private static Logger logger = LogManager.getLogger(FileUtil.class);
	/*public static void main(String[] args) throws IOException{

		String sSrc="C:/StaticContent/SkyBuyPics/Main/Men/Images/5.jpg";//System.getProperty("ImageUploadPath");
		sSrc=sSrc==null?"":sSrc.trim();
		String sDest="C:/StaticContent/SkyBuyPics/Main/Women/Images/7.jpg";//System.getProperty("ImageUploadPath");
		sDest=sDest==null?"":sDest.trim();
		File oSrc = new File(sSrc);
		if(oSrc.exists()){
			
			File oDest = new File(sDest);

			moveFile(oSrc,"C:/StaticContent/SkyBuyPics/Ma/Women1/Images","4.jpg");


			// System.out.println("File or directory moved successfully.");
			
		}

	}*/
	public static void moveFile(String  p_sSrcFilePath,String p_sDestPath,String p_sDestFile){
		File oDestFilePath=null;
		String sDest=null;
		logger.info("FileUtil::moveFile:ENTER");	
		try {
			p_sSrcFilePath=p_sSrcFilePath==null?"":p_sSrcFilePath.trim();
			File oSrcFile = new File(p_sSrcFilePath);
			
			p_sDestPath=p_sDestPath==null?"":p_sDestPath.trim();
			p_sDestFile=p_sDestFile==null?"":p_sDestFile.trim();
			if(p_sDestPath!="" && p_sDestFile!=""){
				sDest=p_sDestPath+"/"+p_sDestFile;
				oDestFilePath=new File(p_sDestPath);
				if(!oDestFilePath.exists()){
					oDestFilePath.mkdirs();
				}
				oDestFilePath=new File(sDest);
				if(oDestFilePath.exists()){
					oDestFilePath.delete();
				}

				if(oSrcFile.isFile()){
					if(oSrcFile.exists())
						copyFile(oSrcFile,oDestFilePath);

				}else{
					copyDirectory(oSrcFile, oDestFilePath);
				}
				if(oSrcFile.exists())
					oSrcFile.delete();
			}
			
			logger.info("FileUtil::moveFile::Exit");
		} catch (IOException e) {		
			logger.error("FileUtil::moveFile::EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}

	}
	public static void copyDirectory(File p_oSourceDir, File p_oDestDir) throws IOException{
		logger.info("FileUtil::copyDirectory:ENTER");
		try{
			if(!p_oDestDir.exists()){
				p_oDestDir.mkdir();
			}
			File[] oaChildren = p_oSourceDir.listFiles();
			for(File oSourceChild : oaChildren){
				String name = oSourceChild.getName();
				File oDestChild = new File(p_oDestDir, name);
				if(oSourceChild.isDirectory()){
					copyDirectory(oSourceChild, oDestChild);
				}
				else{
					copyFile(oSourceChild, oDestChild);
				}
			}	logger.info("FileUtil::copyDirectory::Exit");
		} catch (IOException e) {			
			logger.error("FileUtil::copyDirectory:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}


	}

	public static void copyFile(File p_oSource, File p_oDest) throws IOException{
		logger.info("FileUtil::copyFile:ENTER");
		InputStream in = null;
		OutputStream out = null;
		try{
			// System.out.println("Dest File Path: "+p_oDest);
			if(!p_oDest.exists()){
				p_oDest.createNewFile();
			}else{
				p_oDest.deleteOnExit();
			}


			in = new FileInputStream(p_oSource);
			out = new FileOutputStream(p_oDest);
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			logger.info("FileUtil::copyFile::addFile:Exit");
		} catch (IOException e) {		
			logger.error("FileUtil::copyFile::EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}


		finally{
			in.close();
			out.close();
		}
	}

	public static boolean delete(File p_oResource) throws IOException{
		logger.info("FileUtil::delete:ENTER");
		try{
			if(p_oResource.isDirectory()){
				File[] oaChildFiles = p_oResource.listFiles();
				for(File oChild : oaChildFiles){
					delete(oChild);
				}
			}
			logger.info("FileUtil::delete::Exit");
		} catch (IOException e) {
			logger.error("FileUtil::delete::EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}
		return p_oResource.delete();


	}
}