package com.sbh.util;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DBConnection {
	public static long conCount;
	public static InitialContext ctx = null;
	private static Logger logger = LogManager.getLogger(DBConnection.class);

	public static Connection getSQL2005Connection()
	    throws NamingException, Exception	{
		logger.info("DBConnection::getSQL2005Connection():ENTRY");
		
	    Connection conn = null;
	    InitialContext ctx = null;
	    conCount++;
	    try
	    {
	        ctx = new InitialContext();
	        DataSource ds = (DataSource)ctx.lookup("jdbc/MSSQL2005");
	        //logger.debug("DBConnection::getSQL2005Connection():jdbc/MSSQLDS lookup successfull");
	        conn = ds.getConnection();
	        //logger.debug("DBConnection::getSQL2005Connection():connection estblished successfully from datasource");
	    }
	    catch(Exception e)
	    {
	        System.err.println("DBConnection Error is " + e.getMessage());
	        logger.error("DBConnection::getSQL2005Connection():Exception: " + e.getMessage());
	    }
	    
	    logger.info("DBConnection::getSQL2005Connection():EXIT");
	    return conn;
	}
}
