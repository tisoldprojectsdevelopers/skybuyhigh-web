package com.sbh.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GenerateCatalogueZip {
	private static Logger logger = LogManager.getLogger(GenerateCatalogueZip.class);	
	private ZipOutputStream oZipOutputStream=null;
	private static final int BUFFER = 2048;
	public GenerateCatalogueZip() {
		
	}
	public  GenerateCatalogueZip(String p_sPath,String p_sFileName) throws FileNotFoundException{
		String sZipFilename=null;
		p_sPath=p_sPath==null?"":p_sPath.trim();
		p_sFileName=p_sFileName==null?"":p_sFileName.trim();
		if(p_sPath.length()>0 && p_sFileName.length()>0){
			System.out.print(p_sPath.lastIndexOf("/"));
			if(p_sPath.lastIndexOf("/")==(p_sPath.length()-1) || p_sPath.lastIndexOf("\\")==(p_sPath.length()-1)){
				sZipFilename = p_sPath+p_sFileName;
			}else{
				sZipFilename = p_sPath+File.separator+p_sFileName;
			}
			File oFile =  new File(sZipFilename);
			if(oFile.exists())
				oFile.delete();
			oZipOutputStream = new ZipOutputStream(new FileOutputStream(sZipFilename));
		}
	}

	public void addFile(String p_sSrcPath,String p_sSrcFileName,String p_sDestFolderAndFileName) throws IOException{
		String sFilename=null;
		File oFileOrFolderName=null;
		logger.info("GenerateCatalogueZip::addFile:ENTER");	
		try {
			p_sSrcPath=p_sSrcPath==null?"":p_sSrcPath.trim();
			p_sSrcFileName=p_sSrcFileName==null?"":p_sSrcFileName.trim();

			if(p_sSrcPath.length()>0 && p_sSrcFileName.length()>0){
				if(p_sSrcPath.lastIndexOf("/")==(p_sSrcPath.length()-1) || p_sSrcPath.lastIndexOf("\\")==(p_sSrcPath.length()-1)){
					sFilename = p_sSrcPath+p_sSrcFileName;
				}else{
					sFilename = p_sSrcPath+File.separator+p_sSrcFileName;
				}
				oFileOrFolderName = new File(sFilename);
				if(oFileOrFolderName.isDirectory()){
					appendFolderToZip(oFileOrFolderName,oZipOutputStream,p_sDestFolderAndFileName);
				}else if(oFileOrFolderName.isFile()){
					appendFileToZip(oFileOrFolderName,oZipOutputStream,p_sDestFolderAndFileName);
				}
			}else{
				sFilename = p_sSrcPath;
				oFileOrFolderName = new File(sFilename);
				if(oFileOrFolderName.isDirectory()){
					appendFolderToZip(oFileOrFolderName,oZipOutputStream,p_sDestFolderAndFileName);
				}else if(oFileOrFolderName.isFile()){
					appendFileToZip(oFileOrFolderName,oZipOutputStream,p_sDestFolderAndFileName);
				}
			}
			logger.info("GenerateCatalogueZip::addFile:Exit");	
		} catch (FileNotFoundException e) {
			logger.error("GenerateCatalogueZip::addFile:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("GenerateCatalogueZip::addFile:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}
		
}
	public void finish() throws IOException{
		if(oZipOutputStream!=null){
			oZipOutputStream.finish();
			oZipOutputStream.flush();
			oZipOutputStream.close();
		}
	}
	public static void zipFolder(String p_SrcPath,String p_FileOrFolderName,String p_DestPath,String p_ZipFileName,File p_oAppendFileOrFolder){
		logger.info("GenerateCatalogueZip::zipFolder:ENTER");	
		String sZipFilename = p_DestPath+File.separator+p_ZipFileName;
		String sFileOrFolderName=p_SrcPath+File.separator+p_FileOrFolderName;
		ZipOutputStream oZipOutputStream=null;
		File oFileOrFolderName=null;
		try {


			oZipOutputStream = new ZipOutputStream(new FileOutputStream(sZipFilename));
			oFileOrFolderName = new File(sFileOrFolderName);
			int iLen = oFileOrFolderName.getAbsolutePath().lastIndexOf(File.separator);
			String sBaseName =oFileOrFolderName.getAbsolutePath().substring(0,iLen+1);//sFileOrFolderName+File.separator;
			addFolderToZip(oFileOrFolderName, oZipOutputStream, sBaseName);
			if(p_oAppendFileOrFolder!=null){
				iLen = p_oAppendFileOrFolder.getAbsolutePath().lastIndexOf(File.separator);
				sBaseName =p_oAppendFileOrFolder.getAbsolutePath().substring(0,iLen+1);
				if(p_oAppendFileOrFolder.isDirectory()){
					addFolderToZip(p_oAppendFileOrFolder, oZipOutputStream, sBaseName);
				}else{
					addFileToZip(p_oAppendFileOrFolder, oZipOutputStream, sBaseName);
				}
			}
			
			oZipOutputStream.finish();
			oZipOutputStream.flush();
			oZipOutputStream.close();
			logger.info("GenerateCatalogueZip::zipFolder:Exit");	
		} catch (FileNotFoundException e) {
			logger.error("GenerateCatalogueZip::zipFolder:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("GenerateCatalogueZip::zipFolder:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}	
		finally{
			if(oZipOutputStream!=null){
				try {
//					oZipOutputStream.finish();
					oZipOutputStream.flush(); 
					oZipOutputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			oFileOrFolderName=null;

		}
		logger.info("GenerateCatalogueZip::zipFolder:ENTER");	
	}

	private static void addFolderToZip(File p_ofolder, ZipOutputStream p_oZipOutputStream, String p_sBaseName) throws IOException {
		logger.info("GenerateCatalogueZip::addFolderToZip:ENTER");	
		File[] oFiles=null;
		try{
			oFiles = p_ofolder.listFiles();
			if(oFiles!=null){
				for (File ofile : oFiles) {
					if(ofile!=null){
						addFileToZip(ofile, p_oZipOutputStream, p_sBaseName);
					}
				}
			}
			logger.info("GenerateCatalogueZip::addFolderToZip:Exit");
		}catch (IOException e) {
			logger.error("GenerateCatalogueZip::zipFolder:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}	

	}
	private static void addFileToZip(File p_ofolder, ZipOutputStream p_oZipOutputStream, String p_sBaseName) throws IOException {
		logger.info("GenerateCatalogueZip::addFileToZip:ENTER");
		try{
			if(p_ofolder!=null){
				if (p_ofolder.isDirectory()) {
					addFolderToZip(p_ofolder, p_oZipOutputStream, p_sBaseName);
				} else {
					String sName = p_ofolder.getAbsolutePath().substring(p_sBaseName.length());
					ZipEntry oZipEntry = new ZipEntry(sName);
					p_oZipOutputStream.putNextEntry(oZipEntry);
					IOUtils.copy(new FileInputStream(p_ofolder), p_oZipOutputStream);
					p_oZipOutputStream.closeEntry();
				}
			}
		}catch (IOException e) {
			logger.error("GenerateCatalogueZip::addFileToZip:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}	
		logger.info("GenerateCatalogueZip::addFileToZip:Exit");
	}
	private  void appendFolderToZip(File p_ofolder, ZipOutputStream p_oZipOutputStream, String p_sDestFolderAndFileName) throws IOException {
		logger.info("GenerateCatalogueZip::appendFolderToZip:ENTER");	
		File[] oFiles=null;
		try{
			oFiles = p_ofolder.listFiles();
			if(oFiles!=null){
				for (File ofile : oFiles) {
					if(ofile!=null){
						appendFileToZip(ofile, p_oZipOutputStream, p_sDestFolderAndFileName);
					}
				}
			}
			logger.info("GenerateCatalogueZip::appendFolderToZip:Exit");
		}catch (IOException e) {
			logger.error("GenerateCatalogueZip::appendFolderToZip:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}	
	}
	private  void appendFileToZip(File p_ofolder, ZipOutputStream p_oZipOutputStream, String p_sDestFolderAndFileName) throws IOException {
		logger.info("GenerateCatalogueZip::appendFileToZip:Enter");
		try{
			if(p_ofolder!=null){
				if (p_ofolder.isDirectory()) {
					addFolderToZip(p_ofolder, p_oZipOutputStream, p_sDestFolderAndFileName);
				} else {

					ZipEntry oZipEntry = new ZipEntry(p_sDestFolderAndFileName);
					p_oZipOutputStream.putNextEntry(oZipEntry);
					IOUtils.copy(new FileInputStream(p_ofolder), p_oZipOutputStream);
					p_oZipOutputStream.closeEntry();

				}
			}

			logger.info("GenerateCatalogueZip::appendFileToZip:Exit");
		}catch (IOException e) {
			logger.error("GenerateCatalogueZip::appendFileToZip:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}	
	}
}
