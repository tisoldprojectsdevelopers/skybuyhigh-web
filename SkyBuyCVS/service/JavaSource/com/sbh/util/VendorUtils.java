package com.sbh.util;

import static com.sbh.contants.SkyBuyContants.AIRLINE;
import static com.sbh.contants.SkyBuyContants.CC_CODE_FAILED_STATUS;
import static com.sbh.contants.SkyBuyContants.CC_CODE_SUCCESS_STATUS;
import static com.sbh.contants.SkyBuyContants.FULLY_AUTOMATED;
import static com.sbh.contants.SkyBuyContants.HTML_FILE_TYPE;
import static com.sbh.contants.SkyBuyContants.ORDER_COMPLETED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_FAILED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_TOBE_CHARGED_STATUS;
import static com.sbh.contants.SkyBuyContants.PAYMENT_APPROVED;
import static com.sbh.contants.SkyBuyContants.PDF_FILE_TYPE;
import static com.sbh.contants.SkyBuyContants.PRE_AUTHORIZATION_NO;
import static com.sbh.contants.SkyBuyContants.PRE_AUTHORIZATION_YES;
import static com.sbh.contants.SkyBuyContants.SEMI_AUTOMATED;
import static com.sbh.contants.SkyBuyContants.VENDOR;
import static com.sbh.dao.OrderDAO.sendOrderItemsFailureEmailToVendor;
import static com.sbh.dao.OrderDAO.updateOrderItemStatus;
import static com.sbh.dao.PaymentDAO.postAuthorizePayment;
import static com.sbh.dao.PaymentDAO.salePayment;
import static com.sbh.util.CustomerUtils.createBlob;
import static com.sbh.util.Utils.getRandomNo;

import java.awt.Dimension;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.zefer.pd4ml.PD4ML;

import com.sbh.dao.OrderDAO;
import com.sbh.vo.OrderItemDetailsVO;
import com.sbh.vo.PaymentResponse;

public class VendorUtils {

	private static Logger logger = LogManager.getLogger(VendorUtils.class);

	public static void placeOrderItemsToVendor(OrderItemDetailsVO p_oOrderItemDetailsVO, String p_sFilePath, String p_sLoginId,String p_sIpAddress) throws Exception{
		logger.info("VendorUtils:placeOrderItemsToVendor:ENTER");
		boolean bSentEmail = false;
		byte[] baPdf = null;
		PaymentResponse oPaymentResponse =null;
		String sMsg = null;
		try{			

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			String sAmount = p_oOrderItemDetailsVO.getPayAmount();
			sAmount=sAmount==null?"":sAmount.trim();


			bSentEmail = OrderDAO.sendOrderItemsEmailToVendor(p_oOrderItemDetailsVO);

			/**** Insert Order audit details ******/							
			sMsg=oBundle.getString("order.vendor.placement.msg");
			sMsg=sMsg==null?"":sMsg.trim();								
			OrderDAO.insertOrderAuditDetails(p_oOrderItemDetailsVO.getOrderItemId(),p_oOrderItemDetailsVO.getOrderId(),p_oOrderItemDetailsVO.getCustTransId(),sMsg,p_sLoginId,null,p_sLoginId,"");								


			int iOrderNumber = getRandomNo();
			// System.out.println("OrderNumber"+iOrderNumber);
			if(FULLY_AUTOMATED.equalsIgnoreCase(p_oOrderItemDetailsVO.getChargeType())) {
				if(bSentEmail){
					//Charge Amount from the card
					if(PRE_AUTHORIZATION_YES.equalsIgnoreCase(p_oOrderItemDetailsVO.getPreAuthCC()))
						oPaymentResponse = postAuthorizePayment(p_oOrderItemDetailsVO,p_sLoginId,p_sIpAddress);
					else
						oPaymentResponse = salePayment(p_oOrderItemDetailsVO,p_sLoginId,p_sIpAddress);


					if(PAYMENT_APPROVED.equalsIgnoreCase(oPaymentResponse.getR_Approved())){			
						//To delete the CVV
						if(PRE_AUTHORIZATION_NO.equalsIgnoreCase(p_oOrderItemDetailsVO.getPreAuthCC()))
							OrderDAO.updateCvvStatus(p_oOrderItemDetailsVO.getCustId(), CC_CODE_SUCCESS_STATUS,p_sLoginId);

						//Add Customer Invoice Details
						OrderDAO.addCustomerInvoiceDetails(p_oOrderItemDetailsVO,p_sLoginId);
						
						String htmlText = createBlob(p_oOrderItemDetailsVO);
						String sHTMLFileName = p_sFilePath+p_oOrderItemDetailsVO.getInvoiceNo()+HTML_FILE_TYPE;

						BufferedWriter oBufWriter = new BufferedWriter(new FileWriter(sHTMLFileName));
						ByteArrayOutputStream oByteArrayOutputStream=new ByteArrayOutputStream();
						oBufWriter.write(htmlText);
						oBufWriter.close();

						PD4ML pd4ml = new PD4ML();
						pd4ml.useTTF( "java:/fonts", true );
						pd4ml.setPageSize(new Dimension(615,790));
						pd4ml.setPageInsets(new Insets(2, 5, 0, 2));
						pd4ml.setHtmlWidth(1230);

						pd4ml.enableDebugInfo();

						pd4ml.render("file:" + sHTMLFileName, oByteArrayOutputStream);

						baPdf=oByteArrayOutputStream.toByteArray();

						String pdfFileName=p_sFilePath+p_oOrderItemDetailsVO.getInvoiceNo()+PDF_FILE_TYPE;
						FileOutputStream fileos = new FileOutputStream(pdfFileName);
						fileos.write(baPdf);
						fileos.close();

						File oPdfFile = new File(pdfFileName);
						File oHtmlFile = new File(sHTMLFileName);
						OrderDAO.sendOrderItemsSuccessEmail(p_oOrderItemDetailsVO,baPdf,iOrderNumber,oPdfFile,p_sLoginId,"");
						if(!AIRLINE.equalsIgnoreCase(p_oOrderItemDetailsVO.getOwnerType())){
							OrderDAO.sendAirlineCommissionEMail(p_oOrderItemDetailsVO,p_sFilePath,p_sLoginId,"");
						}
						OrderDAO.updateOrderItemStatus(p_oOrderItemDetailsVO.getOrderItemId(),ORDER_COMPLETED_STATUS,baPdf,p_sLoginId);
						
						if(VENDOR.equalsIgnoreCase(p_oOrderItemDetailsVO.getOwnerType())){
							OrderDAO.createAndSendVendorPO(p_oOrderItemDetailsVO,p_sFilePath,p_sLoginId,"");
						}else if(AIRLINE.equalsIgnoreCase(p_oOrderItemDetailsVO.getOwnerType())){
							OrderDAO.createAndSendAirlinePO(p_oOrderItemDetailsVO,p_sFilePath,p_sLoginId,"");
						}
						/**** Insert Order audit details ******/							
						sMsg=oBundle.getString("customer.order.success.msg");
						sMsg=sMsg==null?"":sMsg.trim();								
						OrderDAO.insertOrderAuditDetails(p_oOrderItemDetailsVO.getOrderItemId(),p_oOrderItemDetailsVO.getOrderId(),p_oOrderItemDetailsVO.getCustTransId(),sMsg,"Admin",null,p_sLoginId,"");								


						if(oPdfFile.exists()) {
							oPdfFile.delete();
						}
						if(oHtmlFile.exists()) {
							oHtmlFile.delete();
						}
					}else{
						//To delete the CVV
						if(PRE_AUTHORIZATION_NO.equalsIgnoreCase(p_oOrderItemDetailsVO.getPreAuthCC()))
							OrderDAO.updateCvvStatus(p_oOrderItemDetailsVO.getCustId(), CC_CODE_FAILED_STATUS,p_sLoginId);

						
						sMsg=oBundle.getString("postauth.ccno.error.msg");
						sMsg=sMsg==null?"":sMsg.trim();		
						p_oOrderItemDetailsVO.setCCNoStatus(ORDER_FAILED_STATUS);
						p_oOrderItemDetailsVO.setCCNoComments(sMsg);
						OrderDAO.updateOrderItemStatus(p_oOrderItemDetailsVO.getOrderItemId(),ORDER_FAILED_STATUS,null,p_sLoginId);
						OrderDAO.sendOrderItemsFailureEmail(p_oOrderItemDetailsVO,sMsg,0,p_sLoginId,"");
						sendOrderItemsFailureEmailToVendor(p_oOrderItemDetailsVO,sMsg,0,p_sLoginId,"");
						
						/**** Insert Order audit details ******/												
						OrderDAO.insertOrderAuditDetails(p_oOrderItemDetailsVO.getOrderItemId(),p_oOrderItemDetailsVO.getOrderId(),p_oOrderItemDetailsVO.getCustTransId(),sMsg,p_sLoginId,null,"","");								
					}
				}
			}else if(SEMI_AUTOMATED.equalsIgnoreCase(p_oOrderItemDetailsVO.getChargeType())) {
				updateOrderItemStatus(p_oOrderItemDetailsVO.getOrderItemId(),ORDER_TOBE_CHARGED_STATUS,null,p_sLoginId);
			}

			logger.info("VendorUtils:placeOrderItemsToVendor:EXIT");
		}
		catch(Exception e){		
			e.printStackTrace();
			logger.error("VendorUtils:placeOrderItemsToVendor:Exception "+e.getMessage());
			throw e;
		}
	}	
}
