/**
 * 
 */
package com.sbh.util;

import org.tiling.scheduling.Scheduler;
import org.tiling.scheduling.SchedulerTask;
import org.tiling.scheduling.examples.iterators.DailyIterator;

/**
 * @author Thapovan
 *
 */
public class EmailScheduler {
	private final Scheduler scheduler = new Scheduler();
    private final int hourOfDay, minute, second;
    
    public EmailScheduler(int hourOfDay, int minute, int second) {
        this.hourOfDay = hourOfDay;
        this.minute = minute;
        this.second = second;
    }
    
    public void startIsDeviceUpdate() {
        scheduler.schedule(new SchedulerTask() {
            public void run() {
            	checkDeviceUpdate();
            }
            private void checkDeviceUpdate() {
            	// System.out.println("Job Scheduler started");
            	EmailScheduleValidate oEmailScheduleValidate = new EmailScheduleValidate();
            	oEmailScheduleValidate.emailScheduleValidate();
            }
        }, new DailyIterator(hourOfDay, minute, second));
    }
    
    public void startIsDeviceUsed() {
        scheduler.schedule(new SchedulerTask() {
            public void run() {
            	checkDeviceUsed();
            }
            private void checkDeviceUsed() {
            	// System.out.println("Job Scheduler started");
            	EmailScheduleValidate oEmailScheduleValidate = new EmailScheduleValidate();
            	oEmailScheduleValidate.emailScheduleValidateDeviceUsed();
            }
        }, new DailyIterator(hourOfDay, minute, second));
    }
    
    public void startGenerateFullCatalogue() {
        scheduler.schedule(new SchedulerTask() {
            public void run() {
            	generateFullCatalogue();
            }
            private void generateFullCatalogue() {
            	// System.out.println("Job Scheduler started");
            	EmailScheduleValidate oEmailScheduleValidate = new EmailScheduleValidate();
            	oEmailScheduleValidate.generateFullCatalogue();
            }
        }, new DailyIterator(hourOfDay, minute, second));
    }
    
    public void startGenerateDemoCatalogueXML() {
        scheduler.schedule(new SchedulerTask() {
            public void run() {
            	generateDemoCatalogueXML();
            }
            private void generateDemoCatalogueXML() {
            	// System.out.println("Job Scheduler started");
            	EmailScheduleValidate oEmailScheduleValidate = new EmailScheduleValidate();
            	oEmailScheduleValidate.generateDemoCatalogueXML();
            }
        }, new DailyIterator(hourOfDay, minute, second));
    }
    
    public void cancel() {
    	// System.out.println("Job Scheduler Ended");
    	scheduler.cancel();
    }
	/*public static void main(String args[]) {
	    try{
	    	EmailScheduler oEmailScheduler = new EmailScheduler(1,10,25);
	    	oEmailScheduler.startIsDeviceUpdate();
	    	oEmailScheduler.startIsDeviceUsed();
	    	// System.out.println("Email Scheduler started");
//	    	oEmailScheduler.cancel();
//	    	// System.out.println("Email Scheduler stopped");
	    }catch(Exception e){
	    	e.printStackTrace();
	    }    
	}*/
}
