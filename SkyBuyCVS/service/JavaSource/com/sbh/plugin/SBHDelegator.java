package com.sbh.plugin;
 
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

import com.sbh.util.DBConnection;
import com.sbh.util.EmailScheduler;
import com.sbh.util.Utils;
import com.sbh.vo.AdminEmailAddressVO;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.DeviceTypesVO;
import com.sbh.vo.MerchandizeSearchVO;
import com.sbh.vo.StateVO;


public class SBHDelegator implements PlugIn{
	private static Logger logger = LogManager.getLogger(SBHDelegator.class);
	
	List<CategoryVO> alCategory = null;
	List<MerchandizeSearchVO> alMerchandizeSearch=null;
	List<StateVO> alStateList=null;
	List<DeviceTypesVO> alDeviceTypes = null;
	List<AdminEmailAddressVO> alAdminEmailId = null;
	Map<String,String> hmInstallPkgList = null;
	Map<String,String> hmAllInstallPkgList = null;
	
	private static ServletContext m_oServletContext = null; 
	private static EmailScheduler oEmailScheduler = null;
	private static final String SCHEDULER_STATUS = "ACTIVE";
	
	public void init(ActionServlet actionServlet, ModuleConfig arg1) {
		logger.info("SBHDelegator::init:ENTER");
		
		ServletConfig config = actionServlet.getServletConfig();
		m_oServletContext=config.getServletContext();
		String sHomePageContent = null;
		String sSchedulerStatus = null;
		String sGenerateCatalogueStatus = null;
		try{
			sSchedulerStatus = System.getProperty("SchedulerStatus"); 
			sGenerateCatalogueStatus = System.getProperty("GenerateCatalogueStatus");
			
			alMerchandizeSearch=getMerchandizeSearchCreteria();
			alCategory=(List<CategoryVO>)getCategory();
			m_oServletContext.setAttribute("MerchandizeSearch",alMerchandizeSearch);
			m_oServletContext.setAttribute("Category",alCategory);
			sHomePageContent ="<artworkinfo><albuminfo><artLocation>coverflow/1.jpg</artLocation><artist>SkyBuy test</artist><Name>SkyBuy Pics-Sri</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/2.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-2</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/3.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-3</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/5.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-5</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/4.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-4</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/6.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-6</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/7.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-7</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/8.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-8</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/9.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-9</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo><albuminfo><artLocation>coverflow/10.jpg</artLocation><artist>SkyBuy</artist><Name>SkyBuy Pics-10</Name><artistLink>http://skybuyhigh.com</artistLink><Link>http://skybuyhigh.com</Link></albuminfo></artworkinfo>";
			m_oServletContext.setAttribute("homePageContent", sHomePageContent);
			alStateList=(List<StateVO>)getState();
			m_oServletContext.setAttribute("StateList",alStateList);
			alDeviceTypes = getDeviceTypes();
			m_oServletContext.setAttribute("DeviceTypesList",alDeviceTypes );
			
			hmInstallPkgList = getMasterInstallationPackage();
			m_oServletContext.setAttribute("MasterInstallPkgList",hmInstallPkgList );
			
			hmAllInstallPkgList = getAllMasterInstallationPackage();
			m_oServletContext.setAttribute("AllMasterInstallPkgList",hmAllInstallPkgList);
			
			
			if(SCHEDULER_STATUS.equalsIgnoreCase(sSchedulerStatus)) {
				logger.info("SBHDelegator::init:SCHEDULAR Started");
				oEmailScheduler = new EmailScheduler(10,0,0); 
				oEmailScheduler.startIsDeviceUpdate();
				oEmailScheduler = new EmailScheduler(10,0,0);
				oEmailScheduler.startIsDeviceUsed();
			}
			if(SCHEDULER_STATUS.equalsIgnoreCase(sGenerateCatalogueStatus)) {
				logger.info("SBHDelegator::init:Full Catalogue Schedular Started");
				oEmailScheduler = new EmailScheduler(3,0,0);
				oEmailScheduler.startGenerateFullCatalogue();
			}
			/*oEmailScheduler = new EmailScheduler(2,0,0);
			oEmailScheduler.startGenerateDemoCatalogueXML();*/
		}catch(Exception e){
			logger.error("SBHDelegator::init:EXCEPTION "+e.getMessage());
			e.printStackTrace();
		}
	}
	private List<MerchandizeSearchVO> getMerchandizeSearchCreteria() throws Exception{
		logger.info("SBHDelegator::getMerchandizeSearchCreteria:ENTER");
		ResourceBundle oResourceBundle=ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
		String sSearchId=null;
		String sSearchLabel=null,sLabelKey=null,sValueKey=null;
		MerchandizeSearchVO oMerchandizeSearchVO=null;
		List<MerchandizeSearchVO> alMerchandizeSearch=null;
		try{
			alMerchandizeSearch=new ArrayList<MerchandizeSearchVO>();
		for(int i=1;i<=5;i++){
			oMerchandizeSearchVO=new MerchandizeSearchVO();
			sLabelKey="merchandizeSearchCreteriaLabel"+i;
			sValueKey="merchandizeSearchCreteriaValue"+i;
			sSearchLabel=oResourceBundle.getString(sLabelKey);
			sSearchLabel=sSearchLabel==null?"":sSearchLabel.trim();
			sSearchId=oResourceBundle.getString(sValueKey);
			sSearchId=sSearchId==null?"":sSearchId.trim();
		 oMerchandizeSearchVO.setSearchLabel(sSearchLabel);
		 oMerchandizeSearchVO.setSearchId(sSearchId);
		 alMerchandizeSearch.add(oMerchandizeSearchVO);
		}
		
		logger.info("SBHDelegator::getMerchandizeSearchCreteria:EXIT");
		}catch(Exception e){
			logger.error("SBHDelegator::getMerchandizeSearchCreteria:EXCEPTION" +e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return alMerchandizeSearch;
	}
	public List<CategoryVO> getCategory() throws Exception{
		logger.info("SBHDelegator::getCategory:ENTER");
		List<CategoryVO> alCategory=new ArrayList<CategoryVO>();
		Connection con=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		CategoryVO oCategoryVO=null;

		String sQry="{call usp_sbh_get_category }";

		try{
			con=DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			while(rs.next()){
				oCategoryVO=new CategoryVO();
				oCategoryVO.setCateId(rs.getString("cate_id"));
				oCategoryVO.setCateName(rs.getString("cate_name"));				
				alCategory.add(oCategoryVO);
			}

			logger.info("SBHDelegator::getCategory:EXIT");
		}catch(Exception e){
			logger.error("SBHDelegator::getCategory:EXCEPTION" +e.getMessage());
			e.printStackTrace();
			throw e;

		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		// System.out.println("Category size:"+alCategory.size());
		return alCategory;
	}
	public List<StateVO> getState() throws Exception{
		logger.info("SBHDelegator::getState:ENTER");
		List<StateVO> arState=new ArrayList<StateVO>();
		Connection con=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		StateVO oStateVO=null;		
		String sQry="{call usp_sbh_get_states}";

		try{
			con=DBConnection.getSQL2005Connection();			
			cstmt=con.prepareCall(sQry);		
			rs=cstmt.executeQuery();
			while(rs.next()){
				oStateVO=new StateVO();
				oStateVO.setStateCode(rs.getString("state_id"));			
				oStateVO.setStateName(rs.getString("state_name"));				
				arState.add(oStateVO);
			}

			logger.info("SBHDelegator::getState:EXIT");
		}catch(Exception e){
			logger.error("SBHDelegator::getState:EXCEPTION" +e.getMessage());
			e.printStackTrace();
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();
			
		}
		return arState;
	}
	public static List<DeviceTypesVO> getDeviceTypes() throws Exception{	
		logger.info("getDeviceTypes:ENTER");		
		List<DeviceTypesVO> alDeviceType = new ArrayList<DeviceTypesVO>();
		DeviceTypesVO oDeviceTypesVO=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_device_types()}";
		String sDBData = "";
		logger.debug("getDeviceTypes:SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				oDeviceTypesVO = new DeviceTypesVO();
				sDBData = rs.getString("device_type_id");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceTypesVO.setDeviceId(sDBData);
				
				sDBData = rs.getString("device_type_name");
				sDBData = sDBData==null?"":sDBData.trim();
				oDeviceTypesVO.setDeviceName(sDBData);

				alDeviceType.add(oDeviceTypesVO);				
			}
			logger.info("getDeviceTypes:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("getDeviceTypes:Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		return alDeviceType;
	}
	
	
	public static Map<String,String> getMasterInstallationPackage() throws Exception{	
		logger.info("SBHDelegator::getMasterInstallationPackage:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		Map<String,String> hmInstallPkg = new LinkedHashMap<String, String>();
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_master_installation_package()}";
		String sKey = "";
		String sValue = "";
		logger.debug("SBHDelegator::getMasterInstallationPackage:SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				sKey = rs.getString("install_pkg_name");
				sKey = sKey==null?"":sKey.trim();
				
				sValue = rs.getString("install_pkg_desc");
				sValue = sValue==null?"":sValue.trim();
				
				hmInstallPkg.put(sKey, sValue);
								
			}
			logger.info("SBHDelegator::getMasterInstallationPackage:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("SBHDelegator::getMasterInstallationPackage:Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);
		}
		return hmInstallPkg;
	}
	
	public static Map<String,String> getAllMasterInstallationPackage() throws Exception{	
		logger.info("SBHDelegator::getAllMasterInstallationPackage:ENTER");		
		Connection con=null;		
		CallableStatement cstmt=null;
		Map<String,String> hmInstallPkg = new LinkedHashMap<String, String>();
		ResultSet rs=null;
		String sQry="{call usp_sbh_get_all_master_installation_package()}";
		String sKey = "";
		String sValue = "";
		logger.debug("SBHDelegator::getAllMasterInstallationPackage:SQL QUERY "+sQry);		

		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);			
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()){
				sKey = rs.getString("install_pkg_name");
				sKey = sKey==null?"":sKey.trim();
				
				sValue = rs.getString("install_pkg_desc");
				sValue = sValue==null?"":sValue.trim();
				
				hmInstallPkg.put(sKey, sValue);
								
			}
			logger.info("SBHDelegator::getAllMasterInstallationPackage:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("SBHDelegator::getAllMasterInstallationPackage:Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);
		}
		return hmInstallPkg;
	}

	
	
	
	
	
	public void destroy() {
		logger.info("SBHDelegator::destroy::Entry");
			if(oEmailScheduler != null) {
				oEmailScheduler.cancel();
			}
		logger.info("SBHDelegator::destroy::Exit");
	}

}
