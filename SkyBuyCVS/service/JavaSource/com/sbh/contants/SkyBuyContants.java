/**
 * 
 */
package com.sbh.contants;

/**
 * @author nbvk
 *
 */
public class SkyBuyContants {
	
	/*
	 * -------------------------------------- OTHER CONSTANTS--------------------------------------
	 */
	
	/**
	 * Constant airline.
	 */
	public static final String AIRLINE = "airline";
	
	/**
	 * Constant admin.
	 */
	public static final String ADMIN = "admin";
	
	/**
	 * Constant vendor.o
	 */
	public static final String VENDOR = "vendor";
	
	/**
	 * Product details constant
	 */
	public static final String PRODUCT_DETAILS="Prod_Det";
	
	/**
	 * Airline details constant
	 */
	public static final String AIRLINE_DETAILS="Air_Det";
	
	/**
	 * Fully automated process of credit card
	 */
	public static final String FULLY_AUTOMATED="FA";
	
	/**
	 * Semi automated process of credit card
	 */
	public static final String SEMI_AUTOMATED="SA";
	
	/**
	 * Pre authorization is no
	 */
	public static final String PRE_AUTHORIZATION_NO="N";
	
	/**
	 * Pre authorization is yes
	 */
	public static final String PRE_AUTHORIZATION_YES="Y";
	
	/**
	 * Airline category folder name
	 */
	public static final String AIRLINE_CATAGEORY_NAME="PrivateJetJaunts";
	
	/**
	 * Airline Login Information
	 */
	public static final String AIRLINE_LOGIN_INFO="airlineLoginInfo";
	
	/**
	 * Admin Login Information
	 */
	public static final String ADMIN_LOGIN_INFO="adminLoginInfo";
	
	/**
	 * Vendor Login Information
	 */
	public static final String VENDOR_LOGIN_INFO="vendorLoginInfo";
	
	/**
	 * Pending Orders
	 */
	public static final String PENDING_ORDERS="pendingOrders";
	
	/**
	 * Incompleted Orders
	 */
	public static final String INCOMPLETE_ORDERS="incompleteOrders";
	
	/**
	 * Completed Orders
	 */
	public static final String COMPLETED_ORDERS="compltedOrders";
	
	/**
	 * Completed Orders
	 */
	public static final String PENDING_ORDERS_INFO="pendingOrderItemsInfo";
	
	/**
	 * No Records Key
	 */
	public static final String NO_RECORDS_KEY="NoRecords";
	
	/**
	 * No Records Constants
	 */
	public static final String NO_RECORDS="noRecords";
	
	/**
	 * No Records Constants
	 */
	public static final String NO_RECORDS_FOUND="No Records Found";
		
	/**
	 * Message when order item id parameter value is not available.
	 */
	public static final String NO_ORDERITEMID_PARAMETER = "Order Item Id is not available.";
	
	/**
	 * Order is already Processed
	 */
	public static final String ORDER_ALREADY_PROCESSED = "This order is already processed";
	
	/**
	 * Order is in process
	 */
	public static final String ORDER_IN_PROCESS = "Order is in process";
	
	/**
	 * Order is already rejected
	 */
	public static final String ORDER_ALREADY_REJECTED = "This order is already rejected";
	
	/**
	 * Order is already completed
	 */
	public static final String ORDER_ALREADY_COMPLETED = "This order is already completed";
	
	/**
	 * Order is already cancelled
	 */
	public static final String ORDER_ALREADY_CANCELED = "This order is already canceled";
	
	/**
	 * Invoice constant.
	 */
	public static final String INVOICE = "INVOICE";
	
	/**
	 * Active Status.
	 */
	public static final String ACTIVE_STATUS = "A";
	
	/**
	 * Invoice constant.
	 */
	public static final String IPHONE = "IPhone";
	
	/**
	 * Active Status.
	 */
	public static final String ITOUCH = "ITouch";
	
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * -------------------------------------- ORDER STATUS CONSTANTS--------------------------------------
	 */
	/**
	 * Order Pending Status.
	 */
	public static final String ORDER_PENDING_STATUS = "P";
	
	/**
	 * Order to be charged after vendor or airline approval Status.
	 */
	public static final String ORDER_TOBE_CHARGED_STATUS = "Q";
	
	/**
	 * Order completed Status.
	 */
	public static final String ORDER_COMPLETED_STATUS = "C";
	
	/**
	 * Order Rejected Status.
	 */
	public static final String ORDER_FAILED_STATUS = "F";
	
	/**
	 * Order Rejected Status.
	 */
	public static final String ORDER_REJECTED_STATUS = "R";
	
	/**
	 * Order Canceled Status.
	 */
	public static final String ORDER_CANCELED_STATUS = "O";
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * -------------------------------------- CC CODE CONSTANTS--------------------------------------
	 */
	/**
	 * Order completed Status.
	 */
	public static final String CC_CODE_SUCCESS_STATUS = "S";
	
	/**
	 * Order Rejected Status.
	 */
	public static final String CC_CODE_FAILED_STATUS = "F";
	
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * -------------------------------------- FILE EXTENSION CONSTANTS--------------------------------------
	 */
	/**
	 * File extension type is html.
	 */
	public static final String HTML_FILE_TYPE = ".html";
	
	/**
	 * File extension type is pdf.
	 */
	public static final String PDF_FILE_TYPE = ".pdf"; 
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * ------------------------------------- PAYMENT DAO CONSTANTS ---------------------------------
	 */
	/**
	 * Payment type is Pre authentication
	 */
	public static final String PRE_AUTHENTICATION = "PREAUTH";
	/**
	 * Payment type is Post authentication
	 */
	public static final String POST_AUTHENTICATION = "POSTAUTH";
	/**
	 * Payment type is Credit and Cancel
	 */
	public static final String CREDIT = "CREDIT";
	/**
	 * Payment type is Sale
	 */
	public static final String SALE = "SALE";
	/**
	 * Payment is Approved
	 */
	public static final String PAYMENT_APPROVED = "APPROVED";
	/**
	 * Payment is Approved
	 */
	public static final String TRANSACTION_SUCCESS = "TS";
	/**
	 * Payment is Approved
	 */
	public static final String TRANSACTION_FAILED = "TF";
	/**
	 * Tax excemption is yes
	 */
	public static String TAX_EXCEMPTION_YES = "y";
	/**
	 * Mode of re-payment is cancel.
	 */
	public static String MODE_OF_REPAYMENT_CANCEL="CANCEL";
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * ------------------------------------- UPLOAD TYPE ADMIN ---------------------------------
	 */
	/**
	 * Upload type is Admin application
	 */
	public static String UPLOADTYPE_ADMIN = "Admin";
	/**
	 * Upload type is ShoppingCart application
	 */
	public static String UPLOADTYPE_SHOPPINGCART = "ShoppingCart";
	/**
	 * Upload type is ShoppingCart application
	 */
	public static String UPLOADTYPE_VIRTUALDESKTOP = "VirtualDesktop";
	/**
	 * Upload type is Auto Update application
	 */
	public static String UPLOADTYPE_AUTOUDPATE = "AutoUpdate";
	/**
	 * Upload type is Schema application
	 */
	public static String UPLOADTYPE_SCHEMA = "Schema";
	/**
	 * Upload type is Service application
	 */
	public static String UPLOADTYPE_SERVICE = "Service";
	/**
	 * Upload type is User name application
	 */
	public static String UPLOADTYPE_USERNAME = "Username";
	/**
	 * Upload type is client certificate application
	 */
	public static String UPLOADTYPE_CLIENTCERT = "ClientCert";
	/**
	 * Upload type is Welcome page swf
	 */
	public static String UPLOADTYPE_WELCOMEPAGE = "WelcomePage";
	/**
	 * Upload type is Skybuy catalogue swf
	 */
	public static String UPLOADTYPE_SKYBUYCATALOGUE = "ECatalogue";
	/**
	 * Upload type is iPhone Desktop Image
	 */
	public static String UPLOADTYPE_IPHONEDESKTOPIMAGE = "iPhoneDesktop";
	
	
	

	
	public static String UPLOADTYPE_IPHONEPERSONAL = "iPhonePersonalShopper";
	public static String UPLOADTYPE_IPHONEWELCOMEPAGE = "iPhoneWelcomePage";
	
	public static String UPLOADTYPE_PERSONALSHOPPER = "personalShopper";
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * -------------------------------Place Order constants-----------------------------------------
	 */
	/**
	 * Invalid Credit card placed from the client side.
	 * <code>"This card is invalid or unsupported"</code> 
	 */
	public static final String INVALID_CREDITCARD = "ICC";
	
	/**
	 * Order placed from the client is failed.
	 * <code>"Order placement failed"</code> 
	 */
	public static final String ORDER_FAILED = "OPF";
	
	/**
	 * Order placed from the client is success.
	 * <code>"Order placement Success"</code> 
	 */
	public static final String ORDER_PLACED = "OPS";
	
	/**
	 * Order already placed from the client is success.
	 * <code>"Order already placed success"</code> 
	 */
	public static final String ORDER_ALREADY_PLACED="OAP";
	
	/**
	 * Invalid total value of order
	 * <code>"Invalid Total Order Value"</code> 
	 */
	public static final String INVALID_TOTAL_ORDER_VALUE="IOV";
	
	/**
	 * Error while parsing XML which is get from the client
	 * <code>"Error while parsing XML"</code> 
	 */
	public static final String ERROR_PARSING_XML="EPX";
	
	/**
	 * Could not read the file any file.
	 * <code>"Could not completely read file "</code>
	 */
	public static final String CANNOT_READ_FILE="Could not completely read file ";
	/**
	 * Could not read the file any file.
	 * <code>"Three times failed order no credit card no "</code>
	 */
	public static final String THREE_TIME_FAILED_ORDER="Three times failed order no credit card no";
	/**
	 * Credit card status is failed.
	 */
	public static final String CREDITCARD_FAILED = "F";
	/**
	 * Credit card status is not processed.
	 */
	public static final String CREDITCARD_NOT_PROCESSED = "NP";
	
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
	/*
	 * -------------------------------Device Registration constants---------------------------------
	 */
	/**
	 * Device is not registered with skybuyhigh.
	 * <code>"Device not Registered with SkyBuyHigh"</code>
	 */
	public static final String DEVICE_NOT_REGISTERED = "DNR";
	
	/**
	 * Provided airline code is invalid.
	 * <code>"Invalid Airline Code"</code>
	 */
	public static final String INVALID_AIRLINE_CODE = "IAC";
	
	/**
	 * Airline is in inactive status.
	 * <code>"Airline is Inactive"</code>
	 */
	public static final String INACTIVE_AIRLINE = "INA";
	
	/**
	 * Given product key is invalid.
	 * <code>"Invalid Product Key"</code>
	 */
	public static final String INVALID_PRODUCT_KEY="IPK";
	
	/**
	 * The requested device is not allocated to any of the airline.
	 * <code>"Device is not allocated"</code>
	 */
	public static final String DEVICE_NOT_ALLOCATED="DNA";
	
	/**
	 * To mention that particular Device is not activated.
	 * <code>"Device is not activated"</code> 
	 */
	public static final String DEVICE_NOT_ACTIVATED="DNC";
	
	/**
	 * To mention that Device is in inactive status.
	 * <code>"Device is Inactive"</code>
	 */
	public static final String DEVICE_INACTIVE="DIN";
	
	/**
	 * To mention that particular device is deleted.
	 * <code>"Device is Deleted"</code>
	 */
	public static final String DEVICE_DELETED="DID";
	
	/**
	 * To mention there is an exception caught while processing the request.
	 * <code>"Problem in device authentication"</code>
	 */
	public static final String DEVICE_EXCEPTION="DEX";
	
	/**
	 * To mention that particular device is invalid.
	 * <code>"Invalid Device"</code>
	 */
	public static final String INVALID_DEVICE="IND";
	
	/**
	 * Invalid airline or product key is provided.
	 * <code>"Invalid Airline Code/Product key"</code>
	 */
	public static final String INVALID_AIRLINE_OR_PRODUCT_KEY="IAP";
	
	/**
	 * To mention that particular device is already registered with skybuyhigh.
	 * <code>"Device is already Registered"</code>
	 */
	public static final String DEVICE_ALREADY_REGISTERED="DAR";
	
	/**
	 * To mention that device which is registered with the airline is deleted.
	 * <code>"Device registered with this airline is Deleted"</code>
	 */
	public static final String DEVICE_REGISTERED_WITH_AIRLINE_DELETED="DRD";
	
	/**
	 * To mention that device is already activated.
	 * <code>"Device is already activated"</code>
	 */
	public static final String DEVICE_ALREADY_ACTIVATED="DAA";
	
	/**
	 * To mention the product download process is failed and contact skybuyhigh admin.
	 * <code>"Product download Process is Failed.Contact SkyBuy Admin."</code>
	 */
	public static final String PRODUCT_DOWNLOAD_PROCESS_FAILED="PDF";

	/**
	 * To mention that device is not registered with skybuyhigh.
	 * <code>"Device not Registered with SkyBuyHigh.Contact SkyBuyHigh Admin."</code>
	 */
	public static final String DEVICE_NOT_REGISTERED_CONTACT_ADMIN="DNRC";
	
	/**
	 * To mention that device is allocated.
	 * <code>"Device is allocated"</code>
	 */
	public static final String DEVICE_IS_ALLOCATED="DIA";
	
	/**
	 * Will check the device status before device registration or placing an order.
	 * If an exception is caught while checking the device status, then return this 
	 * status code.
	 * <code>"Checking Device status is failed.Contact SkyBuy Admin."</code>
	 */
	public static final String CHECKING_DEVICE_STATUS_FAILED="CDSF";
	
	/**
	 * To mention that device is note registered or if any exception caught.
	 * <code>"false-Device is not Registered"</code>
	 */
	public static final String FALSE_DEVICE_IS_NOT_REGISTERED="FDNR";
	/*
	 * ---------------------------------------------------------------------------------------------
	 */
}
