package com.sbh.processor;

import static com.sbh.contants.SkyBuyContants.DEVICE_ALREADY_ACTIVATED;
import static com.sbh.contants.SkyBuyContants.DEVICE_ALREADY_REGISTERED;
import static com.sbh.contants.SkyBuyContants.DEVICE_DELETED;
import static com.sbh.contants.SkyBuyContants.DEVICE_INACTIVE;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_ACTIVATED;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_ALLOCATED;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_REGISTERED;
import static com.sbh.contants.SkyBuyContants.DEVICE_REGISTERED_WITH_AIRLINE_DELETED;
import static com.sbh.contants.SkyBuyContants.INACTIVE_AIRLINE;
import static com.sbh.contants.SkyBuyContants.INVALID_AIRLINE_CODE;
import static com.sbh.contants.SkyBuyContants.INVALID_AIRLINE_OR_PRODUCT_KEY;
import static com.sbh.contants.SkyBuyContants.INVALID_DEVICE;
import static com.sbh.contants.SkyBuyContants.INVALID_PRODUCT_KEY;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.SecureActionConfig;
import org.apache.struts.tiles.TilesRequestProcessor;
import org.apache.struts.util.SecureRequestUtils;

import com.sbh.dao.DeviceRegDAO;
import com.sbh.forms.CustomerLoginForm;
import com.sbh.util.Utils;
import com.sbh.vo.LoginAirlineVO;
import com.sbh.vo.LoginVO;

/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserEntitlementReqProcessor extends TilesRequestProcessor {
	private static Logger logger = LogManager.getLogger(UserEntitlementReqProcessor.class);

	protected boolean processPreprocess(HttpServletRequest request,
			HttpServletResponse response) {
		// System.out.println("inside processPreprocess method of userentitlement req processor");
		logger.info("processActionPerform:ENTER");
		
		HttpSession session = request.getSession(true);
		LoginVO oLoginVO = null;
		LoginVO oCustLoginVO = null;
		LoginAirlineVO oLoginAirlineVO = null;
		String sActionPath = "";
		String sPath=null,sURI=null;
		boolean bStatus = false;
		CustomerLoginForm oOldForm = null; 
		CustomerLoginForm oNewForm = null;
		try {			
			sURI=request.getRequestURI();
			sPath=Utils.getUserTypeFromURI(sURI);
			sPath=sPath==null?"":sPath.trim();
			HttpSession oSession = request.getSession(true);
			
			if(sPath.equalsIgnoreCase("vendor"))
				oLoginVO=(LoginVO)session.getAttribute("vendorLoginInfo");
			else if(sPath.equalsIgnoreCase("airline"))
				oLoginVO=(LoginVO)session.getAttribute("airlineLoginInfo");
			else if(sPath.equalsIgnoreCase("admin"))
				oLoginVO=(LoginVO)session.getAttribute("adminLoginInfo");
			else if(sPath.equalsIgnoreCase("customer")) {			
				oCustLoginVO=(LoginVO)session.getAttribute("custLoginInfo");
			}
			if(oLoginVO!=null)
				logger.info("processActionPerform:User Id:"+oLoginVO.getUserId());
			else if(oCustLoginVO!=null){
				logger.info("processActionPerform:Customer Name:"+oCustLoginVO.getContactName());
				logger.info("processActionPerform:Order Confirmation Number:"+oCustLoginVO.getUserId());
			}
			
			
			//oLoginAirlineVO=(LoginAirlineVO)session.getAttribute("loginAirlineInfo");
			sActionPath = processPath(request,response);
			if(!"/admin/spellchecker".equalsIgnoreCase(sActionPath) && !"/vendor/spellchecker".equalsIgnoreCase(sActionPath) && !"/airline/spellchecker".equalsIgnoreCase(sActionPath)){
				 // Look up the corresponding mapping
		        // This will also be checked later by the calling method,
		        // so we'll leave any error message generation to it
		       SecureActionConfig secureActionMapping = null;
		        try {
		        	secureActionMapping = (SecureActionConfig) processMapping(request, response, sActionPath);
		        } catch (IOException ioe) {
		        	logger.info("processActionPerform:Secure Action Mapping Exception:"+ioe.getMessage());
		        }
		        if (secureActionMapping == null) {
		            return false;
		        }
	
		        // Redirect to https/http if necesssary
		        if (SecureRequestUtils.checkSsl(secureActionMapping, getServletContext(), request, response)) {
		            return false;
		        }
			}

	        // Made it through, delegate to the superclass
	        if(super.processPreprocess(request, response)) {
				if (sActionPath.equalsIgnoreCase("/admin/login") || sActionPath.equalsIgnoreCase("/airline/login") || sActionPath.equalsIgnoreCase("/vendor/login")  ||sActionPath.equalsIgnoreCase("/device/deviceStatus")||sActionPath.equalsIgnoreCase("/downloadProduct") || sActionPath.equalsIgnoreCase("/device/deviceReg") ||sActionPath.equalsIgnoreCase("/device/getCatalogueDetails")||sActionPath.equalsIgnoreCase("/device/placeOrder")||sActionPath.equalsIgnoreCase("/customer/customersLogin") || sActionPath.equalsIgnoreCase("/generateCatalogue")){
	
					//Check Prod key 
					/*if(sActionPath.equalsIgnoreCase("/downloadProduct")){
						boolean bDevciceStatus = validateDevice(request,response,"DOWNLOADPRODUCT");				  
						return bDevciceStatus;					
					}else if(sActionPath.equalsIgnoreCase("/device/deviceReg")){
						boolean bDevciceStatus = validateDevice(request,response,"DEVICEREG");	
						return bDevciceStatus;	
					}else*/
					/* */
					if(sActionPath.equalsIgnoreCase("/device/getCatalogueDetails")){
						String sIsRegistered = request.getParameter("IsRegistered");
						if(sIsRegistered!=null && sIsRegistered.equalsIgnoreCase("NO")){
							return true;
						}else{
							boolean bDevciceStatus = authenticateDevice(request,response);	
							return bDevciceStatus;
							
						}
					}else if(sActionPath.equalsIgnoreCase("/customer/customersLogin")){
						oNewForm = (CustomerLoginForm)oSession.getAttribute("customerLoginForm");
						oSession.setAttribute("customerLoginForm", oNewForm);
						return true;
					}else{
						return true;
					}
				}
		       
				else if( sActionPath.equalsIgnoreCase("/customer/preCustomerLogin") || sActionPath.equalsIgnoreCase("/airline/airlineLogin") || sActionPath.equalsIgnoreCase("/vendor/vendorLogin") || sActionPath.equalsIgnoreCase("/admin/adminLogin") || sActionPath.equalsIgnoreCase("/admin/logout")  || sActionPath.equalsIgnoreCase("/airline/logout")  || sActionPath.equalsIgnoreCase("/vendor/logout")  || sActionPath.equalsIgnoreCase("/customer/logout") ){
					if(session!=null)
						session.invalidate();
					return true;
				}else{
					if(oLoginVO != null && !"Y".equalsIgnoreCase(oLoginVO.getIsSuperUser())){
						ArrayList alListOfAction = null;
						alListOfAction = (ArrayList)oSession.getAttribute("EntitlementActions");
						if(alListOfAction.contains(sActionPath))
							return true;
						else{
							
							ActionMapping mapping = processMapping(request, response, "/"+sPath+"/noAuthorityContentPath");
							processForward(request, response, mapping);
						}
					}
				}
	        }
	        
	        
	   /* if (secureActionMapping == null) {
            return false;
        }

        // Redirect to https/http if necesssary
        if (SecureRequestUtils.checkSsl(secureActionMapping, getServletContext(), request, response)) {
            return false;
        }*/

			log.debug("processPreprocess - Action Path = " +sActionPath);
		} catch (Exception e1) {
			e1.printStackTrace();
			log.error("Authentication of request failed. "+ e1.getMessage());
			return false;
		}

		logger.info("processActionPerform:EXIT");

		
		if(!sPath.equalsIgnoreCase("customer")){
			if (oLoginVO == null) {
				try {

					request.setAttribute("sessionExpired","sessExpired");
					if(sPath.equalsIgnoreCase("vendor"))
						getServletContext().getRequestDispatcher("/vendor/login.do?method=vendorLogin").forward(request,response);
					else if(sPath.equalsIgnoreCase("airline"))
						getServletContext().getRequestDispatcher("/airline/login.do?method=airlineLogin").forward(request,response);
					else if(sPath.equalsIgnoreCase("admin"))
						getServletContext().getRequestDispatcher("/admin/login.do?method=adminLogin").forward(request,response);
					logger.info("processActionPerform:EXIT");
				} catch (Exception e1) {
					log.error("Unable to send response in preProcessor");
				}
			} else {
				bStatus = true;
			}
		}
		if(sPath.equalsIgnoreCase("customer")){

			if(oCustLoginVO!=null){
				bStatus = true;
			}else{
				try {
					request.setAttribute("sessionExpired","sessExpired");
					getServletContext().getRequestDispatcher("/customer/preCustomerLogin.do").forward(request,response);
				} catch (Exception e1) {
					log.error("Unable to send response in preProcessor");
				}
			}
		}
		return bStatus;
	}

	public static boolean validateDevice(HttpServletRequest p_request,HttpServletResponse p_response,String p_sProcessName)throws Exception{
		logger.info("validateDevice:ENTER");
		
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			String sProductKey = p_request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sAirId =  p_request.getParameter("airId");
			sAirId = sAirId ==null?"":sAirId.trim();

			String sMacAddress =  p_request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			String sMacDesc =  p_request.getParameter("macDesc");
			sMacDesc = sMacDesc ==null?"":sMacDesc.trim();

			String sProcessorId =  p_request.getParameter("processorId");
			sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

			String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
			if(sProdkeyStatus!=null &&(sProdkeyStatus.equalsIgnoreCase("AL"))){

				boolean bAirCodeStatus=DeviceRegDAO.checkAirlineCode(sAirId,sProductKey);
				if(bAirCodeStatus){
					String sAirlineStatus = DeviceRegDAO.checkAirlineStatus(sAirId,sProductKey);
					sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();
					if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){	

						ArrayList  alAirlineDeviceStatus = DeviceRegDAO.checkAirlineDeviceStatus(sAirId, "0", sProductKey);

						String sDeviceStatus = (String)alAirlineDeviceStatus.get(0);
						sDeviceStatus = sDeviceStatus==null?"":sDeviceStatus.trim();

						String sAirDeviceStatus = (String)alAirlineDeviceStatus.get(1);	
						sAirDeviceStatus = sAirDeviceStatus==null?"":sAirDeviceStatus.trim();

						if(sDeviceStatus.equalsIgnoreCase("AL") && sAirDeviceStatus.equalsIgnoreCase("AL")){
							boolean bAirlineUserId = DeviceRegDAO.checkAirlineProductKey(sAirId, sProductKey);
							if(bAirlineUserId){
								if(p_sProcessName!=null && p_sProcessName.equals("DEVICEREG")){
									boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
									if(bMacAddrStatus)
										bDeviceStatus = true;
									else{
										pw.println(INVALID_DEVICE);
										pw.flush();
									}
								}else{
									bDeviceStatus = true;
								}
							}else{
								pw.println(INVALID_AIRLINE_OR_PRODUCT_KEY);
								pw.flush();
							}	
						}else if(sAirDeviceStatus.trim().length()==0){
							pw.println(INVALID_PRODUCT_KEY);
							pw.flush();
						}
						else if(sAirDeviceStatus.equalsIgnoreCase("A")){
							pw.println(DEVICE_ALREADY_REGISTERED);
							pw.flush();
						}
						else if(sAirDeviceStatus.equalsIgnoreCase("D")){
							pw.println(DEVICE_REGISTERED_WITH_AIRLINE_DELETED);
							pw.flush();
						}
						else if(sDeviceStatus.equalsIgnoreCase("I")){
							pw.println(DEVICE_INACTIVE);
							pw.flush();
						}
					}else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
						pw.println(INVALID_AIRLINE_CODE);
						pw.flush();
					}
					else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
						pw.println(INACTIVE_AIRLINE);
						pw.flush();
					}
				}else{
					pw.println(INVALID_AIRLINE_OR_PRODUCT_KEY);
					pw.flush();
				}
			}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
				pw.println(INVALID_PRODUCT_KEY);
				pw.flush();
			}
			else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
				pw.println(DEVICE_NOT_ALLOCATED);
				pw.flush();
			}
			else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("A")){
				pw.println(DEVICE_ALREADY_ACTIVATED);
				pw.flush();
			}
			else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
				pw.println(DEVICE_INACTIVE);
				pw.flush();
			}
			else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")){
				pw.println(DEVICE_NOT_ACTIVATED);
				pw.flush();
			}
			else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("D")){
				pw.println(DEVICE_DELETED);
				pw.flush();
			}
		}catch (Exception e) {
			e.printStackTrace();
			log.error("validateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("validateDevice:EXIT");
		return bDeviceStatus ;
	}

	public static boolean authenticateDevice(HttpServletRequest p_request,HttpServletResponse p_response)throws Exception{

		logger.info("validateDevice:ENTER");
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			String sProductKey = p_request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sdeviceId =  p_request.getParameter("deviceId");
			sdeviceId = sdeviceId ==null?"":sdeviceId.trim();

			String sMacAddress =  p_request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			String sProcessorId =  p_request.getParameter("processorId");
			sProcessorId = sProcessorId ==null?"":sProcessorId.trim();
			
			String sAirCode =  p_request.getParameter("airCode");
			sAirCode = sAirCode ==null?"":sAirCode.trim();

			boolean bStatus = DeviceRegDAO.validateDevice(sProductKey, sAirCode, sMacAddress, "DC");

			if(bStatus){
				bDeviceStatus = true;
			}else{
				String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
				if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("A"))){

					String sAirlineStatus = DeviceRegDAO.checkAirlineStatus(sAirCode,sProductKey);			
					sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();
					if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){

						boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
						if(!bMacAddrStatus){					
							p_response.setHeader("ErrorMsg",DEVICE_NOT_REGISTERED);										
						}

					}else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
						p_response.setHeader("ErrorMsg",INVALID_AIRLINE_CODE);				
					}
					else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
						p_response.setHeader("ErrorMsg",INACTIVE_AIRLINE);				
					}

				}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
					p_response.setHeader("ErrorMsg",INVALID_PRODUCT_KEY);						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
					p_response.setHeader("ErrorMsg",DEVICE_NOT_ALLOCATED);						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("AL")){
					p_response.setHeader("ErrorMsg",DEVICE_NOT_ACTIVATED);						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
					p_response.setHeader("ErrorMsg",DEVICE_INACTIVE);					
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")) {
					p_response.setHeader("ErrorMsg",DEVICE_NOT_ALLOCATED);						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("D")){
					p_response.setHeader("ErrorMsg",DEVICE_DELETED);
				}
			}


		}catch (Exception e) {
			e.printStackTrace();
			log.error("validateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("validateDevice:EXIT");
		return bDeviceStatus ;
	}



	/*public static boolean validateDevice(HttpServletRequest p_request,HttpServletResponse p_response,String p_sProcessName)throws Exception{

		logger.info("validateDevice:ENTER");
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			String sProductKey = p_request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sAirId =  p_request.getParameter("airId");
			sAirId = sAirId ==null?"":sAirId.trim();

			String sAirUserId =  p_request.getParameter("airUserId");
			sAirUserId = sAirUserId ==null?"":sAirUserId.trim();

			String sAirpassword =  p_request.getParameter("airPass");
			sAirpassword = sAirpassword ==null?"":sAirpassword.trim();

			String sMacAddress =  p_request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			String sMacDesc =  p_request.getParameter("macDesc");
			sMacDesc = sMacDesc ==null?"":sMacDesc.trim();

			String sProcessorId =  p_request.getParameter("processorId");
			sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

			ArrayList  alAirlineStatus = DeviceRegDAO.checkAirlineStatus(sAirId,sAirUserId,sAirpassword);
			String sAirlineStatus = (String)alAirlineStatus.get(0);
			sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();
			int iAirlineUseruIdCount = (Integer)alAirlineStatus.get(1);

			if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){
				if(iAirlineUseruIdCount>0){
					String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
					if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("AL"))){
						ArrayList  alAirlineDeviceStatus = DeviceRegDAO.checkAirlineDeviceStatus(sAirId, "0", sProductKey);

						String sDeviceStatus = (String)alAirlineDeviceStatus.get(0);
						sDeviceStatus = sDeviceStatus==null?"":sDeviceStatus.trim();

						String sAirDeviceStatus = (String)alAirlineDeviceStatus.get(1);	
						sAirDeviceStatus = sAirDeviceStatus==null?"":sAirDeviceStatus.trim();

						if(sDeviceStatus.equalsIgnoreCase("AL") && sAirDeviceStatus.equalsIgnoreCase("AL")){
							boolean bAirlineUserId = DeviceRegDAO.checkAirlineProductKey(sAirId, sProductKey);
							if(bAirlineUserId){
								if(p_sProcessName!=null && p_sProcessName.equals("DEVICEREG")){
									boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
									if(bMacAddrStatus)
										bDeviceStatus = true;
									else{
										pw.println("Invalid Device");
										pw.flush();
									}
								}else{
									bDeviceStatus = true;
								}
							}else{
								pw.println("Invalid Product Key");
								pw.flush();
							}	
						}else if(sAirDeviceStatus.trim().length()==0){
							pw.println("Invalid Product Key");
							pw.flush();
						}
						else if(sAirDeviceStatus.equalsIgnoreCase("A")){
							pw.println("Device is already Registered");
							pw.flush();
						}
						else if(sAirDeviceStatus.equalsIgnoreCase("D")){
							pw.println("Device registered with this airline is Deleted");
							pw.flush();
						}
						else if(sDeviceStatus.equalsIgnoreCase("I")){
							pw.println("Device is Inactive");
							pw.flush();
						}	
					}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
						pw.println("Invalid Product Key");
						pw.flush();
					}
					else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
						pw.println("Device is not allocated");
						pw.flush();
					}
					else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("A")){
						pw.println("Device is already activated");
						pw.flush();
					}
					else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
						pw.println("Device is Inactive");
						pw.flush();
					}
					else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")){
						pw.println("Device is Deleted");
						pw.flush();
					}
				}else{
					pw.println("Invalid AirCode/UserId/Password");
					pw.flush();
				}
			}
			else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
				pw.println("Invalid Airline Code");
				pw.flush();
			}
			else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
				pw.println("Airline is Inactive");
				pw.flush();
			}


		}catch (Exception e) {
			e.printStackTrace();
			log.error("validateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("validateDevice:EXIT");
		return bDeviceStatus ;
	}*/

	/*public static boolean authenticateDevice(HttpServletRequest p_request,HttpServletResponse p_response)throws Exception{

		logger.info("validateDevice:ENTER");
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			String sProductKey = p_request.getParameter("productKey");
			sProductKey = sProductKey==null?"":sProductKey.trim();

			String sdeviceId =  p_request.getParameter("deviceId");
			sdeviceId = sdeviceId ==null?"":sdeviceId.trim();

			String sMacAddress =  p_request.getParameter("macAddr");
			sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

			String sProcessorId =  p_request.getParameter("processorId");
			sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

			String sAirlineStatus = DeviceRegDAO.checkAirlineStatus("0",sProductKey);			
			sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();


			if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){
				String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
				if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("A"))){
					ArrayList  alAirlineDeviceStatus = DeviceRegDAO.checkAirlineDeviceStatus("0", sdeviceId, sProductKey);

					String sDeviceStatus = (String)alAirlineDeviceStatus.get(0);
					sDeviceStatus = sDeviceStatus==null?"":sDeviceStatus.trim();

					String sAirDeviceStatus = (String)alAirlineDeviceStatus.get(1);	
					sAirDeviceStatus = sAirDeviceStatus==null?"":sAirDeviceStatus.trim();

					if(sDeviceStatus.equalsIgnoreCase("A") && sAirDeviceStatus.equalsIgnoreCase("A")){
						boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
						if(bMacAddrStatus)
							bDeviceStatus = true;
						else{
							p_response.setHeader("ErrorMsg","Invalid Device");										
						}
					}else if(sAirDeviceStatus.trim().length()==0){
						p_response.setHeader("ErrorMsg","Invalid Product Key");							
					}
					else if(sAirDeviceStatus.equalsIgnoreCase("AL")){
						p_response.setHeader("ErrorMsg","Device is not activated");							
					}
					else if(sAirDeviceStatus.equalsIgnoreCase("D")){
						p_response.setHeader("ErrorMsg","Device registered with this airline is Deleted");							
					}
					else if(sDeviceStatus.equalsIgnoreCase("I")){
						p_response.setHeader("ErrorMsg","Device is Inactive");							
					}	
				}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
					p_response.setHeader("ErrorMsg","Invalid Product Key");						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
					p_response.setHeader("ErrorMsg","Device is not allocated");						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("AL")){
					p_response.setHeader("ErrorMsg","Device is not activated");						
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
					p_response.setHeader("ErrorMsg","Device is Inactive");					
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")){
					p_response.setHeader("ErrorMsg","Device is Deleted");						
				}

			}
			else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
				p_response.setHeader("ErrorMsg","Invalid Airline");				
			}
			else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
				p_response.setHeader("ErrorMsg","Airline is Inactive");				
			}


		}catch (Exception e) {
			e.printStackTrace();
			log.error("validateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("validateDevice:EXIT");
		return bDeviceStatus ;
	}*/




}