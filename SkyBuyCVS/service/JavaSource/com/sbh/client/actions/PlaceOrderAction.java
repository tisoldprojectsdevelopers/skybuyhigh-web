package com.sbh.client.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.w3c.dom.Document;

import com.sbh.client.dao.PlaceOrderDAO;
import com.sbh.client.util.PlaceOrderUtil;
import com.sbh.client.vo.AirlineAdvtVO;
import com.sbh.client.vo.OrderDetailsVO;
import com.sbh.client.vo.OrderItemsVO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.DeviceRegDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.util.CCUtils;
import com.sbh.util.DBConnection;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.ProductDetailsVO;

public class PlaceOrderAction extends DispatchAction{

	private static Logger logger = LogManager.getLogger(PlaceOrderAction.class);
	
	public ActionForward placeOrder(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		logger.info("PlaceOrderAction::placeOrder:ENTER");    	  
		
//		String sFwrdKey="";     
		OrderDetailsVO oOrderDetailsVO = null;
		AirlineAdvtVO oAirlineAdvtVO = null;
		boolean bDeviceStatus = false;
		response.setContentType("application/xml");
		PrintWriter pw = response.getWriter();
		List<AirlineAdvtVO> alAirlineAdvt = null;
		List<OrderItemsVO> alOrderItemInfo  = null;
		Connection con = DBConnection.getSQL2005Connection();
		String sError = null;
//		ArrayList alAirlineDetails = null;
		OrderItemsVO oOrderItemsVO = null;
		Map<String,String> p_hmTags =new HashMap<String, String>();
		boolean bCustTransId = false;
		HttpSession oSession = request.getSession(true);
		ArrayList alStateList = null;
		ArrayList<CategoryVO> alCategory =null;
		String sCateFolderName = "",sImageUploadPath = "";
		
		ProductDetailsVO oProductDetailsVO = null;	
		String sMedImagePath = null;
		byte []baMedImage = null;


		try{
			logger.info("PlaceOrderAction::placeOrder:ENTER");  
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			alStateList = (ArrayList)oSession.getServletContext().getAttribute("StateList");
			String sAirlineAdvt =  request.getParameter("airlineadvertisement");
			String sOrderDetails =  request.getParameter("orderdetails");
			sImageUploadPath=System.getProperty("ImageUploadPath");
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			String sRemoteHostName = InetAddress.getLocalHost().getHostName();
			
			if(sAirlineAdvt == null && sOrderDetails!=null){
				logger.info("PlaceOrderAction::placeOrder:OrderDetails: "+sOrderDetails);  
				
				Document doc = PlaceOrderUtil.initXML(sOrderDetails);
				oOrderDetailsVO= PlaceOrderUtil.parseOrderInfo(doc);
				if(oOrderDetailsVO!=null){
					oOrderDetailsVO.setProductKey(oOrderDetailsVO.getProductKey()==null?"":oOrderDetailsVO.getProductKey().trim());

					String sProductKey =oOrderDetailsVO.getProductKey();
					sProductKey = sProductKey==null?"":sProductKey.trim();

					String sDeviceId =  oOrderDetailsVO.getDeviceId();
					sDeviceId = sDeviceId ==null?"":sDeviceId.trim();

					String sMacAddress =  oOrderDetailsVO.getMacAddr();
					sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

					String sProcessorId = oOrderDetailsVO.getProcessorId();
					sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

					String sFlightNo = oOrderDetailsVO.getFlightNo();
					sFlightNo = sFlightNo ==null?"":sFlightNo.trim();

					/*alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);
					String sAirId = (String)alAirlineDetails.get(0);
					sAirId = sAirId==null?"":sAirId.trim();
					oOrderDetailsVO.setAirId(sAirId);*/

					bDeviceStatus = authenticateDevice(response,sProductKey,sDeviceId,sMacAddress);

					if(bDeviceStatus){	
						// Check the order is already placed
						if(oOrderDetailsVO.getCustTransId()!=null && oOrderDetailsVO.getCustTransId().trim().length()>0)
							bCustTransId  = PlaceOrderDAO.checkCustTransId(oOrderDetailsVO.getCustTransId());

						if(!bCustTransId){
							String sCCNo=oOrderDetailsVO.getCCNo();
							sCCNo=sCCNo==null?"":sCCNo.trim();
							if(sCCNo!=null){
								if(CCUtils.validCC(sCCNo,oOrderDetailsVO.getCardType())){
									oOrderDetailsVO.setCCNoStatus("S");
								}else{
									oOrderDetailsVO.setCCNoStatus("F");
									oOrderDetailsVO.setCCNoComments("This card is invalid or unsupported.");
								}
							}
							con.setAutoCommit(false);
							alOrderItemInfo = oOrderDetailsVO.getOrderItemDetails();
							if(alOrderItemInfo!= null && alOrderItemInfo.size()>0){
								for(int i=0;i<alOrderItemInfo.size();i++){
									oOrderItemsVO =(OrderItemsVO) alOrderItemInfo.get(i);
									
									oProductDetailsVO = CatalogueDAO.getProductDetails(oOrderItemsVO.getProdId(),"CLIENT");

									alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
									if(oProductDetailsVO != null) {
										if("Vendor".equalsIgnoreCase(oProductDetailsVO.getOwnerType())){
											if(alCategory!=null && alCategory.size()>0){
												for(CategoryVO oCategoryVO: alCategory){
													if(oCategoryVO.getCateId()!=null){
														if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
															sCateFolderName = oCategoryVO.getCateName();
													}					
												}
											}
										}else if("Airline".equalsIgnoreCase(oProductDetailsVO.getOwnerType())){
											sCateFolderName = "PrivateJetJaunts";
										}
									}

									sMedImagePath = sImageUploadPath+"/"+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getMainImgType();
									baMedImage = getByteArrayFromFilePath(sMedImagePath);


									
									sError = PlaceOrderDAO.placeOrderDetails(oOrderDetailsVO,oOrderItemsVO,baMedImage, con,sRemoteHostName,request.getRemoteHost());

									/**** Insert Order audit details ******/
									if(oOrderItemsVO.getOrderItemId()!=null && sError==null){								
										String sMsg=oBundle.getString("customer.order.confirmation.msg");
										sMsg=sMsg==null?"":sMsg.trim();								
										OrderDAO.insertOrderAuditDetails(oOrderItemsVO.getOrderItemId(),oOrderDetailsVO.getOrderId(),oOrderDetailsVO.getCustTransId(),sMsg,oOrderDetailsVO.getAirName(),con,sRemoteHostName,sDeviceId);								
									}
								}	
								con.commit();
								if(sError!=null && sError.trim().length()>0){
									pw.println("Order placement-failed");
									// System.out.println("PlaceOrderAction::placeOrder:Order placement-failed");
								}	
								else{ 
									pw.println("Order placement-Success");
									pw.flush();
									// System.out.println("PlaceOrderAction::placeOrder:Order placement-Success");


									DeviceRegDAO.insertDeviceLogDetails(oOrderDetailsVO.getAirId(),sDeviceId,sFlightNo,"Order Placement", alOrderItemInfo.size(),oOrderDetailsVO.getMacAddr(),"", sRemoteHostName,"CLIENT");

									//Send order placement details to customer and admin by email
									PlaceOrderDAO.sendEmail(oOrderDetailsVO,p_hmTags,null,null,alStateList);

									/**** To Send Offer and Ideas given by the customer ****/
									if((oOrderDetailsVO.getCustOffer()!=null && !oOrderDetailsVO.getCustOffer().equalsIgnoreCase("")) || (oOrderDetailsVO.getCustIdea()!=null && !oOrderDetailsVO.getCustIdea().equalsIgnoreCase("")) ){
										PlaceOrderDAO.sendFeedbackEmail(oOrderDetailsVO,alStateList);
									}
								}
							}
							else{
								pw.println("Order placement-failed");
								// System.out.println("PlaceOrderAction::placeOrder:Order placement-failed");
							}
						}else{
							pw.println("Order already placed-success");
							// System.out.println("PlaceOrderAction::placeOrder:Order already placed-success");
						}

					}

				}
			}else{
				logger.info("PlaceOrderAction::placeOrder:Advertisement: "+sAirlineAdvt);  
				// System.out.println("Advertisement: "+sAirlineAdvt);		
				Document doc = PlaceOrderUtil.initXML(sAirlineAdvt);
				alAirlineAdvt= PlaceOrderUtil.parseAirlineAdvtInfo(doc);
				if(alAirlineAdvt!=null){
					
					oAirlineAdvtVO = (AirlineAdvtVO)alAirlineAdvt.get(0);
					
					oAirlineAdvtVO.setProductKey(oAirlineAdvtVO.getProductKey()==null?"":oAirlineAdvtVO.getProductKey().trim());

					String sProductKey =oAirlineAdvtVO.getProductKey();
					sProductKey = sProductKey==null?"":sProductKey.trim();

					String sDeviceId =  oAirlineAdvtVO.getDeviceId();
					sDeviceId = sDeviceId ==null?"":sDeviceId.trim();

					String sMacAddress =  oAirlineAdvtVO.getMacAddr();
					sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

					String sProcessorId = oAirlineAdvtVO.getProcessorId();
					sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

					String sFlightNo = oAirlineAdvtVO.getFlightNo();
					sFlightNo = sFlightNo ==null?"":sFlightNo.trim();


					/*alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);
					String sAirId = (String)alAirlineDetails.get(0);
					sAirId = sAirId==null?"":sAirId.trim();
					oAirlineAdvtVO.setAirId(sAirId);*/

					bDeviceStatus = authenticateDevice(response,sProductKey,sDeviceId,sMacAddress);

					if(bDeviceStatus){	
							con.setAutoCommit(false);
							if(alAirlineAdvt!= null && alAirlineAdvt.size()>0){
								for(int i=0;i<alAirlineAdvt.size();i++){
									oAirlineAdvtVO =(AirlineAdvtVO) alAirlineAdvt.get(i);
									sError = PlaceOrderDAO.placeAirlineAdvt(oAirlineAdvtVO,con,sRemoteHostName,request.getRemoteHost());
 
									/**** Insert Order audit details ******/
								/*	if(sError==null){								
										String sMsg=oBundle.getString("customer.order.confirmation.msg");
										sMsg=sMsg==null?"":sMsg.trim();								
										OrderDAO.insertOrderAuditDetails(oOrderItemsVO.getOrderItemId(),oOrderDetailsVO.getOrderId(),oOrderDetailsVO.getCustTransId(),sMsg,oOrderDetailsVO.getAirName(),con);								
									}*/
								}	
								con.commit();
								if(alAirlineAdvt!= null && alAirlineAdvt.size()>0){
									for(int i=0;i<alAirlineAdvt.size();i++){
										oAirlineAdvtVO =(AirlineAdvtVO) alAirlineAdvt.get(i);
										/**** To Send mail to the customer and Airline****/
										if(oAirlineAdvtVO.getCustEmail()!=null && !oAirlineAdvtVO.getCustEmail().equalsIgnoreCase("") ){
											PlaceOrderDAO.sendMailToCustomer(oAirlineAdvtVO,sRemoteHostName,"CLIENT");
										}
										PlaceOrderDAO.sendMailToAirline(oAirlineAdvtVO,sRemoteHostName,"CLIENT");
									}
								}
								if(sError!=null && sError.trim().length()>0){
									pw.println("Order placement-failed");
									// System.out.println("PlaceOrderAction::placeOrder:Order placement-failed");
								}	
								else{ 
									pw.println("Order placement-Success");
									pw.flush();
									// System.out.println("PlaceOrderAction::placeOrder:Order placement-Success");

									DeviceRegDAO.insertDeviceLogDetails(oAirlineAdvtVO.getAirId(),sDeviceId,sFlightNo,"Order Placement", alAirlineAdvt.size(),oAirlineAdvtVO.getMacAddr(),"", sRemoteHostName,"CLIENT");

									//Send order placement details to customer and admin by email
									//PlaceOrderDAO.sendEmail(oOrderDetailsVO,p_hmTags,null,null,alStateList);
								}
							}
							else{
								pw.println("Order placement-failed");
								// System.out.println("PlaceOrderAction::placeOrder:Order placement-failed");
							}
						}else{
							pw.println("Order already placed-success");
							// System.out.println("PlaceOrderAction::placeOrder:Order already placed-success");
						}
					}
				}

			// System.out.println("PlaceOrderAction::placeOrder:Exit");  

		}catch(Exception e){
			pw.println("Order placement-failed");
			pw.flush();
			con.rollback();
			ActionErrors oErrors = new ActionErrors(); 
			oErrors.add(Globals.ERROR_KEY,new ActionError("error.db",e.getMessage()));
			saveErrors(request,oErrors);
//			sFwrdKey="failure";
			e.printStackTrace();
			logger.error("PlaceOrderAction::placeOrder:Exception "+e.getMessage());
		}finally{
			con.commit();
			if(con!=null)
				con.close();
			
		}
		logger.info("PlaceOrderAction::placeOrder:EXIT");
		return null;        		
	}

	public static boolean authenticateDevice(HttpServletResponse p_response,String sProductKey,String sDeviceId,String sMacAddress)throws Exception{

		logger.info("authenticateDevice:ENTER");
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			boolean bStatus = DeviceRegDAO.validateDevice(sProductKey, "0", sMacAddress, "DC");

			if(bStatus){
				bDeviceStatus = true;
			}
			else{
				String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
				if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("A"))){

					String sAirlineStatus = DeviceRegDAO.checkAirlineStatus("0",sProductKey);			
					sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();
					if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){
						boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
						if(!bMacAddrStatus){			
							pw.println("Device not Registered with SkyBuyHigh");
							pw.flush();
						}

					}else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
						pw.println("Invalid Airline Code");	
						pw.flush();
					}
					else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
						pw.println("Airline is Inactive");	
						pw.flush();
					}

				}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
					pw.println("Invalid Product Key");		
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
					pw.println("Device is not allocated");	
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("AL")){
					pw.println("Device is not activated");
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
					pw.println("Device is Inactive");	
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA") ||  sProdkeyStatus.equalsIgnoreCase("D")){
					pw.println("Device is Deleted");	
					pw.flush();
				}
			}


		}catch (Exception e) {
			e.printStackTrace();
			log.error("authenticateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("authenticateDevice:EXIT");
		return bDeviceStatus ;
	}


	/*public static boolean authenticateDevice(HttpServletResponse p_response,String sProductKey,String sAirId,String sDeviceId,String sMacAddress)throws Exception{

		logger.info("validateDevice:ENTER");
		boolean bDeviceStatus = false;
		try{
			p_response.setContentType("application/xml");						
			PrintWriter pw = p_response.getWriter();	

			String sAirlineStatus =  DeviceRegDAO.checkAirlineStatus(sAirId);
			sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();

			if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){

				String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
				if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("A"))){
					ArrayList  alAirlineDeviceStatus = DeviceRegDAO.checkAirlineDeviceStatus(sAirId, sDeviceId, sProductKey);

					String sDeviceStatus = (String)alAirlineDeviceStatus.get(0);
					sDeviceStatus = sDeviceStatus==null?"":sDeviceStatus.trim();

					String sAirDeviceStatus = (String)alAirlineDeviceStatus.get(1);	
					sAirDeviceStatus = sAirDeviceStatus==null?"":sAirDeviceStatus.trim();

					if(sDeviceStatus.equalsIgnoreCase("A") && sAirDeviceStatus.equalsIgnoreCase("A")){
						boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
						if(bMacAddrStatus)
							bDeviceStatus = true;
						else{
							pw.println("Invalid Device");
							pw.flush();
						}
					}else if(sAirDeviceStatus.trim().length()==0){
						pw.println("Invalid Product Key");
						pw.flush();
					}
					else if(sAirDeviceStatus.equalsIgnoreCase("AL")){
						pw.println("Device is not activated");
						pw.flush();
					}
					else if(sAirDeviceStatus.equalsIgnoreCase("D")){
						pw.println("Device registered with this airline is Deleted");	
						pw.flush();
					}
					else if(sDeviceStatus.equalsIgnoreCase("I")){
						pw.println("Device is Inactive");
						pw.flush();
					}	
				}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
					pw.println("Invalid Product Key");	
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
					pw.println("Device is not allocated");		
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("AL")){
					pw.println("Device is not activated");	
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
					pw.println("Device is Inactive");
					pw.flush();
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")){
					pw.println("Device is Deleted");
					pw.flush();
				}

			}
			else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
				pw.println("Invalid Airline");	
				pw.flush();
			}
			else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
				pw.println("Airline is Inactive");
				pw.flush();
			}


		}catch (Exception e) {
			e.printStackTrace();
			log.error("validateDevice :  EXCEPTION : "+ e.getMessage());
			bDeviceStatus = false;
		}
		logger.info("validateDevice:EXIT");
		return bDeviceStatus ;
	}*/
	
	public static byte[] getByteArrayFromFilePath(String p_sFilePath) throws IOException {
		byte[] bytes = null;
		File file = new File(p_sFilePath);
		if(file.exists()){
			InputStream is = new FileInputStream(file);

			// Get the size of the file
			long length = file.length();

			// You cannot create an array using a long type.
			// It needs to be an int type.
			// Before converting to an int type, check
			// to ensure that file is not larger than Integer.MAX_VALUE.
			if (length > Integer.MAX_VALUE) {
				// File is too large
			}

			// Create the byte array to hold the data
			bytes = new byte[(int)length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file "+file.getName());
			}

			// Close the input stream and return bytes
			is.close();
		}
		return bytes;
	}

}
