package com.sbh.client.vo;

import java.io.Serializable;

public class OrderItemsVO implements Serializable {
	
	/**
	 * Serial Version User ID.
	 */
	private static final long serialVersionUID = 1L;
	private String VendId;
	private String CateId ;
	private String ProdId ;
	private String ProdCode;
	private String ProdColor;
	private String ProdSize;
	private String Qty ;
	private String VendPrice ; 
	private String SbhPrice ;
	private String OrderItemId;
	private String prodTitle;
	private String travelDate;
	private String ownerType;
	private String orderItemStatus;
	
	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return VendId;
	}
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		VendId = vendId;
	}
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return CateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		CateId = cateId;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return ProdId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		ProdId = prodId;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return ProdCode;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		ProdCode = prodCode;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return Qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		Qty = qty;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return VendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		VendPrice = vendPrice;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return SbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		SbhPrice = sbhPrice;
	}
	/**
	 * @return the orderItemId
	 */
	public String getOrderItemId() {
		return OrderItemId;
	}
	/**
	 * @param orderItemId the orderItemId to set
	 */
	public void setOrderItemId(String orderItemId) {
		OrderItemId = orderItemId;
	}
	/**
	 * @return the prodTitle
	 */
	public String getProdTitle() {
		return prodTitle;
	}
	/**
	 * @param prodTitle the prodTitle to set
	 */
	public void setProdTitle(String prodTitle) {
		this.prodTitle = prodTitle;
	}
	public String getTravelDate() {
		return travelDate;
	}
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the prodColor
	 */
	public String getProdColor() {
		return ProdColor;
	}
	/**
	 * @param prodColor the prodColor to set
	 */
	public void setProdColor(String prodColor) {
		ProdColor = prodColor;
	}
	/**
	 * @return the prodSize
	 */
	public String getProdSize() {
		return ProdSize;
	}
	/**
	 * @param prodSize the prodSize to set
	 */
	public void setProdSize(String prodSize) {
		ProdSize = prodSize;
	}
	/**
	 * @return the orderItemStatus
	 */
	public String getOrderItemStatus() {
		return orderItemStatus;
	}
	/**
	 * @param orderItemStatus the orderItemStatus to set
	 */
	public void setOrderItemStatus(String orderItemStatus) {
		this.orderItemStatus = orderItemStatus;
	}

}
