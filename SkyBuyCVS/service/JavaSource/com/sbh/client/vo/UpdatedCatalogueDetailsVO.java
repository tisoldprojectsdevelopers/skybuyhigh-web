package com.sbh.client.vo;

public class UpdatedCatalogueDetailsVO {
	
	private String catalogueType;
	private String clientCertPath;
	private String clientCertPassword;
	private String networkUsername;
	private String networkPassword;
	private String schemaPath;
	private String schemaVersion;
	private String uploadType;
	private String updateDate;
	private String keyRefId;
	private String serviceInstallationPath;
	private String serviceVersion;
	private String serviceUpdateDate;
	private String virtualDesktopInstallationPath;
	private String virtualDesktopVersion;
	private String virtualDesktopUpdateDate;
	private String iPhoneDesktopUpdateDate;
	private String iPhoneDesktopPath;
	private String iPhonePersonalShopperUpdateDate;
	private String iPhonePersonalShopperPath;
	private String iPhoneWelcomePageUpdateDate;
	private String iPhoneWelcomePagePath;
	private String welcomePageUpdateDate;
	private String welcomePagePath;
	private String eCatalogueUpdateDate;
	private String eCataloguePath;
	private String personalShopperUpdateDate;
	private String personalShopperPath;
	
	/**
	 * @return the iPhoneDesktopUpdateDate
	 */
	public String getIPhoneDesktopUpdateDate() {
		return iPhoneDesktopUpdateDate;
	}
	/**
	 * @param phoneDesktopUpdateDate the iPhoneDesktopUpdateDate to set
	 */
	public void setIPhoneDesktopUpdateDate(String phoneDesktopUpdateDate) {
		iPhoneDesktopUpdateDate = phoneDesktopUpdateDate;
	}
	/**
	 * @return the iPhoneDesktopPath
	 */
	public String getIPhoneDesktopPath() {
		return iPhoneDesktopPath;
	}
	/**
	 * @param phoneDesktopPath the iPhoneDesktopPath to set
	 */
	public void setIPhoneDesktopPath(String phoneDesktopPath) {
		iPhoneDesktopPath = phoneDesktopPath;
	}
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
	
	/**
	 * @return the schemaPath
	 */
	public String getSchemaPath() {
		return schemaPath;
	}
	/**
	 * @param schemaPath the schemaPath to set
	 */
	public void setSchemaPath(String schemaPath) {
		this.schemaPath = schemaPath;
	}
	/**
	 * @return the schemaVersion
	 */
	public String getSchemaVersion() {
		return schemaVersion;
	}
	/**
	 * @param schemaVersion the schemaVersion to set
	 */
	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}
	/**
	 * @return the catalogueType
	 */
	public String getCatalogueType() {
		return catalogueType;
	}
	/**
	 * @param catalogueType the catalogueType to set
	 */
	public void setCatalogueType(String catalogueType) {
		this.catalogueType = catalogueType;
	}
	
	/**
	 * @return the clientCertPath
	 */
	public String getClientCertPath() {
		return clientCertPath;
	}
	/**
	 * @param clientCertPath the clientCertPath to set
	 */
	public void setClientCertPath(String clientCertPath) {
		this.clientCertPath = clientCertPath;
	}
	/**
	 * @return the clientCertPassword
	 */
	public String getClientCertPassword() {
		return clientCertPassword;
	}
	/**
	 * @param clientCertPassword the clientCertPassword to set
	 */
	public void setClientCertPassword(String clientCertPassword) {
		this.clientCertPassword = clientCertPassword;
	}
	/**
	 * @return the networkUsername
	 */
	public String getNetworkUsername() {
		return networkUsername;
	}
	/**
	 * @param networkUsername the networkUsername to set
	 */
	public void setNetworkUsername(String networkUsername) {
		this.networkUsername = networkUsername;
	}
	/**
	 * @return the networkPassword
	 */
	public String getNetworkPassword() {
		return networkPassword;
	}
	/**
	 * @param networkPassword the networkPassword to set
	 */
	public void setNetworkPassword(String networkPassword) {
		this.networkPassword = networkPassword;
	}
	/**
	 * @return the uploadType
	 */
	public String getUploadType() {
		return uploadType;
	}
	/**
	 * @param uploadType the uploadType to set
	 */
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	/**
	 * @return the updateDate
	 */
	public String getUpdateDate() {
		return updateDate;
	}
	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	/**
	 * @return the serviceInstallationPath
	 */
	public String getServiceInstallationPath() {
		return serviceInstallationPath;
	}
	/**
	 * @param serviceInstallationPath the serviceInstallationPath to set
	 */
	public void setServiceInstallationPath(String serviceInstallationPath) {
		this.serviceInstallationPath = serviceInstallationPath;
	}
	/**
	 * @return the serviceVersion
	 */
	public String getServiceVersion() {
		return serviceVersion;
	}
	/**
	 * @param serviceVersion the serviceVersion to set
	 */
	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}
	/**
	 * @return the virtualDesktopInstallationPath
	 */
	public String getVirtualDesktopInstallationPath() {
		return virtualDesktopInstallationPath;
	}
	/**
	 * @param virtualDesktopInstallationPath the virtualDesktopInstallationPath to set
	 */
	public void setVirtualDesktopInstallationPath(
			String virtualDesktopInstallationPath) {
		this.virtualDesktopInstallationPath = virtualDesktopInstallationPath;
	}
	/**
	 * @return the virtualDesktopVersion
	 */
	public String getVirtualDesktopVersion() {
		return virtualDesktopVersion;
	}
	/**
	 * @param virtualDesktopVersion the virtualDesktopVersion to set
	 */
	public void setVirtualDesktopVersion(String virtualDesktopVersion) {
		this.virtualDesktopVersion = virtualDesktopVersion;
	}
	/**
	 * @return the serviceUpdateDate
	 */
	public String getServiceUpdateDate() {
		return serviceUpdateDate;
	}
	/**
	 * @param serviceUpdateDate the serviceUpdateDate to set
	 */
	public void setServiceUpdateDate(String serviceUpdateDate) {
		this.serviceUpdateDate = serviceUpdateDate;
	}
	/**
	 * @return the virtualDesktopUpdateDate
	 */
	public String getVirtualDesktopUpdateDate() {
		return virtualDesktopUpdateDate;
	}
	/**
	 * @param virtualDesktopUpdateDate the virtualDesktopUpdateDate to set
	 */
	public void setVirtualDesktopUpdateDate(String virtualDesktopUpdateDate) {
		this.virtualDesktopUpdateDate = virtualDesktopUpdateDate;
	}
	public String getIPhonePersonalShopperUpdateDate() {
		return iPhonePersonalShopperUpdateDate;
	}
	public void setIPhonePersonalShopperUpdateDate(
			String phonePersonalShopperUpdateDate) {
		iPhonePersonalShopperUpdateDate = phonePersonalShopperUpdateDate;
	}
	public String getIPhonePersonalShopperPath() {
		return iPhonePersonalShopperPath;
	}
	public void setIPhonePersonalShopperPath(String phonePersonalShopperPath) {
		iPhonePersonalShopperPath = phonePersonalShopperPath;
	}
	public String getIPhoneWelcomePageUpdateDate() {
		return iPhoneWelcomePageUpdateDate;
	}
	public void setIPhoneWelcomePageUpdateDate(String phoneWelcomePageUpdateDate) {
		iPhoneWelcomePageUpdateDate = phoneWelcomePageUpdateDate;
	}
	public String getIPhoneWelcomePagePath() {
		return iPhoneWelcomePagePath;
	}
	public void setIPhoneWelcomePagePath(String phoneWelcomePagePath) {
		iPhoneWelcomePagePath = phoneWelcomePagePath;
	}
	public String getWelcomePageUpdateDate() {
		return welcomePageUpdateDate;
	}
	public void setWelcomePageUpdateDate(String welcomePageUpdateDate) {
		this.welcomePageUpdateDate = welcomePageUpdateDate;
	}
	public String getWelcomePagePath() {
		return welcomePagePath;
	}
	public void setWelcomePagePath(String welcomePagePath) {
		this.welcomePagePath = welcomePagePath;
	}
	public String getECatalogueUpdateDate() {
		return eCatalogueUpdateDate;
	}
	public void setECatalogueUpdateDate(String catalogueUpdateDate) {
		eCatalogueUpdateDate = catalogueUpdateDate;
	}
	public String getECataloguePath() {
		return eCataloguePath;
	}
	public void setECataloguePath(String cataloguePath) {
		eCataloguePath = cataloguePath;
	}
	public String getPersonalShopperUpdateDate() {
		return personalShopperUpdateDate;
	}
	public void setPersonalShopperUpdateDate(String personalShopperUpdateDate) {
		this.personalShopperUpdateDate = personalShopperUpdateDate;
	}
	public String getPersonalShopperPath() {
		return personalShopperPath;
	}
	public void setPersonalShopperPath(String personalShopperPath) {
		this.personalShopperPath = personalShopperPath;
	}
	
	
}
