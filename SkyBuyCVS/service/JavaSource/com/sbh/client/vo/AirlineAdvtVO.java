package com.sbh.client.vo;

import java.io.Serializable;

public class AirlineAdvtVO implements Serializable{
	
	private String processorId;
	private String macAddr ;
	private String productKey;
	private String deviceId;
	private String airId;
	private String orderDt;
	private String prodCode;
	private String prodId;
	private String ownerId;
	private String catgId;
	private String custName;
	private String custPhone;
	private String custEmail;
	private String flightNo;
	private String advtId;
	private String ownerType;
	private String airName;
	private String originatedFrom;
	/**
	 * @return the ownerType
	 */
	public String getOwnerType() {
		return ownerType;
	}
	/**
	 * @param ownerType the ownerType to set
	 */
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the advtId
	 */
	public String getAdvtId() {
		return advtId;
	}
	/**
	 * @param advtId the advtId to set
	 */
	public void setAdvtId(String advtId) {
		this.advtId = advtId;
	}
	/**
	 * @return the processorId
	 */
	public String getProcessorId() {
		return processorId;
	}
	/**
	 * @param processorId the processorId to set
	 */
	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}
	/**
	 * @return the macAddr
	 */
	public String getMacAddr() {
		return macAddr;
	}
	/**
	 * @param macAddr the macAddr to set
	 */
	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}
	/**
	 * @return the productKey
	 */
	public String getProductKey() {
		return productKey;
	}
	/**
	 * @param productKey the productKey to set
	 */
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return prodCode;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return prodId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	/**
	 * @return the ownerId
	 */
	public String getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the catgId
	 */
	public String getCatgId() {
		return catgId;
	}
	/**
	 * @param catgId the catgId to set
	 */
	public void setCatgId(String catgId) {
		this.catgId = catgId;
	}
	/**
	 * @return the custName
	 */
	public String getCustName() {
		return custName;
	}
	/**
	 * @param custName the custName to set
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	/**
	 * @return the custPhone
	 */
	public String getCustPhone() {
		return custPhone;
	}
	/**
	 * @param custPhone the custPhone to set
	 */
	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}
	/**
	 * @return the custEmail
	 */
	public String getCustEmail() {
		return custEmail;
	}
	/**
	 * @param custEmail the custEmail to set
	 */
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the orderDt
	 */
	public String getOrderDt() {
		return orderDt;
	}
	/**
	 * @param orderDt the orderDt to set
	 */
	public void setOrderDt(String orderDt) {
		this.orderDt = orderDt;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the originatedFrom
	 */
	public String getOriginatedFrom() {
		return originatedFrom;
	}
	/**
	 * @param originatedFrom the originatedFrom to set
	 */
	public void setOriginatedFrom(String originatedFrom) {
		this.originatedFrom = originatedFrom;
	}

	
	
}
