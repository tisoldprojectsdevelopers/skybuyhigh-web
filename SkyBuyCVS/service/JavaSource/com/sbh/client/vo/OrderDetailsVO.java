package com.sbh.client.vo;

import java.io.Serializable;
import java.util.List;

public class OrderDetailsVO implements Serializable {
	
	/**
	 * Serial Version User Id.
	 */
	private static final long serialVersionUID = 1L;
	private String CustTransId;
	private String CustBillEmail;
	private String CustBillFname;
	private String CustBillLname;
	private String CustBillPhone;
	private String CustBillAddr1;
	private String CustBillAddr2;
	private String CustBillCity ;
	private String CustBillState;
	private String CustBillCountry ;
	private String CustBillZip ;
	private String CustShipFname;
	private String CustShipLname ;
	private String CustShipAddr1 ;
	private String CustShipAddr2 ;
	private String CustShipPhone ;
	private String CustShipCity; 
	private String CustShipState; 
	private String CustShipCountry;
	private String CustShipZip;
	private String CustShipEmail;
	private String OrderDt;
	private String VendId;
	private String CateId ;
	private String ProdId ;
	private String ProdCode ;
	private String Qty ;
	private String VendPrice ; 
	private String SbhPrice ;
	private String ProcessorId;
	private String MacAddr ;
	private String CCNo ;
	private String CCNoStatus ;
	private String CCNoComments ;
	private String CardFname;
	private String CardType;
	private String ExpDt;
	private String DevicePassword;
	private String orderId;
	private String custId;
	private String airName;
	private String flightNo;
	private String ownerEmail;
	private String productKey;
	private String airId;
	private String deviceId;
	private String custOffer;
	private String custIdea;
	private String cvv;
	private String CCNoLFD;  // Credit Card No.. last four digits.
	private String decryptCCNo;
	private String decryptCVVNo;
	private String SpecialInstruction;
	private String PersonalInformation;
	private String decryptedCardType;
	private String totalOrderItems;
	private String totalPayment;
	private List<OrderItemsVO> OrderItemDetails;
	private String keyRefId;
	private String newServicesFeedback;
	private String suggestion;
	private String originatedFrom;
	
	/**
	 * @return the keyRefId
	 */
	public String getKeyRefId() {
		return keyRefId;
	}
	/**
	 * @param keyRefId the keyRefId to set
	 */
	public void setKeyRefId(String keyRefId) {
		this.keyRefId = keyRefId;
	}
	/**
	 * @return the custTransId
	 */
	public String getCustTransId() {
		return CustTransId;
	}
	/**
	 * @param custTransId the custTransId to set
	 */
	public void setCustTransId(String custTransId) {
		CustTransId = custTransId;
	}
	/**
	 * @return the custBillEmail
	 */
	public String getCustBillEmail() {
		return CustBillEmail;
	}
	/**
	 * @param custBillEmail the custBillEmail to set
	 */
	public void setCustBillEmail(String custBillEmail) {
		CustBillEmail = custBillEmail;
	}
	/**
	 * @return the custBillFname
	 */
	public String getCustBillFname() {
		return CustBillFname;
	}
	/**
	 * @param custBillFname the custBillFname to set
	 */
	public void setCustBillFname(String custBillFname) {
		CustBillFname = custBillFname;
	}
	/**
	 * @return the custBillLname
	 */
	public String getCustBillLname() {
		return CustBillLname;
	}
	/**
	 * @param custBillLname the custBillLname to set
	 */
	public void setCustBillLname(String custBillLname) {
		CustBillLname = custBillLname;
	}
	
	/**
	 * @return the custBillPhone
	 */
	public String getCustBillPhone() {
		return CustBillPhone;
	}
	/**
	 * @param custBillPhone the custBillPhone to set
	 */
	public void setCustBillPhone(String custBillPhone) {
		CustBillPhone = custBillPhone;
	}
	/**
	 * @return the custShipPhone
	 */
	public String getCustShipPhone() {
		return CustShipPhone;
	}
	/**
	 * @param custShipPhone the custShipPhone to set
	 */
	public void setCustShipPhone(String custShipPhone) {
		CustShipPhone = custShipPhone;
	}
	/**
	 * @return the specialInstruction
	 */
	public String getSpecialInstruction() {
		return SpecialInstruction;
	}
	/**
	 * @param specialInstruction the specialInstruction to set
	 */
	public void setSpecialInstruction(String specialInstruction) {
		SpecialInstruction = specialInstruction;
	}
	/**
	 * @return the personalInformation
	 */
	public String getPersonalInformation() {
		return PersonalInformation;
	}
	/**
	 * @param personalInformation the personalInformation to set
	 */
	public void setPersonalInformation(String personalInformation) {
		PersonalInformation = personalInformation;
	}
	/**
	 * @return the custBillAddr1
	 */
	public String getCustBillAddr1() {
		return CustBillAddr1;
	}
	/**
	 * @param custBillAddr1 the custBillAddr1 to set
	 */
	public void setCustBillAddr1(String custBillAddr1) {
		CustBillAddr1 = custBillAddr1;
	}
	/**
	 * @return the custBillAddr2
	 */
	public String getCustBillAddr2() {
		return CustBillAddr2;
	}
	/**
	 * @param custBillAddr2 the custBillAddr2 to set
	 */
	public void setCustBillAddr2(String custBillAddr2) {
		CustBillAddr2 = custBillAddr2;
	}
	/**
	 * @return the custBillCity
	 */
	public String getCustBillCity() {
		return CustBillCity;
	}
	/**
	 * @param custBillCity the custBillCity to set
	 */
	public void setCustBillCity(String custBillCity) {
		CustBillCity = custBillCity;
	}
	/**
	 * @return the custBillState
	 */
	public String getCustBillState() {
		return CustBillState;
	}
	/**
	 * @param custBillState the custBillState to set
	 */
	public void setCustBillState(String custBillState) {
		CustBillState = custBillState;
	}
	/**
	 * @return the custBillCountry
	 */
	public String getCustBillCountry() {
		return CustBillCountry;
	}
	/**
	 * @param custBillCountry the custBillCountry to set
	 */
	public void setCustBillCountry(String custBillCountry) {
		CustBillCountry = custBillCountry;
	}
	/**
	 * @return the custBillZip
	 */
	public String getCustBillZip() {
		return CustBillZip;
	}
	/**
	 * @param custBillZip the custBillZip to set
	 */
	public void setCustBillZip(String custBillZip) {
		CustBillZip = custBillZip;
	}
	/**
	 * @return the custShipFname
	 */
	public String getCustShipFname() {
		return CustShipFname;
	}
	/**
	 * @param custShipFname the custShipFname to set
	 */
	public void setCustShipFname(String custShipFname) {
		CustShipFname = custShipFname;
	}
	/**
	 * @return the custShipLname
	 */
	public String getCustShipLname() {
		return CustShipLname;
	}
	/**
	 * @param custShipLname the custShipLname to set
	 */
	public void setCustShipLname(String custShipLname) {
		CustShipLname = custShipLname;
	}
	/**
	 * @return the custShipAddr1
	 */
	public String getCustShipAddr1() {
		return CustShipAddr1;
	}
	/**
	 * @param custShipAddr1 the custShipAddr1 to set
	 */
	public void setCustShipAddr1(String custShipAddr1) {
		CustShipAddr1 = custShipAddr1;
	}
	/**
	 * @return the custShipAddr2
	 */
	public String getCustShipAddr2() {
		return CustShipAddr2;
	}
	/**
	 * @param custShipAddr2 the custShipAddr2 to set
	 */
	public void setCustShipAddr2(String custShipAddr2) {
		CustShipAddr2 = custShipAddr2;
	}
	/**
	 * @return the custShipCity
	 */
	public String getCustShipCity() {
		return CustShipCity;
	}
	/**
	 * @param custShipCity the custShipCity to set
	 */
	public void setCustShipCity(String custShipCity) {
		CustShipCity = custShipCity;
	}
	/**
	 * @return the custShipState
	 */
	public String getCustShipState() {
		return CustShipState;
	}
	/**
	 * @param custShipState the custShipState to set
	 */
	public void setCustShipState(String custShipState) {
		CustShipState = custShipState;
	}
	/**
	 * @return the custShipCountry
	 */
	public String getCustShipCountry() {
		return CustShipCountry;
	}
	/**
	 * @param custShipCountry the custShipCountry to set
	 */
	public void setCustShipCountry(String custShipCountry) {
		CustShipCountry = custShipCountry;
	}
	/**
	 * @return the custShipZip
	 */
	public String getCustShipZip() {
		return CustShipZip;
	}
	/**
	 * @param custShipZip the custShipZip to set
	 */
	public void setCustShipZip(String custShipZip) {
		CustShipZip = custShipZip;
	}
	/**
	 * @return the custShipEmail
	 */
	public String getCustShipEmail() {
		return CustShipEmail;
	}
	/**
	 * @param custShipEmail the custShipEmail to set
	 */
	public void setCustShipEmail(String custShipEmail) {
		CustShipEmail = custShipEmail;
	}
	/**
	 * @return the orderDt
	 */
	public String getOrderDt() {
		return OrderDt;
	}
	/**
	 * @param orderDt the orderDt to set
	 */
	public void setOrderDt(String orderDt) {
		OrderDt = orderDt;
	}
	/**
	 * @return the vendId
	 */
	public String getVendId() {
		return VendId;
	}
	/**
	 * @param vendId the vendId to set
	 */
	public void setVendId(String vendId) {
		VendId = vendId;
	}
	/**
	 * @return the cateId
	 */
	public String getCateId() {
		return CateId;
	}
	/**
	 * @param cateId the cateId to set
	 */
	public void setCateId(String cateId) {
		CateId = cateId;
	}
	/**
	 * @return the prodId
	 */
	public String getProdId() {
		return ProdId;
	}
	/**
	 * @param prodId the prodId to set
	 */
	public void setProdId(String prodId) {
		ProdId = prodId;
	}
	/**
	 * @return the prodCode
	 */
	public String getProdCode() {
		return ProdCode;
	}
	/**
	 * @param prodCode the prodCode to set
	 */
	public void setProdCode(String prodCode) {
		ProdCode = prodCode;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return Qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		Qty = qty;
	}
	/**
	 * @return the vendPrice
	 */
	public String getVendPrice() {
		return VendPrice;
	}
	/**
	 * @param vendPrice the vendPrice to set
	 */
	public void setVendPrice(String vendPrice) {
		VendPrice = vendPrice;
	}
	/**
	 * @return the sbhPrice
	 */
	public String getSbhPrice() {
		return SbhPrice;
	}
	/**
	 * @param sbhPrice the sbhPrice to set
	 */
	public void setSbhPrice(String sbhPrice) {
		SbhPrice = sbhPrice;
	}
	/**
	 * @return the processorId
	 */
	public String getProcessorId() {
		return ProcessorId;
	}
	/**
	 * @param processorId the processorId to set
	 */
	public void setProcessorId(String processorId) {
		ProcessorId = processorId;
	}
	/**
	 * @return the macAddr
	 */
	public String getMacAddr() {
		return MacAddr;
	}
	/**
	 * @param macAddr the macAddr to set
	 */
	public void setMacAddr(String macAddr) {
		MacAddr = macAddr;
	}
	/**
	 * @return the cCNo
	 */
	public String getCCNo() {
		return CCNo;
	}
	/**
	 * @param no the cCNo to set
	 */
	public void setCCNo(String no) {
		CCNo = no;
	}
	/**
	 * @return the cardFname
	 */
	public String getCardFname() {
		return CardFname;
	}
	/**
	 * @param cardFname the cardFname to set
	 */
	public void setCardFname(String cardFname) {
		CardFname = cardFname;
	}
	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return CardType;
	}
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		CardType = cardType;
	}
	/**
	 * @return the expDt
	 */
	public String getExpDt() {
		return ExpDt;
	}
	/**
	 * @param expDt the expDt to set
	 */
	public void setExpDt(String expDt) {
		ExpDt = expDt;
	}
	/**
	 * @return the devicePassword
	 */
	public String getDevicePassword() {
		return DevicePassword;
	}
	/**
	 * @param devicePassword the devicePassword to set
	 */
	public void setDevicePassword(String devicePassword) {
		DevicePassword = devicePassword;
	}
	
	/**
	 * @return the cCNoStatus
	 */
	public String getCCNoStatus() {
		return CCNoStatus;
	}
	/**
	 * @param noStatus the cCNoStatus to set
	 */
	public void setCCNoStatus(String noStatus) {
		CCNoStatus = noStatus;
	}
	/**
	 * @return the cCNoComments
	 */
	public String getCCNoComments() {
		return CCNoComments;
	}
	/**
	 * @param noComments the cCNoComments to set
	 */
	public void setCCNoComments(String noComments) {
		CCNoComments = noComments;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the custId
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	/**
	 * @return the airName
	 */
	public String getAirName() {
		return airName;
	}
	/**
	 * @param airName the airName to set
	 */
	public void setAirName(String airName) {
		this.airName = airName;
	}
	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}
	/**
	 * @param flightNo the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	/**
	 * @return the ownerEmail
	 */
	public String getOwnerEmail() {
		return ownerEmail;
	}
	/**
	 * @param ownerEmail the ownerEmail to set
	 */
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	public String getProductKey() {
		return productKey;
	}
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	/**
	 * @return the airId
	 */
	public String getAirId() {
		return airId;
	}
	/**
	 * @param airId the airId to set
	 */
	public void setAirId(String airId) {
		this.airId = airId;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the custOffer
	 */
	public String getCustOffer() {
		return custOffer;
	}
	/**
	 * @param custOffer the custOffer to set
	 */
	public void setCustOffer(String custOffer) {
		this.custOffer = custOffer;
	}
	/**
	 * @return the custIdea
	 */
	public String getCustIdea() {
		return custIdea;
	}
	/**
	 * @param custIdea the custIdea to set
	 */
	public void setCustIdea(String custIdea) {
		this.custIdea = custIdea;
	}
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	
	/**
	 * @return the decryptCCNo
	 */
	public String getDecryptCCNo() {
		return decryptCCNo;
	}
	/**
	 * @param decryptCCNo the decryptCCNo to set
	 */
	public void setDecryptCCNo(String decryptCCNo) {
		this.decryptCCNo = decryptCCNo;
	}
	/**
	 * @return the decryptCVVNo
	 */
	public String getDecryptCVVNo() {
		return decryptCVVNo;
	}
	/**
	 * @param decryptCVVNo the decryptCVVNo to set
	 */
	public void setDecryptCVVNo(String decryptCVVNo) {
		this.decryptCVVNo = decryptCVVNo;
	}
	/**
	 * @return the cCNoLFD
	 */
	public String getCCNoLFD() {
		return CCNoLFD;
	}
	/**
	 * @param noLFD the cCNoLFD to set
	 */
	public void setCCNoLFD(String noLFD) {
		CCNoLFD = noLFD;
	}
	/**
	 * @return the decryptedCardType
	 */
	public String getDecryptedCardType() {
		return decryptedCardType;
	}
	/**
	 * @param decryptedCardType the decryptedCardType to set
	 */
	public void setDecryptedCardType(String decryptedCardType) {
		this.decryptedCardType = decryptedCardType;
	}
	/**
	 * @return the orderItemDetails
	 */
	public List<OrderItemsVO> getOrderItemDetails() {
		return OrderItemDetails;
	}
	/**
	 * @param orderItemDetails the orderItemDetails to set
	 */
	public void setOrderItemDetails(List<OrderItemsVO> orderItemDetails) {
		OrderItemDetails = orderItemDetails;
	}
	
	/**
	 * @return the totalPayment
	 */
	public String getTotalPayment() {
		return totalPayment;
	}
	/**
	 * @param totalPayment the totalPayment to set
	 */
	public void setTotalPayment(String totalPayment) {
		this.totalPayment = totalPayment;
	}
	/**
	 * @return the totalOrderItems
	 */
	public String getTotalOrderItems() {
		return totalOrderItems;
	}
	/**
	 * @param totalOrderItems the totalOrderItems to set
	 */
	public void setTotalOrderItems(String totalOrderItems) {
		this.totalOrderItems = totalOrderItems;
	}
	
	
	public String getNewServicesFeedback() {
		return newServicesFeedback;
	}
	public void setNewServicesFeedback(String newServicesFeedback) {
		this.newServicesFeedback = newServicesFeedback;
	}
	public String getSuggestion() {
		return suggestion;
	}
	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}
	/**
	 * @return the originatedFrom
	 */
	public String getOriginatedFrom() {
		return originatedFrom;
	}
	/**
	 * @param originatedFrom the originatedFrom to set
	 */
	public void setOriginatedFrom(String originatedFrom) {
		this.originatedFrom = originatedFrom;
	}
	
	
}
