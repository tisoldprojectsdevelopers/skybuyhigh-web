package com.sbh.client.dao;

import static com.sbh.contants.SkyBuyContants.AIRLINE_DETAILS;
import static com.sbh.contants.SkyBuyContants.CREDITCARD_FAILED;
import static com.sbh.contants.SkyBuyContants.CREDITCARD_NOT_PROCESSED;
import static com.sbh.contants.SkyBuyContants.DEVICE_DELETED;
import static com.sbh.contants.SkyBuyContants.DEVICE_EXCEPTION;
import static com.sbh.contants.SkyBuyContants.DEVICE_INACTIVE;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_ACTIVATED;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_ALLOCATED;
import static com.sbh.contants.SkyBuyContants.DEVICE_NOT_REGISTERED;
import static com.sbh.contants.SkyBuyContants.ERROR_PARSING_XML;
import static com.sbh.contants.SkyBuyContants.INACTIVE_AIRLINE;
import static com.sbh.contants.SkyBuyContants.INVALID_AIRLINE_CODE;
import static com.sbh.contants.SkyBuyContants.INVALID_CREDITCARD;
import static com.sbh.contants.SkyBuyContants.INVALID_PRODUCT_KEY;
import static com.sbh.contants.SkyBuyContants.INVALID_TOTAL_ORDER_VALUE;
import static com.sbh.contants.SkyBuyContants.IPHONE;
import static com.sbh.contants.SkyBuyContants.ITOUCH;
import static com.sbh.contants.SkyBuyContants.ORDER_ALREADY_PLACED;
import static com.sbh.contants.SkyBuyContants.ORDER_FAILED;
import static com.sbh.contants.SkyBuyContants.ORDER_FAILED_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_PENDING_STATUS;
import static com.sbh.contants.SkyBuyContants.ORDER_PLACED;
import static com.sbh.contants.SkyBuyContants.PRODUCT_DETAILS;
import static com.sbh.contants.SkyBuyContants.THREE_TIME_FAILED_ORDER;
import static com.sbh.dao.CatalogueDAO.getDeviceType;
import static com.sbh.dao.OrderDAO.decryptInputData;
import static com.sbh.util.Utils.mergeAddressLines;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.sbh.client.util.PlaceOrderUtil;
import com.sbh.client.vo.AirlineAdvtVO;
import com.sbh.client.vo.OrderDetailsVO;
import com.sbh.client.vo.OrderItemsVO;
import com.sbh.client.vo.UpdatedCatalogueDetailsVO;
import com.sbh.contants.SkyBuyContants;
import com.sbh.dao.AirlineDAO;
import com.sbh.dao.CatalogueDAO;
import com.sbh.dao.DeviceRegDAO;
import com.sbh.dao.EmailDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.dao.UploadSkyBuyCatalogueDAO;
import com.sbh.dao.VendorDAO;
import com.sbh.email.Email;
import com.sbh.util.CCUtils;
import com.sbh.util.DBConnection;
import com.sbh.util.GenerateCatalogueZip;
import com.sbh.util.Utils;
import com.sbh.vo.CategoryVO;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.ProductDetailsVO;
import com.sbh.vo.StateVO;

public class PlaceOrderDAO {

	private static Logger logger = LogManager.getLogger(VendorDAO.class);

	
	public static String placeOrderDetails(OrderDetailsVO p_oOrderDetailsVO,OrderItemsVO p_oOrderItemsVO,byte[] baProdImage,Connection p_con,String p_sLoginId, String p_sIpAddress) throws Exception{	
		logger.info("PlaceOrderDAO::placeOrderDetails::ENTER");		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_place_order(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		logger.debug("PlaceOrderDAO::placeOrderDetails::SQL QUERY "+sQry);
		logger.debug("PlaceOrderDAO::placeOrderDetails::Order Confirmation no "+p_oOrderDetailsVO.getCustTransId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Email "+p_oOrderDetailsVO.getCustBillEmail());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill First Name "+p_oOrderDetailsVO.getCustBillFname());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Last Name "+p_oOrderDetailsVO.getCustBillLname());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Phone "+p_oOrderDetailsVO.getCustBillPhone());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Address 1 "+p_oOrderDetailsVO.getCustBillAddr1());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Address 2 "+p_oOrderDetailsVO.getCustBillAddr2());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill City "+p_oOrderDetailsVO.getCustBillCity());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill State "+p_oOrderDetailsVO.getCustBillState());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Country "+p_oOrderDetailsVO.getCustBillCountry());			
		logger.debug("PlaceOrderDAO::placeOrderDetails::Bill Zip "+p_oOrderDetailsVO.getCustBillZip());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship First Name "+p_oOrderDetailsVO.getCustShipFname());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Last Name "+p_oOrderDetailsVO.getCustShipLname());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Phone "+p_oOrderDetailsVO.getCustShipPhone());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Address 1 "+p_oOrderDetailsVO.getCustShipAddr1());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Address 2 "+p_oOrderDetailsVO.getCustShipAddr2());			
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship City "+p_oOrderDetailsVO.getCustShipCity());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship State "+p_oOrderDetailsVO.getCustShipState());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Country "+p_oOrderDetailsVO.getCustShipCountry());			
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Zip "+p_oOrderDetailsVO.getCustShipZip());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Ship Email "+p_oOrderDetailsVO.getCustShipEmail());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Order Date "+p_oOrderDetailsVO.getOrderDt());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Vendor Id "+p_oOrderItemsVO.getVendId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Category Id "+p_oOrderItemsVO.getCateId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Product Id "+p_oOrderItemsVO.getProdId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Product Code "+p_oOrderItemsVO.getProdCode());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Product Color "+p_oOrderItemsVO.getProdColor());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Product Size "+p_oOrderItemsVO.getProdSize());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Quantity "+p_oOrderItemsVO.getQty());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Vendor Price "+p_oOrderItemsVO.getVendPrice());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Sbh Price "+p_oOrderItemsVO.getSbhPrice());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Processor Id "+p_oOrderDetailsVO.getProcessorId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Mac Address "+p_oOrderDetailsVO.getMacAddr());
		/*logger.debug("PlaceOrderDAO::placeOrderDetails::Device Password "+p_oOrderDetailsVO.getDevicePassword());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Credit Card No "+p_oOrderDetailsVO.getCCNo());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Card First Name "+p_oOrderDetailsVO.getCardFname());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Card Type "+p_oOrderDetailsVO.getCardType());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Expiry Date "+p_oOrderDetailsVO.getExpDt());	*/
		
		logger.debug("PlaceOrderDAO::placeOrderDetails::Air Charter Id "+p_oOrderDetailsVO.getAirId());	
		logger.debug("PlaceOrderDAO::placeOrderDetails::Device Id "+p_oOrderDetailsVO.getDeviceId());	
		logger.debug("PlaceOrderDAO::placeOrderDetails::Flight No "+p_oOrderDetailsVO.getFlightNo());	
		logger.debug("PlaceOrderDAO::placeOrderDetails::Travel Date "+p_oOrderItemsVO.getTravelDate());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Cust Offer "+p_oOrderDetailsVO.getCustOffer());	
		logger.debug("PlaceOrderDAO::placeOrderDetails::Cust Idea "+p_oOrderDetailsVO.getCustIdea());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Special Instruction "+p_oOrderDetailsVO.getSpecialInstruction());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Personal Information "+p_oOrderDetailsVO.getPersonalInformation());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Check Sum Key Ref Id "+p_oOrderDetailsVO.getKeyRefId());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Credit Card Status "+p_oOrderDetailsVO.getCCNoStatus());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Credit Card Status "+p_oOrderDetailsVO.getCCNoComments());
		logger.debug("PlaceOrderDAO::placeOrderDetails::Order Item Status "+p_oOrderItemsVO.getOrderItemStatus());
		logger.debug("PlaceOrderDAO::placeOrderDetails::IP Address "+p_sIpAddress);
		/*logger.debug("PlaceOrderDAO::placeOrderDetails::CVV "+p_oOrderDetailsVO.getCvv());*/
		logger.debug("PlaceOrderDAO::placeOrderDetails::Originated From"+p_oOrderDetailsVO.getOriginatedFrom());
		
		String  sError = null;
		try{			
			
			cstmt=p_con.prepareCall(sQry); 

			cstmt.setString(1,p_oOrderDetailsVO.getCustTransId());
			cstmt.setString(2,p_oOrderDetailsVO.getCustBillEmail());
			cstmt.setString(3,p_oOrderDetailsVO.getCustBillFname());
			cstmt.setString(4,p_oOrderDetailsVO.getCustBillLname());
			cstmt.setString(5,p_oOrderDetailsVO.getCustBillPhone());
			cstmt.setString(6,p_oOrderDetailsVO.getCustBillAddr1());
			cstmt.setString(7,p_oOrderDetailsVO.getCustBillAddr2());
			cstmt.setString(8,p_oOrderDetailsVO.getCustBillCity());
			cstmt.setString(9,p_oOrderDetailsVO.getCustBillState());
			cstmt.setString(10,p_oOrderDetailsVO.getCustBillCountry());			
			cstmt.setString(11,p_oOrderDetailsVO.getCustBillZip());
			cstmt.setString(12,p_oOrderDetailsVO.getCustShipFname());
			cstmt.setString(13,p_oOrderDetailsVO.getCustShipLname());
			cstmt.setString(14,p_oOrderDetailsVO.getCustShipPhone());
			cstmt.setString(15,p_oOrderDetailsVO.getCustShipAddr1());
			cstmt.setString(16,p_oOrderDetailsVO.getCustShipAddr2());			
			cstmt.setString(17,p_oOrderDetailsVO.getCustShipCity());
			cstmt.setString(18,p_oOrderDetailsVO.getCustShipState());
			cstmt.setString(19,p_oOrderDetailsVO.getCustShipCountry());			
			cstmt.setString(20,p_oOrderDetailsVO.getCustShipZip());
			cstmt.setString(21,p_oOrderDetailsVO.getCustShipEmail());
			cstmt.setString(22,p_oOrderDetailsVO.getOrderDt());
			cstmt.setString(23,p_oOrderItemsVO.getVendId());
			cstmt.setString(24,p_oOrderItemsVO.getCateId());
			cstmt.setString(25,p_oOrderItemsVO.getProdId());
			cstmt.setString(26,p_oOrderItemsVO.getProdCode());
			cstmt.setString(27,p_oOrderItemsVO.getProdColor());
			cstmt.setString(28,p_oOrderItemsVO.getProdSize());
			cstmt.setString(29,p_oOrderItemsVO.getQty());
			cstmt.setString(30,p_oOrderItemsVO.getVendPrice());
			cstmt.setString(31,p_oOrderItemsVO.getSbhPrice());
			cstmt.setString(32,p_oOrderDetailsVO.getProcessorId());
			cstmt.setString(33,p_oOrderDetailsVO.getMacAddr());
			cstmt.setString(34,p_oOrderDetailsVO.getDevicePassword());
			cstmt.setString(35,p_oOrderDetailsVO.getCCNo());
			cstmt.setString(36,p_oOrderDetailsVO.getCardFname());
			cstmt.setString(37,p_oOrderDetailsVO.getCardType());
			cstmt.setString(38,p_oOrderDetailsVO.getExpDt());	
			cstmt.setString(39,p_oOrderDetailsVO.getCCNoStatus());	
			cstmt.setString(40,p_oOrderDetailsVO.getCCNoComments());	
			cstmt.setString(41,p_oOrderDetailsVO.getAirId());	
			cstmt.setString(42,p_oOrderDetailsVO.getDeviceId());	
			cstmt.setString(43,p_oOrderDetailsVO.getFlightNo());	
			cstmt.setString(44,p_oOrderItemsVO.getTravelDate());
			cstmt.setString(45,p_oOrderDetailsVO.getCustOffer());	
			cstmt.setString(46,p_oOrderDetailsVO.getCustIdea());
			cstmt.setString(47,p_oOrderDetailsVO.getSpecialInstruction());
			cstmt.setString(48,p_oOrderDetailsVO.getPersonalInformation());
			cstmt.setString(49,p_oOrderDetailsVO.getCvv());
			cstmt.setBytes(50,baProdImage);

			cstmt.registerOutParameter(51, Types.VARCHAR);
			cstmt.registerOutParameter(52, Types.VARCHAR);
			cstmt.registerOutParameter(53, Types.VARCHAR);
			cstmt.registerOutParameter(54, Types.VARCHAR);			
			cstmt.registerOutParameter(55, Types.VARCHAR);
			cstmt.registerOutParameter(56, Types.VARCHAR);
			cstmt.registerOutParameter(57, Types.VARCHAR);
			cstmt.setString(58, p_sLoginId);
			cstmt.setString(59, p_oOrderDetailsVO.getCCNoLFD());
			cstmt.setString(60, p_oOrderDetailsVO.getKeyRefId());
			cstmt.setString(61, p_oOrderItemsVO.getOrderItemStatus());
			cstmt.setString(62, p_sIpAddress);
			cstmt.setString(63, p_oOrderDetailsVO.getOriginatedFrom());
			cstmt.execute();
			sError = cstmt.getString(51);
			p_oOrderDetailsVO.setCustId(cstmt.getString(52));
			p_oOrderDetailsVO.setOrderId(cstmt.getString(53));
			p_oOrderDetailsVO.setAirName(cstmt.getString(54));
			p_oOrderItemsVO.setOrderItemId(cstmt.getString(55));
			p_oOrderItemsVO.setProdTitle(cstmt.getString(56));
			p_oOrderItemsVO.setOwnerType(cstmt.getString(57));
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PlaceOrderDAO::placeOrderDetails::Exception "+e.getMessage());
			sError = "Problem in Order Placement";
			throw e;
			
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null , null);
			
		}
		
		logger.info("PlaceOrderDAO::placeOrderDetails::EXIT");
		return sError;
	}
	
	
	/*public static String getUpdatedCatalogueByDate(String p_sPreviousVersion,String p_sCatalogueFolderPath,String p_sCatalogueFileName,String p_sDeviceId,String p_sTypeOfDownload) 
	throws Exception {
		logger.info("PlaceOrderDAO::getUpdatedCatalogueByDate::ENTER");		
		
		boolean bIsNewVersionFound=false;
		String sAdminFolderPath;
		String sAdminFolderName;
		String sShoppingCartFolderPath;
		String sShoppingCartFolderName;
		String sAdminFolder;
		String sShoppingCartFolder;
		String sDownloadCateDate = null;
		String sQry="{call usp_sbh_get_catalogue_by_date(?,?,?,?,?,?,?,?,?)}";
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		GenerateCatalogueZip oGenerateCatalogueZip =  new GenerateCatalogueZip(p_sCatalogueFolderPath,p_sCatalogueFileName);
		
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::SQL QUERY "+sQry);	
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::Previous Version"+p_sPreviousVersion);	
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::Catalogue Folder Path"+p_sCatalogueFolderPath);
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::Catalogue File Name"+p_sCatalogueFileName);
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::Deivce Id"+p_sDeviceId);
		logger.debug("PlaceOrderDAO::getUpdatedCatalogueByDate::Type Of Download"+p_sTypeOfDownload);
		
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);						
			cstmt.setString(1,p_sPreviousVersion);	
			cstmt.registerOutParameter(2, Types.VARCHAR);
			cstmt.execute();
			rs = cstmt.getResultSet();
			while(rs.next()) {	
				bIsNewVersionFound=true;
				
				sAdminFolder = rs.getString("admin_folder_path");	
				sAdminFolder =sAdminFolder ==null?"":sAdminFolder.trim();
				
				sAdminFolderName = rs.getString("admin_folder_name");	
				sAdminFolderName =sAdminFolderName ==null?"":sAdminFolderName.trim();
				
				sShoppingCartFolder = rs.getString("shopping_cart_folder_path");	
				sShoppingCartFolder =sShoppingCartFolder ==null?"":sShoppingCartFolder.trim();
				
				sShoppingCartFolderName = rs.getString("shopping_cart_folder_name");	
				sShoppingCartFolderName =sShoppingCartFolderName ==null?"":sShoppingCartFolderName.trim();
				
				if("ALL".equalsIgnoreCase(p_sTypeOfDownload)) {
					//SkyBuyHigh Admin folder path 
					sAdminFolderPath =p_sCatalogueFolderPath+"/"+sAdminFolder+"/";
					oGenerateCatalogueZip.addFile(sAdminFolderPath, sAdminFolderName, sAdminFolderName);
					
					//SkyBuyHigh Shopping cart folder path
					sShoppingCartFolderPath =p_sCatalogueFolderPath+"/"+sShoppingCartFolder+"/";
					oGenerateCatalogueZip.addFile(sShoppingCartFolderPath, sShoppingCartFolderName, sShoppingCartFolderName);
					
				}else if("Admin".equalsIgnoreCase(p_sTypeOfDownload)) {
					//SkyBuyHigh Admin folder path 
					sAdminFolderPath =p_sCatalogueFolderPath+"/"+sAdminFolder+"/";
					oGenerateCatalogueZip.addFile(sAdminFolderPath, sAdminFolderName, sAdminFolderName);
					
				}else if("ShoppingCart".equalsIgnoreCase(p_sTypeOfDownload)) {
					//SkyBuyHigh Shopping cart folder path
					sShoppingCartFolderPath =p_sCatalogueFolderPath+"/"+sShoppingCartFolder+"/";
					oGenerateCatalogueZip.addFile(sShoppingCartFolderPath, sShoppingCartFolderName, sShoppingCartFolderName);
				}
			}
			
			if(bIsNewVersionFound) {
				oGenerateCatalogueZip.finish();
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PlaceOrderDAO::getUpdatedCatalogueByDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			if(con!=null)
				con.close();
			if(cstmt!=null)
				cstmt.close();
			if(rs!=null)
				rs.close();

		}
		
		logger.info("PlaceOrderDAO::getUpdatedCatalogueByDate::EXIT");
		return sDownloadCateDate;
		
	}*/
	public static Map<String,String> getCatalogueDetailsByDate(String p_sLastDownloadedCateDt,String p_sCatalogueFolderPath,String p_sCatalogueFileName,String p_sAirId,String p_sDeviceId,String p_sCoverflowProdIds,String p_sIsAirlinePackageDownload,String p_sCatalogueXMLFile,String p_sZipFolderPath,String p_sIsRegistered) throws Exception{	
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::ENTER");		
		
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		String sCoverImageXML = null;
		String sAboutPartnerXML = null;
		String sDownloadCateDate = null;
		String sQry="{call usp_sbh_get_catalogue_by_date(?,?,?,?,?,?,?,?,?)}";
		GenerateCatalogueZip oGenerateCatalogueZip =  null;
		String sSwfFilePath = null,sMainSrcPath =null,sMainSrcFileName =null,sMainDestFolderAndFileName= null;
		String sSwfSrcFileName = null,sView1SrcPath = null,sView1SrcFileName= null,sView1DestFolderAndFileName=null;
		String sView2SrcPath = null,sView2SrcFileName= null,sView2DestFolderAndFileName=null;
		String sView3SrcPath = null,sView3SrcFileName= null,sView3DestFolderAndFileName=null;
		String sAirLogoPath = null,sAirLogoFileName =null,sAirLogoIntroPath = null,sAirLogoIntroFileName = null;
		String sAirLogoDestFolderAndFileName = null,sAirLogoIntroDestFolderAndFileName = null;
		String sNetworkUserName = null;
		String sNetworkPassword = null;
		String sAccountInfo = null;
		String sClientCertPath = null;
		String sClientCertPass = null;
		String sDeviceType = null;
		Map<String,String> hmReturn = new HashMap<String, String>();
		boolean bIsNewVersionFound=false;
		boolean bIsFileUpdated = false;
		String sE_CatalogueFileName = System.getProperty("e-CatalogueFileName").trim();
		int iUpdatedProdCount = 0,iUpdatedAirLogoCount =0, iUpdatedReturnPolicy = 0;
		int iPartnerPriorityUpdated = 0;
		/*sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner_"+p_sDeviceId+".xml";
		sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover_"+p_sDeviceId+".xml";
		sE_CatalogueFileName = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_"+p_sDeviceId+".xml";*/
		sAboutPartnerXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_partner.xml";
		sCoverImageXML = sE_CatalogueFileName==null?"":sE_CatalogueFileName+"_cover.xml";
		sE_CatalogueFileName = sE_CatalogueFileName==null?"":sE_CatalogueFileName+".xml";
		
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::SQL QUERY "+sQry);	
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Last Downloaded Date:"+p_sLastDownloadedCateDt);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Catalogue Folder Path:"+p_sCatalogueFolderPath);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Cataogue File Name:"+p_sCatalogueFileName);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Air Id:"+p_sAirId);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Device Id:"+p_sDeviceId);
		logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Cover Flow Prod Id:"+p_sCoverflowProdIds);
		
		try{
			if(p_sIsRegistered !=null && p_sIsRegistered.equalsIgnoreCase("NO")){
				sDeviceType = IPHONE;
			}else{
				sDeviceType = getDeviceType(p_sDeviceId);
			}
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);						
			cstmt.setString(1,p_sLastDownloadedCateDt);	
			cstmt.setString(2,p_sAirId);
			cstmt.setString(3,p_sCoverflowProdIds);
			cstmt.setString(4, p_sIsAirlinePackageDownload);
			
			cstmt.registerOutParameter(5, Types.VARCHAR);	
			cstmt.registerOutParameter(6, Types.INTEGER);	
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.registerOutParameter(8, Types.INTEGER);
			cstmt.registerOutParameter(9, Types.INTEGER);
			
			cstmt.execute();
			rs = cstmt.getResultSet();
			oGenerateCatalogueZip =  new GenerateCatalogueZip(p_sZipFolderPath,p_sCatalogueFileName);
			while(rs.next()){	
				bIsNewVersionFound=true;
				// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
				String sProdId = rs.getString("prod_id");
				String sCateName = rs.getString("cate_name");
				sCateName = sCateName == null?"":sCateName.trim();
				String sOwnerId = rs.getString("owner_id");	
				String sOwnerType = rs.getString("owner_type");

				String sMainImgPath = rs.getString("main_img_path");
				sMainImgPath =sMainImgPath ==null?"":sMainImgPath.trim();
				String sMainImgType = rs.getString("main_img_type");
				sMainImgType =sMainImgType ==null?"":sMainImgType.trim();

				String sView1ImgPath = rs.getString("view1_img_path");
				sView1ImgPath =sView1ImgPath ==null?"":sView1ImgPath.trim();
				String sView1ImgType = rs.getString("view1_img_type");
				sView1ImgType =sView1ImgType ==null?"":sView1ImgType.trim();

				String sView2ImgPath = rs.getString("view2_img_path");
				sView2ImgPath =sView2ImgPath ==null?"":sView2ImgPath.trim();
				String sView2ImgType = rs.getString("view2_img_type");
				sView2ImgType =sView2ImgType ==null?"":sView2ImgType.trim();

				String sView3ImgPath = rs.getString("view3_img_path");
				sView3ImgPath =sView3ImgPath ==null?"":sView3ImgPath.trim();
				String sView3ImgType = rs.getString("view3_img_type");	
				sView3ImgType =sView3ImgType ==null?"":sView3ImgType.trim();

				String sSwfType = rs.getString("swf_type");	
				sSwfType =sSwfType ==null?"":sSwfType.trim();
				
				String sSwfPath = rs.getString("swf_path");	
				sSwfPath =sSwfPath ==null?"":sSwfPath.trim();
			
				String sIsAdvt = rs.getString("is_advt");	
				sIsAdvt =sIsAdvt ==null?"":sIsAdvt.trim();
				//To SWF file
				
				if("Y".equalsIgnoreCase(sIsAdvt) && sSwfPath.trim().length()>0 && sSwfType.trim().length()>0){
					//Swf folder
					sSwfFilePath =p_sCatalogueFolderPath+"/"+sSwfPath+"/";
					sSwfSrcFileName = sOwnerId+sProdId+"."+sSwfType;
					sMainDestFolderAndFileName = sSwfPath+"/"+sOwnerId+sProdId+"."+sSwfType;
					oGenerateCatalogueZip.addFile(sSwfFilePath, sSwfSrcFileName, sMainDestFolderAndFileName);
					
				}
				
				//To Add Special Product
				String sIsSpecialProd = rs.getString("is_special_prod");	
				sIsSpecialProd =sIsSpecialProd ==null?"":sIsSpecialProd.trim();
				
				String sProdSwfType = rs.getString("prod_swf_type");	
				sProdSwfType =sProdSwfType ==null?"":sProdSwfType.trim();
				
				String sProdSwfPath = rs.getString("prod_swf_path");	
				sProdSwfPath =sProdSwfPath ==null?"":sProdSwfPath.trim();
				
				if("Y".equalsIgnoreCase(sIsSpecialProd) && sProdSwfPath.trim().length()>0 && sProdSwfType.trim().length()>0){
					sSwfFilePath =p_sCatalogueFolderPath+"/"+sProdSwfPath+"/";
					sSwfSrcFileName = sOwnerId+sProdId+"."+sProdSwfType;
					sMainDestFolderAndFileName = sProdSwfPath+"/"+sOwnerId+sProdId+"."+sProdSwfType;
					oGenerateCatalogueZip.addFile(sSwfFilePath, sSwfSrcFileName, sMainDestFolderAndFileName);
					
				}
				
				//Main Img path
				if(sMainImgPath.trim().length()>0 && sMainImgType.trim().length()>0){
					//Image folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Images/";
					sMainSrcFileName = sOwnerId+sProdId+"image."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					// System.out.println("sMainSrcPath "+sMainSrcPath);
					// System.out.println("sMainSrcFileName "+sMainSrcFileName);
					// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
					
					//Large folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Large/";
					sMainSrcFileName = sOwnerId+sProdId+"large."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Med/";
						sMainSrcFileName = sOwnerId+sProdId+"med."+sMainImgType;
						sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					}
					//Thumb folder
					sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/"+sCateName+"/Thumb/";
					sMainSrcFileName = sOwnerId+sProdId+"thumb."+sMainImgType;
					sMainDestFolderAndFileName = sMainImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//CoverFlow Image
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sMainImgPath+"/CoverFlow/";
						sMainSrcFileName = sOwnerId+sProdId+"."+sMainImgType;
						sMainDestFolderAndFileName = sMainImgPath+"/CoverFlow/"+sOwnerId+sProdId+"."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
						
						//Original Img 
						sMainSrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sMainSrcFileName = sOwnerId+sProdId+"main."+sMainImgType;
						sMainDestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"main."+sMainImgType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
					}
				}
				//View1 Img path
				if(sView1ImgPath.trim().length()>0 && sView1ImgType.trim().length()>0){
					//Image folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Images/";
					sView1SrcFileName = sOwnerId+sProdId+"image."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
				
					//Large folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Large/";
					sView1SrcFileName = sOwnerId+sProdId+"large."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Med folder
						sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Med/";
						sView1SrcFileName = sOwnerId+sProdId+"med."+sView1ImgType;
						sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView1ImgType;
						oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					}
					//Thumb folder
					sView1SrcPath =p_sCatalogueFolderPath+"/"+sView1ImgPath+"/"+sCateName+"/Thumb/";
					sView1SrcFileName = sOwnerId+sProdId+"thumb."+sView1ImgType;
					sView1DestFolderAndFileName = sView1ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView1ImgType;
					oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView1SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView1SrcFileName = sOwnerId+sProdId+"view1."+sView1ImgType;
						sView1DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"view1."+sView1ImgType;
						oGenerateCatalogueZip.addFile(sView1SrcPath, sView1SrcFileName, sView1DestFolderAndFileName);
					}
				}
				//View2 Img path
				if(sView2ImgPath.trim().length()>0 && sView2ImgType.trim().length()>0){
					//Image folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Images/";
					sView2SrcFileName = sOwnerId+sProdId+"image."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					//Large folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Large/";
					sView2SrcFileName = sOwnerId+sProdId+"large."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Med/";
						sView2SrcFileName = sOwnerId+sProdId+"med."+sView2ImgType;
						sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView2ImgType;
						oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					}
					
					//Thumb folder
					sView2SrcPath =p_sCatalogueFolderPath+"/"+sView2ImgPath+"/"+sCateName+"/Thumb/";
					sView2SrcFileName = sOwnerId+sProdId+"thumb."+sView2ImgType;
					sView2DestFolderAndFileName = sView2ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView2ImgType;
					oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView2SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView2SrcFileName = sOwnerId+sProdId+"view2."+sView2ImgType;
						sView2DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/"+sOwnerId+sProdId+"view2."+sView2ImgType;
						oGenerateCatalogueZip.addFile(sView2SrcPath, sView2SrcFileName, sView2DestFolderAndFileName);
					}
				}
				//View3 Img path
				if(sView3ImgPath.trim().length()>0 && sView3ImgType.trim().length()>0){
					//Image folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Images/";
					sView3SrcFileName = sOwnerId+sProdId+"image."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Images/"+sOwnerId+sProdId+"image."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					
					//Large folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Large/";
					sView3SrcFileName = sOwnerId+sProdId+"large."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Large/"+sOwnerId+sProdId+"large."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Medium folder
						sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Med/";
						sView3SrcFileName = sOwnerId+sProdId+"med."+sView3ImgType;
						sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Med/"+sOwnerId+sProdId+"med."+sView3ImgType;
						oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					}
					
					//Thumb folder
					sView3SrcPath =p_sCatalogueFolderPath+"/"+sView3ImgPath+"/"+sCateName+"/Thumb/";
					sView3SrcFileName = sOwnerId+sProdId+"thumb."+sView3ImgType;
					sView3DestFolderAndFileName = sView3ImgPath+"/"+sCateName+"/Thumb/"+sOwnerId+sProdId+"thumb."+sView3ImgType;
					oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);		
					if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
						//Original Img 
						sView3SrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/OriginalImg/";
						sView3SrcFileName = sOwnerId+sProdId+"view3."+sView3ImgType;
						sView3DestFolderAndFileName = "SkyBuyPics/"+sOwnerType+"_"+sOwnerId+"/"+"OriginalImg/"+sOwnerId+sProdId+"view3."+sView3ImgType;
						oGenerateCatalogueZip.addFile(sView3SrcPath, sView3SrcFileName, sView3DestFolderAndFileName);
					}
				}
				// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
			}
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				while(rs.next()){	
					bIsNewVersionFound=true;
					// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
					String sProdId = rs.getString("prod_id");
//					String sCateName = rs.getString("cate_name");
					String sOwnerId = rs.getString("owner_id");	
//					String sOwnerType = rs.getString("owner_type");

					String sMainImgPath = rs.getString("main_img_path");
					sMainImgPath =sMainImgPath ==null?"":sMainImgPath.trim();
					String sMainImgType = rs.getString("main_img_type");
					sMainImgType =sMainImgType ==null?"":sMainImgType.trim();

					String sView1ImgPath = rs.getString("view1_img_path");
					sView1ImgPath =sView1ImgPath ==null?"":sView1ImgPath.trim();
					String sView1ImgType = rs.getString("view1_img_type");
					sView1ImgType =sView1ImgType ==null?"":sView1ImgType.trim();

					String sView2ImgPath = rs.getString("view2_img_path");
					sView2ImgPath =sView2ImgPath ==null?"":sView2ImgPath.trim();
					String sView2ImgType = rs.getString("view2_img_type");
					sView2ImgType =sView2ImgType ==null?"":sView2ImgType.trim();

					String sView3ImgPath = rs.getString("view3_img_path");
					sView3ImgPath =sView3ImgPath ==null?"":sView3ImgPath.trim();
					String sView3ImgType = rs.getString("view3_img_type");	
					sView3ImgType =sView3ImgType ==null?"":sView3ImgType.trim();

					String sSwfType = rs.getString("swf_type");	
					sSwfType =sSwfType ==null?"":sSwfType.trim();

					String sSwfPath = rs.getString("swf_path");	
					sSwfPath =sSwfPath ==null?"":sSwfPath.trim();

					String sIsAdvt = rs.getString("is_advt");	
					sIsAdvt =sIsAdvt ==null?"":sIsAdvt.trim();
					//To SWF file
					sMainSrcPath =p_sCatalogueFolderPath+"/SkyBuyPics/CoverImages/";
					sMainSrcFileName = sOwnerId+sProdId+"main."+sMainImgType;
					sMainDestFolderAndFileName = "SkyBuyPics/CoverImages/"+sOwnerId+sProdId+"main."+sMainImgType;
					oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);

					// System.out.println("sMainSrcPath "+sMainSrcPath);
					// System.out.println("sMainSrcFileName "+sMainSrcFileName);
					// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
					
					// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);

				}
			}
			// To get Vendor and Airline About Me files
			if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
					while(rs.next()){	
						bIsNewVersionFound=true;
						// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
						
	//					String sOwnerName = rs.getString("owner_name");
						String sOwnerId = rs.getString("owner_id");	
	//					String sOwnerType = rs.getString("owner_type");
						
						String sAboutMePath = rs.getString("about_me_path");
						sAboutMePath = sAboutMePath == null?"":sAboutMePath.trim();
						String sAboutMeType = rs.getString("about_me_type");
						sAboutMeType = sAboutMeType == null?"":sAboutMeType.trim();
											
						//To About Me SWF file
						sMainSrcPath =p_sCatalogueFolderPath+"/"+sAboutMePath;
						sMainSrcFileName = sOwnerId+"."+sAboutMeType;
						sMainDestFolderAndFileName = sAboutMePath+"/"+sOwnerId+"."+sAboutMeType;
						oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
	
						// System.out.println("sMainSrcPath "+sMainSrcPath);
						// System.out.println("sMainSrcFileName "+sMainSrcFileName);
						// System.out.println("sMainDestFolderAndFileName "+sMainDestFolderAndFileName);
						
						// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
	
					}
				}
			}
			
			/*if(cstmt.getMoreResults()){
				rs = cstmt.getResultSet();
				int iIndex;
				if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
					while(rs.next()){	
						
						// System.out.println("Result Set :Enter::DeviceId "+p_sDeviceId);
						
						String sURL = rs.getString("URL");
						if(sURL!=null && !"".equalsIgnoreCase(sURL)){
							
							sURL = sURL == null?"":sURL.trim();
							sMainSrcPath =p_sCatalogueFolderPath+"/"+sURL;
							File fFile = new File(sMainSrcPath);
							if(fFile.exists()){
								bIsNewVersionFound=true;
								sMainSrcFileName = fFile.getName();
								sMainDestFolderAndFileName = sMainSrcFileName;
								iIndex = sMainSrcPath.lastIndexOf("/");
								sMainSrcPath = sMainSrcPath.substring(0, iIndex);
								oGenerateCatalogueZip.addFile(sMainSrcPath, sMainSrcFileName, sMainDestFolderAndFileName);
							
							}
						}
											
						// System.out.println("Result Set :Exit::DeviceId "+p_sDeviceId);
					}
				}
			}*/
			sDownloadCateDate = cstmt.getString(5);
			iUpdatedProdCount = cstmt.getInt(6);
			iUpdatedAirLogoCount = cstmt.getInt(7);
			iUpdatedReturnPolicy = cstmt.getInt(8);
			iPartnerPriorityUpdated = cstmt.getInt(9);
			if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
				// Airline Logo Path
				if((p_sLastDownloadedCateDt.trim().length()>0 && p_sLastDownloadedCateDt.equals("FullCatalogue")) || (iUpdatedAirLogoCount>0)){						
					bIsNewVersionFound = true;
					sAirLogoPath =p_sCatalogueFolderPath+"/SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/";
					sAirLogoFileName = p_sAirId+"_logo.jpg";
					sAirLogoDestFolderAndFileName = "SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/"+sAirLogoFileName;
					oGenerateCatalogueZip.addFile(sAirLogoPath, sAirLogoFileName, sAirLogoDestFolderAndFileName);
					
					sAirLogoIntroPath =p_sCatalogueFolderPath+"/SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/";
					sAirLogoIntroFileName = p_sAirId+"_logo_intro.jpg";
					sAirLogoIntroDestFolderAndFileName = "SkyBuyPics/Airline_"+p_sAirId+"/AirlineLogo/Logo/"+sAirLogoIntroFileName;
					oGenerateCatalogueZip.addFile(sAirLogoIntroPath, sAirLogoIntroFileName, sAirLogoIntroDestFolderAndFileName);
				}
			}

			//if there is any product is deleted attach the xml file 
			if(iUpdatedProdCount>0 || iUpdatedReturnPolicy > 0 || iPartnerPriorityUpdated > 0)
				bIsNewVersionFound = true;
			
			ArrayList<UpdatedCatalogueDetailsVO> alCertAndNWPass = UploadSkyBuyCatalogueDAO.getInstallationPackageByDate(p_sLastDownloadedCateDt);
			
//			File fFolder = new File(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId);
			
		/*	if(!fFolder .exists())
				fFolder .mkdirs();*/
			// 	To get Account Information and Certificate Information
			for(UpdatedCatalogueDetailsVO oUpdatedCatalogueDetailsVO:alCertAndNWPass){
				
				if("USERNAME".equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
					sNetworkUserName = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkUsername(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					sNetworkPassword = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getNetworkPassword(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					
					sAccountInfo = sNetworkUserName+"|"+sNetworkPassword;
					hmReturn.put("AccountInfo", sAccountInfo);
				}
				if("CLIENTCERT".equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
					sClientCertPath = oUpdatedCatalogueDetailsVO.getClientCertPath();
					sClientCertPass = OrderDAO.decryptInputData(oUpdatedCatalogueDetailsVO.getClientCertPassword(),"SERVER",oUpdatedCatalogueDetailsVO.getKeyRefId());
					sClientCertPath = p_sCatalogueFolderPath +"/"+ sClientCertPath;
					File fSourceFile = new File(sClientCertPath);
					/*File fDestPath = new File(fFolder,fSourceFile.getName());
					
					Utils.copyfile(fSourceFile,fDestPath);*/
//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass/"+fSourceFile.getName());
					hmReturn.put("ClientCertPass", sClientCertPass);
					bIsFileUpdated = true;
				}
				if(!(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType))) {
					if(SkyBuyContants.UPLOADTYPE_WELCOMEPAGE.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sFilePath = oUpdatedCatalogueDetailsVO.getWelcomePagePath();
						sFilePath = p_sCatalogueFolderPath +"/"+ sFilePath;
						File fSourceFile = new File(sFilePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
	//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sFilePath, "", fSourceFile.getName());
						hmReturn.put("WelcomePage", sFilePath);
						bIsFileUpdated = true;
					}
					if(SkyBuyContants.UPLOADTYPE_SKYBUYCATALOGUE.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sFilePath = oUpdatedCatalogueDetailsVO.getECataloguePath();
						sFilePath = p_sCatalogueFolderPath +"/"+ sFilePath;
						File fSourceFile = new File(sFilePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
	//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sFilePath, "", fSourceFile.getName());
						hmReturn.put("ECatalogue", sFilePath);
						bIsFileUpdated = true;
					}
					if(SkyBuyContants.UPLOADTYPE_PERSONALSHOPPER.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sFilePath = oUpdatedCatalogueDetailsVO.getPersonalShopperPath();
						sFilePath = p_sCatalogueFolderPath +"/"+ sFilePath;
						File fSourceFile = new File(sFilePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
	//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sFilePath, "", fSourceFile.getName());
						hmReturn.put("PersonalShopper", sFilePath);
						bIsFileUpdated = true;
					}
				}
				if(IPHONE.equalsIgnoreCase(sDeviceType) || ITOUCH.equalsIgnoreCase(sDeviceType)) {
					if(SkyBuyContants.UPLOADTYPE_IPHONEDESKTOPIMAGE.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sImagePath = oUpdatedCatalogueDetailsVO.getIPhoneDesktopPath();
						sImagePath = p_sCatalogueFolderPath +"/"+ sImagePath;
						File fSourceFile = new File(sImagePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
	//					oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sImagePath, "", fSourceFile.getName());
						hmReturn.put("iPhoneDesktop", sImagePath);
						bIsFileUpdated = true;
					}
					
					if(SkyBuyContants.UPLOADTYPE_IPHONEPERSONAL.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sImagePath = oUpdatedCatalogueDetailsVO.getIPhonePersonalShopperPath();
						sImagePath = p_sCatalogueFolderPath +"/"+ sImagePath;
						File fSourceFile = new File(sImagePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
//						oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sImagePath, "", fSourceFile.getName());
						hmReturn.put("iPhonePersonalShopper", sImagePath);
						bIsFileUpdated = true;
					}
					
					if(SkyBuyContants.UPLOADTYPE_IPHONEWELCOMEPAGE.equalsIgnoreCase(oUpdatedCatalogueDetailsVO.getUploadType())){
						String sImagePath = oUpdatedCatalogueDetailsVO.getIPhoneWelcomePagePath();
						sImagePath = p_sCatalogueFolderPath +"/"+ sImagePath;
						File fSourceFile = new File(sImagePath);
						/*File fDestPath = new File(fFolder,fSourceFile.getName());
						
						Utils.copyfile(fSourceFile,fDestPath);*/
//						oGenerateCatalogueZip.addFile(sClientCertPath, "", "ClientPass_"+p_sDeviceId+"/"+fSourceFile.getName());
						oGenerateCatalogueZip.addFile(sImagePath, "", fSourceFile.getName());
						hmReturn.put("iPhoneWelcomePage", sImagePath);
						bIsFileUpdated = true;
					}
				}
				
			}
			/*File fFolder1 = new File(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId);
			String files[] = fFolder1.list();
			if(files!=null && files.length>0){
				for(int i=0;i<files.length;i++) {
					oGenerateCatalogueZip.addFile(p_sCatalogueFolderPath+"/ClientPass_"+p_sDeviceId, files[i], "ClientPass_"+p_sDeviceId+"/"+files[i]);
				}
			}*/
			//Add eCatalogue xml file
			if(bIsNewVersionFound){					
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sE_CatalogueFileName, sE_CatalogueFileName);
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sCoverImageXML, sCoverImageXML);
				oGenerateCatalogueZip.addFile(p_sCatalogueXMLFile,sAboutPartnerXML, sAboutPartnerXML);
			}
			if(bIsNewVersionFound || bIsFileUpdated) {
				oGenerateCatalogueZip.finish();	
			}
			hmReturn.put("DownloadDate", sDownloadCateDate);
			logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.info("PlaceOrderDAO::getCatalogueDetailsByDate::Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null , con);
		}
		return hmReturn;
	}

	public static boolean sendEmail(OrderDetailsVO oOrderDetailsVO,Map<String, String> p_hmTags, String p_sTagName, String p_sReplaceText,List<StateVO> alState) throws Exception{
		logger.info("PlaceOrderDAO::sendEmail::ENTER");
		
		String sEmailContent="";
		String sOrderDetails="";
		String sToAddr="";
		List<OrderItemsVO> alOrderItemInfo =null;
		OrderItemsVO oOrderItemsVO = null;
		double dVendorPriceTotal=0.00,dSkyBuyPriceTotal=0.00,dQty=0,dVendPrice=0.00,dSbhPrice=0.00;
		String sAdminEmailAddr =null,sEmailSubject=null,sEmailTemplateId=null,sFromEmailAddr=null,sItemDetails ="";
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sEmailOrderDetails =null;
		EmailVO oEmailVO =null;
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		String sBccAddr="",sCcAddr="";
		String sProdTitle = "";
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId=oBundle.getString("email.customer.order.confirmation");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			
			
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
			
			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
			
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			//Header
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			
			/*for(StateVO oStateVO: alState){
				if(oStateVO.getStateCode().equalsIgnoreCase(oOrderDetailsVO.getCustBillState())){
					sBillingState = oStateVO.getStateName();
				}
				if(oStateVO.getStateCode().equalsIgnoreCase(oOrderDetailsVO.getCustShipState())){
					sShippingState = oStateVO.getStateName();
				}
			}*/
			
			if(oOrderDetailsVO!=null){
				sToAddr=oOrderDetailsVO.getCustBillEmail();
				sToAddr=sToAddr==null?"":sToAddr.trim();
			
				p_hmTags.put("{{OCN}}",oOrderDetailsVO.getCustTransId());
				p_hmTags.put("{{AirName}}",Utils.toTitleCase(oOrderDetailsVO.getAirName()));
				p_hmTags.put("{{FlightNo}}",oOrderDetailsVO.getFlightNo());
				p_hmTags.put("{{OrderId}}",oOrderDetailsVO.getOrderId());
				p_hmTags.put("{{OrderDate}}",oOrderDetailsVO.getOrderDt());
				p_hmTags.put("{{CustomerTransId}}",oOrderDetailsVO.getCustTransId());
				p_hmTags.put("{{BillingCustomerName}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillFname()+" "+oOrderDetailsVO.getCustBillLname()));
				p_hmTags.put("{{CustomerFName}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillFname()));
				p_hmTags.put("{{CustomerLName}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillLname()));
				p_hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((oOrderDetailsVO.getCustBillPhone())));
				p_hmTags.put("{{BillingMail}}",oOrderDetailsVO.getCustBillEmail());
				p_hmTags.put("{{BillingAddress}}",Utils.toTitleCase(mergeAddressLines(oOrderDetailsVO.getCustBillAddr1(), oOrderDetailsVO.getCustBillAddr2())));
				p_hmTags.put("{{BillingCity}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillCity()));
				p_hmTags.put("{{BillingState}}",oOrderDetailsVO.getCustBillState());
				p_hmTags.put("{{BillingZip}}",oOrderDetailsVO.getCustBillZip());
				p_hmTags.put("{{BillingCountry}}","USA");
				p_hmTags.put("{{ShippingCustomerName}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipFname()+" "+oOrderDetailsVO.getCustShipLname()));
				p_hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((oOrderDetailsVO.getCustShipPhone())));
				p_hmTags.put("{{ShippingMail}}",oOrderDetailsVO.getCustShipEmail());
				p_hmTags.put("{{ShippingAddress}}",Utils.toTitleCase(mergeAddressLines(oOrderDetailsVO.getCustShipAddr1(), oOrderDetailsVO.getCustShipAddr2())));
				p_hmTags.put("{{ShippingCity}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipCity()));
				p_hmTags.put("{{ShippingState}}",oOrderDetailsVO.getCustShipState());
				p_hmTags.put("{{ShippingZip}}",oOrderDetailsVO.getCustShipZip());
				p_hmTags.put("{{ShippingCountry}}","USA");
				p_hmTags.put("{{CCFName}}",Utils.toTitleCase(decryptInputData(oOrderDetailsVO.getCardFname(),"CLIENT",oOrderDetailsVO.getKeyRefId())));
				p_hmTags.put("{{ExpDate}}",decryptInputData(oOrderDetailsVO.getExpDt(),"CLIENT",oOrderDetailsVO.getKeyRefId()));
				p_hmTags.put("{{CustomerUrl}}",System.getProperty("SkybuyhighCustomerURL"));
				
				//Mask CC No
				String sCCNo = oOrderDetailsVO.getCCNoLFD();
				sCCNo = sCCNo == null?"":sCCNo.trim();
				
				
				p_hmTags.put("{{CCNo}}",sCCNo);
				
				String sCardType = decryptInputData(oOrderDetailsVO.getCardType(),"CLIENT",oOrderDetailsVO.getKeyRefId());
				if("VC".equalsIgnoreCase(sCardType)){
					p_hmTags.put("{{CardType}}","Visa Card");
				}else if("MC".equalsIgnoreCase(sCardType)){
					p_hmTags.put("{{CardType}}","Master Card");
				}else if("AC".equalsIgnoreCase(sCardType)){
					p_hmTags.put("{{CardType}}","American Express");
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sEmailOrderDetails = oEmailVO.getEmailOptionalContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
		
			sEmailOrderDetails=sEmailOrderDetails==null?"":sEmailOrderDetails.trim();
			
			alOrderItemInfo = oOrderDetailsVO.getOrderItemDetails();
			if(alOrderItemInfo!= null && alOrderItemInfo.size()>0){
				for(int i=0;i<alOrderItemInfo.size();i++){
					oOrderItemsVO =(OrderItemsVO) alOrderItemInfo.get(i);
					if(oOrderItemsVO.getQty()!=null)
						dQty=Double.parseDouble(oOrderItemsVO.getQty());
					if(oOrderItemsVO.getVendPrice()!=null)
						dVendPrice=Double.parseDouble(oOrderItemsVO.getVendPrice());
					if(oOrderItemsVO.getSbhPrice()!=null) 
						dSbhPrice=Double.parseDouble(oOrderItemsVO.getSbhPrice());
					
					p_hmTags.put("{{OrderItemId}}",oOrderItemsVO.getOrderItemId());
									
					
					sItemDetails ="";	
					sProdTitle = oOrderItemsVO.getProdTitle();
					sProdTitle = sProdTitle == null?"":sProdTitle.trim();
					sItemDetails = sItemDetails + "<tr><td align=left><b>Item Name:&nbsp;</b>"+sProdTitle+"</td></tr>";	
					sItemDetails = sItemDetails + "<tr><td align=left><b>Item Code:&nbsp;</b>"+oOrderItemsVO.getProdCode()+"</td></tr>";	
					if(oOrderItemsVO.getOwnerType().equalsIgnoreCase("VENDOR")){
						if(oOrderItemsVO.getProdSize()!= null && oOrderItemsVO.getProdSize().trim().length()>0)
								sItemDetails = sItemDetails + "<tr><td align=left><b>Size:&nbsp;</b>"+oOrderItemsVO.getProdSize()+"</td></tr>";
						if(oOrderItemsVO.getProdColor()!= null && oOrderItemsVO.getProdColor().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left><b>Color:&nbsp;</b>"+oOrderItemsVO.getProdColor()+"</td></tr>";
					}	
					if(oOrderItemsVO.getOwnerType().equalsIgnoreCase("AIRLINE")){
						if(oOrderItemsVO.getTravelDate()!= null && oOrderItemsVO.getTravelDate().trim().length()>0)
								sItemDetails = sItemDetails + "<tr><td align=left><b>Travel Date:&nbsp;</b>"+oOrderItemsVO.getTravelDate()+"</td></tr>";
					}	

					sItemDetails = sItemDetails == null?"":sItemDetails.trim();
					p_hmTags.put("{{ItemDetails}}",sItemDetails);
					
					p_hmTags.put("{{Qty}}",oOrderItemsVO.getQty());	
					if(oOrderItemsVO.getSbhPrice()!=null && oOrderItemsVO.getSbhPrice().trim().length()>0)
						p_hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemsVO.getSbhPrice())));	
					else
						p_hmTags.put("{{SkyBuyPrice}}","$0.00");	
					p_hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));	
					sOrderDetails=sOrderDetails+replaceTagValues(p_hmTags,sEmailOrderDetails,null,null);	
					dVendorPriceTotal = dVendorPriceTotal+dQty*dVendPrice;
					dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;
				}
			}						
			
			//dSkyBuyPriceTotal = round(dSkyBuyPriceTotal,2);
			p_hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));			
			p_hmTags.put("{{OrderDetails}}",sOrderDetails);
			
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

		
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));			
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);

			oEmail.sendEmail(oEmailLogVo);
//			System.out.print("EmailContent "+sEmailContent);
			
			logger.info("PlaceOrderDAO::sendEmail::EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			logger.info("PlaceOrderDAO::sendEmail::Exception "+e.getMessage());
			throw e;
		}	

		return true;
	}
	public static boolean checkCustTransId(String p_sCustTransId) throws Exception{	
		logger.info("PlaceOrderDAO::checkCustTransId::ENTER");		
			
		Connection con=null;		
		CallableStatement cstmt=null;
		ResultSet rs=null;
		int iCount = -1;	
		
		boolean bStatus = false;
		String sQry="{call usp_sbh_check_cust_trans_id(?,?)}";
		logger.debug("PlaceOrderDAO::checkCustTransId:SQL QUERY "+sQry);
		logger.info("PlaceOrderDAO::checkCustTransId::Order Confirmation No: "+p_sCustTransId);
	
		try{
			con = DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			cstmt.setString(1, p_sCustTransId);	
			cstmt.registerOutParameter(2, Types.INTEGER);			
			cstmt.execute();
			
			iCount = cstmt.getInt(2);
			if(iCount>0)
				bStatus = true;
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PlaceOrderDAO::checkCustTransId:Exception "+e.getMessage());
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null , con);
		}
		logger.info("PlaceOrderDAO::checkCustTransId::EXIT");
		return bStatus;
	}
	public static boolean sendFeedbackEmail(OrderDetailsVO oOrderDetailsVO,List<StateVO> alState) throws Exception{
		logger.info("PlaceOrderDAO:sendFeedbackEmail:ENTER");
		
		EmailVO oEmailVO =null;
		Map<String, String> p_hmTags =new HashMap<String, String>();	
		String sBccAddr="",sCcAddr="", sSkyBuyLogoPath = null, sSkyBuyAddr1 = null, sSkyBuyAddr2 = null, sSkyBuyPh = null;
		String sAdminEmailAddr =null,sEmailContent=null,sEmailSubject=null,sEmailTemplateId=null,sFromEmailAddr=null,sToEmailAddr = null;
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			/*sEmailTemplateId=oBundle.getString("email.customer.order.confirmation");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			
			*/
			sEmailTemplateId=oBundle.getString("email.customer.feedback");
			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			sToEmailAddr = System.getProperty("feedback.email.address").trim();
			sToEmailAddr=sToEmailAddr==null?"":sToEmailAddr.trim();
			
			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();
			
			sFromEmailAddr = oOrderDetailsVO.getCustBillEmail();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();
			
			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			p_hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			p_hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			p_hmTags.put("{{Address1}}",sSkyBuyAddr1);
			p_hmTags.put("{{Address2}}",sSkyBuyAddr2);
			p_hmTags.put("{{Phone}}",sSkyBuyPh);
			
			/*for(StateVO oStateVO: alState){
				if(oStateVO.getStateCode().equalsIgnoreCase(oOrderDetailsVO.getCustBillState())){
					sBillingState = oStateVO.getStateName();
				}
				if(oStateVO.getStateCode().equalsIgnoreCase(oOrderDetailsVO.getCustShipState())){
					sShippingState = oStateVO.getStateName();
				}
			}*/
			
			/*sEmailContent = "<b>Address:</b><br>";
			sEmailContent = sEmailContent + oOrderDetailsVO.getCustBillFname()+" "+oOrderDetailsVO.getCustBillLname()+"<br>";*/
			
				
			
//			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			
			if(oEmailVO!=null){
				sEmailSubject = oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
				
			}
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();			

			p_hmTags.put("{{BILLING_NAME}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillFname()+ " "+ oOrderDetailsVO.getCustBillLname()));
			p_hmTags.put("{{BILLING_ADDR1}}",Utils.toTitleCase((oOrderDetailsVO.getCustBillAddr1())));
			if(oOrderDetailsVO.getCustBillAddr2()!=null && !oOrderDetailsVO.getCustBillAddr2().equalsIgnoreCase(""))
				p_hmTags.put("{{BILLING_ADDR2}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillAddr2())+",<br/>");
			else
				p_hmTags.put("{{BILLING_ADDR2}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillAddr2()));
			
			p_hmTags.put("{{BILLING_CITY}}",Utils.toTitleCase(oOrderDetailsVO.getCustBillCity()));
			p_hmTags.put("{{BILLING_STATE}}",oOrderDetailsVO.getCustBillState());
			p_hmTags.put("{{BILLING_ZIP}}",oOrderDetailsVO.getCustBillZip());
			p_hmTags.put("{{BILLING_COUNTRY}}",oOrderDetailsVO.getCustBillCountry());
			p_hmTags.put("{{BILLING_EMAIL}}",oOrderDetailsVO.getCustBillEmail());
			p_hmTags.put("{{SHIPPING_NAME}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipFname()+ " "+ oOrderDetailsVO.getCustShipLname()));
			p_hmTags.put("{{SHIPPING_ADDR1}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipAddr1()));
			p_hmTags.put("{{ORD_CONF_NO}}",oOrderDetailsVO.getCustTransId());
			if(oOrderDetailsVO.getCustShipAddr2()!=null && !oOrderDetailsVO.getCustShipAddr2().equalsIgnoreCase(""))
				p_hmTags.put("{{SHIPPING_ADDR2}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipAddr2())+",<br/>");
			else
				p_hmTags.put("{{SHIPPING_ADDR2}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipAddr2()));
			
			p_hmTags.put("{{SHIPPING_CITY}}",Utils.toTitleCase(oOrderDetailsVO.getCustShipCity()));
			p_hmTags.put("{{SHIPPING_STATE}}",oOrderDetailsVO.getCustShipState());
			p_hmTags.put("{{SHIPPING_ZIP}}",oOrderDetailsVO.getCustShipZip());
			p_hmTags.put("{{SHIPPING_COUNTRY}}",oOrderDetailsVO.getCustShipCountry());
			p_hmTags.put("{{SHIPPING_EMAIL}}",oOrderDetailsVO.getCustShipEmail());
			p_hmTags.put("{{SUGGESTIONS}}",oOrderDetailsVO.getCustIdea());
			p_hmTags.put("{{OFFERS}}",oOrderDetailsVO.getCustOffer());
			
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			sEmailContent=replaceTagValues(p_hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();			


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			oEmailLogVo.setEmailTo((oEmail.convertToStringArray(sToEmailAddr)));			
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			
			
			
			oEmail.sendEmail(oEmailLogVo);
			//System.out.print("EmailContent "+sEmailContent);
			
			
		}catch(Exception e){		
			e.printStackTrace();
			logger.error("PlaceOrderDAO:sendFeedbackEmail:Exception "+e.getMessage());
			throw e;
		}	

		logger.info("PlaceOrderDAO::sendFeedbackEmail::EXIT");
		return true;
	}
	
	
	public static boolean sendMailToCustomer(AirlineAdvtVO p_oAirlineAdvtVO, String p_sLoginId, String p_sReqeustFrom) throws Exception{
		logger.info("PlaceOrderDAO::sendMailToCustomer::ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sPurchaseContactName = null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		ProductDetailsVO oProductDetailsVO = null;	
		String sBccAddr="",sCcAddr="";
		try{

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			
			sEmailTemplateId=oBundle.getString("email.customer.airline.advt");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			sPurchaseContactName = oBundle.getString("skybuyContactName");
			sPurchaseContactName=sPurchaseContactName==null?"":sPurchaseContactName.trim();	

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			oProductDetailsVO = CatalogueDAO.getProductDetails(p_oAirlineAdvtVO.getProdId(),"CLIENT");
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{ProdName}}",Utils.toTitleCase(oProductDetailsVO.getProdTitle()));
			hmTags.put("{{CustName}}",Utils.toTitleCase(p_oAirlineAdvtVO.getCustName()));


			sToAddr=p_oAirlineAdvtVO.getCustEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			//oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			
			//oEmailLogVo.setOwnerType("customer");		
			//oEmailLogVo.setOwnerId(Long.parseLong(p_oAirlineAdvtVO.getCustId()));
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId, p_sReqeustFrom,p_oAirlineAdvtVO.getDeviceId());
			
			//System.out.print("EmailContent "+sEmailContent);
			
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.info("PlaceOrderDAO::sendMailToCustomer::Exception "+e.getMessage());
			throw e;
		}
		logger.info("PlaceOrderDAO::sendMailToCustomer::EXIT");
		return bSentEmail;
	}
	
	
	public static boolean sendMailToAirline(AirlineAdvtVO p_oAirlineAdvtVO,String p_sLoginId,String p_sReqeustFrom) throws Exception{
		logger.info("PlaceOrderDAO::sendMailToAirline::ENTER");		

		boolean bSentEmail=true;
		boolean bEmailSent = false;
		String sToAddr="",sCCAddr = "";		
		HashMap<String, String> hmTags = new HashMap<String, String>();
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr =null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null,sPurchaseContactName = null;
		String sEmailTemplateId=null,sEmailContent=null;
		EmailVO oEmailVO =null;
		ProductDetailsVO oProductDetailsVO = null;	
		String sBccAddr="",sCcAddr="", sEmailOrPhoneTag ="", sSecondaryEmail = "";;
		try{

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.airline.airline.advt");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			sPurchaseContactName = oBundle.getString("skybuyContactName");
			sPurchaseContactName=sPurchaseContactName==null?"":sPurchaseContactName.trim();	

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);
			oProductDetailsVO = CatalogueDAO.getProductDetails(p_oAirlineAdvtVO.getProdId(),"CLIENT");
			
			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();

			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	


			//hmTags.put("{{Reason}}",p_sReason);
			hmTags.put("{{ProdName}}",Utils.toTitleCase(oProductDetailsVO.getProdTitle()));
			hmTags.put("{{Name}}",oProductDetailsVO.getOwnerName());
			hmTags.put("{{ContactName}}",Utils.toTitleCase(oProductDetailsVO.getOwnerContactName()));
			hmTags.put("{{CustName}}",Utils.toTitleCase(p_oAirlineAdvtVO.getCustName()));
			
			if(p_oAirlineAdvtVO.getCustEmail()!=null && !p_oAirlineAdvtVO.getCustEmail().equalsIgnoreCase("")){
				sEmailOrPhoneTag = "<tr><td align=left nowrap=nowrap bgcolor=#FFFFFF><b>Email</b></td><td align=left bgcolor=#FFFFFF>:</td><td align=left bgcolor=#FFFFFF>"+p_oAirlineAdvtVO.getCustEmail()+"</td></tr>";
			}
			if(p_oAirlineAdvtVO.getCustPhone()!=null && !p_oAirlineAdvtVO.getCustPhone().equalsIgnoreCase("")){
				sEmailOrPhoneTag = sEmailOrPhoneTag + "<tr><td align=left nowrap=nowrap bgcolor=#FFFFFF ><b>Phone </b></td><td align=left bgcolor=#FFFFFF>:</td><td align=left bgcolor=#FFFFFF nowrap=nowrap>"+AirlineDAO.getPhoneFormat(p_oAirlineAdvtVO.getCustPhone())+"</td></tr>";
			}
			/*hmTags.put("{{CustEmail}}",p_oAirlineAdvtVO.getCustEmail());
			hmTags.put("{{CustPhone}}",AirlineDAO.getPhoneFormat(p_oAirlineAdvtVO.getCustPhone()));*/

			hmTags.put("{{EmailOrPhone}}",sEmailOrPhoneTag);

			sToAddr=oProductDetailsVO.getVendorEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();

			sSecondaryEmail = oProductDetailsVO.getSecondaryEmailId();
			if(sSecondaryEmail!=null && !sSecondaryEmail.equalsIgnoreCase("")){
				if(!sToAddr.contains(sSecondaryEmail)){
					sToAddr = sToAddr + ","+sSecondaryEmail;
				}
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailSubject=replaceTagValues(hmTags,sEmailSubject,null,null);
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();

			sEmailContent=sEmailContent==null?"":sEmailContent.trim();
			
			sEmailContent=replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			
			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));
		
			if(sCcAddr!=null && sCcAddr.trim().length()>0){
				sCcAddr =sAdminEmailAddr+","+sCcAddr;						
			}else{
				sCcAddr = sAdminEmailAddr;
			}
			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));	
			
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			//oEmailLogVo.setAttachmentFile(oPdfFile.getAbsolutePath());

			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent = false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			//oEmailLogVo.setOwnerType("customer");		
			//oEmailLogVo.setOwnerId(Long.parseLong(p_oAirlineAdvtVO.getCustId()));
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailCcAddr(sCCAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sLoginId, p_sReqeustFrom,p_oAirlineAdvtVO.getDeviceId());
			
			//System.out.print("EmailContent "+sEmailContent);

			logger.info("PlaceOrderDAO::sendMailToAirline::EXIT");
		}catch(Exception e){		
			e.printStackTrace();
			bSentEmail=false;
			logger.info("PlaceOrderDAO::sendMailToAirline::Exception "+e.getMessage());
			throw e;
		}

		return bSentEmail;
	}
	
	public static String placeAirlineAdvt(AirlineAdvtVO oAirlineAdvtVO,Connection p_con,String p_sLoginId, String p_sIpAddress) throws Exception{	
		logger.info("PlaceOrderDAO::placeAirlineAdvt::ENTER");
		
		CallableStatement cstmt=null;
		String sQry="{call usp_sbh_place_airline_advt(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		logger.info("PlaceOrderDAO::placeAirlineAdvt::SQL QUERY "+sQry);
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Air Id "+oAirlineAdvtVO.getAirId());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Device Id "+oAirlineAdvtVO.getDeviceId());	
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Flight No "+oAirlineAdvtVO.getFlightNo());	
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Order Date "+oAirlineAdvtVO.getOrderDt());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Product Id "+oAirlineAdvtVO.getProdId());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Cust Name "+oAirlineAdvtVO.getCustName());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Cust Phone "+oAirlineAdvtVO.getCustPhone());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Cust Email "+oAirlineAdvtVO.getCustEmail());
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Host Name "+p_sLoginId);
		logger.info("PlaceOrderDAO::placeAirlineAdvt::IP Address "+p_sIpAddress);
		logger.info("PlaceOrderDAO::placeAirlineAdvt::Originated From "+oAirlineAdvtVO.getOriginatedFrom());
		String  sError = null;
		int iAdvtId = 0;
		try{			
			cstmt=p_con.prepareCall(sQry); 

			cstmt.setString(1,oAirlineAdvtVO.getAirId());
			cstmt.setString(2,oAirlineAdvtVO.getDeviceId());	
			cstmt.setString(3,oAirlineAdvtVO.getFlightNo());	
			cstmt.setString(4,oAirlineAdvtVO.getOrderDt());
			cstmt.setString(5,oAirlineAdvtVO.getProdId());
			cstmt.setString(6,oAirlineAdvtVO.getCustName());
			cstmt.setString(7,oAirlineAdvtVO.getCustPhone());
			cstmt.setString(8,oAirlineAdvtVO.getCustEmail());
			cstmt.setString(9,p_sLoginId);
			cstmt.setString(10,p_sIpAddress);
			cstmt.setString(11,oAirlineAdvtVO.getOriginatedFrom());
			cstmt.registerOutParameter(12, Types.VARCHAR);
			cstmt.registerOutParameter(13, Types.INTEGER);
			cstmt.execute();
			sError = cstmt.getString(12);
			iAdvtId = cstmt.getInt(13);
			if(iAdvtId!=0){
				oAirlineAdvtVO.setAdvtId(iAdvtId+"");
			}

			logger.info("PlaceOrderDAO::placeAirlineAdvt:EXIT");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("PlaceOrderDAO::placeAirlineAdvt:Exception "+e.getMessage());
			sError = "Problem in Order Placement";
			throw e;
			
		}
		finally{
			Utils.closeDBConnection(null, cstmt, null, null, null);
		}
		return sError;
	}
	
	public static String authenticateDevice(String sProductKey,String sDeviceId,String sMacAddress)throws Exception{
		logger.info("PlaceOrderDAO::authenticateDevice:ENTER");
		
		String sDeviceStatus = "";
		try{
			boolean bStatus = DeviceRegDAO.validateDevice(sProductKey, "0", sMacAddress, "DC");

			if(bStatus){
				sDeviceStatus = "";
			}
			else{
				String sProdkeyStatus = DeviceRegDAO.checkProdKey(sProductKey);
				if(sProdkeyStatus!=null && (sProdkeyStatus.equalsIgnoreCase("A"))){

					String sAirlineStatus = DeviceRegDAO.checkAirlineStatus("0",sProductKey);			
					sAirlineStatus = sAirlineStatus==null?"":sAirlineStatus.trim();
					if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("A")){
						boolean bMacAddrStatus = DeviceRegDAO.checkMacAddress(sProductKey,sMacAddress);
						if(!bMacAddrStatus){			
							return DEVICE_NOT_REGISTERED;
						}

					}else if(sAirlineStatus!=null && sAirlineStatus.trim().length()==0){
						return INVALID_AIRLINE_CODE;	
					}
					else if(sAirlineStatus!=null && sAirlineStatus.equalsIgnoreCase("I")){
						return INACTIVE_AIRLINE;	
					}

				}else if(sProdkeyStatus!=null && sProdkeyStatus.trim().length()==0){
					return INVALID_PRODUCT_KEY;		
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("N")){
					return DEVICE_NOT_ALLOCATED;	
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("AL")){
					return DEVICE_NOT_ACTIVATED;
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("I")){
					return DEVICE_INACTIVE;	
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("NA")) {
					return DEVICE_NOT_ALLOCATED;
				}
				else if(sProdkeyStatus!=null && sProdkeyStatus.equalsIgnoreCase("D")){
					return DEVICE_DELETED;	
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			logger.debug("PlaceOrderDAO::authenticateDevice ::  EXCEPTION : "+ e.getMessage());
			sDeviceStatus = DEVICE_EXCEPTION;
		}
		logger.info("PlaceOrderDAO::authenticateDevice:EXIT");
		return sDeviceStatus ;
	}

	public static ArrayList<CategoryVO> getCategory() throws Exception{
		logger.info("PlaceOrderDAO::getCategory:ENTER");
		
		ArrayList<CategoryVO> alCategory=new ArrayList<CategoryVO>();
		Connection con=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		CategoryVO oCategoryVO=null;

		String sQry="{call usp_sbh_get_category }";

		try{
			con=DBConnection.getSQL2005Connection();
			cstmt=con.prepareCall(sQry);
			rs=cstmt.executeQuery();
			while(rs.next()){
				oCategoryVO=new CategoryVO();
				oCategoryVO.setCateId(rs.getString("cate_id"));
				oCategoryVO.setCateName(rs.getString("cate_name"));				
				alCategory.add(oCategoryVO);
			}

			logger.info("PlaceOrderDAO::getCategory:EXIT");
		}catch(Exception e){
			logger.error("PlaceOrderDAO::getCategory:EXCEPTION" +e.getMessage());
			e.printStackTrace();
			throw e;

		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);
			
		}
		
		return alCategory;
	}
	public static List<StateVO> getState() throws Exception{
		logger.info("PlaceOrderDAO::getState:ENTER");
		
		List<StateVO> arState=new ArrayList<StateVO>();
		Connection con=null;
		CallableStatement cstmt=null;
		ResultSet rs=null;
		StateVO oStateVO=null;		
		String sQry="{call usp_sbh_get_states}";

		try{
			con=DBConnection.getSQL2005Connection();			
			cstmt=con.prepareCall(sQry);		
			rs=cstmt.executeQuery();
			while(rs.next()){
				oStateVO=new StateVO();
				oStateVO.setStateCode(rs.getString("state_id"));			
				oStateVO.setStateName(rs.getString("state_name"));				
				arState.add(oStateVO);
			}

			logger.info("PlaceOrderDAO::getState:EXIT");
		}catch(Exception e){
			logger.error("PlaceOrderDAO::getState:EXCEPTION" +e.getMessage());
			e.printStackTrace();
			throw e;
		}
		finally{
			Utils.closeDBConnection(rs, cstmt, null, null, con);
		}
		return arState;
	}

	public static String placeOrderDetails(String p_sOrderType, String p_sOrderDetails,String p_sIpAddress) throws Exception {
		logger.info("PlaceOrderDAO::placeOrderDetails::ENTER");
		
		boolean bCustTransId = false;
		boolean bIsTotalOrderValueSame = false;
		byte []baMedImage = null;
		String sDeviceStatus = "";
		String sError = null;
		String sCateFolderName = "",sImageUploadPath = "";
		String sMedImagePath = null;
		Connection con = null;
		OrderDetailsVO oOrderDetailsVO = null;
		AirlineAdvtVO oAirlineAdvtVO = null;
		List<AirlineAdvtVO> alAirlineAdvt = null;
		List<OrderItemsVO> alOrderItemInfo  = null;
		OrderItemsVO oOrderItemsVO = null;
		Map<String, String> p_hmTags =new HashMap<String, String>();
		List<StateVO> alStateList = null;
		ArrayList<CategoryVO> alCategory =null;
		ProductDetailsVO oProductDetailsVO = null;	

		try{
			logger.info("PlaceOrderDAO::placeOrderDetails::ENTER");
			con = DBConnection.getSQL2005Connection();

			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");
			alStateList = PlaceOrderDAO.getState();
			sImageUploadPath=System.getProperty("ImageUploadPath");
			sImageUploadPath = sImageUploadPath == null?"":sImageUploadPath.trim();
			String sRemoteHostName = InetAddress.getLocalHost().getHostName();

			// Setting the auto commit functionality to false;
			con.setAutoCommit(false);
			if(PRODUCT_DETAILS.equalsIgnoreCase(p_sOrderType) && p_sOrderDetails!=null) {
				logger.info("PlaceOrderDAO::placeOrderDetails::OrderDetails: "+p_sOrderDetails);  

				Document doc = PlaceOrderUtil.initXML(p_sOrderDetails);
				oOrderDetailsVO= PlaceOrderUtil.parseOrderInfo(doc);
				if(oOrderDetailsVO!=null){
					oOrderDetailsVO.setProductKey(oOrderDetailsVO.getProductKey()==null?"":oOrderDetailsVO.getProductKey().trim());

					String sProductKey =oOrderDetailsVO.getProductKey();
					sProductKey = sProductKey==null?"":sProductKey.trim();

					String sDeviceId =  oOrderDetailsVO.getDeviceId();
					sDeviceId = sDeviceId ==null?"":sDeviceId.trim();

					String sMacAddress =  oOrderDetailsVO.getMacAddr();
					sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

					String sProcessorId = oOrderDetailsVO.getProcessorId();
					sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

					String sFlightNo = oOrderDetailsVO.getFlightNo();
					sFlightNo = sFlightNo ==null?"":sFlightNo.trim();

					sDeviceStatus = PlaceOrderDAO.authenticateDevice(sProductKey,sDeviceId,sMacAddress);
					if("".equalsIgnoreCase(sDeviceStatus)) {	
						bIsTotalOrderValueSame = PlaceOrderUtil.checkTotalOrderValue(oOrderDetailsVO);
						if(bIsTotalOrderValueSame){
							// Check the order is already placed
							if(oOrderDetailsVO.getCustTransId()!=null && oOrderDetailsVO.getCustTransId().trim().length()>0)
								bCustTransId  = PlaceOrderDAO.checkCustTransId(oOrderDetailsVO.getCustTransId());

							if(!bCustTransId){
								String sCCNo=oOrderDetailsVO.getDecryptCCNo();
								sCCNo=sCCNo==null?"":sCCNo.trim();
								if(sCCNo!=null && sCCNo.trim().length() > 0){
									if(CCUtils.validCC(sCCNo,oOrderDetailsVO.getDecryptedCardType())){
										oOrderDetailsVO.setCCNoStatus(CREDITCARD_NOT_PROCESSED);
									}else{
										/*oOrderDetailsVO.setCCNo(null);
										oOrderDetailsVO.setCvv(null);*/
										oOrderDetailsVO.setCCNoStatus(CREDITCARD_FAILED);
										oOrderDetailsVO.setCCNoComments(INVALID_CREDITCARD);
									}
								}else {
									oOrderDetailsVO.setCCNoStatus(CREDITCARD_FAILED);
									oOrderDetailsVO.setCCNoComments(THREE_TIME_FAILED_ORDER);
								}
								alOrderItemInfo = oOrderDetailsVO.getOrderItemDetails();
								if(alOrderItemInfo!= null && alOrderItemInfo.size()>0){
									for(int i=0;i<alOrderItemInfo.size();i++){
										oOrderItemsVO =(OrderItemsVO) alOrderItemInfo.get(i);
										if(CREDITCARD_NOT_PROCESSED.equalsIgnoreCase(oOrderDetailsVO.getCCNoStatus())) {
											oOrderItemsVO.setOrderItemStatus(ORDER_PENDING_STATUS);
										}else if(CREDITCARD_FAILED.equalsIgnoreCase(oOrderDetailsVO.getCCNoStatus())) {
											oOrderItemsVO.setOrderItemStatus(ORDER_FAILED_STATUS);
										}

										oProductDetailsVO = CatalogueDAO.getProductDetails(oOrderItemsVO.getProdId(),"CLIENT");
//										alCategory=(ArrayList)oSession.getServletContext().getAttribute("Category");
										alCategory = PlaceOrderDAO.getCategory();
										if(oProductDetailsVO != null) {
											if("Vendor".equalsIgnoreCase(oProductDetailsVO.getOwnerType())){
												if(alCategory!=null && alCategory.size()>0){
													for(CategoryVO oCategoryVO: alCategory){
														if(oCategoryVO.getCateId()!=null){
															if(oCategoryVO.getCateId().equals(oProductDetailsVO.getCateId()))
																sCateFolderName = oCategoryVO.getCateName();
														}		
													}
												}
											}else if("Airline".equalsIgnoreCase(oProductDetailsVO.getOwnerType())){
												sCateFolderName = "PrivateJetJaunts";
											}
										}

										sMedImagePath = sImageUploadPath+"/"+oProductDetailsVO.getMainImgPath()+"/"+sCateFolderName+"/Med/"+oProductDetailsVO.getOwnerId()+oProductDetailsVO.getProdId()+"med."+oProductDetailsVO.getMainImgType();
										baMedImage = PlaceOrderDAO.getByteArrayFromFilePath(sMedImagePath);

										sError = PlaceOrderDAO.placeOrderDetails(oOrderDetailsVO,oOrderItemsVO,baMedImage, con,sRemoteHostName, p_sIpAddress);

										/**** Insert Order audit details ******/
										if(oOrderItemsVO.getOrderItemId()!=null && sError==null){								
											String sMsg=oBundle.getString("customer.order.confirmation.msg");
											sMsg=sMsg==null?"":sMsg.trim();								
											OrderDAO.insertOrderAuditDetails(oOrderItemsVO.getOrderItemId(),oOrderDetailsVO.getOrderId(),oOrderDetailsVO.getCustTransId(),sMsg,oOrderDetailsVO.getAirName(),con,sRemoteHostName,sDeviceId);								
										}
									}
									if(oOrderDetailsVO.getCCNo() == null || CREDITCARD_FAILED.equalsIgnoreCase(oOrderDetailsVO.getCCNoStatus())) {

										PlaceOrderEmail.sendPlaceOrderFailureEmailToCustomer(oOrderDetailsVO, sRemoteHostName,sDeviceId);
									}
									con.commit();
									if(sError!=null && sError.trim().length()>0){
										sDeviceStatus = ORDER_FAILED;
									}	
									else{ 
										DeviceRegDAO.insertDeviceLogDetails(oOrderDetailsVO.getAirId(),sDeviceId,sFlightNo,"Order Placement", alOrderItemInfo.size(),oOrderDetailsVO.getMacAddr(),oOrderDetailsVO.getCustTransId(), sRemoteHostName,"CLIENT");
										if(oOrderDetailsVO.getCCNo() != null && !CREDITCARD_FAILED.equalsIgnoreCase(oOrderDetailsVO.getCCNoStatus())) {
											//Send order placement details to customer and admin by email
											PlaceOrderDAO.sendEmail(oOrderDetailsVO,p_hmTags,null,null,alStateList);
										}
										/**** To Send Offer and Ideas given by the customer ****/
										if((oOrderDetailsVO.getCustOffer()!=null && !oOrderDetailsVO.getCustOffer().equalsIgnoreCase("")) || (oOrderDetailsVO.getCustIdea()!=null && !oOrderDetailsVO.getCustIdea().equalsIgnoreCase("")) ){
											PlaceOrderDAO.sendFeedbackEmail(oOrderDetailsVO,alStateList);
										}
										sDeviceStatus = ORDER_PLACED;
									}
								}
								else{
									sDeviceStatus = ORDER_FAILED;
								}
							}else{
								sDeviceStatus = ORDER_ALREADY_PLACED;
							}
						}else{
							sDeviceStatus = INVALID_TOTAL_ORDER_VALUE;
						}
					}
				}
			}else if(AIRLINE_DETAILS.equalsIgnoreCase(p_sOrderType)){
				logger.info("PlaceOrderDAO::placeOrderDetails::Advertisement: "+p_sOrderDetails);

				Document doc = PlaceOrderUtil.initXML(p_sOrderDetails);
				alAirlineAdvt= PlaceOrderUtil.parseAirlineAdvtInfo(doc);
				if(alAirlineAdvt!=null){

					oAirlineAdvtVO = (AirlineAdvtVO)alAirlineAdvt.get(0);

					oAirlineAdvtVO.setProductKey(oAirlineAdvtVO.getProductKey()==null?"":oAirlineAdvtVO.getProductKey().trim());

					String sProductKey =oAirlineAdvtVO.getProductKey();
					sProductKey = sProductKey==null?"":sProductKey.trim();

					String sDeviceId =  oAirlineAdvtVO.getDeviceId();
					sDeviceId = sDeviceId ==null?"":sDeviceId.trim();

					String sMacAddress =  oAirlineAdvtVO.getMacAddr();
					sMacAddress = sMacAddress ==null?"":sMacAddress.trim();

					String sProcessorId = oAirlineAdvtVO.getProcessorId();
					sProcessorId = sProcessorId ==null?"":sProcessorId.trim();

					String sFlightNo = oAirlineAdvtVO.getFlightNo();
					sFlightNo = sFlightNo ==null?"":sFlightNo.trim();


					/*alAirlineDetails = DeviceRegDAO.getAirlineId(sProductKey);
					String sAirId = (String)alAirlineDetails.get(0);
					sAirId = sAirId==null?"":sAirId.trim();
					oAirlineAdvtVO.setAirId(sAirId);*/

					sDeviceStatus = PlaceOrderDAO.authenticateDevice(sProductKey,sDeviceId,sMacAddress);

					if("".equalsIgnoreCase(sDeviceStatus)) {	
						con.setAutoCommit(false);
						if(alAirlineAdvt!= null && alAirlineAdvt.size()>0){
							for(int i=0;i<alAirlineAdvt.size();i++){
								oAirlineAdvtVO =(AirlineAdvtVO) alAirlineAdvt.get(i);
								sError = PlaceOrderDAO.placeAirlineAdvt(oAirlineAdvtVO,con,sRemoteHostName,p_sIpAddress);

								/**** Insert Order audit details ******/
								/*	if(sError==null){								
										String sMsg=oBundle.getString("customer.order.confirmation.msg");
										sMsg=sMsg==null?"":sMsg.trim();								
										OrderDAO.insertOrderAuditDetails(oOrderItemsVO.getOrderItemId(),oOrderDetailsVO.getOrderId(),oOrderDetailsVO.getCustTransId(),sMsg,oOrderDetailsVO.getAirName(),con);								
									}*/
							}	
							con.commit();
							if(alAirlineAdvt!= null && alAirlineAdvt.size()>0){
								for(int i=0;i<alAirlineAdvt.size();i++){
									oAirlineAdvtVO =(AirlineAdvtVO) alAirlineAdvt.get(i);
									/**** To Send mail to the customer and Airline****/
									if(oAirlineAdvtVO.getCustEmail()!=null && !oAirlineAdvtVO.getCustEmail().equalsIgnoreCase("") ){
										PlaceOrderDAO.sendMailToCustomer(oAirlineAdvtVO,sRemoteHostName,"CLIENT");
									}
									PlaceOrderDAO.sendMailToAirline(oAirlineAdvtVO,sRemoteHostName,"CLIENT");
								}
							}
							if(sError!=null && sError.trim().length()>0){
								// System.out.println("PlaceOrder::placeOrder:Order placement-failed");
								sDeviceStatus = ORDER_FAILED;
							}	
							else{ 
								DeviceRegDAO.insertDeviceLogDetails(oAirlineAdvtVO.getAirId(),sDeviceId,sFlightNo,"Order Placement", alAirlineAdvt.size(),oAirlineAdvtVO.getMacAddr(), "",sRemoteHostName,"CLIENT");

								//Send order placement details to customer and admin by email
								//PlaceOrderDAO.sendEmail(oOrderDetailsVO,p_hmTags,null,null,alStateList);
								sDeviceStatus = ORDER_PLACED;
							}
						}
						else{
							sDeviceStatus = ORDER_FAILED;
						}
					}
				}
			}
			// System.out.println("PlaceOrder::placeOrder:Exit");  

		}catch(Exception e){
			if("XML".equalsIgnoreCase(e.getMessage())) {
				sDeviceStatus = ERROR_PARSING_XML;
			}else {
				sDeviceStatus = ORDER_FAILED;
			}
			con.rollback();
			e.printStackTrace();
			logger.error("PlaceOrderDAO::placeOrder:Exception "+e.getMessage());
		}finally{
			con.commit();
			Utils.closeDBConnection(null, null, null, null, con);
			
		}
		logger.info("PlaceOrderDAO::placeOrderDetails:EXIT");
		return sDeviceStatus; 
	}
	
	public static int updateOrderDetails(String p_sOrderDetails,String p_sHostName, String p_sIPAddress)throws Exception{
		logger.error("PlaceOrderDAO::updateOrderDetails:ENTER");
		Document doc = PlaceOrderUtil.initXML(p_sOrderDetails);
		Connection con = null;
		CallableStatement cstmt=null;
		String sQry=null;
		int iIsUpdate=0;
		OrderDetailsVO oOrderDetailsVO = PlaceOrderUtil.parseUpdateOrderDetails(doc);
		if(oOrderDetailsVO!=null){
			try{
				con = DBConnection.getSQL2005Connection();
				sQry="{call usp_sbh_upd_customer_feedback(?,?,?,?,?,?)}";
				logger.error("PlaceOrderDAO::updateOrderDetails:Query: "+sQry);
				logger.error("PlaceOrderDAO::updateOrderDetails:Order Conformation No : "+oOrderDetailsVO.getCustTransId());
				logger.error("PlaceOrderDAO::updateOrderDetails:Suggesstion: "+oOrderDetailsVO.getSuggestion());
				logger.error("PlaceOrderDAO::updateOrderDetails:New Service Feedback: "+oOrderDetailsVO.getNewServicesFeedback());
				logger.error("PlaceOrderDAO::updateOrderDetails:Host Name: "+p_sHostName);
				logger.error("PlaceOrderDAO::updateOrderDetails:IP Address: "+p_sIPAddress);
				cstmt=con.prepareCall(sQry);
				cstmt.registerOutParameter(1,Types.INTEGER);
				cstmt.setString(2,oOrderDetailsVO.getCustTransId());
				cstmt.setString(3,oOrderDetailsVO.getSuggestion());
				cstmt.setString(4,oOrderDetailsVO.getNewServicesFeedback());
				cstmt.setString(5,p_sHostName);
				cstmt.setString(6,p_sIPAddress);
				cstmt.execute();
				iIsUpdate=cstmt.getInt(1);
			}
			catch(Exception e){
				e.printStackTrace();
				logger.error("PlaceOrderDAO::updateOrderDetails:Exception "+e.getMessage());
			}
			finally{
				Utils.closeDBConnection(null, cstmt, null, null, con);
			}
		}
		logger.error("PlaceOrderDAO::updateOrderDetails:EXIT");
		return iIsUpdate;
	}
	public static byte[] getByteArrayFromFilePath(String p_sFilePath) throws IOException {
		byte[] bytes = null;
		File file = new File(p_sFilePath);
		if(file.exists()){
			InputStream is = new FileInputStream(file);

			// Get the size of the file
			long length = file.length();

			// You cannot create an array using a long type.
			// It needs to be an int type.
			// Before converting to an int type, check
			// to ensure that file is not larger than Integer.MAX_VALUE.
			if (length > Integer.MAX_VALUE) {
				// File is too large
			}

			// Create the byte array to hold the data
			bytes = new byte[(int)length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file "+file.getName());
			}

			// Close the input stream and return bytes
			is.close();
		}
		return bytes;
	}
	public static String replaceTagValues(Map<String, String> p_hmTags,String p_sEmailText, String p_sTagName, 
			String p_sReplaceText) throws Exception{
		String sHMKey = null,sRegex = null,sHMValue = null;

		if(p_hmTags != null && p_hmTags.size() > 0){

			Iterator itr = p_hmTags.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry oEntry = (Map.Entry)itr.next();
				sHMKey = (String)oEntry.getKey();

				sHMValue = (String) oEntry.getValue();
				sHMKey = sHMKey.replaceAll("([{])", "\\\\{");
				sHMKey = sHMKey.replaceAll("([}])", "\\\\}");
				sRegex = "(?i)" + sHMKey.trim();
				sHMValue = (sHMValue == null?"":sHMValue.trim());
				sHMValue = escapeRegExp (sHMValue);
				//System.out.println("Key: "+sRegex +" : Value : "+sHMValue);
				p_sEmailText = p_sEmailText.replaceAll(sRegex,sHMValue);

			}
		}else if(p_sTagName != null && p_sReplaceText != null){

			p_sTagName = p_sTagName.replaceAll("([{])", "\\\\{");
			p_sTagName = p_sTagName.replaceAll("([}])", "\\\\}");
			sRegex = "(?i)"+p_sTagName.trim();
			p_sReplaceText = escapeRegExp (p_sReplaceText);
			p_sEmailText = p_sEmailText.replaceAll(sRegex, p_sReplaceText);

		}
		return p_sEmailText;
	}	
	public static String escapeRegExp (String p_sText) {
		StringBuffer sbText = new StringBuffer();
		if (p_sText != null && p_sText.trim().length() > 0) {
			int iLen = p_sText.length();
			char[] caText = new char[iLen];

			p_sText.getChars(0,iLen,caText,0);
			for(int i = 0; i<(iLen);i++) {
				if (caText[i] == '$'){
					sbText.append ("\\"+caText[i]);

				} else {
					sbText.append(caText[i]);
				}
			}
		}
		return sbText.toString();
	}
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}
	public static double round(double d, int decimalPlace){
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	public static float round(float Rval, int Rpl) {
		float p = (float)Math.pow(10,Rpl);
		Rval = Rval * p;
		float tmp = Math.round(Rval);
		return (float)tmp/p;
	}
	
	
	


	

}
