/**
 * 
 */
package com.sbh.client.dao;

import static com.sbh.util.Utils.mergeAddressLines;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sbh.client.vo.OrderDetailsVO;
import com.sbh.client.vo.OrderItemsVO;
import com.sbh.dao.AirlineDAO;
import com.sbh.dao.EmailDAO;
import com.sbh.dao.OrderDAO;
import com.sbh.email.Email;
import com.sbh.util.Utils;
import com.sbh.vo.EmailLogVO;
import com.sbh.vo.EmailVO;
import com.sbh.vo.OrderItemDetailsVO;

/**
 * @author nbvk
 *
 */
public class PlaceOrderEmail {
	private static Logger logger = LogManager.getLogger(PlaceOrderEmail.class);
	
	public static boolean sendPlaceOrderFailureEmailToCustomer(OrderDetailsVO p_oOrderDetailsVO, String p_sRemoteHost, String p_sDeviceId) 
	throws Exception {
		boolean bEmailSent = false;
		logger.info("OrderDAO::sendPlaceOrderFailureEmailToCustomer:ENTRY");
		
		String sEmailContent="";	
		String sToAddr="";		
		HashMap<String, String> hmTags = new HashMap<String, String>();	
		String sAdminEmailAddr =null,sEmailSubject=null,sFromEmailAddr=null,sEmailTemplateId =null,sEmailOrderDetails = null;
		String sSkyBuyLogoPath = null,sSkyBuyAddr1=null,sSkyBuyAddr2=null,sSkyBuyPh=null;
		EmailVO oEmailVO =null;
		String sBccAddr="",sCcAddr="",sItemDetails = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
		List<OrderItemsVO> alOrderItemsVO = null;
		String sOrderDetails = "";
		double dSkyBuyPriceTotal = 0.0;
		try{
			ResourceBundle oBundle = ResourceBundle.getBundle("com.sbh.properties.resources.ApplicationResources");

			sEmailTemplateId=oBundle.getString("email.customer.place.order.failure");
			sEmailTemplateId=sEmailTemplateId==null?"":sEmailTemplateId.trim();			

			oEmailVO = EmailDAO.getEmailContentDetails(sEmailTemplateId);

			sSkyBuyLogoPath = System.getProperty("SkyBuyHighLogoPath").trim();
			sSkyBuyLogoPath=sSkyBuyLogoPath==null?"":sSkyBuyLogoPath.trim();

			sFromEmailAddr=System.getProperty("email.from.address").trim();
			sFromEmailAddr=sFromEmailAddr==null?"":sFromEmailAddr.trim();

			sAdminEmailAddr=oEmailVO.getAdminEmailAddress();
			sAdminEmailAddr=sAdminEmailAddr==null?"":sAdminEmailAddr.trim();

			sSkyBuyAddr1=oBundle.getString("skybuy.address1");
			sSkyBuyAddr1=sSkyBuyAddr1==null?"":sSkyBuyAddr1.trim();
			sSkyBuyAddr2=oBundle.getString("skybuy.address2");
			sSkyBuyAddr2=sSkyBuyAddr1==null?"":sSkyBuyAddr2.trim();
			sSkyBuyPh=oBundle.getString("skybuy.phone");
			sSkyBuyPh=sSkyBuyPh==null?"":sSkyBuyPh.trim();
			
			alOrderItemsVO = p_oOrderDetailsVO.getOrderItemDetails();
			
			//Header
			hmTags.put("{{Logo}}",sSkyBuyLogoPath);
			hmTags.put("{{Date}}",dateFormat(new Date()));
			//SkyBuy Address
			hmTags.put("{{Address1}}",sSkyBuyAddr1);
			hmTags.put("{{Address2}}",sSkyBuyAddr2);
			hmTags.put("{{Phone}}",sSkyBuyPh);	

			hmTags.put("{{AirName}}",p_oOrderDetailsVO.getAirName());
			hmTags.put("{{FlightNo}}",p_oOrderDetailsVO.getFlightNo());
			hmTags.put("{{OrderId}}",p_oOrderDetailsVO.getOrderId());
			hmTags.put("{{OrderDate}}",p_oOrderDetailsVO.getOrderDt());
			hmTags.put("{{CustomerTransId}}",p_oOrderDetailsVO.getCustTransId());
			hmTags.put("{{BillingCustomerName}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustBillFname() +" "+p_oOrderDetailsVO.getCustBillLname()));
			hmTags.put("{{CustomerFName}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustBillFname()));
			hmTags.put("{{CustomerLName}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustBillLname()));
			hmTags.put("{{BillingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderDetailsVO.getCustBillPhone())));
			hmTags.put("{{BillingAddress}}",Utils.toTitleCase(mergeAddressLines(p_oOrderDetailsVO.getCustBillAddr1(), p_oOrderDetailsVO.getCustBillAddr2())));
			hmTags.put("{{BillingCity}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustBillCity()));
			hmTags.put("{{BillingState}}",p_oOrderDetailsVO.getCustBillState());
			hmTags.put("{{BillingZip}}",p_oOrderDetailsVO.getCustBillZip());
			hmTags.put("{{BillingCountry}}","US");
			hmTags.put("{{ShippingCustomerName}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustShipFname()+" "+p_oOrderDetailsVO.getCustShipLname()));
			hmTags.put("{{ShippingPhone}}",AirlineDAO.getPhoneFormat((p_oOrderDetailsVO.getCustShipPhone())));
			hmTags.put("{{ShippingMail}}",p_oOrderDetailsVO.getCustShipEmail());
			hmTags.put("{{ShippingAddress}}",Utils.toTitleCase(mergeAddressLines(p_oOrderDetailsVO.getCustShipAddr1(), p_oOrderDetailsVO.getCustShipAddr2())));
			hmTags.put("{{ShippingCity}}",Utils.toTitleCase(p_oOrderDetailsVO.getCustShipCity()));
			hmTags.put("{{ShippingState}}",p_oOrderDetailsVO.getCustShipState());
			hmTags.put("{{ShippingZip}}",p_oOrderDetailsVO.getCustShipZip());
			hmTags.put("{{ShippingCountry}}","US");
			hmTags.put("{{BillingMail}}",p_oOrderDetailsVO.getCustShipEmail());
			hmTags.put("{{CCNo}}",p_oOrderDetailsVO.getCCNoLFD());
			hmTags.put("{{ExpDate}}",OrderDAO.decryptInputData(p_oOrderDetailsVO.getExpDt(),"CLIENT",p_oOrderDetailsVO.getKeyRefId()));
			String sCardType = OrderDAO.decryptInputData(p_oOrderDetailsVO.getCardType(),"CLIENT",p_oOrderDetailsVO.getKeyRefId());
			
			if("VC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","Visa Card");
			}else if("MC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","Master Card");
			}else if("AC".equalsIgnoreCase(sCardType)){
				hmTags.put("{{CardType}}","American Express");
			}
			
			if(oEmailVO!=null){
				sEmailSubject=oEmailVO.getEmailSubject();
				sEmailContent=oEmailVO.getEmailContent();
				sEmailOrderDetails = oEmailVO.getEmailOptionalContent();
				sCcAddr=oEmailVO.getEmailCcList();
				sBccAddr=oEmailVO.getEmailBccList();
			}
			
			String CCFName = "";
			if(p_oOrderDetailsVO.getCardFname() != null && p_oOrderDetailsVO.getCardFname().trim().length() > 0)
				CCFName = OrderDAO.decryptInputData(p_oOrderDetailsVO.getCardFname(),"CLIENT",p_oOrderDetailsVO.getKeyRefId());
			else 
				CCFName = "";
			
			hmTags.put("{{CCFName}}",Utils.toTitleCase(CCFName));
			double dQty=  0;
			double dSbhPrice= 0;
			for(OrderItemsVO oOrderItemsVO:alOrderItemsVO) {
				dQty=  0;
				dSbhPrice= 0;
				sItemDetails ="";
				if(oOrderItemsVO.getQty()!=null && oOrderItemsVO.getQty().trim().length() > 0)
					dQty=Double.parseDouble(oOrderItemsVO.getQty());
				
				
				
				if(oOrderItemsVO.getSbhPrice() != null && oOrderItemsVO.getSbhPrice().trim().length() > 0)
					dSbhPrice= Double.parseDouble(oOrderItemsVO.getSbhPrice());
				
				OrderItemDetailsVO oOrderItemDetailsVO = OrderDAO.getProductDetails(oOrderItemsVO.getProdId());
				oOrderItemDetailsVO.setItemName(oOrderItemDetailsVO.getItemName()==null?"":oOrderItemDetailsVO.getItemName().trim());
				oOrderItemDetailsVO.setBrandName(oOrderItemDetailsVO.getBrandName()==null?"":oOrderItemDetailsVO.getBrandName().trim());
				if("VENDOR".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					sItemDetails = sItemDetails + "<tr><td width=36% align=left nowrap=nowrap><b>Brand Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getBrandName()+"</td></tr>";
					 
				}
				sItemDetails = sItemDetails + "<tr><td align=left width=36% nowrap=nowrap><b>Item Name</b></td><td align=left width=5%><b>:</b></td><td align=left width=59%>"+oOrderItemDetailsVO.getItemName()+"</td></tr>";
				sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Item Code</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemDetailsVO.getProdCode()+"</td></tr>";
				if("VENDOR".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					if(oOrderItemsVO.getProdSize()!= null && oOrderItemsVO.getProdSize().trim().length()>0)
							sItemDetails = sItemDetails + "<tr><td align=left nowrap=nowrap><b>Size</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemsVO.getProdSize()+"</td></tr>";
					if(oOrderItemsVO.getProdColor()!= null && oOrderItemsVO.getProdColor().trim().length()>0)
						sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Color</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemsVO.getProdColor()+"</td></tr>";
				}	
				if("AIRLINE".equalsIgnoreCase(oOrderItemDetailsVO.getOwnerType())){
					sItemDetails = sItemDetails +  "<tr><td align=left nowrap=nowrap><b>Travel Date</b></td><td align=left><b>:</b></td><td align=left>"+oOrderItemsVO.getTravelDate()+"</td></tr>";
				}	
			
				hmTags.put("{{OrderItemId}}", oOrderItemsVO.getOrderItemId());
				sItemDetails = sItemDetails == null?"":sItemDetails.trim();
				hmTags.put("{{ItemDetails}}",sItemDetails);
				
				hmTags.put("{{Category}}",oOrderItemsVO.getCateId());
				hmTags.put("{{Status}}","Failed");
				hmTags.put("{{Qty}}",oOrderItemsVO.getQty());
				hmTags.put("{{SkyBuyPrice}}",numberFormat.format(Double.parseDouble(oOrderItemsVO.getSbhPrice())));
				hmTags.put("{{SkyBuyPriceAmount}}",numberFormat.format(dQty*dSbhPrice));
				sOrderDetails=sOrderDetails+OrderDAO.replaceTagValues(hmTags,sEmailOrderDetails,null,null);	
				dSkyBuyPriceTotal = dSkyBuyPriceTotal+dQty*dSbhPrice;
				

			}
			
			hmTags.put("{{SkyBuyPriceTotal}}",numberFormat.format(dSkyBuyPriceTotal));			
			hmTags.put("{{OrderDetails}}",sOrderDetails);
			
			

			
			sEmailSubject=sEmailSubject==null?"":sEmailSubject.trim();
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();

			sEmailContent=OrderDAO.replaceTagValues(hmTags,sEmailContent,null,null);
			sEmailContent=sEmailContent==null?"":sEmailContent.trim();


			EmailLogVO oEmailLogVo=new EmailLogVO();
			Email oEmail=new Email();
			oEmailLogVo.setEmailFrom(sFromEmailAddr);
			
			sToAddr=p_oOrderDetailsVO.getCustBillEmail();
			sToAddr=sToAddr==null?"":sToAddr.trim();
			
			oEmailLogVo.setEmailTo(oEmail.convertToStringArray(sToAddr));

			oEmailLogVo.setEmailCc(oEmail.convertToStringArray(sCcAddr));
			if(sBccAddr!=null && sBccAddr.trim().length()>0){
				sBccAddr =sAdminEmailAddr+","+sBccAddr;						
			}else{
				sBccAddr =sAdminEmailAddr;
			}
			
			oEmailLogVo.setEmailBcc(oEmail.convertToStringArray(sBccAddr));
			oEmailLogVo.setEmailSubject(sEmailSubject);
			oEmailLogVo.setEmailText(sEmailContent);
			try {
				bEmailSent = oEmail.sendEmail(oEmailLogVo);
			}catch(Exception e) {
				e.printStackTrace();
				bEmailSent=false;
			}
			if(bEmailSent)				
				oEmailLogVo.setEmailStatus("Y");
			else
				oEmailLogVo.setEmailStatus("N");				
			/*oEmailLogVo.setOwnerType("customer");		
			oEmailLogVo.setOwnerId(Long.parseLong(oOrderItemDetailsVO.getCustId()));*/
			oEmailLogVo.setEmailTemplateId(sEmailTemplateId);
			oEmailLogVo.setEmailToAddr(sToAddr);
			oEmailLogVo.setEmailBccAddr(sAdminEmailAddr);

			/** Insert Email Log Details **/
			EmailDAO.insertEmailLogDetails(oEmailLogVo,p_sRemoteHost+"@"+p_sDeviceId,p_sDeviceId,"");
			System.out.print("EmailContent "+sEmailContent);

		}catch(Exception e){		
			e.printStackTrace();
			bEmailSent=false;
			logger.error("OrderDAO:sendPlaceOrderFailureEmailToCustomer:Exception "+e.getMessage());
			throw e;
		}
		
		logger.info("OrderDAO::sendPlaceOrderFailureEmailToCustomer:EXIT");
		return bEmailSent;
	}
	
	public static String dateFormat(Date p_oDateObj){
		SimpleDateFormat oFormat = new SimpleDateFormat("MM/dd/yyyy");
		String sFormattedDate = oFormat.format(p_oDateObj);
		//// System.out.println("sFormattedDate "+sFormattedDate);
		return sFormattedDate;
	}
}
