package com.sbh.client.util;

import static com.sbh.util.Utils.decrypt;
import static com.sbh.util.Utils.getPrivateKeyFromString;

import java.io.CharArrayReader;
import java.io.Reader;
import java.security.PrivateKey;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sbh.client.vo.AirlineAdvtVO;
import com.sbh.client.vo.OrderDetailsVO;
import com.sbh.client.vo.OrderItemsVO;
import com.sbh.dao.OrderDAO;
import com.sbh.util.Utils;
import com.sbh.vo.KeyVO;



public class PlaceOrderUtil {

	private static Logger logger = LogManager.getLogger(PlaceOrderUtil.class);


	//This method parse the PlaceOrder.xml File
	public static Document initXML(String p_sOrderDetails) throws Exception {
		logger.info("PlaceOrderUtil::initXML:ENTER");
		
//		DocumentBuilderFactory docBuilderFactory = null;
		Document document = null;
//		DocumentBuilder docBuilder =null;
		try{
			javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
			javax.xml.parsers.DocumentBuilder db = factory.newDocumentBuilder();
			//Parse the document
			Reader reader=new CharArrayReader(p_sOrderDetails.toCharArray());
			document = db.parse(new org.xml.sax.InputSource(reader));
			logger.info("PlaceOrderUtil::initXML:EXIT");
		}catch(Exception e){
			logger.error("PlaceOrderUtil::initXML:Exception "+e.getMessage());
			logger.error("PlaceOrderUtil::initXML:Aborted");			
		}	
		return document;
	}	


	public static OrderDetailsVO parseOrderInfo(Document doc) throws Exception{
		logger.error("PlaceOrderUtil::parseOrderInfo:ENTER");
		
		String sDecryptData = null;
		String sChecksumRefIdName=null;
		Connection con=null;		
		CallableStatement cstmt=null;
		KeyVO oKeyVO = null;
		OrderDetailsVO oOrderDetailsVO = null;
		OrderItemsVO oOrderItemsVO =null;
		PrivateKey oPrivateKey = null;
		ResultSet rs=null;
		List<OrderItemsVO> alOrderItemsInfo = null;
		String originatedFrom=null;
//		Date date = null;
//		DateFormat oDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss"); 
//		DateFormat oDateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
//		SimpleDateFormat oSdfFormat = new SimpleDateFormat("MM/d/yyyy h:mm:ss a");
//		SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat("MM/d/yyyy");
		try {	
			
//			oPrivateKey = getPrivateKeyFromFile("C:/SSL_BACKUP/RSA/rsaprivpk8.der");
			
			doc.getDocumentElement ().normalize ();

			NodeList nlListOrderInfo = doc.getElementsByTagName("OrderRecordSet");
			
			for(int s=0; s<nlListOrderInfo.getLength() ; s++){

				oOrderDetailsVO = new OrderDetailsVO();
				Node firstNode = nlListOrderInfo.item(s);
				if(firstNode.getNodeType() == Node.ELEMENT_NODE){
					Element firstElement = (Element)firstNode;
					originatedFrom =firstElement.getAttribute("originatedFrom");
					oOrderDetailsVO.setOriginatedFrom(originatedFrom);
					//------ GETTING Check Sum(PrivateKey, PublicKey) Ref Id
					NodeList nlChecksumRefIdList = firstElement.getElementsByTagName("CheckSumRefID");
					Element nlChecksumRefIdElement = (Element)nlChecksumRefIdList.item(0);
					CharacterData cChecksumRefIdName=(CharacterData)nlChecksumRefIdElement.getFirstChild();
					if(cChecksumRefIdName!=null){
						sChecksumRefIdName=cChecksumRefIdName.getData();
						if(sChecksumRefIdName!=null){
//							sDecryptData = decrypt(sChecksumRefIdName, oPrivateKey);
							oOrderDetailsVO.setKeyRefId(sChecksumRefIdName);
							//System.out.println("sCustTransId : " + sDecryptData);
						}	
					}
					oKeyVO = OrderDAO.getPrivateKey("CLIENT", sChecksumRefIdName);
					if(oKeyVO != null)
						oPrivateKey = getPrivateKeyFromString(oKeyVO.getPrivateKey());
					//------ GETTING Cust Trans Id
					NodeList nlCustTransIdList = firstElement.getElementsByTagName("CustTransId");
					Element nlCustTransIdElement = (Element)nlCustTransIdList.item(0);
					CharacterData cCustTransIdName=(CharacterData)nlCustTransIdElement.getFirstChild();
					if(cCustTransIdName!=null){
						String sCustTransIdName=cCustTransIdName.getData();
						if(sCustTransIdName!=null){
							sDecryptData = decrypt(sCustTransIdName, oPrivateKey);
							oOrderDetailsVO.setCustTransId(sDecryptData);
							//System.out.println("sCustTransId : " + sDecryptData);
						}	
					}                

					//------ GETTING CustBillEmail
					NodeList nlCustBillEmailNameList = firstElement.getElementsByTagName("CustBillEmail");
					Element nlCustBillEmailElement = (Element)nlCustBillEmailNameList.item(0);
					CharacterData cCustBillEmailName=(CharacterData)nlCustBillEmailElement.getFirstChild();
					if(cCustBillEmailName!=null){
						String sCustBillEmail=cCustBillEmailName.getData().trim();
						if(sCustBillEmail!=null){
							sDecryptData = decrypt(sCustBillEmail, oPrivateKey);
							oOrderDetailsVO.setCustBillEmail(sDecryptData);
							// System.out.println("Cust Bill Email : " + sDecryptData);
						}
					}

					//------ GETTING CustBillFname
					NodeList nlCustBillFnameNameList = firstElement.getElementsByTagName("CustBillFname");
					Element nlCustBillFnameElement = (Element)nlCustBillFnameNameList.item(0);
					CharacterData cCustBillFnameName=(CharacterData)nlCustBillFnameElement.getFirstChild();
					if(cCustBillFnameName!=null){
						String sCustBillFname=cCustBillFnameName.getData().trim();
						if(sCustBillFname!=null){
							sDecryptData = decrypt(sCustBillFname, oPrivateKey);
							oOrderDetailsVO.setCustBillFname(sDecryptData);
							// System.out.println("Cust Bill Fname : " + sDecryptData);
						}
					}

					//------ GETTING Cust Bill Lname
					NodeList nlCustBillLnameNameList = firstElement.getElementsByTagName("CustBillLname");
					Element nlCustBillLnameElement = (Element)nlCustBillLnameNameList.item(0);
					CharacterData cCustBillLnameName=(CharacterData)nlCustBillLnameElement.getFirstChild();
					if(cCustBillLnameName!=null){
						String sCustBillLname=cCustBillLnameName.getData().trim();
						if(sCustBillLname!=null){
							sDecryptData = decrypt(sCustBillLname, oPrivateKey);
							oOrderDetailsVO.setCustBillLname(sDecryptData);
							// System.out.println("sCust Bill Lname : " + sDecryptData);
						}
					}

					//------ GETTING Cust Bill Phone
					NodeList nlCustPhoneNameList = firstElement.getElementsByTagName("CustBillPhone");
					Element nlCustPhoneElement = (Element)nlCustPhoneNameList.item(0);
					CharacterData cCustPhoneName=(CharacterData)nlCustPhoneElement.getFirstChild();
					if(cCustPhoneName!=null){
						String sCustPhone=cCustPhoneName.getData().trim();
						if(sCustPhone!=null){
							sDecryptData = decrypt(sCustPhone, oPrivateKey);
							oOrderDetailsVO.setCustBillPhone(sDecryptData);
							// System.out.println("Cust Bill Phone : " + sDecryptData);
						}
					}  

					//------ GETTING Cust Bill Address1
					NodeList nlCustBillAddr1NameList = firstElement.getElementsByTagName("CustBillAddr1");
					Element nlCustBillAddr1Element = (Element)nlCustBillAddr1NameList.item(0);
					CharacterData cCustBillAddr1Name=(CharacterData)nlCustBillAddr1Element.getFirstChild();
					if(cCustBillAddr1Name!=null){
						String sCustBillAddr1=cCustBillAddr1Name.getData().trim();
						if(sCustBillAddr1!=null){
							sDecryptData = decrypt(sCustBillAddr1, oPrivateKey);
							oOrderDetailsVO.setCustBillAddr1(sDecryptData);
							// System.out.println("Cust Bill Addr1 : " + sDecryptData);
						}
					}  

					//------ GETTING Cust Bill Address2
					NodeList nlCustBillAddr2NameList = firstElement.getElementsByTagName("CustBillAddr2");
					Element nlCustBillAddr2Element = (Element)nlCustBillAddr2NameList.item(0);
					CharacterData cCustBillAddr2Name=(CharacterData)nlCustBillAddr2Element.getFirstChild();
					if(cCustBillAddr2Name!=null){
						String sCustBillAddr2=cCustBillAddr2Name.getData().trim();
						if(sCustBillAddr2!=null){
							sDecryptData = decrypt(sCustBillAddr2, oPrivateKey);
							oOrderDetailsVO.setCustBillAddr2(sDecryptData);
							// System.out.println("Cust Bill Addr2 : " + sDecryptData);
						}
					}  

					//------ GETTING Cust Bill City
					NodeList nlCustBillCityNameList = firstElement.getElementsByTagName("CustBillCity");
					Element nlCustBillCityElement = (Element)nlCustBillCityNameList.item(0);
					CharacterData cCustBillCityName=(CharacterData)nlCustBillCityElement.getFirstChild();
					if(cCustBillCityName!=null){
						String sCustBillCity =cCustBillCityName.getData().trim();
						if(sCustBillCity!=null){
							sDecryptData = decrypt(sCustBillCity, oPrivateKey);
							oOrderDetailsVO.setCustBillCity(sDecryptData);
							// System.out.println("Cust Bill City: " + sDecryptData);
						}
					}  

					//------ GETTING Cust Bill State
					NodeList nlCustBillStateNameList = firstElement.getElementsByTagName("CustBillState");
					Element nlCustBillStateElement = (Element)nlCustBillStateNameList.item(0);
					CharacterData cCustBillStateName=(CharacterData)nlCustBillStateElement.getFirstChild();
					if(cCustBillStateName!=null){
						String sCustBillState =cCustBillStateName.getData().trim();
						if(sCustBillState!=null){
							sDecryptData = decrypt(sCustBillState, oPrivateKey);
							oOrderDetailsVO.setCustBillState(sDecryptData);
							// System.out.println("Cust Bill State: " + sDecryptData);
						}
					}  

					//------ GETTING Cust Bill Zip
					NodeList nlCustBillZipNameList = firstElement.getElementsByTagName("CustBillZip");
					Element nlCustBillZipElement = (Element)nlCustBillZipNameList.item(0);
					CharacterData cCustBillZipName=(CharacterData)nlCustBillZipElement.getFirstChild();
					if(cCustBillZipName!=null){
						String sCustBillZip =cCustBillZipName.getData().trim();
						if(sCustBillZip!=null){
							sDecryptData = decrypt(sCustBillZip, oPrivateKey);
							oOrderDetailsVO.setCustBillZip(sDecryptData);
							// System.out.println("Cust Bill Zip: " + sDecryptData);
						}
					}  

					//------ GETTING Cust Ship Fname
					NodeList nlCustShipFnameNameList = firstElement.getElementsByTagName("CustShipFname");
					Element nlCustShipFnameElement = (Element)nlCustShipFnameNameList.item(0);
					CharacterData cCustShipFnameName=(CharacterData)nlCustShipFnameElement.getFirstChild();
					if(cCustShipFnameName!=null){
						String sCustShipFname =cCustShipFnameName.getData().trim();
						if(sCustShipFname!=null){
							sDecryptData = decrypt(sCustShipFname, oPrivateKey);
							oOrderDetailsVO.setCustShipFname(sDecryptData);
							// System.out.println("Cust Ship Fname: " + sDecryptData);
						}
					} 

					//------ GETTING Cust Ship Lname
					NodeList nlCustShipLnameNameList = firstElement.getElementsByTagName("CustShipLname");
					Element nlCustShipLnameElement = (Element)nlCustShipLnameNameList.item(0);
					CharacterData cCustShipLnameName=(CharacterData)nlCustShipLnameElement.getFirstChild();
					if(cCustShipLnameName!=null){
						String sCustShipLname =cCustShipLnameName.getData().trim();
						if(sCustShipLname!=null){
							sDecryptData = decrypt(sCustShipLname, oPrivateKey);
							oOrderDetailsVO.setCustShipLname(sDecryptData);
							// System.out.println("Cust Ship Lname: " + sDecryptData);
						}
					}  

					//------ GETTING CustShipAddr1
					NodeList nlCustShipAddr1NameList = firstElement.getElementsByTagName("CustShipAddr1");
					Element nlCustShipAddr1Element = (Element)nlCustShipAddr1NameList.item(0);
					CharacterData cCustShipAddr1Name=(CharacterData)nlCustShipAddr1Element.getFirstChild();
					if(cCustShipAddr1Name!=null){
						String sCustShipAddr1 =cCustShipAddr1Name.getData().trim();
						if(sCustShipAddr1!=null){
							sDecryptData = decrypt(sCustShipAddr1, oPrivateKey);
							oOrderDetailsVO.setCustShipAddr1(sDecryptData);
							// System.out.println("Cust Ship Addr1: " + sDecryptData);
						}
					}  

					//------ GETTING CustShipAddr2
					NodeList nlCustShipAddr2NameList = firstElement.getElementsByTagName("CustShipAddr2");
					Element nlCustShipAddr2Element = (Element)nlCustShipAddr2NameList.item(0);
					CharacterData cCustShipAddr2Name=(CharacterData)nlCustShipAddr2Element.getFirstChild();
					if(cCustShipAddr2Name!=null){
						String sCustShipAddr2 =cCustShipAddr2Name.getData().trim();
						if(sCustShipAddr2!=null){
							sDecryptData = decrypt(sCustShipAddr2, oPrivateKey);
							oOrderDetailsVO.setCustShipAddr2(sDecryptData);
							// System.out.println("Cust Ship Addr2: " + sDecryptData);
						}
					} 

					//------ GETTING Cust Ship Phone
					NodeList nlCustShipPhoneNameList = firstElement.getElementsByTagName("CustShipPhone");
					Element nlCustShipPhoneElement = (Element)nlCustShipPhoneNameList.item(0);
					CharacterData cCustShipPhoneName=(CharacterData)nlCustShipPhoneElement.getFirstChild();
					if(cCustShipPhoneName!=null){
						String sCustShipPhone=cCustShipPhoneName.getData().trim();
						if(sCustShipPhone!=null){
							sDecryptData = decrypt(sCustShipPhone, oPrivateKey);
							oOrderDetailsVO.setCustShipPhone(sDecryptData);
							// System.out.println("Cust Ship Phone : " + sDecryptData);
						}
					}

					//------ GETTING Cust Ship City
					NodeList nlCustShipCityNameList = firstElement.getElementsByTagName("CustShipCity");
					Element nlCustShipCityElement = (Element)nlCustShipCityNameList.item(0);
					CharacterData cCustShipCityName=(CharacterData)nlCustShipCityElement.getFirstChild();
					if(cCustShipCityName!=null){
						String sCustShipCity =cCustShipCityName.getData().trim();
						if(sCustShipCity!=null){
							sDecryptData = decrypt(sCustShipCity, oPrivateKey);
							oOrderDetailsVO.setCustShipCity(sDecryptData);
							// System.out.println("Cust Ship City: " + sDecryptData);
						}
					} 

					//------ GETTING Cust Ship State
					NodeList nlCustShipStateNameList = firstElement.getElementsByTagName("CustShipState");
					Element nlCustShipStateElement = (Element)nlCustShipStateNameList.item(0);
					CharacterData cCustShipStateName=(CharacterData)nlCustShipStateElement.getFirstChild();
					if(cCustShipStateName!=null){
						String sCustShipState =cCustShipStateName.getData().trim();
						if(sCustShipState!=null){
							sDecryptData = decrypt(sCustShipState, oPrivateKey);
							oOrderDetailsVO.setCustShipState(sDecryptData);
							// System.out.println("Cust Ship State: " + sDecryptData);
						}
					} 

					/*//------ GETTING Cust Ship Country
                NodeList nlCustShipCountryNameList = firstElement.getElementsByTagName("CustShipCountry");
                Element nlCustShipCountryElement = (Element)nlCustShipCountryNameList.item(0);
                CharacterData cCustShipCountryName=(CharacterData)nlCustShipCountryElement.getFirstChild();
                if(cCustShipCountryName!=null){
                    String sCustShipCountry =cCustShipCountryName.getData().trim();
                    if(sCustShipCountry!=null){
                    	// System.out.println("Cust Ship Country: " + sCustShipCountry);
                    }
                } */

					//------ GETTING Cust Ship Zip
					NodeList nlCustShipZipNameList = firstElement.getElementsByTagName("CustShipZip");
					Element nlCustShipZipElement = (Element)nlCustShipZipNameList.item(0);
					CharacterData cCustShipZipName=(CharacterData)nlCustShipZipElement.getFirstChild();
					if(cCustShipZipName!=null){
						String sCustShipZip =cCustShipZipName.getData().trim();
						if(sCustShipZip!=null){
							sDecryptData = decrypt(sCustShipZip, oPrivateKey);
							oOrderDetailsVO.setCustShipZip(sDecryptData);
							// System.out.println("Cust Ship Zip: " + sDecryptData);
						}
					} 

					//------ GETTING Cust Ship Email
					NodeList nlCustShipEmailNameList = firstElement.getElementsByTagName("CustShipEmail");
					Element nlCustShipEmailElement = (Element)nlCustShipEmailNameList.item(0);
					CharacterData cCustShipEmailName=(CharacterData)nlCustShipEmailElement.getFirstChild();
					if(cCustShipEmailName!=null){
						String sCustShipEmail =cCustShipEmailName.getData().trim();
						if(sCustShipEmail!=null){
							sDecryptData = decrypt(sCustShipEmail, oPrivateKey);
							oOrderDetailsVO.setCustShipEmail(sDecryptData);
							// System.out.println("Cust Ship Email: " + sDecryptData);
						}
					} 
					
					//------ GETTING Cust Offers
					NodeList nlCustOfferList = firstElement.getElementsByTagName("NewServicesFeedback");
					Element nlCustOfferElement = (Element)nlCustOfferList.item(0);
					CharacterData cCustOffer=(CharacterData)nlCustOfferElement.getFirstChild();
					if(cCustOffer!=null){
						String sCustOffer =cCustOffer.getData();
						sCustOffer = sCustOffer ==null?"":sCustOffer.trim();
						if(sCustOffer!=null){
//							sDecryptData = decrypt(sCustOffer, oPrivateKey);
							oOrderDetailsVO.setCustOffer(sCustOffer);
							// System.out.println("Customer Offer: " + sCustOffer);
						}
					} 
					
					//------ GETTING Cust Idea
					NodeList nlCustIdeaList = firstElement.getElementsByTagName("Suggestion");
					Element nlCustIdeaElement = (Element)nlCustIdeaList.item(0);
					CharacterData cCustIdea=(CharacterData)nlCustIdeaElement.getFirstChild();
					if(cCustIdea!=null){
						String sCustIdea =cCustIdea.getData().trim();
						sCustIdea = sCustIdea == null?"":sCustIdea.trim();
						if(sCustIdea!=null){
//							sDecryptData = decrypt(sCustIdea, oPrivateKey);
							oOrderDetailsVO.setCustIdea(sCustIdea);
							// System.out.println("Customer Idea: " + sCustIdea);
						}
					} 
					
					//------ GETTING Cust Special Instruction
					NodeList nlSpecialInstructionList = firstElement.getElementsByTagName("SpecialInstruction");
					Element nlSpecialInstructionElement = (Element)nlSpecialInstructionList.item(0);
					CharacterData cSpecialInstruction=(CharacterData)nlSpecialInstructionElement.getFirstChild();
					if(cSpecialInstruction!=null){
						String sSpecialInstruction =cSpecialInstruction.getData().trim();
						sSpecialInstruction = sSpecialInstruction == null?"":sSpecialInstruction.trim();
						if(sSpecialInstruction!=null){
//							sDecryptData = decrypt(sSpecialInstruction, oPrivateKey);
							oOrderDetailsVO.setSpecialInstruction(sSpecialInstruction);
							// System.out.println("Customer Special Instruction: " + sSpecialInstruction);
						}
					} 

					//------ GETTING Cust Personal Information
					NodeList nlPersonalInformationList = firstElement.getElementsByTagName("PersonalInformation");
					Element nlPersonalInformationElement = (Element)nlPersonalInformationList.item(0);
					CharacterData cPersonalInformation=(CharacterData)nlPersonalInformationElement.getFirstChild();
					if(cPersonalInformation!=null){
						String sPersonalInformation =cPersonalInformation.getData().trim();
						sPersonalInformation = sPersonalInformation == null?"":sPersonalInformation.trim();
						if(sPersonalInformation!=null){
//							sDecryptData = decrypt(sPersonalInformation, oPrivateKey);
							oOrderDetailsVO.setPersonalInformation(sPersonalInformation);
							// System.out.println("Customer Personal Information: " + sPersonalInformation);
						}
					} 

					//------ GETTING Order Dt
					NodeList nlOrderDtNameList = firstElement.getElementsByTagName("OrderDt");
					Element nlOrderDtElement = (Element)nlOrderDtNameList.item(0);
					CharacterData cOrderDtName=(CharacterData)nlOrderDtElement.getFirstChild();
					if(cOrderDtName!=null){
						String sOrderDt =cOrderDtName.getData().trim();
						if(sOrderDt!=null){
							//date = (Date)oDateTimeFormat.parse(sOrderDt);    
							//oOrderDetailsVO.setOrderDt(oSdfFormat.format(date));
//							sDecryptData = decrypt(sOrderDt, oPrivateKey);
							oOrderDetailsVO.setOrderDt(sOrderDt);
							// System.out.println("Order Dt: " + oOrderDetailsVO.getOrderDt());
						}
					} 				
	
					
					//------ GETTING Processor Id
					NodeList nlProcessorIdNameList = firstElement.getElementsByTagName("ProcessorId");
					Element nlProcessorIdElement = (Element)nlProcessorIdNameList.item(0);
					CharacterData cProcessorIdName=(CharacterData)nlProcessorIdElement.getFirstChild();
					if(cProcessorIdName!=null){
						String sProcessorId =cProcessorIdName.getData().trim();
						if(sProcessorId!=null){
//							sDecryptData = decrypt(sProcessorId, oPrivateKey);
							oOrderDetailsVO.setProcessorId(sProcessorId);
							// System.out.println("Processor Id: " + sProcessorId);
						}
					}
					//------ GETTING Airline Id
					NodeList nlAirIdNameList = firstElement.getElementsByTagName("AirId");
					Element nlAirIdElement = (Element)nlAirIdNameList.item(0);
					CharacterData cAirId=(CharacterData)nlAirIdElement.getFirstChild();
					if(cAirId!=null){
						String sAirId =cAirId.getData().trim();
						if(sAirId!=null){
//							sDecryptData = decrypt(sAirId, oPrivateKey);
							oOrderDetailsVO.setAirId(sAirId);
							// System.out.println("Air Id: " + sAirId);
						}
					}
					
					//------ GETTING Device Id
					NodeList nlDeviceIdNameList = firstElement.getElementsByTagName("DeviceId");
					Element nlDeviceIdElement = (Element)nlDeviceIdNameList.item(0);
					CharacterData cDeviceId=(CharacterData)nlDeviceIdElement.getFirstChild();
					if(cDeviceId!=null){
						String sDeviceId =cDeviceId.getData().trim();
						if(sDeviceId!=null){
//							sDecryptData = decrypt(sDeviceId, oPrivateKey);
							oOrderDetailsVO.setDeviceId(sDeviceId);
							// System.out.println("Device Id: " + sDeviceId);
						}
					}
					
					//------ GETTING Flight No
					NodeList nlFlightNoList = firstElement.getElementsByTagName("FlightNo");
					Element nlFlightNoElement = (Element)nlFlightNoList.item(0);
					CharacterData cFlightNo=(CharacterData)nlFlightNoElement.getFirstChild();
					if(cFlightNo!=null){
						String sFlightNo =cFlightNo.getData().trim();
						if(sFlightNo!=null){
//							sDecryptData = decrypt(sFlightNo, oPrivateKey);
							oOrderDetailsVO.setFlightNo(sFlightNo);
							// System.out.println("Flight No: " + sFlightNo);
						}
					}
					
					

					//------ GETTING Mac Addr
					NodeList nlMacAddrNameList = firstElement.getElementsByTagName("MacAddr");
					Element nlMacAddrElement = (Element)nlMacAddrNameList.item(0);
					CharacterData cMacAddrName=(CharacterData)nlMacAddrElement.getFirstChild();
					if(cMacAddrName!=null){
						String sMacAddr =cMacAddrName.getData().trim();
						if(sMacAddr!=null){
//							sDecryptData = decrypt(sMacAddr, oPrivateKey);
							oOrderDetailsVO.setMacAddr(sMacAddr);
							// System.out.println("Mac Addr: " + sMacAddr);
						}
					}
					//------ GETTING Device Password
					/*NodeList nlDevicePasswordNameList = firstElement.getElementsByTagName("DevicePassword");
					Element nlDevicePasswordElement = (Element)nlDevicePasswordNameList.item(0);
					CharacterData cDevicePasswordName=(CharacterData)nlDevicePasswordElement.getFirstChild();
					if(cDevicePasswordName!=null){
						String sDevicePassword =cDevicePasswordName.getData().trim();
						if(sDevicePassword!=null){
//							sDecryptData = decrypt(sDevicePassword, oPrivateKey);
							oOrderDetailsVO.setDevicePassword(sDevicePassword);
							// System.out.println("Device Password: " + sDevicePassword);
						}
					}*/

					//------ GETTING CC No
					NodeList nlCCNoNameList = firstElement.getElementsByTagName("PaymentNumber");
					Element nlCCNoElement = (Element)nlCCNoNameList.item(0);
					CharacterData cCCNoName=(CharacterData)nlCCNoElement.getFirstChild();
					if(cCCNoName!=null){
						String sCCNo =cCCNoName.getData().trim();
						if(sCCNo!=null){
							oOrderDetailsVO.setCCNo(sCCNo);
							sDecryptData = decrypt(sCCNo, oPrivateKey);
							oOrderDetailsVO.setDecryptCCNo(sDecryptData);
							// System.out.println("CC No: " + sCCNo);
						}
					}
					
					//------ GETTING CVV No
					NodeList nlCVVNoNameList = firstElement.getElementsByTagName("PaymentCode");
					Element nlCVVNoElement = (Element)nlCVVNoNameList.item(0);
					CharacterData cCVVNoName=(CharacterData)nlCVVNoElement.getFirstChild();
					if(cCVVNoName!=null){
						String sCVVNo =cCVVNoName.getData().trim();
						if(sCVVNo!=null){
							oOrderDetailsVO.setCvv(sCVVNo);
							sDecryptData = decrypt(sCVVNo, oPrivateKey);
							oOrderDetailsVO.setDecryptCVVNo(sDecryptData);
							// System.out.println("CVV No: " + sCVVNo);
						}
					}
					
					//------ GETTING Last four digit of CC No
					NodeList nlLFDList = firstElement.getElementsByTagName("LFD");
					Element nlLFDElement = (Element)nlLFDList.item(0);
					CharacterData cCCNoLFDNo=(CharacterData)nlLFDElement.getFirstChild();
					if(cCCNoLFDNo!=null){
						String sCCNoLFD =cCCNoLFDNo.getData().trim();
						if(sCCNoLFD!=null){
							sDecryptData = decrypt(sCCNoLFD, oPrivateKey);
							oOrderDetailsVO.setCCNoLFD(sDecryptData);
							// System.out.println("LFD...."+sDecryptData);
						}
					}
					
					//------ GETTING Card Fname
					NodeList nlCardFnameNameList = firstElement.getElementsByTagName("PaymentName");
					Element nlCardFnameElement = (Element)nlCardFnameNameList.item(0);
					CharacterData cCardFnameName=(CharacterData)nlCardFnameElement.getFirstChild();
					if(cCardFnameName!=null){
						String sCardFname =cCardFnameName.getData().trim();
						if(sCardFname!=null){
							oOrderDetailsVO.setCardFname(sCardFname);
							// System.out.println("Card Fname: " + sCardFname);
						}
					}

					//------ GETTING CardType
					NodeList nlCardTypeNameList = firstElement.getElementsByTagName("PaymentType");
					Element nlCardTypeElement = (Element)nlCardTypeNameList.item(0);
					CharacterData cCardTypeName=(CharacterData)nlCardTypeElement.getFirstChild();
					if(cCardTypeName!=null){
						String sCardType =cCardTypeName.getData().trim();
						if(sCardType!=null){
							oOrderDetailsVO.setCardType(sCardType);
							// System.out.println("Card Type: " + sCardType);
							sDecryptData = decrypt(sCardType, oPrivateKey);
							oOrderDetailsVO.setDecryptedCardType(sDecryptData);
						}
					}

					/*//------ GETTING Card Bank Code
					NodeList nlCardBankCodeNameList = firstElement.getElementsByTagName("CardBankCode");
					Element nlCardBankCodeElement = (Element)nlCardBankCodeNameList.item(0);
					CharacterData cCardBankCodeName=(CharacterData)nlCardBankCodeElement.getFirstChild();
					if(cCardBankCodeName!=null){
						String sCardBankCode =cCardBankCodeName.getData().trim();
						if(sCardBankCode!=null){
							oOrderDetailsVO.setCardBankCode(sCardBankCode);
							// System.out.println("Card Bank Code: " + sCardBankCode);
						}
					}*/

					//------ GETTING Exp Dt
					NodeList nlExpDtNameList = firstElement.getElementsByTagName("PaymentExpDt");
					Element nlExpDtElement = (Element)nlExpDtNameList.item(0);
					CharacterData cExpDtName=(CharacterData)nlExpDtElement.getFirstChild();
					if(cExpDtName!=null){
						String sExpDt =cExpDtName.getData().trim();
						if(sExpDt!=null){
							//date = (Date)oDateFormat.parse(sExpDt);    
							oOrderDetailsVO.setExpDt(sExpDt);
							// System.out.println("Exp Dt: " + oOrderDetailsVO.getExpDt());
						}
					}
					
					//------ GETTING Product Key
					NodeList nlProdKeyList = firstElement.getElementsByTagName("DeviceKey");
					Element nlProdKeyElement = (Element)nlProdKeyList.item(0);
					CharacterData cProdKeyName=(CharacterData)nlProdKeyElement.getFirstChild();
					if(cProdKeyName!=null){
						String sProdKey =cProdKeyName.getData().trim();
						if(sProdKey!=null){
//							sDecryptData = decrypt(sProdKey, oPrivateKey);
							oOrderDetailsVO.setProductKey(sProdKey);
							// System.out.println("Product Key: " + oOrderDetailsVO.getProductKey());
						}
					}
					//------ GETTING Total Orders
					NodeList nlTotalOrderItems = firstElement.getElementsByTagName("TotalOrderItems");
					Element nlTotalOrderItemsElement = (Element)nlTotalOrderItems.item(0);
					CharacterData cTotalOrderItems=(CharacterData)nlTotalOrderItemsElement.getFirstChild();
					if(cTotalOrderItems!=null){
						String sTotalOrderItems =cTotalOrderItems.getData().trim();
						if(sTotalOrderItems!=null){
//							sDecryptData = decrypt(sProdKey, oPrivateKey);
							oOrderDetailsVO.setTotalOrderItems(sTotalOrderItems);
							// System.out.println("Total Orders: " + oOrderDetailsVO.getTotalOrderItems());
						}
					}
					//------ GETTING Total Payment
					NodeList nlTotalPayment = firstElement.getElementsByTagName("TotalPayment");
					Element nlTotalPaymentElement = (Element)nlTotalPayment.item(0);
					CharacterData cTotalPayment=(CharacterData)nlTotalPaymentElement.getFirstChild();
					if(cTotalPayment!=null){
						String sTotalPayment =cTotalPayment.getData().trim();
						if(sTotalPayment!=null){
//							sDecryptData = decrypt(sProdKey, oPrivateKey);
							oOrderDetailsVO.setTotalPayment(sTotalPayment);
							// System.out.println("TotalPayment: " + oOrderDetailsVO.getTotalPayment());
						}
					}

					//Start- Parsing Order Items details

					NodeList nlListOrderItemsInfo = doc.getElementsByTagName("Items");
					alOrderItemsInfo = new  ArrayList<OrderItemsVO>();
					for(int i=0;i<nlListOrderItemsInfo.getLength() ; i++){
						
						Node firstOrderItemNode = nlListOrderItemsInfo.item(i);
						if(firstOrderItemNode.getNodeType() == Node.ELEMENT_NODE){
							// Start -Parsing Item details
							NodeList nlListItemsInfo = doc.getElementsByTagName("Item");
							
							for(int j=0;j<nlListItemsInfo.getLength() ; j++){
								oOrderItemsVO = new OrderItemsVO();
								Node firstItemNode = nlListItemsInfo.item(j);
								if(firstItemNode.getNodeType() == Node.ELEMENT_NODE){
									
									Element firstItemElement = (Element)firstItemNode;
									
									NodeList nlVendIdNameList = firstItemElement.getElementsByTagName("OwnerId");
									Element nlVendIdElement = (Element)nlVendIdNameList.item(0);
									CharacterData cVendIdName=(CharacterData)nlVendIdElement.getFirstChild();
									if(cVendIdName!=null){
										String sVendId =cVendIdName.getData().trim();
										if(sVendId!=null){
//											sDecryptData = decrypt(sVendId, oPrivateKey);
											oOrderItemsVO.setVendId(sVendId);
											// System.out.println("Vend Id: " + sVendId);
										}
									}
									//------ GETTING Cate Id
									NodeList nlCateIdNameList = firstItemElement.getElementsByTagName("CateId");
									Element nlCateIdElement = (Element)nlCateIdNameList.item(0);
									CharacterData cCateIdName=(CharacterData)nlCateIdElement.getFirstChild();
									if(cCateIdName!=null){
										String sCateId =cCateIdName.getData().trim();
										if(sCateId!=null){
//											sDecryptData = decrypt(sCateId, oPrivateKey);
											oOrderItemsVO.setCateId(sCateId);
											// System.out.println("Cate Id: " + sCateId);
										}
									} 


									//------ GETTING Prod Id
									NodeList nlProdIdNameList = firstItemElement.getElementsByTagName("ProdId");
									Element nlProdIdElement = (Element)nlProdIdNameList.item(0);
									CharacterData cProdIdName=(CharacterData)nlProdIdElement.getFirstChild();
									if(cProdIdName!=null){
										String sProdId =cProdIdName.getData().trim();
										if(sProdId!=null){
//											sDecryptData = decrypt(sProdId, oPrivateKey);
											oOrderItemsVO.setProdId(sProdId);
											// System.out.println("Prod Id: " + sProdId);
										}
									} 


									//------ GETTING Prod Code
									NodeList nlProdCodeNameList = firstItemElement.getElementsByTagName("ProdCode");
									Element nlProdCodeElement = (Element)nlProdCodeNameList.item(0);
									CharacterData cProdCodeName=(CharacterData)nlProdCodeElement.getFirstChild();
									if(cProdCodeName!=null){
										String sProdCode =cProdCodeName.getData().trim();
										if(sProdCode!=null){
//											sDecryptData = decrypt(sProdCode, oPrivateKey);
											oOrderItemsVO.setProdCode(sProdCode);
											// System.out.println("Prod Code: " + sProdCode);
										}
									} 

									//------ GETTING Prod Color
									NodeList nlProdColorList = firstItemElement.getElementsByTagName("ProdColor");
									Element nlProdColorElement = (Element)nlProdColorList.item(0);
									CharacterData cProdColorName=(CharacterData)nlProdColorElement.getFirstChild();
									if(cProdColorName!=null){
										String sProdColor =cProdColorName.getData().trim();
										if(sProdColor!=null){
//											sDecryptData = decrypt(sProdColor, oPrivateKey);
											oOrderItemsVO.setProdColor(sProdColor);
											// System.out.println("Prod Color: " + sProdColor);
										}
									} 


									//------ GETTING Prod Size
									NodeList nlProdSizeList = firstItemElement.getElementsByTagName("ProdSize");
									Element nlProdSizeElement = (Element)nlProdSizeList.item(0);
									CharacterData cProdSizeName=(CharacterData)nlProdSizeElement.getFirstChild();
									if(cProdSizeName!=null){
										String sProdSize =cProdSizeName.getData().trim();
										if(sProdSize!=null){
//											sDecryptData = decrypt(sProdSize, oPrivateKey);
											oOrderItemsVO.setProdSize(sProdSize);
											// System.out.println("Prod Size: " + sProdSize);
										}
									} 

									//------ GETTING Qty
									NodeList nlQtyNameList = firstItemElement.getElementsByTagName("Qty");
									Element nlQtyElement = (Element)nlQtyNameList.item(0);
									CharacterData cQtyName=(CharacterData)nlQtyElement.getFirstChild();
									if(cQtyName!=null){
										String sQty =cQtyName.getData().trim();
										if(sQty!=null && sQty.trim().length()>0){
//											sDecryptData = decrypt(sQty, oPrivateKey);
											oOrderItemsVO.setQty(sQty);
											// System.out.println("Qty: " + sQty);
										}else{
											oOrderItemsVO.setQty("0");
										}
											
									} 

									//------ GETTING Vend Price
									NodeList nlVendPriceNameList = firstItemElement.getElementsByTagName("VendPrice");
									Element nlVendPriceElement = (Element)nlVendPriceNameList.item(0);
									CharacterData cVendPriceName=(CharacterData)nlVendPriceElement.getFirstChild();
									if(cVendPriceName!=null){
										String sVendPrice =cVendPriceName.getData().trim();
										if(sVendPrice!=null && sVendPrice.trim().length()>0){
//											sDecryptData = decrypt(sVendPrice, oPrivateKey);
											sVendPrice = sVendPrice.replaceAll(",", "");
											oOrderItemsVO.setVendPrice(sVendPrice);
											// System.out.println("Vend Price: " + sVendPrice);
										}else{
											oOrderItemsVO.setVendPrice("0");
										}
									} 

									//------ GETTING Sbh Price
									NodeList nlSbhPriceNameList = firstItemElement.getElementsByTagName("SbhPrice");
									Element nlSbhPriceElement = (Element)nlSbhPriceNameList.item(0);
									CharacterData cSbhPriceName=(CharacterData)nlSbhPriceElement.getFirstChild();
									if(cSbhPriceName!=null){
										String sSbhPrice =cSbhPriceName.getData().trim();
										if(sSbhPrice!=null && sSbhPrice.trim().length()>0){
//											sDecryptData = decrypt(sSbhPrice, oPrivateKey);
											sSbhPrice = sSbhPrice.replaceAll(",", "");
											oOrderItemsVO.setSbhPrice(sSbhPrice);
											// System.out.println("Sbh Price: " + sSbhPrice);
										}else{
											oOrderItemsVO.setSbhPrice("0");
										}
									} 
									//------ GETTING Travel Date
									NodeList nlTravelDateList = firstItemElement.getElementsByTagName("TravelDate");
									Element nlTravelDateElement = (Element)nlTravelDateList.item(0);
									CharacterData cTravelDateName=(CharacterData)nlTravelDateElement.getFirstChild();
									if(cTravelDateName!=null){
										String sTravelDate =cTravelDateName.getData().trim();
										if(sTravelDate!=null && sTravelDate.trim().length()>0){
//											sDecryptData = decrypt(sTravelDate, oPrivateKey);
											oOrderItemsVO.setTravelDate(sTravelDate);
											// System.out.println("Travel Date: " + sTravelDate);
										}else{
											oOrderItemsVO.setTravelDate("");
										}
									} 
								}
								alOrderItemsInfo.add(oOrderItemsVO);
							}
							//End - Parsing Item details

						}
					}
					//End - Parsing Order Items details
					
					oOrderDetailsVO.setOrderItemDetails(alOrderItemsInfo);
				}

			}
			System.out.print("PlaceOrderUtil::parseOrderInfo:Exit");
			logger.error("PlaceOrderUtil::parseOrderInfo:EXIT");
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("PlaceOrderUtil::parseOrderInfo:Exception "+e.getMessage());
			logger.error("PlaceOrderUtil::parseOrderInfo:Aborted");
			// System.out.println("PlaceOrderUtil::parseOrderInfo:Exception"+e.getMessage());
			throw new Exception("XML");
		}
		finally{
			if(rs!=null)
				rs.close();
			if(cstmt!=null)
				cstmt.close();
			if(con!=null)
				con.close();
		}
		return oOrderDetailsVO;
	}
	
	public static List<AirlineAdvtVO> parseAirlineAdvtInfo(Document doc) throws Exception{
		logger.error("PlaceOrderUtil::parseOrderInfo:ENTER");
		
//		String sDecryptData = null;
		AirlineAdvtVO oAirlineAdvtVO = null;
		List<AirlineAdvtVO> alAirlineAdvt = null;
		Connection con=null;		
		CallableStatement cstmt=null;
//		PrivateKey oPrivateKey = null;
		ResultSet rs=null;
		String sOriginatedFrom=null;
//		byte[] bytesDBData = null;
//		Date date = null;
//		DateFormat oDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss"); 
//		DateFormat oDateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
//		SimpleDateFormat oSdfFormat = new SimpleDateFormat("MM/d/yyyy h:mm:ss a");
//		SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat("MM/d/yyyy");
		try {	
			
			doc.getDocumentElement ().normalize ();

			//Start- Parsing Airline Advt details

			NodeList nlListOrderItemsInfo = doc.getElementsByTagName("AirlineRecordSet");
			alAirlineAdvt = new  ArrayList<AirlineAdvtVO>();
			for(int i=0;i<nlListOrderItemsInfo.getLength() ; i++){

				Node firstOrderItemNode = nlListOrderItemsInfo.item(i);
				if(firstOrderItemNode.getNodeType() == Node.ELEMENT_NODE){
					// Start -Parsing Item details
					NodeList nlListItemsInfo = doc.getElementsByTagName("Item");

					for(int j=0;j<nlListItemsInfo.getLength() ; j++){
						oAirlineAdvtVO = new AirlineAdvtVO();
						
						Element firstOriginatedFromElement = (Element)firstOrderItemNode;
						sOriginatedFrom =firstOriginatedFromElement.getAttribute("originatedFrom");
						oAirlineAdvtVO.setOriginatedFrom(sOriginatedFrom);
						
						Node firstItemNode = nlListItemsInfo.item(j);
						if(firstItemNode.getNodeType() == Node.ELEMENT_NODE){

							Element firstItemElement = (Element)firstItemNode;

							//------ GETTING Product Key
							NodeList nlProdKeyList = firstItemElement.getElementsByTagName("DeviceKey");
							Element nlProdKeyElement = (Element)nlProdKeyList.item(0);
							CharacterData cProdKeyName=(CharacterData)nlProdKeyElement.getFirstChild();
							if(cProdKeyName!=null){
								String sProdKey =cProdKeyName.getData().trim();
								if(sProdKey!=null){
//									sDecryptData = decrypt(sProdKey, oPrivateKey);
									oAirlineAdvtVO.setProductKey(sProdKey);
									// System.out.println("Product Key: " + oAirlineAdvtVO.getProductKey());
								}
							}

							//------ GETTING Order Dt
							NodeList nlOrderDtNameList = firstItemElement.getElementsByTagName("OrderDt");
							Element nlOrderDtElement = (Element)nlOrderDtNameList.item(0);
							CharacterData cOrderDtName=(CharacterData)nlOrderDtElement.getFirstChild();
							if(cOrderDtName!=null){
								String sOrderDt =cOrderDtName.getData().trim();
								if(sOrderDt!=null){
									//date = (Date)oDateTimeFormat.parse(sOrderDt);    
									//oOrderDetailsVO.setOrderDt(oSdfFormat.format(date));
//									sDecryptData = decrypt(sOrderDt, oPrivateKey);
									oAirlineAdvtVO.setOrderDt(sOrderDt);
									// System.out.println("Order Dt: " + oAirlineAdvtVO.getOrderDt());
								}
							} 				


							//------ GETTING Processor Id
							NodeList nlProcessorIdNameList = firstItemElement.getElementsByTagName("ProcessorId");
							Element nlProcessorIdElement = (Element)nlProcessorIdNameList.item(0);
							CharacterData cProcessorIdName=(CharacterData)nlProcessorIdElement.getFirstChild();
							if(cProcessorIdName!=null){
								String sProcessorId =cProcessorIdName.getData().trim();
								if(sProcessorId!=null){
//									sDecryptData = decrypt(sProcessorId, oPrivateKey);
									oAirlineAdvtVO.setProcessorId(sProcessorId);
									// System.out.println("Processor Id: " + sProcessorId);
								}
							}
							//------ GETTING Airline Id
									NodeList nlAirIdNameList = firstItemElement.getElementsByTagName("AirId");
									Element nlAirIdElement = (Element)nlAirIdNameList.item(0);
									CharacterData cAirId=(CharacterData)nlAirIdElement.getFirstChild();
									if(cAirId!=null){
										String sAirId =cAirId.getData().trim();
										if(sAirId!=null){
//											sDecryptData = decrypt(sAirId, oPrivateKey);
											oAirlineAdvtVO.setAirId(sAirId);
											// System.out.println("Air Id: " + sAirId);
										}
									}

							//------ GETTING Device Id
							NodeList nlDeviceIdNameList = firstItemElement.getElementsByTagName("DeviceId");
							Element nlDeviceIdElement = (Element)nlDeviceIdNameList.item(0);
							CharacterData cDeviceId=(CharacterData)nlDeviceIdElement.getFirstChild();
							if(cDeviceId!=null){
								String sDeviceId =cDeviceId.getData().trim();
								if(sDeviceId!=null){
//									sDecryptData = decrypt(sDeviceId, oPrivateKey);
									oAirlineAdvtVO.setDeviceId(sDeviceId);
									// System.out.println("Device Id: " + sDeviceId);
								}
							}

							//------ GETTING Flight No
							NodeList nlFlightNoList = firstItemElement.getElementsByTagName("FlightNo");
							Element nlFlightNoElement = (Element)nlFlightNoList.item(0);
							CharacterData cFlightNo=(CharacterData)nlFlightNoElement.getFirstChild();
							if(cFlightNo!=null){
								String sFlightNo =cFlightNo.getData().trim();
								if(sFlightNo!=null){
//									sDecryptData = decrypt(sFlightNo, oPrivateKey);
									oAirlineAdvtVO.setFlightNo(sFlightNo);
									// System.out.println("Flight No: " + sFlightNo);
								}
							}



							//------ GETTING Mac Addr
							NodeList nlMacAddrNameList = firstItemElement.getElementsByTagName("MacAddr");
							Element nlMacAddrElement = (Element)nlMacAddrNameList.item(0);
							CharacterData cMacAddrName=(CharacterData)nlMacAddrElement.getFirstChild();
							if(cMacAddrName!=null){
								String sMacAddr =cMacAddrName.getData().trim();
								if(sMacAddr!=null){
//									sDecryptData = decrypt(sMacAddr, oPrivateKey);
									oAirlineAdvtVO.setMacAddr(sMacAddr);
									// System.out.println("Mac Addr: " + sMacAddr);
								}
							}
							

							//------ GETTING Cate Id
							NodeList nlCateIdNameList = firstItemElement.getElementsByTagName("CateId");
							Element nlCateIdElement = (Element)nlCateIdNameList.item(0);
							CharacterData cCateIdName=(CharacterData)nlCateIdElement.getFirstChild();
							if(cCateIdName!=null){
								String sCateId =cCateIdName.getData().trim();
								if(sCateId!=null){
//									sDecryptData = decrypt(sCateId, oPrivateKey);
									oAirlineAdvtVO.setCatgId(sCateId);
									// System.out.println("Catg Id: " + sCateId);
								}
							} 


							//------ GETTING Prod Id
							NodeList nlProdIdNameList = firstItemElement.getElementsByTagName("ProdId");
							Element nlProdIdElement = (Element)nlProdIdNameList.item(0);
							CharacterData cProdIdName=(CharacterData)nlProdIdElement.getFirstChild();
							if(cProdIdName!=null){
								String sProdId =cProdIdName.getData().trim();
								if(sProdId!=null){
//									sDecryptData = decrypt(sProdId, oPrivateKey);
									oAirlineAdvtVO.setProdId(sProdId);
									// System.out.println("Prod Id: " + sProdId);
								}
							} 


							//------ GETTING Prod Code
							NodeList nlProdCodeNameList = firstItemElement.getElementsByTagName("ProdCode");
							Element nlProdCodeElement = (Element)nlProdCodeNameList.item(0);
							CharacterData cProdCodeName=(CharacterData)nlProdCodeElement.getFirstChild();
							if(cProdCodeName!=null){
								String sProdCode =cProdCodeName.getData().trim();
								if(sProdCode!=null){
//									sDecryptData = decrypt(sProdCode, oPrivateKey);
									oAirlineAdvtVO.setProdCode(sProdCode);
									// System.out.println("Prod Code: " + sProdCode);
								}
							} 

							//------ GETTING Owner Id
							NodeList nlOwnerIdList = firstItemElement.getElementsByTagName("OwnerId");
							Element nlOwnerIdElement = (Element)nlOwnerIdList.item(0);
							CharacterData cOwnerIdName=(CharacterData)nlOwnerIdElement.getFirstChild();
							if(cOwnerIdName!=null){
								String sOwnerId =cOwnerIdName.getData().trim();
								if(sOwnerId!=null){
//									sDecryptData = decrypt(sOwnerId, oPrivateKey);
									oAirlineAdvtVO.setOwnerId(sOwnerId);
									// System.out.println("Owner Id: " + sOwnerId);
								}
							} 


							//------ GETTING Customer Name
							NodeList nlCustNameList = firstItemElement.getElementsByTagName("CustomerName");
							Element nlCustNameElement = (Element)nlCustNameList.item(0);
							CharacterData cCustName=(CharacterData)nlCustNameElement.getFirstChild();
							if(cCustName!=null){
								String sCustName =cCustName.getData().trim();
								if(sCustName!=null){
//									sDecryptData = decrypt(sCustName, oPrivateKey);
									oAirlineAdvtVO.setCustName(sCustName);
									// System.out.println("Customer Name: " + sCustName);
								}
							} 

							//------ GETTING Customer Phone
							NodeList nlCustPhoneList = firstItemElement.getElementsByTagName("ContactPhone");
							Element nlCustPhoneElement = (Element)nlCustPhoneList.item(0);
							CharacterData cCustPhone=(CharacterData)nlCustPhoneElement.getFirstChild();
							if(cCustPhone!=null){
								String sCustPhone =cCustPhone.getData().trim();
								if(sCustPhone!=null){
//									sDecryptData = decrypt(sCustPhone, oPrivateKey);
									oAirlineAdvtVO.setCustPhone(sCustPhone);
									// System.out.println("Customer Phone: " + sCustPhone);
								}

							} 

							//------ GETTING Customer Email
							NodeList nlCustEmailList = firstItemElement.getElementsByTagName("ContactEmail");
							Element nlCustEmailElement = (Element)nlCustEmailList.item(0);
							CharacterData cCustEmail=(CharacterData)nlCustEmailElement.getFirstChild();
							if(cCustEmail!=null){
								String sCustEmail =cCustEmail.getData().trim();
								if(sCustEmail!=null){
//									sDecryptData = decrypt(sCustEmail, oPrivateKey);
									oAirlineAdvtVO.setCustEmail(sCustEmail);
									// System.out.println("Cust Email: " + sCustEmail);
								}
							} 
							
							alAirlineAdvt.add(oAirlineAdvtVO);
					}
					//End - Parsing Item details
				}
			}
			//End - Parsing Order Items details

		}
	
		System.out.print("PlaceOrderUtil::parseOrderInfo:Exit");
		logger.error("PlaceOrderUtil::parseOrderInfo:EXIT");
	}catch (Exception e) {
		logger.error("PlaceOrderUtil::parseOrderInfo:Exception "+e.getMessage());
		logger.error("PlaceOrderUtil::parseOrderInfo:Aborted");
		// System.out.println("PlaceOrderUtil::parseOrderInfo:Exception"+e.getMessage());
	}finally{
		if(rs != null)
			rs.close();
		
		if(cstmt != null)
			cstmt.close();
		
		if(con != null)
			con.close();
	}
	return alAirlineAdvt;
	}
	
	
	public static boolean checkTotalOrderValue(OrderDetailsVO oOrderDetailsVO){
		logger.info("PlaceOrder::checkTotalOrderValue:Enter:");
		boolean bIsSuccess = false;
		int iNoOfItems = 0;
		int iOrderItemCount = 0;
		int iQty = 0;
		double dSkyBuyPrice = 0.0;
		double dTotalOrderItemValue = 0.0;
		List<OrderItemsVO> alOrderItemInfo = null;
		OrderItemsVO oOrderItemsVO = null;

		double dTotalPayment = 0.0;

		try{
			if(oOrderDetailsVO.getTotalOrderItems()!=null && oOrderDetailsVO.getTotalPayment()!=null){
				try{
					iNoOfItems = Integer.parseInt(oOrderDetailsVO.getTotalOrderItems());
					dTotalPayment = Double.parseDouble(oOrderDetailsVO.getTotalPayment());
					logger.info("PlaceOrder::checkTotalOrderValue:Total Items Value From Client:"+ iNoOfItems);
					logger.info("PlaceOrder::checkTotalOrderValue:Total Payment From Client:"+ dTotalPayment);

				}catch(NumberFormatException e){
					logger.debug("PlaceOrder::checkTotalOrderValue:Eception:"+ e.getMessage());
				}
			}


			alOrderItemInfo = oOrderDetailsVO.getOrderItemDetails();
			if(alOrderItemInfo!= null && alOrderItemInfo.size()>0){
				for(int i=0;i<alOrderItemInfo.size();i++){
					iOrderItemCount++;
					oOrderItemsVO =(OrderItemsVO) alOrderItemInfo.get(i);

					iQty = Integer.parseInt(oOrderItemsVO.getQty());
					dSkyBuyPrice = Double.parseDouble(oOrderItemsVO.getSbhPrice());

					dTotalOrderItemValue += (iQty*dSkyBuyPrice);

				}

			}
			dTotalOrderItemValue = Utils.round(dTotalOrderItemValue,2);
			logger.info("PlaceOrder::checkTotalOrderValue:Total Items Value At Server:"+ iOrderItemCount);
			logger.info("PlaceOrder::checkTotalOrderValue:Total Payment At Server:"+ dTotalOrderItemValue);

			if(iOrderItemCount ==  iNoOfItems && dTotalOrderItemValue == dTotalPayment){
				bIsSuccess = true;
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("PlaceOrder::checkTotalOrderValue:Eception:"+ e.getMessage());
		}
		logger.info("PlaceOrder::checkTotalOrderValue:Exit");
		return bIsSuccess;
	}
	
	
	public static OrderDetailsVO parseUpdateOrderDetails(Document doc)throws Exception {
		doc.getDocumentElement ().normalize ();
		NodeList nlUpdateOrderDetailsInfo = doc.getElementsByTagName("customerFeedback");
		OrderDetailsVO oOrderDetailsVO = null;
		String sCustTransId=null;
		String sNewServicesFeedback=null;
		String sSuggestion =null;
		for(int i=0;i<nlUpdateOrderDetailsInfo.getLength();i++){
			 oOrderDetailsVO =new OrderDetailsVO();
			Element el = (Element)nlUpdateOrderDetailsInfo.item(i);
			sCustTransId = getTextValue(el,"CustTransId");
			sNewServicesFeedback =getTextValue(el,"NewServicesFeedback");
			sSuggestion=getTextValue(el,"Suggestion");
			oOrderDetailsVO.setCustTransId(sCustTransId);
			oOrderDetailsVO.setSuggestion(sSuggestion);
			oOrderDetailsVO.setNewServicesFeedback(sNewServicesFeedback);
		}
		return oOrderDetailsVO;
	}
	
	
	private static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}
	

	
}
