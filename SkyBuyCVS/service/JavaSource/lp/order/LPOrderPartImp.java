

package lp.order;

import java.util.*;



public class LPOrderPartImp extends HashMap
    implements LPOrderPart
{

    protected LPOrderPartImp()
    {
        partName = "";
    }

    protected LPOrderPartImp(String name)
    {
        partName = "";
        partName = name;
    }

    public String getPartName()
    {
        return partName;
    }

    public void setPartName(String _name)
    {
        partName = _name;
    }

    public void put(String key, String val)
    {
        super.put(key, val);
    }

    public String get(String key)
    {
        Object ret = super.get(key);
        if(ret != null && (ret instanceof String))
            return (String)ret;
        else
            return null;
    }

    public void remove(String key)
    {
        Object ret = get(key);
        if(ret != null && (ret instanceof String))
            super.remove(key);
    }

    public void removeAllByType(int type)
    {
        for(Iterator it = keySet().iterator(); it.hasNext();)
        {
            String key = (String)it.next();
            Object cur = get(key);
            if(key == null || cur == null)
                it.remove();
            else
            if(type == 1 && cur != null && (cur instanceof String))
                it.remove();
            else
            if(type == 2 && cur != null && (cur instanceof LPOrderPart))
                it.remove();
        }

    }

    public void removeAll()
    {
        removeAllByType(1);
    }

    public void addPart(String key, LPOrderPart val)
    {
        addPart(key, val, -1);
    }

    public void addPart(String key, LPOrderPart o, int idx)
    {
        LPOrderPartImp val = (LPOrderPartImp)o;
        val.setPartName(key);
        if(idx > 0)
            key = key + idx;
        put(key, val.Clone());
    }

    public LPOrderPart getPart(String key)
    {
        return getPart(key, -1);
    }

    public LPOrderPart getPart(String key, int idx)
    {
        if(idx > 0)
            key = key + idx;
        Object o = super.get(key);
        if(o != null && (o instanceof LPOrderPart))
            return ((LPOrderPartImp)o).Clone();
        else
            return null;
    }

    public void removePart(String key)
    {
        removePart(key, -1);
    }

    public void removePart(String key, int idx)
    {
        if(idx > 0)
            key = key + idx;
        Object ret = super.get(key);
        if(ret != null && (ret instanceof LPOrderPart))
            super.remove(key);
    }

    public void removeAllParts()
    {
        removeAllByType(2);
    }

    public String toXML()
    {
        StringBuffer xml = new StringBuffer(2048);
        buildPart(xml);
        return xml.toString();
    }

    public void buildPart(StringBuffer sb)
    {
        sb.append("\n<");
        sb.append(partName);
        sb.append(">");
        Object key = null;
        Object val = null;
        for(Iterator it = keySet().iterator(); it.hasNext();)
        {
            key = it.next();
            val = super.get(key);
            if(val != null)
                if(val instanceof String)
                {
                    sb.append("\n<");
                    sb.append(key);
                    sb.append(">");
                    sb.append(val);
                    sb.append("</");
                    sb.append(key);
                    sb.append(">");
                } else
                if(val instanceof LPOrderPart)
                    ((LPOrderPartImp)val).buildPart(sb);
        }

        sb.append("\n</");
        sb.append(partName);
        sb.append(">");
    }

    protected LPOrderPartImp Clone()
    {
        LPOrderPartImp cl = new LPOrderPartImp(partName);
        for(Iterator it = keySet().iterator(); it.hasNext();)
        {
            String key = (String)it.next();
            Object val = super.get(key);
            if(val != null && key != null)
                if(val instanceof String)
                    cl.put(key, val);
                else
                if(val instanceof LPOrderPartImp)
                    cl.put(key, ((LPOrderPartImp)val).Clone());
        }

        return cl;
    }

    protected String partName;
}