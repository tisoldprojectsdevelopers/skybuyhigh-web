
package lp.order;

public interface LPOrderPart
{
        // interface to set/get part name, e.a. "billing"
        String getPartName();
        void  setPartName(String name);

        // Interface to handle key/value pairs
        void  put(String key, String val);
        String  get( String key);
        void  remove(String key);
        void  removeAll();

        // Interface to handle nested parts
        void  addPart(String key, LPOrderPart val);
        void  addPart(String key, LPOrderPart val, int idx);
        LPOrderPart  getPart(String key);
        LPOrderPart  getPart(String key, int idx);
        void  removePart(String key);
        void  removePart(String key, int idx);
        void  removeAllParts();

        // dumps content into XML format
        String toXML();

        // clears all content
        void  clear();

}
