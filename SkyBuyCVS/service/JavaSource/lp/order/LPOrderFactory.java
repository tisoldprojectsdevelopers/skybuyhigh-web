
package lp.order;



public class LPOrderFactory
{

    public LPOrderFactory()
    {
    }

    public static LPOrderPart createOrderPart(String name)
    {
        return new LPOrderPartImp(name);
    }

    public static LPOrderPart createOrderPart()
    {
        return new LPOrderPartImp("");
    }
}