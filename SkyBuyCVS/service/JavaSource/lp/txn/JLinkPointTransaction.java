// Decompiled by DJ v3.10.10.93 Copyright 2007 Atanas Neshkov  Date: 12/1/2008 5:03:53 PM
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   JLinkPointTransaction.java

package lp.txn;

import java.io.*;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.*;

import com.sbh.dao.PaymentDAO;
import com.sbh.payment.txn.JLinkPointCreditCardTxn;
import com.sbh.vo.CustomerOrderInfo;
import com.sbh.vo.OrderItemDetails;
import com.sbh.vo.PaymentResponse;




public class JLinkPointTransaction
{

    public JLinkPointTransaction()
    {
        m_sClientCertPath = null;
        m_sPassword = null;
        m_sHost = null;
        m_iPort = 0;
    }

    public JLinkPointTransaction(String sClientCertPath, String sPw, String sHost, int iPort)
    {
        m_sClientCertPath = null;
        m_sPassword = null;
        m_sHost = null;
        m_iPort = 0;
        m_sClientCertPath = sClientCertPath;
        m_sPassword = sPw;
        m_sHost = sHost;
        m_iPort = iPort;
    }

    private String validate()
    {
        String sRet = null;
        if(m_sClientCertPath == null || m_sClientCertPath.length() == 0)
            sRet = "Client certificate file path has not been specified.";
        if(m_sPassword == null || m_sPassword.length() == 0)
            sRet = "Password has not been specified.";
        if(m_sHost == null || m_sHost.length() == 0)
            sRet = "Host has not been specified.";
        if(m_iPort == 0)
            sRet = "Port number has not been specified.";
        if(sRet != null)
            sRet = "<r_error>" + sRet + "</r_error>";
        return sRet;
    }

    public void setClientCertificatePath(String s)
    {
        m_sClientCertPath = s;
    }

    public void setPassword(String s)
    {
        m_sPassword = s;
    }

    public void setHost(String s)
    {
        m_sHost = s;
    }

    public void setPort(int i)
    {
        m_iPort = i;
    }

    public String send(String sXml)
        throws Exception
    {
        String sRet = validate();
        if(sRet != null)
            return sRet;
        BufferedReader in = null;
        PrintWriter out = null;
        SSLSocket socket = null;
        try
        {
            SSLSocketFactory factory = null;
            try
            {
                SSLContext ctx = SSLContext.getInstance("TLS");
                KeyStore ks = KeyStore.getInstance("JKS");
                ks.load(null, null);
                KeyStore ks2 = KeyStore.getInstance("PKCS12", "SunJSSE");
                FileInputStream fin = new FileInputStream(m_sClientCertPath);
                ks2.load(fin, m_sPassword.toCharArray());
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SUNX509");
              
                kmf.init(ks2, m_sPassword.toCharArray());
                
         
                fin.close();
                ctx.init(kmf.getKeyManagers(), null, null);
                factory = ctx.getSocketFactory();
            }
            catch(Exception e)
            {
                throw e;
            }
            socket = (SSLSocket)factory.createSocket(m_sHost, m_iPort);
            socket.startHandshake();
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
            out.println(sXml);
            out.println();
            out.flush();
            if(out.checkError())
            {
                try
                {
                    out.close();
                }
                catch(Exception e1) { }
                try
                {
                    socket.close();
                }
                catch(Exception e1) { }
                return "<r_error>SSLSocketClient: java.io.PrintWriter error</r_error>";
            }
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String sResp = "";
            String s;
            while((s = in.readLine()) != null) 
                if(s != null)
                    sResp = sResp + s;
            in.close();
            out.close();
            socket.close();
            in = null;
            out = null;
            socket = null;
            return sResp;
        }
        catch(Exception e)
        {
            try
            {
                if(in != null)
                    in.close();
            }
            catch(Exception e1) {
            	// System.out.println("Socket-in "+e);
            }
            try
            {
                if(out != null)
                    out.close();
            }
            catch(Exception e1) { 
            	// System.out.println("Socket-out "+e);
            }
            try
            {
                if(socket != null)
                    socket.close();
            }
            catch(Exception e1) { 
            	// System.out.println("Socket "+e);
            }
            throw e;
        }
    }

    public static void main(String args[])
    {
        try
        {   
        	CustomerOrderInfo p_oCustomerInfo=new CustomerOrderInfo();
        	//PaymentDAO.getIPAddress();
      JLinkPointTransaction txn = new JLinkPointTransaction();
            String sClientCertPath = "C:/sbh/cert/1001201379.p12";
            txn.setClientCertificatePath(sClientCertPath);
            txn.setPassword("vaidy123");
            txn.setHost("secure.linkpt.net");
            txn.setPort(1129);
         
         
        
         
            String sXml ="<order><orderoptions><ordertype>CREDIT</ordertype></orderoptions><merchantinfo><configfile>1909892036</configfile></merchantinfo><creditcard><cardnumber>4005550000000019</cardnumber><cardexpmonth>04</cardexpmonth><cardexpyear>09</cardexpyear></creditcard><transactiondetails><oid>12345678-200903</oid></transactiondetails><payment><chargetotal>1</chargetotal></payment></order>";
            String sResponse = txn.send(sXml);
            // System.out.println(sResponse);
        	/*JLinkPointCreditCardTxn oJLinkPointCreditCard=new JLinkPointCreditCardTxn();
        	
        	p_oCustomerInfo.setOrderType("PREAUTH");
        	p_oCustomerInfo.setPaymentTxnRefId("2223334444140226");
        	p_oCustomerInfo.setOrderConfirmationNo("123412345");
        	p_oCustomerInfo.setStoreNumber("1909892036");
        	p_oCustomerInfo.setCardNumber("4111111111111111");
        	p_oCustomerInfo.setCardExpMonth("03");
        	p_oCustomerInfo.setCardExpYear("09");
        	p_oCustomerInfo.setChargeTotal("100");
        	p_oCustomerInfo.setAddrNum("3456");
        	p_oCustomerInfo.setZip("14588");
        	PaymentResponse oPaymentResponse=oJLinkPointCreditCard.process(p_oCustomerInfo);
        	// System.out.println("PaymentResponse "+oPaymentResponse.getR_Error());
        	// System.out.println("PaymentResponse Error Code"+oPaymentResponse.getR_Error_Code());
        	// System.out.println("PaymentResponse Error_Msg"+oPaymentResponse.getR_Error_Msg());
        	// System.out.println("PaymentResponse Msg"+oPaymentResponse.getR_Message());
        	// System.out.println("PaymentResponse Order Number"+oPaymentResponse.getR_OrderNum());*/
        	
        	/*p_oCustomerInfo.setOrderType("POSTAUTH");
        	p_oCustomerInfo.setOrderId(oPaymentResponse.getR_OrderNum());
        	p_oCustomerInfo.setStoreNumber("1909892036");
        	p_oCustomerInfo.setCardNumber("4111111111111111");
        	p_oCustomerInfo.setCardExpMonth("03");
        	p_oCustomerInfo.setCardExpYear("09");
        	p_oCustomerInfo.setChargeTotal("900.5");
        	p_oCustomerInfo.setAddrNum("3456");
        	p_oCustomerInfo.setZip("14588");
        	oPaymentResponse=oJLinkPointCreditCard.process(p_oCustomerInfo);
        	// System.out.println("PaymentResponse "+oPaymentResponse.getR_Error());
        	// System.out.println("PaymentResponse Error Code"+oPaymentResponse.getR_Error_Code());
        	// System.out.println("PaymentResponse Error_Msg"+oPaymentResponse.getR_Error_Msg());
        	// System.out.println("PaymentResponse Msg"+oPaymentResponse.getR_Message());*/
        	
        	/*p_oCustomerInfo.setOrderType("POSTAUTH");
        	p_oCustomerInfo.setPaymentTxnRefId(oPaymentResponse.getR_OrderNum());
        	p_oCustomerInfo.setOrderConfirmationNo("123412345");
        	p_oCustomerInfo.setStoreNumber("1909892036");
        	p_oCustomerInfo.setCardNumber("4111111111111111");
        	p_oCustomerInfo.setCardExpMonth("03");
        	p_oCustomerInfo.setCardExpYear("09");
        	p_oCustomerInfo.setChargeTotal("100");
        	p_oCustomerInfo.setAddrNum("3456");
        	p_oCustomerInfo.setZip("14588");
        	p_oCustomerInfo.setBillingAddr1("123 Broadway");
        	p_oCustomerInfo.setBillingAddr2("Suite 23");
        	p_oCustomerInfo.setBillingCity("Moorpark");
        	p_oCustomerInfo.setBillingState("CA");
        	p_oCustomerInfo.setBillingCountry("US");
        	p_oCustomerInfo.setBillingCustName("Sridevi");
        	p_oCustomerInfo.setBillingEmail("sridevi.mohan@gmail.com");
        	p_oCustomerInfo.setBillingPhone("8051234567");
        	p_oCustomerInfo.setShippingAddr1("123 Broadway");
        	p_oCustomerInfo.setShippingAddr2("Suite 23");
        	p_oCustomerInfo.setShippingCity("Moorpark");
        	p_oCustomerInfo.setShippingState("CA");
        	p_oCustomerInfo.setShippingCountry("US");
        	p_oCustomerInfo.setShippingCustName("Joe Customer");
        	p_oCustomerInfo.setTaxexempt("y");
        	
        	OrderItemDetails oOrderItemInfo=new OrderItemDetails();
        	oOrderItemInfo.setDescription("Test Product");
        	oOrderItemInfo.setId("12345");
        	oOrderItemInfo.setPrice("12.45");
        	oOrderItemInfo.setQuantity("2");
        	p_oCustomerInfo.setOrderItemDetails(oOrderItemInfo);
        	oPaymentResponse=oJLinkPointCreditCard.process(p_oCustomerInfo);
        	// System.out.println("PaymentResponse "+oPaymentResponse.getR_Error());
        	// System.out.println("PaymentResponse Error Code"+oPaymentResponse.getR_Error_Code());
        	// System.out.println("PaymentResponse Error_Msg"+oPaymentResponse.getR_Error_Msg());
        	// System.out.println("PaymentResponse Msg"+oPaymentResponse.getR_Message());*/
        }
        catch(Exception e) { 
        	// System.out.println("Main "+e);
        }
    }

    private String m_sClientCertPath;
    private String m_sPassword;
    private String m_sHost;
    private int m_iPort;
   
}