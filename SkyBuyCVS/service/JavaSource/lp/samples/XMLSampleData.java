/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class XMLSampleData {

  public static  String  AVS_CVM = "<!-- An Example Credit Card SALE With Minimum Fields required for AVS and Card Code fraud prevention measures-->" +
  "<order>" +
          "<merchantinfo>" +
                  "<!-- Replace with your STORE NUMBER or STORENAME-->" +
                  "<configfile>1909892036</configfile>" +
          "</merchantinfo>" +
          "<orderoptions>" +
                  "<ordertype>SALE</ordertype>" +
          "</orderoptions>" +
          "<payment>" +
                  "<chargetotal>15.99</chargetotal>" +
          "</payment>" +
            "<creditcard>" +
                  "<cardnumber>4214-0904-5000-4887</cardnumber>" +
                  "<cardexpmonth>03</cardexpmonth>" +
                  "<cardexpyear>10</cardexpyear>" +
                  "<!-- CVM is the three-digit security code usually found on the signature line on the back of the card -->" +
                  "<cvmvalue>123</cvmvalue>" +
                  "<cvmindicator>provided</cvmindicator>" +
          "</creditcard>" +
          "<billing>" +
                  "<!-- Required for Address Verification -->" +
                  "<addrnum>1234</addrnum>" +
                  "<zip>87123</zip>" +
          "</billing>" +
  "</order>";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a FORCED TICKET Transaction
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String FORCED_TICKET = "<!-- Minimum Required Fields for a FORCED TICKET Transaction -->" +
  "<order>" +
          "<merchantinfo>" +
                  "<!-- Replace with your STORE NUMBER or STORENAME-->" +
                  "<configfile>1909892036</configfile>" +
          "</merchantinfo>" +
          "<orderoptions>" +
                  "<ordertype>POSTAUTH</ordertype>" +
          "</orderoptions>" +
          "<payment>" +
                  "<chargetotal>12.99</chargetotal>" +
          "</payment>" +
          "<creditcard>" +
                  "<cardnumber>4111-1111-1111-1111</cardnumber>" +
                  "<cardexpmonth>03</cardexpmonth>" +
                  "<cardexpyear>09</cardexpyear>" +
          "</creditcard>" +
          "<transactiondetails>" +
                  "<!-- The reference number given over the phone --> " +
                  "<reference_number>NEW987654</reference_number> " +
          "</transactiondetails>" +
  "</order>";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Required Fields for an Example Credit Card SALE with ITEMS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public static  String ITEMS_W_ESD ="<!-- Required Fields for an Example Credit Card SALE with ITEMS-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<subtotal>45.98</subtotal> " +
                  "<tax>0.32</tax> " +
                  "<shipping>1.02</shipping> " +
                  "<chargetotal>47.32</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<billing> " +
                  "<!-- Required for AVS. If not provided, transactions will downgrade.--> " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip> " +
                  "<!-- If you use ESD items - name is required --> " +
                  "<name>Joe Customer</name> " +
          "</billing> " +
          "<items> " +
                  "<item> " +
                          "<id>123456-A98765</id> " +
                          "<description>Logo T-Shirt</description> " +
                          "<price>12.99</price> " +
                          "<quantity>1</quantity> " +
                          "<serial>0987654321</serial> " +
                          "<options> " +
                                  "<option> " +
                                          "<name>Color</name> " +
                                          "<value>Red</value> " +
                                  "</option> " +
                                  "<option> " +
                                          "<name>Size</name> " +
                                          "<value>XL</value> " +
                                  "</option> " +
                          "</options> " +
                  "</item> " +
                          "<item> " +
                          "<id>123456-A98765</id> " +
                          "<description>Blast-Em Software</description> " +
                          "<price>32.99</price> " +
                          "<quantity>1</quantity> " +
                          "<serial>0987654321</serial> " +
                          "<esdtype>softgood</esdtype> " +
                          "<softfile>blastemgame.exe</softfile> " +
                  "</item>  " +
          "</items> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// An Example Level 2 Purchasing Card SALE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String  L2PURCHASING_CARD = "<!-- An Example Level 2 Purchasing Card SALE -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<!-- Tax is required for purchasing cards. If the tax is $0.00, pass a value of 0 for tax --> " +
                  "<tax>0.32</tax> " +
                  "<chargetotal>47.32</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<transactiondetails> " +
                  "<!-- If there is no PO Number for this order, pass a department code or other value, but make sure the value you pass is supplied by the customer --> " +
                  "<ponumber>1203A-G4567</ponumber> " +
                  "<!-- If the purchase is tax exempt, pass a value of Y for taxexempt--> " +
                  "<taxexempt>N</taxexempt> " +
          "</transactiondetails> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a Credit Card CREDIT
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String RETURN = "<!-- Minimum Required Fields for a Credit Card CREDIT -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>CREDIT</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<!-- Amount returned must be less than or equal to the order amount. If there is more than one return against this order, make sure the total of the returns doesn't exceed the original sale amount. --> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<transactiondetails> " +
                  "<!-- Must be a valid order ID from a prior Sale or PostAuth --> " +
                  "<!-- <oid>12345678-A345</oid> --> " +
          "</transactiondetails> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// An Example Credit Card SALE with all commonly used fields included
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String SALE_MAXINFO = "<!-- An Example Credit Card SALE with all commonly used fields included -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
                  "<!-- For test transactions, set to GOOD, DECLINE, or DUPLICATE --> " +
                  "<result>LIVE</result> " +
          "</orderoptions> " +
          "<transactiondetails> " +
                  "<!-- For credit card retail txns, set to RETAIL, for Mail order/telephone order, set to MOTO, for e-commerce, leave out or set to ECI --> " +
                  "<transactionorigin>MOTO</transactionorigin> " +
                  "<!-- Order ID number must be unique. If not set, gateway will assign one. --> " +
                  "<!-- <oid>12345678-A345</oid>  --> " +
                  "<ponumber>09876543-Q1234</ponumber> " +
                  "<taxexempt>N</taxexempt> " +
                  "<terminaltype>UNSPECIFIED</terminaltype> " +
                  "<ip>123.123.123.123</ip> " +
          "</transactiondetails> " +
          "<items> " +
                  "<item> " +
                          "<id>123456-A98765</id> " +
                          "<description>Logo T-Shirt</description> " +
                          "<price>12.99</price> " +
                          "<quantity>1</quantity> " +
                          "<serial>0987654321</serial> " +
                          "<options> " +
                                  "<option> " +
                                          "<name>Color</name> " +
                                          "<value>Red</value> " +
                                  "</option> " +
                                  "<option> " +
                                          "<name>Size</name> " +
                                          "<value>XL</value> " +
                                  "</option> " +
                          "</options> " +
                  "</item> " +
          "</items> " +
          "<payment> " +
                  "<subtotal>12.99</subtotal> " +
                  "<!-- Replace value with calculated tax  --> " +
                  "<tax>0.34</tax> " +
                  "<!-- Replace value with calculated shipping charges --> " +
                  "<shipping>1.45</shipping> " +
                  "<vattax>0.00</vattax> " +
                  "<chargetotal>14.78</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
                  "<!-- CVM is the three-digit security code usually found on the signature line on the back of the card --> " +
                  "<cvmvalue>123</cvmvalue> " +
                  "<cvmindicator>provided</cvmindicator> " +
                 "</creditcard>" +
                  "<billing> " +
                  "<!-- Required for Address Verification --> " +
                  "<addrnum>123</addrnum> " +
                  "<zip>87123</zip> " +
                  "<!-- Billing name and address et al. --> " +
                  "<name>Joe Customer</name> " +
                  "<company>SomeWhere, Inc.</company> " +
                  "<address1>123 Broadway</address1> " +
                  "<address2>Suite 23</address2> " +
                  "<city>Moorpark</city> " +
                  "<state>CA</state> " +
                  "<country>US</country> " +
                  "<phone>8051234567</phone> " +
                  "<fax>8059876543</fax> " +
                  "<email>joe.customer@somewhere.com</email> " +
          "</billing> " +
          "<shipping> " +
                  "<!-- Shipping name and address --> " +
                  "<name>Joe Customer</name> " +
                  "<!--<company>SomeWhere, Inc.</company> -->" +
                  "<address1>123 Broadway</address1> " +
                  "<address2>Suite 23</address2> " +
                  "<city>Moorpark</city> " +
                  "<state>CA</state> " +
                  "<zip>12345</zip> " +
                  "<country>US</country> " +
          "</shipping> " +
          "<notes> " +
                  "<comments>Repeat customer. Ship immediately.</comments> " +
                  "<referred>Saw ad on Web site.</referred> " +
          "</notes> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a Credit Card SALE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String SALE_MININFO = "<!-- Minimum Required Fields for a Credit Card SALE-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
                  "<!-- For a test, set result to GOOD, DECLINE, or DUPLICATE --> " +
                  "<result>LIVE</result>  " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<billing> " +
                  "<!-- Required for AVS. If not provided, transactions will downgrade.--> " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip> " +
          "</billing> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// An Example shipping calculation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String SHIPPING = "<!-- An Example shipping calculation -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>CALCSHIPPING</ordertype> " +
          "</orderoptions> " +
          "<shipping> " +
                  "<!-- Include the fields needed for your shipping calculation method --> " +
                  "<carrier>2</carrier> " +
                  "<weight>1.2</weight> " +
                  "<items>1</items> " +
                  "<total>12.99</total> " +
                  "<state>CA</state> " +
          "</shipping> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// An Example tax calculation
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String TAX ="<!-- An Example tax calculation --> " +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>CALCTAX</ordertype> " +
          "</orderoptions> " +
          "<shipping> " +
                  "<!-- Include the fields needed for tax calculation --> " +
                  "<total>12.99</total> " +
                  "<state>CA</state> " +
                  "<zip>12345</zip> " +
          "</shipping> " +
          "<payment> " +
                  "<subtotal>12.99</subtotal> " +
          "</payment> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a VirtualCheck SALE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String VCHECK_SALE = "<!-- Minimum Required Fields for a VirtualCheck SALE-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<telecheck> " +
                  "<!-- Customer's Driver's license # and DL state.--> " +
                  "<dl>120381698</dl> " +
                  "<dlstate>CA</dlstate> " +
                  "<!-- Bank's name and state.--> " +
                  "<bankname>Wells Fargo</bankname> " +
                  "<bankstate>CA</bankstate> " +
                  "<!-- Transit routing number for the customer's bank --> " +
                  "<routing>12345678</routing> " +
                  "<!-- Customer's bank account number --> " +
                  "<account>2139842610</account> " +
                  "<!-- Is this a business or consumer (personal) purchase? personal  = pc, business = bc --> " +
                  "<accounttype>pc</accounttype> " +
          "</telecheck> " +
          "<billing> " +
                  "<name>Joe Customer</name> " +
                  "<address1>123 Broadway</address1> " +
                  "<city>Moorpark</city> " +
                  "<state>CA</state> " +
                  "<zip>12345</zip> " +
                  "<phone>8051234567</phone> " +
                  "<email>joe.customer@somewhere.com</email> " +
          "</billing> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a VirtualCheck VOID
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String VCHECK_VOID ="<!-- Minimum Required Fields for a VirtualCheck VOID -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>VOID</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<telecheck> " +
                  "<void>1</void> " +
          "</telecheck> " +
          "<transactiondetails> " +
                  "<!-- Must be a valid order ID from a prior Sale  --> " +
                  "<oid>12345678-A345</oid> " +
          "</transactiondetails> " +
  "</order>";


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a Credit Card VOID
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String VOID ="<!-- Minimum Required Fields for a Credit Card VOID --> " +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>VOID</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<transactiondetails> " +
                  "<!-- Must be a valid order ID from a prior Sale or PostAuth --> " +
                  "<oid>7AA4202D-493632AF-617-1C8A9C</oid> " +
          "</transactiondetails> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a PreAuth
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String PREAUTH ="<!-- Minimum Required Fields for a PreAuth -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>PREAUTH</ordertype> " +
                  "<!-- For a test, set result to GOOD, DECLINE, or DUPLICATE --> " +
                  "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.5</chargetotal> " +
          "</payment> " +          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<billing> " +
                  "<!-- Required for AVS. If not provided, transactions will downgrade.--> " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip> " +
          "</billing> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a PostAuth
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String POSTAUTH="<!-- Minimum Required Fields for a PostAuth -->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>POSTAUTH</ordertype> " +
                  "<!-- For a test, set result to GOOD, DECLINE, or DUPLICATE --> " +
                  
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.5</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<transactiondetails> " +
                  "<!-- Must be a valid order ID from a prior PreAuth --> " +
                  "<oid>7AA4202D-493632AF-617-1C8A9C</oid> " +
          "</transactiondetails> " +
  "</order>";


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimum Required Fields for a Recurring Credit Card Sale (Periodic Bill)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String PB_NEW = "<!-- Minimum Required Fields for a Recurring Credit Card Sale (Periodic Bill)-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<periodic> " +
                  "<!-- Submits a recurring transaction charging the card 3 times, once a month, starting today --> " +
                  "<action>SUBMIT</action> " +
                  "<installments>3</installments> " +
                  "<threshold>3</threshold> " +
                  "<!-- If you don't want it to start today, pass a date in the format YYYYMMDD --> " +
                  "<startdate>immediate</startdate> " +
                  "<periodicity>monthly</periodicity> " +
          "</periodic> " +
          "<billing> " +
                  "<!-- Required for AVS. If not provided, transactions will downgrade.--> " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip> " +
          "</billing> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Required Fields to MODIFY a Recurring Credit Card Sale (Periodic Bill)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String PB_MODIFY = "<!-- Required Fields to MODIFY a Recurring Credit Card Sale (Periodic Bill)-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<periodic> " +
                  "<action>MODIFY</action> " +
                  "<startdate>immediate</startdate> " +
                  "<installments>3</installments> " +
                  "<threshold>3</threshold> " +
                  "<periodicity>monthly</periodicity> " +
          "</periodic> " +
          "<transactiondetails> " +
                  "<!-- You MUST pass a valid Order ID for an EXISTING periodic bill to identify which bill you want to modify --> " +
                  "<oid>111.456.87.03-O986646-A123</oid> " +
          "</transactiondetails> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Required Fields to CANCEL a Recurring Credit Card Sale (Periodic Bill)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String  PB_CANCEL="<!-- Required Fields to CANCEL a Recurring Credit Card Sale (Periodic Bill)-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<periodic> " +
                  "<action>CANCEL</action> " +
                  "<startdate>immediate</startdate> " +
                  "<installments>3</installments> " +
                  "<threshold>3</threshold> " +
                  "<periodicity>monthly</periodicity> " +
          "</periodic> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<transactiondetails> " +
                  "<!-- You MUST pass a valid Order ID for an EXISTING periodic bill to identify which bill you want to cancel --> " +
                  "<oid>111.456.87.03-O986646-A123</oid> " +
          "</transactiondetails> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimal Retail Keyed Transaction
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String  RETAIL_KEYED="<!-- Minimum Required Fields for a Credit Card SALE-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<transactiondetails> " +
                  "<transactionorigin>RETAIL</transactionorigin> " +
                  "<!-- set terminaltype to POS for an electronic cash register or integrated POS system, STANDALONE for a point-of-sale credit card terminal, UNATTENDED for a self-service station, or UNSPECIFIED for e-commerce or other applications --> " +
                  "<terminaltype>POS</terminaltype> " +
          "</transactiondetails> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<!-- <billing> " +
                  "You should not do AVS for a retail swiped transaction, unless it is an UNATTENDED terminaltype.  " +
                  "billing addrnum and billing zip are not required because we're not doing AVS ... " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip>  " +
          "</billing>--> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimal Retail Transaction with partial AVS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String  RETAIL_PARTIAL_AVS = "<!-- Minimum Required Fields for a Credit Card SALE-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<transactiondetails> " +
                  "<transactionorigin>RETAIL</transactionorigin> " +
                  "<!-- set terminaltype to POS for an electronic cash register or integrated POS system, STANDALONE for a point-of-sale credit card terminal, UNATTENDED for a self-service station, or UNSPECIFIED for e-commerce or other applications --> " +
                  "<terminaltype>UNATTENDED</terminaltype> " +
          "</transactiondetails> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<cardnumber>4111-1111-1111-1111</cardnumber> " +
                  "<cardexpmonth>03</cardexpmonth> " +
                  "<cardexpyear>09</cardexpyear> " +
          "</creditcard> " +
          "<billing> " +
                  "<!-- For an UNATTENDED terminaltype, do a partial AVS: zip code only. " +
                  "<addrnum>123</addrnum> --> " +
                  "<zip>12345</zip>  " +
          "</billing> " +
  "</order>";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Minimal Retail Swipe Transaction
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static  String  RETAIL_SWIPE = "<!-- Minimum Required Fields for a Credit Card SALE-->" +
  "<order> " +
          "<merchantinfo> " +
                  "<!-- Replace with your STORE NUMBER or STORENAME--> " +
                  "<configfile>1909892036</configfile> " +
          "</merchantinfo> " +
          "<transactiondetails> " +
                  "<transactionorigin>RETAIL</transactionorigin> " +
                  "<!-- set terminaltype to POS for an electronic cash register or integrated POS system, STANDALONE for a point-of-sale credit card terminal, UNATTENDED for a self-service station, or UNSPECIFIED for e-commerce or other applications --> " +
                  "<terminaltype>POS</terminaltype> " +
          "</transactiondetails> " +
          "<orderoptions> " +
                  "<ordertype>SALE</ordertype> " +
          "</orderoptions> " +
          "<payment> " +
                  "<chargetotal>12.99</chargetotal> " +
          "</payment> " +
          "<creditcard> " +
                  "<track>1298361023614908</track> " +
          "</creditcard> " +
          "<!-- <billing> " +
                  "You should not do AVS for a retail swiped transaction, unless it is an UNATTENDED terminaltype.  " +
                  "billing addrnum and billing zip are not required because we're not doing AVS ... " +
                  "<addrnum>123</addrnum> " +
                  "<zip>12345</zip>  " +
          "</billing>--> " +
  "</order>";


}