/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;

import java.io.*;
import java.util.*;
import lp.txn.JLinkPointTransaction;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */
 abstract public class JLinkPointSample {



  public JLinkPointSample(String _clientCertPath, String _configfile,
                          String _host, String _password, int _port)
 {
    clientCertPath = _clientCertPath;
    password = _password;
    host = _host;
    configfile = _configfile;
    port = _port;
  }

  public JLinkPointSample()
  {
    InputStream is = null;
    try
    {
     is = new FileInputStream("lpcli.prop");
     Properties ps = new Properties();
     ps.load(is);
      clientCertPath = ps.getProperty("ClientCertificatePath","");
      password = ps.getProperty("Password");
      host = ps.getProperty("Host");
      configfile = ps.getProperty("ConfigFile");
      port = Integer.parseInt((String)ps.get("Port"));

    }
    catch( IOException e)
    {
    // System.out.println("Cannot find/load property file 'lpcli.prop'");
    }
    finally{
    if( is != null ) try{ is.close(); } catch (IOException e1){}
    }
/*
    clientCertPath = "./909005.p12";
    password = "12345678";
    host = "api.qa.linkpoint.com";
    configfile = "909005";
    port = 1129;
*/
  }

  public boolean process()
  {
   JLinkPointTransaction txn = new JLinkPointTransaction();

    txn.setClientCertificatePath(clientCertPath);
    txn.setPassword(password);
    txn.setHost(host);
    txn.setPort(port);

    // System.out.println("OUTGOING XML:\n"+getOrderXML());

    String sResponse = "";
    boolean ret=false;
    try{
        sResponse = txn.send(getOrderXML());
       }
     catch (Exception e)
     {
     e.printStackTrace();
     sResponse = "<r_error>"+e.toString()+"</r_error>";
     }

    parseResponse(sResponse);
    printResp();

  return ret;
  }

  abstract protected String getOrderXML();


  protected void parseResponse( String rsp )
  {
    R_Time = parseTag("r_time", rsp);
    R_Ref = parseTag("r_ref", rsp);
    R_Approved = parseTag("r_approved", rsp);
    R_Code = parseTag("r_code", rsp);
    R_Authresr = parseTag("r_authresronse", rsp);
    R_Error = parseTag("r_error", rsp);
    R_OrderNum = parseTag("r_ordernum", rsp);
    R_Message = parseTag("r_message", rsp);
    R_Score = parseTag("r_score", rsp);
    R_TDate = parseTag("r_tdate", rsp);
    R_AVS = parseTag("r_avs", rsp);
    R_Tax = parseTag("r_tax", rsp);
    R_Shipping = parseTag("r_shipping", rsp);
    R_FraudCode = parseTag("r_fraudCode", rsp);
    R_ESD = parseTag("esd", rsp);
  }

  protected String parseTag(String tag, String rsp )
  {
    StringBuffer sb = new StringBuffer(256);
    sb.append('<' + tag + '>');
    int len = sb.length();
    int idxSt=-1,idxEnd=-1;
    if( -1 == (idxSt = rsp.indexOf(sb.toString())))
    { return ""; }
    idxSt += len;
    sb.setLength(0);
    sb.append("</" + tag + '>');
    if( -1 == (idxEnd = rsp.indexOf(sb.toString(),idxSt)))
    { return ""; }
    return rsp.substring(idxSt,idxEnd);
  }

  public void  printResp()
  {
    // System.out.println("\n******  XML RESPONSE  *******");
   /* if(R_Time.length() != 0)
    // System.out.println("<r_time>" + R_Time + "</r_time>");
    if(R_Ref.length() != 0)
    // System.out.println("<r_ref>" + R_Ref + "</r_ref>");
    if(R_Approved.length() != 0)
    // System.out.println("<r_approved>" + R_Approved + "</r_approved>");
    if(R_Code.length() != 0)
    // System.out.println("<r_code>" + R_Code + "</r_code>");
    if(R_OrderNum.length() != 0)
    // System.out.println("<r_ordernum>" + R_OrderNum + "</r_ordernum>");
    if(R_Error.length() != 0)
    // System.out.println("<r_error>" + R_Error + "</r_error>");
    if(R_FraudCode.length() != 0)
    // System.out.println("<r_fraudCode>" + R_FraudCode + "</r_fraudCode>");
    if(R_Authresr.length() != 0)
    // System.out.println("<r_authresponse>" + R_Authresr + "</r_authresponse>");
    if(R_Message.length() != 0)
    // System.out.println("<r_message>" + R_Message + "</r_message>");
    if(R_TDate.length() != 0)
    // System.out.println("<r_tdate>" + R_TDate + "</r_tdate>");
    if(R_Shipping.length() != 0)
    // System.out.println("<r_shipping>" + R_Shipping + "</r_shipping>");
    if(R_Tax.length() != 0)
    // System.out.println("<r_tax>" + R_Tax + "</r_tax>");
    if(R_AVS.length() != 0)
    // System.out.println("<r_avs>" + R_AVS + "</r_avs>");
    if(R_ESD.length() != 0)
    // System.out.println("<esd>" + R_ESD + "</esd>");
    if(R_Score.length() !=0)*/
    // System.out.println("<r_score>" + R_Score + "</r_score>");

    // System.out.println("\n****** End Of XML RESPONSE ********");

  }

   protected String clientCertPath="";
   protected String password="";
   protected String host="";
   protected String configfile="";
   protected int    port=0;

   // response holders
   protected String R_Time="";
   protected String R_Ref="";
   protected String R_Approved="";
   protected String R_Code="";
   protected String R_Authresr="";
   protected String R_Error="";
   protected String R_OrderNum="";
   protected String R_Message="";
   protected String R_Score="";
   protected String R_TDate="";
   protected String R_AVS="";
   protected String R_FraudCode="";
   protected String R_ESD="";
   protected String R_Tax="";
   protected String R_Shipping="";

}