/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class VCHECK_VOID extends JLinkPointSample {
  public VCHECK_VOID() {
  }

  public VCHECK_VOID(String _oid) {
  oid = _oid;
  }

  protected String getOrderXML() {
    // Create an empty order
      LPOrderPart  order = LPOrderFactory.createOrderPart("order");

  // Create a part we will use to build the order
      LPOrderPart  op = LPOrderFactory.createOrderPart();

  // Build 'orderoptions'
      op.put("ordertype","VOID");
  // add 'orderoptions to order
  order.addPart("orderoptions", op );

      // Build 'merchantinfo'
      op.clear();
      op.put("configfile",configfile);
  // add 'merchantinfo to order
  order.addPart("merchantinfo", op );

      // Build 'telecheck' part
      op.clear();
      op.put("void","1");
  // add 'telecheck' to order
  order.addPart("telecheck", op );


       // Build 'payment'
      op.clear();
      op.put("chargetotal","12.99");
  // add 'payment to order
  order.addPart("payment", op );

      // Add oid
      op.clear();
      op.put("oid",oid);
  // add 'transactiondetails to order
  order.addPart("transactiondetails", op );

  return  order.toXML();
 }

private String oid="";
}