/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class PB_CANCEL extends JLinkPointSample {
  public PB_CANCEL() {
  }
  public PB_CANCEL(String _oid) {
  oid = _oid;
  }
  protected String getOrderXML() {
    // Create an empty order
    LPOrderPart      order = LPOrderFactory.createOrderPart("order");

// Create a part we will use to build the order
    LPOrderPart  op = LPOrderFactory.createOrderPart();


// Build 'orderoptions'
    // For a test, set result to GOOD, DECLINE, or DUPLICATE
    op.put("result","GOOD");
    op.put("ordertype","SALE");
// add 'orderoptions to order
order.addPart("orderoptions", op );


    // Build 'merchantinfo'
    op.clear();
    op.put("configfile",configfile);
// add 'merchantinfo to order
order.addPart("merchantinfo", op );


    // Build 'creditcard'
    op.clear();
    op.put("cardnumber","4111-1111-1111-1111");
    op.put("cardexpmonth","03");
    op.put("cardexpyear","05");
// add 'creditcard to order
order.addPart("creditcard", op );

    // Build 'billing'
    op.clear();
    op.put("addrnum","123");
    op.put("zip","12345");
    // add 'billing to order
order.addPart("billing", op );

     // Build 'payment'
    op.clear();
    op.put("chargetotal","14.99");
// add 'payment to order
order.addPart("payment", op );

     // build 'periodic' part
    op.clear();
    op.put("action","CANCEL");
    op.put("startdate","immediate");
    op.put("periodicity","monthly");
    op.put("installments","3");
    op.put("threshold","3");
    // add 'periodic' to order
order.addPart("periodic", op );

    // Add oid
    op.clear();
    op.put("oid",oid);
// add 'transactiondetails to order
order.addPart("transactiondetails", op );

  return  order.toXML();
 }

private String oid="";
}