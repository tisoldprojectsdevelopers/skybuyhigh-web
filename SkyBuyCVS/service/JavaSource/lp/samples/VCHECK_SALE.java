/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class VCHECK_SALE extends JLinkPointSample {
  public VCHECK_SALE() {
  }
  protected String getOrderXML() {
    // Create an empty order
      LPOrderPart   order = LPOrderFactory.createOrderPart("order");

  // Create a part we will use to build the order
      LPOrderPart  op = LPOrderFactory.createOrderPart();

  // Build 'orderoptions'
      op.put("result","LIVE");
      op.put("ordertype","SALE");
  // add 'orderoptions to order
  order.addPart("orderoptions", op );


      // Build 'merchantinfo'
      op.clear();
      op.put("configfile",configfile);
  // add 'merchantinfo to order
  order.addPart("merchantinfo", op );


       // Build 'billing'
      op.clear();
      op.put("name","Joe Customer");
      op.put("address1","123 Broadway");
      op.put("state","CA");
      op.put("city","Moorpark");
      op.put("zip","12345");
      op.put("phone","8051234567");
      op.put("email","joe.customer@somewhere.com");
      // add 'billing to order
  order.addPart("billing", op );


       // Build 'telecheck' part
      op.clear();
      op.put("routing","123456789");
      op.put("account","2139842610");
      op.put("accounttype","pc");
      op.put("dl","120381698");
      op.put("dlstate","CA");
      op.put("bankname","Wells Fargo");
      op.put("bankstate","CA");
  // add 'telecheck' to order
  order.addPart("telecheck", op );


       // Build 'payment'
      op.clear();
      op.put("chargetotal","12.99");
  // add 'payment to order
  order.addPart("payment", op );

  return order.toXML();
 }

}