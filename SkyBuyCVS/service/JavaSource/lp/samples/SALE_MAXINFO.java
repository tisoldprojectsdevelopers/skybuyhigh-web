/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class SALE_MAXINFO extends JLinkPointSample {
  public SALE_MAXINFO() {
  }
  protected String getOrderXML() {
    // Create an empty order
      LPOrderPart      order = LPOrderFactory.createOrderPart("order");

  // Create a part we will use to build the order
      LPOrderPart  op = LPOrderFactory.createOrderPart();

  // Build 'orderoptions'
      op.put("ordertype","SALE");
      // live transaction
      op.put("result","LIVE");
  // add 'orderoptions to order
  order.addPart("orderoptions", op );

      // Build 'merchantinfo'
      op.clear();
      op.put("configfile",configfile);
  // add 'merchantinfo to order
  order.addPart("merchantinfo", op );


      // Build 'transactiondetails'
      op.clear();
      op.put("transactionorigin","MOTO");
      // If there is no PO Number for this order,
      // pass a department code or other value, but make sure the value you pass is supplied by the customer
      op.put("ponumber","09876543-Q1234");
      // If the purchase is tax exempt,
      // pass a value of Y for taxexempt
      op.put("taxexempt","N");
      op.put("terminaltype","UNSPECIFIED");
      op.put("ip","123.123.123.123");
      // add 'billing to order
  order.addPart("transactiondetails", op );

      // Build 'billing'
      // Required for AVS. If not provided,
      // transactions will downgrade.
      op.clear();
      op.put("zip","12345");
      op.put("addrnum","123");

      op.put("name","Joe Customer");
      op.put("company","SomeWhere, Inc.");
      op.put("address1","123 Broadway");
      op.put("address2","Suite 23");
      op.put("city","Moorpark");
      op.put("state","CA");
      op.put("country","US");
      op.put("phone","8051234567");
      op.put("fax","8059876543");
      op.put("email","joe.customer@somewhere.com");
      // add 'billing to order
  order.addPart("billing", op );


  // build shipping
      op.clear();
      op.put("name","Joe Customer");
      op.put("address1","123 Broadway");
      op.put("address2","Suite 23");
      op.put("city","Moorpark");
      op.put("state","CA");
      op.put("country","US");
      op.put("zip","12345");
  order.addPart("shipping",op);

      // Build 'creditcard'
      op.clear();
      op.put("cardnumber","4111-1111-1111-1111");
      op.put("cardexpmonth","03");
      op.put("cardexpyear","05");
      op.put("cvmvalue","123");
      op.put("cvmindicator","provided");
  // add 'creditcard to order
  order.addPart("creditcard", op );

       // Build 'payment'
      op.clear();
      op.put("subtotal","12.99");
      op.put("tax","0.34");
      op.put("shipping","1.45");
      op.put("vattax","0.00");
      op.put("chargetotal","14.78");
  // add 'payment to order
  order.addPart("payment", op );

  // add notes to order
      op.clear();
      op.put("referred","Saw ad on Web site.");
      op.put("comments","Repeat customer. Ship immediately.");
      order.addPart("notes",op);

  // Create some parts we use to build order itmes
      LPOrderPart items = LPOrderFactory.createOrderPart();
      LPOrderPart item = LPOrderFactory.createOrderPart();
      LPOrderPart options = LPOrderFactory.createOrderPart();

              // build and build 'item1'
              item.put("id","123456-A98765");
              item.put("description","Logo T-Shirt");
              item.put("quantity","1");
              item.put("price","12.99");
              item.put("serial","0987654321");

              // build item's options
                      op.clear();
                      op.put("name","Color");
                      op.put("value","Red");
                      options.addPart("option",op,1);

                      op.clear();
                      op.put("name","Size");
                      op.put("value","XL");
                      options.addPart("option",op,2);

              // add 'options' to item
              item.addPart("options", options );

        // add 'item' to 'items' collection
    items.addPart("item", item, 1 );
  // add 'items' to order
  order.addPart("items", items );

   return  order.toXML();
   }

}