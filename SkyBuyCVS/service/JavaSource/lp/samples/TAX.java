/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class TAX extends JLinkPointSample {
  public TAX() {
  }
protected String getOrderXML() {
// Create an empty order
    LPOrderPart  order = LPOrderFactory.createOrderPart("order");
// Create a part
    LPOrderPart  op = LPOrderFactory.createOrderPart();
    // Build 'orderoptions'
    op.put("ordertype","CALCTAX");
// add 'orderoptions to order
order.addPart("orderoptions", op );

    op.clear();
    // Build 'merchantinfo'
    op.put("configfile",configfile);
// add 'merchantinfo to order
order.addPart("merchantinfo", op );

     // Build 'payment'
    op.clear();
    op.put("subtotal","12.99");
// add 'payment to order
order.addPart("payment", op );

    // Build 'shipping'
    op.clear();
    op.put("zip","12345");
    op.put("total","12.99");
    op.put("state","CA");
// add shpping to order
order.addPart("shipping", op );

return order.toXML();
}

}