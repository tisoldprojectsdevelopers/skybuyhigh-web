/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class MULTIPLE extends JLinkPointSample {

//   This sample demonstrates running an array of PREAUTH transactions.
//
//   This could equally well be a series of POSTAUTHS or RETURNS if the
//   OIDs match those previously approved.

  // Definition of an array of transactions specific data.
  // For minimal transaction we use following fields:
  // 1	TOTAL,
  // 2	CREDIT CARD NUMBER
  // 3	CREDIT CARD EXP. MONTH
  // 4	CREDIT CARD EXP. YEAR
  //		Next two are required for AVS. If not provided
  //		transaction will dongrade
  // 5	ADDRESS NUMBERS
  // 6	ZIP
    String orders[][]= { {"1.11","4111-1111-1111-1111","01","04","123","12345"},
                         {"2.22","4111-1111-1111-1111","02","05","234","23456"},
                         {"3.33","4111-1111-1111-1111","03","06","345","34567"}};

  public MULTIPLE() {
  }

  public boolean process()
  {
    // Assign common fields for all transactions
    // 1.1 'orderoptions'
         op.put("ordertype","PREAUTH");
         op.put("result","GOOD");
     // add 'orderoptions to order
     order.addPart("orderoptions", op );
         // 1.2 'merchantinfo'
         op.clear();
         op.put("configfile",configfile);
     // add 'merchantinfo to order
     order.addPart("merchantinfo", op );

     for ( idxTxn=0; idxTxn<3; idxTxn++)
     {
      // this will call back to getOrderXML()
      super.process();
     }

  return true;
  }
  protected String getOrderXML() {
    // Add transaction specific fields

    // Build 'billing'
    // Required for AVS. If not provided,
    // transactions will downgrade.
    op.clear();
    op.put("zip",orders[idxTxn][5]);
    op.put("addrnum",orders[idxTxn][4]);
    // add 'billing to order
    order.addPart("billing", op );

    // Build 'creditcard'
    op.clear();
    op.put("cardnumber",orders[idxTxn][1]);
    op.put("cardexpmonth",orders[idxTxn][2]);
    op.put("cardexpyear",orders[idxTxn][3]);
    // add 'creditcard to order
    order.addPart("creditcard", op );

     // Build 'payment'
    op.clear();
    op.put("chargetotal",orders[idxTxn][0]);
    // add 'payment to order
    order.addPart("payment", op );

 return order.toXML();
}

  // Create an empty order
  private  LPOrderPart order = LPOrderFactory.createOrderPart("order");
 // Create a part we will use to build the order
  private  LPOrderPart  op = LPOrderFactory.createOrderPart();
 // Keep current transaction num
  private  int idxTxn=0;
}