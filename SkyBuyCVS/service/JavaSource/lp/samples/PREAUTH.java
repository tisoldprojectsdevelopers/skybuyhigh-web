/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class PREAUTH extends JLinkPointSample {
  public PREAUTH() {
  }
  protected String getOrderXML() {
    // Create an empty order
   LPOrderPart  order = LPOrderFactory.createOrderPart("order");

   // Create a part we will use to build the order
   LPOrderPart  op = LPOrderFactory.createOrderPart();

   // Build 'orderoptions'
       // For a test, set result to GOOD, DECLINE, or DUPLICATE
       op.put("result","GOOD");
       op.put("ordertype","PREAUTH");
   // add 'orderoptions to order
   order.addPart("orderoptions", op );


       // Build 'merchantinfo'
       op.clear();
       op.put("configfile",configfile);
   // add 'merchantinfo to order
   order.addPart("merchantinfo", op );


       // Build 'creditcard'
       op.clear();
       op.put("cardnumber","4111-1111-1111-1111");
       op.put("cardexpmonth","03");
       op.put("cardexpyear","05");
   // add 'creditcard to order
   order.addPart("creditcard", op );

       // Build 'billing'
       op.clear();
       op.put("addrnum","123");
       op.put("zip","12345");
       // add 'billing to order
   order.addPart("billing", op );

        // Build 'payment'
       op.clear();
       op.put("chargetotal","12.99");
   // add 'payment to order
   order.addPart("payment", op );

 return  order.toXML();
}

}