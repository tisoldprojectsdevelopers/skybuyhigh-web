/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class L2PURCHASING_CARD extends JLinkPointSample {
  public L2PURCHASING_CARD() {
  }
  protected String getOrderXML() {
    // Create an empty order
        LPOrderPart  order = LPOrderFactory.createOrderPart("order");

    // Create a part we will use to build the order
        LPOrderPart  op = LPOrderFactory.createOrderPart();

    // Build 'orderoptions'
        op.put("ordertype","SALE");
    // add 'orderoptions to order
    order.addPart("orderoptions", op );

        // Build 'merchantinfo'
        op.clear();
        op.put("configfile",configfile);
    // add 'merchantinfo to order
    order.addPart("merchantinfo", op );

        // Build 'transactiondetails'
        op.clear();
        // If there is no PO Number for this order,
        // pass a department code or other value, but make sure the value you pass is supplied by the customer
        op.put("ponumber","1203A-G4567");
        // If the purchase is tax exempt,
        // pass a value of Y for taxexempt
        op.put("taxexempt","N");
        // add 'billing to order
    order.addPart("transactiondetails", op );

        // Build 'creditcard'
        op.clear();
        op.put("cardnumber","4111-1111-1111-1111");
        op.put("cardexpmonth","03");
        op.put("cardexpyear","05");
    // add 'creditcard to order
    order.addPart("creditcard", op );

         // Build 'payment'
        op.clear();
        op.put("chargetotal","12.99");
        // Tax is required for purchasing cards.
        // If the tax is $0.00, pass a value of 0 for tax
        op.put("tax","0.32");

    // add 'payment to order
    order.addPart("payment", op );

  // Process the order
return order.toXML();
}

}