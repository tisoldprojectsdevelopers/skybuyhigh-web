/*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
'
' This software is the proprietary information of LinkPoint International, Inc.
' Use is subject to license terms.
'
'******************************************************************************/
package lp.samples;
import lp.order.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: LinkPoint International</p>
 * @author Sergey Chudnovsky
 * @version 1.0
 */

public class ITEMS_W_ESD extends JLinkPointSample {
  public ITEMS_W_ESD() {
  }
  protected String getOrderXML() {
    // Create an empty order
       LPOrderPart      order = LPOrderFactory.createOrderPart("order");

   // Create a part we will use to build the order
       LPOrderPart  op = LPOrderFactory.createOrderPart();

   // Build 'orderoptions'
       op.put("ordertype","SALE");
       // live transaction
       op.put("result","LIVE");
   // add 'orderoptions to order
   order.addPart("orderoptions", op );

       // Build 'merchantinfo'
       op.clear();
       op.put("configfile",configfile);
   // add 'merchantinfo to order
   order.addPart("merchantinfo", op );


       // Build 'creditcard'
       op.clear();
       op.put("cardnumber","4111-1111-1111-1111");
       op.put("cardexpmonth","03");
       op.put("cardexpyear","05");
       // add 'creditcard to order
   order.addPart("creditcard", op );

        // Build 'payment'
       op.clear();
       op.put("subtotal","45.98");
       op.put("tax","0.32");
       op.put("shipping","1.02");
       op.put("chargetotal","47.32");
   // add 'payment to order
   order.addPart("payment", op );

       // Build 'billing'
       // Required for AVS. If not provided,
       // transactions will downgrade.
       op.clear();
       op.put("zip","12345");
       op.put("addrnum","123");
   // When using ESD items - name must present
       op.put("name","Joe Customer");
       // add 'billing to order
   order.addPart("billing", op );


  // Create some parts we use to build order itmes
       LPOrderPart  items = LPOrderFactory.createOrderPart();
       LPOrderPart  item = LPOrderFactory.createOrderPart();
       LPOrderPart  options = LPOrderFactory.createOrderPart();

               //  build 'item1'
               item.put("id","123456-A98765");
               item.put("description","Logo T-Shirt");
               item.put("quantity","1");
               item.put("price","12.99");
               item.put("serial","0987654321");

               // build item's options
                   op.clear();
                       op.put("name","Color");
                       op.put("value","Red");
                       options.addPart("option",op,1);

                       op.clear();
                       op.put("name","Size");
                       op.put("value","XL");
                       options.addPart("option",op,2);

               // add 'options' to item
               item.addPart("options", options );
         // add 'item' to 'items' collection
     items.addPart("item", item, 1 );
               //  build 'item2'
               item.clear();
               item.put("id","123456-A98767");
               item.put("description","Blast-Em Software");
               item.put("price","32.99");
               item.put("quantity","1");
               item.put("serial","0987654321");
               item.put("esdtype","softgood");
               item.put("softfile","blastem.exe");

         // add 'item' to 'items' collection
     items.addPart("item", item, 2 );

   // add 'items' to order
   order.addPart("items", items );

 return order.toXML();
 }

}