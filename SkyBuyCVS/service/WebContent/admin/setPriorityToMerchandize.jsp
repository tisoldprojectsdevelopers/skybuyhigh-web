<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

	function textLimit(field,maxlen,dispName) {
	   fieldLen=trim((field.value)).length;
	  if (fieldLen > parseInt(maxlen) + 1){
			alert(dispName+" can have maximum of "+maxlen+" chars only."); 
			return false;
		}else
			return true;
	}
	function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
	function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}

	function triggerEvent() {
		if(event.keyCode==13) {
		fnCallSearch();
		}           
	}
	function fnValidateName(jsName,jsLabelName) {		
			if(isEmpty(jsName)){
				alert("Please enter "+jsLabelName);
				document.forms[0].searchValue.focus();
				return false;
			}else{
				document.forms[0].searchValue.value = trim(document.forms[0].searchValue.value);	
				document.forms[0].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority";
				document.forms[0].submit();			
			}
		}	
	function fnValidateId(jsId,jsLabelName) {		
			if(isEmpty(jsId)){
				alert("Please enter valid "+jsLabelName);
				document.forms[0].airlineSearchValue.focus();
				return false;
			}else if(!isNumber(jsId)){
				alert("Please enter valid "+jsLabelName);
				document.forms[0].airlineSearchValue.focus();
				return false;
			}else{	
				document.forms[0].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority";
				document.forms[0].submit();			
			}
		}
	function fnCallSearch(){
		if(document.forms[0].searchBy.value=='VendorName'){
			fnValidateName(document.forms[0].searchValue,"Vendor Name");
		}else if(document.forms[0].searchBy.value=='AirlineName'){
			fnValidateName(document.forms[0].searchValue,"Airline Name");
		}
		else if(document.forms[0].searchBy.value=='ItemCode'){
			fnValidateName(document.forms[0].searchValue,"Item Code");
		}else if(document.forms[0].searchBy.value=='All' || document.forms[0].searchBy.value=='Vendor' || document.forms[0].searchBy.value=='Airline'){
			document.forms[0].searchValue.value = '';
			document.forms[0].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority";
			document.forms[0].submit();	
		}else{
			document.forms[0].searchValue.value = trim(document.forms[0].searchValue.value);	
			document.forms[0].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority";
			document.forms[0].submit();			
		}
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function trimOtherThanNumber(jsPriorityOfMerchandize) {
		var priorityRegEx = new RegExp("^[0-9]*$");
		var returnValue = jsPriorityOfMerchandize;
		var startChar = returnValue.substring(0,1);
		while(!priorityRegEx.test(startChar)) {
			returnValue = returnValue.substring(1, returnValue.length);
			startChar = returnValue.substring(0,1);
		}
		startChar = returnValue.substring(returnValue.length-1, returnValue.length);
		while(!priorityRegEx.test(startChar)) {
			returnValue = returnValue.substring(0, returnValue.length-1);
			startChar = returnValue.substring(returnValue.length-1, returnValue.length);
		}
		return returnValue;
	}
	
	function isValidNumber(field) {
		var priorityOfProduct = field.value;
		var priorityRegEx = new RegExp("^[0-9]*$");
		
		field.value = trim(priorityOfProduct);
		field.value = trimQuantity(priorityOfProduct);
		
		if(!priorityRegEx.test(field.value)) {
			
			field.value = trimOtherThanNumber(priorityOfProduct);
			alert('Enter a valid number');
		}
	}
	
	function fnCallSavePriorityToMerchandize() {
		document.forms[0].action="updatePriorityOfMerchandize.do?method=updatePriorityOfMerchandize";
		document.forms[0].submit();
	}
	
	function fnCallSetPriorityToMerchandize() {
		document.forms[0].action="initSetPriorityToMerchandize.do?method=initSetPriorityToMerchandize";
		document.forms[0].submit();
	}	


/********gtky search end *****/	
	
</script>
<html:form action="admin/initSetPriorityToMerchandize" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Prioritize Merchandise</li>
				</ul>
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
    	<logic:present name="ErrorMsg">
	      <tr align="center">
	      <td class="error" align="center">
	      	<c:out value="${ErrorMsg}"/>
	      <br></td>
	      </tr>
	    </logic:present>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap">
				<html:select property="searchBy" styleClass="textarea2" name="setPriorityMerchandizeForm">
					<html:option value="All">All</html:option>	
					<html:option value="Vendor">All  Vendor</html:option>	
					<html:option value="VendorName">Vendor Name</html:option>	
					<html:option value="Airline">All  Air Charter</html:option>				
					<html:option value="AirlineName">Air Charter Name</html:option>							  		
					<html:option value="ItemCode">Item Code</html:option>	
              </html:select>            </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Category :</td>
            <td nowrap="nowrap"><html:select property="category" styleClass="textarea2">
                <html:option value="All">All</html:option>
                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
				<html:option value="20">Private Jet Jaunts</html:option>	
            </html:select></td>
			  
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="2">
            <tr>
              <td nowrap="nowrap" class="lable">Approval Status :</td>
            	<td nowrap="nowrap"><html:select property="sbhStatus" styleClass="textarea2">
	              <html:option value="All">All</html:option>
	              <html:option value="N">Pending</html:option>
	              <html:option value="A">Accepted</html:option>
	              <html:option value="R">Rejected</html:option>
	            </html:select>
	          </td>
              <td nowrap="nowrap" class="lable">Show in Catalogue</td>
              <td class="lable">:</td>
              <td nowrap="nowrap">
			  <html:select property="prodStatus" styleClass="textarea2">
			   	   <html:option value="All">All</html:option>
                  <html:option value="A">Yes</html:option>
                  <html:option value="I">No</html:option>
              </html:select></td>
               <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
            </tr>
          </table>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td colspan="3" class="td" align="center" valign="top">
    <logic:present  name="setPriorityMerchandizeForm" property="recordSet"> 
 	 <logic:notEmpty name="setPriorityMerchandizeForm" property="recordSet">	
	<table border="0" width="90%" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Merchandise Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
          <td align="center" class="table_header">Partner Type</td>
          <td class="table_header" nowrap="nowrap">Name</td>
          <td height="32" class="table_header">Item</td>
            <td class="table_header">Description </td>
            <td class="table_header" nowrap="nowrap">Retail Price</td>  
			 <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup> Price</td>        
             <td class="table_header" nowrap="nowrap">Show in <br/>Catalogue</td>
			 <td class="table_header" nowrap="nowrap">Approval <br/> Status</td>
			   <td nowrap="nowrap" class="table_header">Last Approval<br/> Date </td>
			   <td class="table_header">Comments</td>
              
            <td align="center" class="table_header">Priority</td>           
          </tr>
		  <logic:iterate name="setPriorityMerchandizeForm" property="recordSet" id="MerchandizeInfo"  indexId="ctr">
          <tr class="tdbg">
          <td align="left"><div class="lable"> <c:if test="${MerchandizeInfo.ownerType eq 'vendor'}">
		  Vendor
		  </c:if>
		  <c:if test="${MerchandizeInfo.ownerType eq 'airline'}">
		  Airline
		  </c:if></div></td>
          <td align="left" nowrap="nowrap"><div class="lable" ><bean:write name="MerchandizeInfo" property="ownerName"/></div></td>
            <td align="left" valign="center" class="catagory"><div align="center">
			<c:set var="imgpath"><bean:write name='MerchandizeInfo' property='mainImgPath'/></c:set>
			
			
			<html-el:img src="${MerchandizeInfo.mainImgPath}" width="79" height="79"  alt="${MerchandizeInfo.prodCode}"/>
            </div>
              <div class="lable"><bean:write name="MerchandizeInfo" property="prodCode"/></div>			  </td>
            <td align="left">
			 <div style="width:250px; height:60px; word-wrap: break-word; overflow:auto;" >
			<span class="lable"><bean:write name="MerchandizeInfo" property="brandName"/></span><br/>
                <span class="lable"><bean:write name="MerchandizeInfo" property="prodTitle"/></span><br/>
              <div class="desc">     
						
						 <logic:present name="MerchandizeInfo">
							<logic:notEmpty name="MerchandizeInfo">
						 		 <c:out value="${MerchandizeInfo.shortDesc}" escapeXml="false"/>
							 </logic:notEmpty>
					  	</logic:present>	
			</div>
              </div></td>
            <td align="right" valign="middle" nowrap="nowrap"><div class="lable"><fmt:setLocale value="en_US" /><fmt:formatNumber value="${MerchandizeInfo.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>	</div>              </td>
           <td align="right"><div class="lable">
		   <c:if test="${MerchandizeInfo.sbhPrice ne '0.00'}">
		   <fmt:formatNumber value="${MerchandizeInfo.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
		   </c:if>
		   </div></td>
            <td align="center"><div class="lable">
			<c:if test="${MerchandizeInfo.inShopStatus eq  'Active'}">
			Yes
			</c:if>
			<c:if test="${MerchandizeInfo.inShopStatus eq 'InActive'}">
			No
			</c:if>
			</div></td>
           <td><div class="lable">
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'A'}">Accepted</c:if>
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'R'}">Rejected</c:if>
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'N'}">Pending</c:if>
		   </div></td>
		     <td><div class="lable"><bean:write name="MerchandizeInfo" property="adminApprovalDt"/></div></td>
			 <td align="center" valign="middle"><div class="lable" >
			  <div style="width:150px; height: 60px; word-wrap: break-word; overflow:auto;" >
			 <bean:write name="MerchandizeInfo" property="sbhComment"/>
			 </div>
			 </div></td>
           
           		<td align="center" >
           			<html-el:text property="merchandizeDetails[${ctr}].priorityOfMerchandize" value="${MerchandizeInfo.priorityOfMerchandize}" styleClass="search-input" onkeyup="javascript:isValidNumber(this);" onchange="javascript:isValidNumber(this);"/>
           		</td>		
            </tr>
		   </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="setPriorityMerchandizeForm" property="recordSet"> 
	 <logic:notEmpty name="setPriorityMerchandizeForm" property="recordSet">	
  <tr>
      <td height="50" colspan="3" align="center" class="td">
      	<html:button property="method" onclick="javascript:fnCallSavePriorityToMerchandize();" styleClass="button" value="Save" />
      	<html:button property="method" onclick="javascript:fnCallSetPriorityToMerchandize();" styleClass="button" value="Cancel" />
      </td>
    </tr>
	</logic:notEmpty>
	</logic:present>
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
    