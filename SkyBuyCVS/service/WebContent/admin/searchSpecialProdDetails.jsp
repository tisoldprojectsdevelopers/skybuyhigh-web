<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormat1(datevar);
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
		
}

function dateFormat1(sDate){
	var parseYr;
	var yl=1990;
	var ym=2200;
		if(sDate.length==8 || sDate.length==10  ){
			if(sDate.length==10){
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
		   /*else if(sDate.length==6){
				var yTemp = sDate.substring(4,6);				
				y="20"+yTemp;
				m = sDate.substring(0,2);				
				d = sDate.substring(2,4);				
			}	*/
	if (y<yl || y>ym) {
		parseYr = y+""+m+""+d;
		return parseYr;
	}
	else
		return sDate;
	}
	return sDate;
}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
	fnCallSearch();
	}           
}
function fnValidateName(jsName,jsLabelName) {		
		if(isEmpty(jsName)){
			alert("Please enter "+jsLabelName);
			document.forms[0].searchValue.focus();
			return false;
		}else{	
			document.forms[0].action="searchSpecialProduct.do?method=searchSpecialProducts";
			document.forms[0].submit();			
		}
	}	
function fnValidateId(jsId,jsLabelName) {		
		if(isEmpty(jsId)){
			alert("Please enter valid "+jsLabelName);
			document.forms[0].searchValue.focus();
			return false;
		}else if(!isNumber(jsId)){
			alert("Please enter valid "+jsLabelName);
			document.forms[0].searchValue.focus();
			return false;
		}else{	
			document.forms[0].action="searchSpecialProduct.do?method=searchSpecialProducts";
			document.forms[0].submit();			
		}
	}
function fnCallSearch(){

	if(trim(document.forms[0].uploadFromDate.value) != "" && !isDate(document.forms[0].uploadFromDate.value)){
		alert("Please enter valid From Date ");
		document.forms[0].uploadFromDate.focus();			
		return false;
	}
	else if(trim(document.forms[0].uploadToDate.value) != "" && !isDate(document.forms[0].uploadToDate.value)){	
		alert("Please enter valid To Date ");
		document.forms[0].uploadToDate.focus();			
		return false;
	}else if(trim(document.forms[0].uploadToDate.value) == ""){	
		alert("Please enter valid To Date ");
		document.forms[0].uploadToDate.focus();			
		return false;
	}else if(document.forms[0].searchBy.value=='VendorName'){
		fnValidateName(document.forms[0].searchValue,"Vendor Name");
	}else if(document.forms[0].searchBy.value=='AirlineName'){
		fnValidateName(document.forms[0].searchValue,"Air Charter Name");
	}
	else if(document.forms[0].searchBy.value=='ItemCode'){
		fnValidateName(document.forms[0].searchValue,"Item Code");
	}else{	
		
		document.forms[0].action="searchSpecialProduct.do?method=searchSpecialProducts";
		document.forms[0].submit();			
	}
	
}
function fnCallEdit(jsProdIdValue) {
	document.forms[0].action="specialProduct.do?method=editSpecialProducts&ProdId="+jsProdIdValue;
	document.forms[0].submit();

}
function fnCallDelete(jsProdIdValue) {
	var flag=false;
	flag=confirm('Are you sure you want to delete this Item');
	if(flag){
	document.forms[0].action="searchSpecialProduct.do?method=deleteSpecialProducts&ProdId="+jsProdIdValue;
	document.forms[0].submit();
	}

}
function fnCallAccept(jsProdIdValue,jsSbhPrice) {
	document.forms[0].action="acceptSpecialProduct.do?method=acceptMerchandise&prodId="+jsProdIdValue+"&sbhPrice="+jsSbhPrice;
	document.forms[0].submit();
}

function fnCallReject(jsProdIdValue) {
	document.forms[0].action="acceptSpecialProduct.do?method=rejectMerchandise&prodId="+jsProdIdValue;
	document.forms[0].submit();
}

function fnCallHome() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();
}

function fnCallInitSearchMerchandize() {
	document.forms[0].action="searchSpecialProduct.do?method=initSearchSpecialProducts";
	document.forms[0].submit();
}

/********gtky search end *****/	
	
</script>
<html:form action="admin/searchMerchandize" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Edit/Search Checkout Product</li>
				</ul>
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap">
				<html:select property="searchBy" styleClass="textarea2" name="searchMerchandizeForm">
					<html:option value="All">All</html:option>	
					<html:option value="ItemCode">Item Code</html:option>	
              </html:select>            </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Approval Status :</td>
            <td nowrap="nowrap"><html:select property="sbhStatus" styleClass="textarea2">
              <html:option value="All">All</html:option>
              <html:option value="N">Pending</html:option>
              <html:option value="A">Accepted</html:option>
              <html:option value="R">Rejected</html:option>
            </html:select></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="2">
            <tr>
              <td class="lable">Upload Date</td>
              <td class="lable">:</td>
              <td class="lable">From</td>
              <td><html:text property="uploadFromDate"  styleClass="search-input" onkeydown="triggerEvent();"/>
                  <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('uploadFromDate', false, 'mdy', '/');" /></td>
              <td class="lable">To</td>
              <td><html:text property="uploadToDate"  styleClass="search-input" onkeydown="triggerEvent();" />
                  <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('uploadToDate', false, 'mdy', '/');" /></td>
              <td nowrap="nowrap" class="lable">Show in Catalogue</td>
              <td class="lable">:</td>
              <td nowrap="nowrap">
			  <html:select property="prodStatus" styleClass="textarea2">
			   	   <html:option value="All">All</html:option>
                  <html:option value="A">Yes</html:option>
                  <html:option value="I">No</html:option>
              </html:select></td>
               <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
            </tr>
          </table>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center" valign="top">
	<logic:present  name="searchMerchandizeForm" property="recordSet"> 
 	 <logic:notEmpty name="searchMerchandizeForm" property="recordSet">	
	<table border="0" width="90%" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Checkout Product Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
	          <td height="32" class="table_header" nowrap="nowrap">Owner Type</td>
	          <td class="table_header" nowrap="nowrap">Item Code</td>
	          <td class="table_header" nowrap="nowrap">Brand Name</td>
	          <td class="table_header" nowrap="nowrap">In-Store Price</td>  
			  <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup> Price</td>        
	          <td class="table_header" nowrap="nowrap">Show in <br/>Catalogue</td>
			  <td class="table_header" nowrap="nowrap">Approval <br/> Status</td>
			  <td class="table_header" nowrap="nowrap">Last Approval<br/> Date </td>
			  <td class="table_header" nowrap="nowrap">Comments</td>
           	  <td class="table_header"nowrap="nowrap">Action</td>           
          </tr>
		  <logic:iterate name="searchMerchandizeForm" property="recordSet" id="MerchandizeInfo"  indexId="ctr">
          <tr class="tdbg">
          <td align="left" nowrap="nowrap">
	          <div class="lable"> 
		          <c:if test="${MerchandizeInfo.ownerType eq 'vendor'}">
				  	Vendor
				  </c:if>
				  <c:if test="${MerchandizeInfo.ownerType eq 'airline'}">
				  	Air Charter		  
				  </c:if>
			  </div>
		  </td>
          <td align="left" valign="center" class="catagory">
              <div class="lable"><bean:write name="MerchandizeInfo" property="prodCode"/></div>			  
          </td>
          <td align="left" valign="center" class="catagory">
              <div class="lable"><bean:write name="MerchandizeInfo" property="brandName"/></div>			  
          </td>
              
            <td align="right" valign="middle" nowrap="nowrap"><div class="lable"><fmt:setLocale value="en_US" /><fmt:formatNumber value="${MerchandizeInfo.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>	</div>              </td>
           <td align="right"><div class="lable">
		   <c:if test="${MerchandizeInfo.sbhPrice ne '0.00'}">
		   <fmt:formatNumber value="${MerchandizeInfo.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
		   </c:if>
		   </div></td>
            <td align="center"><div class="lable">
			<c:if test="${MerchandizeInfo.inShopStatus eq  'Active'}">
			Yes			</c:if>
			<c:if test="${MerchandizeInfo.inShopStatus eq 'InActive'}">
			No			</c:if>
			</div></td>
           <td><div class="lable">
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'A'}">Accepted</c:if>
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'R'}">Rejected</c:if>
		   <c:if test="${MerchandizeInfo.sbhProdStatus eq 'N'}">Pending</c:if>
		   </div></td>
		     <td><div class="lable"><bean:write name="MerchandizeInfo" property="adminApprovalDt"/></div></td>
			 <td align="center" valign="middle"><div class="lable" >
			  <div style="width:150px; height: 60px; word-wrap: break-word; overflow:auto;vertical-align: middle;" >
			 <bean:write name="MerchandizeInfo" property="sbhComment"/>
			 </div>
			 </div></td>
           
           <td class="action" align="center">
           		<a href="viewSpecialProduct.do?method=viewSpecialProducts&ProdId=<bean:write name='MerchandizeInfo' property='prodId'/>" title="View Item">View</a>
           		&nbsp;&nbsp;<a href="javascript: void fnCallEdit('<bean:write name='MerchandizeInfo' property='prodId'/>')" title="Edit Item">Edit</a>
           		&nbsp;&nbsp;<a href="javascript: void fnCallDelete('<bean:write name='MerchandizeInfo' property='prodId'/>')" title="Delete Item">Delete</a>
           		<c:if test="${MerchandizeInfo.sbhProdStatus eq 'R'}">
           			&nbsp;&nbsp;<a href="javascript: void fnCallAccept('<bean:write name='MerchandizeInfo' property='prodId'/>','<bean:write name='MerchandizeInfo' property='sbhPrice'/>')" title="Accept Item">Accept</a>
           		</c:if>
           		<c:if test="${MerchandizeInfo.sbhProdStatus eq 'A'}">
           			&nbsp;&nbsp;<a href="javascript: void fnCallReject('<bean:write name='MerchandizeInfo' property='prodId'/>')" title="Reject Item">Reject</a>
           		</c:if>
           </td>		
            </tr>
		   </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="searchMerchandizeForm" property="recordSet"> 
	 <logic:notEmpty name="searchMerchandizeForm" property="recordSet">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnCallInitSearchMerchandize();" styleClass="button">Cancel</html:button></td>
    </tr>
	</logic:notEmpty>
	</logic:present>
  <tr>
  
<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
