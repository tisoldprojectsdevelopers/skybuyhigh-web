<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>





<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script>
window.onload=function() {
		tablecollapse();
	}
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/




function fnCallAddProduct(){
	document.forms[0].action="addProduct.do?method=addProductDetails";
	document.forms[0].submit();
}
function fnCallSaveProduct(){
	document.forms[0].action="updateProduct.do?method=updateProductDetails";
	document.forms[0].submit();
}

function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}

function getCatalogue(link, windowname){
	window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');

}

function fnCallEditMerchandise() {
	document.forms[0].action="editSearchMerchandize.do?method=editSearchMerchandize";
	document.forms[0].submit();
}
function fnCallEdit(jsProdIdValue) {
	document.forms[0].action="editMerchandizeProduct.do?method=editMerchandizeDetails&ProdId="+jsProdIdValue+"&SourceOfEdit=View";
	document.forms[0].submit();
}

</script>
<html:form action="vendor/addVendorProduct" method="post" enctype="multipart/form-data">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Edit/Search Merchandise</li>
				</ul>
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
	  <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Item Summary Page </h2></td>
        </tr>
      <tr>
	    <td colspan="3">
		<table width="100%" border="0" cellpadding="3" cellspacing="0" class="broder_top0">
		
		<tr class="tdbg">
        	<td height="15" colspan="9"  align="right"><div align="right" class="editlink"><a href="javascript: void fnCallEdit('<bean:write name='merchandizeDetails' property='prodId'/>')" title="Edit Merchandise">Edit Merchandise</a></div></td>
        </tr>
        
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap" class="lable"><c:if test="${merchandizeDetails.ownerType eq 'airline'}">Air Charter</c:if><c:if test="${merchandizeDetails.ownerType eq 'vendor'}">Vendor</c:if> Name</td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" nowrap="nowrap"><span class="catagory">
          <bean:write  name="merchandizeDetails" property="ownerName"/>	
      </span></td>
	   <c:if test="${merchandizeDetails.ownerType eq 'vendor'}">
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Category</span></td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
          <logic:iterate id="category" name="Category"scope="application">
            <c:if test="${category.cateId eq merchandizeDetails.cateId}">
              <bean:write name="category" property="cateName" />
            </c:if>
          </logic:iterate>
        </span></td>
		</c:if>
          </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Item Code </span></td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="merchandizeDetails"  property="prodCode" />
        </span></td>
		<c:if test="${merchandizeDetails.ownerType eq 'vendor'}">
            <td width="120" align="left" valign="top" class="lable"><span class="boldtext">Brand Name</span></td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
              <bean:write  name="merchandizeDetails"  property="brandName"/>
            </span></td>
		</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Item Name</span></td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" nowrap="nowrap" class="catagory"><span class="catagory">
              <bean:write  name="merchandizeDetails"  property="prodTitle" />
            </span></td>
            <td width="120" align="left" valign="top" class="lable">Item Status </td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
			  <c:if test="${merchandizeDetails.inShopStatus eq 'A'}">Active</c:if>
			  <c:if test="${merchandizeDetails.inShopStatus eq 'I'}">Inactive</c:if>			  
            </span></td>
		</tr>	
		<c:if test="${merchandizeDetails.ownerType eq 'vendor'}">
		<tr class="tdbg">
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Color</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="size">
          <bean:write  name="merchandizeDetails"  property="color" />
        </td>
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Size</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td width="280" align="left" valign="top" class="size">
          <bean:write  name="merchandizeDetails"  property="size" />
    </td>
          </tr>
          </c:if>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><c:if test="${merchandizeDetails.ownerType eq 'vendor'}"> In-Store Price </c:if>
                <c:if test="${merchandizeDetails.ownerType eq 'airline'}"> Price </c:if>            </td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
              <fmt:setLocale value="en_US" />
              <fmt:formatNumber value="${merchandizeDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
            </span></td>
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">SkyBuy<sup>High</sup> Price</span></td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"><span class="catagory">
              <fmt:formatNumber value="${merchandizeDetails.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
            </span></td>
          </tr>
          
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">PO Percentage </td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory"> <bean:write  name="merchandizeDetails"  property="poPct" /></td>
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable">&nbsp;</td>
            <td align="center" valign="top" class="lable">&nbsp;</td>
            <td width="250" align="left" valign="top">&nbsp;</td>
          </tr>
		  <tr class="tdbg">
            <td width="200" align="left" valign="top" nowrap="nowrap" class="lable">Pre-Authorize Credit Card </td>
            <td align="center" valign="top" class="lable">:</td>
            <td width="250" align="left" valign="top" class="catagory">
            <c:if test="${merchandizeDetails.preAuthCC eq 'Y'}">
            	Yes
            </c:if>
            <c:if test="${merchandizeDetails.preAuthCC eq 'N' || merchandizeDetails.preAuthCC eq null}">
            	No
            </c:if>
            </td>
            <td align="left" valign="top" nowrap="nowrap" class="lable">&nbsp; </td>
            <td align="center" valign="top" class="lable">&nbsp;</td>
            <td width="250" align="left" valign="top">&nbsp;</td>
            
          </tr>
          <c:if test="${merchandizeDetails.ownerType eq 'vendor'}">
	         <tr class="tdbg">
	            <td align="left" class="lable" nowrap="nowrap">Approval Status<br></td>
	            <td class="lable">:</td>
	            <td align="left"><span class="catagory">
	              <c:if test="${merchandizeDetails.sbhProdStatus eq 'N'}">Pending</c:if>
		          <c:if test="${merchandizeDetails.sbhProdStatus eq 'A'}">Accepted</c:if>
		          <c:if test="${merchandizeDetails.sbhProdStatus eq 'R'}">Rejected</c:if>
	            </span></td>
	            
				 <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
				 <td class="lable">&nbsp;</td>
	            <td align="left">&nbsp;</td>
	          </tr>
          </c:if>
		  <c:if test="${merchandizeDetails.ownerType eq 'airline'}">
		   
		<tr class="tdbg">
            <td align="left" class="lable" nowrap="nowrap">Approval Status<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <c:if test="${merchandizeDetails.sbhProdStatus eq 'N'}">Pending</c:if>
	          <c:if test="${merchandizeDetails.sbhProdStatus eq 'A'}">Accepted</c:if>
	          <c:if test="${merchandizeDetails.sbhProdStatus eq 'R'}">Rejected</c:if>
            </span></td>
            
			 <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
			 <td class="lable">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap"  class="lable"><span class="boldtext">Special Instruction</span></td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td colspan="4" align="left" valign="top" style="width:600px; word-wrap:break-word; "><c:out value="${merchandizeDetails.instructions}" escapeXml="false"/></td>
          </tr>
		  
		  
		  </c:if>
		  
		  
		  
          
          <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap"  class="lable">Title</td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td colspan="4" align="left" valign="top" style="width:600px; word-wrap:break-word; "><c:out value="${merchandizeDetails.shortDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap" class="lable">Full Description </td>
            <td width="11" align="center" valign="top" class="lable">:</td>
            <td colspan="4" align="left" valign="top" class="catagory" style="width:600px; word-wrap:break-word; "><c:out value="${merchandizeDetails.longDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" class="lable">Item Image(s)</td>
            <td width="11" align="center" valign="top" class="lable"></td>
            <td colspan="4" align="left" valign="top" class="catagory"></td>
          </tr>
		  
		  <tr align="left" valign="top" class="tdbg" >
		  <td colspan="6" ><table border="0" align="center" cellpadding="2" cellspacing="2">
            <tr>
            <c:if test="${!empty merchandizeDetails.mainImgPath}">	
              <td class="border" align="center"><img src="<c:out value='${merchandizeDetails.mainImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			  <c:if test="${!empty merchandizeDetails.view1ImgPath}">
              <td class="border"  align="center"><img src="<c:out value='${merchandizeDetails.view1ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			  <c:if test="${!empty merchandizeDetails.view2ImgPath}">
              <td class="border"  align="center"><img src="<c:out value='${merchandizeDetails.view2ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			  <c:if test="${!empty merchandizeDetails.view3ImgPath}">
              <td class="border"  align="center"><img src="<c:out value='${merchandizeDetails.view3ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${empty merchandizeDetails.mainImgPath && empty merchandizeDetails.view1ImgPath && empty merchandizeDetails.view2ImgPath && empty merchandizeDetails.view3ImgPath}">
			   	<td class="border" align="center"><img src="<c:out value='${merchandizeDetails.noImgPath}' />" width="79" height="79" /></td>
			   </c:if>
            </tr>
            <tr>
               <c:if test="${!empty merchandizeDetails.mainImgPath}">	<td bgcolor="#d9ecff" class="border"  align="center" style="width:100px;"><bean:write  name="merchandizeDetails"  property="mainImgCap" />&nbsp;</td></c:if>
              <c:if test="${!empty merchandizeDetails.view1ImgPath}"> <td bgcolor="#d9ecff" class="border"><bean:write  name="merchandizeDetails"  property="view1ImgCap" />&nbsp;</td></c:if>
             <c:if test="${!empty merchandizeDetails.view2ImgPath}"> <td bgcolor="#d9ecff" class="border"><bean:write  name="merchandizeDetails"  property="view2ImgCap" />&nbsp;</td></c:if>
              <c:if test="${!empty merchandizeDetails.view3ImgPath}"><td bgcolor="#d9ecff" class="border"><bean:write  name="merchandizeDetails"  property="view3ImgCap" />&nbsp;</td></c:if>
            </tr>
          </table>		  </td>
		  </tr>
		    <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td width="127" align="left" valign="top" nowrap="nowrap" class="lable">Comments</td>
            <td width="11" align="center" valign="top" class="lable">&nbsp;</td>
            <td colspan="4" align="left" valign="top" class="catagory"><!-- <c:out value="${merchandizeDetails.sbhComment}"/> --></td>
          </tr>
          <tr align="left" valign="top" class="tdbg">
					<td colspan="6" style="margin:5px 0 5px 0;">
						<logic:present name="productComments" scope="request">
							<logic:notEmpty name="productComments" scope="request">
								<div id="productComments" style="margin:5px 0 5px 0;">
									<table width="99%"align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
						     			<thead class="tablehead">
						     				<tr  height="21">
						     					<td align="center">
						     						User Id
						     					</td>
						     					<td align="center">
						     						Date
						     					</td>
						     					<td width="70%" align="center">
						     						Comments
						     					</td>
						     				</tr>
						     			</thead>
				
						     			<tfoot>
								          <tr>
								            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
								          </tr>
								        </tfoot>
				
						     			<tbody>
							     			<logic:iterate id="prodComment" name="productComments">
						     					<tr height="20">
													<td align="left">
														<bean:write name="prodComment" property="userId" />
													</td>
													<td align="center" nowrap="nowrap">
														<bean:write name="prodComment" property="date" />
													</td>
													<td align="left" >
														<bean:write name="prodComment" property="comments" />
													</td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
								</div>
							</logic:notEmpty>
						</logic:present>
				  </td>
				</tr>
          <tr align="left" valign="top" class="tdbg">
            <td colspan="6" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
		   <c:if test="${merchandizeDetails.inShopStatus eq 'A'}">
          <tr align="left" valign="top" class="tdbg">
           <td colspan="6" class="clickhere"><a href="javascript:getCatalogue('../previewProduct.jsp','merchandizeproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue</td>
            </tr>
			</c:if>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center">  <!--<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none">&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</html:link>-->
        	<html:button property="method" onclick="javascript:fnCallEditMerchandise();" value="OK" styleClass="button"></html:button>
        </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
	<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
   </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
