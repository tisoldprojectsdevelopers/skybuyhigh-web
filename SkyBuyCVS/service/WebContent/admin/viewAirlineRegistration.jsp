<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>

<script type="text/javascript">
	window.onload=function() {
		tablecollapse();
	}
	function fnCallEdit(jsAirIdValue) {
		document.forms[0].action="editAirlineReg.do?method=editAirlineDetails&airId="+jsAirIdValue+"&SourceOfEdit=View";
		document.forms[0].submit();
	}
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
	function getCatalogue(link, windowname){
		window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');
	}
</script>

<html:form action="admin/addAirlineReg" method="post" >
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<!--Admin -->
		<logic:present name="adminLoginInfo" scope="session">	
		<c:if test="${adminLoginInfo.userType eq 'admin' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Air Charter</li>
		</ul>
		</c:if>
		</logic:present>
		
		<logic:present name="adminLoginInfo" scope="session">	
		<c:if test="${adminLoginInfo.userType eq 'admin'  && mode eq 'airlineAdd'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Air Charter</li>
		</ul>
		</c:if>
		</logic:present>
		</div>			
		</div>

	</td>
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">

	<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
	
        <td colspan="3" class="tablehead" align="center"><h2>Air Charter Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="6" cellspacing="0" class="broder_top0">
		<tr class="tdbg">
        <td colspan="6"align="right"><div  class="editlink" align="right"><a href="javascript: void fnCallEdit('<bean:write name='airlineDetails' property='airId'/>')" title="Edit Air Charter Info">Edit Air Charter Info</a></div></td>
        </tr>
      <tr>
       	<tr class="tdbg">
      	  <td align="left" nowrap="nowrap" class="lable" valign="top">Air Charter Name </td>
      	  <td align="left" class="lable" valign="top">:</td>
      	  <td width="250" align="left" valign="top"><span class="catagory">
            <bean:write  name="airlineDetails" property="airName" />
          </span></td>
      	  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name </span></td>
      	  <td align="left" class="lable">:</td>
      	  <td width="250" align="left"><span class="catagory">
            <bean:write  name="airlineDetails" property="airContactName" />
          </span></td>
      	</tr>
      	<tr class="tdbg">
      	  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Status </span></td>
      	  <td align="left" class="lable">:</td>
      	  <td width="250" align="left"><span class="catagory">
            <c:if test="${airlineDetails.airStatus eq 'A'}"> Active </c:if>
            <c:if test="${airlineDetails.airStatus eq 'I'}"> Inactive </c:if>
          </span></td>
      		<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Charge Type  </span></td>
      		<td align="left" class="lable">:</td>
      		<td width="250" align="left"><span class="catagory">
              <c:if test="${airlineDetails.chargeType eq 'FA'}"> Automated </c:if>
              <c:if test="${airlineDetails.chargeType eq 'SA'}"> Semi Automated </c:if>
            </span></td>
      	</tr>
       <tr class="tdbg">
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address</span></td>
         <td width="10" align="left" class="lable">:</td>
         <td width="250" align="left" valign="top" style="word-wrap:break-word;width:200px;"><span class="catagory">
           <bean:write  name="airlineDetails" property="airAddr1" />
         </span></td>
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
         <td width="10" align="left" class="lable">:</td>
         <td width="250" align="left" valign="top" style="word-wrap:break-word;width:150px;"><span class="catagory">
           <bean:write  name="airlineDetails" property="airAddr2" />
         </span></td>
       </tr>
       <tr class="tdbg">
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
         <td width="10" align="left" class="lable">:</td>
         <td width="250" align="left"><span class="catagory">
           <bean:write  name="airlineDetails" property="airCity" />
         </span></td>
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">State </span></td>
         <td width="10" align="left" class="lable">:</td>
         <td width="250" align="left"><span class="catagory">
         
			  <logic:iterate id="states" name="StateList" scope="application">
			<c:if
				test="${states.stateCode eq airlineDetails.airState}">
				<bean:write name="states" property="stateName" />
			</c:if>
			</logic:iterate>	
	         </span></td>
       </tr>
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airCountry" />
        </span></td>
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airZip"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone  </td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap="nowrap"> <span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhone1" />
              </span></td>
			  <c:if test="${!empty airlineDetails.airPhoneExt1 && airlineDetails.airPhoneExt1 ne ''}">
              <td class="lable">Ext :</td>
              <td><span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhoneExt1" />
              </span></td>
			  </c:if>
            </tr>
        </table></td>
        <td align="left" class="lable">Mobile</td>
            <td class="lable">:</td>
            <td width="250" align="left"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td ><span class="catagory">
                    <bean:write  name="airlineDetails" property="airMobile1" />
                  </span></td>
                </tr>
            </table></td>
          </tr>
     
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Fax</span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airFax"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Email </span></td>
        <td align="left" class="lable">:</td>
        <td width="250" align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airEmail" />
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext"> PO Percentage</span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="poPct"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Commission Percentage </span></td>
        <td align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airCommPct" />
        </span></td>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Upload Air Charter Info </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr>
      	 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Logo  </span></td>
	     <td align="left" class="lable">:</td>
      	 <td align="right" valign="top" nowrap="nowrap" class="lable"><c:if test="${ !empty airlineDetails.airlineLogoPath  && airlineDetails.airlineLogoPath ne ''}"><img src="<c:out value='${airlineDetails.airlineLogoPath}' />" width="178" height="52" /></c:if></td>
      </tr>
      <c:if test="${!empty airlineDetails.aboutMePath and airlineDetails.aboutMePath ne ''}">
      <tr>
      	 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">About Me  </span></td>
	     <td align="left" class="lable">:</td>
      	 <td align="left" valign="top" nowrap="nowrap" colspan="8" class="lable">
      	 	      <a class="clickhere" href="javascript:getCatalogue('../previewAboutMe.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue
	     </td>
      </tr>
      </c:if>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Second Person Contact Details </td>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>  
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airContactName2"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Email</span></td>
        <td align="left" class="lable">:</td>
        <td width="250" align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airEmail2" />
        </span></td>
        </tr>
        <tr class="tdgb">
        
        <td align="left" class="lable">Phone</td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap="nowrap"><span class="catagory">
                <bean:write  name="airlineDetails" property="airPhone2" />
              </span></td>
			    <c:if test="${!empty airlineDetails.airPhoneExt2 && airlineDetails.airPhoneExt2 ne ''}">
              <td class="lable">Ext :</td>
              <td nowrap="nowrap"><span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhoneExt2" />
              </span></td>
			  </c:if>
            </tr>
        </table></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Mobile</span></td>
        <td class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
        <bean:write  name="airlineDetails" property="airMobile2" />          
        </span><br></td>
      
        <tr class="tdbg">
          <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
        <tr class="tdbg">
          <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Customer Service</td>
        </tr>
        <tr class="tdbg">
          <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
 		<tr class="tdbg">
        <td align="left" class="lable">Phone</td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap="nowrap"><span class="catagory">
                 <bean:write  name="airlineDetails" property="custServicePhoneno" />
              </span></td>
			  <c:if test="${!empty airlineDetails.custServicePhonenoext && airlineDetails.custServicePhonenoext ne ''}">
              <td class="lable">Ext :</td>
              <td nowrap="nowrap"><span class="catagory">
                 <bean:write  name="airlineDetails" property="custServicePhonenoext" />
              </span></td>
			  </c:if>
            </tr>
           </table></td>
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Email </span></td>
        <td align="left" class="lable">:</td>
        <td width="250" align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="custServiceEmail" />
        </span></td>
             </tr> 
	   <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Return Details </td>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>  
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnAddr1"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnAddr2" />
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnCity"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span></td>
        <td align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
           <logic:iterate id="states" name="StateList" scope="application">
			<c:if
				test="${states.stateCode eq airlineDetails.returnState}">
				<bean:write name="states" property="stateName" />
			</c:if>
			</logic:iterate>	
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnCountry"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable">Zip</td>
        <td align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnZip" />
        </span></td>
      </tr>
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Return Days</span></td>
        <td width="10" align="left" class="lable">:</td>
        <td width="250" align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="returnDays"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
        <td align="left" class="lable">&nbsp;</td>
        <td width="250" align="left" nowrap="nowrap">&nbsp;</td>
        </tr>	 
			 
          <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
            </tr>  
        <tr class="tdbg">
        <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Restrictions/Validity/Cancellation</span></td>
        <td width="10" align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left" style="word-wrap:break-word;width:700px;" valign="top"> <bean:write  name="airlineDetails" property="airReturnPolicy"/></td>
        </tr>   
		<tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
         </tr>
         <logic:present name="AirlineComments" scope="request">
			<logic:notEmpty name="AirlineComments" scope="request">   
		      <tr class="tdbg">
		        <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
		        <td width="10" colspan="5" align="left" valign="top" class="lable">&nbsp;</td>
		     </tr>
		     <tr class="tdbg">
		        <td colspan="6" align="left" valign="top"> 
					<div id="AirlineComments" style="margin:5px 0 5px 0;">
						<table width="100%"align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
			     			<thead class="tablehead">
			     				<tr  height="21">
			     					<td align="center" nowrap="nowrap">
			     						User Id
			     					</td>
			     					<td align="center">
			     						Date
			     					</td>
			     					<td width="70%" align="center">
			     						Comments
			     					</td>
			     				</tr>
			     			</thead>
	
			     			<tfoot>
					          <tr>
					            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
					          </tr>
					        </tfoot>
	
			     			<tbody>
				     			<logic:iterate id="airlineComment" name="AirlineComments">
			     					<tr height="20">
										<td align="left">
											<bean:write name="airlineComment" property="createId" />
										</td>
										<td align="center" nowrap="nowrap">
											<bean:write name="airlineComment" property="createDate" />
										</td>
										<td align="left" >
											<bean:write name="airlineComment" property="comments" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</div>
        			</td>
        		</tr>
				<tr align="left" valign="top" class="tdbg">
	              	<td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
	            </tr>
        	</logic:notEmpty>
		</logic:present>	      	
    </table>
      </tr>
      <tr>
         <td height="50" colspan="3" align="center">
		<!--<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none"> &nbsp;&nbsp;&nbsp; OK&nbsp;&nbsp;&nbsp;   </html:link>-->
			<html:button property="method" onclick="javascript:fnCallHome();" value="OK" styleClass="button"/>
		 </td>
       
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
  <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>

</html:form>
