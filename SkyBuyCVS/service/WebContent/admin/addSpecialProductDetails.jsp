<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}
function validatePrice(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		if(startChar=="$") { 
			retVal=retVal.substring(1,retVal.length);
		} 
		var startChar=retVal.substring(0,1);
		while(startChar=="0") { 
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1); 
		} 
		var startChar=retVal.substring(0,1);
		if(startChar==".") { 
			retVal="0"+retVal; 
		} 
		if(retVal>=0.01)
		  return false;
	    else
		    return true;       

}
function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }
 
 function getSwfFile(imagePath,jsField){
	if(imagePath==''){
	    <c:if test="${mode eq 'editSpecialProduct' }">
	    	return true;
	    </c:if>
	    <c:if test="${mode eq 'addSpecialProduct' }">
	    	alert('Please select checkout product file');
			return false;
	    </c:if>
		
	}
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".swf") || (fileType == ".SWF")) {
		return true;
	} else {
		alert("We supports .SWF formats. "+jsField+" file-type is " + fileType );
		return false;
	}
}
function fnCallUpdateProduct(){
	
	 <c:if test="${mode eq 'editSpecialProduct' }">
	 var poPct = document.forms[0].poPct.value;
	    	if(poPct==""){
				alert("Please enter PO Percentage");
				document.forms[0].poPct.focus();	
				return;
			}else if(isNaN(poPct)){
				alert("Please enter valid PO Percentage");
				document.forms[0].poPct.focus();	
				return;
			}else if(!(poPct>=0.00 && poPct <=100.00)){
				alert("Please enter valid PO Percentage");
				document.forms[0].poPct.focus();	
				return;
			}else if(document.forms[0].sbhComment.value == ""){
				alert("Please enter comments");
				document.forms[0].sbhComment.focus();	
				return;
			}
	 </c:if>
	
	
	if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}else if(document.forms[0].sbhPrice.value==""){
		alert("Please enter SkyBuy Price");
		document.forms[0].sbhPrice.focus();	
		return;
	}
	else if(document.forms[0].sbhPrice.value!="" && validatePrice(document.forms[0].sbhPrice.value)){
		alert("Please enter valid SkyBuy Price");
		document.forms[0].sbhPrice.focus();	
		return;
	}	
	else{
	
	if(document.forms[0].vendPrice.value.charAt(0)=="$")
		document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
		
	if(document.forms[0].sbhPrice.value.charAt(0)=="$")
		document.forms[0].sbhPrice.value=(document.forms[0].sbhPrice.value).substring(1,document.forms[0].sbhPrice.value.length)
		
	/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/ 
	
	/*document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
	
	var swfPath = document.forms[0].specialProdPath.value;
	
	if(getSwfFile(swfPath , "Checkout Product")) {
		document.forms[0].action="specialProduct.do?method=updateSpecialProducts";
		document.forms[0].submit();
	}else{
		return false;
	}
	
	
		
	}
}
function fnCallAddProduct(){
	/*var shortDescValue=tinyMCE.get('shtDesc').getContent();	
	shortDescValue=(stripTags(shortDescValue));
	
	var longDescValue=tinyMCE.get('longDesc').getContent();	
	longDescValue=(stripTags(longDescValue));  */  
	
	if(document.forms[0].ownerId.value==""){
		alert("Please select vendor");
		document.forms[0].ownerId.focus();	
		return;
	}else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}
	else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}
	else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].sbhPrice.value==""){
		alert("Please enter SkyBuy Price");
		document.forms[0].sbhPrice.focus();	
		return;
	}
	else if(document.forms[0].sbhPrice.value!="" && validatePrice(document.forms[0].sbhPrice.value)){
		alert("Please enter valid SkyBuy Price");
		document.forms[0].sbhPrice.focus();	
		return;
	}
	else{
	
	if(document.forms[0].vendPrice.value.charAt(0)=="$")
		document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
		
	if(document.forms[0].sbhPrice.value.charAt(0)=="$")
		document.forms[0].sbhPrice.value=(document.forms[0].sbhPrice.value).substring(1,document.forms[0].sbhPrice.value.length)
	/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/ 
	
	/*document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
	
	var swfPath = document.forms[0].specialProdPath.value;
	
	if(getSwfFile(swfPath , "Checkout Product")) {
		document.forms[0].action="specialProduct.do?method=addSpecialProducts";
		document.forms[0].submit();
	}else{
		return false;
	}
	
	
		
	}
}
function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}
function fnCallHome(jsProdId) {
	<c:if test="${!empty SourceOfEdit and SourceOfEdit eq 'View'}">
		document.forms[0].action="viewSpecialProduct.do?method=viewSpecialProducts&ProdId="+jsProdId;
		document.forms[0].submit();
	</c:if>
	<c:if test="${empty SourceOfEdit or SourceOfEdit ne 'View'}">
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	</c:if>
}

function getCatalogue(link, windowname){
	window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');

}
</script>
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    
        <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>
		<c:if test="${mode eq 'editSpecialProduct' }">
	    	Edit/Search Checkout Product
	    </c:if>
	    <c:if test="${mode eq 'addSpecialProduct' }">
	    	Add Checkout Product
	    </c:if>
		 </li>
				</ul>
			</div>			
		</div>
		<!-- end nav-header -->
	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="admin/specialProduct" method="post" enctype="multipart/form-data">
	<logic:present name="errorMsg">
	<table width="500">
		<tr>
	    	<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
		</tr>
	</table>
  </logic:present>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Checkout Product  Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="4" bgcolor="#FFFFFF" class="broder_top0">
          <tr class="tdbg">
            <td align="left" class="lable">Vendor Name</td>
            <td width="4%" class="lable">:</td>
            <td align="left"><span class="catagory">
            <c:if test="${mode eq 'addSpecialProduct' }">
            <html:select property="ownerId" styleClass="textarea2">
            	<html:option value="">--Select Vendor--</html:option>
                <html:options collection="AllVendorNames" property="vendId" labelProperty="vendName"></html:options>
              </html:select> 
           </c:if>
           <c:if test="${mode eq 'editSpecialProduct' }">
           <c:out value="${SpecialProductDetails.ownerName}"/>
			<html:hidden property="ownerId" />		
			</c:if>	
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"><br></span></td>
            <td width="5%" class="lable"></td>
            <td align="left"><span class="catagory">
              
			  <input type="hidden" name="cateId" value="6" />	
			 </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code *<br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="prodCode" styleClass="input" tabindex="2" maxlength="15"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Brand Name *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="brandName" styleClass="input" tabindex="3" maxlength="63"/>
            </span></td>
            
          </tr>
          <tr class="tdbg">
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="inShopStatus" styleClass="textarea2" tabindex="3">
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
              </html:select>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Color<br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="color" styleClass="input" tabindex="4" maxlength="250"/>
            </span></td>
            
          </tr>
          <tr class="tdbg">
          <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Size<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="size" styleClass="input" tabindex="5" maxlength="250"/>
            </span></td>
            <td align="left" class="lable" nowrap="nowrap">Retail Price (in US$) *<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);" tabindex="6"/>
            </span></td>
			
			</tr>
			<tr class="tdbg">
			<td align="left" class="lable" nowrap="nowrap">SkyBuy Price (in US$) *<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="sbhPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);" tabindex="7"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Pre-Authorize Credit Card</td>
          	<td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:checkbox  property="preAuthCC" styleClass="checkboxHTML" value="Y"  tabindex="8"/> Yes
            </span></td>
          </tr>
          <tr class="tdbg">
          	<c:if test="${mode eq 'editSpecialProduct' }">
				<td align="left" nowrap="nowrap" class="lable">PO Percentage * <br></td>
	            <td class="lable">:</td>
	            <td align="left"><span class="catagory">
	               <html:text property="poPct" styleClass="input" tabindex="9" maxlength="6"/>
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
				<td align="left" valign="top" nowrap="nowrap" class="lable"> 
					Checkout Product<span class="boldtext">* </span>
				</td>
				<td valign="top" class="lable">
					:
				</td>
				<td colspan="4" align="left">
					<html:file property="specialProdPath" accept="application/swf" styleClass="browse" tabindex="10"/><BR/>
					<c:if test="${mode eq 'editSpecialProduct' }">
						<a class="clickhere" href="javascript:getCatalogue('../previewAirlinePackage.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue.<br/>
					</c:if>
					(Upload only the Checkout Product file with an extension .swf.)
				</td>
			</tr>
			<c:if test="${mode eq 'editSpecialProduct' }">
			<tr valign="top" class="tdbg">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Comments *<br></span></td>
	            <td class="lable">:</td>
	            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
	        </span><textarea name="sbhComment"  class="textarea"  rows="5" onkeyup="textLimit(this.form.sbhComment,this.form.sbhCommentLen,250,'Comments');"  id="shtDesc" style="width:520px;" tabindex="11" ></textarea>
			 <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="sbhCommentLen" size="3" maxlength="3" value="250" class="wordcount"/> 
		</td>
            </tr>
            </c:if>
      </table>
      </tr>
	  
      <tr>
        <td height="50" colspan="3" align="center">
        <c:if test="${mode eq 'addSpecialProduct' }">
			<html:button property="method" styleClass="button" onclick="javascript:fnCallAddProduct()" tabindex="12"> Add </html:button>
		</c:if>
        
        <c:if test="${mode eq 'editSpecialProduct' }">
        <html:hidden property="prodId"/>
			<html:button property="method" styleClass="button" onclick="javascript:fnCallUpdateProduct()" tabindex="12"> Update </html:button>
		</c:if>
          <input type="button" class="button" 
		  onclick="javascript:fnCallHome('<c:out value="${SpecialProductDetails.prodId}"/>');" tabindex="13" value="Cancel"/>
		 </td>
      </tr>
    </table>
	</html:form>
	</td>
    </tr>
  <tr>
     <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
		<!-- end nav-footer -->
	</td>
  </tr>
</table>

<!--<ul>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>