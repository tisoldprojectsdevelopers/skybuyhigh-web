<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>



<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">

	 function triggerEvent() {
		if(event.keyCode==13) {
		  fnCallSearch();  
		}           
	 }

	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	function fnValidateName(jsName,jsLabelName) {
		var nameFormat = new RegExp("^[A-Za-z'][A-Za-z'., ]*$");
		if(jsName == "") {
			alert("Please enter "+jsLabelName);
			document.forms[0].airlineSearchBy.focus();
			return false;
		}else if(!nameFormat.test(jsName)) {
			alert("Enter valid "+jsLabelName);
			document.forms[0].airlineSearchBy.focus();
			return false;
		}
		return true;			
	}	
	function fnValidateId(jsId,jsLabelName) {
		if(jsId==""){
			alert("Please enter valid "+jsLabelName);
			document.forms[0].airlineSearchBy.focus();
			return false;
		}else if(isNaN(jsId)) {
			alert("Air Charter Id must be an numeric and should be valid");
			document.forms[0].airlineSearchBy.focus();
			return false;
		}		
		return true;		
	}
	
	/*** gtky search start  ****/
	function fnCallSearch() {
		var jsIsEligibleForSubmit=true;
		var jsSearchBy = trim(document.forms[0].airlineSearchBy.value);
		var jsSearchValue = trim(document.forms[0].airlineSearchValue.value);
		if(jsSearchBy=="airContactName"){			
			jsIsEligibleForSubmit = fnValidateName(jsSearchValue,"Air Charter Contact Name");
		}else if(jsSearchBy=="airName"){			
			jsIsEligibleForSubmit = fnValidateName(jsSearchValue,"Air Charter Name");
		}else if(jsSearchBy=="airId"){			
			jsIsEligibleForSubmit = fnValidateId(jsSearchValue,"Air Charter Id");
		}else if(jsSearchBy=="All"){
			document.forms[0].airlineSearchValue.value="";
		}
		if(jsIsEligibleForSubmit) {
			document.forms[0].action="searchAirline.do?method=searchAirline";
			document.forms[0].submit();
		}else {
			document.forms[0].airlineSearchValue.focus();
			return false;
		}		
	}
	function fnCallEdit(jsAirIdValue) {
		document.forms[0].action="editAirlineReg.do?method=editAirlineDetails&airId="+jsAirIdValue;
		document.forms[0].submit();
	}
	function fnCancel() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}

	
</script>

<html:form action="/admin/searchAirline.do?method=searchAirline" method="post"  onsubmit="return false;" >
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
     <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<!--Admin -->
		<logic:present name="adminLoginInfo" scope="session">	
		<c:if test="${adminLoginInfo.userType eq 'admin'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Air Charter</li>
		</ul>
		</c:if>
		</logic:present>
		<!--Airline-->
		<logic:present name="airlineLoginInfo" scope="session">	
		<c:if test="${airlineLoginInfo.userType eq 'airline'}">
		<ul>
		<li>You navigated from :</li>
		<li>Air Charter</li>
		<li>></li>
		<li>Edit/Search Air Charter</li>
		</ul>
		</c:if>
		</logic:present>	
	
			</div>			
		</div>
	</td>
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td><html:select property="airlineSearchBy" >					  						  					
        				<html:option value="All">All</html:option>	
						<html:option value="airContactName">Air Charter Contact Name</html:option>	 		  			
						<html:option value="airName">Air Charter Name</html:option>
						<html:option value="airId">Air Charter Id</html:option>						
	               		 </html:select>	</td>
        <td class="lable">Search Value:</td>
        <td> <html:text property="airlineSearchValue" styleClass="input" onkeydown="javascript:triggerEvent();"/></td>
        <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="airlineInfo" scope="request"> 
		 <logic:notEmpty name="airlineInfo">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Air Charter Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1"  bgcolor="#cccccc" class="broder_top0">
              <tr>
               
                <td height="32" align="center" nowrap="nowrap" class="table_header">Air Charter Code</td>
				<td class="table_header" nowrap="nowrap" width="150px">Air Charter Name</td>
                <td class="table_header" nowrap="nowrap" width="150px">Contact Name</td>
                <td class="table_header" nowrap="nowrap" width="200px">Address</td>
                <td class="table_header" nowrap="nowrap"  width="100px">City</td>
                <td class="table_header" nowrap="nowrap"  width="50px">State</td>
                <td class="table_header" nowrap="nowrap"  width="50px">Country</td>
                <td class="table_header" nowrap="nowrap" width="60px">Status</td>
				 <td class="table_header" nowrap="nowrap" width="110px" >Charge Type</td>
                <td class="table_header" nowrap="nowrap">Action</td>
              </tr>
              <logic:iterate id="AirlineInfo" name="airlineInfo">
              <tr class="tdbg">
			  	  <td align="left"><bean:write name="AirlineInfo" property="airId"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airName"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airContactName"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airAddr1"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airCity"/></td>
                 <td align="left" nowrap="nowrap" width="80px;">
				 	<logic:iterate id="states" name="StateList" scope="application">
					<c:if
						test="${states.stateCode eq AirlineInfo.airState}">
						<bean:write name="states" property="stateName" />
					</c:if>
			    </logic:iterate>	
				 
				</td>
                <td align="center"><bean:write name="AirlineInfo" property="airCountry"/></td>
                <td align="center" nowrap="nowrap" >
				<c:if test="${AirlineInfo.airStatus eq 'A'}">Active</c:if>
				<c:if test="${AirlineInfo.airStatus eq 'I'}">Inactive</c:if>
				</td>
				<td align="center" nowrap="nowrap">
				<c:if test="${AirlineInfo.chargeType eq 'FA'}">Automated</c:if>
				<c:if test="${AirlineInfo.chargeType eq 'SA'}">Semi Automated</c:if>
				</td>
                <td align="center" class="action" nowrap="nowrap">
                
                <a href="viewAirline.do?method=viewAirlineDetails&airId=<bean:write name='AirlineInfo' property='airId'/>" title="View My Info">View</a>&nbsp;&nbsp;
                <a href="javascript: void fnCallEdit('<bean:write name='AirlineInfo' property='airId'/>')" title="Edit My Info">Edit</a>
                
                </td>
              </tr>
             </logic:iterate>
            </table></td>
      </tr>
      
    </table>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
	</td>
    </tr>
	<logic:present name="airlineInfo" scope="request"> 
		 <logic:notEmpty name="airlineInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnCancel();" styleClass="button">Cancel</html:button></td>
    </tr>
	 </logic:notEmpty>
	</logic:present>
  
  <tr>
   <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
    </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>

</div>
</td></tr>
</html:form>

