<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	function disablePaste(e) {
		if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	    {
	       window.clipboardData.clearData();
	    }
	    return true; 
	}

	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 function textLimit(field, countfield,maxlen,dispName) {		
			if (field.value.length > maxlen + 1){
			  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
			  countfield.value = 0;	
			 } 
			if (field.value.length > maxlen){
			   field.value = field.value.substring(0, maxlen);
			   countfield.value = 0;		
			}   
			else			
				countfield.value = maxlen - field.value.length;
	}
	
	function getFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
			return true;
		} else {
			alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function getZipFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".zip") || (fileType == ".ZIP")) {
			return true;
		} else {
			alert("We supports .ZIP formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function isEligibleForSubmit() {
	
		var isInstallationPath = document.forms[0].installationPath.value
		var reasonForUpload = document.forms[0].reasonForUpload.value
		var uploadType = document.forms[0].uploadType.value
		var isEligible = true;
		
		if(uploadType == '') {
			alert("Please Select an option.");
			document.forms[0].uploadType.focus();	
			return false;
		}
		if(isInstallationPath =='') {
			alert("Installation package path is required");
			document.forms[0].installationPath.focus();	
			return false;
		}
		if(isInstallationPath != '' && !getZipFile(isInstallationPath , "Catalogue Path ")) {
			return false;
		}
		if(reasonForUpload == '') {
			alert('Reason for upload is required');
			return false;
		}
		return isEligible;
	}
	function fnCallSkyBuyCatalogueUpload() {
		if(isEligibleForSubmit()) {
			document.forms[0].action="viewUploadInstallationPackage.do?method=uploadInstallationPackage";
			document.forms[0].submit();
		}
	}
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
	function fnChangeInput() {
		var uploadType = document.forms[0].uploadType.value
		if(uploadType == 'Admin') {
			document.getElementById("AdminVersion").style.display="inline";
			document.getElementById("CatalogueVersion").style.display="none";
		}else if(uploadType == 'Catalogue') {
			document.getElementById("AdminVersion").style.display="none";
			document.getElementById("CatalogueVersion").style.display="inline";
		}else if(uploadType == 'All') {
			document.getElementById("AdminVersion").style.display="inline";
			document.getElementById("CatalogueVersion").style.display="inline";
			
		}
	
	}
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
		   <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Catalogue</li>
					<li>></li>
					<li>upload SkyBuy Catalogue</li>
				</ul>
			</div>			
		</div>

	</td>
		   
		   
		    </tr>
			<tr>
				<td colspan="3" class="td">
					&nbsp;
				</td>
			</tr>
				<tr>
					<td colspan="3" class="td">
						<html:form action="admin/initUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500">
									<tr>
					    				<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
						  			</tr>
						  		</table>
					  		</logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2> 
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="4"
											bgcolor="#FFFFFF" class="broder_top0">
											<tr class="tdbg">
												<td>	
													<table>
														<tr>
															<td align="left" valign="top" nowrap="nowrap" class="lable">
																<span class="boldtext">Need to upload package for  </span>
															</td>
															<td valign="top" class="lable">
																:
															</td>
															<td colspan="4" align="left">
																<html:select property="uploadType" styleClass="textarea2" onchange="fnChangeInput();">
																	<html:option value="All">All</html:option>					  						  		
																	<html:option value="Admin">Admin</html:option>		
																	<html:option value="Catalogue">Catalogue</html:option>							
											               		</html:select>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr class="tdbg">
												<td>
													<table>
														<tr class="tdbg">
															<td align="left" valign="top" nowrap="nowrap" class="lable">
																<span class="boldtext">SkyBuy<sup>High</sup> installation package  </span>
															</td>
															<td valign="top" class="lable">
																:
															</td>
															<td colspan="4" align="left">
																<html:file property="installationPath" accept="application/zip" styleClass="browse" tabindex="4"/><BR/>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr class="tdbg">
												<td>
													<div id="AdminVersion" style="display:inline;">
														<table>
															<tr>
																<td align="left" valign="top" nowrap="nowrap" class="lable">
																	Admin package version
																</td>
																<td valign="top" class="lable">
																	:
																</td>
																<td colspan="4" align="left">
																	<html:text property="installationVersion" maxlength="10" />
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
											<tr class="tdbg">
												<td>
													<div id="CatalogueVersion" style="display:inline;">
														<table>
															<tr class="tdbg">
																<td align="left" valign="top" nowrap="nowrap" class="lable">
																	Catalogue package version
																</td>
																<td valign="top" class="lable">
																	:
																</td>
																<td colspan="4" align="left">
																	<html:text property="installationVersion" maxlength="10" />
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
											<tr class="tdbg">
												<td>
													<table>
														<tr class="tdbg">
															<td align="left" valign="top" nowrap="nowrap" class="lable">
																<span class="boldtext">Reason for upload  </span>
															</td>
															<td valign="top" class="lable">
																:
															</td>
															<td colspan="4" align="left">
																<span class="helptext" style="vertical-align:top">Max 500 chars.</span><br />
																<textarea name="reasonForUpload" class="textarea" cols="66" rows="6" tabindex="36" onKeyUp="textLimit(this,this.form.commentlen,500,'Comments');"></textarea><br/>
																<span class="normaltext">Remaining characters</span>
																 <input readonly type=text name=commentlen size=3 maxlength=3 value="500" class="wordcount"/> 
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="50" colspan="3" align="center">
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallSkyBuyCatalogueUpload()" tabindex="11"> Save </html:button>
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallHome();" tabindex="12"> Cancel </html:button>
									</td>

								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
		<td colspan="3">
			<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>


	</td>
				</tr>
			</table>
		</div>
	</div>
</td></tr>



