<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	function disablePaste(e) {
		if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	    {
	       window.clipboardData.clearData();
	    }
	    return true; 
	}

	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 function textLimit(field, countfield,maxlen,dispName) {		
			if (field.value.length > maxlen + 1){
			  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
			  countfield.value = 0;	
			 } 
			if (field.value.length > maxlen){
			   field.value = field.value.substring(0, maxlen);
			   countfield.value = 0;		
			}   
			else			
				countfield.value = maxlen - field.value.length;
	}
	
	function parseCurrency(field)
	{
		var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
		var testDollar=(field.value).charAt(0);
		var testData=(field.value).substring(1,(field.value).length);
		var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
		if( testDollar!="$"){
			if(!currency.test(field.value) )
				 field.value = field.value.replace(onlyCurrency, "$1");
		}else{
		  if(!currency.test(testData) )
				field.value = field.value.replace(onlyCurrency, "$1");
	      }
	 }
		
	function trimPrice1(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		while(startChar=="0" || startChar==".") {		
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);			
		}
		if(retVal>=1)		 
			return true;
	    else
	      	return false;
	}
	
	function validatePrice(jsPrice) {
			var retVal=jsPrice;
			var startChar=retVal.substring(0,1);
			if(startChar=="$") { 
				retVal=retVal.substring(1,retVal.length);
			} 
			var startChar=retVal.substring(0,1);
			while(startChar=="0") { 
				retVal=retVal.substring(1,retVal.length);
				startChar=retVal.substring(0,1); 
			} 
			var startChar=retVal.substring(0,1);
			if(startChar==".") { 
				retVal="0"+retVal; 
			} 
			if(retVal>=0.01)
			  return false;
			    else
			    return true;       
	
	}
	
	function fnCallAddProduct(){	
		if(document.forms[0].prodTitle.value=="") {
			alert("Please enter Item Name");
			document.forms[0].prodTitle.focus();	
			return;
		}else if(document.forms[0].prodCode.value=="") {
			alert("Please enter Item Code");
			document.forms[0].prodCode.focus();	
			return;
		}
	}
	
	function getFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
			return true;
		} else {
			alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function getSwfFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".swf") || (fileType == ".SWF")) {
			return true;
		} else {
			alert("We supports .SWF formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function isValidFileName(jsUploadType, jsImagePath) {
	
		var lastDot = jsImagePath.lastIndexOf('.');
		var lastSlash = jsImagePath.lastIndexOf('\\');
		if(lastSlash > 0) {
			var fileName = jsImagePath.substring(lastSlash+1, lastDot);
		}else {
			var fileName = jsImagePath.substring(0, lastDot);
		}
		
		if(jsUploadType =='ECatalogue') {
			if(fileName != 'skybuyCatalogue') {
				alert('Please select the correct swf file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='WelcomePage') {
			if(fileName != 'welcomepage') {
				alert('Please select the correct swf file to upload with respective to the option selected');
				return false;
			}
		}
		return true;
	}
	function isEligibleForSubmit(jsUploadType) {
		var uploadType = jsUploadType
		var isCataloguePath = document.forms[0].cataloguePath.value
		var reasonForUpload = document.forms[0].reasonForUpload.value
		var isEligible = true;
		var uploadeTypeVar
		if(uploadType == 'ECatalogue') {
			uploadeTypeVar = 'E-Catalogue Path'
		}else {
			uploadeTypeVar = 'Welcome Path Path'
		}
		if(isCataloguePath =='') {
			alert(uploadeTypeVar+" is required");
			document.forms[0].cataloguePath.focus();	
			return false;
		}
		if(isCataloguePath != '' && !getSwfFile(isCataloguePath , uploadeTypeVar)) {
			return false;
		}
		if(isCataloguePath != '' && !isValidFileName(uploadType, isCataloguePath)) {
			return false;
		}
		if(reasonForUpload == '') {
			alert('Reason for upload is required');
			return false;
		}
		return isEligible;
	}
	
	function fnCallSkyBuyCatalogueUpdate(jsPackageId,jsUploadType) {
		if(isEligibleForSubmit(jsUploadType)) {
			document.forms[0].action="editSkybuyCatalogue.do?method=updateSkybuyCatalogue&PackageId="+jsPackageId;
			document.forms[0].submit();
		}
	}
	function fnCallCancel() {
		document.forms[0].action="initSearchSkybuyCatalogue.do?method=searchSkyBuyCatalogueDetails";
		document.forms[0].submit();
	}
	window.onload = function() {
		var uploadType = <c:out value="${uploadSkyBuyCatalogueForm.uploadType}"/> 
		if(uploadType == 'ECatalogue') {
			document.getElementById("Welcomepage").style.display="none";
			document.getElementById("eCatalogue").style.display="inline";
			document.getElementById("WelcomePageMsg").style.display="none";
			document.getElementById("ECatalogueMsg").style.display="inline";
		}else if(uploadType == 'WelcomePage') {
			document.getElementById("Welcomepage").style.display="inline";
			document.getElementById("eCatalogue").style.display="none";
			document.getElementById("WelcomePageMsg").style.display="inline";
			document.getElementById("ECatalogueMsg").style.display="none";
		}
	}
	
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
			 <td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Admin</li>
					<li>></li>
					<li>Search SkyBuy Catalogue</li>
				</ul>
			</div>			
		</div>
	</td>
		  
		  
		  
		    </tr>
			<tr>
				<td colspan="3" class="td">
					&nbsp;
				</td>
			</tr>
				<tr>
					<td colspan="3" class="td">
						<html:form action="admin/initUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500">
									<tr>
					    				<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
						  			</tr>
						  		</table>
					  		</logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2> 
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td align="left">
										<div class="lable">
											Need to upload SWF for * <span style="margin:0 0 0 28px;">:</span>
						               			<bean:write name="uploadSkyBuyCatalogueForm" property="uploadType"/>
										</div>
											<br/>
										<div class="lable"  style=" margin:10 0 10 0px;">
												<span id="eCatalogue" style="display:inline;">SkyBuy<sup>High</sup> e-catalogue *<span style="margin:0 0 0 30px;">:</span></span>
												<span id="Welcomepage" style="display:none;">Skybuy<sup>High</sup> welcome page *<span style="margin:0 0 0 16px;">:</span></span> 
											<html:file property="cataloguePath" accept="application/zip" styleClass="browse" tabindex="4"/><BR/>
											<div style="margin:0 0 0 180px;">
											<span id="ECatalogueMsg" style="display:inline;" class="normaltext">(File Name should be skybuyCatalogue.swf)</span>
											<span id="WelcomePageMsg" style="display:none;" class="normaltext">(File Name should be welcomepage.swf)</span>
											</div> 
										</div>
										<div class="lable" style="margin:10 0 10 0px;">
												<br/>Reason for upload *<span style="margin:0 0 0 60px;">:</span> <span class="helptext" style="vertical-align:top">Max 500 chars.</span><br />
										</div>
									
										<div style="margin:0 0 0 180px;">
											<textarea name="reasonForUpload" class="textarea" cols="66" rows="6" tabindex="36" onKeyUp="textLimit(this,this.form.commentlen,500,'Comments');"></textarea><br/>
											<span class="normaltext">Remaining characters </span>
											<input readonly type=text name=commentlen size=3 maxlength=3 value="500" class="wordcount"/>
										</div>
									</td>
								</tr>
								
								<tr>
									<td height="50" colspan="3" align="center">
										<input type="button" value="Save" class="button" onclick="javascript:fnCallSkyBuyCatalogueUpdate('<c:out value="${PackageId}" />','<bean:write name="uploadSkyBuyCatalogueForm" property="uploadType"/>')" tabindex="11" />
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallCancel();" tabindex="12"> Cancel </html:button>
									</td>
								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
				    <td colspan="3">
					<div class="nav-footer">
					<div class="nav-footer-right"></div>
					<div class="nav-footer-left"></div>
					<div class="nav-footer-content"></div>			
					</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</td></tr>



