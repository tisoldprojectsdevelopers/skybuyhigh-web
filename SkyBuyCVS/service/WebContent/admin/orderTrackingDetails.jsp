<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">
	
	window.onload=function() {
		tablecollapse();
	}
	function displayOrderTrackingDetails() {
		var trackValue = document.getElementById("TrackingDetails");
		if(trackValue.style.display == "none") {
			document.getElementById("TrackingDetails").style.display="inline";
		}
	}
	function fnCallSubmit(js_OrderItemId,IsUpdate,jsItemStatus,jsIsViewOrCharge) {
		var trackingDetails = document.forms[0].orderTrackingDetail.value;
		if(trackingDetails != "") {
			document.forms[0].action="orderTrackingDetails.do?method=updateOrderTrackingDetails&OrderItemId="+js_OrderItemId+"&IsUpdate="+IsUpdate+"&ItemStatus="+jsItemStatus+"&ViewOrder="+jsIsViewOrCharge;
			document.forms[0].submit();		
		}else {
			alert("Tracking Details is required");
		}
	}
	
	function fnCallEditOrderItemDetails(js_OrderItemId,IsUpdate,jsItemStatus,jsIsViewOrCharge,jsSourceOfEdit) {
		document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+js_OrderItemId+"&IsUpdate="+IsUpdate+"&ItemStatus="+jsItemStatus+"&ViewOrder="+jsIsViewOrCharge+"&PlaceOrder="+jsSourceOfEdit;
		document.forms[0].submit();
	}
	
	function fnCallChargeOrderItemDetails(js_OrderItemId) {
		document.forms[0].action="chargeOrder.do?method=chargeOrder&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	
	function fnCallViewOrderItemDetails(js_OrderItemId) {
		document.forms[0].action="editOrder.do?method=viewOrderDetails&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	
	function textLimit(field, countfield,maxlen,dispName) {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1) {
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		} 
		if (fieldvallength > maxlen) {
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
	}

</script>

<html:form action="/admin/orderTrackingDetails" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			
			   <td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit Order Details</li>
				</ul>
			</div>			
		</div>
	</td>
			
			
			
			</tr>
			<tr class="tdbg">
				<td class="td" colspan="3" align="center">
					<table border="0" cellpadding="0" cellspacing="0"
						style="margin: 10px" class="border">
						<tr>
							<td height="30" colspan="3" class="tablehead" align="center">
								<h2>
									Order Tracking Details
								</h2>
							</td>
						</tr>

						<tr>
							<td colspan="3" class="td">
								<table border="0" cellpadding="0" cellspacing="0"
									style="margin: 10px">
									<c:if test="${ItemStatus eq 'C'}">
										<tr>
											<td class="lable" align="left" nowrap="nowrap"
												valign="top">
												Shipment Status
											</td>
											<td class="lable" width="15%" valign="top">
												<b>:</b>
											</td>
											<td align="left">
												<html:radio property="shipmentStatus" value="Y"
													styleClass="checkboxHTML">Yes</html:radio>
												<html:radio property="shipmentStatus" value="N"
													styleClass="checkboxHTML">No</html:radio>
											</td>
										</tr>
									</c:if>
									<c:if test="${ItemStatus ne 'C'}">
										<html:hidden property="shipmentStatus" value="N" />
									</c:if>
									<tr>
										<td class="lable" align="left" nowrap="nowrap" valign="top">
											Enter Tracking Details of this Order
										</td>
										<td class="lable" width="15%" valign="top">
											<b>:</b>
										</td>
										<td>
											<html:textarea rows="6" cols="60"
												property="orderTrackingDetail" styleClass="textarea_otd"
												tabindex="1"
												onkeyup="textLimit(this,this.form.policylen,1000,'Reason for return');" />
											<br />

											<div align="left">
												<span class="normaltext">Remaining characters</span>
												<input readonly="readonly" type="text" name="policylen"
													size="3" maxlength="3" value="1000" class="wordcount" />
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="td">
								<input type="button" value="Submit" style="margin: 0 0 10px 15px;" onclick="javascript:fnCallSubmit('<c:out value="${OrderItemId}"/>','add','<c:out value="${ItemStatus}"/>','<c:out value="${IsViewOrCharge}" />');" class="button" tabindex="3" />
								<c:if test="${ItemStatus ne 'Q' and ItemStatus ne 'C' and ItemStatus ne 'O'}">
									<input type="button" value="Cancel" style="margin: 0 0 10px 15px;" onclick="javascript:fnCallEditOrderItemDetails('<c:out value="${OrderItemId}"/>','Cancel','<c:out value="${ItemStatus}"/>','<c:out value="${IsViewOrCharge}" />','<c:out value="${SourceOfEdit}" />');" class="button" tabindex="4" />
								</c:if>
								<c:if test="${ItemStatus eq 'Q'}">
									<input type="button" value="Cancel" style="margin: 0 0 10px 15px;" onclick="javascript:fnCallChargeOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" tabindex="4" />
								</c:if>
								<c:if test="${ItemStatus eq 'C' or ItemStatus eq 'O'}">
									<input type="button" value="Cancel" style="margin: 0 0 10px 15px;" onclick="javascript:fnCallViewOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" tabindex="4" />
								</c:if>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td colspan="3" class="lable" style="margin: 15px">
					<logic:present name="TrackingDetails" scope="request">
						<logic:notEmpty name="TrackingDetails" scope="request">
							<div id="TrackingDetails" style="margin: 20px;">
								<table width="54%" border="0" align="center" cellpadding="1"
									cellspacing="1" bgcolor="#CCCCCC"
									class="footcollapse border">
									<thead class="tablehead">
										<tr>
											<td width="70%" height="28">
												Tracking Details
											</td>
											<td>
												Updated Date
											</td>
										</tr>
									</thead>

									<tfoot>
										<tr>
											<td colspan=3 bgcolor="#E6EEFB" align="right" border="0px"
												class="normaltext"></td>
										</tr>
									</tfoot>

									<tbody>
										<logic:iterate id="trackingDetail" name="TrackingDetails">
											<tr>
												<td align="left" bgcolor="#FFFFFF">
													<bean:write name="trackingDetail"
														property="trackingDetails" />
												</td>
												<td align="left" nowrap="nowrap" bgcolor="#FFFFFF">
													<bean:write name="trackingDetail" property="create_dt" />
												</td>
											</tr>
										</logic:iterate>
									</tbody>
								</table>
							</div>
						</logic:notEmpty>
					</logic:present>
				</td>
			</tr>
			<tr>
			 <td colspan="3">
			<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
			</tr>
		</tbody>
	</table>
</div>
</div>
</td></tr>
</html:form>
</body>