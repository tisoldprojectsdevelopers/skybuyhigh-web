<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />

<script>

	function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	var datevar = dateObj;
	datevar = dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}
		}else {
			return datevar;
		}	
	}
	
	function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
	function checkdate(d,m,y)
	{
	//alert(m);
		var yl=1990; // least year to consider
		var ym=2200; // most year to consider
		if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
		if (m<1 || m>12) return(false);
		if (d<1 || d>31) return(false);
		if (y<yl || y>ym) return(false);
		if (m==4 || m==6 || m==9 || m==11)
		if (d==31) return(false);
		if (m==2)
		{
		var b=parseInt(y/4);
		if (isNaN(b)) return(false);
		if (d>29) return(false);
		if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
		}
		return(true);
	}
	function IsNumeric(sText)
	
	{
	   var ValidChars = "0123456789.";
	   var IsNumber=true;
	   var Char;
	
	 
	   for (i = 0; i < sText.length && IsNumber == true; i++) 
	      { 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1) 
	         {
	         IsNumber = false;
	         }
	      }
	   return IsNumber;
	}
	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function disablePaste(e)
		{
		  
		  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	      {
	       window.clipboardData.clearData();
			
	     }
	   
	     
	    return true; 
		}
	
	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 
	function textLimit(fieldLen,maxlen,dispName) {
		if (fieldLen > parseInt(maxlen) + 1){
			alert(dispName+" can have maximum of "+maxlen+" chars only."); 
			return false;
		}else
			return true;
	}
	function validateContactEmail(str) {
		isValid=true;
		var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		if (!regExp.test(str)) {
			isValid=false;
			alert("Invalid e-mail address");	
		}
		return isValid;
	}	
	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	
	function getValidZip(p_value){
		var phTemp="";
		if (p_value == null) {
			return false;
		}	
		if(isNaN(p_value)) {
			alert("Invalid format of Zip code ");
			return false;
		}
		if (p_value=="00000"){
			alert("Invalid Zip code");		
			return false;
		}
		if(p_value.length == 5 || p_value.length == 9) {
			return true;
		}else {
			if(p_value.length > 5 && p_value.length < 9) {
				alert("Zip code should not greater than 5 digits or less than 9 digits");
			}
			if(p_value.length > 9) {
				alert("Zip code should not greater than 9 digits");
			}
			if(p_value.length < 5) {
				alert("Zip code should not less than 5 digits");
			}
			return false;
		}
	}	
	function isEligibleForSubmit() {
			
		var today = new Date();
		var month=today.getMonth()+1;
		var date=today.getDate();
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		var quantity = trim(document.forms[0].qty.value);
		document.forms[0].qty.value = quantity;
		var regexQuantity = /^[0-9]*$/i;
		today=month+"/"+date+"/"+today.getFullYear();
		var travelDatePresent = document.forms[0].TravelDatePresent.value;
		
		var emailId = trim(document.getElementById("custShipEmail").value);
		var custShipZip = trim(document.getElementById("custShipZip").value);
		var firstName = trim(document.getElementById("custShipFirstName").value);
		var lastName = trim(document.getElementById("custShipLastName").value);
		var state = trim(document.getElementById("custShipState").value);
		var address = trim(document.getElementById("custShipAddress1").value);
		var city = trim(document.getElementById("custShipCity").value);
		
		document.getElementById("custShipEmail").value = emailId;
		document.getElementById("custShipZip").value = custShipZip;
		document.getElementById("custShipFirstName").value = firstName;
		document.getElementById("custShipLastName").value = lastName;
		document.getElementById("custShipState").value = state;
		document.getElementById("custShipAddress1").value = address;
		document.getElementById("custShipCity").value = city;
		
		if(firstName == "") {
			alert("Customer FirstName is required");
			return false;
		}
		if(lastName == "") {
			alert("Customer LastName is required");
			return false;
		}
		if(address == "") {
			alert("Address is required");
			return false;
		}
		if(city == "") {
			alert("City is required");
			return false;
		}
		if(state == "") {
			alert("Select a State");
			return false;
		}
		if(!getValidZip(custShipZip)) {
			return false;
		}
		if(!validateContactEmail(emailId)) {
			return false;
		}
		
		if(travelDatePresent == "YES") {
		var dateVar = trim(document.forms[0].travelDate.value);
		document.forms[0].travelDate.value = dateVar;
			if(dateVar =="") {
				alert("Travel Date is required");
				return false;
			}
			if(isDate(dateVar)) {
				if(dateFormats(dateVar) == "Past") { 
					alert("Entered Travel date is too Past. Please enter a future date");
					return false;
				}
				if(dateFormats(dateVar) == "Advance") {
					alert("Entered Travel date is too Advance. Please enter a earlier date");
					return false;
				}
				document.forms[0].travelDate.value = dateFormats(dateVar);
				if(!validateDate(today,dateVar)) {
					alert("Enter future date");
					return false;
				}
			}else {
				alert("Enter Travel date in correct format");
				return false;
			}
		}
		
		if(regexQuantity.test(quantity)) {
			if(quantity==0) {
				alert("Quantity should not be zero");
				document.forms[0].qty.focus();
				return false;
			}
			document.forms[0].qty.value=trimQuantity(quantity);
			
		}else {
			alert("Enter the valid quantity value");
			document.forms[0].qty.focus();
			return false;
		}
	
		return true;
	}

/****Offer Description B****/


function displayReturnPolicy(orderItemId,ownerType) {
	document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
	document.forms[0].submit();
}

function fnCallSaveOrder(){
	
	if(isEligibleForSubmit()) {
		document.forms[0].action="updateOrder.do?method=updateOrderDetails";
		document.forms[0].submit();
	}
}

function fnCallSearchOrder(){
	document.forms[0].action="searchOrder.do?method=searchOrder";
	document.forms[0].submit();
}

function fnCallPlaceOrder(){
	document.forms[0].action="processOrder.do?method=initPlaceOrder";
	document.forms[0].submit();
}

function fnEditCreditCard(orderItemId) {
	document.forms[0].action="editCreditCardDetails.do?method=editCreditCardDetails&OrderItemId="+orderItemId;
	document.forms[0].submit();
}

function fnGetCustTrackingDetails(orderItemId,jsItemStatus,jsIsViewOrChange) {
	document.forms[0].action="orderTrackingDetails.do?method=getOrderTrackingDetails&OrderItemId="+orderItemId+"&ItemStatus="+jsItemStatus+"&ViewOrder="+jsIsViewOrChange;
	document.forms[0].submit();
}	
function fnPaymentTxnRefDetails(orderItemId) {
	document.forms[0].action="displayReasonForFailure.do?method=getReasonForFailure&OrderItemId="+orderItemId;
	document.forms[0].submit();
}
function fnViewEmail(orderItemId,email) {
	document.forms[0].target = "_blank";
	document.forms[0].action="editOrder.do?method=viewEmailPdf&orderItemId="+orderItemId+"&email="+email;
	document.forms[0].submit();
	document.forms[0].target = "";
}

</script>

<html:form action="/admin/viewReturnOrderDetails" method="post">
<tr><td>
<div class="contentcontainer">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="2" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
           	<td align="right" colspan="4" class="lable">
           		<c:if test="${returnOrderItemDetails.orderItemStatus eq 'C'}">
		           		<a href="javascript:fnViewEmail('<bean:write  name="returnOrderItemDetails"  property="orderItemId" />','PO')">View PO</a> | 
		        </c:if>
		        <c:if test="${returnOrderItemDetails.orderItemStatus eq 'C'}">
		           		<a href="javascript:fnViewEmail('<bean:write  name="returnOrderItemDetails"  property="orderItemId" />','INVOICE')">View Invoice</a> | 
		        </c:if>
	           	<c:if test="${returnOrderItemDetails.orderItemStatus eq 'C'}">
		           		<a href="javascript:fnGetCustTrackingDetails('<bean:write  name="returnOrderItemDetails"  property="orderItemId" />','<bean:write  name="returnOrderItemDetails"  property="orderItemStatus" />','VIEW')">Order Tracking Details</a> | 
		        </c:if>
	          	<c:if test="${(returnOrderItemDetails.orderItemStatus ne 'P')}">
	            	 <a href="javascript:fnPaymentTxnRefDetails('<bean:write  name="returnOrderItemDetails"  property="orderItemId" />')">Payment Transaction Details</a>
	            </c:if>
           	</td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="returnOrderItemDetails" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable" >Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="returnOrderItemDetails" property="custFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" >Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              	<bean:write  name="returnOrderItemDetails" property="custLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"  valign="top"> Address Line 1 </td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" class="catagory" valign="top"><span class="catagory">
				<bean:write  name="returnOrderItemDetails" property="custAddress1" />		
       		</span></td>
       		<c:if test="${returnOrderItemDetails.custAddress2 ne null && returnOrderItemDetails.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable" > Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="returnOrderItemDetails" property="custAddress2" />		
	       		</span></td>
       		</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			   <bean:write  name="returnOrderItemDetails" property="custCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<logic:iterate id="states" name="StateList" scope="application">
					<c:if test="${states.stateCode eq returnOrderItemDetails.custState}">
						<bean:write name="states" property="stateName" />
					</c:if>
				</logic:iterate>
            </span></td>
          </tr>
          <tr class="tdbg">
             <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <bean:write  name="returnOrderItemDetails" property="custCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="returnOrderItemDetails" property="custZip" />
            </span></td>
          </tr>
		   <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             	<bean:write  name="returnOrderItemDetails" property="custPhone" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Email&nbsp;  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="returnOrderItemDetails" property="custEmail" />
            </span></td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="returnOrderItemDetails" property="custShipFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="returnOrderItemDetails" property="custShipLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" valign="top"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" nowrap="nowrap" class="catagory" valign="top"><span class="catagory">
            <bean:write  name="returnOrderItemDetails" property="custShipAddress1" />
            </span></td>
            <c:if test="${(returnOrderItemDetails.custShipAddress2 ne null && returnOrderItemDetails.custShipAddress2 ne '')}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	            <bean:write  name="returnOrderItemDetails" property="custShipAddress2" />
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="returnOrderItemDetails" property="custShipCity" />
            </span></td>
            
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq returnOrderItemDetails.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              <!--  <bean:write  name="returnOrderItemDetails" property="custShipState" />-->
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="returnOrderItemDetails" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <bean:write  name="returnOrderItemDetails" property="custShipZip" />
             </span></td>
          </tr>
          <tr class="tdbg">
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="returnOrderItemDetails" property="custShipPhone" />
            </span></td>
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="returnOrderItemDetails" property="custShipEmail" />
            </span></td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable" > <span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="returnOrderItemDetails"  property="airName"/>
            </span></td>
            <c:if test="${returnOrderItemDetails.flightNo ne null}">
            	<td align="left" nowrap="nowrap" class="lable" >Tail No </td>
            	<td align="left" class="lable">:</td>
            </c:if>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="returnOrderItemDetails"  property="flightNo" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" ><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="returnOrderItemDetails"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="returnOrderItemDetails"  property="orderItemId"/>','<bean:write  name="returnOrderItemDetails"  property="ownerType"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="9" align="left" class="lable"><h4>Credit Card Details :</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="returnOrderItemDetails"  property="custFirstName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Last Name </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="returnOrderItemDetails"  property="custLastName" />
            </span></td>
			
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Credit Card No</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="returnOrderItemDetails"  property="maskCCNo"/>
        </span></td>
		 <td align="left" class="lable"><span class="boldtext">Credit Card Type</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
            <c:if test="${returnOrderItemDetails.cardType eq 'VC'}">
            	Visa Card
            </c:if>
            <c:if test="${returnOrderItemDetails.cardType eq 'MC'}">
            	Master Card
            </c:if>
            <c:if test="${returnOrderItemDetails.cardType eq 'AC'}">
            	American Express
            </c:if>
        </span></td>
		  </tr>
		  <tr>
		   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Exp Date</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="returnOrderItemDetails"  property="expDt"/>
        </span></td>
        <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
		  </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="returnOrderItemDetails"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="11" align="left" class="lable"><div align="center"><img src="<c:out value='${returnOrderItemDetails.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			    <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Item Id</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="returnOrderItemDetails"  property="orderItemId" />
               </span></td>
			   </tr>
			   <c:if test="${returnOrderItemDetails.rmaGeneratedNo ne null and returnOrderItemDetails.rmaGeneratedNo ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">RMA Number</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                 <bean:write  name="returnOrderItemDetails"  property="rmaGeneratedNo" />
		               </span></td>
				   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="returnOrderItemDetails"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq returnOrderItemDetails.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${returnOrderItemDetails.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="returnOrderItemDetails"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="returnOrderItemDetails"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="returnOrderItemDetails"  property="itemName"/>
               </span></td>
			   </tr>
			   <c:if test="${returnOrderItemDetails.ownerType eq 'airline'}">
				  <tr class="tdbg">
				  <c:if test="${LoginType eq 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="YES"/>
				  </c:if>
				  <c:if test="${LoginType ne 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="NO"/>
				  </c:if>
				  <c:if test="${returnOrderItemDetails.travelDate ne null || (LoginType eq 'admin' && (returnOrderItemDetails.orderItemStatus eq 'P'))}">
					   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Travel Date </span></td>
					   <td align="left" class="lable">:</td>
				  </c:if>
				   <td align="left" class="catagory"><span class="catagory">
	                	<bean:write  name="returnOrderItemDetails" property="travelDate" />
                   </span></td>
				   </tr>
			   </c:if>

			  <c:if test="${returnOrderItemDetails.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="returnOrderItemDetails"  property="brandName" />
				   </span></td>
				 </tr>
			 </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Return Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${returnOrderItemDetails.rmaStatus eq 'RG'}"> RMA Generated </c:if>
                 <c:if test="${returnOrderItemDetails.rmaStatus eq 'WR'}"> Waiting for Admin to charge back</c:if>
				 <c:if test="${returnOrderItemDetails.rmaStatus eq 'WA'}"> Waiting for Admin to reject</c:if>
				 <c:if test="${returnOrderItemDetails.rmaStatus eq 'RR'}"> RMA Rejected </c:if>
				 <c:if test="${returnOrderItemDetails.rmaStatus eq 'RA'}"> RMA Approved </c:if>
                 <!--    <bean:write  name="returnOrderItemDetails"  property="orderItemStatus" />-->
               </span></td>
			   </tr>
			   <c:if test="${returnOrderItemDetails.orderItemStatus eq 'F' && returnOrderItemDetails.reasonForFailure ne null && returnOrderItemDetails.reasonForFailure ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Failure</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" valign="top"><span class="catagory">
					 <bean:write  name="returnOrderItemDetails"  property="reasonForFailure" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${returnOrderItemDetails.orderItemStatus eq 'R' && returnOrderItemDetails.reasonForReject ne null && returnOrderItemDetails.reasonForReject ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Rejected</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" valign="top"><span class="catagory">
					 <bean:write  name="returnOrderItemDetails"  property="reasonForReject" />
				   </span></td>
				 </tr>
			 </c:if>
			 	<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Quantity</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="returnOrderItemDetails" property="qty" />
	                </span></td>
			   </tr>		
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${returnOrderItemDetails.ownerType eq 'vendor'}">		   
			   Instore Price
			   </c:if>
			   <c:if test="${returnOrderItemDetails.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${returnOrderItemDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               
               </span></td>			  
			   </tr>
			   
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${returnOrderItemDetails.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
             
               </span></td>
			  	 
			   </tr>
			   <c:if test="${returnOrderItemDetails.returnReason ne null and returnOrderItemDetails.returnReason ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason for return </td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<bean:write name="returnOrderItemDetails" property="returnReason" />
		               </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${returnOrderItemDetails.vendorComments ne null and returnOrderItemDetails.vendorComments ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">Vendor comments </td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<bean:write name="returnOrderItemDetails" property="vendorComments" />
		               </span></td>
				   </tr>
			   </c:if>
			   
			   <c:if test="${returnOrderItemDetails.rmaAuthorizeSignatory ne null and returnOrderItemDetails.rmaAuthorizeSignatory ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">RMA Authorize Signatory </td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<bean:write name="returnOrderItemDetails" property="rmaAuthorizeSignatory" />
		               </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${returnOrderItemDetails.chargeBackPrice ne null and returnOrderItemDetails.chargeBackPrice ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">Charge Back Unit Price</td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<fmt:formatNumber value="${returnOrderItemDetails.chargeBackPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
		               </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${returnOrderItemDetails.chargeBackAmount ne null and returnOrderItemDetails.chargeBackAmount ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">Charge back amount</td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<fmt:formatNumber value="${returnOrderItemDetails.chargeBackAmount}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
		               </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${returnOrderItemDetails.adminComments ne null and returnOrderItemDetails.adminComments ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">Admin Comments</td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<bean:write name="returnOrderItemDetails" property="adminComments" />
		               </span></td>
				   </tr>
			   </c:if>
      		</table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 
				<html:button property="method" onclick="javascript:history.back()" styleClass="button">OK</html:button> 
		   </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
   <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>
</div>
</div>
</td></tr>
</html:form>
