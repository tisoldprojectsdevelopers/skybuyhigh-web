<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
	
	function trim(inputString) {
	 var retValue = inputString;
	 var ch = retValue.substring(0, 1);
	 while (ch == " ") {
			retValue = retValue.substring(1, retValue.length);
			ch = retValue.substring(0, 1);
	 }
	 ch = retValue.substring(retValue.length-1, retValue.length);
	 while (ch == " ") {
			retValue = retValue.substring(0, retValue.length-1);
			ch = retValue.substring(retValue.length-1, retValue.length);
	 }
	 return retValue;
	}
	function disablePaste(e) {
		if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	    {
	       window.clipboardData.clearData();
	    }
	    return true; 
	}

	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 function textLimit(field, countfield,maxlen,dispName) {		
			if (field.value.length > maxlen + 1){
			  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
			  countfield.value = 0;	
			 } 
			if (field.value.length > maxlen){
			   field.value = field.value.substring(0, maxlen);
			   countfield.value = 0;		
			}   
			else			
				countfield.value = maxlen - field.value.length;
	}
	
	function getFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
			return true;
		} else {
			alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function getZipFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".zip") || (fileType == ".ZIP")) {
			return true;
		} else {
			alert("We supports .ZIP formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	
	function isValidFileName(jsUploadType, jsImagePath, jsField) {
	
		var lastDot = jsImagePath.lastIndexOf(".");
		var lastSlash = jsImagePath.lastIndexOf("\\");
		if(lastSlash > 0) {
			var fileName = jsImagePath.substring(lastSlash+1, lastDot);
		}else {
			var fileName = jsImagePath.substring(0, lastDot);
		}
		
		if(jsUploadType =='ALL') {
			if(fileName != 'ALL') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='Admin') {
			if(fileName != 'Admin') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='Catalogue') {
			if(fileName != 'Catalogue') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}
		if((fileName == 'ALL') || (fileName == 'Admin') || fileName == 'Catalogue') {
			return true;
		} else {
			alert(jsField);
			return false;
		}
	}
	
	function isEligibleForSubmit() {
	
		var isInstallationPath = document.forms[0].installationPath.value
		var reasonForUpload = document.forms[0].reasonForUpload.value
		var isEligible = true;
		
		if(isInstallationPath =='') {
			alert("Installation package path is required");
			document.forms[0].installationPath.focus();	
			return false;
		}else if(isInstallationPath != '' ) {
			if(!getZipFile(isInstallationPath , "Catalogue Path ")) {
				return false;
			}
		}		
		if(reasonForUpload == '') {
			alert('Reason for upload is required');
			return false;
		}
		return isEligible;
	}
	function fnCallInstallationPackageUpdate(jsPackageId) {
		if(isEligibleForSubmit()) {
			document.forms[0].action="editInstallationPackage.do?method=updateInstallationPackage&PackageId="+jsPackageId;
			document.forms[0].submit();
		}
	}
	function fnCallSearchInstallationPackage() {
		document.forms[0].action="initSearchInstallationPackage.do?method=searchInstallationPackage";
		document.forms[0].submit();
	}
	
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
			<td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Catalogue</li>
					<li>></li>
					<li>upload SkyBuy Catalogue</li>
				</ul>
			</div>			
		</div>
	</td>
		  
		  
		  
		    </tr>
			<tr>
				<td colspan="3" class="td">&nbsp;
					
				</td>
			</tr>
				<tr>
					<td colspan="3" class="td">
						<html:form action="admin/initUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500" align="center">
									<tr>
					    				<td colspan="3" class="error" align="center"><c:out value="${errorMsg}" /></td>
						  			</tr>
						  		</table>
					  		</logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2>
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td align="left">
										<div class="lable">
											Need to upload package for * <span style="margin:0 0 0 33px;">:</span>
										
											<!--<html:select property="uploadType" styleClass="textarea2" onchange="fnChangeInput();">
												<html:option value="ALL">ALL</html:option>					  						  		
												<html:option value="Admin">Admin</html:option>		
												<html:option value="Catalogue">Catalogue</html:option>							
						               		</html:select>--> 
						               		<bean:write name="uploadSkyBuyCatalogueForm" property="uploadType"/>
										</div>
											<br/>
										<div class="lable"  style=" margin:10 0 10 0px;">
												<span id="AdminPackage" style="display:inline;">Admin installation package *<span style="margin:0 0 0 37px;">:</span></span>
												<span id="CataloguePackage" style="display:none;">Catalogue installation package *<span style="margin:0 0 0 16px;">:</span></span> 
											<html:file property="installationPath" accept="application/zip" styleClass="browse" tabindex="4"/><BR/>
										</div>
											<br/>
											<div class="lable" style="display:inline;">
												<span id="AdminVersion" style="display:inline;">Admin package version *<span style="margin:0 0 0 55px;">:</span></span>
												<span id="CatalogueVersion" style="display:none;">Catalogue package version *<span style="margin:0 0 0 34px;">:</span></span>
												<bean:write name="uploadSkyBuyCatalogueForm" property="version"/>
											</div>
										<div class="lable" style="margin:10 0 10 0px;">
												<br/>Reason for upload *<span style="margin:0 0 0 85px;">:</span> <span class="helptext" style="vertical-align:top">Max 500 chars.</span><br />
										</div>
									
										<div style="margin:0 0 0 200px;">
											<textarea name="reasonForUpload" class="textarea" cols="66" rows="6" tabindex="36" onKeyUp="textLimit(this,this.form.commentlen,500,'Comments');"></textarea><br/>
											<span class="normaltext">Remaining characters </span>
											<input readonly type=text name=commentlen size=3 maxlength=3 value="500" class="wordcount"/>
										</div>
									</td>
								</tr>
								<tr>
									<td height="50" colspan="3" align="center">
										<input type="button" class="button" onclick="javascript:fnCallInstallationPackageUpdate('<bean:write name="InstallationDetails" property="packageId" />');" tabindex="11" value="Save" />
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallSearchInstallationPackage();" tabindex="12"> Cancel </html:button>
									</td>
								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
				  <td colspan="3">
					<div class="nav-footer">
					<div class="nav-footer-right"></div>
					<div class="nav-footer-left"></div>
					<div class="nav-footer-content"></div>			
				</div>
				</td>
				</tr>
			</table>
		</div>
	</div>
</td></tr>



