<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>


<link type="text/css" href="../style/registration.css" rel="stylesheet">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

  <head>
    <title>Customer Transaction Details</title>
    <script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
    <script type="text/javascript">
    	function fnCallReply(jsCustId) {
			var path = "custFeedback.do?method=getCustFeedbackEmail&custId="+jsCustId;
			window.open(path,'SkyBuyHigh','width=650, height=420, location=no, menubar=no, status=no, toolbar=no, scrollbars=no, resizable=no, left=200,top=150')
		}
		function fnCallCustomerFeedback() {
			document.forms[0].action="custFeedback.do?method=searchCustFeedback";
			document.forms[0].submit();
		}
    </script>
  </head>
  
  <body>
    <html:form action="admin/custFeedback" method="post">
    <tr><td>
     		<div class="contentcontainer">
				    <table border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Admin</li>
					<li>></li>
					<li>Customer Feedback</li>
				</ul>
			</div>			
		</div>

	</td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  
			 	 <div class="ReplaceOrderDetailsEnd"></div>
			  <tr>
				  <td colspan="3" valign="top" align="center" class="td">
				 <logic:present name="custSuggDetails">
	     			<table width="700"  border="0" cellpadding="0" cellspacing="0"  align="center"  class="border">
					 <tr>
						<td height="30" colspan="3" class="tablehead" align="center"><h2>Customer Feedback</h2></td>
					  </tr> 
					  <tr>
			        <td valign="top">
					  <table border="0" width="100%" cellpadding="2" cellspacing="2" class="broder_top0">
	   				<tr> 
   						<td align="left" width="150" nowrap="nowrap">&nbsp;</td>
   						<td width="10">&nbsp;</td>
   						<td width="245" align="left">&nbsp;</td>
   						<td align="left" width="150">&nbsp;</td>
   						<td width="10">&nbsp;</td>
   						<td width="245" align="right"><span class="lable"><a href="javascript: void fnCallReply('<bean:write name='custSuggDetails' property='custId'/>')" class="editdelete" title="Reply">Reply</a></span></td>
   					</tr>
					<tr> 
   						<td align="left" width="150" nowrap="nowrap"><b>Order Confirmation No</b></td>
   						<td width="10"><b>:</b></td>
   						<td width="245" align="left"><bean:write name="custSuggDetails" property="orderConfirmationNo"/></td>
   						<td align="left" width="150"><b>Air Charter Name</b></td>
   						<td width="10"><b>:</b></td>
   						<td width="245" align="left"><bean:write name="custSuggDetails" property="airlineName"/></td>
   					</tr>	
   					<tr>
   						<td align="left"><b>Order Date</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="orderDate"/></td>
						<td align="left"><b>Tail No</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="flightNo"/></td>
   						
   					</tr>					
   					
					<tr>
						<td align="left" colspan="6"> </td>
					</tr>
					<tr>
   						<td align="left" valign="top"><b> Billing Address</b></td>
   						<td align="left" valign="top"><b>:</b></td>
   						<td align="left" valign="top">
						<bean:write name="custSuggDetails" property="billingFName" />
   							<bean:write name="custSuggDetails" property="billingLName" />,&nbsp;<br>
							<logic:present name="custSuggDetails" property="billingAddr1">
   								<c:if test="${custSuggDetails.billingAddr1 ne '' and !empty custSuggDetails.billingAddr1}">
   									<bean:write name="custSuggDetails" property="billingAddr1" />,&nbsp;<br>
   								</c:if> 
   							</logic:present>
   							<logic:present name="custSuggDetails" property="billingAddr2">
   								<c:if test="${custSuggDetails.billingAddr2 ne '' and !empty custSuggDetails.billingAddr2}">
   									<bean:write name="custSuggDetails" property="billingAddr2" />,&nbsp;<br>
   								</c:if> 
   							</logic:present>
   							<bean:write name="custSuggDetails" property="billingCity" />,&nbsp;
							<logic:iterate id="states" name="StateList" scope="application">
								<c:if test="${states.stateCode eq custSuggDetails.billingState}">
									<bean:write name="states" property="stateName" />
								</c:if>
							</logic:iterate>-<bean:write name="custSuggDetails" property="billingZip" /><br>
						</td>
   						<td align="left" valign="top" nowrap="nowrap"><b> Shipping Address</b></td>
   						<td align="left" valign="top"><b>:</b></td>
   						<td align="left" valign="top">
							<bean:write name="custSuggDetails" property="shippingFName" />
   							<bean:write name="custSuggDetails" property="shippingLName" />,&nbsp;<br>
							<logic:present name="custSuggDetails" property="shippingAddr1">
   								<logic:notEmpty name="custSuggDetails" property="shippingAddr1">
   									<bean:write name="custSuggDetails" property="shippingAddr1" />,&nbsp;<br>
   								</logic:notEmpty>
   							</logic:present>
   							<logic:present name="custSuggDetails" property="shippingAddr2">
   								<logic:notEmpty name="custSuggDetails" property="shippingAddr2">
   									<bean:write name="custSuggDetails" property="shippingAddr2" />,&nbsp;<br>
   								</logic:notEmpty>
   							</logic:present>
   							<bean:write name="custSuggDetails" property="shippingCity" />,&nbsp;
   							<logic:iterate id="states" name="StateList" scope="application">
								<c:if test="${states.stateCode eq custSuggDetails.shippingState}">
									<bean:write name="states" property="stateName" />
								</c:if>
							</logic:iterate>-<bean:write name="custSuggDetails" property="shippingZip" /><br>
						</td>
   					</tr>
   					<tr>	
   						<td align="left"><b>Phone</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="custPhone" /></td>
   						<td align="left"><b>Phone</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="shippingPhone" /></td>
   					</tr>	
					<tr>
   						<td align="left"><b>Email</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="billingEmail" /></td>
   						<td align="left"><b>Email</b></td>
   						<td align="left"><b>:</b></td>
   						<td align="left"><bean:write name="custSuggDetails" property="shippingEmail" /></td>
   					</tr>	
   					<tr>
   						 <td height="1" colspan="7" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
   					</tr>
   					<tr>
   						<td align="left" valign="top"><b>Suggestion</b></td>
   						<td align="left" valign="top"><b>:</b></td>
   						<td align="left" colspan="4" valign="top"><bean:write name="custSuggDetails" property="custSuggestions" /></td>
   						
   					</tr>
					<tr>
   						  <td height="1" colspan="7" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
   					</tr>
   					
   					<tr>
   						<td align="left" valign="top"><b>Offers</b></td>
   						<td align="left" valign="top"><b>:</b></td>
   						<td align="left" colspan="4" valign="top"><bean:write name="custSuggDetails" property="custFeedback" /></td>
   						
   					</tr>
					
					<tr>
   						 <td height="50" colspan="7" align="center"> <html:button property="method" onclick="javascript:fnCallCustomerFeedback();" styleClass="button">OK</html:button> </td>
   						
   					</tr></table></td></tr>
   				</table>
				<!-- end order-items -->
   				
     		</logic:present>
     		
				</td>
	   		 </tr></td>
	   		  <tr>
    	<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>


	</td>
     </tr>
			 </table>
			 </div>
     	
		<!-- end contentcontainer -->
		</td></tr>
    </html:form>
  </body>

