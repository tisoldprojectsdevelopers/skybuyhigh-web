<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />


<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}


/****Offer Description B****/


	function displayReturnPolicy(orderItemId,ownerType) {
		document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
		document.forms[0].submit();
	}
	
	function fnCallProcessPayment(js_OrderItemId){
		document.forms[0].action="processPayment.do?method=processPayment&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	


</script>




<html:form action="admin/editOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
   <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
				</ul>
			</div>			
		</div>
	</td>


  
  
  
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	
	<table width="850" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="9" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="chargeOrderItem" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
			<td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable">Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custFirstName" />
             </span></td>
            <td align="left" nowrap="nowrap" class="lable">Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custLastName" />
              </span></td>
			 <td align="left" nowrap="nowrap" class="lable"> Address </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				 <bean:write  name="chargeOrderItem" property="custAddress1" />		
				
        </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
				  <bean:write  name="chargeOrderItem" property="custCity" />
		    </span></td>
			
			<td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				 <logic:iterate id="states" name="StateList" scope="application">
						<c:if test="${states.stateCode eq chargeOrderItem.custState}">
							<bean:write name="states" property="stateName" />
						</c:if>
					</logic:iterate>
				
			
             <!-- <bean:write  name="chargeOrderItem" property="custState" />-->
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
				<bean:write  name="chargeOrderItem" property="custCountry" />
		    </span></td>
          </tr>
         
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="chargeOrderItem" property="custZip" />
			</span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
        		  <bean:write  name="chargeOrderItem" property="custPhone" />
		    </span></td>
			
			<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				 <bean:write  name="chargeOrderItem" property="custEmail" />
		    </span></td>
          </tr>
		   
          <tr class="tdbg">
            <td colspan="9" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem" property="custShipFirstName" />
             </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <bean:write  name="chargeOrderItem" property="custShipLastName" />
            </span></td>
			
			 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            	<bean:write  name="chargeOrderItem" property="custShipAddress1" />
            </span></td>
          </tr>
          <tr class="tdbg">
           
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custShipCity" />
            </span></td>
			
			<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
               <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq chargeOrderItem.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              
              <!--  <bean:write  name="chargeOrderItem" property="custShipState" />-->
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <bean:write  name="chargeOrderItem" property="custShipCountry" />
             
            </span></td>
          </tr>
         
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
               <bean:write  name="chargeOrderItem" property="custShipZip" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custShipEmail" />
             </span></td>
			<td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="9" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="airName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Tail No </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="flightNo" />
            </span></td>
			<td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="chargeOrderItem"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="chargeOrderItem"  property="orderItemId"/>','<bean:write  name="chargeOrderItem"  property="ownerType"/>')" title="Customer Service">Customer Service</a></div></td>
		   <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
		  
		  
		  <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="9" align="left" class="lable"><h4>Credit Card Details :</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="custFirstName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Last Name </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="custLastName" />
            </span></td>
			<td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Credit Card No</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="chargeOrderItem"  property="CCNo"/>
        </span></td>
		 <td align="left" class="lable"><span class="boldtext">Credit Card Type</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="chargeOrderItem"  property="cardType"/>
        </span></td>
		 <td align="left" class="lable"><span class="boldtext">Exp Date</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="chargeOrderItem"  property="expDt"/>
        </span></td>
           
          </tr>
          
          <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="9" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="orderId"/>
               </span></td>
			   <td colspan="6" rowspan="10" align="left" class="lable"><div align="center"><img src="<c:out value='${chargeOrderItem.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq chargeOrderItem.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${chargeOrderItem.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="chargeOrderItem"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="itemName"/>
               </span></td>
			   </tr>
			    <c:if test="${chargeOrderItem.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="chargeOrderItem"  property="brandName" />
				   </span></td>
				   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'P'}"> Pending </c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'C'}"> Completed </c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'F'}"> Failed </c:if>
                 <!--    <bean:write  name="chargeOrderItem"  property="orderItemStatus" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Quantity</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                   <bean:write  name="chargeOrderItem" property="qty" />
                </span></td>
			   </tr>		
			 
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${chargeOrderItem.ownerType eq 'vendor'}">		   
			   Retail Price
			   </c:if>
			   <c:if test="${chargeOrderItem.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="vendPrice"/>
               </span></td>			  
			   </tr>
			   
			    <tr class="tdbg">
			
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="sbhPrice" />
               </span></td>
			  
			   </tr>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 
			 <input type="button" class="bigbutton" value="Process Payment" onclick="javascript:fnCallProcessPayment('<bean:write name="chargeOrderItem" property="orderItemId"/>')" />  <html:button property="method" onclick="javascript:history.back();" styleClass="button">Cancel</html:button>
		  </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
     <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</div>
</td></tr>
</html:form>
