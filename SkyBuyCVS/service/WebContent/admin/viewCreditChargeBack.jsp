<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />

<script>

	
	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function disablePaste(e)
		{
		  
		  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	      {
	       window.clipboardData.clearData();
			
	     }
	   
	     
	    return true; 
		}
	
	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 
	function textLimit(field, countfield,maxlen,dispName) {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1) {
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		} 
		if (fieldvallength > maxlen) {
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
	}
	 
	
/****Offer Description B****/


	function displayReturnPolicy(orderItemId,ownerType) {
		document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
		document.forms[0].submit();
	}
	
	function fnGetCustTrackingDetails(orderItemId) {
		document.forms[0].action="orderTrackingDetails.do?method=getOrderTrackingDetails&OrderItemId="+orderItemId;
		document.forms[0].submit();
	}	
	
	function fnPaymentTxnRefDetails(orderItemId) {
		document.forms[0].action="displayReasonForFailure.do?method=getReasonForFailure&OrderItemId="+orderItemId;
		document.forms[0].submit();
	}
	
	function isEligibleToChargeBack() {
		var adminComments = document.forms[0].adminComments.value;
		
		if(adminComments == '') {
			alert("Admin Comments is required");
			return false;
		}
		return true;
	}
	
	function fnCallCreditChargeBack(jsReturnId) {
		if(isEligibleToChargeBack()) {		
			document.forms[0].action="creditChargeBack.do?method=creditChargeBack&ReturnId="+jsReturnId;
			document.forms[0].submit();
		}
	}
	function fnCallCreditReject(jsOrderItemId) {
		document.forms[0].action="creditRejected.do?method=creditRejected&ReturnId="+jsReturnId;
		document.forms[0].submit();
	}
	
	function fnCallAcceptRejectReturn() {
		document.forms[0].action="OrderReturn.do?method=getOrderToReturn";
		document.forms[0].submit();
	}
	
</script>

<html:form action="admin/editOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">			
		<ul>
		<li>You navigated from :</li>
		<li>Order</li>	
		<li>></li>
		<li>Accept/Reject Return</li>	
		</ul>		
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
    
    
    
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Accept/Reject Return<</li>
				</ul>
			</div>			
		</div>

	</td>
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="2" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
           	<td align="right" colspan="4" class="lable">
           		<c:if test="${(custOrderItemDetails.orderItemStatus ne 'P')}">
	            	 <a href="javascript:fnPaymentTxnRefDetails('<bean:write  name="custOrderItemDetails"  property="orderItemId" />')">Payment Transaction Details</a>
	            </c:if>
           	</td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="custOrderItemDetails" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable" >Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="custOrderItemDetails" property="custFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" >Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              	<bean:write  name="custOrderItemDetails" property="custLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"  valign="top"> Address Line 1 </td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" class="catagory" valign="top"><span class="catagory">
				<bean:write  name="custOrderItemDetails" property="custAddress1" />		
       		</span></td>
       		<c:if test="${custOrderItemDetails.custAddress2 ne null && custOrderItemDetails.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable" > Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="custOrderItemDetails" property="custAddress2" />		
	       		</span></td>
       		</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			   <bean:write  name="custOrderItemDetails" property="custCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<logic:iterate id="states" name="StateList" scope="application">
					<c:if test="${states.stateCode eq custOrderItemDetails.custState}">
						<bean:write name="states" property="stateName" />
					</c:if>
				</logic:iterate>
            </span></td>
          </tr>
          <tr class="tdbg">
             <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <bean:write  name="custOrderItemDetails" property="custCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="custOrderItemDetails" property="custZip" />
            </span></td>
          </tr>
		   <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             	<bean:write  name="custOrderItemDetails" property="custPhone" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Email&nbsp;  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="custOrderItemDetails" property="custEmail" />
            </span></td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="custOrderItemDetails" property="custShipFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custShipLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" valign="top"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" nowrap="nowrap" class="catagory" valign="top"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custShipAddress1" />
            </span></td>
            <c:if test="${(custOrderItemDetails.custShipAddress2 ne null && custOrderItemDetails.custShipAddress2 ne '')}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	            <bean:write  name="custOrderItemDetails" property="custShipAddress2" />
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="custOrderItemDetails" property="custShipCity" />
            </span></td>
            
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq custOrderItemDetails.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              <!--  <bean:write  name="custOrderItemDetails" property="custShipState" />-->
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="custOrderItemDetails" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <bean:write  name="custOrderItemDetails" property="custShipZip" />
             </span></td>
          </tr>
          <tr class="tdbg">
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="custOrderItemDetails" property="custShipPhone" />
            </span></td>
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="custOrderItemDetails" property="custShipEmail" />
            </span></td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable" > <span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="airName"/>
            </span></td>
            <c:if test="${custOrderItemDetails.flightNo ne null}">
            	<td align="left" nowrap="nowrap" class="lable" >Tail No </td>
            	<td align="left" class="lable">:</td>
            </c:if>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="flightNo" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" ><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="custOrderItemDetails"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="custOrderItemDetails"  property="orderItemId"/>','<bean:write  name="custOrderItemDetails"  property="ownerType"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="9" align="left" class="lable"><h4>Credit Card Details :</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="custFirstName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Last Name </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="custLastName" />
            </span></td>
			
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Credit Card No</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="custOrderItemDetails"  property="maskCCNo"/>
        </span></td>
		 <td align="left" class="lable"><span class="boldtext">Credit Card Type</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
            <c:if test="${custOrderItemDetails.cardType eq 'VC'}">
            	Visa Card
            </c:if>
            <c:if test="${custOrderItemDetails.cardType eq 'MC'}">
            	Master Card
            </c:if>
            <c:if test="${custOrderItemDetails.cardType eq 'AC'}">
            	American Express
            </c:if>
        </span></td>
		  </tr>
		  <tr>
		   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Expiry Date</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="custOrderItemDetails"  property="expDt"/>
        </span></td>
        <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
		  </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="11" align="left" valign="top" class="lable"><div align="center"><img src="<c:out value='${custOrderItemDetails.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			    <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Item Id</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderItemId" />
               </span></td>
			   </tr>
			   <c:if test="${custOrderItemDetails.rmaGeneratedNo ne null and custOrderItemDetails.rmaGeneratedNo ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">RMA Number</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                 <bean:write  name="custOrderItemDetails"  property="rmaGeneratedNo" />
		               </span></td>
				   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq custOrderItemDetails.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${custOrderItemDetails.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="custOrderItemDetails"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="itemName"/>
               </span></td>
			   </tr>
			   <c:if test="${custOrderItemDetails.ownerType eq 'airline'}">
				  <tr class="tdbg">
				  <c:if test="${LoginType eq 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="YES"/>
				  </c:if>
				  <c:if test="${LoginType ne 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="NO"/>
				  </c:if>
				  <c:if test="${custOrderItemDetails.travelDate ne null || (LoginType eq 'admin' && (custOrderItemDetails.orderItemStatus eq 'P'))}">
					   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Travel Date </span></td>
					   <td align="left" class="lable">:</td>
				  </c:if>
				   <td align="left" class="catagory"><span class="catagory">
	                	<bean:write  name="custOrderItemDetails" property="travelDate" />
                   </span></td>
				   </tr>
			   </c:if>

			  <c:if test="${custOrderItemDetails.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="brandName" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${custOrderItemDetails.size ne null and custOrderItemDetails.size ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Size </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="size" />
				   </span></td>
				 </tr>
			 </c:if>
			  <c:if test="${custOrderItemDetails.color ne null and custOrderItemDetails.color ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Color </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="color" />
				   </span></td>
				 </tr>
			 </c:if>
			 <!-- <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'P' && custOrderItemDetails.ownerType eq 'vendor'}"> Order to be placed with Vendor</c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'P' && custOrderItemDetails.ownerType eq 'airline'}"> Order to be placed with Airline</c:if>				                 
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'F'}"> Failed </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'R'}"> Rejected </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'Q' && custOrderItemDetails.ownerType eq 'vendor'}"> To be charged after Vendor approval</c:if>
				 <c:if test="${custOrderItemDetails.orderItemStatus eq 'Q' && custOrderItemDetails.ownerType eq 'airline'}"> To be charged after Airline approval</c:if>
				 <c:if test="${custOrderItemDetails.orderItemStatus eq 'C'}"> Completed</c:if>
				 <c:if test="${custOrderItemDetails.orderItemStatus eq 'A'}"> RMA Approved</c:if>
				 <c:if test="${custOrderItemDetails.orderItemStatus eq 'E'}"> RMA Rejected</c:if>

               </span></td>
			   </tr>-->
			   <c:if test="${custOrderItemDetails.orderItemStatus eq 'F' && custOrderItemDetails.reasonForFailure ne null && custOrderItemDetails.reasonForFailure ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Failure</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="reasonForFailure" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${custOrderItemDetails.orderItemStatus eq 'R' && custOrderItemDetails.reasonForReject ne null && custOrderItemDetails.reasonForReject ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Rejected</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="reasonForReject" />
				   </span></td>
				 </tr>
			 </c:if>
			 	<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Return Quantity</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="custOrderItemDetails" property="custReturnQuantity" />
	                </span></td>
			   </tr>
			   <c:if test="${custOrderItemDetails.returnQuantity ne null and custOrderItemDetails.returnQuantity ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">Approved Quantity</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                   <bean:write  name="custOrderItemDetails" property="returnQuantity" />
		                </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${custOrderItemDetails.rmaAuthorizeSignatory ne null and custOrderItemDetails.rmaAuthorizeSignatory ne ''}">
                  <tr class="tdbg">
                    <td align="left" nowrap="nowrap" class="lable"> RMA Authorize Signatory </td>
                    <td align="left" class="lable"> : </td>
                    <td align="left" class="catagory"><bean:write name="custOrderItemDetails" property="rmaAuthorizeSignatory" /></td>
                  </tr>
             </c:if>		
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${custOrderItemDetails.ownerType eq 'vendor'}">		   
			   In-Store Price
			   </c:if>
			   <c:if test="${custOrderItemDetails.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${custOrderItemDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               
               </span></td>			  
			   </tr>
			   
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${custOrderItemDetails.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00" />  
             
               </span></td>
			</tr>
			<c:if test="${custOrderItemDetails.rmaStatus ne 'RR' and custOrderItemDetails.rmaStatus ne 'RG'}">
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Charge Back Unit Price </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
				   		<fmt:formatNumber value="${custOrderItemDetails.chargeBackPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
	               </span></td>
				</tr>
			</c:if>
			<c:if test="${custOrderItemDetails.rmaStatus ne 'RR' and custOrderItemDetails.rmaStatus ne 'RG'}">
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Charge Back Total Amount </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
				   		<fmt:formatNumber value="${custOrderItemDetails.chargeBackAmount}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
	               </span></td>
				</tr>
			</c:if>
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Return Status</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   	  <c:if test="${custOrderItemDetails.rmaStatus eq 'WR'}"> Waiting for Admin to Reject </c:if>
                  <c:if test="${custOrderItemDetails.rmaStatus eq 'WA'}"> Waiting for Admin to Charge back </c:if>
                  <c:if test="${custOrderItemDetails.rmaStatus eq 'RG'}"> RMA Generated </c:if>
                  <c:if test="${custOrderItemDetails.rmaStatus eq 'RA'}"> RMA Approved </c:if>
                  <c:if test="${custOrderItemDetails.rmaStatus eq 'RR'}"> RMA Rejected </c:if>
               </span></td>
			</tr>
			 	  
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable"  valign="top">Reason for Return </td>
			   <td align="left" class="lable"  valign="top">:</td>
			   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
			   		<bean:write  name="custOrderItemDetails" property="returnReason" />  
               </span></td>
			</tr>
			<c:if test="${custOrderItemDetails.vendorComments ne null and custOrderItemDetails.vendorComments ne ''}">
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Vendor Comments </td>
				   <td align="left" class="lable"  valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
				   		<bean:write  name="custOrderItemDetails" property="vendorComments" />  
	               </span></td>
				</tr>
			</c:if>
			<c:if test="${custOrderItemDetails.rmaStatus eq 'WA' and Mode ne 'PREVIEW'}">
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Admin Comments </td>
				   <td align="left" class="lable"  valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
				   		
					   		<textarea name="adminComments" rows="5" cols="60" class="textarea1" onkeyup="textLimit(this,this.form.policylen,1000,'Reason for return');"></textarea>
					   		<br />
							<br />
				            <div align="left"><span class="normaltext">Remaining characters</span>
				            <input readonly="readonly" type="text" name="policylen" size="3" maxlength="3" value="1000" class="wordcount"/></div>
			         	
	               </span></td>
				</tr>
			</c:if> 
         	<c:if test="${custOrderItemDetails.rmaStatus ne 'WA' and custOrderItemDetails.adminComments ne null and custOrderItemDetails.adminComments ne ''}">
         		<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Admin Comments </td>
				   <td align="left" class="lable"  valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
						<bean:write name="custOrderItemDetails" property="adminComments"/>				         	
	               </span></td>
				</tr>
         	</c:if>
			<input type="hidden" name="Skybuyprice" value="${custOrderItemDetails.sbhPrice}"/>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 

			<c:if test="${custOrderItemDetails.rmaStatus eq 'WA' and Mode ne 'PREVIEW'}">
				<input type="button" onclick="javascript:fnCallCreditChargeBack('<bean:write name="custOrderItemDetails" property="returnId" />')" class="bigbutton" title="Credit Charge Back" value="Charge Back" />
				<html:button property="method" onclick="javascript:fnCallAcceptRejectReturn();" styleClass="button" title="Cancel">Cancel</html:button>
			</c:if>
			<c:if test="${custOrderItemDetails.rmaStatus eq 'WR'}">
       			<input type="button" onclick="javascript:fnCallCreditReject('<bean:write name="custOrderItemDetails" property="returnId" />')" class="button" title="Credit Reject" value="Reject" />
       			<html:button property="method" onclick="javascript:fnCallAcceptRejectReturn();" styleClass="button" title="Cancel">Cancel</html:button>
       		</c:if>
			<c:if test="${(custOrderItemDetails.rmaStatus ne 'WR' and custOrderItemDetails.rmaStatus ne 'WA') and Mode ne 'PREVIEW'}">
       			<html:button property="method" onclick="javascript:fnCallAcceptRejectReturn();" styleClass="button" title="OK">OK</html:button>
       		</c:if>
       		<c:if test="${(custOrderItemDetails.rmaStatus ne 'WR' or custOrderItemDetails.rmaStatus ne 'WA') and Mode eq 'PREVIEW'}">
       			<html:button property="method" onclick="javascript:fnCallAcceptRejectReturn();" styleClass="button" title="OK">OK</html:button>
       		</c:if>
		 </td>
       </tr>
    </table>
	
	</td>
    </tr>
  	<tr>
	  <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</td></tr>
</html:form>
