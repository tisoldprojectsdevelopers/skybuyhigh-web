<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
	
	
	
	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	function disablePaste(e) {
		if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	    {
	       window.clipboardData.clearData();
	    }
	    return true; 
	}

	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 function textLimit(field, countfield,maxlen,dispName) {		
			if (field.value.length > maxlen + 1){
			  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
			  countfield.value = 0;	
			 } 
			if (field.value.length > maxlen){
			   field.value = field.value.substring(0, maxlen);
			   countfield.value = 0;		
			}   
			else			
				countfield.value = maxlen - field.value.length;
	}
	
	function getFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
			return true;
		} else {
			alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	function getZipFile(imagePath,jsField,jsUploadType){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		fileType = fileType.toUpperCase();
		if(jsUploadType == 'Admin' || jsUploadType == 'ShoppingCart' || jsUploadType == 'AutoUpdate' || jsUploadType == 'VirtualDesktop'|| jsUploadType == 'Service') {
			if((fileType == ".zip") || (fileType == ".ZIP")) {
				return true;
			}else {
				alert("We supports .zip formats. "+jsField+" file-type is " + fileType );
				return false;
			}
		}else if(jsUploadType == 'Schema') {
			if(fileType == ".sql" || fileType == ".SQL" ) {
				return true;
			}else {
				alert("We supports .sql formats. "+jsField+" file-type is " + fileType );
				return false;
			}
		}else if(jsUploadType == 'ClientCert') {
			if(fileType == ".p12" || fileType == ".P12") {
				return true;
			}else {
				alert("We supports .p12 formats. "+jsField+" file-type is " + fileType );
				return false;
			}
		}else if(jsUploadType == 'iPhoneDesktop') {
			if(fileType == ".PNG") {
				return true;
			}else {
				alert("We supports .png formats. "+jsField+" file-type is " + fileType );
				return false;
			}
		}
		
		else if(jsUploadType == 'iPhonePersonalShopper') {
			if((fileType == ".PNG") ||(fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
				return true;
			}else {
				alert("We supports .png formats and .jpg formats. "+jsField+" file-type is " + fileType );
				return false;
			}
	
	}
	else if(jsUploadType =='iPhoneWelcomePage'){
			if((fileType == ".PNG") ||(fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
				return true;
			}else {
				alert("We supports .png formats and .jpg formats. "+jsField+" file-type is " + fileType );
				return false;
			}
		}
		
	
	
	}
	
	function isValidFileName(jsUploadType, jsImagePath, jsField) {
	
		var lastDot = jsImagePath.lastIndexOf(".");
		var lastSlash = jsImagePath.lastIndexOf("/");
		var fileName = jsImagePath.substring(lastSlash, lastDot);
		alert(fileName); 
		if(jsUploadType =='ALL') {
			if(fileName != 'ALL') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='Admin') {
			if(fileName != 'Admin') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='ShoppingCart') {
			if(fileName != 'Catalogue') {
				alert('Please select the correct zip file to upload with respective to the option selected');
				return false;
			}
		}
		if((fileName == 'ALL') || (fileName == 'Admin') || fileName == 'Catalogue') {
			return true;
		} else {
			alert(jsField);
			return false;
		}
	}
	
	function isEligibleForSubmit() {
		
		
		var uploadType = document.forms[0].uploadType.value
		
		
		var reasonForUpload = trim(document.forms[0].reasonForUpload.value)
		var isEligible = true;
		if(uploadType == '') {
			alert("Please Select an option.");
			document.forms[0].uploadType.focus();	
			return false;
		}else if(uploadType == 'Admin' || uploadType == 'ShoppingCart' || uploadType == 'AutoUpdate'
			|| uploadType == 'Schema' || uploadType == 'VirtualDesktop'|| uploadType == 'Service' ) {
			
			var isInstallationPath = document.forms[0].installationPath.value
			
			var version = document.forms[0].version.value
			var regExpForVersion = new RegExp("^\d*[0-9]+(|.\d*[0-9]+|)*$"); 
			
			if(isInstallationPath =='') {
				if(uploadType == 'Schema') {
					alert("Schema file path is required");
				}else {
					alert("Installation package path is required");
				}
				document.forms[0].installationPath.focus();	
				return false;
			}else if(isInstallationPath != '' ) {
				if(!getZipFile(isInstallationPath , uploadType+" Path ",uploadType)) {
					return false;
				}
			}
			if(!regExpForVersion.test(version)) {
				alert('Enter valid version');
				return false;
			}
		}else if(uploadType == 'Username') {
			var jsUserName = document.forms[0].userName.value
			var jsPassword = document.forms[0].password.value
			
			if(jsUserName == null || jsUserName == '') {
				alert('Please enter the user name');
				return false;
			}else if(jsUserName.length < 5) {
				alert('User name should not less than 5 characters');
				return false;		
			}
			if(jsPassword == null || jsPassword == '') {
				alert('Please enter the password');
				return false;
			}else if(jsPassword.length < 5) {
				alert('Password should not less than 5 characters');
				return false;		
			}
		}else if(uploadType == 'ClientCert') {
			var jsInstallationPath = document.forms[0].installationPath.value
			var jsPassword = document.forms[0].password.value
			if(jsInstallationPath =='') {
				alert("Client Cert path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}else if(jsInstallationPath != '' ) {
				if(!getZipFile(jsInstallationPath , uploadType+" Path ",uploadType)) {
					return false;
				}
			}
			if(jsPassword == null || jsPassword == '') {
				alert('Please enter the password');
				return false;
			}else if(jsPassword.length < 5) {
				alert('Password should not less than 5 characters');
				return false;		
			}
		}else if(uploadType == 'iPhoneDesktop') {
			var jsInstallationPath = document.forms[0].installationPath.value
			if(jsInstallationPath =='') {
				alert("IPhone Desktop Image path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}else if(jsInstallationPath != '' ) {
				if(!getZipFile(jsInstallationPath , uploadType+" Path ",uploadType)) {
					return false;
				}
			}
		}
		else if(uploadType == 'iPhonePersonalShopper') {
			var jsInstallationPath = document.forms[0].installationPath.value
			if(jsInstallationPath =='') {
				alert("IPhone Personal Shopper Image path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}else if(jsInstallationPath != '' ) {
				if(!getZipFile(jsInstallationPath , uploadType+" Path ",uploadType)) {
					return false;
				}
			}
		}else if(uploadType == 'iPhoneWelcomePage') {
				
			var jsInstallationPath = document.forms[0].installationPath.value
			if(jsInstallationPath =='') {
				alert("IPhoneWelcomePage  Image path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}else if(jsInstallationPath != '' ) {
				if(!getZipFile(jsInstallationPath , uploadType+" Path ",uploadType)) {
					return false;
				}
			}
		}
		else if(uploadType == 'ECatalogue') {
			var jsInstallationPath = document.forms[0].installationPath.value;
			uploadeTypeVar = 'E-Catalogue Path';
			
			if(jsInstallationPath =='') {
				alert("E-Catalogue Path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}
			
			if(jsInstallationPath != '' && !getSwfFile(jsInstallationPath , uploadeTypeVar)) {
			return false;
			}
			if(jsInstallationPath != '' && !isValidFileName(uploadType, jsInstallationPath)) {
			return false;
			}
			
		}else if(uploadType == 'WelcomePage'){
			var jsInstallationPath = document.forms[0].installationPath.value;
			uploadeTypeVar = 'Welcome Path';
			
			if(jsInstallationPath =='') {
				alert("WelcomePage  Image path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}
			if(jsInstallationPath != '' && !getSwfFile(jsInstallationPath , uploadeTypeVar)) {
			return false;
			}
			if(jsInstallationPath != '' && !isValidFileName(uploadType, jsInstallationPath)) {
			return false;
			}
		}
		else if(uploadType == 'Service'){
			var jsInstallationPath = document.forms[0].installationPath.value;
				if(jsInstallationPath =='') {
					alert("Service Package Path is required");
					return false;
				}
		
		}else if(uploadType == 'VirtualDesktop'){
		
			var jsInstallationPath = document.forms[0].installationPath.value;
				if(jsInstallationPath =='') {
					alert("VirtualDesktop Package Path is required");
					return false;
				}
		
		}else if(uploadType == 'PersonalShopper') {
			var jsInstallationPath = document.forms[0].installationPath.value;
			uploadeTypeVar = 'Personal Shopper Path';
			
			if(jsInstallationPath =='') {
				alert("Personal Shopper Path is required");
				document.forms[0].installationPath.focus();	
				return false;
			}
			
			if(jsInstallationPath != '' && !getSwfFile(jsInstallationPath , uploadeTypeVar)) {
			return false;
			}
			if(jsInstallationPath != '' && !isValidFileName(uploadType, jsInstallationPath)) {
			return false;
			}
			
		}
		
		if(reasonForUpload == '') {
			alert('Reason for upload is required');
			return false;
		}
		return isEligible;
	}
	function fnCallSkyBuyCatalogueUpload() {
		if(isEligibleForSubmit()) {
		 
		var uploadType = document.forms[0].uploadType.value;
		/* if(uploadType == 'ECatalogue'){
		 	alert("in ecatalogue");
		 	document.forms[0].action="viewUploadSkybuyCatalogue.do?method=uploadSkybuyCatalogue";
			document.forms[0].submit();
		 }else{
		 	alert("in elsepart");*/
			document.forms[0].action="viewUploadInstallationPackage.do?method=uploadInstallationPackage";
			document.forms[0].submit();
		//}
		}
		
	}
	
	
	/*function fnCallSkyBuyCatalogueUpload() {
		if(isEligibleForSubmit()) {
			document.forms[0].action="viewUploadSkybuyCatalogue.do?method=uploadSkybuyCatalogue";
			document.forms[0].submit();
		}*/
	
	
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
	function fnChangeInput() {
		var uploadType = document.forms[0].uploadType.value;
		document.forms[0].version.value='';
		document.forms[0].userName.value='';
		document.forms[0].password.value='';
		document.forms[0].installationPath.value='';
		
		if(uploadType == 'VirtualDesktop') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";		
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="inline-block";
			document.getElementById("VirtualDesktopVersion").style.display="inline-block";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.forms[0].version.value = '';
		}else if(uploadType == 'Schema') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("SchemaVersion").style.display="inline-block";
			document.getElementById("SchemaPackage").style.display="inline-block";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.forms[0].version.value = '';
		}else if(uploadType == 'ClientCert') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ClientCertPackage").style.display="inline-block";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.forms[0].version.value = '';
		}else if(uploadType == 'Username') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("UserName").style.display="inline";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="none";
			document.getElementById("NonUserNameVersion").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}else if(uploadType == 'Service') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ServicePackage").style.display="inline";
			document.getElementById("ServiceVersion").style.display="inline";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}
		else if(uploadType == 'iPhoneDesktop') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="inline";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType =='iPhonePersonalShopper') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="inline";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType == 'ECatalogue') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ECatalogue").style.display="inline";	
			document.getElementById("ECatalogueMsg").style.display="inline";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
	
	
		else if(uploadType == 'WelcomePage') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="inline";	
			document.getElementById("WelcomePageMsg").style.display="inline";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
			else if(uploadType == 'iPhoneWelcomePage') {
			document.getElementById("PersonalShopperMsg").style.display="none";	
			document.getElementById("PersonalShopperImage").style.display="none";	
			document.getElementById("iPhoneWelcomePage").style.display="inline";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType == 'PersonalShopper') {
			document.getElementById("PersonalShopperMsg").style.display="inline";
			document.getElementById("PersonalShopperImage").style.display="inline";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
	
	
	
	}
	
	window.onload = function() {
		var uploadType = document.forms[0].uploadType.value
		
		if(uploadType == 'VirtualDesktop') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="inline-block";
			document.getElementById("VirtualDesktopVersion").style.display="inline-block";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}else if(uploadType == 'Schema') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("SchemaVersion").style.display="inline-block";
			document.getElementById("SchemaPackage").style.display="inline-block";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}else if(uploadType == 'ClientCert') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ClientCertPackage").style.display="inline-block";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}else if(uploadType == 'Username') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("UserName").style.display="inline";
			document.getElementById("ClientPassword").style.display="inline";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="none";
			document.getElementById("NonUserNameVersion").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}else if(uploadType == 'Service') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("ServicePackage").style.display="inline";
			document.getElementById("ServiceVersion").style.display="inline";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="inline";
			document.getElementById("iPhoneDesktopImage").style.display="none";
		}
		else if(uploadType == 'iPhoneDesktop') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="inline";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
		}
		else if(uploadType == 'iPhonePersonalShopper') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="inline";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType == 'ECatalogue') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="inline";	
			document.getElementById("ECatalogueMsg").style.display="inline";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType == 'WelcomePage') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="inline";	
			document.getElementById("WelcomePageMsg").style.display="inline";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		else if(uploadType == 'iPhoneWelcomePage') {
			document.getElementById("PersonalShopperMsg").style.display="none";
			document.getElementById("PersonalShopperImage").style.display="none";
			document.getElementById("iPhoneWelcomePage").style.display="inline";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}else if(uploadType == 'PersonalShopper') {
		
			document.getElementById("PersonalShopperMsg").style.display="inline";
			document.getElementById("PersonalShopperImage").style.display="inline";
			document.getElementById("iPhoneWelcomePage").style.display="none";
			document.getElementById("WelcomePage").style.display="none";	
			document.getElementById("WelcomePageMsg").style.display="none";	
			document.getElementById("ECatalogue").style.display="none";	
			document.getElementById("ECatalogueMsg").style.display="none";	
			document.getElementById("iPhoneDesktopImage").style.display="none";
			document.getElementById("iPhonePersonalShopperImage").style.display="none";
			document.getElementById("ServicePackage").style.display="none";
			document.getElementById("ServiceVersion").style.display="none";
			document.getElementById("UserName").style.display="none";
			document.getElementById("ClientPassword").style.display="none";
			document.getElementById("ClientCertPackage").style.display="none";
			document.getElementById("VirtualDesktopPackage").style.display="none";
			document.getElementById("VirtualDesktopVersion").style.display="none";
			document.getElementById("SchemaVersion").style.display="none";
			document.getElementById("SchemaPackage").style.display="none";
			document.getElementById("NonUserNameLabel").style.display="inline";
			document.getElementById("NonUserNameVersion").style.display="none";
		}
		
		
		
	}
	
	
	function getSwfFile(imagePath,jsField){
		if(imagePath==''){
			return false;
		}
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".swf") || (fileType == ".SWF")) {
			return true;
		} else {
			alert("We supports .SWF formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}
	
	
	function isValidFileName(jsUploadType, jsImagePath) {
	
		var lastDot = jsImagePath.lastIndexOf(".");
		var lastSlash = jsImagePath.lastIndexOf("\\");
		var fileName = jsImagePath.substring(lastSlash+1, lastDot);
		if(jsUploadType =='ECatalogue') {
			if(fileName != 'skybuyCatalogue') {
				alert('Please select the correct swf file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='WelcomePage') {
			if(fileName != 'welcomepage') {
				alert('Please select the correct swf file to upload with respective to the option selected');
				return false;
			}
		}else if(jsUploadType =='CheckList') {
			if(fileName != 'checklist') {
				alert('Please select the correct swf file to upload with respective to the option selected');
				return false;
			}
		}
		return true;
	}
	
	
	
</script>
<tr>
	<td>
		<div class="contentcontainer">
			<table width="100%" border="0" cellpadding="0" cellspacing="0"
				class="table">
				<tr>
					<td colspan="3">
						<div class="nav-header">
							<div class="nav-header-right"></div>
							<div class="nav-header-left"></div>
							<div class="nav-header-content">
								<ul>
									<li>
										You navigated from :
									</li>
									<li>
										Admin
									</li>
									<li>
										>
									</li>
									<li>
										Upload Installation Package
									</li>
								</ul>
							</div>
						</div>

					</td>




				</tr>
				<tr>
					<td colspan="3" class="td">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="3" class="td" align="center">
						<html:form action="admin/initUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500" align="center">
									<tr>
										<td colspan="3" class="error" align="center">
											<c:out value="${errorMsg}" />
										</td>
									</tr>
								</table>
							</logic:present>
							<table border="0" align="center" cellpadding="0" cellspacing="0"
								class="border" width="65%">
								<tr>
									<td align="center" class="tablehead">
										<h2>
											Package Information
										</h2>
									</td>
								</tr>
								<tr>
									<td align="left">
										<div class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: block;">
											Need to upload package for *
											<span style="margin: 0 0 0 33px;">:</span>

											<html:select property="uploadType" styleClass="textarea2"
												onchange="fnChangeInput();">
												<html:options collection="MasterInstallPkgList" property="key" labelProperty="value"/>
											</html:select>
										</div>

										<div id="NonUserNameLabel" class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: block;">
											<!-- <span id="AdminPackage" style="display:inline-block; width:205px; float:left;">Admin installation package *<span style="margin:0 0 0 37px;">:</span></span>
												<span id="AutoUpdatePackage" style="display:none; width:205px; float:left;">Auto Update package *<span style="margin:0 0 0 69px;">:</span></span>
												<span id="CataloguePackage" style="display:none; width:205px; float:left;">Catalogue installation package *<span style="margin:0 0 0 16px;">:</span></span> -->
											<span id="VirtualDesktopPackage"
												style="display: none; width: 205px; float: left;"><span style="margin: 0 2px 0 0; float:right;">:</span>Virtual
												Desktop Package *
											</span>
											<span id="ClientCertPackage"
												style="display: none; width: 205px; float: left;"><span style="margin: 0 2px 0 0; float:right;">:</span>Certificate
												*
											</span>
											<span id="SchemaPackage"
												style="display: none; width: 205px; float: left;"><span style="margin: 0 2px 0 0; float:right;">:</span>Schema
												package *
											</span>
											<span id="ServicePackage"
												style="display: none; width: 205px; float: left;"><span style="margin: 0 2px 0 0; float:right;">:</span>Service
												package *
											</span>
											<span id="iPhoneDesktopImage"
												style="display: none; width: 205px; float: left;"><span style="margin: 0 2px 0 0; float:right;">:</span>iPhone
												Desktop Image*
											</span>
												<span id="iPhonePersonalShopperImage" 
												style="display: none;width:205px;float:left;"><span style="margin: 0 2px 0 0; float:right;">:</span>iPhone
												Personal Shopper*
											</span>
											
											
											</span>
												<span id="PersonalShopperImage" 
												style="display: none;width:205px;float:left;"><span style="margin: 0 2px 0 0; float:right;">:</span>Personal Shopper*
											</span>
											
												<span id="iPhoneWelcomePage" 
												style="display: none;width:205px;float:left;"><span style="margin: 0 2px 0 0; float:right;">:</span>iPhone
												Welcome Page*
											</span>
											
										
											<span id="ECatalogue" style="display:none; width:205px; float:left;"><span style="margin:0 3px 0 0; float:right;">:</span>SkyBuy<sup>High</sup> e-catalogue *</span>
											<span id="WelcomePage" style="display:none; width:205px; float:left;"><span style="margin:0 3px 0 0; float:right;">:</span>Skybuy<sup>High</sup> welcome page *</span> 
											
												
											<html:file property="installationPath"
												accept="application/zip" styleClass="browse" tabindex="4"
												style="margin:0; float:left;" />
											
											<span id="ECatalogueMsg" style="display:none; clear:left; float:left; margin-left:205px; white-space:nowrap;" class="normaltext">(File Name should be skybuyCatalogue.swf )</span>
											<span id="WelcomePageMsg" style="display:none; clear:left; float:left; margin-left:205px; white-space:nowrap;" class="normaltext">(File Name should be welcomepage.swf )</span>
											<span id="PersonalShopperMsg" style="display:none; clear:left; float:left; margin-left:205px; white-space:nowrap;" class="normaltext">(File Name should be personal_shopper.swf )</span>
											
												
										
										</div>
										<div id="UserName" class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: none;">
											<span id="UsernamePackage">User name *<span
												style="margin: 0 0 0 128px;">:</span>
											</span>
											<html:text property="userName"
												styleClass="input-version-password"></html:text>
										</div>
										<div id="ClientPassword" class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: none;">
											<span id="UsernamePackage">Password *<span
												style="margin: 0 0 0 131px;">:</span>
											</span>
											<html:password property="password"
												styleClass="input-version-password"></html:password>
										</div>

										<div id="NonUserNameVersion" class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: block;">
											<!-- <span id="AdminVersion" style="display:inline-block; width:205px; float:left;">Admin package version *<span style="margin:0 0 0 55px;">:</span></span>
												<span id="AutoUpdateVersion" style="display:none; width:205px; float:left;">Auto Update version *<span style="margin:0 0 0 74px;">:</span></span>
												<span id="CatalogueVersion" style="display:none; width:205px; float:left;">Catalogue package version *<span style="margin:0 0 0 34px;">:</span></span> -->
											<span id="VirtualDesktopVersion"
												style="display: none; width: 205px; float: left;">Virtual
												Desktop version *<span style="margin: 0 0 0 59px;">:</span>
											</span>
											<span id="SchemaVersion"
												style="display: none; width: 205px; float: left;">Schema
												version *<span style="margin: 0 0 0 99px;">:</span>
											</span>
											<span id="ServiceVersion"
												style="display: none; width: 205px; float: left;">Service
												version *<span style="margin: 0 0 0 103px;">:</span>
											</span>
											<html:text property="version"
												styleClass="input-version-password" maxlength="10"
												style="margin:0; float:left;" />
										</div>
										<div class="lable"
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: block;">
											Reason for upload *
											<span style="margin: 0 0 0 85px;">:</span>
											<span class="helptext" style="vertical-align: top">Max
												500 chars.</span>
										</div>

										<div
											style="margin: 10px 0; clear: left; float: left; width: 100%; display: block;">
											<textarea name="reasonForUpload" class="textarea"
												style="margin: 0 0 0 200px;" cols="66" rows="6"
												tabindex="36"
												onKeyUp="textLimit(this,this.form.commentlen,500,'Comments');"></textarea>
											<span class="normaltext" style="margin: 0 0 0 200px;">Remaining
												characters </span>
											<input readonly type=text name=commentlen size=3 maxlength=3
												value="500" class="wordcount" />
										</div>
									</td>
								</tr>
								<tr>
									<td height="50" align="center">
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallSkyBuyCatalogueUpload();"
											tabindex="11"> Save </html:button>
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallHome();" tabindex="12"> Cancel </html:button>
									</td>

								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="nav-footer">
							<div class="nav-footer-right"></div>
							<div class="nav-footer-left"></div>
							<div class="nav-footer-content"></div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		</div>
	</td>
</tr>