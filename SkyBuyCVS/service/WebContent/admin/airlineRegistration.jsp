<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;   
}
function validateContactEmail(str,addstr)
{
	isValid=true;
	var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	if (!regExp.test(str)){
		isValid=false
		alert(addstr+" Email is an invalid e-mail address.");	
	}
	return (isValid)
	
	
}	
function getValidTelephone(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp.length != 10){		
		return "";
	}
	if (phTemp=="0000000000"){		
		return "";
	}
	return phTemp;
}
function getValidZip(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp=="00000"){		
		return "";
	}
	return phTemp;
}	

function IsValidUserIdOrPassword(sText){
   var ValidChars = " ";
   var IsValid=true;
   var Char;
   for (i = 0; i < sText.length && IsValid == true; i++){ 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) != -1) 
         {
         	IsValid = false;
         }
      }
   return IsValid;   
}
function fnCallAddAirline(){		
	var userId = document.forms[0].airUserId.value;
	if(userId.length<3){	
		alert("Please enter Air Charter User Id more than 3 characters");
		document.forms[0].airUserId.focus();
		return false;
	}
	
	if(!IsValidUserIdOrPassword(userId)){	
		alert("Please enter valid Air Charter User Id");
		document.forms[0].airUserId.focus();
		return false;
	}
	var password = document.forms[0].airPassword.value;
	if(password.length<3){	
		alert("Please enter Air Charter Password more than 3 characters");
		document.forms[0].airPassword.focus();
		return false;
	}	
	if(!IsValidUserIdOrPassword(password)){	
		alert("Please enter valid Air Charter Password");
		document.forms[0].airPassword.focus();
		return false;
	}	
	var phoneNo;	
	phoneNo = getValidTelephone(document.forms[0].airPhone1.value)
	
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].airPhone1.focus();
		return false;
	} else {	
		document.forms[0].airPhone1.value = phoneNo;
	}
	
	
	
	var phoneNoExt1=document.forms[0].airPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Ext should be valid.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>6){
			alert("Ext should not be more than 6  digits.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		} 
	}
	
	var mobile;
	if(document.forms[0].airMobile1.value!=""){
		mobile=getValidTelephone(document.forms[0].airMobile1.value)
		if(mobile==""){
		alert("Mobile Number should be valid");
			document.forms[0].airMobile1.focus();
			return false;
		} else {	
			document.forms[0].airMobile1.value = mobile;
		}
	}
	
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].airFax.value)	
	if (document.forms[0].airFax.value != null && document.forms[0].airFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airFax.focus();
			return false;
		} else {	
			document.forms[0].airFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].airZip.value)	
	if (document.forms[0].airZip.value != null && document.forms[0].airZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].airZip.focus();
			return false;
		} else {	
			document.forms[0].airZip.value = zipNo;
		}
	}
	var phoneNo2=document.forms[0].airPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Second Contact Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airPhone2.focus();
			return false;
		} else {	
			document.forms[0].airPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].airPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Second Contact Ext should be valid.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>6){
			alert("Second Contact Ext should not be more than 6  digits.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		} 
	}
	var Mobile2;
	if(document.forms[0].airMobile2.value!=""){
		Mobile2 = getValidTelephone(document.forms[0].airMobile2.value)
		if(Mobile2==""){
		alert("Second Contact Mobile Number should be valid.");
			document.forms[0].airMobile2.focus();
			return false;
		} else {	
			document.forms[0].airMobile2.value = Mobile2;
		}	
	}
	
	var phoneeNo2=document.forms[0].custServicePhoneno.value ;
	if(trim(phoneeNo2)!=""){	
		phoneeNo2 = getValidTelephone(phoneeNo2);
		if(phoneeNo2==""){
		alert("Customer Service Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].custServicePhoneno.focus();
			return false;
		} else {	
			document.forms[0].custServicePhoneno.value = phoneeNo2;
		}
	}
	
	var phoneNoExtt2=document.forms[0].custServicePhonenoext.value ;
	if(trim(phoneNoExtt2)!=""){	
		if(!IsNumeric(phoneNoExtt2)){
			alert("Customer Service  Ext should be valid.");
			document.forms[0].custServicePhonenoext.focus();
			return false;
		}
		if(phoneNoExtt2.length>6){
			alert("Customer Service  Ext should not be more than 6  digits.");
			document.forms[0].custServicePhoneext.focus();
			return false;
		} 
	}
	
	
	
	var sEmail1;
	sEmail1=validateContactEmail(document.forms[0].airEmail.value,"");
	if(!sEmail1) {
		document.forms[0].airEmail.focus();	
		return false;
	} 
	var sEmail2;
	if(document.forms[0].airEmail2.value!="") {
		sEmail2=validateContactEmail(document.forms[0].airEmail2.value, "Second Person");
		if(!sEmail2) {
			document.forms[0].airEmail2.focus();
			return false;
		}  
	}
	var sCustEmail;
	sCustEmail=validateContactEmail(document.forms[0].custServiceEmail.value, "Customer Service"); 
	if(!sCustEmail) {
		document.forms[0].custServiceEmail.focus();
		return false;
	} 
	
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- starts**/
	if(trim(document.forms[0].airEmail2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	if(trim(document.forms[0].airPhone2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	if(trim(document.forms[0].airPhoneExt2.value)!=""){
		if(trim(document.forms[0].airPhone2.value)!=""){
			if(trim(document.forms[0].airContactName2.value)==""){
				alert("Enter Second Person Contact Name.");
				document.forms[0].airContactName2.focus();
				return false;
			}
		}else {
			alert("Enter Second Person Phone.");
			document.forms[0].airPhone2.focus();
			return false;
		}	
	}
	if(trim(document.forms[0].airMobile2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	var imagePath = trim(document.forms[0].airlineLogo.value);
	if(!getFile(imagePath, 'Air Charter Logo')) {
		return false;
	}
	var aboutMePath = trim(document.forms[0].aboutMe.value);
	if(!getAboutMeFile(aboutMePath, 'About Me')) {
		return false;
	}
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- ends**/
	
	
	if((sEmail1 == true && sEmail2 == true && sCustEmail == true) || (sEmail1 == true && sCustEmail == true && document.forms[0].					      airEmail2.value==""))
		{
		document.forms[0].action="addAirlineReg.do?method=addAirlineReg";
		document.forms[0].submit();
	}	
}
function fnCallEditAirline(){

	var userId = document.forms[0].airUserId.value;
	if(userId.length<3){	
		alert("Please enter Air Charter User Id more than 3 characters");
		document.forms[0].airUserId.focus();
		return false;
	}
	
	if(!IsValidUserIdOrPassword(userId)){	
		alert("Please enter valid Air Charter User Id");
		document.forms[0].airUserId.focus();
		return false;
	}
	
	var password = document.forms[0].airPassword.value;
	if(password.length<3){	
		alert("Please enter Air Charter Password more than 3 characters");
		document.forms[0].airPassword.focus();
		return false;
	}	
	if(!IsValidUserIdOrPassword(password)){	
		alert("Please enter valid Air Charter Password");
		document.forms[0].airPassword.focus();
		return false;
	}	

	var phoneNo;	
	phoneNo = getValidTelephone(document.forms[0].airPhone1.value)
	
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].airPhone1.focus();
		return false;
	} else {	
		document.forms[0].airPhone1.value = phoneNo;
	}
	
	
	
	var phoneNoExt1=document.forms[0].airPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Ext should be valid.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>6){
			alert("Ext should not be more than 6  digits.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		} 
	}
	
	var mobile;
	if(document.forms[0].airMobile1.value!=""){
		mobile=getValidTelephone(document.forms[0].airMobile1.value)
		if(mobile==""){
		alert("Mobile Number should be valid");
			document.forms[0].airMobile1.focus();
			return false;
		} else {	
			document.forms[0].airMobile1.value = mobile;
		}
	}
	
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].airFax.value)	
	if (document.forms[0].airFax.value != null && document.forms[0].airFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airFax.focus();
			return false;
		} else {	
			document.forms[0].airFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].airZip.value)	
	if (document.forms[0].airZip.value != null && document.forms[0].airZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].airZip.focus();
			return false;
		} else {	
			document.forms[0].airZip.value = zipNo;
		}
	}
	var phoneNo2=document.forms[0].airPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Second Contact Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airPhone2.focus();
			return false;
		} else {	
			document.forms[0].airPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].airPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Second Contact Ext should be valid.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>6){
			alert("Second Contact Ext should not be more than 6  digits.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		} 
	}
	
	var Mobile2;
	if(document.forms[0].airMobile2.value!=""){
		Mobile2 = getValidTelephone(document.forms[0].airMobile2.value)
		if(Mobile2==""){
		alert("Second Contact Mobile Number should be valid.");
			document.forms[0].airMobile2.focus();
			return false;
		} else {	
			document.forms[0].airMobile2.value = Mobile2;
		}	
	}
	
	var phoneeNo2=document.forms[0].custServicePhoneno.value ;
	if(trim(phoneeNo2)!=""){	
		phoneeNo2 = getValidTelephone(phoneeNo2);
		if(phoneeNo2==""){
		alert("Customer Service Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].custServicePhoneno.focus();
			return false;
		} else {	
			document.forms[0].custServicePhoneno.value = phoneeNo2;
		}
	}
	
	var phoneNoExtt2=document.forms[0].custServicePhonenoext.value ;
	if(trim(phoneNoExtt2)!=""){	
		if(!IsNumeric(phoneNoExtt2)){
			alert("Customer Service  Ext should be valid.");
			document.forms[0].custServicePhonenoext.focus();
			return false;
		}
		if(phoneNoExtt2.length>6){
			alert("Customer Service  Ext should not be more than 6  digits.");
			document.forms[0].custServicePhoneext.focus();
			return false;
		} 
	}
	
	var sEmail1;
	sEmail1=validateContactEmail(document.forms[0].airEmail.value,"");
	if(!sEmail1) {
		document.forms[0].airEmail.focus();	
		return false;
	} 
	var sEmail2;
	if(document.forms[0].airEmail2.value!="") {
		sEmail2=validateContactEmail(document.forms[0].airEmail2.value, "Second Person");
		if(!sEmail2) {
			document.forms[0].airEmail2.focus();
			return false;
		}  
	}
	var sCustEmail;
	sCustEmail=validateContactEmail(document.forms[0].custServiceEmail.value, "Customer Service"); 
	if(!sCustEmail) {
		document.forms[0].custServiceEmail.focus();
		return false;
	} 
	
	
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- starts**/
	if(trim(document.forms[0].airEmail2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	if(trim(document.forms[0].airPhone2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	if(trim(document.forms[0].airPhoneExt2.value)!=""){
		if(trim(document.forms[0].airPhone2.value)!=""){
			if(trim(document.forms[0].airContactName2.value)==""){
				alert("Enter Second Person Contact Name.");
				document.forms[0].airContactName2.focus();
				return false;
			}
		}else {
			alert("Enter Second Person Phone.");
			document.forms[0].airPhone2.focus();
			return false;
		}	
	}
	if(trim(document.forms[0].airMobile2.value)!=""){
		if(trim(document.forms[0].airContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].airContactName2.focus();
			return false;
		}
	}
	
	var imagePath = trim(document.forms[0].airlineLogo.value);
	if(!getUpdatedFile(imagePath, 'Air Charter Logo')) {
		return false;
	}
	var aboutMePath = trim(document.forms[0].aboutMe.value);
	if(!getAboutMeFile(aboutMePath, 'About Me')) {
		return false;
	}
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- ends**/
	
	
	if((sEmail1 == true && sEmail2 == true && sCustEmail == true) || (sEmail1 == true && sCustEmail == true && document.forms[0].					      airEmail2.value==""))
		{
		document.forms[0].action="updateAirlineReg.do?method=updateAirlineDetails";
		document.forms[0].submit();
	}
}

function getFile(imagePath,jsField){
	if(imagePath=='') {
		alert("Air Charter Logo is required.");
		return false;
	}
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
		return true;
	} else {
		alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
		return false;
	}
}

function getUpdatedFile(imagePath,jsField){
	if(imagePath!='') {	
		var pathLength = imagePath.length;
		var lastDot = imagePath.lastIndexOf(".");
		var fileType = imagePath.substring(lastDot,pathLength);
		if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
			return true;
		} else {
			alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
			return false;
		}
	}else{
		return true;
	}
}

function getAboutMeFile(filePath,jsField){
	if(filePath!='') {	
		var pathLength = filePath.length;
		var lastDot = filePath.lastIndexOf(".");
		var fileType = filePath.substring(lastDot,pathLength);
		if((fileType == ".swf") || (fileType == ".SWF")) {
			return true;
		} else {
			alert("We supports .SWF file format. "+jsField+" file-type is " + fileType );
			return false;
		}
	}else{
		return true;
	}
}
function textLimit(field, countfield,maxlen,dispName)
 {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (fieldvallength > maxlen){
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
}
function fnCancel() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();

}

function fnCallSearchAirline(jsAirId) {
	<c:if test="${!empty SourceOfEdit and SourceOfEdit eq 'View'}">
		document.forms[0].action="viewAirline.do?method=viewAirlineDetails&airId="+jsAirId;
		document.forms[0].submit();
	</c:if>
	<c:if test="${empty SourceOfEdit or SourceOfEdit ne 'View'}">
		document.forms[0].action="searchAirline.do?method=searchAirline";
		document.forms[0].submit();
	</c:if>
}

function fnCopyMainToCustomerService(){
	document.forms[0].custServicePhoneno.value = document.forms[0].airPhone1.value;
	document.forms[0].custServicePhonenoext.value = document.forms[0].airPhoneExt1.value;
	document.forms[0].custServiceEmail.value = document.forms[0].airEmail.value;
}
function fnClearCustomerService(){
	document.forms[0].custServicePhoneno.value = document.forms[0].custServicePhonenohidden.value;
	document.forms[0].custServicePhonenoext.value = document.forms[0].custServicePhonenoexthidden.value;
	document.forms[0].custServiceEmail.value = document.forms[0].custServiceEmailhidden.value;
}

function fnCopyMainToReturnAddress(){
	document.forms[0].returnAddr1.value = document.forms[0].airAddr1.value;
	document.forms[0].returnAddr2.value = document.forms[0].airAddr2.value;
	document.forms[0].returnCity.value = document.forms[0].airCity.value;
	document.forms[0].returnState.value = document.forms[0].airState.value;
	document.forms[0].returnZip.value = document.forms[0].airZip.value;
}

function fnClearReturnAddress(){
	if(document.forms[0].returnStatehidden.value == '') {
		document.forms[0].returnState.value = "";
	}else {
		document.forms[0].returnState.value = document.forms[0].returnStatehidden.value;
	}
	document.forms[0].returnAddr1.value = document.forms[0].returnAddr1hidden.value;
	document.forms[0].returnAddr2.value = document.forms[0].returnAddr2hidden.value;
	document.forms[0].returnCity.value = document.forms[0].returnCityhidden.value;
	document.forms[0].returnZip.value = document.forms[0].returnZiphidden.value;
}
function fnCallServiceSameAsMain(){
	
	if(document.forms[0].customerService.checked){
		fnCopyMainToCustomerService();
	}

}
function fnCallServiceSameAsMainOnClick(){
	
	if(document.forms[0].customerService.checked){
		fnCopyMainToCustomerService();
	}else{
		fnClearCustomerService();
	}

}

function fnCallReturnSameAsMain(){
	
	if(document.forms[0].returnDetails.checked){
		fnCopyMainToReturnAddress();
	}

}

function fnCallReturnSameAsMainOnClick(){
	
	if(document.forms[0].returnDetails.checked){
		fnCopyMainToReturnAddress();
	}else{
		fnClearReturnAddress();
	}
}

function getCatalogue(link, windowname){
		window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');
	}

</script>

<tr> <td>
<div class="contentcontainer">
<logic:present name="adminLoginInfo" scope="session">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
   
    
     <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<c:if test="${adminLoginInfo.userType eq 'admin' && mode eq 'airlineAdd'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Air Charter</li>
		</ul>
		</c:if>
	
	<c:if test="${adminLoginInfo.userType eq 'admin' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Air Charter</li>
		</ul>
		</c:if>
	
	
	
			</div>			
		</div>
		<!-- end nav-header -->
	</td>
    
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	<html:javascript formName="airlineRegForm"/>
<html:form action="admin/addAirlineReg" method="post" onsubmit="return validateAirlineRegForm(this);" enctype="multipart/form-data">
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3"  class="tablehead" align="center"><h2>Air Charter Setup</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="6" cellspacing="0" class="broder_top0">
	<c:if test="${!empty airNameairUserIdExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.NameUserId"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty airNameExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.Name"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty airUserIdExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.UserId"/></span></td>
	</tr>
	</c:if>
      
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable">Air Charter Name*</td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <logic:present name="mode">
            <logic:equal name="mode"  value="airlineAdd">
              <html:text property="airName" styleClass="input" tabindex="1" maxlength="63"/>
            </logic:equal>
          </logic:present>
          <logic:present name="mode">
            <logic:equal name="mode"  value="airlineEdit">
              <bean:write  name="airlineDetails" property="airName"/>
            </logic:equal>
          </logic:present>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name * </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airContactName" styleClass="input" tabindex="2" maxlength="63" />
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Status * </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="airStatus" tabindex="3" styleClass="select">
            <html:option value="A">Active</html:option>
            <html:option value="I">Inactive</html:option>
          </html:select>
        </span></td>
      	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Charge Type  * </span></td>
      	<td align="left" class="lable">:</td>
      	<td align="left"><span class="catagory">
          <html:select property="chargeType" tabindex="4" styleClass="select">
		   <html:option value="SA">Semi Automated</html:option>
            <html:option value="FA">Automated</html:option>           
          </html:select>
        </span></td>
      </tr>
	   <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter User Id *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airUserId" styleClass="input" tabindex="5" maxlength="63"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Password *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:password property="airPassword" styleClass="input" tabindex="6"  maxlength="63"/>
        </span></td>
      </tr>
	    <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <html:text property="airAddr1" styleClass="input" tabindex="7"  maxlength="127" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airAddr2" styleClass="input" tabindex="8"  maxlength="127" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airCity" styleClass="input" tabindex="9"  maxlength="63" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
    	   <html:select property="airState" styleId="state" tabindex="10" onchange="fnCallReturnSameAsMain();">
			 <html:option value="">------Select State------</html:option>
			 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
            </html:select>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="airCountry" styleClass="textarea2" tabindex="11">
			 <html:option value="US">US</html:option>
		  </html:select>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airZip" styleClass="input" tabindex="12" maxlength="9" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone *</td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="airPhone1" styleClass="input-phone" tabindex="13" maxlength="10" onchange="fnCallServiceSameAsMain();"/>
        </span></td>
              <td class="lable">Ext:</td>
              <td><span class="catagory">
                <html:text property="airPhoneExt1" styleClass="input-ext" tabindex="14" maxlength="6" onchange="fnCallServiceSameAsMain();"/>
              </span></td>
            </tr>
        </table></td>
        <td align="left" class="lable">Mobile </td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
                <html:text property="airMobile1" styleClass="input-phone" tabindex="15" maxlength="10"/>
              </span></td>
              </tr>
      
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Fax  </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airFax" styleClass="input" tabindex="16" maxlength="10"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airEmail" styleClass="input" tabindex="17" maxlength="63" onchange="fnCallServiceSameAsMain();"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">PO Percentage * </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="poPct" styleClass="input" tabindex="18" maxlength="6"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Commission Percentage * </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airCommPct" styleClass="input" tabindex="18" maxlength="63"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Upload Air Charter Info</td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <c:if test="${mode ne 'airlineEdit'}">
	      <tr class="tdbg">
	        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Logo * </span></td>
	        <td align="left" class="lable">:</td>
	        <td align="left" colspan="4" valign="top"><span class="catagory">
	          <html:file property="airlineLogo" accept="image/jpeg" styleClass="browse" tabindex="19" />
	        </span></td>
	      </tr>
	      <tr class="tdbg">
	        <td align="left" nowrap="nowrap" class="lable" valign="top"><div style="margin:6px 0 0 0">About Me  </div></td>
	        <td align="left" class="lable" valign="top"><div style="margin:6px 0 0 0">:</div></td>
	        <td align="left" colspan="4" valign="top">
		            <html:file property="aboutMe" accept="image/jpeg" styleClass="browse" tabindex="19" />
		            <BR/>(Upload a file with extension .swf) 
	        </td>
	      </tr>
      </c:if>
      <c:if test="${mode eq 'airlineEdit'}">
      	<tr class="tdbg">
	        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter Logo * </span></td>
	        <td align="left" class="lable">:</td>
	        <td align="left" colspan="3"><span class="catagory">
	          <html:file property="airlineLogo" accept="image/gif,image/jpeg" styleClass="browse" tabindex="19" />
	        </span></td>
	        <td align="right" valign="top" nowrap="nowrap" class="lable"><c:if test="${!empty airlineDetails.airlineLogoPath and airlineDetails.airlineLogoPath ne ''}"><img src="<c:out value='${airlineDetails.airlineLogoPath}' />" width="178" height="52" /></c:if></td>
	      </tr>
	      
	      <tr class="tdbg">
	        <td align="left" nowrap="nowrap" class="lable" valign="top"><div style="margin:6px 0 0 0">About Me&nbsp;</div> </td>
	        <td align="left" class="lable" valign="top"><div style="margin:6px 0 0 0">:</div></td>
	        <td align="left" colspan="4"><span class="catagory">
	          <html:file property="aboutMe"  styleClass="browse" tabindex="19" />
	          <c:if test="${!empty airlineDetails.aboutMePath and airlineDetails.aboutMePath ne ''}">
		          <br/><a class="clickhere" href="javascript:getCatalogue('../previewAboutMe.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue
	          </c:if>
	          <BR/>(Upload a file with extension .swf only)  
	        </span></td>
	        <td align="right" valign="top" nowrap="nowrap" class="lable">
	        </td>
	      </tr>
      </c:if>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Second Person Contact Details </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
		<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airContactName2"  tabindex="20"  maxlength="63"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email  </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airEmail2" styleClass="input" tabindex="21" maxlength="63"/>
        </span></td>
        </tr>
        <tr class ="tdbg">
        <td align="left" class="lable">Phone </td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
                <html:text property="airPhone2" styleClass="input-phone" tabindex="22"   maxlength="10"/>
              </span></td>
              <td class="lable">Ext:</td>
              <td><span class="catagory">
                <html:text property="airPhoneExt2" styleClass="input-ext" tabindex="23" maxlength="6"/>
              </span></td>
            </tr>
        </table></td>
        
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Mobile </span></td>
        <td class="lable">:</td>
        <td align="left" valign="bottom"><span class="catagory">
          <html:text property="airMobile2" styleClass="input" tabindex="24" maxlength="10"/>
        </span><br></td>
      </tr>
        <tr class="tdbg">
          <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
        <tr class="tdbg">
          <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Customer Service &nbsp;
          <input type="checkbox" class="checkboxHTML" name="customerService" onclick="fnCallServiceSameAsMainOnClick();"/> Same as main information
          
          </td>
        </tr>
        <tr class="tdbg">
          <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone *</td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="custServicePhoneno" styleClass="input-phone" tabindex="25" maxlength="10"/>
          <html:hidden property="custServicePhonenohidden" value="${airlineRegForm.custServicePhoneno}"/>
        </span></td>
              <td class="lable">Ext:</td>
              <td><span class="catagory">
                <html:text property="custServicePhonenoext" styleClass="input-ext" tabindex="26" maxlength="6"/>
                <html:hidden property="custServicePhonenoexthidden" value="${airlineRegForm.custServicePhonenoext}"/>
              </span></td>
              </tr>
        </table></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email*</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="custServiceEmail" styleClass="input" tabindex="27" maxlength="63"/>
          <html:hidden property="custServiceEmailhidden" value="${airlineRegForm.custServiceEmail}"/>
        </span></td>
      </tr>
	  <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#efefef" class="lable">Return  Details &nbsp;
        <input type="checkbox" class="checkboxHTML" name="returnDetails" onclick="fnCallReturnSameAsMainOnClick();"/> Same as main information
        </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" nowrap="nowrap" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Company Address * </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnAddr1" styleClass="input" tabindex="28" maxlength="127"/>
          <html:hidden property="returnAddr1hidden" value="${airlineRegForm.returnAddr1}"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
        <td class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <html:text property="returnAddr2" styleClass="input" tabindex="29" maxlength="127"/>
          <html:hidden property="returnAddr2hidden" value="${airlineRegForm.returnAddr2}"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">City *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnCity" styleClass="input" tabindex="30" maxlength="63"/>
          <html:hidden property="returnCityhidden" value="${airlineRegForm.returnCity}"/>
        </span></td>
        <td align="left" class="lable"><span class="boldtext">State *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="returnState" styleClass="input" styleId="state" tabindex="31">
            <html:option value="">------Select State------</html:option>
            <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
          </html:select>
          <html:hidden property="returnStatehidden" value="${airlineRegForm.returnState}"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td class="lable">:</td>
        <td align="left"><html:select property="returnCountry" tabindex="32" styleClass="select">
            <html:option value="US">US</html:option>
          </html:select>        </td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnZip" styleClass="input" tabindex="33" maxlength="9"/>
          <html:hidden property="returnZiphidden" value="${airlineRegForm.returnZip}"/>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Return Days * </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnDays" styleClass="input" tabindex="34" maxlength="3"/>
        </span></td>
        <td align="left"  nowrap="nowrap" class="lable">&nbsp;</td>
        <td class="lable">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
	  <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Restrictions/Validity/Cancellation * </span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left">	
        	<span class="helptext" style="vertical-align:top">Max 500 chars.</span><br />
			<textarea name="airReturnPolicy" class="textarea"  cols="66" rows="6" tabindex="35" onKeyUp="textLimit(this,this.form.policylen,1000,'Return Policy');"><logic:present name="airlineDetails"><logic:notEmpty name="airlineDetails"><bean:write  name="airlineDetails" property="airReturnPolicy"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=policylen size=3 maxlength=3 value="1000" class="wordcount"/>        </td>
        </tr>
      
	  
      <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left">	
        	<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="airComments" class="textarea" cols="66" rows="6" tabindex="36" onKeyUp="textLimit(this,this.form.commentlen,250,'Comments');"></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/>        </td>
        </tr>
      
    </table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center">
         <logic:present name="mode">
			 <logic:equal name="mode"  value="airlineAdd">
	  <html:button property="method" styleClass="button" 
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallAddAirline(); void 0" tabindex="37"> Add </html:button> 
		   <html:button property="method" onclick="javascript:fnCancel();" styleClass="button" tabindex="38">Cancel</html:button>
		   </logic:equal> 
		</logic:present>   
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="airlineEdit">
	  <html:button property="method" styleClass="button" 
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallEditAirline(); void 0" tabindex="37">Save </html:button> 
		   <input type="button" onclick="javascript:fnCallSearchAirline('<c:out value="${airlineRegForm.airId}" />');" class="button" tabindex="38" value="Cancel" />
		   </logic:equal>
		</logic:present> 
		  
       </td>
       
      </tr>
    </table>
	<html:hidden property="airId"/>
	<logic:present name="mode">
		<logic:equal name="mode"  value="airlineEdit">
			 <html:hidden property="airName"/>
		</logic:equal>
	</logic:present>

<logic:present name="mode">
		<c:if test="${mode eq 'airlineAdd'}">
		<html:hidden property="airComments" value=""/>
		</c:if>

</logic:present> 	

</html:form>

	</td>
    </tr>
  
  <tr>
      <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
     </tr>
</table>

</logic:present>
</div>
</div>
</td>
</tr>


