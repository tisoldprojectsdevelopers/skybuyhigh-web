<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

function getCatalogue(link, windowname,jsProdId){
		makeRequest("searchMerchandize.do?method=createMerchandiseXml&prodId="+jsProdId);
}			
	function makeRequest(url){
	if(window.XMLHttpRequest){
		request = new XMLHttpRequest();		   
		request.onreadystatechange = onResponse;
		//request.overrideMimeType("text/xml"); 
	    request.open("POST", url, true);
	    request.send(null);	
				
	}
	else if(window.ActiveXObject){	  
		request = new ActiveXObject("MSXML2.XMLHTTP");
		request.onreadystatechange = onResponse;
		//request.overrideMimeType("text/xml"); 
	    request.open("POST", url, true);
	    request.send();		
	}	
//	sendRequest(url);
}

function onResponse(){
	if(checkReadyState(request)){
	   window.open('../previewProduct.jsp','merchandizeproduct', 'width=1024,height=600,scrollbars=Yes,resizable=Yes');
	}	
}
function checkReadyState(obj){
	/*if(obj.readyState == 0) { document.getElementById('copy').innerHTML = "Sending Request..."; }
	if(obj.readyState == 1) { document.getElementById('copy').innerHTML = "Loading Response..."; }
	if(obj.readyState == 2) { document.getElementById('copy').innerHTML = "Response Loaded..."; }
	if(obj.readyState == 3) { document.getElementById('copy').innerHTML = "Response Ready..."; }*/
	if(obj.readyState == 4)	{
		if(obj.status == 200){
			//document.getElementById("copy").style.display='none'; 
			return true;
		}		
	}
}
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormat1(datevar);
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
		
}

function dateFormat1(sDate){
	var parseYr;
	var yl=1990;
	var ym=2200;
		if(sDate.length==8 || sDate.length==10  ){
			if(sDate.length==10){
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
		   /*else if(sDate.length==6){
				var yTemp = sDate.substring(4,6);				
				y="20"+yTemp;
				m = sDate.substring(0,2);				
				d = sDate.substring(2,4);				
			}	*/
	if (y<yl || y>ym) {
		parseYr = y+""+m+""+d;
		return parseYr;
	}
	else
		return sDate;
	}
	return sDate;
}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
	fnCallSearch();
	}           
}
function fnValidateName(jsName,jsLabelName) {		
		if(isEmpty(jsName)){
			alert("Please enter "+jsLabelName);
			document.forms[0].airlineSearchValue.focus();
			return false;
		}else{	
			document.forms[0].action="searchMerchandize.do?method=searchMerchandize&MerchandizeAction=Approve";
			document.forms[0].submit();				
		}
	}	
function fnValidateId(jsId,jsLabelName) {		
		if(isEmpty(jsId)){
			alert("Please enter valid "+jsLabelName);
			document.forms[0].airlineSearchValue.focus();
			return false;
		}else if(!isNumber(jsId)){
			alert("Please enter valid "+jsLabelName);
			document.forms[0].airlineSearchValue.focus();
			return false;
		}else{	
			document.forms[0].action="searchMerchandize.do?method=searchMerchandize&MerchandizeAction=Approve";
			document.forms[0].submit();			
		}
	}
function fnCallSearch(){
	if(document.forms[0].effectiveDate.value == ""){
		alert("Please enter valid Catalogue Date ");
		document.forms[0].effectiveDate.focus();			
		return false;
	}else if(document.forms[0].effectiveDate.value != "" && !isDate(document.forms[0].effectiveDate.value)){
		alert("Please enter valid Catalogue Date ");
		document.forms[0].effectiveDate.focus();			
		return false;
	}
	else if(document.forms[0].searchBy.value=='Vendor:VendorName'){
		fnValidateName(document.forms[0].searchValue,"Vendor Name");
	}else if(document.forms[0].searchBy.value=='Airline:AirlineName'){
		fnValidateName(document.forms[0].searchValue,"Air Charter Name");
	}
	else{	
		document.forms[0].action="searchMerchandize.do?method=searchMerchandize&MerchandizeAction=Approve";
		document.forms[0].submit();			
	}
}

function fnCallEdit() {
	document.forms[0].action="updateMerchandize.do?method=updateMerchandizeStatus";
	document.forms[0].submit();

}
function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }
function fnCallInitSearchMerchandize() {
	document.forms[0].action="initSearchMerchandize.do?method=initSearchMerchandize&MerchandizeAction=Approve";
	document.forms[0].submit();
}

/********gtky search end *****/	
	
</script>
<html:form action="admin/searchMerchandize" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
     <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Accept/Reject Merchandise</li>
				</ul>
			</div>			
		</div>
	</td>
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap"><html:select property="searchBy" styleClass="textarea2" name="searchMerchandizeForm">
                <html:options collection="MerchandizeSearch" property="searchId" labelProperty="searchLabel"></html:options>
              </html:select>            </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Category :</td>
            <td nowrap="nowrap"><html:select property="category" styleClass="textarea2">
                <html:option value="All">All</html:option>
                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
				<html:option value="20">Private Jet Jaunts</html:option>	
            </html:select></td>
            <td nowrap="nowrap" class="lable">Product Type :</td>
            <td nowrap="nowrap"><html:select property="productType" styleClass="textarea2">
                <html:option value="All">All</html:option>
                <html:option value="ACP">Air Charter Package</html:option>
                <html:option value="CP">Checkout Product</html:option>
				<html:option value="M">Merchandise</html:option>	
            </html:select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
            <td nowrap="nowrap" class="lable">As of Date  : </td>
            <td nowrap="nowrap"><html:text property="effectiveDate"  styleClass="search-input" onkeydown="javascript:triggerEvent();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('effectiveDate', false, 'mdy', '/');" /></td>
            <td nowrap="nowrap" class="lable">Show in Catalogue :</td>
            <td nowrap="nowrap"><html:select property="prodStatus" styleClass="textarea2">
                <html:option value="A">Yes</html:option>
                <html:option value="I">No</html:option>
            </html:select></td>
            <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center" valign="top">
	<logic:present  name="searchMerchandizeForm" property="recordSet"> 
 	 <logic:notEmpty name="searchMerchandizeForm" property="recordSet">	
	<table  border="0" align="center" cellpadding="0" cellspacing="0" class="border" width="98%">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Merchandise Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0" style="margin:0 0 0 0;">
          <tr>
          <td align="center" class="table_header">Owner Type </td>
          <td class="table_header" nowrap="nowrap">Name</td>
          <td class="table_header" align="center">Product<br>Type</td>
          <td height="32" class="table_header">Item</td>
            <td class="table_header">Title</td>
            <td class="table_header" nowrap="nowrap">In-Store Price</td>
           <!-- <td nowrap="nowrap" class="table_header">Effective Date </td>-->
            <td class="table_header">Show in Catalogue</td>
            <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup> Price</td>
            <td class="table_header" width="170">Action</td>           
          </tr>
		  <logic:iterate name="searchMerchandizeForm" property="recordSet" id="MerchandizeInfo"  indexId="ctr">
          <tr class="tdbg">
          <td align="left"><div class="lable"> <c:if test="${MerchandizeInfo.ownerType eq 'vendor'}">
		  Vendor
		  </c:if>
		  <c:if test="${MerchandizeInfo.ownerType eq 'airline'}">
		  Air Charter
		  </c:if></div></td>
          <td align="left" nowrap="nowrap"><div class="lable"><bean:write name="MerchandizeInfo" property="ownerName"/></div></td>
          <td align="left" nowrap="nowrap" class="lable">
          		<c:if test="${MerchandizeInfo.isAdvt eq 'Y' and MerchandizeInfo.isSpecialProd eq 'N'}">
          			Air Charter Package
          		</c:if>
          		<c:if test="${MerchandizeInfo.isSpecialProd eq 'Y' and MerchandizeInfo.isAdvt eq 'N'}">
          			Checkout Product
          		</c:if>
          		<c:if test="${MerchandizeInfo.isSpecialProd eq 'N' and MerchandizeInfo.isAdvt eq 'N'}">
          			Merchandise
          		</c:if>
          </td>
            <td align="left" valign="center" class="catagory">
            <div align="center" class="editlink">
            	<c:if test="${MerchandizeInfo.isSpecialProd ne 'Y' and MerchandizeInfo.inShopStatus eq 'Active'}">
            		<a href="javascript:getCatalogue('../previewProduct.jsp','merchandizeproduct',<bean:write name="MerchandizeInfo" property="prodId"/>)">Preview</a>
            	</c:if>
            </div>
            <div align="center">
			<c:set var="imgpath"><bean:write name='MerchandizeInfo' property='mainImgPath'/></c:set>
			
			
			<html-el:img src="${MerchandizeInfo.mainImgPath}" width="79" height="79"   alt="${MerchandizeInfo.prodCode}"/>
            </div>
              <div class="lable"><bean:write name="MerchandizeInfo" property="prodCode"/></div>			  </td>
            <td align="left">
				<div style="width:250px; height:60px; word-wrap: break-word; overflow:auto;" >
			  <span class="lable"><bean:write name="MerchandizeInfo" property="brandName"/></span><br/>
                <span class="lable"><bean:write name="MerchandizeInfo" property="prodTitle"/></span><br/>
              <div class="desc">
             			 <logic:present name="MerchandizeInfo">
							<logic:notEmpty name="MerchandizeInfo">
						 		 <c:out value="${MerchandizeInfo.shortDesc}" escapeXml="false"/>
							 </logic:notEmpty>
					  	</logic:present>	
			</div>
              </div></td>
            <td align="right" valign="middle" nowrap="nowrap">
            	<div class="lable">
            		<c:if test="${MerchandizeInfo.isAdvt ne 'Y'}">
            			<fmt:setLocale value="en_US" /><fmt:formatNumber value="${MerchandizeInfo.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
            		</c:if>
            		<c:if test="${MerchandizeInfo.isAdvt eq 'Y'}">
            			N/A
            		</c:if>
            	</div>            
            </td>
           <!-- <td><div class="lable"><bean:write name="MerchandizeInfo" property="effectiveDate"/></div></td>-->
            <td align="center" class="lable">
			<c:if test="${MerchandizeInfo.inShopStatus eq 'Active'}">
				Yes
			</c:if>
			<c:if test="${MerchandizeInfo.inShopStatus eq 'InActive'}">
				No
			</c:if>
			
            <html-el:hidden property="merchandizeDetails[${ctr}].prodId" value="${MerchandizeInfo.prodId}"/>
            <td align="center">
            	<div class="lable">
            		<c:if test="${MerchandizeInfo.isAdvt ne 'Y'}">
            			<html-el:text property="merchandizeDetails[${ctr}].sbhPrice" value="${MerchandizeInfo.sbhPrice}" styleClass="search-input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);"/>
            		</c:if>
            		<c:if test="${MerchandizeInfo.isAdvt eq 'Y'}">
            			N/A
            		</c:if>
            	</div>
            </td>
            <td nowrap="nowrap" align="center" width="170">
			  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
	  <td><html-el:radio styleClass="input-radio" property="merchandizeDetails[${ctr}].sbhProdStatus" value="N" title="New" >New</html-el:radio> </td>
	  <td><html-el:radio styleClass="input-radio" property="merchandizeDetails[${ctr}].sbhProdStatus" value="A" title="Accept">Accept</html-el:radio></td>
	  <td><html-el:radio styleClass="input-radio" property="merchandizeDetails[${ctr}].sbhProdStatus" value="R" title="Reject"> Reject</html-el:radio></td>
	</tr>
	<tr>
	  <td colspan="3"><html-el:textarea property="merchandizeDetails[${ctr}].sbhComment" rows="4" value="" onkeyup="textLimit(this,256,'Comments');" styleClass="textarea1"  /></td>
	  </tr>
              </table>
			  
			
			
			</td>		
            </tr>
		   </logic:iterate>
      </table>
		</td>
       
    </table> 
	 </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
		</td>
    </tr>
	<logic:present  name="searchMerchandizeForm" property="recordSet"> 
 	 <logic:notEmpty name="searchMerchandizeForm" property="recordSet">	
  	<tr>
      <td height="50" colspan="3" align="center" class="td">	 
		 
		  <html:button property="method" styleClass="button" 
		   onclick="javascript:fnCallEdit()" >Submit </html:button> 	
          <html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallInitSearchMerchandize();" tabindex="18"> Cancel </html:button>
	  </td>
    </tr>
	</logic:notEmpty>
	</logic:present>
  <tr>
     <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
