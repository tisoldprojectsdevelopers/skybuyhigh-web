<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript">
	
	function isEligibleForSubmit() {
		var rejectValue=trim(document.forms[0].rejectReason.value);
		if(rejectValue != "") {
			return true;
		}else {
			alert("Reject reason is required");
			return false;
		}
	}
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	function fnCallSubmit(jsOrderItemId) {
		if(isEligibleForSubmit()) {
			document.forms[0].action="creditRejected.do?method=creditRejected&OrderItemId="+jsOrderItemId;
			document.forms[0].submit();
		}
	}
	
</script>
  
<html:form action="/admin/rejectOrder">
	<tr><td>
	<body>
		<div class="contentcontainer">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
				  <tr>
				   <td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Return</li>
				</ul>
			</div>			
		</div>
	</td>


				 
				 
				 
				  </tr>
				  <tr class="tdbg">
					  <td class="td" colspan="3">
						  <table border="0" cellpadding="0" cellspacing="0" class="border" style="margin:15px">
		  					<tr>
		  						<td class="tablehead" align="center" height="30">
		  							<b>Reject Order</b>
		  						</td>
		  					</tr>
							  <tr class="tdbg">
							  	<td class="td">
							  		<table border="0" cellpadding="0" cellspacing="0" style="margin:30px">
							  			<tr class="tdbg">
							  				<td align="center" class="lable">
							  					Reason for Reject the order    
							  				</td>
							  				<td>
							  					<b>:</b>
							  				</td>
							  				<td>
							  					<html:textarea rows="5" cols="60" property="rejectReason"></html:textarea>
							  				</td>
							  				
							  			</tr>
							  			<tr>
							  				<td colspan="3" align="center" class="td">
							  				<div style="margin:15px">
							  					<input type="button" value="Submit" onclick="fnCallSubmit('<c:out value="${OrderItemId}"/>')" class="button"/>
							  					<html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button></td>
							  				</div>
							  				</td>
							  			</tr>
							  		</table>
							  	</td>
							  </tr>
						  </table>
					  </td>
				  </tr>
				  <tr>
					 <td colspan="3">
					<div class="nav-footer">
					<div class="nav-footer-right"></div>
					<div class="nav-footer-left"></div>
					<div class="nav-footer-content"></div>			
					</div>
					</td>
				  </tr>
			 </table>
		</div>
	</body>
	</div>
	</td></tr>
</html:form>
