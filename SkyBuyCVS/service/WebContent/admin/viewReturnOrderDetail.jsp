<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />

<script>

	function fnCallOrderReturn() {
		document.forms[0].action="displayCreditChargeBack.do?method=displayCreditChargeBack";
		document.forms[0].submit();
	}
	
	function fnPaymentTxnRefDetails(orderItemId) {
		document.forms[0].action="displayReasonForFailure.do?method=getReasonForFailure&OrderItemId="+orderItemId;
		document.forms[0].submit();
	}
	
	function displayReturnPolicy(orderItemId,ownerType) {
		document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
		document.forms[0].submit();
	}
	
</script>

<html:form action="admin/editOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
   <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Accept/Reject Return</li>
				</ul>
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      
      	<tr>
        	<td height="30" colspan="3" align="center">
        		<c:if test="${ProcessStatus ne null and ProcessStatus ne ''}">
        			<font color="#FF0000"> <B> <c:out value="${ProcessStatus}" /> </B> </font>
        		</c:if> 
        	</td>
        </tr>
      
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="2" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
           	<td align="right" colspan="4" class="lable">
           		<c:if test="${(ViewReturnOrderDetail.orderItemStatus ne 'P')}">
	            	 <a href="javascript:fnPaymentTxnRefDetails('<bean:write  name="ViewReturnOrderDetail"  property="orderItemId" />')">Payment Transaction Details</a>
	            </c:if>
           	</td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="ViewReturnOrderDetail" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable" >Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ViewReturnOrderDetail" property="custFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" >Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              	<bean:write  name="ViewReturnOrderDetail" property="custLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"  valign="top"> Address Line 1 </td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" class="catagory" valign="top"><span class="catagory">
				<bean:write  name="ViewReturnOrderDetail" property="custAddress1" />		
       		</span></td>
       		<c:if test="${ViewReturnOrderDetail.custAddress2 ne null && ViewReturnOrderDetail.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable" > Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="ViewReturnOrderDetail" property="custAddress2" />		
	       		</span></td>
       		</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			   <bean:write  name="ViewReturnOrderDetail" property="custCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<logic:iterate id="states" name="StateList" scope="application">
					<c:if test="${states.stateCode eq ViewReturnOrderDetail.custState}">
						<bean:write name="states" property="stateName" />
					</c:if>
				</logic:iterate>
            </span></td>
          </tr>
          <tr class="tdbg">
             <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <bean:write  name="ViewReturnOrderDetail" property="custCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="ViewReturnOrderDetail" property="custZip" />
            </span></td>
          </tr>
		   <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             	<bean:write  name="ViewReturnOrderDetail" property="custPhone" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Email&nbsp;  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="ViewReturnOrderDetail" property="custEmail" />
            </span></td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ViewReturnOrderDetail" property="custShipFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="ViewReturnOrderDetail" property="custShipLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" valign="top"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" nowrap="nowrap" class="catagory" valign="top"><span class="catagory">
            <bean:write  name="ViewReturnOrderDetail" property="custShipAddress1" />
            </span></td>
            <c:if test="${(ViewReturnOrderDetail.custShipAddress2 ne null && ViewReturnOrderDetail.custShipAddress2 ne '')}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	            <bean:write  name="ViewReturnOrderDetail" property="custShipAddress2" />
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="ViewReturnOrderDetail" property="custShipCity" />
            </span></td>
            
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq ViewReturnOrderDetail.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              <!--  <bean:write  name="ViewReturnOrderDetail" property="custShipState" />-->
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="ViewReturnOrderDetail" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <bean:write  name="ViewReturnOrderDetail" property="custShipZip" />
             </span></td>
          </tr>
          <tr class="tdbg">
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ViewReturnOrderDetail" property="custShipPhone" />
            </span></td>
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ViewReturnOrderDetail" property="custShipEmail" />
            </span></td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable" > <span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ViewReturnOrderDetail"  property="airName"/>
            </span></td>
            <c:if test="${ViewReturnOrderDetail.flightNo ne null}">
            	<td align="left" nowrap="nowrap" class="lable" >Tail No </td>
            	<td align="left" class="lable">:</td>
            </c:if>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ViewReturnOrderDetail"  property="flightNo" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" ><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="ViewReturnOrderDetail"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="ViewReturnOrderDetail"  property="orderItemId"/>','<bean:write  name="ViewReturnOrderDetail"  property="ownerType"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="9" align="left" class="lable"><h4>Credit Card Details :</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ViewReturnOrderDetail"  property="custFirstName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Last Name </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ViewReturnOrderDetail"  property="custLastName" />
            </span></td>
			
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Credit Card No</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="ViewReturnOrderDetail"  property="maskCCNo"/>
        </span></td>
		 <td align="left" class="lable"><span class="boldtext">Credit Card Type</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
            <c:if test="${ViewReturnOrderDetail.cardType eq 'VC'}">
            	Visa Card
            </c:if>
            <c:if test="${ViewReturnOrderDetail.cardType eq 'MC'}">
            	Master Card
            </c:if>
            <c:if test="${ViewReturnOrderDetail.cardType eq 'AC'}">
            	American Express
            </c:if>
        </span></td>
		  </tr>
		  <tr>
		   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Expiry Date</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="ViewReturnOrderDetail"  property="expDt"/>
        </span></td>
        <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
		  </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ViewReturnOrderDetail"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="11" align="left" valign="top" class="lable"><div align="center"><img src="<c:out value='${ViewReturnOrderDetail.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			    <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Item Id</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ViewReturnOrderDetail"  property="orderItemId" />
               </span></td>
			   </tr>
			   <c:if test="${ViewReturnOrderDetail.rmaGeneratedNo ne null and ViewReturnOrderDetail.rmaGeneratedNo ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">RMA Number</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                 <bean:write  name="ViewReturnOrderDetail"  property="rmaGeneratedNo" />
		               </span></td>
				   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ViewReturnOrderDetail"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq ViewReturnOrderDetail.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${ViewReturnOrderDetail.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="ViewReturnOrderDetail"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ViewReturnOrderDetail"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ViewReturnOrderDetail"  property="itemName"/>
               </span></td>
			   </tr>
			   <c:if test="${ViewReturnOrderDetail.ownerType eq 'airline'}">
				  <tr class="tdbg">
				  <c:if test="${LoginType eq 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="YES"/>
				  </c:if>
				  <c:if test="${LoginType ne 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="NO"/>
				  </c:if>
				  <c:if test="${ViewReturnOrderDetail.travelDate ne null || (LoginType eq 'admin' && (ViewReturnOrderDetail.orderItemStatus eq 'P'))}">
					   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Travel Date </span></td>
					   <td align="left" class="lable">:</td>
				  </c:if>
				   <td align="left" class="catagory"><span class="catagory">
	                	<bean:write  name="ViewReturnOrderDetail" property="travelDate" />
                   </span></td>
				   </tr>
			   </c:if>

			  <c:if test="${ViewReturnOrderDetail.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="ViewReturnOrderDetail"  property="brandName" />
				   </span></td>
				 </tr>
			 </c:if>
			 <!-- <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'P' && ViewReturnOrderDetail.ownerType eq 'vendor'}"> Order to be placed with Vendor</c:if>
                 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'P' && ViewReturnOrderDetail.ownerType eq 'airline'}"> Order to be placed with Airline</c:if>				                 
                 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'F'}"> Failed </c:if>
                 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'R'}"> Rejected </c:if>
                 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'Q' && ViewReturnOrderDetail.ownerType eq 'vendor'}"> To be charged after Vendor approval</c:if>
				 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'Q' && ViewReturnOrderDetail.ownerType eq 'airline'}"> To be charged after Airline approval</c:if>
				 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'C'}"> Completed</c:if>
				 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'A'}"> RMA Approved</c:if>
				 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'E'}"> RMA Rejected</c:if>

               </span></td>
			   </tr>-->
			   <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'F' && ViewReturnOrderDetail.reasonForFailure ne null && ViewReturnOrderDetail.reasonForFailure ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Failure</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="ViewReturnOrderDetail"  property="reasonForFailure" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${ViewReturnOrderDetail.orderItemStatus eq 'R' && ViewReturnOrderDetail.reasonForReject ne null && ViewReturnOrderDetail.reasonForReject ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Rejected</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="ViewReturnOrderDetail"  property="reasonForReject" />
				   </span></td>
				 </tr>
			 </c:if>
			 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Return Quantity</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="ViewReturnOrderDetail" property="custReturnQuantity" />
	                </span></td>
			   </tr>
			 	<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Approved Quantity</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="ViewReturnOrderDetail" property="returnQuantity" />
	                </span></td>
			   </tr>
			   <c:if test="${ViewReturnOrderDetail.rmaAuthorizeSignatory ne null and ViewReturnOrderDetail.rmaAuthorizeSignatory ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable" valign="top">RMA Authorize Signatory </td>
					   <td align="left" class="lable" valign="top">:</td>
					   <td align="left" class="catagory" valign="top"><span class="catagory">
					   		<bean:write name="ViewReturnOrderDetail" property="rmaAuthorizeSignatory" />
		               </span></td>
				   </tr>
			   </c:if>		
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${ViewReturnOrderDetail.ownerType eq 'vendor'}">		   
			   In-Store Price
			   </c:if>
			   <c:if test="${ViewReturnOrderDetail.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${ViewReturnOrderDetail.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               
               </span></td>			  
			   </tr>
			   
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${ViewReturnOrderDetail.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00" />  
             
               </span></td>
			</tr>
			<c:if test="${ViewReturnOrderDetail.chargeBackPrice ne null and ViewReturnOrderDetail.chargeBackPrice ne ''}">
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Charge Back Unit Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   		<fmt:formatNumber value="${ViewReturnOrderDetail.chargeBackPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00" />
               </span></td>
			</tr>
			</c:if>
			<c:if test="${ViewReturnOrderDetail.chargeBackAmount ne null and ViewReturnOrderDetail.chargeBackAmount ne '' }">
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Charge Back Total Amount </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   		<fmt:formatNumber value="${ViewReturnOrderDetail.chargeBackAmount}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00" />
               </span></td>
			</tr>
			</c:if>
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Return Status</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   	  <c:if test="${ViewReturnOrderDetail.rmaStatus eq 'WR'}"> Waiting for Admin to Reject </c:if>
                  <c:if test="${ViewReturnOrderDetail.rmaStatus eq 'WA'}"> Waiting for Admin to Charge back </c:if>
                  <c:if test="${ViewReturnOrderDetail.rmaStatus eq 'RG'}"> RMA Generated </c:if>
                  <c:if test="${ViewReturnOrderDetail.rmaStatus eq 'RA'}"> RMA Approved </c:if>
                  <c:if test="${ViewReturnOrderDetail.rmaStatus eq 'RR'}"> RMA Rejected </c:if>
               </span></td>
			</tr>
			 	  
			<tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable"  valign="top">Reason for Return </td>
			   <td align="left" class="lable"  valign="top">:</td>
			   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
			   		<bean:write  name="ViewReturnOrderDetail" property="returnReason" />  
               </span></td>
			</tr>
			<c:if test="${ViewReturnOrderDetail.vendorComments ne null and ViewReturnOrderDetail.vendorComments ne ''}">
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Vendor Comments </td>
				   <td align="left" class="lable"  valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
				   		<bean:write  name="ViewReturnOrderDetail" property="vendorComments" />  
	               </span></td>
				</tr>
			</c:if>
			<c:if test="${ViewReturnOrderDetail.adminComments ne null and ViewReturnOrderDetail.adminComments ne ''}">
	        	<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Admin Comments </td>
				   <td align="left" class="lable"  valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
						<bean:write name="ViewReturnOrderDetail" property="adminComments"/>				         	
	               </span></td>
				</tr>
			</c:if>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 

			<input type="button" onclick="javascript:fnCallOrderReturn()" class="button" title="OK" value="OK" />
			 
		 </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</td></tr>
</html:form>

