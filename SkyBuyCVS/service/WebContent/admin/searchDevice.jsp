<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>


<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

	 function triggerEvent() {
		if(event.keyCode==13) {
		  fnCallSearch();  
		}           
	 }
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	/*function fnCallCustInfoByCustIdAndName(){
		if(document.forms[0].vendorSearchBy.value=="" && document.forms[0].vendorSearchBy.value!='All'){
			alert("Please select either Customer ID or Customer/DBA Name");
			return;
		}
		else if(document.forms[0].vendorSearchBy.value=="customerId"){
			fnCallCustInfoByCustId(document.forms[0].vendorSearchValue);
		}else if(document.forms[0].vendorSearchBy.value=="customerName"){			
			fnCallCustInfoByCustName(document.forms[0].vendorSearchValue);
		}
		else{	
			if(document.forms[0].vendorSearchBy.value=="All"){
			   document.forms[0].vendorSearchValue.value="";
			}		
			document.forms[0].action="searchVendor.do?method=searchVendor";
			document.forms[0].submit();
		}
	}
	function fnCallCustInfoByCustId(jsCustIdValue) {
		var jsVendorSearchValue = document.forms[0].vendorSearchValue.value;		
		if(isEmpty(jsCustIdValue)) {			
			alert("Please enter valid Customer ID");			
			document.forms[0].vendorSearchBy.focus();			
			return;			
		}
		else{	
			if(isNaN(jsVendorSearchValue)) {
				if(checkLength(jsVendorSearchValue)){
					document.forms[0].action="searchVendor.do?method=searchVendor";
					document.forms[0].submit();
				}
				else
					document.forms[0].vendorSearchBy.focus();
			}	
		}
	}
	function fnCallCustInfoByCustName(jsCustName) {		
		if(isEmpty(jsCustName)){
			alert("Please enter Customer/DBA Name");
			document.forms[0].vendorSearchBy.focus();
			return;
		}
		if(jsCustName.value.length<3){
			alert("Please enter atleast 3 characters");
			document.forms[0].vendorSearchValue.focus();
			return;
		}		
		else{					
			document.forms[0].action="searchVendor.do?method=searchVendor";
			document.forms[0].submit();	
		}
	}
	function isNumber(jsCustNo) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert("Customer ID must be an numeric and should be valid.");
			document.forms[0].vendorSearchBy.focus();						     
			return false;
		}
	  }
	 return true;
	}*/
	
	/*function checkLength(jsText){
		
		if((jsText < -2147483648) || (jsText > 2147483647)){
			alert("Please Enter valid Customer ID ");
			return false;
		}	
		return true;	
	}*/
	
	function fnValidateName(jsName,jsLabelName) {
		//var nameFormat = new RegExp("^[A-Za-z0-9_\- ]*$");
		var nameFormat = new RegExp("^[A-Za-z0-9 -_ ]*[A-Za-z0-9_ ]$");
		if(jsName == ""){
			alert("Please Enter "+jsLabelName);
			document.forms[0].deviceSearchValue.focus();
			return false;
		}else if(!nameFormat.test(jsName)) {
			alert("Enter Valid "+jsLabelName);
			document.forms[0].deviceSearchValue.focus();
			return false;
		}
		return true;			
	}	
	
	function fnValidateSerialNo(jsName,jsLabelName) {
		var nameFormat = new RegExp("^[A-Za-z0-9 -_ ]*[A-Za-z0-9]$");
		if(jsName == ""){
			alert("Please enter "+jsLabelName);
			document.forms[0].deviceSearchValue.focus();
			return false;
		}else if(!nameFormat.test(jsName)) {
			alert("Enter valid "+jsLabelName);
			document.forms[0].deviceSearchValue.focus();
			return false;
		}
		return true;			
	}
	
	function fnIsEligibleForSubmit() {
		var isEligible=true;
	    var jsDeviceSearchBy = trim(document.forms[0].deviceSearchBy.value);
	    var jsDeviceSearchValue = trim(document.forms[0].deviceSearchValue.value);
	    
	    if(jsDeviceSearchBy == 'All'){
		 	document.forms[0].deviceSearchValue.value = '';
		 	return isEligible;
		 }
		 else if(jsDeviceSearchBy == 'airName') {
			isEligible = fnValidateName(jsDeviceSearchValue, "Air Charter Name");
			return isEligible;
		}	
		else if(jsDeviceSearchBy == 'devSerialNo') {
			isEligible = fnValidateSerialNo(jsDeviceSearchValue,"Device Serial No.");
			return isEligible;
		}
		else if(jsDeviceSearchBy == 'devModel'){
			isEligible = fnValidateName(jsDeviceSearchValue, "Device Model");
			return isEligible;
		}else if(jsDeviceSearchBy == 'devId'){
			isEligible = fnValidateName(jsDeviceSearchValue, "Device Id");
			return isEligible;
		}
	}
/*** gtky search start  ****/
	function fnCallSearch() {
		if(fnIsEligibleForSubmit()){
			document.forms[0].action="searchDevice.do?method=searchDevice";
			document.forms[0].submit();
		}		
	}
	function fnCallEdit(jsDeviceIdValue) {
		document.forms[0].action="editDeviceReg.do?method=editDeviceDetails&deviceId="+jsDeviceIdValue;
		document.forms[0].submit();
	}
	function fnCallView(jsDeviceIdValue) {
		document.forms[0].action="updateDeviceReg.do?method=viewDeviceDetails&deviceId="+jsDeviceIdValue;
		document.forms[0].submit();
	}
	function fnCallDelete(jsDeviceIdValue,jsAirId) {
		var ans = confirm('Are you sure to delete the device');
		if(ans){
			document.forms[0].action="deleteDevice.do?method=deleteDeviceDetails&deviceId="+jsDeviceIdValue+"&airId="+jsAirId;
			document.forms[0].submit();
		}
	}
	function fnCallSelect(jsCustIdValue,jsCustName) {
		document.forms[0].action="gtkyRetrieveCustomer.do?method=retrieveCustomerByCustId&cid="+jsCustIdValue+"&custName="+jsCustName;
		document.forms[0].submit();
	}
/********gtky search end *****/	
	
</script>

<html:form action="admin/searchDevice.do?method=searchDevice" method="post" onsubmit="return false;">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Admin</li>
					<li>></li>
					<li>Edit/Search Device</li>
				</ul>
			</div>			
		</div>
	</td>
    
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td>
       
        <html:select property="deviceSearchBy" styleClass="textarea2">	
						 <html:option value="All">All</html:option>	
						 <html:option value="airName">Air Charter Name</html:option>									  		
	 		  			<html:option value="devSerialNo">Device Serial No</html:option>
						<html:option value="devModel">Device Model</html:option>
						<html:option value="devId">Device Id</html:option>
												
	               		 </html:select>	</td>
        <td class="lable">Search Value :</td>
        <td><html:text property="deviceSearchValue" styleClass="input" onkeydown="javascript:triggerEvent();"/></td>
        <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="deviceInfo" scope="request"> 
		 <logic:notEmpty name="deviceInfo">	
	<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC" class="broder_top0">
              <tr>
              	<td class="table_header" nowrap="nowrap">Device Id</td>
			  	<td height="32" nowrap="nowrap" class="table_header">Air Charter Code</td>
                <td height="32" nowrap="nowrap" class="table_header">Air Charter Name</td>
                <td class="table_header" nowrap="nowrap">Device Type</td>
                <td class="table_header" nowrap="nowrap">Device Serial No</td>              
                <td class="table_header" nowrap="nowrap">Product No</td>
                <td class="table_header" nowrap="nowrap">Device Model</td>
                
				<!--  <td class="table_header" nowrap="nowrap">Tail No</td> -->
				<td class="table_header" nowrap="nowrap">Status</td>
				<td class="table_header" nowrap="nowrap">Action</td>
              </tr>
              <logic:iterate id="DeviceInfo" name="deviceInfo">
              <tr class="tdbg">
              <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceCode"/></td>
			   <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="airId"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="airName"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF">
                <c:forEach items="${DeviceTypesList}" var="dType">
                   <c:if test="${dType.deviceId eq DeviceInfo.deviceType}" >
                      <c:out value="${dType.deviceName}" />
                   </c:if>
                </c:forEach></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceSerialNo"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="productNo"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceModel"/></td>
                
				<!-- <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="flightNo"/></td>  -->
				<td align="left" nowrap="nowrap" bgcolor="#FFFFFF">
	  			  <c:if test="${DeviceInfo.status eq 'A'}">Active</c:if>
		          <c:if test="${DeviceInfo.status eq 'I'}">Inactive</c:if>
		          <c:if test="${DeviceInfo.status eq 'N'}">New</c:if>
		          <c:if test="${DeviceInfo.status eq 'AL'}">Allocated</c:if>
		          <c:if test="${DeviceInfo.status eq 'NA'}">Not Allocated</c:if>
	  			</td>
              <td class="action" nowrap="nowrap" style="width:125px;" align="left">
              &nbsp;&nbsp;<a href="javascript: void fnCallView('<bean:write name='DeviceInfo' property='deviceId'/>')"  title="View Device">View</a>
              <c:if test="${DeviceInfo.status ne 'D'}">
              &nbsp;&nbsp;<a href="javascript: void fnCallEdit('<bean:write name='DeviceInfo' property='deviceId'/>')"  title="Edit Device">Edit</a>            
			  &nbsp;&nbsp;<a href="javascript: void fnCallDelete('<bean:write name='DeviceInfo' property='deviceId'/>','<bean:write name='DeviceInfo' property='airId'/>')"  title="Delete Device">Delete</a>
			
              </c:if>
              </td>
              </tr>
             </logic:iterate>
            </table></td>
      </tr>     
    </table>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present></td>
    </tr>
  
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>

</td></tr>
</html:form>
