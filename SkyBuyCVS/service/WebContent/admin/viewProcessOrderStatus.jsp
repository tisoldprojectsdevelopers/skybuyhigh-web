<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
	function fnCallSearchOrder(){
		<c:if test="${!empty SourceOfProcess and SourceOfProcess eq 'SearchOrder'}">
			document.forms[0].action="searchOrder.do?method=searchOrder";
			document.forms[0].submit();	
		</c:if>
		<c:if test="${!empty SourceOfProcess and SourceOfProcess eq 'PlaceOrder'}">
			document.forms[0].action="processOrder.do?method=initPlaceOrder";
			document.forms[0].submit();	
		</c:if>	
	}
	function fnCallEdit(jsOrderItemIdValue) {
		document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemIdValue;
		document.forms[0].submit();
	}
</script>

<html:form action="admin/addAirlineReg" method="post" >
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Place Order</li>
				</ul>
			</div>			
		</div>

	</td>
    
  </tr>
  
  
  
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	        <logic:present name="ItemStatusList">
	        	<logic:notEmpty name="ItemStatusList">
	        		<c:choose>
	        			<c:when test="${NoScroll eq 'NOSCROLL'}">
	        				<div>
	        			</c:when>
	        			<c:otherwise>
		        			<div class="placed_order_details">
		        		</c:otherwise>
		        	</c:choose>
		        		<table border="0" width="60%" cellspacing="1" cellpadding="0" bgcolor="#cccccc" class="broder_top0" align="center">
		        			<tr>
		        				<td colspan="9" class="tablehead" align="center" height="32"><b>Order Information</b></td>
		        			</tr>
		        			<tr>
		        				<td height="32" nowrap="nowrap" class="table_header">Payment Transaction Reference ID</td>
		        				<td class="table_header" nowrap="nowrap">Order Item ID</td>
		        				<td class="table_header" nowrap="nowrap">Order Confirmation No</td>		        				
		        				<td class="table_header" nowrap="nowrap">Item Code</td>
		        				<td class="table_header" nowrap="nowrap">Customer Name</td>
		        				<!-- <td class="table_header" nowrap="nowrap">Phone No</td>
		        				<td class="table_header" nowrap="nowrap">Category</td> -->
		        				<td class="table_header" nowrap="nowrap">Quantity</td>
		        				<td class="table_header" nowrap="nowrap">Status</td>
		        				<td class="table_header" nowrap="nowrap">Comments</td>
		        			</tr>
		        			<logic:iterate id="statusList" name="ItemStatusList">
			        			<tr bgcolor="#FFFFFF">
		        				  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="paymentTxnRefId"/> </td>
	        					  <td align="center" nowrap="nowrap" class="lable"><bean:write name="statusList" property="orderItemId"/></td>
	        					  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="custTransId"/> </td>
		        				  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="prodCode"/> </td>
		        				  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="custFirstName"/> </td>
		        				  <!--  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="custPhone"/> </td>
			        				<td align="left" nowrap="nowrap" class="lable">
			        					     
											<logic:iterate id="category" name="Category"scope="application">
												<c:if test="${category.cateId eq statusList.cateId}">
												<bean:write name="category" property="cateName" />
												</c:if>
											</logic:iterate>
											<c:if test="${statusList.cateId eq 20}">
											Private Jet Jaunts
											</c:if>
										 
						 	 	  </td> -->   
						 	 	  <td align="right" nowrap="nowrap" class="lable"> <bean:write name="statusList" property="qty"/>  </td>
			        				<td align="left" nowrap="nowrap">
			        					<div class="lable"> 
				        					<c:if test="${statusList.orderItemStatus eq 'P' && statusList.ownerType eq 'vendor'}">
											 	Order to be placed with Vendor
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'P' && statusList.ownerType eq 'airline'}">
											 	Order to be placed with Air Charter
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'C'}">
											  	Completed
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'F'}">
											 	Failed
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'R'}">
											 	Rejected
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'O'}">
											 	Canceled
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'Q' && statusList.ownerType eq 'vendor'}">
											 	To be charged after Vendor Approval
											 </c:if>
											 <c:if test="${statusList.orderItemStatus eq 'Q' && statusList.ownerType eq 'airline'}">
											 	To be charged after Air Charter Approval
											 </c:if> 
									  </div>
								  </td>
								  <td align="left" nowrap="nowrap" class="lable"><bean:write name="statusList" property="reasonForFailure"/> </td>
								</tr>
							</logic:iterate>
	        		  </table>
		        	</div>
	        	</logic:notEmpty>
	       	</logic:present>
	    </td>   	
        </tr>
	    <tr>
	        <td colspan="3">
	        	<table width="100%" border="0" cellpadding="2" cellspacing="0" class="broder_top0">
			 		<tr class="tdbg">
	       				 <td class="lable" nowrap="nowrap" align="center"></td>   
        
      				</tr>
				    <tr class="tdbg">
			        	<td  nowrap="nowrap" align="center" class="lable">
							<br/>
							Successfully processed the selected Order Items. &nbsp;<a href="searchOrder.do?method=searchOrder&OrderStatus=ALLORDERS">Click here</a> to view the  Order Item details<br/>
          				</td>  
        
     				 </tr>
	  				<tr class="tdbg">
        				<td  nowrap="nowrap" align="center" class="lable">
							<br/>
				 			<input type="button" class="button" onclick="javascript:fnCallSearchOrder();" tabindex="10" value="OK" />
          				</td>  
      				</tr>
	  				<tr class="tdbg">
	  					<td></td>
	 				</tr>
    			</table>
    		</td>
      	</tr>
	</td>
    </tr>
  <tr>
   <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>

</td></tr>
</html:form>
