<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
	}else {
		return datevar;
	}
}

function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
		fnCallSearch();
	}           
}

function fnCallSearch(){
	
	if(isEligibleForSubmit()) {
		document.forms[0].action="deviceAttendance.do?method=searchDeviceAttendance";
		document.forms[0].submit();			
	}
	
}

	function isEligibleForSubmit() {
	
		var searchby = trim(document.forms[0].searchType.value);
		var searchvalue = trim(document.forms[0].searchValue.value);
		var fromDate = trim(document.forms[0].fromDate.value);
		var toDate = trim(document.forms[0].toDate.value);
		var nameFormat = new RegExp("^[A-Za-z ]*$");
		var today = new Date();
		
		var month=today.getMonth()+1;
		var date=today.getDate();
		
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();

		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDate == ""){
			alert("Please enter valid To Date ");
			document.forms[0].toDate.focus();
			return false;
		}else {
			var isToDate=isDate(toDate);
			if(!isToDate) {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
				return false;
			}else if(isToDate=="Advance" || isToDate=="Past") {
				if(isToDate == "Past") {
					alert("Enter the To Date as earlier as you entered");
					return false;
				}else if(isToDate == "Advance") {
					alert("Enter the To Date as past as you entered");
					return false;
				}
			}else {
				document.forms[0].toDate.value = dateFormats(toDate);
			}
			if(fromDate == "" && !validateDate(dateFormats(toDate),today)) {
				alert("To Date should be earlier or equal to current Date");
				return false;
			}
		}
		
		if(!((toDate.substring(2,3) == "/" && toDate.substring(5,6) == "/") 
		   || (toDate.substring(2,3) == "-" && toDate.substring(5,6) == "-"))) {
			alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && !((fromDate.substring(2,3) == "/" && fromDate.substring(5,6) == "/") 
		   || (fromDate.substring(2,3) == "-" && fromDate.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && toDate != "") {
			
			isCorrectFromDate = isDate(fromDate);
			isCorrectToDate = isDate(toDate);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].fromDate.value = dateFormats(fromDate);
				document.forms[0].toDate.value = dateFormats(toDate);
				if(!validateDate(dateFormats(fromDate),dateFormats(toDate))) {
					alert("From Date should be earlier than To Date");
					return false;
				}else if(!validateDate(dateFormats(toDate),today)) {
					alert("To Date should be earlier or equal to current Date");
					return false;
				}/*else {
					return true;
				}*/
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
			 	return false;
			}
		}
		
		if(searchby != "ALL") {
			if(searchvalue != "") {
				document.forms[0].searchValue.value=trim(searchvalue);
				if(searchby == "AIRNAME") {
					if(trim(searchvalue)=='') {
						alert("You must enter a air charter name.");
					}else {
						return true;
					}
				}else if(searchby == "DEVICESERIALNO") {
					if(trim(searchvalue)=='') {
						alert("You must enter a valid device serial number.");
					}else {
						return true;
					}
				}else if(searchby == "FLIGHTNO") {
					if(trim(searchvalue)=='') {
						alert("You must enter a valid Tail no.");
					}else{
						return true;
					}
				}else {
					return true;
				}
			}else {
				alert("You must provide the search value");
			}
		}else {
			document.forms[0].searchValue.value="";
			return true;
		}
		
		return false;
	}


	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	

</script>
<html:form action="admin/deviceAttendance" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
  <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Admin</li>
					<li>></li>
					<li>Device Attendance</li>
				</ul>
			</div>			
		</div>
	</td>

 
 
 
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search Type : </td>
            <td nowrap="nowrap">			  
			  			 <html:select property="searchType" styleClass="textarea2">	
							 <html:option value="ALL">All</html:option>							  		
	 		  				<html:option value="AIRNAME">Air Charter Name</html:option>
	 		  				<html:option value="DEVICEID">Device Id</html:option>
							<html:option value="DEVICESERIALNO">Device Serial No</html:option>	
							<html:option value="FLIGHTNO">Tail No</html:option>	
																			
	               		 </html:select>
			          </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" maxlength="64" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Activity :</td>
            <td nowrap="nowrap"><html:select property="activity" styleClass="textarea2">
                <html:option value="ALL">All</html:option>				
                <html:option value="CATALGDOWNLOAD">Catalogue Download</html:option>	
                <html:option value="ORDPLACEMENT">Order Placement</html:option>
                <html:option value="DEVREG">Device Registered</html:option>
                <html:option value="AUTOUPD">Auto Update Activity</html:option>
                <html:option value="COREAUTOUPD">Core Auto Update Activity</html:option>
		    </html:select></td>
			
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
           	 <td nowrap="nowrap" class="lable">From Date: </td>
            <td nowrap="nowrap"><html:text property="fromDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('fromDate', false, 'mdy', '/');" /></td>
			<td nowrap="nowrap" class="lable">To Date: </td>
            <td nowrap="nowrap"><html:text property="toDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('toDate', false, 'mdy', '/');" /></td>	
            <td nowrap="nowrap">&nbsp;</td>
            
            <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center" valign="top">
	<logic:present  name="AttendanceInfo"> 
 	 <logic:notEmpty name="AttendanceInfo">	
	<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Device Attendance</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table border="0" width="100%" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
          <td nowrap="NOWRAP" class="table_header">Device Id</td>
          <td height="32" align="center" nowrap="NOWRAP" class="table_header">Air Charter Name</td>
          <td nowrap="NOWRAP" class="table_header">Tail No</td>
          <td nowrap="NOWRAP" class="table_header">Device Serial No</td>  
		  <td nowrap="NOWRAP" class="table_header">Activity</td>
		  <td nowrap="NOWRAP" class="table_header">Activity downloaded</td>
		  <td nowrap="NOWRAP" class="table_header">Order Confirmation No</td>        
          <td nowrap="NOWRAP" class="table_header">Total Orders</td>
		 <!--  <td class="table_header">Status</td> -->
          <td class="table_header">Date</td>
          </tr>
		  <logic:iterate id="attendanceInfo" name="AttendanceInfo">
          <tr class="tdbg">
          	  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="deviceCode"/></div></td>
          	  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="airlineName"/></div></td>
         	  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="flightNo"/></div></td>          	
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="deviceSerialNo"/></div></td>      
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="activity"/></div></td>     
			  <td align="left" nowrap="NOWRAP"><div class="lable"><c:out value="${attendanceInfo.activityDownloaded}" escapeXml="false"/></div></td>
			  <td align="center" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="custTransId"/></div></td>    			
			  <td align="center" nowrap="NOWRAP"><div class="lable"><bean:write name="attendanceInfo" property="totalOrders"/></div></td>   
			  <!--  <td align="center"><div class="lable"><bean:write name="attendanceInfo" property="status"/></div></td>  -->           	
			  <td align="center" nowrap="nowrap"><div class="lable"><bean:write name="attendanceInfo" property="date"/></div></td>
	       </tr>
		  </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="orderItemsInfo"> 
	 <logic:notEmpty name="orderItemsInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnHome();" styleClass="button">Cancel</html:button></td> 
    </tr>
	</logic:notEmpty>
	</logic:present> 
  <tr>
  <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
