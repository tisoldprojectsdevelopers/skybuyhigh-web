<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Mail - Order</title>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link type="text/css" href="../style/style.css" rel="stylesheet" media="all" />
<link type="text/css" href="../style/style-print.css" rel="stylesheet" media="print" />
<link rel="stylesheet" type="text/css" media="screen,projection" href="../style/Menu/scene.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../style/Menu/masthead.css" />
<link href="../style/index.css" rel="stylesheet"  type="text/css" />
<link href="../style/home.css" type="text/css" rel="stylesheet"  media="screen"/>
<link href="../style/registration.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="../style/ie.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../style/Menu/ie6-mh.css" media="screen"/>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="../style/ie7.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../style/Menu/ie7-mh.css" media="screen"/>
<![endif]-->
<script>

var testresults

function checkEmail(jsEmail,jsFieldName){
 var str=jsEmail;
 var filter=/^.+@.+\..{2,3}$/
	if(str != ''){
		 if (filter.test(str))
		    testresults=true
		 else {
		    alert("Please enter valid Email "+ jsFieldName +" address.")
		    testresults=false
		}
	}else{
		testresults = true;
	
	}
 return (testresults)
}

function fnCallSendMail(){
	if(checkEmail(document.forms[0].emailToAddr.value,"To") && checkEmail(document.forms[0].emailCcAddr.value,"CC") && checkEmail(document.forms[0].emailBccAddr.value,"BCC")){
		if(document.forms[0].emailSubject.value != ''){
			if(document.forms[0].emailContent.value != ''){
				document.forms[0].emailContent.value=document.forms[0].emailContent.value.replace(/\n/g,'<br/>');
				document.forms[0].emailContent.value=document.forms[0].emailContent.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
				
				document.forms[0].action="custFeedback.do?method=sendCustFeedbackEmail";
				document.forms[0].submit();
				alert("Email Sent successfully.");
				self.close();
			}else{
				alert('Please enter message');
			}
		}else{
			alert('Please enter email subject');
		}
	}
}
</script>

</head>
<body>
<form method="post" >

<table align="left" border="0" cellpadding="0" cellspacing="0" class="table" style="width:600px;">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td height="44" align="left" background="../images/top_nav_middlebg.png"> 
		
		<strong>&nbsp;Send Mail</strong></td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">
		<table width="98%" border="0" align="center" cellpadding="4" cellspacing="0">
			 <tr>
			 	<td align="left"><b>To</b><span style="color:red">*</span></td>
				<td align="left"><b>:</b></td>
				<td align="left"><input name="emailToAddr" id="emailToAddr" type="text" class="input_normal" value="<c:out value="${custSuggDetails.billingEmail}"/>" />
				<span id="reqto" style="display:none;color:red;">Required</span>
				<span id="validto" style="display:none;color:red;">Invalid format</span>
				</td>
			</tr>
			<tr>
			 	<td align="left"><b>Cc</b></td>
				<td align="left"><b>:</b></td>
				<td align="left"><input type="text" name="emailCcAddr" id="cc" class="input_normal"/>
				<span id="validcc" style="display:none;color:red;">Invalid format</span>
				</td>
			</tr>
			<tr>
			 	<td align="left"><b>BCC</b></td>
				<td align="left"><b>:</b></td>
				<td align="left"><input type="text" name="emailBccAddr" id="bcc" class="input_normal"/>
				<span id="validbcc" style="display:none;color:red;">Invalid format</span>
				</td>
			</tr>
			<tr>
			 	<td align="left"><b>Subject</b><span style="color:red">*</span></td>
				<td align="left"><b>:</b></td>
				<td align="left"><input type="text" name="emailSubject" id="subject" value="Customer Feedback" class="input_normal">
				<span id="reqsubject" style="display:none;color:red;">Required</span>
				</td>
			</tr>
			<tr>
			 	<td align="left" valign="top"><b>Message</b><span style="color:red">*</span></td>
				<td align="left" valign="top"><b>:</b></td>
				<td align="left"><textarea name="emailContent" id="message" cols="45" rows="10" class="input_normal"><c:if test="${!empty custSuggDetails.custSuggestions}">Suggestion:<c:out value="${custSuggDetails.custSuggestions}"/></c:if><c:if test="${!empty custSuggDetails.custFeedback}">Offer:<c:out value="${custSuggDetails.custFeedback}"/></c:if>
				</textarea><span id="reqmessage" style="display:none;color:red;">Required</span></td>
			
			</tr>
			
		</table>
		<table width="99%">
			<tr>
			 	<td align="center"><input type="button" value="Send" class="button" onclick="fnCallSendMail();"/>&nbsp;&nbsp;<input type="button" name="btncancel" value="Cancel" id="btncancel" class="button" onclick="javascript:self.close();"/></td>
			</tr>
		</table>
      </td>
	</tr>
	<tr>
   <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png" width="100%">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
 </tr>
</table>


			
    

 




 




	


