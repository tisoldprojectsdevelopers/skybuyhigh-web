<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript">
	
	function fnCallViewOrderItemDetails(jsOrderItemId) {
		document.forms[0].action = "editOrder.do?method=viewOrderDetails&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	
	function fnCallEditOrderItemDetails(jsOrderItemId, jsSourceOfEdit) {
		document.forms[0].action = "editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemId+"&PlaceOrder="+jsSourceOfEdit;
		document.forms[0].submit();
	}
	
	function fnCallOrderItemDetails(jsOrderItemId, jsSourceOfEdit) {
		document.forms[0].action = "editOrder.do?method=viewOrderDetails&OrderItemId="+jsOrderItemId+"&PlaceOrder="+jsSourceOfEdit;
		document.forms[0].submit();
	}
	
	function fnCallChargeOrderItemDetails(jsOrderItemId) {
		document.forms[0].action = "chargeOrder.do?method=chargeOrder&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	
</script>

    <html:form action="customer/customersOrderDisplay">  
    	<tr><td>
     	<div class="contentcontainer">     		
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table">
			  <tbody>
			  	<tr>
			      <td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>
	</td>
			  
			  
			  
			  	</tr>
			 	<tr>
			  		<td colspan="3" valign="top" align="center" class="td">
			  			<table border="0" cellspacing="0" cellpadding="0" class="returnpolicy">
			  				<tr>
			   					 <td><h2><c:if test="${OwnerType eq 'vendor'}">Return Policy </c:if>
									<c:if test="${OwnerType eq 'airline'}">Restrictions/Validity/Cancellation</c:if></h2></td>
			  				</tr>
			  				<tr>
				    			<td >
				  					<bean:write name="ReturnPolicy" property="returnPolicy"/>
								</td>
			  				</tr>
			  				<tr>
			    				<td>
			    					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="customerService">
			    						<tr>
			    							<td colspan="4" align="left"><h3>Customer Service</h3></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable">Phone No</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable"><bean:write name="ReturnPolicy" property="custServicePhone"/></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable">Extn No</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable"><bean:write name="ReturnPolicy" property="custServicePhoneExtnNo"/></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable">Email</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable"><bean:write name="ReturnPolicy" property="custServiceEmailId"/></td>
			    						</tr>
			    					</table>
			    				</td>
			  				</tr>
						</table>
					</td>
			    </tr>
			 	<tr>
			 		<c:if test="${ItemStatus eq 'C' or ItemStatus eq 'O'}"> 
					    <td height="50" align="center" class="td" colspan="3">
					      	<input type="button" onclick="javascript:fnCallViewOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" value="OK" />
					    </td>
			      	</c:if>
			      	<c:if test="${ItemStatus ne 'C' and ItemStatus ne 'Q' and ItemStatus ne 'O'}">
			      		<c:if test="${Mode eq 'Edit'}"> 
						    <td height="50" align="center" class="td" colspan="3">
						      	<input type="button" onclick="javascript:fnCallEditOrderItemDetails('<c:out value="${OrderItemId}"/>','<c:out value="${SourceOfEdit}"/>');" class="button" value="OK" />
						    </td>
					    </c:if>
					    <c:if test="${Mode ne 'Edit'}"> 
						    <td height="50" align="center" class="td" colspan="3">
						      	<input type="button" onclick="javascript:fnCallOrderItemDetails('<c:out value="${OrderItemId}"/>','<c:out value="${SourceOfEdit}"/>');" class="button" value="OK" />
						    </td>
					    </c:if>
			      	</c:if>
			      	<c:if test="${ItemStatus eq 'Q'}"> 
					    <td height="50" align="center" class="td" colspan="3">
					      	<input type="button" onclick="javascript:fnCallChargeOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" value="OK" />
					    </td>
			      	</c:if>
			    </tr>
				<tr>
				  <td colspan="3">
					<div class="nav-footer">
					<div class="nav-footer-right"></div>
					<div class="nav-footer-left"></div>
					<div class="nav-footer-content"></div>			
					</div>
				</td>
			 	</tr>
			  </tbody>
		  </table>
     	</div>
     	</div>
     	</td></tr>
		<!-- end contentcontainer -->
    </html:form>
  
