<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">


	window.onload=function() {
		tablecollapse();
	}
	function fnCallSubmit(js_OrderItemId,IsUpdate) {
		var trackingDetails = document.forms[0].orderTrackingDetail.value;
		if(trackingDetails != "") {
			document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+js_OrderItemId+"&IsUpdate="+IsUpdate
			document.forms[0].submit();		
		}else {
			alert("Value must be Supplied");
		}
	}
	function fnCallViewAddressDetails(jsTransactionId) {
		document.forms[0].action = "viewTransactionDetails.do?method=displayTransactionDetails&TransactionId="+jsTransactionId;
		document.forms[0].submit();
	}
	function fnCallOrderItemDetails(jsOrderItemId, jsSourceOfEdit) {
		document.forms[0].action = "editOrder.do?method=viewOrderDetails&OrderItemId="+jsOrderItemId+"&PlaceOrder="+jsSourceOfEdit;
		document.forms[0].submit();
	}
	function fnCallEditOrderItemDetails(jsOrderItemId, jsSourceOfEdit) {
		document.forms[0].action = "editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemId+"&PlaceOrder="+jsSourceOfEdit;
		document.forms[0].submit();
	}
	
	function fnCallChargeOrderItemDetails(jsOrderItemId) {
		document.forms[0].action = "chargeOrder.do?method=chargeOrder&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	
	function fnCallViewOrderItemDetails(jsOrderItemId) {
		document.forms[0].action = "editOrder.do?method=viewOrderDetails&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	
</script>
<html:form action="/admin/displayReasonForFailure" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			<td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>

	</td>
			
			</tr>
			<logic:present name="ReasonForFailure">
				<logic:notEmpty name="ReasonForFailure">
					<tr class="tdbg">
					  <td colspan="3" >&nbsp;</td>
					</tr>
					<tr class="tdbg">
					  <td class="td" colspan="3" align="center"><table border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td align="left"><table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td	align="left" nowrap="nowrap" height="30" class="lable"> Order Item ID </td>
                              <td class="lable">:</td>
                              <td align="left" nowrap="nowrap" class="lable"><c:out value="${OrderItemId}"/>
                              </td>
                            </tr>
                            <tr>
                              <td align="left" nowrap="nowrap" height="30" class="lable"> Order Confirmation No </td>
                              <td class="lable">:</td>
                              <td nowrap="nowrap" class="lable" align="left"><c:out value="${custTransId}"/>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td><table border="0" cellpadding="0" cellspacing="0" class="border">
                            <tr>
                              <td height="30" colspan="3" class="tablehead" align="center"><h2>Transaction Details</h2></td>
                            </tr>
                            <tr>
                              <td width="100%" colspan="3" bgcolor="#CCCCCC" class="td"><table border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
                                  <tr>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Payment Transaction Reference ID </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Card Holder Name </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Credit Card No. </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Transaction Date </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Transaction Type </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Transaction Status </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Comments </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Action </td>
                                  </tr>
                                  <logic:iterate id="reasonForFailure" name="ReasonForFailure">
                                    <tr bgcolor="#FFFFFF">
                                      <td nowrap="nowrap" class="lable"><bean:write name="reasonForFailure" property="paymentTxnRefId"/>
                                      </td>
                                      <td nowrap="nowrap" class="lable" align="left"><bean:write name="reasonForFailure" property="creditCardHolderName"/>
                                      </td>
                                      <td nowrap="nowrap" class="lable"><bean:write name="reasonForFailure" property="maskedCreditCardNo"/>
                                      </td>
                                      <td nowrap="nowrap" align="left" class="lable"><bean:write name="reasonForFailure" property="transactionDate"/>
                                      </td>
                                      <td nowrap="nowrap" class="lable" align="left">
										  <c:if test="${reasonForFailure.transactionType eq 'PREAUTH'}"> Pre Authentication </c:if>
                                          <c:if test="${reasonForFailure.transactionType eq 'POSTAUTH'}"> Post Authentication </c:if>
										  <c:if test="${reasonForFailure.transactionType eq 'SALE'}"> Sale Completed </c:if>
										  <c:if test="${reasonForFailure.transactionType eq 'CREDIT'}"> Refund Completed </c:if>
                                      </td>
                                      <td nowrap="nowrap" class="lable" align="left"><c:if test="${reasonForFailure.transactionStatus eq 'TF'}"> Transaction Failed </c:if>
                                          <c:if test="${reasonForFailure.transactionStatus eq 'TS'}"> Transaction Success </c:if>
                                          <c:if test="${reasonForFailure.transactionStatus eq 'OR'}"> Order Rejected </c:if>
                                      </td>
                                      <td nowrap="nowrap" class="lable" align="left"><bean:write name="reasonForFailure" property="txnMsg"/>
                                      </td>
                                      <td nowrap="nowrap" class="lable" align="center"><a href="javascript:fnCallViewAddressDetails('<bean:write name="reasonForFailure" property="transactionId"/>')">View</a> </td>
                                    </tr>
                                  </logic:iterate>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
				      </td>
					</tr>
				</logic:notEmpty>
			</logic:present>
			<logic:empty name="ReasonForFailure">
				<tr>
					<td colspan="3" class="td" align="center" style="margin:30px;">
						<h4><font color="red"> No Transaction Details for this Order Item</font></h4>
					</td>
				</tr>
			</logic:empty>
			<tr>
				<c:if test="${ItemStatus eq 'Q'}">
					<td colspan="3" class="td" align="center">
						<input type="button" onclick="javascript:fnCallChargeOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" value="OK" />
					</td>
				</c:if>
				<c:if test="${ItemStatus ne 'Q' and ItemStatus ne 'C' and ItemStatus ne 'O'}">
					<c:if test="${Mode eq 'Edit'}">
						<td colspan="3" class="td" align="center">
							<input type="button" onclick="javascript:fnCallEditOrderItemDetails('<c:out value="${OrderItemId}"/>','<c:out value="${SourceOfEdit}"/>');" class="button" value="OK" />
						</td>
					</c:if>
					<c:if test="${Mode ne 'Edit'}">
						<td colspan="3" class="td" align="center">
							<input type="button" onclick="javascript:fnCallOrderItemDetails('<c:out value="${OrderItemId}"/>','<c:out value="${SourceOfEdit}"/>');" class="button" value="OK" />
						</td>
					</c:if>
				</c:if>
				<c:if test="${ItemStatus eq 'C' or ItemStatus eq 'O'}">
					<td colspan="3" class="td" align="center">
						<input type="button" onclick="javascript:fnCallViewOrderItemDetails('<c:out value="${OrderItemId}"/>');" class="button" value="OK" />
					</td>
				</c:if>
			</tr>
			<tr>
			 
	<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>


	</td>
			</tr>
		</tbody>
	</table>
</div>
</td></tr>
</html:form>