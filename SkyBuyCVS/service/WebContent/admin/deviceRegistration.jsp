<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function isEligibleForSubmit(deviceStatus,deviceAirId) {
	if(deviceStatus != 'N'){
		if(document.forms[0].airId.value == ''){
			document.forms[0].status.value = 'NA';
		}
	}
	if(deviceStatus == 'N'){
		var status = document.forms[0].status.value;
		if(document.forms[0].airId.value !=''){
			if(status == 'N'){
				alert('Change the device status to Allocate');
				return false;
			}
		}
		if(document.forms[0].airId.value ==''){
			if(status == 'AL'){
				alert('Please select Air Charter name');
				return false;
			}
		}
	}
	if(deviceStatus == "NA") {
		var airlineId=document.forms[0].airId.value;
		var status = document.forms[0].status.value;
		if(airlineId != "") {
			if(status == "NA") {
					alert("Change the device status to Allocate");
					return false;
			}
		}
	}
	if(deviceStatus == "AL") {
		var airlineId=document.forms[0].airId.value;
		var status = document.forms[0].status.value;
		if(deviceAirId != airlineId) { // While changing the Airline and setting status as deallocated
			if(status == "NA") {
					alert("Change the device status to Allocated");
					return false;
			}
		}
	}
	return true;
}
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}


function fnCallAddDevice(){
	var jsPassword = document.forms[0].devicePassword.value;
	var jsConfirmPassword = document.forms[0].confirmPassword.value;
	if(trim(jsConfirmPassword) == ''){
		alert('Please enter confirm password');
		return false;
	}else if(jsPassword!=jsConfirmPassword){
		alert('Device password and confirm password are not same');
		return false;
	}else{
		document.forms[0].action="addDeviceReg.do?method=addDeviceRegDetails";
		document.forms[0].submit();
	}
}
function fnCallEditDevice(status,airId){
	var jsPasswordChanged = document.forms[0].changeDevicePassword.value;
	var jsPassword = document.forms[0].newDevicePassword.value;
	var jsConfirmPassword = document.forms[0].newConfirmPassword.value;
	if(isEligibleForSubmit(status,airId)) {
		if(jsPasswordChanged == 'YES'){
			if(jsPassword == ''){
				alert('Please enter new device password');
				return false;
			}else if(jsConfirmPassword == ''){
				alert('Please enter new confirm password');
				return false;
			}else if(jsPassword!=jsConfirmPassword){
				alert('Device password and confirm password are not same');
				return false;
			}else{
				document.forms[0].devicePassword.value = jsPassword;
				document.forms[0].action="updateDeviceReg.do?method=updateDeviceDetails";
				document.forms[0].submit();
			}
		}else{
			document.forms[0].action="updateDeviceReg.do?method=updateDeviceDetails";
			document.forms[0].submit();
		}
	}
}

function fnCallHome() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();
}

function fnChange(){
		document.forms[0].changeDevicePassword.value = 'YES';
		document.getElementById("DeviceChangeLabel").style.display="none";
		document.getElementById("DeviceNoChangeLabel").style.display="inline";
		document.getElementById("newDevicePasswordTR").style.display="inline";
		document.forms[0].newDevicePassword.focus();
	}
	
	function fnNoChange(){
		document.forms[0].changeDevicePassword.value = 'NO';
		document.getElementById("DeviceChangeLabel").style.display="inline";
		document.getElementById("DeviceNoChangeLabel").style.display="none";
		document.getElementById("newDevicePasswordTR").style.display="none";
		
	}
function fnCallSearchDevice(jsDeviceId) {
	<c:if test="${!empty SourceOfEdit and SourceOfEdit eq 'View'}">
		document.forms[0].action="updateDeviceReg.do?method=viewDeviceDetails&deviceId="+jsDeviceId;
		document.forms[0].submit();
	</c:if>
	<c:if test="${empty SourceOfEdit or SourceOfEdit ne 'View'}">
		document.forms[0].action="searchDevice.do?method=searchDevice";
		document.forms[0].submit();
	</c:if>
}
</script>

<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from:</li>
				<li>Admin</li>
				<li>></li>		
				<logic:notEqual name="mode"  value="deviceEdit">
				<li>Add Device</li>
				</logic:notEqual>
				<logic:equal name="mode"  value="deviceEdit">
				<li>Edit/Search Device</li>
				</logic:equal>
				</ul>
			</div>			
		</div>
	</td>
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
    <html:javascript formName="deviceRegForm"/>
	<html:form action="admin/addDeviceReg" method="post" onsubmit="return validateDeviceRegForm(this);">
	<logic:present name="error" scope="request">
	<table align="center">
	<tr align="center">
	<td class="error">
		<c:out value="${error}"></c:out>
	</td>
	</tr>
	</table>
	</logic:present>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1" class="broder_top0">

      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable">Air Charter Name </td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
		<c:if test="${deviceDetails.status eq 'N' or deviceDetails.status eq 'NA' or deviceDetails.status eq 'AL' or deviceDetails.status eq null}"> 
			<html:select property="airId" styleClass="textarea2" tabindex="1">
			 <html:option value="">None</html:option>
			 <html:options collection="airCodeList" property="airId" labelProperty="airCode"></html:options>
            </html:select>
        </c:if>
        <c:if test="${deviceDetails.status ne 'N' and deviceDetails.status ne 'NA' and deviceDetails.status ne 'AL'  and deviceDetails.status ne null}"> 
			<html:hidden property="airId"/>
			<c:out value="${deviceDetails.airName}" />
			
			
		  </c:if>		
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Type</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
        <html:select property="deviceType" styleClass="textarea2" tabindex="1">
			 <html:option value="">--Select Device Type--</html:option>
			 <html:options collection="DeviceTypesList" property="deviceId" labelProperty="deviceName"></html:options>
            </html:select>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Serial No</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          	<html:text property="deviceSerialNo" styleClass="input" tabindex="3" maxlength="24"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Model</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <html:text property="deviceModel" styleClass="input" tabindex="4"  maxlength="24"/>
        </span></td>
      </tr>
      	<tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Id</span></td>
        <td align="left" class="lable">:</td>
        <td align="left">
	        <span class="catagory">
	        	<c:if test="${mode ne 'deviceEdit'}">
	        		<html:text property="deviceCode" styleClass="input" tabindex="5" maxlength="24"/>
	        	</c:if>
	        	<c:if test="${mode eq 'deviceEdit'}"> 
	        		<bean:write name="deviceRegForm" property="deviceCode" />
	        		<html:hidden property="deviceCode" />
	        	</c:if>
	        </span>
	    </td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Product No</span></td>
        <td align="left" class="lable">:</td>
        <td align="left">
        	<span class="catagory">
        		<html:text property="productNo" styleClass="input" tabindex="6" maxlength="24"/>
         	</span>
        </td>
      </tr>
      
      <tr class="tdbg">
       <c:if test="${mode ne 'deviceEdit'}">
       <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Password</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          	<html:password property="devicePassword" styleClass="input" tabindex="7" maxlength="24"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Confirm Password</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
        <input type="password" name="confirmPassword" value="" tabindex="8" maxlength="24"/>
         </span></td>
         </c:if>
        <c:if test="${mode eq 'deviceEdit'}">
        <input type="hidden" name="changeDevicePassword" value="NO"/>
		        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Password</span></td>
		        <td align="left" class="lable">:</td>
		        <td align="left"><span class="catagory">
		        <html:hidden property="devicePassword"/>
		        <c:out value="${maskedDP}"/>&nbsp;&nbsp;&nbsp;<span id="DeviceChangeLabel">&nbsp;&nbsp;<a href="javascript:fnChange();" class="bluelink">Change</a></span>
				<span id="DeviceNoChangeLabel" style="display:none;">&nbsp;<a href="javascript:fnNoChange();" class="bluelink">No&nbsp;Change</a></span>
		        </span></td>
		        <td align="left" nowrap="nowrap" class="lable" colspan="4">&nbsp;</td>
		        
		        
        </c:if>
        </tr>
        <tr class="tdbg" style="display:none;" id="newDevicePasswordTR">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">New Password</span></td>
		        <td align="left" class="lable">:</td>
		        <td align="left"><span class="catagory">
		        <input type="password" name="newDevicePassword" class="input" maxlength="24"/>
		        </span></td>
		        <td align="left" nowrap="nowrap" class="lable">Confirm Password</td>
		        <td align="left" class="lable">:</td>
		        <td align="left"><span class="catagory">
		        <input type="password" name="newConfirmPassword" class="input" maxlength="24"/>
				 </span></td>
      </tr>
      
      <c:if test="${mode eq 'deviceEdit'}">
      	 <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Status</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
        <c:if test="${deviceDetails.status eq 'A' or deviceDetails.status eq 'I'}"> 
          <html:select property="status" styleClass="textarea2" tabindex="1">
			 <html:option value="A">Active</html:option>
			 <html:option value="I">Inactive</html:option>
			  <html:option value="NA">Deallocate</html:option>	
		  </html:select>
		 </c:if>
		 
		  <c:if test="${deviceDetails.status eq 'N'}">
		  	 <html:select property="status" styleClass="textarea2" tabindex="1">
			 <html:option value="N">New</html:option>
			 <html:option value="AL">Allocate</html:option>
			 </html:select>
		</c:if>
         
           <c:if test="${deviceDetails.status eq 'AL' or deviceDetails.status eq 'NA'}">
			 <html:select property="status" styleClass="textarea2" tabindex="1">
			 <html:option value="AL">Allocate</html:option>
			 <html:option value="NA">Deallocate</html:option>
		 	 </html:select>
		   </c:if>
		</span></td>
		<td>&nbsp;</td>
        	<td>&nbsp;</td>
        	<td>&nbsp;</td>
        <!-- <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Id</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><c:out value="${deviceDetails.deviceCode}"></c:out>
        <html:hidden property="deviceCode"/></td>-->
      </tr>
      </c:if>
      
     
    <!-- 
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Mac Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="macAddr" styleClass="input" tabindex="5"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Processor Id</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="processorId" styleClass="input_180" tabindex="6"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Mac Description</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"> 
			<textarea name="macDesc" rows="6" onKeyUp="textLimit(this.form.macDesc,this.form.commentlen,128,'Mac Description');" class="textarea1" tabindex="7"><logic:present name="deviceDetails"><logic:notEmpty name="deviceDetails"><bean:write  name="deviceDetails" property="macDesc"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="128" class="wordcount"/></td>
        </tr>
       -->   
    </table>
      </tr>
      <tr>
         <td height="50" colspan="3" align="center"> <logic:present name="mode">
			 <logic:equal name="mode"  value="deviceAdd">	 
	 		 <html:button property="method" styleClass="button" 
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallAddDevice(); void 0" tabindex="9">Add</html:button> 
		  </logic:equal>
		</logic:present>    
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="deviceEdit">
	  			<input type="button" class="button" value="Save" onclick="javascript:if(document.forms[0].onsubmit())fnCallEditDevice('<c:out value="${deviceDetails.status}"/>','<c:out value="${deviceDetails.airId}"/>'); void 0" tabindex="9" />   
	  		</logic:equal>
		</logic:present>   
		
         <c:if test="${mode ne 'deviceEdit'}">
	        <html:button property="method" styleClass="button" 
			  onclick="javascript:fnCallHome();" tabindex="10" > Cancel </html:button>
		</c:if>
		<c:if test="${mode eq 'deviceEdit'}">
	        <input type="button" class="button" 
			  onclick="javascript:fnCallSearchDevice(<c:out value="${deviceDetails.deviceId}" />);" tabindex="10" value="Cancel"/>
		</c:if>
       	</td>
      </tr>
    </table>
	<html:hidden property="deviceId"/>
</html:form>

	</td>
    </tr>
  
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->

</div>
</td></tr>

