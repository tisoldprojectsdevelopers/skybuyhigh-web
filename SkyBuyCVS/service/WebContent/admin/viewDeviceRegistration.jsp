<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>

<script>
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

window.onload=function() {
	tablecollapse();
} 
    	
function fnCallAddDevice(){
	document.forms[0].action="addDeviceReg.do?method=addDeviceRegDetails";
	document.forms[0].submit();
}
function fnCallEditDevice(){
	document.forms[0].action="updateDeviceReg.do?method=updateDeviceDetails";
	document.forms[0].submit();
}

function fnCallSearchDevice() {
	document.forms[0].action="searchDevice.do?method=searchDevice";
	document.forms[0].submit();
}

function fnCallEdit(jsDeviceId) {
	document.forms[0].action="editDeviceReg.do?method=editDeviceDetails&deviceId="+jsDeviceId+"&SourceOfEdit=View";
	document.forms[0].submit();
}
</script>




<html:form action="admin/addDeviceReg" method="post" >
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
				<li>You navigated from:</li>
				<li>Admin</li>
				<li>></li>		
				<logic:notEqual name="mode"  value="deviceEdit">
				<li>Add Device</li>
				</logic:notEqual>
				<logic:equal name="mode"  value="deviceEdit">
				<li>Edit/Search Device</li>
				</logic:equal>
				</ul>
				</div>			
		</div>

	</td>
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">

	<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td  colspan="3"class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1" class="broder_top0">
			<tr class="tdbg">
				<td height="15" colspan="9"  align="right">
					<div align="right" class="editlink"><a href="javascript: void fnCallEdit('<bean:write name='deviceDetails' property='deviceId'/>')" title="Edit Device Info">Edit Device Info</a></div>
				</td>
			</tr>
	      <tr class="tdbg">
	        <td align="left" valign="top" nowrap="nowrap" class="lable">Air Charter Name</td>
	        <td align="left" valign="top" class="lable">:</td>
	        <td align="left" valign="top" nowrap="nowrap"><span class="catagory">
	          	 
			  <c:if test="${empty deviceDetails.airId or deviceDetails.airId eq '' }">
			  None
			  </c:if>  
			  <c:if test="${!empty deviceDetails.airId and deviceDetails.airId ne '' }">
			  <c:out value="${deviceDetails.airName}"/>
			  </c:if> 
	        </span></td>
	        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Device Type</span></td>
	        <td align="left" valign="top" class="lable">:</td>
	        <td align="left" valign="top"><span class="catagory">
	    		
		
		    <c:forEach items="${DeviceTypesList}" var="dType">
	                   <c:if test="${dType.deviceId eq deviceDetails.deviceType}" >
	                      <c:out value="${dType.deviceName}" />
	                   </c:if>
	        </c:forEach>
	          
	        </span></td>
	      </tr>
	      
      <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable">Air Charter Code</td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top"><span class="catagory">
          	 
		  <c:if test="${!empty deviceDetails.airId and deviceDetails.airId ne '' }">
		  	<c:out value="${deviceDetails.airId}"/>
		  </c:if>  
		 
        </span></td>
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Air Charter User Id</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top">
        <c:out value="${deviceDetails.airUserId}"/>
        </td>
      </tr>
       <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Device Serial No</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top" nowrap="nowrap"><span class="catagory">
          <bean:write  name="deviceDetails" property="deviceSerialNo"/>
        </span></td>
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Device Model</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top" nowrap="nowrap"><span class="catagory">
         <bean:write  name="deviceDetails" property="deviceModel"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <!--<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Is Active</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <c:if test="${deviceDetails.isActive eq 'Y'}">Yes</c:if>
         <c:if test="${deviceDetails.isActive eq 'N'}">No</c:if>
         
        </span></td> -->
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Device Status</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top"><span class="catagory">
          <c:if test="${deviceDetails.status eq 'A'}">Active</c:if>
          <c:if test="${deviceDetails.status eq 'I'}">Inactive</c:if>
          <c:if test="${deviceDetails.status eq 'N'}">New</c:if>
          <c:if test="${deviceDetails.status eq 'AL'}">Allocated</c:if>
          <c:if test="${deviceDetails.status eq 'NA'}">Not Allocated</c:if>
          
        </span></td>
		
		 <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Device Id</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left" valign="top" nowrap="nowrap"><span class="catagory">
         <bean:write  name="deviceDetails" property="deviceCode"/>
        </span></td>
      </tr>
     <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Product Key</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left"> <bean:write  name="deviceDetails" property="productKey"/>	</td>
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Product No</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td align="left"> <bean:write  name="deviceDetails" property="productNo"/>	</td>
     </tr>
     <tr class="tdbg">
     	<c:if test="${! empty deviceDetails.adminVersion and deviceDetails.adminVersion != ''}">
	        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Admin Version</span></td>
	        <td align="left" valign="top" class="lable">:</td>
	        <td align="left"> <bean:write  name="deviceDetails" property="adminVersion"/>	</td>
        </c:if>
        <c:if test="${!empty deviceDetails.catalogueVersion and deviceDetails.catalogueVersion != ''}">
	        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Catalogue Version</span></td>
	        <td align="left" valign="top" class="lable">:</td>
	        <td align="left"> <bean:write  name="deviceDetails" property="catalogueVersion"/>	</td>
        </c:if>
     </tr>
      
    </table>
      </tr>
	  <tr class="tdbg">
	  	   <td class="lable">
		   		<logic:present name="CatalogueDownloadDetails" scope="request">
					<logic:notEmpty name="CatalogueDownloadDetails" scope="request">
						Catalogue Downloaded Details
				   		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
							<thead class="tablehead">
								<tr>
									<td class="lable">
										Device Id
									</td>
									<td class="lable">
										Activity
									</td>
									<td class="lable">
										Air Charter Name
									</td>
									<td class="lable">
										Update Id
									</td>
									<td class="lable">
										Update Date
									</td>
								</tr>
							</thead>
		
							<tfoot>
							  <tr>
								<td colspan=5 bgcolor="#E6EEFB" align="right"  class="normaltext"></td>
							  </tr>
							</tfoot>
		
							<tbody>
								<logic:iterate id="catalogueDownloadDetails" name="CatalogueDownloadDetails">
									<tr>
										<td align="center" class="lable">
											<bean:write name="catalogueDownloadDetails" property="ctlgDownloadDeviceId" />
										</td>
										<td class="lable">
											<bean:write name="catalogueDownloadDetails" property="ctlgDownloadActivity" />
										</td>
										<td class="lable">
											<bean:write name="catalogueDownloadDetails" property="ctlgDownloadAirName" />
										</td>
										<td class="lable">
											<bean:write name="catalogueDownloadDetails" property="ctlgDownloadUpdateId" />
										</td>
										<td class="lable">
											<bean:write name="catalogueDownloadDetails" property="ctlgDownloadUpdateDate" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:notEmpty>
				</logic:present>
		   </td>
	  </tr>
	  <tr class="tdbg">
	  	   <td class="lable">
		   		<logic:present name="DeviceAllocateDetails" scope="request">
					<logic:notEmpty name="DeviceAllocateDetails" scope="request">
						Device Allocate/DeAllocate Details
				   		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
							<thead class="tablehead">
								<tr>
									<td class="lable">
										Device Id
									</td>
									<td class="lable">
										Activity
									</td>
									<td class="lable">
										Air Charter Name
									</td>
									<td class="lable">
										Update Id
									</td>
									<td class="lable">
										Update Date
									</td>
								</tr>
							</thead>
		
							<tfoot>
							  <tr>
								<td colspan=5 bgcolor="#E6EEFB" align="right" class="normaltext"></td>
							  </tr>
							</tfoot>
		
							<tbody>
								<logic:iterate id="deviceAllocateDetails" name="DeviceAllocateDetails">
									<tr>
										<td align="center" class="lable">
											<bean:write name="deviceAllocateDetails" property="deviceAllocateDeviceId" />
										</td>
										<td class="lable">
											<bean:write name="deviceAllocateDetails" property="deviceAllocateActivity" />
										</td>
										<td class="lable">
											<bean:write name="deviceAllocateDetails" property="deviceAllocateAirName" />
										</td>
										<td class="lable">
											<bean:write name="deviceAllocateDetails" property="deviceAllocateUpdateId" />
										</td>
										<td class="lable">
											<bean:write name="deviceAllocateDetails" property="deviceAllocateUpdateDate" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:notEmpty>
				</logic:present>
		   </td>
	  </tr>
	  <tr class="tdbg">
	  	   <td class="lable">
		   		<logic:present name="DeviceRegDetails" scope="request">
					<logic:notEmpty name="DeviceRegDetails" scope="request">
						Device Registration Detail
				   		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
							<thead class="tablehead">
								<tr>
									<td class="lable">
										Device Id
									</td>
									<td class="lable">
										Activity
									</td>
									<td class="lable">
										Air Charter Name
									</td>
									<td class="lable">
										Update Id
									</td>
									<td class="lable">
										Update Date
									</td>
								</tr>
							</thead>
		
							<tfoot>
							  <tr>
								<td colspan=5 bgcolor="#E6EEFB" align="right"  class="normaltext"></td>
							  </tr>
							</tfoot>
		
							<tbody>
								<logic:iterate id="deviceRegDetails" name="DeviceRegDetails">
									<tr>
										<td align="center" class="lable">
											<bean:write name="deviceRegDetails" property="deviceRegDeviceId" />
										</td>
										<td class="lable">
											<bean:write name="deviceRegDetails" property="deviceRegActivity" />
										</td>
										<td class="lable">
											<bean:write name="deviceRegDetails" property="deviceRegAirName" />
										</td>
										<td class="lable">
											<bean:write name="deviceRegDetails" property="deviceRegUpdateId" />
										</td>
										<td class="lable">
											<bean:write name="deviceRegDetails" property="deviceRegUpdateDate" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:notEmpty>
				</logic:present>
		   </td>
	  </tr>
	  
	  <tr class="tdbg">
	  	   <td class="lable">
		   		<logic:present name="OrderPlacedDetails" scope="request">
					<logic:notEmpty name="OrderPlacedDetails" scope="request">
						Order Placed Details
				   		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
							<thead class="tablehead">
								<tr>
									<td class="lable">
										Device Id
									</td>
									<td class="lable">
										Activity
									</td>
									<td class="lable">
										Air Charter Name
									</td>
									<td class="lable">
										Flight No
									</td>
									<td class="lable">
										Total Orders
									</td>
									<td class="lable">
										Update Id
									</td>
									<td class="lable">
										Update Date
									</td>
								</tr>
							</thead>
		
							<tfoot>
							  <tr>
								<td colspan=7 bgcolor="#E6EEFB" align="right"  class="normaltext"></td>
							  </tr>
							</tfoot>
		
							<tbody>
								<logic:iterate id="orderPlacedDetails" name="OrderPlacedDetails">
									<tr>
										<td align="center" class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceDeviceId" />
										</td>
										<td class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceActivity" />
										</td>
										<td class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceAirName" />
										</td>
										<td class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceFlightNo" />
										</td>
										<td align="right" class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceTotalOrders" />
										</td>
										<td class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceUpdateId" />
										</td>
										<td class="lable">
											<bean:write name="orderPlacedDetails" property="orderPlaceUpdateDate" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:notEmpty>
				</logic:present>
		   </td>
	  </tr>
	  <tr class="tdbg">
	  	   <td class="lable">
		   		<logic:present name="PackageDownloadDetails" scope="request">
					<logic:notEmpty name="PackageDownloadDetails" scope="request">
						Package Downloaded Details
				   		<table width="100%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
							<thead class="tablehead">
								<tr>
									<td class="lable">
										Device Id
									</td>
									<td class="lable">
										Activity
									</td>
									<td class="lable">
										Package Downloaded Type
									</td>
									<td class="lable">
										Admin Version
									</td>
									<td class="lable">
										Catalogue Version
									</td>
									<td class="lable">
										Downloaded Date
									</td>
								</tr>
							</thead>
		
							<tfoot>
							  <tr>
								<td colspan=6 bgcolor="#E6EEFB" align="right"  class="normaltext"></td>
							  </tr>
							</tfoot>
		
							<tbody>
								<logic:iterate id="packageDownloadDetails" name="PackageDownloadDetails">
									<tr>
										<td align="center" class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadDeviceId" />
										</td>
										<td class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadActivity" />
										</td>
										<td class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadType" />
										</td>
										<td class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadAdminVersion" />
										</td>
										<td class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadCatalogueVersion" />
										</td>
										<td class="lable">
											<bean:write name="packageDownloadDetails" property="packageDownloadDate" />
										</td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</logic:notEmpty>
				</logic:present>
		   </td>
	  </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 
	        <!--<html:link href="searchDevice.do?method=searchDevice" styleClass="button" style="text-decoration:none">OK</html:link>-->
	        <html:button property="method" styleClass="button" value="OK" onclick="javascript:fnCallSearchDevice();"/> 
	    </td>
       
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
  <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>


	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


<html:hidden property="deviceId"/>
</td></tr>
</html:form>
