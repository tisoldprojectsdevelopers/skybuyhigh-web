<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
			    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
			    <td width="100%" align="left" background="../images/top_nav_middlebg.png">
					<ul>
						<li>You navigated from :</li>
						<li>Admin</li>
						<li>></li>
						<li>Upload SkyBuy Catalogue<br></li>
					</ul>
				</td>
			    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
		    </tr>
			<tr>
				<td colspan="3" class="td">
					&nbsp;
				</td>
			</tr>
				<tr>
					<td colspan="3" class="td">
						<html:form action="admin/viewUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500">
									<tr>
					    				<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
						  			</tr>
						  		</table>
					  		</logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2> 
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="4"
											bgcolor="#FFFFFF" class="broder_top0">
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">
														<c:if test="${UploadType eq 'ECatalogue'}">Skybuy<sup>High</sup> e-catalogue </c:if>
														<c:if test="${UploadType eq 'WelcomePage'}">Skybuy<sup>High</sup> welcome page  </c:if> 
														<c:if test="${UploadType eq 'CheckList'}">Skybuy<sup>High</sup> check list  </c:if>
													</span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<c:if test="${UploadType eq 'ECatalogue'}">
													<td colspan="4" align="left">
														<c:if test="${CatalogueUploaded eq 'Y'}">
															E-Catalogue successfully uploaded
														</c:if>
														<c:if test="${CatalogueUploaded eq 'N'}">
															E-Catalogue upload is unsuccessful
														</c:if>
														<c:if test="${CatalogueUploaded eq 'NU'}">
															E-Catalogue not uploaded
														</c:if>
													</td>
												</c:if>
												<c:if test="${UploadType eq 'WelcomePage'}">
													<td colspan="4" align="left">
														<c:if test="${CatalogueUploaded eq 'Y'}">
															Welcome page successfully uploaded
														</c:if>
														<c:if test="${CatalogueUploaded eq 'N'}">
															Welcome page upload is unsuccessful
														</c:if>
														<c:if test="${CatalogueUploaded eq 'NU'}">
															Welcome page not uploaded
														</c:if>
													</td>
												</c:if>
												<c:if test="${UploadType eq 'CheckList'}">
													<td colspan="4" align="left">
														<c:if test="${CatalogueUploaded eq 'Y'}">
															Check list successfully uploaded
														</c:if>
														<c:if test="${CatalogueUploaded eq 'N'}">
															Check list upload is unsuccessful
														</c:if>
														<c:if test="${CatalogueUploaded eq 'NU'}">
															Check list not uploaded
														</c:if>
													</td>
												</c:if>
											</tr>
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">Reason For Upload  </span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left" valign="top">
													<bean:write name="uploadSkyBuyCatalogueForm" property="reasonForUpload"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="50" colspan="3" align="center">
										&nbsp;<html:button property="method" styleClass="button"
											onclick="javascript:fnCallHome();" tabindex="12">OK</html:button>
									</td>

								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
				    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
				    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
				    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
				</tr>
			</table>
		</div>
	</div>
</td></tr>



