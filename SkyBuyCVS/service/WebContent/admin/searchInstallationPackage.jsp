<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

	 function triggerEvent() {
		if(event.keyCode==13) {
		  fnCallSearch();  
		}           
	 }
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	function fnValidateVersion(jsVersion) {
		var regExpForVersion = new RegExp("^\d*[0-9]+(|.\d*[0-9]+|)*$"); 
		
		if(!regExpForVersion.test(jsVersion)) {
			alert('Enter valid version');
			document.forms[0].catalogueVersion.focus();
			return false;
		}else {
			return true;
		}
	}
	
	function fnIsEligibleForSubmit() {
		var isEligible=true;
	    var jsSearchBy = trim(document.forms[0].searchBy.value);
	    var jsSearchValue = trim(document.forms[0].searchValue.value);
	    
	    if(jsSearchBy == 'All'){
		 	document.forms[0].searchValue.value = '';
		 	return isEligible;
		 }else if(jsSearchBy == 'Schema') {
			if(jsSearchValue == '') {
				return isEligible;
			}else{
				isEligible = fnValidateVersion(jsSearchValue);
				return isEligible;
			}
		}else if(jsSearchBy == 'Username') {
			if(jsSearchValue == '') {
				return isEligible;
			}
		}else if(jsSearchBy == 'ClientCert') {
			if(jsSearchValue == '') {
				return isEligible;
			}
		}else if(jsSearchBy == 'Service') {
			if(jsSearchValue == '') {
				return isEligible;
			}else{
				isEligible = fnValidateVersion(jsSearchValue);
				return isEligible;
			}
		}else if(jsSearchBy == 'VirtualDesktop') {
			if(jsSearchValue == '') {
				return isEligible;
			}else{
				isEligible = fnValidateVersion(jsSearchValue);
				return isEligible;
			}
		}else if(jsSearchBy == 'iPhoneDesktop') {
				return isEligible;
		}
		else if(jsSearchBy == 'iPhonePersonalShopper') {
				return isEligible;
		}
		else if(jsSearchBy == 'iPhoneWelcomePage') {
				return isEligible;
		}
		
		else if(jsSearchBy == 'WelcomePage') {
				return isEligible;
		}
		else if(jsSearchBy == 'ECatalogue') {
				return isEligible;
		}
		else if(jsSearchBy == 'PersonalShopper') {
				return isEligible;
		}
		
	}
	function fnCallSearch() {
		if(fnIsEligibleForSubmit()){
			document.forms[0].action="initSearchInstallationPackage.do?method=searchInstallationPackage";
			document.forms[0].submit();
		}		
	}
	function fnCallEdit(jsPackageIdValue) {
		document.forms[0].action="editInstallationPackage.do?method=editInstallationPackage&PackageId="+jsPackageIdValue;
		document.forms[0].submit();
	}
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
	
</script>

<html:form action="/admin/initSearchInstallationPackage" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Admin</li>
					<li>></li>
					<li>Search Installation package</li>
				</ul>
			</div>			
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td>
        	<html:select property="searchBy" styleClass="textarea2">	
				<html:option value="All">All</html:option>	
				<html:options collection="MasterInstallPkgList" property="key" labelProperty="value"/>
	    	</html:select>	
	   	</td>
        <td class="lable">Version :</td>
        <td><html:text property="searchValue" styleClass="input" onkeydown="javascript:triggerEvent();"/></td>
        <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="UploadDetails" scope="request"> 
		 <logic:notEmpty name="UploadDetails">	
			<table width="780" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
		      <tr>
		        <td height="30" colspan="3" class="tablehead" align="center"><h2>Installation Package Information<br></h2></td>
		        </tr>
		      <tr>
		        <td><table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC" class="broder_top0">
		              <tr>
					  	<td height="32" nowrap="nowrap" class="table_header">Package Id</td>
		                <td height="32" nowrap="nowrap" class="table_header">Package Type</td>
		                <td class="table_header" nowrap="nowrap">Version</td>
		                <td class="table_header" nowrap="nowrap">Upload Reason</td>              
		                <td class="table_header" nowrap="nowrap">Update date</td>
						 <td class="table_header" nowrap="nowrap">Action</td> 
		              </tr>
		              <logic:iterate id="uploadDetails" name="UploadDetails">
		              <tr class="tdbg">
					    <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="uploadDetails" property="packageId"/></td>
		                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF">
           					<c:out value="${AllMasterInstallPkgList[uploadDetails.uploadType]}"/>
					    </td>
		                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="uploadDetails" property="version"/></td>
		                <td align="left" bgcolor="#FFFFFF"><bean:write name="uploadDetails" property="reasonForUpload"/></td>
		                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="uploadDetails" property="updatedDate"/></td>
		              	  <td class="action" nowrap="nowrap" style="width:125px;" align="center">
				            <c:if test="${uploadDetails.uploadType eq 'iPhoneDesktop' or uploadDetails.uploadType eq 'iPhonePersonalShopper' or  uploadDetails.uploadType eq 'WelcomePage' or uploadDetails.uploadType eq 'iPhoneWelcomePage' or uploadDetails.uploadType eq 'PersonalShopper'}">
				            	<a target="_blank" href='<bean:write name="uploadDetails" property="filePath"/>'  title="View iPhone Desktop Image">Preview</a>
				            </c:if>
				            <c:if test="${uploadDetails.uploadType ne 'iPhoneDesktop' and  uploadDetails.uploadType ne 'iPhonePersonalShopper' and uploadDetails.uploadType ne 'WelcomePage' and  uploadDetails.uploadType ne 'iPhoneWelcomePage' and  uploadDetails.uploadType ne 'PersonalShopper'}">
				            	<b>N/A</b>
				            </c:if>            
		              	</td>
		              </tr>
		             </logic:iterate>
		            </table></td>
		      </tr>     
		    </table>
		</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present></td>
    </tr>
  <logic:present name="UploadDetails" scope="request"> 
		 <logic:notEmpty name="UploadDetails">
  <tr class="tdbg">
  		<td height="50" colspan="3" align="center">  
	        <html:button property="method" onclick="javascript:fnCallHome();" value="OK" styleClass="button"></html:button>
	    </td>
  </tr>
  </logic:notEmpty>
  </logic:present>
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>

</td></tr>
</html:form>
