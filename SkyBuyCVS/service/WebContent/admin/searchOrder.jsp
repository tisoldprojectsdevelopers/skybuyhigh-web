<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript"><!--
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
	}else {
		return datevar;
	}
}

function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
		fnCallSearch();
	}           
}

function fnCallSearch(){
	
	if(isEligibleForSubmit()) {
		document.forms[0].action="searchOrder.do?method=searchOrder";
		document.forms[0].submit();			
	}
	
}

	function isEligibleForSubmit() {
	
		var searchby = trim(document.forms[0].searchBy.value);
		var searchvalue = trim(document.forms[0].searchValue.value);
		var fromDate = trim(document.forms[0].orderFromDate.value);
		var toDate = trim(document.forms[0].orderToDate.value);
		var nameFormat = new RegExp("^[A-Za-z ']*$");
		var OrderConfFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		var phoneFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		var orderItemIdFormat = new RegExp("^[0-9]*$");
		
		var today = new Date();
		
		var month=today.getMonth()+1;
		var date=today.getDate();
		
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();

		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDate == ""){
			alert("Please enter valid Order To Date ");
			document.forms[0].orderToDate.focus();
			return false;
		}else {
			var isToDate=isDate(toDate);
			if(!isToDate) {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
				return false;
			}else if(isToDate=="Advance" || isToDate=="Past") {
				if(isToDate == "Past") {
					alert("Enter the To Date as earlier as you entered");
					return false;
				}else if(isToDate == "Advance") {
					alert("Enter the To Date as past as you entered");
					return false;
				}
			}else {
				document.forms[0].orderToDate.value = dateFormats(toDate);
			}
			if(fromDate == "" && !validateDate(dateFormats(toDate),today)) {
				alert("To Date should be earlier or equal to Current Date");
				return false;
			}
		}
		
		if(!((toDate.substring(2,3) == "/" && toDate.substring(5,6) == "/") 
		   || (toDate.substring(2,3) == "-" && toDate.substring(5,6) == "-"))) {
			alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && !((fromDate.substring(2,3) == "/" && fromDate.substring(5,6) == "/") 
		   || (fromDate.substring(2,3) == "-" && fromDate.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && toDate != "") {
			
			isCorrectFromDate = isDate(fromDate);
			isCorrectToDate = isDate(toDate);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].orderFromDate.value = dateFormats(fromDate);
				document.forms[0].orderToDate.value = dateFormats(toDate);
				if(!validateDate(dateFormats(fromDate),dateFormats(toDate))) {
					alert("From Date should be earlier than To Date");
					return false;
				}else if(!validateDate(dateFormats(toDate),today)) {
					alert("To Date should be earlier or equal to current Date");
					return false;
				}/*else {
					return true;
				}*/
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
			 	return false;
			}
		}
		
		if(searchby != "ALL") {
			if(searchvalue != "") {
				document.forms[0].searchValue.value=trim(searchvalue);
				if(searchby == "CUSTTRNSID") {
					if(!OrderConfFormat.test(searchvalue)) {
						alert("You must enter a valid Order Confirmation No.");
					}else {
						return true;
					}
				}else if(searchby == "CUSTPHONE") {
					if(!phoneFormat.test(searchvalue)) {
						alert("You must enter a valid Phone Number");
					}else {
						return true;
					}
				}else if(searchby == "CUSTNAME") {
					if(!nameFormat.test(searchvalue)) {
						alert("You must enter a valid customer name");
					}else{
						return true;
					}
				}else if(searchby == "ORDERITEMID") {
					if(!orderItemIdFormat.test(searchvalue)) {
						alert("You must enter a valid order item id");
					}else{
						return true;
					}
				}else {
					return true;
				}
			}else {
				alert("You must provide the search value");
			}
		}else {
			document.forms[0].searchValue.value="";
			return true;
		}
		
		return false;
	}


	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	function fnCallEdit(jsOrderItemIdValue, jsSearchOrder) {
		document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemIdValue+"&PlaceOrder="+jsSearchOrder;
		document.forms[0].submit();
	}
	function fnCallView(jsOrderItemIdValue) {
		document.forms[0].action="editOrder.do?method=viewOrderDetails&OrderItemId="+jsOrderItemIdValue;
		document.forms[0].submit();
	}
	function fnCallCharge(js_OrderItemId) {
		document.forms[0].action="chargeOrder.do?method=chargeOrder&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	function fnCallReject(js_OrderItemId) {
		document.forms[0].action="initRejectOrder.do?method=initRejectOrder&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	function fnCallCancelOrder(jsOrderItemIdValue) {
		document.forms[0].action="editOrder.do?method=viewCancelOrderItem&OrderItemId="+jsOrderItemIdValue;
		document.forms[0].submit();
	}
	function fnProcessOrder(js_OrderItemId) {
		document.forms[0].action="processOrder.do?method=processOrder&OrderItemId="+js_OrderItemId+"&SourceOfProcess=SearchOrder";
		document.forms[0].submit();
	}
	function fnHome(){
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();		
	}

/********gtky search end *****/	
	
--></script>
<html:form action="admin/initSearchOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    
      <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>
	</td>
    
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap">			  
			  			 <html:select property="searchBy" styleClass="textarea2">	
							 <html:option value="ALL">All</html:option>							  		
	 		  				<html:option value="CUSTTRNSID">Order Confirmation No</html:option>
							<html:option value="ORDERITEMID">Order Item Id</html:option>
							<html:option value="CUSTNAME">Customer Name</html:option>	
							<html:option value="CUSTPHONE">Customer Phone No</html:option>	
							<html:option value="ITEMCODE">Item Code</html:option>	
							<html:option value="AIRNAME">Air Charter Name</html:option>	
							<html:option value="FLIGHTNO">Tail No</html:option>												
	               		 </html:select>
			          </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" maxlength="64" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Category :</td>
            <td nowrap="nowrap"><html:select property="category" styleClass="textarea2">
                <html:option value="ALL">All</html:option>				
                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>			
				<html:option value="20">Private Jet Jaunts</html:option>	
				
            </html:select></td>
			 <td nowrap="nowrap" class="lable">Status :</td>
            <td nowrap="nowrap"><html:select property="orderStatus" styleClass="textarea2">
                 <html:option value="ALL">All</html:option>
                 <html:option value="C">Completed</html:option>
				 <html:option value="F">Failed</html:option> 
				 <html:option value="R">Rejected</html:option>
				 <html:option value="P">Order to be placed with Vendor/Air Charter</html:option>
				 <html:option value="Q">To be charged after Vendor/Air Charter Approval</html:option>
				 <!--<html:option value="A">RMA Approved</html:option>
				 <html:option value="E">RMA Rejected</html:option>-->
				  <html:option value="O">Canceled</html:option>
				 
            </html:select></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
           	 <td nowrap="nowrap" class="lable">From Date: </td>
            <td nowrap="nowrap"><html:text property="orderFromDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('orderFromDate', false, 'mdy', '/');" /></td>
			<td nowrap="nowrap" class="lable">To Date: </td>
            <td nowrap="nowrap"><html:text property="orderToDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('orderToDate', false, 'mdy', '/');" /></td>	
            <td nowrap="nowrap">&nbsp;</td>
            
            <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center" valign="top">
	<logic:present  name="orderItemsInfo"> 
 	 <logic:notEmpty name="orderItemsInfo">	
	<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table border="0" width="100%" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
          <td height="32" align="center" nowrap="NOWRAP" class="table_header">Order Item ID</td>
          <td nowrap="NOWRAP" class="table_header">Order Confirmation No</td>
          <td nowrap="NOWRAP" class="table_header">Customer Name</td>  
		  <td class="table_header">Phone No</td>        
          <td class="table_header">Item Code</td>
		  <!--  <td nowrap="NOWRAP" class="table_header">Item Name </td>
          <td nowrap="NOWRAP" class="table_header">Brand Name</td>
		   <td nowrap="NOWRAP" class="table_header">Category</td>-->
		    <td class="table_header">Quantity</td>
		    <td class="table_header">Unit Price</td>
		    <td class="table_header">Amount</td>
          <td class="table_header">Status</td>
          <td class="table_header" nowrap="nowrap">Order Date</td>
		 <!-- <td  class="table_header" nowrap="nowrap">Airline Name</td>	-->
		   <td class="table_header" nowrap="nowrap">Action</td>			
          </tr>
		  <logic:iterate id="OrderItemsInfo" name="orderItemsInfo">
          <tr class="tdbg">
          	  <td align="center" nowrap="NOWRAP"><div class="lable"><a href="javascript: void fnCallView('<bean:write name='OrderItemsInfo' property='orderItemId'/>')"><bean:write name="OrderItemsInfo" property="orderItemId"/></a></div></td>
         	  <td align="center" nowrap="NOWRAP"><div class="lable"><bean:write name="OrderItemsInfo" property="custTransId"/></div></td>          	
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="OrderItemsInfo" property="custFirstName"/></div></td>      
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="OrderItemsInfo" property="custPhone"/></div></td>         			
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="OrderItemsInfo" property="prodCode"/></div></td>   
			  <!--  <td align="left"><div class="lable"><bean:write name="OrderItemsInfo" property="itemName"/></div></td>    
			  <td align="left" nowrap="NOWRAP"><div class="lable"><bean:write name="OrderItemsInfo" property="brandName"/></div></td>   
			  <td align="left" nowrap="NOWRAP"><div class="lable">
			  <logic:iterate id="category" name="Category"scope="application">
					<c:if test="${category.cateId eq OrderItemsInfo.cateId}">
					  <bean:write name="category" property="cateName" />
					</c:if>				
          		</logic:iterate>
				<c:if test="${OrderItemsInfo.cateId eq '20'}">
					  Private Jet Jaunts				</c:if>
			 </td> -->
			   <td align="right"><div class="lable"><bean:write name="OrderItemsInfo" property="qty"/></div></td>
			   <td align="right">
			   		<div class="lable">
			   			<fmt:formatNumber value="${OrderItemsInfo.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
			   		</div>
			   	</td>
			   <td align="right"><div class="lable"><bean:write name="OrderItemsInfo" property="payAmount"/></div></td>
			 <td align="left" nowrap="nowrap"><div class="lable">
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'P' && OrderItemsInfo.ownerType eq 'vendor'}">
			 	Order to be placed with Vendor
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'P' && OrderItemsInfo.ownerType eq 'airline'}">
			 	Order to be placed with Air Charter
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'C'}">
			 	Completed
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'F'}">
			 	Failed
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'Q' && OrderItemsInfo.ownerType eq 'vendor'}">
			 	To be charged after Vendor Approval
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'Q' && OrderItemsInfo.ownerType eq 'airline'}">
			 	To be charged after Air Charter Approval
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'R'}">
			 	Rejected
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'A'}">
			 	RMA Approved
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'E'}">
			 	RMA Rejected
			 </c:if>
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'O'}">
			 	Canceled
			 </c:if>
			</div></td>
			 <td align="center"><div class="lable"><bean:write name="OrderItemsInfo" property="orderCreatedDt"/></div></td>
			 <!--<td align="left"><div class="lable"><bean:write name="OrderItemsInfo" property="airName"/></div></td> --> 
			<c:if test="${OrderItemsInfo.orderItemStatus eq 'P'}">    
				<td nowrap="nowrap" class="lable" align="center"><div class="action">
						<a href="javascript: void fnCallEdit('<bean:write name='OrderItemsInfo' property='orderItemId'/>','SearchOrder')" class="editdelete" title="Edit Order details">Edit</a> |
						<a href="javascript: void fnProcessOrder('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title="Process Order">Process Order</a>
					</div>
				</td>
			</c:if>	
			<c:if test="${OrderItemsInfo.orderItemStatus eq 'F' || OrderItemsInfo.orderItemStatus eq 'R'}">    
			
				<td nowrap="nowrap" class="lable"><div class="action"><span class="lable">
				<a href="javascript: void fnCallEdit('<bean:write name='OrderItemsInfo' property='orderItemId'/>','SearchOrder')" class="editdelete" title="Edit Order details">Edit</a> |
				<a href="javascript: void fnCallView('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a></span></div></td>
			</c:if>	
			
			<c:if test="${(OrderItemsInfo.orderItemStatus eq 'A' or OrderItemsInfo.orderItemStatus eq 'E' or OrderItemsInfo.orderItemStatus eq 'O')}">    
			
				<td nowrap="nowrap" class="lable"><div class="action"><span class="lable">
				<a href="javascript: void fnCallView('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a></span></div></td>
			</c:if>		
			
			<c:if test="${OrderItemsInfo.orderItemStatus eq 'C' and OrderItemsInfo.shipmentStatus eq 'Y'}">    
			
				<td nowrap="nowrap" class="lable"><div class="action"><span class="lable">
				<a href="javascript: void fnCallView('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a></span></div></td>
			</c:if>
			<c:if test="${OrderItemsInfo.orderItemStatus eq 'C' and OrderItemsInfo.shipmentStatus ne 'Y'}">    
			
				<td nowrap="nowrap" class="lable"><div class="action"><span class="lable">
				<a href="javascript: void fnCallView('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a> | 
				<a href="javascript: void fnCallCancelOrder('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" Cancel Order">Cancel</a></span></div></td>
			</c:if>	
			 
			<c:if test="${OrderItemsInfo.orderItemStatus eq 'Q'}">    
				<td nowrap="nowrap" class="lable" align="center">
					<div class="action">
							<a href="javascript: void fnCallCharge('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title="Charge Order details">Charge</a> |
							<a href="javascript: void fnCallReject('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title="Reject Order details">Reject</a>
					</div>
				</td>
			</c:if>
			       	
           </tr>
		  </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="orderItemsInfo"> 
	 <logic:notEmpty name="orderItemsInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnHome();" styleClass="button">Cancel</html:button></td> 
    </tr>
	</logic:notEmpty>
	</logic:present> 
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
    
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
