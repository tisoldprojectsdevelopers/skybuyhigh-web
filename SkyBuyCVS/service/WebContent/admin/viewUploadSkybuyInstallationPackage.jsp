<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
			 <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Upload Installation Package</li>
				</ul>
			</div>			
		</div>

	</td>
		  
		  
		    </tr>
			<tr>
				<td colspan="3" class="td">
					&nbsp;
				</td>
			</tr>
				<tr>
					<td colspan="3" class="td" align="center">
						<html:form action="admin/viewUploadSkybuyCatalogue" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
								<table width="500">
									<tr>
					    				<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
						  			</tr>
						  		</table>
					  		</logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2> 
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="4"
											bgcolor="#FFFFFF" class="broder_top0">
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">SkyBuy<sup>High</sup> installation package  </span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left">
													<c:if test="${InstallationUploaded eq 'Y'}">
														<c:out value="${AllMasterInstallPkgList[UploadType]}"/> is successfully uploaded
													</c:if>
													<c:if test="${InstallationUploaded eq 'N'}">
														<font class="error">Upload is unsuccessful</font>
													</c:if>
												</td>
											</tr>
											<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and (uploadSkyBuyCatalogueForm.uploadType eq 'Catalogue'
											or uploadSkyBuyCatalogueForm.uploadType eq 'Admin' or uploadSkyBuyCatalogueForm.uploadType eq 'AutoUpdate' 
											or uploadSkyBuyCatalogueForm.uploadType eq 'Schema' or uploadSkyBuyCatalogueForm.uploadType eq 'Service')}">
												<tr class="tdbg">
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'Admin'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Admin package version  </span>
														</td>
													</c:if>
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'Catalogue'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Catalogue package version  </span>
														</td>
													</c:if>
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'AutoUpdate'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Auto Update package version  </span>
														</td>
													</c:if>
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'Schema'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Schema package version  </span>
														</td>
													</c:if>
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'Service'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Service package version  </span>
														</td>
													</c:if>
													<c:if test="${!empty uploadSkyBuyCatalogueForm.uploadType and uploadSkyBuyCatalogueForm.uploadType eq 'VirtualDesktop'}">
														<td align="left" valign="top" nowrap="nowrap" class="lable">
															<span class="boldtext">Virtual Desktop package version  </span>
														</td>
													</c:if>
													<td valign="top" class="lable">
														:
													</td>
													<td colspan="4" align="left">
														<bean:write name="uploadSkyBuyCatalogueForm" property="version"/>
													</td>
												</tr>
											</c:if>
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">Reason For Upload  </span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left">
													<bean:write name="uploadSkyBuyCatalogueForm" property="reasonForUpload"/>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="50" colspan="3" align="center">
										&nbsp;<html:button property="method" styleClass="button"
											onclick="javascript:fnCallHome();" tabindex="12">OK</html:button>
									</td>

								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>
				<tr>
		<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>


	</td>
				</tr>
			</table>
		</div>
	</div>
</td></tr>



