<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">
	function fnCallTransactionDetails(jsOrderItemId) {
		document.forms[0].action = "displayReasonForFailure.do?method=getReasonForFailure&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
</script>
<html:form action="/admin/displayReasonForFailure" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			 <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>

	</td>
			
			
			</tr>
			<tr>
			    <td colspan="3" class="td">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" class="td" align="center">

					<table width="750" border="0" align="center" cellpadding="0"
						cellspacing="0" class="border">
						<tr>
							<td height="30" colspan="3" class="tablehead" align="center">
								<h2>
									Credit Card Information
								</h2>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table width="100%" border="0" cellpadding="2" cellspacing="2"
									class="broder_top0">
									<tr class="tdbg">
										<td colspan="2" align="left" nowrap="nowrap" class="lable">
											<h4>
												Billing Details :
											</h4>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" nowrap="nowrap" class="lable">
											Customer First Name
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custFirstName" /> </span>
										</td>
										<td align="left" nowrap="nowrap" class="lable">
											Customer Last Name
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" nowrap="nowrap" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custLastName" /> </span>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" nowrap="nowrap" class="lable" valign="top">
											Address Line 1
										</td>
										<td align="left" class="lable" valign="top">
											:
										</td>
										<td align="left" class="catagory" valign="top">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custAddress1" /> </span>
										</td>
										<c:if
											test="${TransactionDetails.custAddress2 ne null && TransactionDetails.custAddress2 ne ''}">
											<td align="left" nowrap="nowrap" class="lable">
												Address Line 2
											</td>
											<td align="left" class="lable">
												:
											</td>
											<td align="left" class="catagory">
												<span class="catagory"> <bean:write
														name="TransactionDetails" property="custAddress2" /> </span>
											</td>
										</c:if>
									</tr>
									<tr class="tdbg">
										<td align="left" nowrap="nowrap" class="lable">
											<span class="boldtext">City </span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" nowrap="nowrap" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custCity" /> </span>
										</td>
										<td align="left" nowrap="nowrap" class="lable">
											<span class="boldtext"> State </span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" class="catagory">
											<bean:write name="TransactionDetails" property="custState" />
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" nowrap="nowrap" class="lable">
											<span class="boldtext"> Country</span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" nowrap="nowrap" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custCountry" /> </span>
										</td>
										<td align="left" nowrap="nowrap" class="lable">
											<span class="boldtext">Zip </span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custZip" /> </span>
										</td>
									</tr>
									<tr align="left" class="tdbg">
										<td colspan="9" valign="top" nowrap="nowrap" class="lable">
											<hr />
										</td>
									</tr>
									<tr class="tdbg">
										<td colspan="9" align="left" class="lable">
											<h4>
												Credit Card Details :
											</h4>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" class="lable">
											<span class="boldtext">Card Holder Name </span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="creditCardHolderName" /> </span>
										</td>
										<td align="left" class="lable">
											<span class="boldtext">Credit Card No</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="maskedCreditCardNo" /> </span>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" class="lable">
											<span class="boldtext">Credit Card Type</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory">
											<span class="catagory"> 
												<c:if test="${TransactionDetails.cardType eq 'VC'}">
									            	Visa Card
									            </c:if> <c:if test="${TransactionDetails.cardType eq 'MC'}">
									            	Master Card
									            </c:if> <c:if test="${TransactionDetails.cardType eq 'AC'}">
									            	American Express
									            </c:if> </span>
										</td>
										<td align="left" class="lable" nowrap="nowrap">
											<span class="boldtext">Expiry Date</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="expiryDate" /> </span>
										</td>
									</tr>
									<tr align="left" class="tdbg">
										<td colspan="9" valign="top" nowrap="nowrap" class="lable">
											<hr />
										</td>
									</tr>
									<tr class="tdbg">
										<td colspan="9" align="left" class="lable">
											<h4>
												Transaction Details :
											</h4>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" class="lable" nowrap="nowrap"> 
											<span class="boldtext">Order Confirmation No </span>
										</td>
										<td align="left" class="lable">
											:
										</td>
										<td align="left" class="catagory" nowrap="nowrap">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="custTransId" /> </span>
										</td>
										<c:if test="${TransactionDetails.paymentTxnRefId ne null and TransactionDetails.paymentTxnRefId ne ''}">
											<td align="left" class="lable" nowrap="nowrap">
												<span class="boldtext">Payment Transaction Reference Id</span>
											</td>
											<td width="10" align="left" class="lable">
												:
											</td>
											<td width="50%" align="left" class="catagory" nowrap="nowrap">
												<span class="catagory"> <bean:write
														name="TransactionDetails" property="paymentTxnRefId" /> </span>
											</td>
										</c:if>
									</tr>
									<tr class="tdbg">
										<td align="left" class="lable" nowrap="nowrap">
											<span class="boldtext">Transaction Date</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory" nowrap="nowrap">
											<span class="catagory"> <bean:write
													name="TransactionDetails" property="transactionDate" /> </span>
										</td>
										<td align="left" class="lable" nowrap="nowrap">
											<span class="boldtext" nowrap="nowrap">Transaction Type</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory" nowrap="nowrap">
											<span class="catagory"> 
												<c:if test="${TransactionDetails.transactionType eq 'PREAUTH'}">
													Pre Authentication
												</c:if>
												<c:if test="${TransactionDetails.transactionType eq 'POSTAUTH'}">
													Post Authentication
												</c:if>
												<c:if test="${TransactionDetails.transactionType eq 'SALE'}">
													Sale Completed
												</c:if>
												<c:if test="${TransactionDetails.transactionType eq 'CREDIT'}">
													Refund Completed
												</c:if>
											</span>
										</td>
									</tr>
									<tr class="tdbg">
										<td align="left" class="lable" nowrap="nowrap">
											<span class="boldtext">Transaction Status</span>
										</td>
										<td width="10" align="left" class="lable">
											:
										</td>
										<td width="50%" align="left" class="catagory" nowrap="nowrap">
											<span class="catagory"> 
												<c:if test="${TransactionDetails.transactionStatus eq 'TF'}"> Transaction Failed </c:if>
	                                          	<c:if test="${TransactionDetails.transactionStatus eq 'TS'}"> Transaction Success </c:if>
	                                          	<c:if test="${TransactionDetails.transactionStatus eq 'OR'}"> Order Rejected </c:if>
                                         	</span>
										</td>
										<c:if test="${TransactionDetails.txnMsg ne null and TransactionDetails.txnMsg ne ''}">
											<td align="left" class="lable" nowrap="nowrap">
												<span class="boldtext">Comments</span>
											</td>
											<td width="10" align="left" class="lable">
												:
											</td>
											<td width="50%" align="left" class="catagory">
												<span class="catagory"> 
													<bean:write
														name="TransactionDetails" property="txnMsg" />
												</span>
											</td>
										</c:if>
									</tr>
									<tr align="left" class="tdbg">
										<td colspan="9" valign="top" nowrap="nowrap" class="lable">
											<hr />
										</td>
									</tr>
									<tr class="tdbg">
										<td colspan="9" align="center">
											<input type="button" onclick="javascript:fnCallTransactionDetails('<c:out value="${TransactionDetails.orderItemId}" />');" class="button" value="OK" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
			td colspan="3">
			<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>   
				</tr>
		</tbody>
	</table>
</div>
</div>
</td></tr>
</html:form>