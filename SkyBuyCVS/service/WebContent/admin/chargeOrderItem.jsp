<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />


<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}


/****Offer Description B****/


	function displayReturnPolicy(orderItemId,ownerType,jsItemStatus) {
		document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType+"&ItemStatus="+jsItemStatus;
		document.forms[0].submit();
	}
	
	function fnCallProcessPayment(js_OrderItemId){
		document.forms[0].action="processPayment.do?method=processPayment&OrderItemId="+js_OrderItemId;
		document.forms[0].submit();
	}
	function fnPaymentTxnRefDetails(jsOrderItemId,jsItemStatus) {
		document.forms[0].action="displayReasonForFailure.do?method=getReasonForFailure&OrderItemId="+jsOrderItemId+"&ItemStatus="+jsItemStatus;
		document.forms[0].submit();
	}
	function fnGetCustTrackingDetails(jsOrderItemId,jsItemStatus,jsViewOrder) {
		document.forms[0].action="orderTrackingDetails.do?method=getOrderTrackingDetails&OrderItemId="+jsOrderItemId+"&ItemStatus="+jsItemStatus+"&ViewOrder="+jsViewOrder;
		document.forms[0].submit();
	}
	function fnCallSearchOrderDetails() {
		document.forms[0].action="searchOrder.do?method=searchOrder";
		document.forms[0].submit();
	}

</script>




<html:form action="admin/editOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
       <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>
	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	
	<table width="850" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
  		<tr class="tdbg">
  			<td align="center" style="margin:10px;">
  				<c:if test="${OrderCompleted ne null and OrderCompleted ne ''}">
  					<font color="#FF0000"> <c:out value="${OrderCompleted}" /> </font>
  				</c:if>
  			</td>
  		</tr>
	  	<tr>
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
           		<td colspan="3" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
            	<td colspan="3" align="left" nowrap="nowrap" class="lable">
            		<a href="javascript:fnCallProcessPayment('<bean:write name="chargeOrderItem" property="orderItemId"/>')">Charge Customer Credit Card</a> | 
					<a href="javascript:fnPaymentTxnRefDetails('<bean:write name="chargeOrderItem" property="orderItemId"/>','<bean:write  name="chargeOrderItem"  property="orderItemStatus" />')">Payment Transaction Details</a> | 
					<a href="javascript:fnGetCustTrackingDetails('<bean:write  name="chargeOrderItem"  property="orderItemId" />','<bean:write  name="chargeOrderItem"  property="orderItemStatus" />','CHARGE')">Order Tracking Details</a>            		 
            	</td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">        
			<bean:write  name="chargeOrderItem" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
			
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable">Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custFirstName" />
             </span></td>
            <td align="left" nowrap="nowrap" class="lable">Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custLastName" />
              </span></td>
			
          </tr>
          <tr class="tdbg">
		   <td align="left" nowrap="nowrap" class="lable"> Address Line 1</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				 <bean:write  name="chargeOrderItem" property="custAddress1" /></span></td>
          	<c:if test="${chargeOrderItem.custAddress2 ne null && chargeOrderItem.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable"> Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="chargeOrderItem" property="custAddress2" />		
	        	</span></td>
			</c:if>
			
          </tr>
         <tr class="tdbg">
		 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
				  <bean:write  name="chargeOrderItem" property="custCity" />
		    </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				 <logic:iterate id="states" name="StateList" scope="application">
						<c:if test="${states.stateCode eq chargeOrderItem.custState}">
							<bean:write name="states" property="stateName" />
						</c:if>
					</logic:iterate>
				
			
             <!-- <bean:write  name="chargeOrderItem" property="custState" />-->
            </span></td>
		 </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
				<bean:write  name="chargeOrderItem" property="custCountry" />
		    </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="chargeOrderItem" property="custZip" />
			</span></td>
			
			
          </tr>
		   <tr class="tbdg">
		   <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
        		  <bean:write  name="chargeOrderItem" property="custPhone" />
		    </span></td>
		   	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory" nowrap="nowrap"><span class="catagory">
				 <bean:write  name="chargeOrderItem" property="custEmail" />
		    </span></td>
		    
		   </tr>
		    <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem" property="custShipFirstName" />
             </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <bean:write  name="chargeOrderItem" property="custShipLastName" />
            </span></td>
			
			
          </tr>
          <tr class="tdbg">
		  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            	<bean:write  name="chargeOrderItem" property="custShipAddress1" />
            </span></td>
           <c:if test="${chargeOrderItem.custShipAddress2 ne null && chargeOrderItem.custShipAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	            	<bean:write  name="chargeOrderItem" property="custShipAddress2" />
	            </span></td>
			</c:if>
			
          </tr>
         <tr class="tdbg">
		 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custShipCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
               <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq chargeOrderItem.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              
              <!--  <bean:write  name="chargeOrderItem" property="custShipState" />-->
            </span></td>
		 </tr>
		
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <bean:write  name="chargeOrderItem" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
               <bean:write  name="chargeOrderItem" property="custShipZip" />
            </span></td>
			
          </tr>
		  <tr class="tdbg">
		  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory" nowrap="nowrap"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custShipPhone" />
             </span></td>
			 
			 <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory" nowrap="nowrap"><span class="catagory">
                <bean:write  name="chargeOrderItem" property="custShipEmail" />
             </span></td>
		  </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="airName"/>
            </span></td>
            <c:if test="${chargeOrderItem.flightNo ne null}">
            	<td align="left" nowrap="nowrap" class="lable">Tail No </td>
            	<td align="left" class="lable">:</td>
            </c:if>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="flightNo" />
            </span></td>
			
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="chargeOrderItem"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="chargeOrderItem"  property="orderItemId"/>','<bean:write  name="chargeOrderItem"  property="ownerType"/>','<bean:write  name="chargeOrderItem"  property="orderItemStatus"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
		  
		  
		  
		  <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Credit Card Details :</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Card Holder Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="chargeOrderItem"  property="custFirstName"/>
            </span></td>
            <td align="left" class="lable"><span class="boldtext">Credit Card Type</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
            <c:if test="${chargeOrderItem.cardType eq 'VC'}">
            	Visa Card
            </c:if>
            <c:if test="${chargeOrderItem.cardType eq 'MC'}">
            	Master Card
            </c:if>
            <c:if test="${chargeOrderItem.cardType eq 'AC'}">
            	American Express
            </c:if>
        	</span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Credit Card No</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
	          <bean:write  name="chargeOrderItem"  property="maskCCNo"/>
	        </span></td>
		 	<td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Exp Date</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
	          <bean:write  name="chargeOrderItem"  property="expDt"/>
	        </span></td>
		  </tr>
		  <tr class="tdbg">
		 
		
		<td align="left" class="lable" nowrap="nowrap">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
		  </tr>
          
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="9" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="orderId"/>
               </span></td>
			   
			   <td colspan="3" rowspan="10" align="left" class="lable"><div align="center">
			   
			   <c:if test="${chargeOrderItem.isSpecialProd ne 'Y' }">
			    <img src="editOrder.do?method=renderImage&orderItemId=<c:out value='${chargeOrderItem.orderItemId}' />" width="200" height="200" />
			   </c:if>
			   </div></td>
			   </tr>
			   <tr class="tdbg">
			    <td align="left" class="lable">Order Item Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="orderItemId"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq chargeOrderItem.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${chargeOrderItem.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="chargeOrderItem"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="chargeOrderItem"  property="itemName"/>
               </span></td>
               
                <c:if test="${chargeOrderItem.ownerType eq 'airline'}">
				  <tr class="tdbg">
				  <c:if test="${chargeOrderItem.travelDate ne null}">
					   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Travel Date </span></td>
					   <td align="left" class="lable">:</td>
				  </c:if>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="chargeOrderItem" property="travelDate" />
	               </span></td>
				   </tr>
			   </c:if>
			   
			   </tr>
			    <c:if test="${chargeOrderItem.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="chargeOrderItem"  property="brandName" />
				   </span></td>
				   </tr>
			   </c:if>
			    <c:if test="${chargeOrderItem.color ne '' }" >
			 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Color</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="chargeOrderItem" property="color" />
	               </span></td>
			   </tr>
			   </c:if>
			   <c:if test="${chargeOrderItem.size ne '' }" >
			    <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Size</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                    <bean:write  name="chargeOrderItem" property="size" />
	               </span></td>
			   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory" nowrap="nowrap"><span class="catagory">
			     <c:if test="${chargeOrderItem.orderItemStatus eq 'P' && chargeOrderItem.ownerType eq 'vendor'}">Order to be placed with Vendor </c:if>
				 <c:if test="${chargeOrderItem.orderItemStatus eq 'P' && chargeOrderItem.ownerType eq 'airline'}">Order to be placed with Air Charter</c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'C'}"> Completed </c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'F'}"> Failed </c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'R'}"> Rejected </c:if>
                 <c:if test="${chargeOrderItem.orderItemStatus eq 'Q' && chargeOrderItem.ownerType eq 'vendor'}"> To be charged after Vendor approval</c:if>
				 <c:if test="${chargeOrderItem.orderItemStatus eq 'Q' && chargeOrderItem.ownerType eq 'airline'}"> To be charged after Air Charter approval</c:if>
                 
                 <!--    <bean:write  name="chargeOrderItem"  property="orderItemStatus" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Quantity</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                   <bean:write  name="chargeOrderItem" property="qty" />
                </span></td>
			   </tr>		
			 
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${chargeOrderItem.ownerType eq 'vendor'}">		   
			   Retail Price
			   </c:if>
			   <c:if test="${chargeOrderItem.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			    <fmt:formatNumber value="${chargeOrderItem.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               </span></td>			  
			   </tr>
			   
			    <tr class="tdbg">
			
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${chargeOrderItem.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               </span></td>
			  
			   </tr>
      </table>
		</td>
      </tr>
      <tr>
        <td height="30" colspan="3" align="center"> 
			 <input type="button" class="bigbutton" value="Process Payment" onclick="javascript:fnCallProcessPayment('<bean:write name="chargeOrderItem" property="orderItemId"/>')" />  <html:button property="method" onclick="javascript:fnCallSearchOrderDetails();" styleClass="button">Cancel</html:button>
			
		  </td>
       </tr>
       <tr>
       		<td colspan="3" valign="top" align="center">
			<table width="250px" border="0" cellspacing="0" cellpadding="0" height="32px">
                <tr>
                  <td align="left">(Charge Customer Credit Card) </td>
                  
                </tr>
              </table></td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</div>
</td></tr>
</html:form>
