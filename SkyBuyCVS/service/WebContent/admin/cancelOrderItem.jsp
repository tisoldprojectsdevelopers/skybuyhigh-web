<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">
	
	window.onload=function() {
		tablecollapse();
	}
	function displayOrderTrackingDetails() {
		var trackValue = document.getElementById("TrackingDetails");
		if(trackValue.style.display == "none") {
			document.getElementById("TrackingDetails").style.display="inline";
		}
	}
	function fnCallSubmit(jsOrderItemId) {
	//alert(jsOrderItemId);
		var cancelReason = document.forms[0].cancelReason.value;
		if(cancelReason != "") {
			document.forms[0].action="processPayment.do?method=cancelOrder&orderItemId="+jsOrderItemId;
			document.forms[0].submit();		
		}else {
			alert("Enter Reason for Cancellation.");
		}
	}
	function fnCallSearchOrder() {
		document.forms[0].action="searchOrder.do?method=searchOrder";
		document.forms[0].submit();
	}
	

</script>

<html:form action="/admin/orderTrackingDetails" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			<td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit Order Details</li>
				</ul>
			</div>			
		</div>
	</td>
	
			
			
			
			
			</tr>
			<tr class="tdbg">
				<td class="td" colspan="3" align="center">
					<table  border="0" cellpadding="0" cellspacing="0" style="margin:10px" class="border">
						<tr>
				        	<td height="30" colspan="3" class="tablehead" align="center"><h2>Canceled Order Item Details</h2></td>
				        </tr>
				        <tr>
				        	<td colspan="3" class="td" >
				        		<table border="0" cellpadding="0" cellspacing="0" style="margin:10px">
									<tr>
										<td	class="lable" align="left" nowrap="nowrap" valign="top">
											Order Item Id
										</td>
										<td class="lable" width="15%" valign="top">
											<b>:</b>
										</td>
										<td align="left">
											<c:out value="${orderItemId}"/>
										</td>
									</tr>
									<tr>
										<td	class="lable" align="left" nowrap="nowrap" valign="top">
											Enter Reason for Cancellation
										</td>
										<td class="lable" width="15%" valign="top">
											<b>:</b>
										</td>
										<td>
											<textarea rows="6" cols="45" name="cancelReason" tabindex="1"></textarea>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" class="td" >
								<input type="button" value="Submit" style="margin:0 0 10px 15px;" onclick="javascript:fnCallSubmit('<c:out value="${orderItemId}"/>');" class="button" tabindex="3" />
								<html:button property="method" onclick="javascript:fnCallSearchOrder();" styleClass="button" tabindex="4" style="margin:0 0 10px 10px;">Cancel</html:button>
						  </td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
			<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
			
				  
			  	</tr>
		</tbody>
	</table>
</div>
</div>
</td></tr>
</html:form>
</body>