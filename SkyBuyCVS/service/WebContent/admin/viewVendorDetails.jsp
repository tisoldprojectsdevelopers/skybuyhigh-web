<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>

<script type="text/javascript">
	window.onload=function() {
		tablecollapse();
	}
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

function fnCallEdit(jsVendIdValue) {
		document.forms[0].action="editVendor.do?method=EditVendorDetails&VendId="+jsVendIdValue+"&SourceOfEdit=View";
		document.forms[0].submit();
	}


function fnCallSubmit(){
	document.forms[0].action="updateVendor.do?method=UpdateVendorDetails";
	document.forms[0].submit();
}
function getCatalogue(link, windowname){
		window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');
}
</script>




<html:form action="admin/editVendor" method="post" >
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
			<!--Admin -->
		<logic:present name="adminLoginInfo" scope="session">	
		<c:if test="${adminLoginInfo.userType eq 'admin' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
		</logic:present>
		<logic:present name="adminLoginInfo" scope="session">	
		<c:if test="${adminLoginInfo.userType eq 'admin' && mode eq 'Add'}">
		<ul>
		<li>You navigated from:</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Vendor</li>
		</ul>
		</c:if>
		</logic:present>		
			</div>			
		</div>

	</td>
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" class="td" align="center">
    
    <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Vendor Information</h2></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
            
             <tr class="tdbg">
                 <td align="right" valign="top"  colspan="6"><span class="editlink"> <a href="javascript: void fnCallEdit('<bean:write name='vendorDetails' property='vendId'/>')" title="Edit Vendor Info">Edit Vendor Info</a></span></td>             
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">Vendor Name</td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top" style="word-wrap:break-word;" ><span class="catagory">
                <bean:write  name="vendorDetails"  property="vendName"/>
              </span></td>
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable">&nbsp;</td>
                  <td width="10" align="center" valign="top" class="lable">&nbsp;</td>
                  <td width="250" align="left" valign="top">&nbsp;</td>             
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Vendor Type</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <c:if test="${vendorDetails.vendType eq 'B'}"> Basic </c:if>
                <c:if test="${vendorDetails.vendType eq 'P'}"> Premium </c:if>
              </span></td>
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Charge Type </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <c:if test="${vendorDetails.chargeType eq 'FA'}"> Automated </c:if>
                <c:if test="${vendorDetails.chargeType eq 'SA'}"> Semi Automated </c:if>
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Contact Name </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top" style="word-wrap:break-word;" ><span class="catagory">
                <bean:write  name="vendorDetails" property="vendContactName" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Status</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <logic:notEmpty name="vendorDetails">
                  <logic:equal name="vendorDetails" property="vendStatus" value="I"> Inactive </logic:equal>
                  <logic:equal name="vendorDetails" property="vendStatus" value="A"> Active </logic:equal>
                </logic:notEmpty>
              </span> </td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Company Address</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top" style="word-wrap:break-word;" ><span class="catagory">
                <bean:write  name="vendorDetails" property="vendAddr1" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Additional Address</span></td>
              <td width="10" align="center" valign="top" class="lable"  >:</td>
              <td width="250" align="left" valign="top" style="word-wrap:break-word" ><span class="catagory">
                <bean:write  name="vendorDetails" property="vendAddr2" />
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top" style="word-wrap:break-word;" ><span class="catagory">
                <bean:write  name="vendorDetails" property="vendCity" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <logic:iterate id="states" name="StateList" scope="application">
                  <c:if
				test="${states.stateCode eq vendorDetails.vendState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendCountry" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendZip" />
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">Phone</td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap"> <span class="catagory">
                      <bean:write  name="vendorDetails" property="vendPhone1" />
                    </span></td>
					<c:if test="${!empty vendorDetails.vendPhoneExt1 && vendorDetails.vendPhoneExt1 ne ''}">
                    <td class="lable">Ext :</td>
                    <td nowrap="nowrap"><span class="catagory">
                      <bean:write  name="vendorDetails" property="vendPhoneExt1" />
                    </span></td>
					</c:if>
                  </tr>
              </table></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">Mobile</td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendMobile1" />
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Fax</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendFax" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Email</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendEmail" />
              </span></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">PO Percentage</td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="poPct" />
              </span></td>
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">&nbsp;</td>
              <td width="10" align="center" valign="top" class="lable">&nbsp;</td>
              <td width="250" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
            <c:if test="${!empty vendorDetails.aboutMePath and vendorDetails.aboutMePath ne ''}">
            <tr align="left" valign="middle" class="tdbg">
              <td height="23" colspan="6" nowrap="nowrap" bgcolor="#EFEFEF" class="lable">Uploaded Vendor Info </td>
            </tr>
            
             <tr class="tdbg">
              <td width="150" align="left" nowrap="NOWRAP" class="lable"><span class="boldtext">About Me</span></td>
              <td width="10" align="center" class="lable">:</td>
              <td align="left" colspan="6" nowrap="nowrap">
		          <a class="clickhere" href="javascript:getCatalogue('../previewAboutMe.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue
	          </td>
            </tr>
            </c:if>
            <tr align="left" valign="middle" class="tdbg">
              <td height="23" colspan="6" nowrap="nowrap" bgcolor="#EFEFEF" class="lable">Second Person Contact Details </td>
            </tr>
            <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Contact Name </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendContactName2" />
              </span></td>
              <td width="150" align="left" valign="top"  nowrap="NOWRAP" class="lable"><span class="boldtext">Email </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendEmail2" />
              </span><br /></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">Phone</td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td nowrap="nowrap"><span class="catagory">
                      <bean:write  name="vendorDetails" property="vendPhone2" />
                    </span><br /></td>
					<c:if test="${!empty vendorDetails.vendPhoneExt2 && vendorDetails.vendPhoneExt2 ne ''}">
                    <td class="lable">Ext :</td>
                    <td nowrap="nowrap"><span class="catagory">
                      <bean:write  name="vendorDetails" property="vendPhoneExt2" />
                    </span><br /></td>
					</c:if>
                  </tr>
              </table></td>
              <td width="150" align="left" valign="top"  nowrap="NOWRAP" class="lable"><span class="boldtext">Mobile </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="vendMobile2" />
              </span><br /></td>
            </tr>
            <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
            <tr align="left" valign="middle" class="tdbg">
              <td height="23" colspan="6" nowrap="NOWRAP" bgcolor="#EFEFEF" class="lable">Customer Service </td>
            </tr>
            <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="nowrap" class="lable">Phone </td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><span class="catagory">
                      <bean:write  name="vendorDetails" property="custServicePhoneno" />
                    </span><br /></td>
					<c:if test="${!empty vendorDetails.custServicePhonenoExt && vendorDetails.custServicePhonenoExt ne ''}">
                    <td class="lable">Ext :</td>
                    <td><span class="catagory">
                      <bean:write  name="vendorDetails" property="custServicePhonenoExt" />
                    </span><br /></td>
					</c:if>
                  </tr>
              </table></td>
              <td width="150" align="left" valign="top"  nowrap="NOWRAP" class="lable"><span class="boldtext">Email</span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="custServiceEmail" />
              </span><br /></td>
            </tr>
			<tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
			<tr align="left" valign="middle" class="tdbg">
              <td height="23" colspan="6" nowrap="nowrap" bgcolor="#EFEFEF" class="lable">Return Details </td>
            </tr>
            <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="nowrap" bgcolor="#CCCCCC" class="lable"></td>
            </tr>
            <tr class="tdbg">
              <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Company Address</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnAddr1" />
              </span></td>
              <td align="left" valign="top"  nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnAddr2" />
              </span><br /></td>
              </tr>
            <tr class="tdbg">
              <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnCity" />
              </span></td>
              <td align="left" valign="top"  nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">

				  <logic:iterate id="states" name="StateList" scope="application">
                  <c:if
				test="${states.stateCode eq vendorDetails.returnState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              </span><br /></td>
              </tr>
            <tr class="tdbg">
              <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnCountry" />
              </span></td>
              <td align="left" valign="top"  nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
              <td align="center" valign="top" class="lable">:</td>
              <td align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnZip" />
              </span><br /></td>
              </tr>
            <tr class="tdbg">
              <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Return Days </span></td>
              <td width="10" align="center" valign="top" class="lable">:</td>
              <td width="250" align="left" valign="top"><span class="catagory">
                <bean:write  name="vendorDetails" property="returnDays" />
              </span></td>
              <td width="150" align="left" valign="top"  nowrap="NOWRAP" class="lable">&nbsp;</td>
              <td width="10" align="center" valign="top" class="lable">&nbsp;</td>
              <td width="250" align="left" valign="top">&nbsp;</td>
            </tr>
			<tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
            </tr>
            <tr class="tdbg">
            <td width="150" align="left" valign="top" nowrap="NOWRAP" class="lable"><span class="boldtext">Return Policy</span></td>
            <td width="10" align="center" valign="top" class="lable">:</td>
            <td colspan="5" align="left" style="word-wrap:break-word;width:700px;" valign="top"><bean:write  name="vendorDetails" property="vendReturnPolicy"/></td>
          </tr>
		   <tr align="left" valign="top" class="tdbg">
              <td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
            </tr>
            <logic:present name="VendorComments" scope="request">
				<logic:notEmpty name="VendorComments" scope="request">
		           <tr class="tdbg">
			        <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
			        <td width="10" colspan="5" align="left" valign="top" class="lable">&nbsp;</td>
			     </tr>
		    	 <tr class="tdbg">
		        	<td colspan="6" align="left" style="word-wrap:break-word;width:700px;" valign="top"> 
						<div id="VendorComments" style="margin:5px 0 5px 0;">
							<table width="100%"align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
				     			<thead class="tablehead">
				     				<tr  height="21">
				     					<td align="center" nowrap="nowrap">
				     						User Id
				     					</td>
				     					<td align="center">
				     						Date
				     					</td>
				     					<td width="70%" align="center">
				     						Comments
				     					</td>
				     				</tr>
				     			</thead>
		
				     			<tfoot>
						          <tr>
						            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
						          </tr>
						        </tfoot>
		
				     			<tbody>
					     			<logic:iterate id="vendorComment" name="VendorComments">
				     					<tr height="20">
											<td align="left">
												<bean:write name="vendorComment" property="createId" />
											</td>
											<td align="center" nowrap="nowrap">
												<bean:write name="vendorComment" property="createDate" />
											</td>
											<td align="left" >
												<bean:write name="vendorComment" property="comments" />
											</td>
										</tr>
									</logic:iterate>
								</tbody>
							</table>
						</div>
			        </td>
		        </tr>
		        <tr align="left" valign="top" class="tdbg">
		              <td height="1" colspan="6" nowrap="NOWRAP" bgcolor="#CCCCCC" > </td>
		         </tr>
        	 </logic:notEmpty>
		</logic:present>
        </table></td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center">
        	<!--<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none"> &nbsp;&nbsp;&nbsp; OK&nbsp;&nbsp;&nbsp; </html:link>-->
        	<html:button property="method" onclick="javascript:fnCallHome();" value="OK" styleClass="button"/>
        </td>
      </tr>
    </table></td>
    </tr>
  
  <tr>
  <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
<html:hidden property="vendId" />

</td></tr>
</html:form>
