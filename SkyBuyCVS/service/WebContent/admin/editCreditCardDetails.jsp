<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<script type="text/javascript" src="../js/validate_creditcard.js"></script>
<link type="text/css" href="../style/registration.css" rel="stylesheet">
<style>
.bluelink{
	font-size:11px;
	font-weight:bold;
	color:#0066CC;
	text-decoration:none;
}
.bluelink:hover{ 
	font-size:11px;
	font-weight:bold;
	color:#3f6695;
	text-decoration:underline;
}
</style>
<script type="text/javascript">
	function IsInteger(sText)
	
	{
	   var ValidChars = "0123456789";
	   var IsNumber=true;
	   var Char;
	
	 
	   for (i = 0; i < sText.length && IsNumber == true; i++) 
	      { 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1) 
	         {
	         IsNumber = false;
	         }
	      }
	   return IsNumber;
	}
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	function isCardValidityExpired() {
		var month=new Number(document.forms[0].expMonth.value);
		var year=new Number(document.forms[0].expYear.value);
		var date = new Date(year,month,0); 
		var curDate = new Date();
		curDate.setDate(curDate.getDate() + 60);
		if(date<curDate){
			alert('Invalid expiration date. Expiration date should be greater than 60 days');
			return false;
		}else{
			return true;
		}
		
	}
	function getValidTelephone(p_value){
		var phTemp="";
		var validchars = "0123456789";
		if (p_value == null)
			return "";
			
		var parm1 =trim(p_value);	
		for (var i=0; i<parm1.length; i++) {
			temp1 = "" + parm1.substring(i, i+1);		
				if(IsNumeric(temp1)){			
					phTemp=phTemp+temp1;				
				}		
		}
		if (phTemp.length != 10){		
			return phTemp;
		}
		if (phTemp=="0000000000"){		
			return phTemp;
		}
		return phTemp;
	}
	function IsNumeric(sText){
	   var ValidChars = "0123456789";
	   var IsNumber=true;
	   var Char;
	   for (i = 0; i < sText.length && IsNumber == true; i++) 
	      { 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1) 
	         {
	         IsNumber = false;
	         }
	      }
	   return IsNumber;   
	}
	function getValidZip(p_value){
		var phTemp="";
		if (p_value == null) {
			return false;
		}	
		if(isNaN(p_value)) {
			alert("Invalid format of Zip code ");
			return false;
		}
		if (p_value=="00000"){
			alert("Invalid Zip code");		
			return false;
		}
		if(p_value.length == 5 || p_value.length == 9) {
			return true;
		}else {
			if(p_value.length > 5 && p_value.length < 9) {
				alert("Zip code should not greater than 5 digits or less than 9 digits");
			}
			if(p_value.length > 9) {
				alert("Zip code should not greater than 9 digits");
			}
			if(p_value.length < 5) {
				alert("Zip code should not less than 5 digits");
			}
			return false;
		}
	}	
	
	function validateContactEmail(str) {
		isValid=true;
		var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
		if (!regExp.test(str)) {
			isValid=false;
			alert("Invalid e-mail address");	
		}
		return isValid;
	}	
	
	function isValidTelephone(p_value){
		var phTemp="";
		if (p_value == null){
			alert("Telephone No is required.");
			return false;
		}
		if(isNaN(p_value)) {
			alert("Invalid format of phone no");
			return false;
		}	
		if(p_value.length<10) {
			alert("Telephone No. should be equal to 10 or 11 digits");
			return false;
		}
		if(p_value.length>11) {
			alert("Telephone No. should not more than 11 digits");
			return false;
		}		
		if (p_value=="0000000000" || p_value=="00000000000"){
			alert("Invalid Telephone No.");		
			return false;
		}
		return true;
	}
	
	function isValidCard() {
		var cardNo = document.forms[0].creditCardNo.value;
		var cardType = document.forms[0].cardType.value;
		if(checkCreditCard (cardNo, cardType)){
			return true;
		}else{
			return false;
		}
	}
	
	function isEligibleForSubmit() {
		var TeleNo= trim(document.forms[0].custPhone.value);
		var emailId = trim(document.forms[0].custEmail.value);
		var zipNo = trim(document.forms[0].custZip.value);
		var firstName = trim(document.forms[0].custFirstName.value);
		var lastName = trim(document.forms[0].custLastName.value);
		var state = trim(document.forms[0].custState.value);
		var address = trim(document.forms[0].custAddress1.value);
		var city = trim(document.forms[0].custCity.value);
		
		TeleNo = getValidTelephone(TeleNo);
		
		document.forms[0].custPhone.value = TeleNo;
		document.forms[0].custEmail.value = emailId;
		document.forms[0].custZip.value = zipNo;
		document.forms[0].custFirstName.value = firstName;
		document.forms[0].custLastName.value = lastName;
		document.forms[0].custState.value = state;
		document.forms[0].custAddress1.value = address;
		document.forms[0].custCity.value = city;
		
		if(firstName == "") {
			alert("Customer FirstName is required");
			return false;
		}
		if(lastName == "") {
			alert("Customer LastName is required");
			return false;
		}
		if(state == "") {
			alert("Select a State");
			return false;
		}
		if(address == "") {
			alert("Address is required");
			return false;
		}
		if(city == "") {
			alert("City is required");
			return false;
		}
		if(!isCardValidityExpired()) {
			return false;
		}
		/*if(!isValidCard()) {
			return false;
		}*/
		if(!isValidTelephone(TeleNo)) {
			return false;
		}
		if(!validateContactEmail(emailId)) {
			return false;
		}
		if(!getValidZip(zipNo)) {
			return false;
		}
		
		return true;	
	}
	
	function fnCallSubmit(js_OrderItemId,js_IsUpdate,j_sSourceOfEdit) {
		var cvvNo = document.forms[0].cvv.value;
		var cardType = document.forms[0].cardType.value;
		if(!IsInteger(cvvNo) || (trim(cvvNo).length < 3)){
				alert('Please enter valid CVV Number');
				return false;
		}
		
		
		var isCCNoChange = document.forms[0].ChangeCardNo.value;
		if(isCCNoChange == 'YES'){
			if(document.forms[0].newCreditCardNo.value != ''){
				
				if(checkCreditCard (document.forms[0].newCreditCardNo.value, cardType)){
					document.forms[0].creditCardNo.value = document.forms[0].newCreditCardNo.value;
				}else{
					return false;
				}
			}else{
				alert('Enter new credit card no');
				return false;
			}
		}	
		
		if(!checkCreditCard (document.forms[0].creditCardNo.value, cardType)){
			//alert('Enter valid credit card no');
			return false;
		}
		
		if(isEligibleForSubmit()) {
			document.forms[0].action="editCreditCardDetails.do?method=updateCreditCardDetail&OrderItemId="+js_OrderItemId+"&IsUpdate="+js_IsUpdate+"&SourceOfEdit="+j_sSourceOfEdit;
			document.forms[0].submit();
		}
	}
	
	function fnChange(){
		document.forms[0].ChangeCardNo.value = 'YES';
		document.getElementById("CCChangeLabel").style.display="none";
		document.getElementById("CCNoChangeLabel").style.display="inline";
		document.getElementById("newCCNo").style.visibility="visible";
		document.forms[0].newCreditCardNo.focus();
	}
	
	function fnNoChange(){
		document.forms[0].ChangeCardNo.value = 'NO';
		document.getElementById("CCChangeLabel").style.display="inline";
		document.getElementById("CCNoChangeLabel").style.display="none";
		document.getElementById("newCCNo").style.visibility="hidden";
		
	}
	function fnCallOrderItemDetails(jsOrderItemId, jsSearchOrder) {
		document.forms[0].action = "editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemId+"&PlaceOrder="+jsSearchOrder;
		document.forms[0].submit();
	}
</script>
<html:form action="/admin/editCreditCardDetails" method="post">
<tr><td>
<input type="hidden" name="ChangeCardNo" value="NO"/>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			 <td colspan="3">
			<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Edit/Search Order</li>
				</ul>
			</div>			
		</div>
	</td>
		
		
		
		
		
			</tr>
			<tr class="tdbg">
				<td class="td" colspan="3">
					<div class="error">
					<logic:present name="InvalidCC">
						<logic:notEmpty name="InvalidCC">
							<c:out value="${InvalidCC}" />
						</logic:notEmpty>
					</logic:present>
					</div>
				</td>
			</tr>
			<tr class="tdbg">
				<td colspan="3" class="td">
					<div id="creditcard">
					  <table width="65%" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:15px" class="border">
					  	<tr>
					  		<td align="center" colspan="4" class="tablehead" height="25">
					  			<b>Credit Card Information</b>
					  		</td>
					  	</tr>
					    <tr>
					    	<td>
					    	 	<table border="0" cellpadding="0" cellspacing="0" align="center" style="margin:15px">
					    	 		<tr>
				                      <td width="100" align="left" valign="top" nowrap="nowrap" class="lable">Card Type </td>
				                      <td valign="top" class="lable"> <b> : </b> </td>
				                      <td align="left" valign="top" class="lable"> 
				                      	<html:select property="cardType" styleClass="textarea2" tabindex="1">
				                      		<html:option value="">--Select Card Type--</html:option>
				                      		<html:option value="AC">American Express</html:option>
				                      		<html:option value="MC">Master Card</html:option>
				                      		<html:option value="VC">Visa Card</html:option>
				                      	</html:select>
				                      </td>
				                      <td align="left" nowrap="nowrap" valign="top" class="lable">Credit Card No </td>
				                      <td valign="top" class="lable"> <b> : </b> </td>
				                      <td align="left" class="lable"> 
				                      <html:hidden property="creditCardNo"/>
				                       <c:out value="${CreditCardDetails.maskedCCNo}"/><span id="CCChangeLabel">&nbsp;&nbsp;<a href="javascript:fnChange();" class="bluelink">Change</a></span>
				                      <span id="CCNoChangeLabel" style="display:none;">&nbsp;<a href="javascript:fnNoChange();" class="bluelink">No&nbsp;Change</a></span>
				                      <span id="newCCNo" style="visibility:hidden;">
				                      <input type="text" name="newCreditCardNo" class="input" maxlength="16"/>
				                      </span>
				                      </td>
									  
									   <td align="left" nowrap="nowrap" valign="top" class="lable">CVV No * </td>
				                      <td valign="top" class="lable"> <b> : </b> </td>
				                      <td align="left" class="lable" valign="top"> 
				                      <html:password property="cvv" styleClass="input" style="width:50px;" maxlength="4"/>
				                      </td>
				                    </tr>
				                    <tr>
				                      <td width="100" align="left" nowrap="nowrap" class="lable"> Cardholder's Name</td>
				                      <td class="lable"> <b> : </b> </td>
				                      <td align="left" class="lable"> <html:text property="creditCardHolderName" styleClass="input" maxlength="63" tabindex="2" style="width:145px;"/> </td>
				                      <td align="left" nowrap="nowrap" class="lable">Exp. Date</td>
				                      <td class="lable"> <b> : </b> </td>
				                      <td width="16%" nowrap="nowrap" class="lable" valign="top">
					                      Month <b>:</b>
					                      <html:select property="expMonth" styleClass="textarea1" style="width:40px;" tabindex="3">
					                          <html:option value="1">01</html:option>
					                          <html:option value="2">02</html:option>
					                          <html:option value="3">03</html:option>
					                          <html:option value="4">04</html:option>
					                          <html:option value="5">05</html:option>
					                          <html:option value="6">06</html:option>
					                          <html:option value="7">07</html:option>
					                          <html:option value="8">08</html:option>
					                          <html:option value="9">09</html:option>
					                          <html:option value="10">10</html:option>
					                          <html:option value="11">11</html:option>
					                          <html:option value="12">12</html:option>
				                        </html:select>
					                        Year <b>:</b>
					                        <html:select property="expYear" styleClass="textarea1" style="width:60px;" tabindex="4">
					                            <html:options collection="ExpYears" property="yearCode" labelProperty="yearValue"/>
					                        </html:select>
			                          </td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
								 </tr>
			                  </table>
		                  </td>
				            </tr>
				  
							<tr>
				            	<td colspan="3" class="td">
				            		<table width=60% border="0" cellpadding="0" cellspacing="0" align="center" style="margin:15px" class="border">
				            			<tr class="tdbg">
				            				<td align="center" class="tablehead" height="20">
				            					<b>Billing Details</b>
				            				</td>
				            			</tr>
				            			<tr class="tdbg">
				            				<td>
					            				<table width="100%" border="0" cellpadding="1" cellspacing="1" class="broder_top0">
							            			<tr>
							            				<td class="lable" nowrap="nowrap" align="left"> Customer First Name </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custFirstName" tabindex="5" maxlength="64"/></td>
							            				<td class="lable" nowrap="nowrap" align="left"> Customer Last Name </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custLastName" tabindex="6" maxlength="64"/></td>
							            			</tr>
							            			<tr>
							            				<td class="lable" nowrap="nowrap" align="left"> Address Line 1</td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custAddress1" tabindex="7" maxlength="128"/></td>
							            				<td class="lable" nowrap="nowrap" align="left"> Address Line 2</td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custAddress2" tabindex="8" maxlength="128"/></td>
							            			</tr>
							            			<tr>
							            				<td class="lable" nowrap="nowrap" align="left"> City </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custCity" tabindex="9" maxlength="64"/></td>
							            				<td class="lable" nowrap="nowrap" align="left"> State </td>
							            				<td class="lable">:</td>
							            				<td class="lable" align="left"> 	 
								            				<html:select property="custState" styleClass="input" styleId="state" tabindex="10">
																 <html:option value="">------Select State------</html:option>
																 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
										           			</html:select>
														</td>
							            			</tr>
							            			<tr>
							            				<td class="lable" nowrap="nowrap" align="left"> Country </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> 
							            					<html:select property="custCountry" tabindex="11">
							            						<html:option value="US">US</html:option>
							            					</html:select>
							            				</td>
							            				<td class="lable" nowrap="nowrap" align="left"> Zip  </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custZip" tabindex="12"/></td>
							            			</tr>
							            			<tr>
							            				<td class="lable" nowrap="nowrap" align="left"> Phone No </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custPhone" tabindex="13"/></td>
							            				<td class="lable" nowrap="nowrap" align="left"> Email  </td>
							            				<td class="lable">:</td>
							            				<td class="lable" nowrap="nowrap" align="left"> <html:text property="custEmail" tabindex="14" maxlength="64"/></td>
							            			</tr>
							            		</table>
						            		</td>
						            	</tr>
				            		</table>
				            	</td>
				            </tr>
				            <tr class="tdbg">
				            	<td colspan="3" align="center" class="td">
				            		<input type="button" value="Submit" class="button" onclick="fnCallSubmit('<bean:write name="orderItemForm" property="orderItemId"/>','update','<c:out value="${SourceOfEdit}" />')" tabindex="15"/>
				            		<input type="button" onclick="javascript:fnCallOrderItemDetails('<bean:write name="orderItemForm" property="orderItemId"/>','<c:out value="${SourceOfEdit}" />');" class="button" value="Cancel" />
				            	</td>
				            </tr>
				        </table>
	        		</div>
	        	</td>
	        </tr>
			<tr>
			<td colspan="3">
			<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
			</div>
			</td>
			  
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	</td></tr>
</html:form>