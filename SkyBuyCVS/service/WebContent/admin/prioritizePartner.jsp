<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

	function textLimit(field,maxlen,dispName) {
	   fieldLen=trim((field.value)).length;
	  if (fieldLen > parseInt(maxlen) + 1){
			alert(dispName+" can have maximum of "+maxlen+" chars only."); 
			return false;
		}else
			return true;
	}
	function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
	function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}

	function triggerEvent() {
		if(event.keyCode==13) {
		fnCallSearch();
		}           
	}
	function fnValidateName(jsName,jsLabelName) {		
			if(isEmpty(jsName)){
				alert("Please enter "+jsLabelName);
				document.forms[0].searchValue.focus();
				return false;
			}else{
				document.forms[0].searchValue.value = trim(document.forms[0].searchValue.value);	
				document.forms[0].action="partnerPriority.do?method=searchPartnerPriority";
				document.forms[0].submit();			
			}
		}	
	function fnValidateId(jsId,jsLabelName) {		
			if(isEmpty(jsId)){
				alert("Please enter valid "+jsLabelName);
				document.forms[0].airlineSearchValue.focus();
				return false;
			}else if(!isNumber(jsId)){
				alert("Please enter valid "+jsLabelName);
				document.forms[0].airlineSearchValue.focus();
				return false;
			}else{	
				document.forms[0].action="partnerPriority.do?method=searchPartnerPriority";
				document.forms[0].submit();			
			}
		}
	function fnCallSearch(){
		if(document.forms[0].searchBy.value=='VENDORNAME'){
			fnValidateName(document.forms[0].searchValue,"Vendor Name");
		}else if(document.forms[0].searchBy.value=='AIRLINENAME'){
			fnValidateName(document.forms[0].searchValue,"Air Charter Name");
		}
		else if(document.forms[0].searchBy.value=='ItemCode'){
			fnValidateName(document.forms[0].searchValue,"Item Code");
		}else if(document.forms[0].searchBy.value=='All' || document.forms[0].searchBy.value=='ALLVENDOR' || document.forms[0].searchBy.value=='ALLAIRLINE'){
			document.forms[0].searchValue.value = '';
			document.forms[0].action="partnerPriority.do?method=searchPartnerPriority";
			document.forms[0].submit();	
		}else{
			document.forms[0].searchValue.value = trim(document.forms[0].searchValue.value);	
			document.forms[0].action="partnerPriority.do?method=searchPartnerPriority";
			document.forms[0].submit();			
		}
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function trimOtherThanNumber(jsPriorityOfMerchandize) {
		var priorityRegEx = new RegExp("^[0-9]*$");
		var returnValue = jsPriorityOfMerchandize;
		var startChar = returnValue.substring(0,1);
		while(!priorityRegEx.test(startChar)) {
			returnValue = returnValue.substring(1, returnValue.length);
			startChar = returnValue.substring(0,1);
		}
		startChar = returnValue.substring(returnValue.length-1, returnValue.length);
		while(!priorityRegEx.test(startChar)) {
			returnValue = returnValue.substring(0, returnValue.length-1);
			startChar = returnValue.substring(returnValue.length-1, returnValue.length);
		}
		return returnValue;
	}
	
	function isValidNumber(field) {
		var priorityOfProduct = field.value;
		var priorityRegEx = new RegExp("^[0-9]*$");
		
		field.value = trim(priorityOfProduct);
		field.value = trimQuantity(priorityOfProduct);
		
		if(!priorityRegEx.test(field.value)) {
			
			field.value = trimOtherThanNumber(priorityOfProduct);
			alert('Enter a valid number');
		}
	}
	
	function fnCallSavePriority() {
		document.forms[0].action="partnerPriority.do?method=updatePartnerPrioritize";
		document.forms[0].submit();
	}
	
	function fnCallSetPriorityToMerchandize() {
		document.forms[0].action="partnerPriority.do?method=initPartnerPriority";
		document.forms[0].submit();
	}	
 
 	function fnCallMerchandise(jsOwnerType,jsOwnerId){
 	
 	if(jsOwnerType == 'Vendor')
 		jsOwnerType = 'VendorName'
 	else
 		jsOwnerType = 'AirlineName'
 		document.forms[1].searchBy.value=jsOwnerType;
 		document.forms[1].searchValue.value=jsOwnerId;
 		
 		document.forms[1].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority&source=prioritizepartner";
 		//document.forms[1].action="searchMerchandizeToSetPriority.do?method=searchMerchandizeToSetPriority&searchBy="+jsOwnerType+"&searchValue="+jsOwnerId+"&source=prioritizepartner;
		document.forms[1].submit();
 	}


	
</script>

<html:form action="admin/partnerPriority" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
   
  
  
  
      <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Merchandise</li>
					<li>></li>
					<li>Prioritize Partner</li>
				</ul>
			</div>			
		</div>
	</td>
</tr>
  
  
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
    	<logic:present name="ErrorMsg">
	      <tr align="center">
	      <td class="error" align="center">
	      	<c:out value="${ErrorMsg}"/>
	      <br></td>
	      </tr>
	    </logic:present>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap">
				<html:select property="searchBy" styleClass="textarea2" name="partnerPrioritizeForm">
					<html:option value="ALL">All</html:option>	
					<html:option value="ALLVENDOR">All  Vendor</html:option>	
					<html:option value="VENDORNAME">Vendor Name</html:option>	
					<html:option value="ALLAIRLINE">All  Air Charter</html:option>				
					<html:option value="AIRLINENAME">Air Charter Name</html:option>							  		
              </html:select>            </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Status :</td>
            <td nowrap="nowrap"><html:select property="status" styleClass="textarea2">
                <html:option value="ALL">All</html:option>
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
            </html:select></td>
             <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
			</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
   <td colspan="3" class="td" align="center" valign="top">
    <logic:present  name="partnerPrioritizeForm" property="recordSet"> 
 	 <logic:notEmpty name="partnerPrioritizeForm" property="recordSet">	
	<table border="0" width="90%" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Partner Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
	          <td align="center" class="table_header" nowrap="nowrap">Partner Type</td>
	          <td class="table_header" nowrap="nowrap">Name</td>
	          <td height="32" class="table_header" nowrap="nowrap">Contact Name</td>
              <td class="table_header">Address </td>
              <td class="table_header" nowrap="nowrap">City</td>  
			  <td class="table_header" nowrap="nowrap">State</td>        
              <td class="table_header" nowrap="nowrap">Country</td>
			  <td class="table_header" nowrap="nowrap">Status</td>
			  <td nowrap="nowrap" class="table_header">Priority</td>
		</tr>
		  <logic:iterate name="partnerPrioritizeForm" property="recordSet" id="partnerInfo"  indexId="ctr">
          <tr class="tdbg">
          <td align="center" class="lable" nowrap="nowrap">
          <bean:write name="partnerInfo" property="ownerType"/>
          </td>
          <td align="left" nowrap="nowrap" width="100px"><div class="lable" >
          <a href="javascript:fnCallMerchandise('<bean:write name="partnerInfo" property="ownerType"/>','<bean:write name="partnerInfo"  property="ownerId"/>');">
          <bean:write name="partnerInfo"  property="ownerName"/>
          </a>
          </div>
          </td>
            <td align="left" valign="center" class="lable"><div align="left">
			<bean:write name='partnerInfo' property='ownerContactName'/>
			</div>
			</td>
			 <td align="left" nowrap="nowrap" valign="center" class="lable"><div align="left">
			 <bean:write name="partnerInfo" property="ownerAddress1"/>
			</div>
			</td>
             
            <td align="left">
			    <span class="lable"><bean:write name="partnerInfo" property="ownerCity"/></span><br/>
             </td>
          
            <td align="left" valign="middle" nowrap="nowrap"><div class="lable">
            <logic:iterate id="states" name="StateList" scope="application">
					<c:if
						test="${states.stateCode eq partnerInfo.ownerState}">
						<bean:write name="states" property="stateName" />
					</c:if>
			 </logic:iterate>	
            </div>
            </td>
             <td align="center" valign="middle" nowrap="nowrap"><div class="lable">
             <bean:write name="partnerInfo" property="ownerCountry"/>
            </div>
            </td>
             <td align="center" valign="middle" nowrap="nowrap"><div class="lable">
             <c:if test="${partnerInfo.ownerStatus eq  'A'}">
			Active
			</c:if>
			<c:if test="${partnerInfo.ownerStatus eq 'I'}">
			Inactive
			</c:if>
             
            </div>
            </td>
           		<td align="center" class="action" > 
           			<html-el:text property="partnerDetails[${ctr}].ownerPriority" value="${partnerInfo.ownerPriority}" styleClass="search-input" onkeyup="javascript:isValidNumber(this);" onchange="javascript:isValidNumber(this);"/>
           		</td>		
            </tr>
		   </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="partnerPrioritizeForm" property="recordSet"> 
	 <logic:notEmpty name="partnerPrioritizeForm" property="recordSet">	
  <tr>
      <td height="50" colspan="3" align="center" class="td">
      	<html:button property="method" onclick="javascript:fnCallSavePriority();" styleClass="button" value="Save" />
      	<html:button property="method" onclick="javascript:fnCallSetPriorityToMerchandize();" styleClass="button" value="Cancel" />
      </td>
    </tr>
	</logic:notEmpty>
	</logic:present>
  <tr>
   <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
<html:form action="admin/initSetPriorityToMerchandize" method="post" >
<html:hidden property="searchBy"/>
<html:hidden property="searchValue"/>
</html:form>