<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script>
function fnCallAddAirline(){
	document.forms[0].action="addAirlineReg.do?method=addAirlineReg";
	document.forms[0].submit();
}
function fnCallEditAirline(){
	document.forms[0].action="updateAirlineReg.do?method=updateAirlineDetails";
	document.forms[0].submit();
}
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

function getCatalogue(link, windowname){
	window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');	
}
</script>





<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
   <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Generate Catalogue</li>
		</ul>
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">

	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Preview Catalogue</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="0" class="broder_top0">
	 <tr class="tdbg">
        <td class="lable" nowrap="nowrap" align="center"></td>   
        
      </tr>
      <tr class="tdbg">
        <td class="clickhere" nowrap="nowrap" align="center"><strong>Successfully  Generated Catalogue.<br /> 
          <a href="javascript:getCatalogue('../previewCatalogue.jsp','catalogue');">Click here</a> to view the Catalogue</strong></td>   
        
      </tr>
	   <tr class="tdbg">
        <td class="lable" nowrap="nowrap" align="center"></td>   
        
      </tr>
     
    </table></td>
      </tr>
      
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>


