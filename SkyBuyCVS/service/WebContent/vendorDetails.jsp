<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}


function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;   
}	
function getValidTelephone(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp.length != 10){		
		return "";
	}
	if (phTemp=="0000000000"){		
		return "";
	}
	return phTemp;
}
function validateContactEmail(str){
	var str=document.forms[0].vendEmail.value;
	isValid=true;
	var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	if (!regExp.test(str)){
		isValid=false
		alert("Email is an invalid e-mail address.")	
	}
	return (isValid)
}
function getValidZip(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp=="00000"){		
		return "";
	}
	return phTemp;
}	
function fnCallAddVendor(){
	<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'admin'}">
				var userId = document.forms[0].vendUserId.value;
				if(userId.length<3){	
					alert("Please enter Vendor User Id more than 3 characters");
					document.forms[0].vendUserId.focus();
					return false;
				}
				
				var password = document.forms[0].vendPassword.value;
				if(password.length<3){	
					alert("Please enter Vendor Password more than 3 characters");
					document.forms[0].vendPassword.focus();
					return false;
				}
	  </c:if>
	</logic:present>

	var phoneNo;
	phoneNo = getValidTelephone(document.forms[0].vendPhone1.value)
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].vendPhone1.focus();
		return false;
	} else {	
		document.forms[0].vendPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].vendPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension1 should be valid.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>=6){
			alert("Extension1 should not be more than 6 digits.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		} 
	}
	
	
	var phoneNo2=document.forms[0].vendPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Phone Number2 should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendPhone2.focus();
			return false;
		} else {	
			document.forms[0].vendPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].vendPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Extension2 should be valid.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>=6){
			alert("Extension2 should not be more than 6 digits.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		} 
	}
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].vendFax.value)	
	if (document.forms[0].vendFax.value != null && document.forms[0].vendFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendFax.focus();
			return false;
		} else {	
			document.forms[0].vendFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].vendZip.value)	
	if (document.forms[0].vendZip.value != null && document.forms[0].vendZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].vendZip.focus();
			return false;
		} else {	
			document.forms[0].vendZip.value = zipNo;
		}
	}

	if(validateContactEmail()){
		document.forms[0].action="addVendor.do?method=addVendorDetails";
		document.forms[0].submit();
	}
}
function fnCallEditVendor(){
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'}">
			var userId = document.forms[0].vendUserId.value;
			if(userId.length<3){	
				alert("Please enter Vendor User Id more than 3 characters");
				document.forms[0].vendUserId.focus();
				return false;
			}
			
			var password = document.forms[0].vendPassword.value;
			if(password.length<3){	
				alert("Please enter Vendor Password more than 3 characters");
				document.forms[0].vendPassword.focus();
				return false;
			}
		</c:if>
	</logic:present>
	var phoneNo;	
	phoneNo = getValidTelephone(document.forms[0].vendPhone1.value)
	
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].vendPhone1.focus();
		return false;
	} else {	
		document.forms[0].vendPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].vendPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension1 should be valid.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>=6){
			alert("Extension1 should not be more than 6 digits.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		} 
	}
	
	
	var phoneNo2=document.forms[0].vendPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Phone Number2 should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendPhone2.focus();
			return false;
		} else {	
			document.forms[0].vendPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].vendPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Extension2 should be valid.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>=6){
			alert("Extension2 should not be more than 6 digits.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		} 
	}
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].vendFax.value)	
	if (document.forms[0].vendFax.value != null && document.forms[0].vendFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendFax.focus();
			return false;
		} else {	
			document.forms[0].vendFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].vendZip.value)	
	if (document.forms[0].vendZip.value != null && document.forms[0].vendZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].vendZip.focus();
			return false;
		} else {	
			document.forms[0].vendZip.value = zipNo;
		}
	}
	if(validateContactEmail()){
		document.forms[0].action="updateVendor.do?method=UpdateVendorDetails";
		document.forms[0].submit();
	}
}
function fnCancel() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();

}

function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of 250 chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}


</script>
<html:javascript formName="vendorDetailsForm"/>
<html:form action="addVendor" method="post" onsubmit="return validateVendorDetailsForm(this);">

<div class="contentcontainer">
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="images/top_nav_middlebg.png">
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Add'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Vendor</li>
		</ul>
		</c:if>
	</logic:present>
	
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
	</logic:present>
	
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'vendor' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Vendor</li>
		<li>></li>
		<li>Edit My Info</li>
		</ul>
		</c:if>
	</logic:present>
	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Vendor Setup</h2></td>
        </tr>
      <tr>
        <td align="center"><table width="96%" border="0" cellpadding="2" cellspacing="2" >
	<c:if test="${!empty vendorNamevendorUserIdExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center" ><span class="error">
		  <bean:message key="vendor.NameUserId"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty vendNameExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="vendor.Name"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty vendorUserIdExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="vendor.UserId"/></span></td>
	</tr>
	</c:if>
   
      <tr class="tdbg">
        <td align="left" class="lable">Vendor Name *</td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
		<logic:present name="mode">
			 <logic:equal name="mode"  value="Add">
          		 <html:text property="vendName" styleClass="input" tabindex="1"/>
		   </logic:equal>
		</logic:present>   
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="Edit">
			 		 <bean:write  name="vendorDetails" property="vendName"/>
          	</logic:equal>
		</logic:present>   
        </span></td>
	
		<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'admin'}">
				<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor Type *</span></td>
				<td class="lable">:</td>
				<td align="left"><span class="catagory">
					<html:select property="vendType" tabindex="2" styleClass="select">
					 <html:option value="B">Basic</html:option>
					 <html:option value="P">Premium</html:option>
				  </html:select>
				</span></td>
			</c:if>
		</logic:present>
      </tr>	
		
	  	<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'admin'}">
			
	  <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor User Id *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendUserId" styleClass="input" tabindex="3"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor Password *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:password property="vendPassword" styleClass="input" tabindex="4"/>
        </span></td>
      </tr>
	   </c:if>
	</logic:present>   
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendContactName" styleClass="input" tabindex="5"/>
          </span></td>
        <td align="left" class="lable"><span class="boldtext">Status * </span></td>
        <td class="lable">:</td>
        <td align="left">
			<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'admin'}">
				<html:select property="vendStatus" tabindex="6" styleClass="select">
				 <html:option value="A">Active</html:option>
				 <html:option value="I">Inactive</html:option>
				</html:select>
			</c:if>
			</logic:present>
			
			<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'vendor'}">
				 <logic:equal name="vendorDetails" property="vendStatus" value="I">
					Inactive					</logic:equal>				
					<logic:equal name="vendorDetails" property="vendStatus" value="A">
					Active					</logic:equal>
				<html:hidden property="vendStatus" />
			</c:if>
			</logic:present>		</td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address*</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendAddr1" styleClass="input" tabindex="7"/>
        </span></td>
        <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Additional Address</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendAddr2" styleClass="input" tabindex="8"/>
        </span></td>
      </tr>
	   <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">City *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendCity" styleClass="input" tabindex="9"/>
        </span></td>
        <td align="left" class="lable"><span class="boldtext">State *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
		  <html:select property="vendState" styleClass="input" styleId="state" tabindex="10">
			 <html:option value="">------Select State------</html:option>
			 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
            </html:select>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td class="lable">:</td>
        <td align="left">
         
		  <html:select property="vendCountry" tabindex="11" styleClass="select">
			 <html:option value="US">US</html:option>
		  </html:select>       </td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendZip" styleClass="input" tabindex="12"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone1 *</td>
        <td class="lable">:</td>
        <td align="left">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="vendPhone1" styleClass="input-phone" tabindex="13"/>
        </span></td>
              <td class="lable">Ext1 :</td>
              <td><span class="catagory">
          <html:text property="vendPhoneExt1" styleClass="input-ext" tabindex="13"/>
        </span></td>
            </tr>
          </table></td>
        <td align="left" class="lable">Phone2 </td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
                <html:text property="vendPhone2" styleClass="input-phone" tabindex="13"/>
              </span></td>
              <td class="lable">Ext2:</td>
              <td><span class="catagory">
                <html:text property="vendPhoneExt2" styleClass="input-ext" tabindex="13"/>
              </span></td>
            </tr>
        </table></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">Fax *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendFax" styleClass="input" tabindex="15"/>
        </span></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Email *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendEmail" styleClass="input" tabindex="14"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Return Policy</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
          <textarea name="vendReturnPolicy" cols="66" rows="6" tabindex="14" onkeyup="textLimit(this.form.vendReturnPolicy,this.form.policylen,250,'Return Policy');"><logic:present name="vendorDetails"><logic:notEmpty name="vendorDetails"><bean:write  name="vendorDetails" property="vendReturnPolicy"/></logic:notEmpty></logic:present></textarea>
          <br/>
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="policylen" size="3" maxlength="3" value="250" class="wordcount"/>        </td>
      </tr>
	  <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
          <textarea name="vendComments" cols="66" rows="6" tabindex="14" onkeyup="textLimit(this.form.vendComments,this.form.commentlen,250,'Comments');"><logic:present name="vendorDetails"><logic:notEmpty name="vendorDetails"><bean:write  name="vendorDetails" property="vendComments"/></logic:notEmpty></logic:present></textarea>
          <br/>
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="commentlen" size="3" maxlength="3" value="250" class="wordcount"/>        </td>
      </tr>
      
    </table>      </tr>
     
      <tr>
         <td height="30" colspan="3" align="center"><logic:present name="mode">
			 <logic:equal name="mode"  value="Add">
	  <html:button property="method" styleClass="button" tabindex="16"
		  onclick=" javascript:if(document.forms[0].onsubmit())fnCallAddVendor(); void 0"> Add </html:button> 
		   <html:button property="method" onclick="javascript:fnCancel();" styleClass="button">Cancel</html:button>
		   </logic:equal>
		</logic:present>   
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="Edit">
	  <html:button property="method" styleClass="button" tabindex="16"
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallEditVendor(); void 0">Save </html:button> 
		      <html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button>
		   </logic:equal>
		</logic:present>   
		
        </td>
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>


<html:hidden property="vendId"/>
<logic:present name="mode">
	<logic:equal name="mode"  value="Edit">
		<html:hidden property="vendName"/>
	</logic:equal>
</logic:present>

<logic:present name="mode">
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType ne 'admin' && mode eq 'Edit'}">
			<html:hidden property="vendUserId" value="-1"/>
			<html:hidden property="vendPassword" value="-1"/>
			<html:hidden property="vendType"/>
			
		</c:if>
	</logic:present>
</logic:present> 

</html:form>
</div>