<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "Description",
theme_advanced_buttons1 : "",
	plugins : "noneditable",			
	theme_advanced_toolbar_location : "none",
	theme_advanced_toolbar_align : "none"
	
});

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/




function fnCallAddProduct(){
	document.forms[0].action="addProduct.do?method=addProductDetails";
	document.forms[0].submit();
}
function fnCallSaveProduct(){
	document.forms[0].action="updateProduct.do?method=updateProductDetails";
	document.forms[0].submit();
}

function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}

function getCatalogue(link, windowname){
	window.open(link, windowname, 'width=800,height=600,scrollbars=No');
}
</script>




<html:form action="addAirlineProduct" method="post" enctype="multipart/form-data">

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
     <td width="100%" align="left" background="images/top_nav_middlebg.png">		
	
		<c:if test="${mode eq 'editAirlineProduct'}">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item</li>
		</ul>
		</c:if>
		
		<c:if test="${mode eq 'addAirlineProduct'}">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Add Item</li>
		</ul>
		</c:if>
	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">

	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Item Summary Page</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
          <tr class="tdbg">
            <td width="119" align="left" nowrap="nowrap" class="lable">Vendor Name</td>
            <td width="10" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">
         <logic:present name="loginInfo" scope="session">
			<bean:write  name="loginInfo" property="userName" />			
		</logic:present>
      </span></td>
            <td width="87" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              <bean:write  name="airlineProductDetails"  property="prodTitle" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code </span></td>
            <td class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="airlineProductDetails"  property="prodCode" />
            </span></td>
            <td  align="left" nowrap="nowrap" class="lable">Price</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <fmt:setLocale value="en_US" />
              <fmt:formatNumber value="${airlineProductDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
            </span></td>
          </tr>
          
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap"  class="lable">Short Description</td>
            <td width="10" valign="top" class="lable">:</td>
            <td colspan="4" align="left"><c:out value="${airlineProductDetails.shortDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">Long Description </td>
            <td width="10" valign="top" class="lable">:</td>
            <td width="50%" colspan="4" align="left" class="catagory"><c:out value="${airlineProductDetails.longDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" class="lable">Item Image(s)</td>
            <td width="10" valign="top" class="lable"></td>
            <td colspan="4" align="left" class="catagory"></td>
          </tr>
		  
		  <tr align="left" class="tdbg" >
		  <td colspan="6" >
		  
		  <table border="0" align="center" cellpadding="2" cellspacing="2">
            <tr>
			
              <td class="border"><img src="<c:out value='${airlineProductDetails.mainImgPath}' />" width="63" height="79" /></td>
              <c:if test="${!empty airlineProductDetails.view1ImgPath}">
			  <td class="border"><img src="<c:out value='${airlineProductDetails.view1ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${!empty airlineProductDetails.view2ImgPath}">
              <td class="border"><img src="<c:out value='${airlineProductDetails.view2ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${!empty airlineProductDetails.view3ImgPath}">
              <td class="border"><img src="<c:out value='${airlineProductDetails.view3ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
            </tr>
            <tr>
              <td bgcolor="#d9ecff" class="border"><bean:write  name="airlineProductDetails"  property="mainImgCap" /></td>
			  <c:if test="${!empty airlineProductDetails.view1ImgPath}">
              <td bgcolor="#d9ecff" class="border"><bean:write  name="airlineProductDetails"  property="view1ImgCap" /></td></c:if>
              <c:if test="${!empty airlineProductDetails.view2ImgPath}"><td bgcolor="#d9ecff" class="border"><bean:write  name="airlineProductDetails"  property="view2ImgCap" /></td></c:if>
              <c:if test="${!empty airlineProductDetails.view3ImgPath}"><td bgcolor="#d9ecff" class="border"><bean:write  name="airlineProductDetails"  property="view3ImgCap" /></td></c:if>
            </tr>
          </table>		</td>
		  </tr>
		  <c:if test="${mode eq 'editAirlineProduct'}">
		  <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
		  
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">Comments </td>
            <td width="10" valign="top" class="lable">:</td>
            <td width="50%" colspan="4" align="left" class="catagory"><c:out value="${airlineProductDetails.sbhComment}" escapeXml="false"/></td>
          </tr>
		  </c:if>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
		   <c:if test="${airlineProductDetails.inShopStatus eq 'A'}">  
          <tr align="left" class="tdbg">
         <td colspan="6" valign="top" class="clickhere"><a href="javascript:getCatalogue('previewProduct.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuyHigh Catalogue </td>
            </tr>
		</c:if>
		
      </table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> <html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none">&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</html:link> </td>
       
      </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


</html:form>
