<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

	 function triggerEvent() {
			if(event.keyCode==13) {
			  var val=document.forms[0].vendorSearchBy.value;
			  fnCallCustInfoByCustIdAndName();  
			}           
	 }
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	function isEmpty(frm_fld){
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
	
	function fnCallCustInfoByCustIdAndName(){
		if(document.forms[0].vendorSearchBy.value=="" && document.forms[0].vendorSearchBy.value!='All'){
			alert("Please select either Customer ID or Customer/DBA Name");
			return;
		}
		else if(document.forms[0].vendorSearchBy.value=="customerId"){
			fnCallCustInfoByCustId(document.forms[0].vendorSearchValue);
		}else if(document.forms[0].vendorSearchBy.value=="customerName"){			
			fnCallCustInfoByCustName(document.forms[0].vendorSearchValue);
		}
		else{	
			if(document.forms[0].vendorSearchBy.value=="All"){
			   document.forms[0].vendorSearchValue.value="";
			}		
			document.forms[0].action="searchVendor.do?method=searchVendor";
			document.forms[0].submit();
		}
	}
	function fnCallCustInfoByCustId(jsCustIdValue) {		
		if(isEmpty(jsCustIdValue)){			
			alert("Please enter valid Customer ID");			
			document.forms[0].vendorSearchBy.focus();			
			return;			
		}
		else{	
			if(isNumber(document.forms[0].vendorSearchValue)){
				if(checkLength(document.forms[0].vendorSearchValue)){
					document.forms[0].action="searchVendor.do?method=searchVendor";
					document.forms[0].submit();
				}
				else
					document.forms[0].vendorSearchBy.focus();
			}	
		}
	}
	function fnCallCustInfoByCustName(jsCustName) {		
		if(isEmpty(jsCustName)){
			alert("Please enter Customer/DBA Name");
			document.forms[0].vendorSearchBy.focus();
			return;
		}
		if(jsCustName.value.length<3){
			alert("Please enter atleast 3 characters");
			document.forms[0].vendorSearchValue.focus();
			return;
		}		
		else{					
			document.forms[0].action="searchVendor.do?method=searchVendor";
			document.forms[0].submit();	
		}
	}
	function isNumber(jsCustNo) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert("Customer ID must be an numeric and should be valid.");
			document.forms[0].vendorSearchBy.focus();						     
			return false;
		}
	  }
	 return true;
	}
	
	function checkLength(jsText){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid Customer ID ");
			return false;
		}	
		return true;	
	}
	
/*** gtky search start  ****/
function fnCallSearch(){
	document.forms[0].action="searchDevice.do?method=searchDevice";
	document.forms[0].submit();		
}
function fnCallEdit(jsDeviceIdValue) {
	document.forms[0].action="editDeviceReg.do?method=editDeviceDetails&deviceId="+jsDeviceIdValue;
	document.forms[0].submit();

}
function fnCallSelect(jsCustIdValue,jsCustName) {
	document.forms[0].action="gtkyRetrieveCustomer.do?method=retrieveCustomerByCustId&cid="+jsCustIdValue+"&custName="+jsCustName;
	document.forms[0].submit();

}
/********gtky search end *****/	
	
</script>

<html:form action="searchDevice" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
     <td width="100%" background="images/top_nav_middlebg.png" align="left">
	<ul>
		<li>You navigated from :</li>
		<li>Airline</li>
		<li>></li>
		<li>Search Device</li>
		</ul>
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td>
       
        <html:select property="deviceSearchBy" styleClass="textarea2">	
						 <html:option value="All">All</html:option>							  		
	 		  			<html:option value="devSerialNo">Device Serial No</html:option>
						<html:option value="flightNo">Tail No</html:option>
												
	               		 </html:select>	</td>
        <td class="lable">Search Value :</td>
        <td><html:text property="deviceSearchValue" styleClass="input" onkeydown="javascript:triggerEvent();"/></td>
        <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="deviceInfo" scope="request"> 
		 <logic:notEmpty name="deviceInfo">	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" class="broder_top0">
              <tr>
                <td height="32" nowrap="nowrap" class="table_header">Airline Name</td>
                <td class="table_header" nowrap="nowrap">Device Type</td>
                <td class="table_header" nowrap="nowrap">Device Serial No</td>              
                <td class="table_header" nowrap="nowrap">Device Model</td>
				<td class="table_header" nowrap="nowrap">Tail No</td>
                <%--<td class="table_header" nowrap="nowrap">Action</td>--%>
              </tr>
              <logic:iterate id="DeviceInfo" name="deviceInfo">
              <tr class="tdbg">
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="airName"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceType"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceSerialNo"/></td>
                <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="deviceModel"/></td>
				 <td align="left" nowrap="nowrap" bgcolor="#FFFFFF"><bean:write name="DeviceInfo" property="flightNo"/></td>
               <%-- <td><a href="javascript: void fnCallEdit('<bean:write name='DeviceInfo' property='deviceId'/>')" class="editdelete"><img src="images/editicon.gif" alt="Edit Item" width="14" height="15" hspace="3" border="0" align="absmiddle" longdesc="#" /></a></td>--%>
              </tr>
             </logic:iterate>
            </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present></td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


</html:form>
