<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "shortDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
				var key= ed.selection.select(e.keyCode);
				if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
					var str=ed.getContent();
					str=(stripTags(str)); 
					str=trim(str); 
					var textLen = 128;
					if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
						textLen = 50;
					//alert(textLen);
					if(textLimit(str.length,textLen,"Short Description A"))
						return true; 
				}	
		  }); 
		return false;
	  } 
});

/****Offer Description B****/

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "longDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
			var key= ed.selection.select(e.keyCode);
			if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
				var str=ed.getContent();
				str = stripTags(str);
				str=trim(str); 
				var textLen = 128;
				if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
					textLen = 60;
				if(textLimit(str.length,textLen,"Long Description")) 
					return true; 
			}		
		  }); 
		return false;
	  } 
});

function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }



function fnCallSaveProduct(){
	/*var shortDescValue=tinyMCE.get('shtDesc').getContent();	
	shortDescValue=(stripTags(shortDescValue));
	
	var longDescValue=tinyMCE.get('longDesc').getContent();	
	longDescValue=(stripTags(longDescValue));  */  
	
	if(document.forms[0].cateId.value==""){
		alert("Please select Category");
		document.forms[0].cateId.focus();	
		return;
	}
	else if(document.forms[0].prodTitle.value==""){
		alert("Please enter Item Name");
		document.forms[0].prodTitle.focus();	
		return;
	}
	else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}
	else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Vendor Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].shortDesc.value==""){
		alert("Please enter Short Description");
		document.forms[0].shortDesc.focus();	
		return;
	}
	else if(document.forms[0].longDesc.value==""){
		alert("Please enter Long Description");
		document.forms[0].longDesc.focus();	
		return;
	}else if(trim(document.forms[0].sbhComment.value)==""){
		alert("Please enter Comments");
		document.forms[0].sbhComment.focus();	
		return;
	}else{
		if(document.forms[0].vendPrice.value.charAt(0)=="$")
			document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
	
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
	
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');

	document.forms[0].action="updateVendorProduct.do?method=updateVendorProductDetails";
	document.forms[0].submit();
	}
	
}

function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}
function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of 250 chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}
</script>


<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
   <td width="100%" background="images/top_nav_middlebg.png" align="left">		
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item </li>
		</ul>
	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="updateVendorProduct" method="post" enctype="multipart/form-data">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Item  Information</h2></td>
        </tr>
      <tr>
        <td align="center"><table border="0" cellpadding="0" cellspacing="4" class="tablecontent">
          <tr class="tdbg">
            <td align="left" class="lable">Vendor Name</td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
            <logic:present name="loginInfo" scope="session">
			<bean:write  name="loginInfo" property="userName" />	
			<input type="hidden" name="ownerId" value="<c:out value='${loginInfo.refId}'/>"/>		
			</logic:present>		
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Category</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
            
		   <html:select property="cateId" styleClass="textarea2">
                  <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Item Name</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="prodTitle" styleClass="input" tabindex="3"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Brand Name</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="brandName" styleClass="input" tabindex="12"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
               <bean:write  name="vendorProductDetails" property="prodCode"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="inShopStatus" styleClass="textarea2">
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable">Retail Price</td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td class="lable">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr valign="top" class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Short Description</span></td>
            <td class="lable">:</td>
            <td colspan="4" align="left"><textarea name="shortDesc" rows="4" onkeyup="textLimit(this.form.shortDesc,this.form.shortDesclen,500,'Short Description');"  id="shtDesc" style="width:520px;" ><logic:present name="vendorProductDetails"><logic:notEmpty name="vendorProductDetails"><bean:write  name="vendorProductDetails" property="shortDesc"/></logic:notEmpty></logic:present></textarea></td>
            </tr>
          <tr valign="top" class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Long Description</span></td>
            <td class="lable">:</td>
            <td colspan="4" align="left"><textarea name="longDesc" rows="4" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Long Description');"  id="longDesc" style="width:520px;" ><logic:present name="vendorProductDetails"><logic:notEmpty name="vendorProductDetails"><bean:write  name="vendorProductDetails" property="longDesc"/></logic:notEmpty></logic:present></textarea></td>
            </tr>
		 
        <tr align="left" >
        <td colspan="6" valign="top" class="lable">
		  <table width="100%" align="center" cellpadding="0" cellspacing="0"  class="broder_top0">
		   <tr class="tdbg">
            <td colspan="2" class="tdtop">&nbsp;</td>
            </tr>
			<tr>
		  		<td rowspan="3">
		<div align="center"><img src="<c:out value='${vendorProductDetails.mainImgPath}' />" width="63" height="79" />
		<br/>
		</div>
			  <div align="center"><bean:write  name="vendorProductDetails"  property="mainImgCap" /></div></td>
            <td width="85%" align="left" nowrap="nowrap" class="lable">Main Image </td>
            </tr>
			<tr>
			  <td width="85%" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadMainImagePath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
			  </tr>
		   
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"> Caption </td>
            </tr>
          <tr class="tdbg">
            <td>&nbsp;</td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadMainImgCap" styleClass="caption_input"/></td>
            </tr>
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
            <td colspan="2" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
		  	<td rowspan="3"><c:if test="${vendorProductDetails.view1ImgPath ne ''}">
		  	  <div align="center"><img src="<c:out value='${vendorProductDetails.view1ImgPath}' />" width="63" height="79" /><br/>
		  	        </div>
		  	  <div align="center"><bean:write  name="vendorProductDetails"  property="view1ImgCap" /></div></c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"> View1 </td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView1ImagePath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable">Caption1</td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view1ImgPath ne ''}">Remove <html:checkbox property="deleteView1Img" value="yes" styleClass="checkbox"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView1ImgCap" styleClass="caption_input"/></td>
            </tr>
          <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3"><c:if test="${vendorProductDetails.view2ImgPath ne ''}">
		    <div align="center"><img src="<c:out value='${vendorProductDetails.view2ImgPath}' />" width="63" height="79" /><br/>
		        </div>
		   <div align="center"> <bean:write  name="vendorProductDetails"  property="view2ImgCap" />	</div>	  </c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View2</td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView2ImagePath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"> Caption2</td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view2ImgPath ne ''}">Remove <html:checkbox property="deleteView2Img"  value="yes" styleClass="checkbox"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView2ImgCap" styleClass="caption_input"/></td>
            </tr>
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3"><c:if test="${vendorProductDetails.view3ImgPath ne ''}">
		    <div align="center"><img src="<c:out value='${vendorProductDetails.view3ImgPath}' />" width="63" height="79" /><br/>
		        </div>
		    <div align="center"><bean:write  name="vendorProductDetails"  property="view3ImgCap" /></div>
		  </c:if>             </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View3<br/></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView3ImagePath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span>             </td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable">Caption3 </td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view3ImgPath ne ''}">Remove <html:checkbox property="deleteView3Img"  value="yes" styleClass="checkbox"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"> <html:text property="uploadView3ImgCap" styleClass="caption_input"/></td>
            </tr>
          
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          </table>			
		<p>&nbsp;</p></td>
            </tr>
			 <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left">
			<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="sbhComment" rows="5"  tabindex="14" style="width:520px;" onKeyUp="textLimit(this.form.sbhComment,this.form.commentlen,250,'Comments');"><logic:present name="vendorProductDetails"><logic:notEmpty name="vendorProductDetails"><bean:write  name="vendorProductDetails" property="sbhComment"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/>	
			
			</td>
			 </tr>
			
			
			
      </table>
      </tr>
      <tr>
             <td height="50" colspan="3" align="center"><html:button property="method" styleClass="button" 
		   onclick="javascript:fnCallSaveProduct()">Save </html:button> 	
          <html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="28"> Cancel </html:button></td>

       
      </tr>
	   <tr>
        <td height="50" colspan="3" align="center" class="help"><table width="80%" border="0" cellpadding="2">
            <tr>
              <td valign="top"><strong>Note:</strong> </td>
              <td align="left">The image uploaded should have a minimum resolution of 175dpi 
                and the minimum dimension of 920 x 1380 pixels.</td>
            </tr>
          </table>
          </td>
      </tr>
    </table>
	<html:hidden property="prodId"/>	
<html:hidden property="ownerId"/>
<html:hidden property="prodCode"/>
<html:hidden property="imgType"/>


</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
