<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<html:form action="addAirlineReg" method="post" >

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="images/top_nav_middlebg.png">
	<!--Admin -->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Airline</li>
		</ul>
		</c:if>
		</logic:present>
		
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'  && mode eq 'airlineAdd'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Airline</li>
		</ul>
		</c:if>
		</logic:present>
		<!--Airline-->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'airline' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Airline</li>
		<li>></li>
		<li>Edit My Info</li>
		</ul>
		</c:if>
		</logic:present>	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">

	<table width="650" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Airline Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="3" class="broder_top0">

      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable">Airline Name </td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airName" />
        </span></td>
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name  </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airContactName"/>
        </span></td>
      </tr>
 
       <tr class="tdbg">
         <td width="120" align="left"  valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Company Address</span></td>
         <td width="10" align="left"  valign="top" class="lable">:</td>
         <td align="left" nowrap="nowrap" style="word-wrap:break-word;width:150px;" valign="top"><span class="catagory">
           <bean:write  name="airlineDetails" property="airAddr1" />
         </span></td>
         <td width="120" align="left"  valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Additional Address</span></td>
         <td width="10" align="left"  valign="top" class="lable">:</td>
         <td align="left" nowrap="nowrap" style="word-wrap:break-word;width:150px;" valign="top"><span class="catagory">
           <bean:write  name="airlineDetails" property="airAddr2" />
         </span></td>
       </tr>
       <tr class="tdbg">
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
         <td width="10" align="left" class="lable">:</td>
         <td align="left"><span class="catagory">
           <bean:write  name="airlineDetails" property="airCity" />
         </span></td>
         <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">State </span></td>
         <td width="10" align="left" class="lable">:</td>
         <td align="left"><span class="catagory">
         
			  <logic:iterate id="states" name="StateList" scope="application">
			<c:if
				test="${states.stateCode eq airlineDetails.airState}">
				<bean:write name="states" property="stateName" />
			</c:if>
			</logic:iterate>	
	         </span></td>
       </tr>
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airCountry" />
        </span></td>
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airZip"/>
        </span></td>
      </tr>
      <tr class="tdbg">
         <td width="120" align="left" nowrap="nowrap" class="lable">Phone1 <br></td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap="nowrap"><span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhone1" />
              </span></td>
              <c:if test="${!empty airlineDetails.airPhoneExt1 && airlineDetails.airPhoneExt1 ne ''}">
              <td class="lable">Ext1 :</td>
              <td nowrap="nowrap"><span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhoneExt1" />
              </span></td>
              </c:if>
            </tr>
        </table></td>
        <c:if test="${!empty airlineDetails.airPhoneExt1 && airlineDetails.airPhoneExt1 ne ''}">
         <td width="120" align="left" nowrap="nowrap" class="lable">Phone2 </td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap="nowrap"><span class="catagory">
                <bean:write  name="airlineDetails" property="airPhone2" />
              </span></td>
			  <c:if test="${!empty airlineDetails.airPhoneExt2 && airlineDetails.airPhoneExt2 ne ''}">
              <td class="lable" nowrap="nowrap">Ext2 :</td>
              <td><span class="catagory">
                 <bean:write  name="airlineDetails" property="airPhoneExt2" />
              </span></td>
			  </c:if>
			  </tr>
        </table></td>
        </c:if>
      </tr>
      <tr class="tdbg">
        <td width="120" align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Fax</span></td>
        <td width="10" align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="airlineDetails" property="airFax"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Email </span></td>
        <td align="left" class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <bean:write  name="airlineDetails" property="airEmail" />
        </span></td>
      </tr>   
	   <tr class="tdbg">
        <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Restrictions/Validity/Cancellation</span></td>
        <td width="10" align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left" style="word-wrap:break-word;width:420px;" valign="top"> <bean:write  name="airlineDetails" property="airReturnPolicy"/></td>
        </tr>   
      <tr class="tdbg">
        <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
        <td width="10" align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left" style="word-wrap:break-word;width:420px;" valign="top"> <bean:write  name="airlineDetails" property="airComments"/></td>
        </tr>
      
    </table>
      </tr>
      <tr>
         <td height="50" colspan="3" align="center">
<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none"> &nbsp;&nbsp;&nbsp; OK&nbsp;&nbsp;&nbsp;   </html:link> </td>
       
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


</html:form>
