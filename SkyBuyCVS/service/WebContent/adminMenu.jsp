<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script src="../js/SpryMenuBar.js" type="text/javascript"></script>
<link href="../style/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />

<div id="top_menu">
	<logic:present name="adminLoginInfo" scope="session">
	<c:if test="${adminLoginInfo.userType eq 'admin'}">
	    <ul id="MenuBar1" class="MenuBarHorizontal">
	  		<li id="home"><a href="preEntry.do?method=preEntry" title="Back to home">Home</a></li>
			<li id="admin"><a href="#" title="Admin" class="MenuBarItemSubmenu">Admin</a>
				<ul id="admin-menu">
					<li><a href="#" title="Vendor" class="MenuBarItemSubmenu">Vendor</a>
						<ul id="admin-submenu">
				          	<li><a  href="initAddVendor.do?method=initAddVendor" title="Add Vendor">Add Vendor</a> </li>
							<li><a  href="initSearchVendor.do?method=initSearchVendor" title="Edit/Search Vendor">Edit/Search Vendor</a></li>
				        </ul>
					</li>
					<li><a href="#" title="Vendor" class="MenuBarItemSubmenu">Air Charter</a>
						<ul id="admin-submenu">
				          	<li><a  href="initAddAirlineReg.do?method=initAddAirlineReg" title="Add Air Charter">Add Air Charter</a></li>
							<li><a  href="initSearchAirline.do?method=initSearchAirline" title="Edit/Search Air Charter">Edit/Search Air Charter</a></li>
				        </ul>
					</li>	
					<li><a href="#" title="Device" class="MenuBarItemSubmenu">Device</a>
						<ul id="admin-submenu">
				          	<li><a  href="initAddDeviceReg.do?method=initAddDeviceReg" title="Add Device">Add Device</a>  </li>
							<li><a  href="initSearchDevice.do?method=initSearchDevice" title="Edit/Search Device">Edit/Search Device</a> 	</li>
							<li ><a  href="deviceAttendance.do?method=initSearchDeviceAttendance" title="Device Attendance">Device Attendance</a> 	</li>
				        </ul>
					</li>
					<li><a href="#" title="Customer" class="MenuBarItemSubmenu">Customer</a>
						<ul id="admin-submenu">
				          	<li ><a class="menuItem" href="airlineAdvt.do?method=initSearchAirlineAdvt" title="Search Customer Contact Info">Search Customer Contact Info</a> 	</li>
							<li ><a class="menuItem" href="custFeedback.do?method=initCustFeedbackSearch" title="Customer Feedback">Customer Feedback</a> 	</li>
				        </ul>
					</li>
					<li><a href="#" title="Merchandise" class="MenuBarItemSubmenu">Merchandise</a>
						<ul id="admin-submenu">
				          	<li><a  href="merchandisePriority.do?method=initSearchMerchandizePriority" title="Select Cover Photos">Select Cover Photos</a> </li>
							<li><a  href="initSetPriorityToMerchandize.do?method=initSetPriorityToMerchandize" title="Prioritize Merchandise">Prioritize Merchandise</a> </li>
							<li><a  href="initUploadSkybuyCatalogue.do?method=initUploadSkybuyCatalogue" title="Upload Skybuy Catalgoue">Upload Skybuy Catalgoue</a> </li>
							<li><a  href="initSearchSkybuyCatalogue.do?method=searchSkyBuyCatalogueDetails" title="Search Skybuy Catalgoue">Search Skybuy Catalgoue</a> </li>
				        </ul>
					</li>
					<li><a href="#" title="Installation" class="MenuBarItemSubmenu">Installation</a>
						<ul id="admin-submenu">
							<li><a  href="initUploadInstallationPackage.do?method=initUploadInstallationPackage" title="Upload Installation Package">Upload Installation Package</a> </li>
							<li class="last"><a  href="initSearchInstallationPackage.do?method=initSearchInstallationPackage" title="Search Installation Package">Search Installation Package</a> </li>
						</ul>
					</li>
			   </ul>	
			</li>
			
			<li id="catalogue"><a href="#" title="Merchandise" class="MenuBarItemSubmenu">Merchandise</a>
				<ul id="catalogue-menu">
					<li><a  href="specialProduct.do?method=initAddSpecialProducts" title="Add Checkout Product">Add Checkout Product</a> </li>
					<li><a  href="searchSpecialProduct.do?method=initSearchSpecialProducts" title="Edit/Search Checkout Product">Edit/Search Checkout Product</a> </li>
					<li><a  href="initEditSearchMerchandize.do?method=initEditSearchMerchandize" title="Edit/Search Merchandise">Edit/Search Merchandise</a> </li>
					<li><a  href="initSearchAirlineAdvtDetails.do?method=initSearchAirlineAdvtDetails" title="Edit/Search Air Charter Package">Edit/Search Air Charter Package</a> </li>
					<li><a  href="initSearchMerchandize.do?method=initSearchMerchandize&MerchandizeAction=Approve" title="Accept/Reject Merchandise">Accept/Reject Merchandise</a> </li>
				</ul>	
			</li>
			
			<li id="order"><a href="#" title="Order" class="MenuBarItemSubmenu">Order</a>
				<ul id="order-menu">
					<li><a  href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>
					<li><a  href="initSearchOrderSummary.do?method=initSearchOrderSummary" title="Search Order Summary">Search Order Summary</a></li>
					<li  class="last"><a  href="initPlaceOrder.do?method=initPlaceOrder" title="Place Order">Place Order</a></li>		
				</ul>
			</li>
		
			<li id="reports"><a href="#" title="Reports/Statements" class="MenuBarItemSubmenu">Reports/Statements</a>
				<ul id="reports-menu">	
					<li><a class="menuItem" href="orderReport.do?method=initOrderReport" title="Order Report">Order Report</a></li>
					<li class="last"><a class="menuItem" href="airlineCommReport.do?method=initAirlineCommReport" title="Commission Statement">Commission Statement</a></li>		
					
				</ul>
			</li>
		</ul> 
	</c:if>
	</logic:present>
</div>
</td>
</tr>