<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script src="js/datepicker.js" type=text/javascript></script>
<link media=screen href="style/datepic.css" type=text/css rel=stylesheet>
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	theme_advanced_buttons1 : "",
	plugins : "noneditable",			
	theme_advanced_toolbar_location : "none",
	theme_advanced_toolbar_align : "none"
	
});
function isEmpty(frm_fld){

		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormat(datevar);
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
		
}

function dateFormat(sDate){
	var parseYr;
	var yl=1990;
	var ym=2200;
		if(sDate.length==8 || sDate.length==10  ){
			if(sDate.length==10){
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
		   /*else if(sDate.length==6){
				var yTemp = sDate.substring(4,6);				
				y="20"+yTemp;
				m = sDate.substring(0,2);				
				d = sDate.substring(2,4);				
			}	*/
	if (y<yl || y>ym) {
		parseYr = y+""+m+""+d;
		return parseYr;
	}
	else
		return sDate;
	}
	return sDate;
}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
	fnCallSubmit();
	}           
}

function fnCallSearch(){

	
	/*if(document.forms[0].catalogueDate.value == ""){
			
		alert("Please enter valid Catalogue Date ");
		document.forms[0].catalogueDate.focus();			
		return false;
	}
	else if(!isDate(document.forms[0].catalogueDate.value)){	
		alert('d2');			
		alert("Please enter valid Catalogue Date ");
		document.forms[0].catalogueDate.focus();			
		return false;
	}
	else{	*/
		
		document.forms[0].action="searchAirlineCatalogue.do?method=searchAirlineCatalogue";
		document.forms[0].submit();			
/*	}*/
	
}
function fnCallEdit(jsProdIdValue) {
	document.forms[0].action="editAirlineProduct.do?method=editAirlineProductDetails&ProdId="+jsProdIdValue;
	document.forms[0].submit();

}
function fnCallDelete(jsProdIdValue) {
	var flag=false;
	flag=confirm('Are you sure you want to delete this Item');
	if(flag){
	document.forms[0].action="deleteProduct.do?method=deleteProductDetails&ProdId="+jsProdIdValue;
	document.forms[0].submit();
	}

}

/********gtky search end *****/	
	
</script>

<html:form action="searchAirlineCatalogue" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="images/top_nav_middlebg.png">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item</li>
		</ul>
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td align="center" class="lable"><table border="0" cellpadding="2" cellspacing="0">
            <tr>
              <td nowrap="nowrap" class="lable">Show in Catalogue </td>
              <td class="lable">:</td>
              <td><html:select property="prodStatus" styleClass="textarea2">
						<html:option value="ALL">All</html:option>
						<html:option value="A">Yes</html:option>
	 		  			<html:option value="I">No</html:option>
    </html:select></td>
              <td nowrap="nowrap" class="lable">Approval Status</td>
              <td class="lable">:</td>
              <td><html:select property="sbhProdStatus" styleClass="textarea2">
						<html:option value="ALL">All</html:option>
						<html:option value="N">Pending</html:option>
	 		  			<html:option value="A">Accepted</html:option>
						<html:option value="R">Rejected</html:option>
    </html:select></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center"  class="lable"><table border="0" cellpadding="2">
            <tr>
              <td class="lable">Upload Date</td>
              <td class="lable">:</td>
              <td class="lable">From</td>
              <td><html:text property="uploadFromDate"  styleClass="search-input" />
         <img  src="images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('uploadFromDate', false, 'mdy', '/');" /></td>
              <td class="lable">To</td>
              <td><html:text property="uploadToDate"  styleClass="search-input" />
          <img  src="images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('uploadToDate', false, 'mdy', '/');" /></td>
              <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td"  align="center">
	<logic:present name="catelogueInfo" scope="request"> 
  <logic:notEmpty name="catelogueInfo">	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td  colspan="3" class="tablehead" background="images/header_bg.gif" align="center"><h2>Item  Information</h2> </td>
        </tr>
      <tr>
        <td><table width="750" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" class="broder_top0">
          <tr>
            <td height="32" class="table_header">Item</td>
            <td class="table_header">Description </td>
            <td class="table_header" nowrap="nowrap">Price</td>           
            <td class="table_header" nowrap="nowrap">Show in Catalogue</td>
			<td class="table_header" nowrap="nowrap">Approval Status</td>
			 <td nowrap="nowrap" class="table_header">Last Approval Date</td>
			<td class="table_header"  width="60px;" align="center">Comments</td>
            <td class="table_header">Action</td>
          </tr>
		  <logic:iterate id="CatelogueInfo" name="catelogueInfo">
          <tr class="tdbg">
            <td align="left" valign="top" class="catagory"><div align="center">
			
			
			<img src='<bean:write name="CatelogueInfo" property="mainImgPath"/>' alt="BGOS8_Y04SK" width="79" height="79" />
            </div>
              <div class="lable"><bean:write name="CatelogueInfo" property="prodCode"/></div>			  </td>
            <td align="left"><div class="lable"><bean:write name="CatelogueInfo" property="brandName"/></div>
                <div class="lable"><bean:write name="CatelogueInfo" property="prodTitle"/></div>
              <div class="desc">
              <div style="width:270px;height: 20px" >
						
						 <logic:present name="CatelogueInfo">
							<logic:notEmpty name="CatelogueInfo">
						 		 <c:out value="${CatelogueInfo.shortDesc}" escapeXml="false"/>
								 
							 </logic:notEmpty>
					  	</logic:present>	
					  	
			</div>
             
              
              
              </div></td>
            <td align="right" valign="middle" nowrap="nowrap"><div class="lable"><fmt:setLocale value="en_US" /><fmt:formatNumber value="${CatelogueInfo.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>	</div>              </td>           
            <td align="center"><div class="lable"><bean:write name="CatelogueInfo" property="inShopStatus"/></div></td>
			<td><div class="lable"><bean:write name="CatelogueInfo" property="sbhProdStatus"/></div></td>
			 <td align="center"><div class="lable"><bean:write name="CatelogueInfo" property="adminApprovalDt"/></div></td> 
			<td ><div class="lable"  width="60px;" align="left"><bean:write name="CatelogueInfo" property="sbhComment"/></div></td>
            <td nowrap="nowrap" align="center" class="action"><div class="lable"><a href="javascript: void fnCallEdit('<bean:write name='CatelogueInfo' property='prodId'/>')" class="editdelete" title="Edit Item">Edit</a>&nbsp;&nbsp;&nbsp;<a href="javascript: void fnCallDelete('<bean:write name='CatelogueInfo' property='prodId'/>')" class="editdelete" title="Delete Item">Delete</a> </div></td>
          </tr>
		   </logic:iterate>
		    
		
      </table></td>
      </tr>
      
    </table>
	<tr><td class="td" colspan="3">&nbsp;</td></tr>
  <tr><td class="td" colspan="3" align="center"><html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button></td></tr>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
	</td>
    </tr>
	 
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png" align="center"></td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</html:form>