<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script>
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

function fnCallAddDevice(){
	document.forms[0].action="addDeviceReg.do?method=addDeviceRegDetails";
	document.forms[0].submit();
}
function fnCallEditDevice(){
	document.forms[0].action="updateDeviceReg.do?method=updateDeviceDetails";
	document.forms[0].submit();
}

</script>




<html:form action="addDeviceReg" method="post" >

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="images/top_nav_middlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">

	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td  colspan="3"class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="0" class="broder_top0">

      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable">Air Code</td>
        <td align="left" class="lable">:</td>
        <td><span class="catagory">
          	  
		  <logic:iterate id="airline" name="airCodeList"scope="request">
				<c:if test="${airline.airId eq deviceDetails.airId}">
					<bean:write name="airline" property="airCode" />
				</c:if>
			</logic:iterate>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Type</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="deviceDetails" property="deviceType"/>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Serial No</span></td>
        <td align="left" class="lable">:</td>
        <td><span class="catagory">
          <bean:write  name="deviceDetails" property="deviceSerialNo"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Model</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <bean:write  name="deviceDetails" property="deviceModel"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Mac Address</span></td>
        <td align="left" class="lable">:</td>
        <td><span class="catagory">
         <bean:write  name="deviceDetails" property="macAddr"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Processor Id</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <bean:write  name="deviceDetails" property="processorId"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Mac Description</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4"> <bean:write  name="deviceDetails" property="macDesc"/>	</td>
        </tr>
      
    </table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> <html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none">OK</html:link> </td>
       
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


<html:hidden property="deviceId"/>
</html:form>
