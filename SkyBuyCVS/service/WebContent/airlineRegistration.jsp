<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;   
}
function validateContactEmail(str){
	var str=document.forms[0].airEmail.value;
	isValid=true;
	var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	if (!regExp.test(str)){
		isValid=false
		alert("Contact Email is an invalid e-mail address.")	
	}
	return (isValid)
}	
function getValidTelephone(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp.length != 10){		
		return "";
	}
	if (phTemp=="0000000000"){		
		return "";
	}
	return phTemp;
}
function getValidZip(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp=="00000"){		
		return "";
	}
	return phTemp;
}	
function fnCallAddAirline(){

<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'}">
		
		var userId = document.forms[0].airUserId.value;
		if(userId.length<3){	
			alert("Please enter Airline User Id more than 3 characters");
			document.forms[0].airUserId.focus();
			return false;
		}
		
		var password = document.forms[0].airPassword.value;
		if(password.length<3){	
			alert("Please enter Airline Password more than 3 characters");
			document.forms[0].airPassword.focus();
			return false;
		}	
	   </c:if>
</logic:present>
	var phoneNo;	
	phoneNo = getValidTelephone(document.forms[0].airPhone1.value)
	
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].airPhone1.focus();
		return false;
	} else {	
		document.forms[0].airPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].airPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension1 should be valid.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>=6){
			alert("Extension1 should not be more than 6 digits.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		} 
	}
	
	
	var phoneNo2=document.forms[0].airPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Phone Number2 should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airPhone2.focus();
			return false;
		} else {	
			document.forms[0].airPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].airPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Extension2 should be valid.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>=6){
			alert("Extension2 should not be more than 6 digits.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		}
	}
	
	
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].airFax.value)	
	if (document.forms[0].airFax.value != null && document.forms[0].airFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airFax.focus();
			return false;
		} else {	
			document.forms[0].airFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].airZip.value)	
	if (document.forms[0].airZip.value != null && document.forms[0].airZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].airZip.focus();
			return false;
		} else {	
			document.forms[0].airZip.value = zipNo;
		}
	}
	if(validateContactEmail()){
		document.forms[0].action="addAirlineReg.do?method=addAirlineReg";
		document.forms[0].submit();
	}	
}
function fnCallEditAirline(){
<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'}">
		
			var userId = document.forms[0].airUserId.value;
			if(userId.length<3){	
				alert("Please enter Airline User Id more than 3 characters");
				document.forms[0].airUserId.focus();
				return false;
			}
			
			var password = document.forms[0].airPassword.value;
			if(password.length<3){	
				alert("Please enter Airline Password more than 3 characters");
				document.forms[0].airPassword.focus();
				return false;
			}
	  </c:if>
</logic:present>
	var phoneNo;	
	phoneNo = getValidTelephone(document.forms[0].airPhone1.value)
	
	if(phoneNo==""){
	alert("Phone Number1 is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].airPhone1.focus();
		return false;
	} else {	
		document.forms[0].airPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].airPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension1 should be valid.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		}
		if(phoneNoExt1.length>=6){
			alert("Extension1 should not be more than 6 digits.");
			document.forms[0].airPhoneExt1.focus();
			return false;
		}
	}
	
	
	var phoneNo2=document.forms[0].airPhone2.value ;
	if(trim(phoneNo2)!=""){	
		phoneNo2 = getValidTelephone(phoneNo2);
		if(phoneNo2==""){
		alert("Phone Number2 should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airPhone2.focus();
			return false;
		} else {	
			document.forms[0].airPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].airPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Extension2 should be valid.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		}
		if(phoneNoExt2.length>=6){
			alert("Extension2 should not be more than 6 digits.");
			document.forms[0].airPhoneExt2.focus();
			return false;
		} 
	}
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].airFax.value)	
	if (document.forms[0].airFax.value != null && document.forms[0].airFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].airFax.focus();
			return false;
		} else {	
			document.forms[0].airFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].airZip.value)	
	if (document.forms[0].airZip.value != null && document.forms[0].airZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].airZip.focus();
			return false;
		} else {	
			document.forms[0].airZip.value = zipNo;
		}
	}
	if(validateContactEmail()){
		document.forms[0].action="updateAirlineReg.do?method=updateAirlineDetails";
		document.forms[0].submit();
	}
}
function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of 250 chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}
function fnCancel() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();

}



</script>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%"  align="left" background="images/top_nav_middlebg.png">
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'airlineAdd'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Airline</li>
		</ul>
		</c:if>
	</logic:present>
	
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Airline</li>
		</ul>
		</c:if>
	</logic:present>
	
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'airline' && mode eq 'airlineEdit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Airline</li>
		<li>></li>
		<li>Edit My Info</li>
		</ul>
		</c:if>
	</logic:present>
	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<html:javascript formName="airlineRegForm"/>
<html:form action="addAirlineReg" method="post" onsubmit="return validateAirlineRegForm(this);">
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3"  class="tablehead" align="center"><h2>Airline Setup</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="3" class="broder_top0">
	<c:if test="${!empty airNameairUserIdExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.NameUserId"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty airNameExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.Name"/></span></td>
	</tr>
	</c:if>
	<c:if test="${!empty airUserIdExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error">
		  <bean:message key="airline.UserId"/></span></td>
	</tr>
	</c:if>
      <tr class="tdbg">
		<td align="left" nowrap="nowrap" class="lable">Airline Name*</td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
			<logic:present name="mode">
			 	<logic:equal name="mode"  value="airlineAdd">
         		 <html:text property="airName" styleClass="input" tabindex="1"/>
				</logic:equal>				
			</logic:present>	
			<logic:present name="mode">
			 	<logic:equal name="mode"  value="airlineEdit">
         		 <bean:write  name="airlineDetails" property="airName"/>
				</logic:equal>
			</logic:present>	
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name *
		 </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airContactName"  tabindex="2"/>
        </span></td>
      </tr>
	  	<logic:present name="loginInfo" scope="session">	
			<c:if test="${loginInfo.userType eq 'admin'}">
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Airline User Id *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airUserId" styleClass="input" tabindex="3"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Airline Password *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:password property="airPassword" styleClass="input" tabindex="4"/>
        </span></td>
      </tr>
	    </c:if>
	</logic:present>   
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <html:text property="airAddr1" styleClass="input" tabindex="5"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Additional Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airAddr2" styleClass="input" tabindex="6"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airCity" styleClass="input" tabindex="7"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
    	   <html:select property="airState" styleId="state" tabindex="8">
			 <html:option value="">------Select State------</html:option>
			 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
            </html:select>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="airCountry" styleClass="textarea2" tabindex="9">
			 <html:option value="US">US</html:option>
		  </html:select>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airZip" styleClass="input" tabindex="10"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone1 *</td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="airPhone1" styleClass="input-phone" tabindex="11"/>
        </span></td>
              <td class="lable">Ext1:</td>
              <td><span class="catagory">
                <html:text property="airPhoneExt1" styleClass="input-ext" tabindex="13"/>
              </span></td>
            </tr>
        </table></td>
        <td align="left" class="lable">Phone2 </td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50px;"><span class="catagory">
                <html:text property="airPhone2" styleClass="input-phone" tabindex="13"/>
              </span></td>
              <td class="lable">Ext2:</td>
              <td><span class="catagory">
                <html:text property="airPhoneExt2" styleClass="input-ext" tabindex="13"/>
              </span></td>
            </tr>
        </table></td>
      </tr>
      
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Fax</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airFax" styleClass="input" tabindex="13"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email *</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="airEmail" styleClass="input" tabindex="12"/>
        </span></td>
      </tr>
	  <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable">Restrictions/Validity/Cancellation</td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left">	
        	<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="airReturnPolicy" cols="66" rows="6" tabindex="14" onKeyUp="textLimit(this.form.airComments,this.form.policylen,250,'Return Policy');"><logic:present name="airlineDetails"><logic:notEmpty name="airlineDetails"><bean:write  name="airlineDetails" property="airReturnPolicy"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=policylen size=3 maxlength=3 value="250" class="wordcount"/>        </td>
        </tr>
      
	  
      <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left">	
        	<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="airComments" cols="66" rows="6" tabindex="14" onKeyUp="textLimit(this.form.airComments,this.form.commentlen,250,'Comments');"><logic:present name="airlineDetails"><logic:notEmpty name="airlineDetails"><bean:write  name="airlineDetails" property="airComments"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/>        </td>
        </tr>
      
    </table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> <logic:present name="mode">
			 <logic:equal name="mode"  value="airlineAdd">
	  <html:button property="method" styleClass="button" 
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallAddAirline(); void 0" tabindex="15"> Add </html:button> 
		   <html:button property="method" onclick="javascript:fnCancel();" styleClass="button">Cancel</html:button>
		   </logic:equal> 
		</logic:present>   
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="airlineEdit">
	  <html:button property="method" styleClass="button" 
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallEditAirline(); void 0" tabindex="15">Save </html:button> 
		   <html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button>
		   </logic:equal>
		</logic:present> 
		  
       </td>
       
      </tr>
    </table>
	<html:hidden property="airId"/>
	<logic:present name="mode">
		<logic:equal name="mode"  value="airlineEdit">
			 <html:hidden property="airName"/>
		</logic:equal>
	</logic:present>				
	
<logic:present name="mode">
	<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType ne 'admin' && mode eq 'airlineEdit'}">
			<html:hidden property="airUserId" value="-1"/>
			<html:hidden property="airPassword" value="-1"/>
			
		</c:if>
	</logic:present>
</logic:present> 

</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>



