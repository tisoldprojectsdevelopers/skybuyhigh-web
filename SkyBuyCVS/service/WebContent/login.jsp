<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/main.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript" src="js/menu.js"></script>
<script>
 function triggerEvent() {  
		if(event.keyCode==13) {
		  	fnCallLogin();
		}           
	 }
	
function fnCallLogin(){
		document.forms[0].action="login.do?method=login";
		document.forms[0].submit();
}
function fnCallCustomerLogin(){
		document.forms[0].action="preCustomerLogin.do";
		document.forms[0].submit();
}
window.onload=function() {
	document.forms[0].userId.focus();
}
</script>

<div><html:form action="admin/login" method="post">
<tr><td>
<html:errors/>
<br /><br/>
<div id="page">
  <div id="logincontainer">
<h1>Login</h1>
<div class="loginbox">
<div class="error" align="center">
<html:messages id="loginErrorMsg" property="loginFailed" message="true">	
		<bean:write name="loginErrorMsg"/>
</html:messages></div>

<div class="lable">User Name :&nbsp;</div>
	<html:text property="userId" styleClass="input" tabindex="1"/>
<div class="lable">Password :&nbsp;</div>
	<html:password property="password" styleClass="input" tabindex="2"  onkeydown="javascript:triggerEvent();"/>

<div class="login"><a href="javascript:fnCallLogin()" >Login</a></div>
<div class="customer">SkyBuy<sup>High</sup> Customer Please <a href="javascript:fnCallCustomerLogin()">Click Here</a>  to Login</div>
</div>
</div>
</div>
</td></tr>
</html:form></div>
</div>
</div>