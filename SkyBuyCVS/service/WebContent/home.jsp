<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/>    
<script type="text/javascript" src="swfobject.js"></script>
<script src="AC_RunActiveContent.js" language="javascript"></script>
<%
String path = request.getContextPath();
String basePath = "http://"+request.getServerName()+path+"/";

%>
<script>
function getCatalogue(link, windowname){
	var sURL = '<%=basePath%>';
	sURL += link
	window.open(sURL, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');	
}
</script>
		<tr><td>
		<div class="coverflowcontainer">
		<h1>Home Page of  <logic:present name="loginInfo" scope="session"><bean:write  name="loginInfo" property="userName" /></logic:present></h1>
			
			 
		<div class="container">
		<div class="orderdetails">
		<logic:present name="adminLoginInfo" scope="session">
			<c:if test="${adminLoginInfo.userType eq 'admin'}">
				<h1>Order Details</h1>		
				<ul>
				<logic:present name="compltedOrders" scope="session">
					<logic:notEmpty name="compltedOrders" scope="session">
						<c:choose>
							<c:when test="${compltedOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=C"><c:out value="${compltedOrders}"/> 
						Successfully Completed Order(s)</a></li>
						 </c:when>
						 <c:otherwise>						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="pendingOrders" scope="session">
					<logic:notEmpty name="pendingOrders" scope="session">
						<c:choose>
							<c:when test="${pendingOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=P"><c:out value="${pendingOrders}"/> 
						Pending Order(s)</a></li>
						</c:when>
						 <c:otherwise>
								<li>No Pending Order
								  <logic:present name="incompleteOrders" scope="session">
								    <logic:notEmpty name="incompleteOrders" scope="session">
								      <c:choose>
								        <c:when test="${incompleteOrders ne '0'}"></c:when>
							          </c:choose>
							        </logic:notEmpty>
							      </logic:present>
								</li>
						  </c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				<logic:present name="incompleteOrders" scope="session"><logic:notEmpty name="incompleteOrders" scope="session"><c:choose><c:when test="${incompleteOrders ne '0'}"><li><a href="searchOrder.do?method=searchOrder&OrderStatus=F"><c:out value="${incompleteOrders}"/> 
					  Incomplete Order(s)</a></li>
						</c:when>
				  <c:otherwise>
						 <li>No Incomplete Order
						   <!--<li><a href="#">Click here for Mange your Orders</a></li>-->
				</li>
				  </c:otherwise>
				</c:choose>
				</logic:notEmpty>
				</logic:present>
				</ul>
				<hr/>
				<h1>Catalogue Details (last 7 days)</h1>		
				<ul>
				<logic:present name="pendingCatalogue" scope="session">
					<logic:notEmpty name="pendingCatalogue" scope="session">
						<c:choose>
							<c:when test="${pendingCatalogue ne '0'}">
							<li><a href="searchMerchandize.do?method=searchMerchandize&MerchandizeAction=Approve&ApprovalStatus=N"><c:out value="${pendingCatalogue}"/> 
							Pending Item(s) </a></li>
							</c:when>
							<c:otherwise>
								<li>No Pending Item </li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="acceptedCatalogue" scope="session">
					<logic:notEmpty name="acceptedCatalogue" scope="session">						
						<c:choose>
							<c:when test="${acceptedCatalogue ne '0'}">
							<li><a href="editSearchMerchandize.do?method=editSearchMerchandize&ApprovalStatus=A"><c:out value="${acceptedCatalogue}"/> 
							Accepted Item(s) </a></li>
							</c:when>
							<c:otherwise>								
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="rejectedCatalogue" scope="session">
					<logic:notEmpty name="rejectedCatalogue" scope="session">
						<c:choose>
							<c:when test="${rejectedCatalogue ne '0'}">
							<li><a href="editSearchMerchandize.do?method=editSearchMerchandize&ApprovalStatus=R"><c:out value="${rejectedCatalogue}"/> 
							Rejected Item(s) </a></li>
							</c:when>
							<c:otherwise>
								<li>No Rejected Item </li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>			
				
				</ul>
					
			</c:if>
		</logic:present>
		<logic:present name="loginInfo" scope="session">
			<c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline'}">
				<h1>Order Details</h1>		
								<ul>
				<logic:present name="compltedOrders" scope="session">
					<logic:notEmpty name="compltedOrders" scope="session">
						<c:choose>
							<c:when test="${compltedOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=C"><c:out value="${compltedOrders}"/> 
						Successfully Completed Order(s)</a></li>
						 </c:when>
						 <c:otherwise>						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="pendingOrders" scope="session">
					<logic:notEmpty name="pendingOrders" scope="session">
						<c:choose>
							<c:when test="${pendingOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=P"><c:out value="${pendingOrders}"/> 
						Pending Order(s)</a></li>
						</c:when>
						 <c:otherwise>
								<li>No Pending Order </li>
						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="incompleteOrders" scope="session">
					<logic:notEmpty name="incompleteOrders" scope="session">
						<c:choose>
							<c:when test="${incompleteOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=F"><c:out value="${incompleteOrders}"/> 
						Incomplete Order(s)</a></li>
						</c:when>
						 <c:otherwise>
								<li>No Incomplete Order </li>
						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>
					
				<!--<li><a href="#">Click here for Mange your Orders</a></li>-->
				</ul>
				<hr/>
				
				
				<h1>Catalogue Details (last 7 days)</h1>		
				<ul>
				<logic:present name="pendingCatalogue" scope="session">
					<logic:notEmpty name="pendingCatalogue" scope="session">
						<c:choose>
							<c:when test="${pendingCatalogue ne '0'}">
								<c:if test="${loginInfo.userType eq 'vendor'}">
									<li><a href="searchVendorCatalogue.do?method=searchVendorCatalogue&ApprovalStatus=N"><c:out value="${pendingCatalogue}"/>									
									Pending Item(s) </a></li>
								</c:if>
								<c:if test="${loginInfo.userType eq 'airline'}">
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=N"><c:out value="${pendingCatalogue}"/>									
									Pending Item(s) </a></li>
								</c:if>
							</c:when>
							<c:otherwise>
								<li>No Pending Item </li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="acceptedCatalogue" scope="session">
					<logic:notEmpty name="acceptedCatalogue" scope="session">						
						<c:choose>
							<c:when test="${acceptedCatalogue ne '0'}">
								<c:if test="${loginInfo.userType eq 'vendor'}">
									<li><a href="searchVendorCatalogue.do?method=searchVendorCatalogue&ApprovalStatus=A"><c:out value="${acceptedCatalogue}"/> 
									Accepted Item(s) </a></li>
								</c:if>
								<c:if test="${loginInfo.userType eq 'airline'}">
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=A"><c:out value="${acceptedCatalogue}"/> 
									Accepted Item(s) </a></li>
								</c:if>
							</c:when>
							<c:otherwise>
								<!--<li>No Accepted Catalogue</li>-->
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="rejectedCatalogue" scope="session">
					<logic:notEmpty name="rejectedCatalogue" scope="session">
						<c:choose>
							<c:when test="${rejectedCatalogue ne '0'}">
								<c:if test="${loginInfo.userType eq 'vendor'}">
									<li><a href="searchVendorCatalogue.do?method=searchVendorCatalogue&ApprovalStatus=R"><c:out value="${rejectedCatalogue}"/> 
									Rejected Item(s) </a></li>
								</c:if>
								<c:if test="${loginInfo.userType eq 'airline'}">
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=R"><c:out value="${rejectedCatalogue}"/> 
									Rejected Item(s) </a></li>
								</c:if>
							</c:when>
							<c:otherwise>
								<li>No Rejected Item 
								  <hr/>
								</li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>			
				</ul>
			</c:if>
		</logic:present>
		<h1>Bottom Line </h1>
		<ul>
		<c:choose>
		<c:when test="${orderPlacedTotAmt ne 0}">
		<li>Total value of the orders placed since last statement $<c:out value="${orderPlacedTotAmt}"/></li>
		</c:when>
		<c:otherwise>
		<li>Total value of the orders placed since last statement $<c:out value="${orderPlacedTotAmt}"/></li>
		</c:otherwise>
		</c:choose>
		</ul>
		</div>
		<div class="divider"><img src="images/h_line.gif" height="275" width="2" /></div>
		<div class="orderdetails">
		
		<c:if test="${adminLoginInfo.userType eq 'vendor' || adminLoginInfo.userType eq 'admin'}">
		<h1>Most Popular Merchandise</h1>
		<hr />
		<logic:present name="popualrMerchandizeInfo" scope="session"> 
		 <logic:notEmpty name="popualrMerchandizeInfo">
		      
				<div class="imgcontainer">
					<logic:iterate id="MerchandizeInfo" name="popualrMerchandizeInfo">	
							<img src="<c:out value='${MerchandizeInfo.mainImgPath}' />" border="0" width="63" height="79"/>
					</logic:iterate>
				</div>
				
				</logic:notEmpty>
				
		</logic:present>
		<logic:notPresent name="popualrMerchandizeInfo" scope="session">
		        
					<img src="images/Infonotavailable.png" border="0" width="250" height="50"/>
				
		</logic:notPresent>
		<hr/>
		</c:if>
		<div class="orderdetails">
		<h2>By clicking the following links you can :</h2>
		<hr />
		<!--For Admin-->
		<logic:present name="adminLoginInfo" scope="session">
			<c:if test="${adminLoginInfo.userType eq 'admin'}">		
			<ul>
			<li><a class="menuItem" href="initAddVendor.do?method=initAddVendor" title="Add Vendor">Add Vendor</a></li>
			<li><a class="menuItem" href="initSearchVendor.do?method=initSearchVendor" title="Edit/Search Vendor">Edit/Search Vendor</a></li>
			<li><a class="menuItem" href="initAddAirlineReg.do?method=initAddAirlineReg" title="Add Air Charter">Add Air Charter</a></li>
			<li><a class="menuItem" href="initSearchAirline.do?method=initSearchAirline" title="Edit/Search Air Charter">Edit/Search Air Charter</a></li>		
			<li><a class="menuItem"href="javascript:getCatalogue('previewCatalogue.jsp','catalogue');" title="View Catalogue">View Catalogue</a> </li>			
			</ul>
			<img src="images/h_line.gif" height="100" width="1" class="divider"/>
			<ul>
			<li><a class="menuItem" href="initEditSearchMerchandize.do?method=initEditSearchMerchandize" title="Edit/Search Merchandise">Edit/Search Merchandise</a></li>
			<li><a class="menuItem" href="initSearchMerchandize.do?method=initSearchMerchandize&MerchandizeAction=Approve" title="Accept/Reject Merchandise">Accept/Reject Merchandise</a></li>
			<li><a href="initSearchOrder.do?method=initSearchOrder" title="View Order">View Order</a></li>			
			<li><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Admin" title="Generate Catalogue">Generate Catalogue</a> </li>			
			</ul>
			</c:if>
		</logic:present>
		
		<!--For Vendor-->
		<logic:present name="loginInfo" scope="session">
			<c:if test="${loginInfo.userType eq 'vendor'}">		
			<ul>			
			<li><a class="menuItem" href="initAddVendorProduct.do?method=initAddVendorProduct" title="Add Item">Add Item</a></li>
			<c:if test="${productCount ne '0'}">

				<li><a class="menuItem"href="javascript:getCatalogue('../previewCatalogue.jsp','catalogue');" title="View Catalogue">View Catalogue</a> </li>
			</c:if>
			<li><a class="menuItem" href="editVendor.do?method=EditVendorDetails&VendId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a></li>
			</ul>
			<img src="images/h_line.gif" height="100" width="1" class="divider"/>
			<ul>	
			<li><a class="menuItem" href="initSearchVendorCatalogue.do?method=initSearchVendorCatalogue" title="Edit/Search Item">Edit/Search Item</a> </li>	
			<c:if test="${productCount ne '0'}">
				<li><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Vendor" title="Generate Catalogue">Generate Catalogue</a> </li>
			</c:if>
			<li><a href="initSearchOrder.do?method=initSearchOrder" title="View Order">View Order</a></li>
			
			</ul>			
			</c:if>
		</logic:present>
		<!--For Airline-->
		<logic:present name="loginInfo" scope="session">
		  <c:if test="${loginInfo.userType eq 'airline'}">		
			<ul>			
			<li><a class="menuItem" href="initAddAirlineProduct.do?method=initAddAirlineProduct" title="Add Item">Add Item</a></li>
			<c:if test="${productCount ne '0'}">
				<li><a class="menuItem"href="javascript:getCatalogue('../previewCatalogue.jsp','catalogue');" title="View Catalogue">View Catalogue</a></li>		
			</c:if>	
			<li><a class="menuItem" href="initSearchDevice.do?method=initSearchDevice" title="Search Device">Search Device</a> </li>
			<li><a class="menuItem" href="editAirlineReg.do?method=editAirlineDetails&airId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a> </li>
			</ul>
			<img src="images/h_line.gif" height="100" width="1" class="divider"/>
			<ul>
				<li><a class="menuItem" href="initSearchAirlineCatalogue.do?method=initSearchAirlineCatalogue" title="Edit/Search Item">Edit/Search Item</a></li>	
				<c:if test="${productCount ne '0'}">
				 <li><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Airline" title="Generate Catalogue">Generate Catalogue</a> </li>	
				 </c:if>		
			<li><a href="initSearchOrder.do?method=initSearchOrder" title="View Order">View Order</a></li>
			</ul>			
			</c:if>
		</logic:present>
		
		</div>
		
		</div>
		</div>
		
<!--<script type="text/javascript">
		// <![CDATA[
			

		
		var so = new SWFObject("SkyBuy.swf", "sotester", "590px", "300px", "9", "#000",false,"'wmode':'transparent'");
		so.addParam("quality", "low");
	    so.addParam("wmode", "transparent");
	    so.addParam("salign", "t");

		so.addVariable("xmlStr","<c:out value='${homePageContent}'/>" );
		so.write("flashcontent");
		

		
		// ]]>
	</script>-->
	

		</div>
		
		
		</div>
		</div>
		</td></tr>

<logic:empty name="loginInfo" scope="session">
  <jsp:include page="login.jsp" flush="true"/>
</logic:empty>