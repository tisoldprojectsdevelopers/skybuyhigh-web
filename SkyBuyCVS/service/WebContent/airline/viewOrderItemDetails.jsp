<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>



<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
	 
</script>

<html:form action="admin/searchAirline.do?method=searchAirline" method="post">
<tr><td>

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">
		<c:if test="${OrderStatus eq 'C'}">
		<ul>
		<li>Successfully Completed Order Details</li>
		</ul>
		</c:if>
		
		<c:if test="${OrderStatus eq 'P'}">
		<ul>
		<li>Pending Order Details</li>
		</ul>
		</c:if>
		
		
		<c:if test="${OrderStatus eq 'F'}">
		<ul>
		<li>Incomplete Order Details</li>
		</ul>
		</c:if>
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="orderItemsDetails" scope="request"> 
		 <logic:notEmpty name="orderItemsDetails">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Items Details</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="1"  bgcolor="#cccccc" class="broder_top0">
              <tr>
			   <c:if test="${loginInfo.userType eq 'admin'}">
			  	<td class="table_header" nowrap="nowrap" >Vendor Name</td>
				</c:if>
               	<td class="table_header" nowrap="nowrap" >Customer Name</td>
                <td height="32" align="center" class="table_header" nowrap="nowrap">Item Code</td>
				 <td height="32" align="center" class="table_header" nowrap="nowrap">Item Name</td>
				  <td height="32" align="center" class="table_header" nowrap="nowrap">Brand Name</td>
                <td class="table_header" nowrap="nowrap" >Category Name</td>
				<td class="table_header" nowrap="nowrap" >Quantity</td>
				<c:if test="${loginInfo.userType eq 'admin'}">
                <td class="table_header" nowrap="nowrap">Price</td>
				</c:if>
                <td class="table_header" nowrap="nowrap">SkyBuy Price</td>
                <td class="table_header"   nowrap="nowrap">Order Date</td>
                         
              </tr>
              <logic:iterate id="OrderItemsDetails" name="orderItemsDetails">
              <tr class="tdbg">
			     <c:if test="${loginInfo.userType eq 'admin'}">
			  	 <td align="left" nowrap="nowrap">&nbsp;<bean:write name="OrderItemsDetails" property="vendorName"/>&nbsp;</td>
				 </c:if>
			 	 <td align="left" nowrap="nowrap">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="custFirstName"/>&nbsp;&nbsp;</td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="prodCode"/>&nbsp;&nbsp;</td>
				<td align="left" nowrap="nowrap">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="itemName"/>&nbsp;&nbsp;</td>
				<td align="left" nowrap="nowrap">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="brandName"/></td>
                <td align="left">&nbsp;&nbsp;
				  <logic:iterate id="category" name="Category"scope="application">
					<c:if test="${category.cateId eq OrderItemsDetails.cateId}">
					  <bean:write name="category" property="cateName" />
					</c:if>				
          		</logic:iterate>
				<c:if test="${OrderItemsDetails.cateId eq '20'}">
					  Private Jet Jaunts
				</c:if>
				
				</td>
                <td align="center">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="qty"/></td>
				<c:if test="${loginInfo.userType eq 'admin'}">
				<td align="right">&nbsp;&nbsp;
				  <fmt:setLocale value="en_US" /><fmt:formatNumber value="${OrderItemsDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
				  </td>
				</c:if>
				<td align="right">&nbsp;&nbsp;
				 <fmt:setLocale value="en_US" /><fmt:formatNumber value="${OrderItemsDetails.sbhPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
				</td>
                <td align="left">&nbsp;&nbsp;<bean:write name="OrderItemsDetails" property="orderCreatedDt"/></td>           
			
              </tr>
             </logic:iterate>
            </table></td>
      </tr>
      
    </table>
	
	</td>
    </tr>
   <tr>
    <td height="30" colspan="3" align="center" valign="bottom" class="td"><html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="18"> Cancel </html:button>			
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
	  <font color="#FF0000" size="-2">No Records Found.</font>        </logic:present></td>
  </tr>
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

</div>

</div>

</td></tr>
</html:form>


