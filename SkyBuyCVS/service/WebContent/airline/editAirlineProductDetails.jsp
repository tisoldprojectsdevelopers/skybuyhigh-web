<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }

function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
	}else {
		return datevar;
	}
}
function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function validatePrice(jsPrice) {
			var retVal=jsPrice;
			var startChar=retVal.substring(0,1);
			if(startChar=="$") { 
				retVal=retVal.substring(1,retVal.length);
			} 
			var startChar=retVal.substring(0,1);
			while(startChar=="0") { 
				retVal=retVal.substring(1,retVal.length);
				startChar=retVal.substring(0,1); 
			} 
			var startChar=retVal.substring(0,1);
			if(startChar==".") { 
				retVal="0"+retVal; 
			} 
			if(retVal>=0.01)
			  return false;
			    else
			    return true;       

}	
function getFile(imagePath,jsField){
	if(imagePath=='')
		return true;
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
		return true;
	} else {
		alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
	}
}

function fnCallSaveProduct(){
	/*var shortDescValue=tinyMCE.get('shtDesc').getContent();
	shortDescValue=(stripTags(shortDescValue)); 
	
	var longDescValue=tinyMCE.get('longDesc').getContent();	
	longDescValue=(stripTags(longDescValue));    
	*/
	if(document.forms[0].prodTitle.value==""){
		alert("Please enter Item Name");
		document.forms[0].prodTitle.focus();	
		return;
	}
	/*else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}*/
	else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Price");
		document.forms[0].vendPrice.focus();	
		return;
	}	
	/*else if(document.forms[0].validFromDate.value!="" && document.forms[0].validToDate.value==""){		
		alert("Please enter Valid To Date");
		document.forms[0].validToDate.focus();	
		return;	
	}
	else if(document.forms[0].validFromDate.value=="" && document.forms[0].validToDate.value!=""){		
		alert("Please enter Valid From Date");
		document.forms[0].validFromDate.focus();	
		return;	
	}*/	
	else if(document.forms[0].shortDesc.value==""){
		alert("Please enter Short Description");
		document.forms[0].shortDesc.focus();	
		return;
	}
	else if(document.forms[0].longDesc.value==""){
		alert("Please enter Full Description");
		document.forms[0].longDesc.focus();	
		return;
	}else if(trim(document.forms[0].sbhComment.value)==""){
		alert("Please enter Comments");
		document.forms[0].sbhComment.focus();	
		return;
	}else{	
	
	if(document.forms[0].vendPrice.value.charAt(0)=="$")
		document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
		
		/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
		document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
		
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
		
		document.forms[0].shortDesc.value=trim(document.forms[0].shortDesc.value);
		document.forms[0].longDesc.value=trim(document.forms[0].longDesc.value);
		
		document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/(\r\n)|(\n)/g, "<br>");
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/(\r\n)|(\n)/g, "<br>"); 
		
		var mainImg = document.forms[0].uploadMainImagePath.value;
		var view1Img = document.forms[0].uploadView1ImagePath.value;
		var view2Img = document.forms[0].uploadView2ImagePath.value;
		var view3Img = document.forms[0].uploadView3ImagePath.value;
		if(getFile(mainImg, 'Main Image') && getFile(view1Img,'View1') && getFile(view2Img,'View2') && getFile(view3Img,'View3')){
			
			/*if(!document.forms[0].validFromDate.value=="" && !document.forms[0].validToDate.value==""){		
				if(validateFromandToDate()){		
					document.forms[0].action="updateAirlineProduct.do?method=updateAirlineProductDetails";
					document.forms[0].submit();
				}
			}else{*/
			      document.forms[0].action="updateAirlineProduct.do?method=updateAirlineProductDetails";
				  document.forms[0].submit();
			
			/*}*/		
		}
	}
	
}
	/*function validateFromandToDate() {
	
	
		var fromDate = trim(document.forms[0].validFromDate.value);
		var toDate = trim(document.forms[0].validToDate.value);
		var today = new Date();
		
		var month=today.getMonth()+1;
		var date=today.getDate();
		
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();
		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDate == ""){
			alert("Please enter Valid To Date ");
			document.forms[0].validToDate.focus();
			return false;
		}		
		if(!((toDate.substring(2,3) == "/" && toDate.substring(5,6) == "/") 
		   || (toDate.substring(2,3) == "-" && toDate.substring(5,6) == "-"))) {
			alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && !((fromDate.substring(2,3) == "/" && fromDate.substring(5,6) == "/") 
		   || (fromDate.substring(2,3) == "-" && fromDate.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && toDate != "") {
			
			isCorrectFromDate = isDate(fromDate);
			isCorrectToDate = isDate(toDate);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].validFromDate.value = dateFormats(fromDate);
				document.forms[0].validToDate.value = dateFormats(toDate);
				if(!validateDate(dateFormats(fromDate),dateFormats(toDate))) {
					alert("From Date should be earlier than To Date");
					return false;
				}else if(!validateDate(today, dateFormats(toDate))) {
					alert("To Date should be greater than or equal to current date");
					return false;
				}else {
					return true;
				}
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
			 	return false;
			}
		}	
	}

	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}*/
	function fnCallPreview(){
		document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
		document.forms[0].submit();
	}
	function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
	}
	
	function fnCallSearchAdvertisement(jsProdId){
		<c:if test="${!empty SourceOfEdit and SourceOfEdit eq 'View'}">
			document.forms[0].action="viewAirlineProduct.do?method=viewAirlineProductDetails&ProdId="+jsProdId;
			document.forms[0].submit();
		</c:if>
		<c:if test="${empty SourceOfEdit or SourceOfEdit ne 'View'}">
			document.forms[0].action="searchAirlineCatalogue.do?method=searchAirlineCatalogue";
			document.forms[0].submit();
		</c:if>
	}
</script>
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item </li>
		</ul>
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
    <logic:present name="ErrMsg">
    <div class="error"><c:out value="${ErrMsg}"/></div>
     </logic:present>
    &nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="vendor/updateVendorProduct" method="post" enctype="multipart/form-data">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3"  class="tablehead" align="center"><h2>Item Information</h2></td>
        </tr>
      <tr>
        <td><table border="0" cellpadding="0" cellspacing="4" bgcolor="#FFFFFF" class="broder_top0">
          <tr class="tdbg">
            <td align="left" class="lable">Air Charter Name</td>
            <td align="left" class="lable">:</td>
            <td align="left"><span class="catagory">
            <logic:present name="airlineLoginInfo" scope="session">
			<bean:write  name="airlineLoginInfo" property="userName" />	
			<input type="hidden" name="ownerId" value="<c:out value='${airlineLoginInfo.refId}'/>" tabindex="1"/>		
			</logic:present>		
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td align="left" class="lable">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Item Name * </span></td>
            <td align="left" class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="prodTitle" styleClass="input" tabindex="2" maxlength="30"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left"><span class="catagory">
            	 <html:text styleId="input" property="prodCode" styleClass="input" tabindex="4" maxlength="16"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable">Price (in US$) * </td>
            <td align="left" class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);" tabindex="3"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status * </span></td>
            <td align="left" class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="inShopStatus" styleClass="textarea2" tabindex="4">
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
              </html:select>
            </span></td>
          </tr>
		<tr class="tdbg">
		 <td align="left" nowrap="nowrap"  class="lable">Pre-Authorize Credit Card</td>
		 <td align="center"  class="lable">:</td>
		 <td  align="left"> <html:checkbox  property="preAuthCC" styleClass="checkboxHTML" value="Y" tabindex="8"/> Yes </td>
		 <td align="left" class="lable" nowrap="nowrap">Approval Status </td>
		 <td class="lable" class="lable">:</td>
		 <td align="left" nowrap="nowrap">
			<c:if test="${airlineProductDetails.sbhProdStatus eq 'N'}">Pending</c:if>
	        <c:if test="${airlineProductDetails.sbhProdStatus eq 'A'}">Accepted</c:if>
	        <c:if test="${airlineProductDetails.sbhProdStatus eq 'R'}">Rejected</c:if>
		 </td>
		</tr>
		<tr class="tdbg">
		  <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Special Instructions </span></td>
		  <td valign="top" class="lable">:</td>
		  <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span><textarea name="instructions"  class="textarea" rows="5" onkeyup="textLimit(this.form.instructions,this.form.instructionslen,500,'Instructions');"  id="instructions" style="width:520px;" tabindex="7"><logic:present name="airlineProductDetails"><logic:notEmpty name="airlineProductDetails"><bean:write  name="airlineProductDetails" property="instructions"/></logic:notEmpty></logic:present></textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="instructionslen" size="3" maxlength="3" value="500" class="wordcount"/></td>
		</tr>
		  
		  
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Title * </span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span><textarea name="shortDesc" class="textarea" rows="5" onkeyup="textLimit(this.form.shortDesc,this.form.shortDesclen,250,'Short Description');"  id="shtDesc" style="width:520px;" tabindex="8"><logic:present name="airlineProductDetails"><logic:notEmpty name="airlineProductDetails"><bean:write  name="airlineProductDetails" property="shortDesc"/></logic:notEmpty></logic:present></textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="shortDesclen" size="3" maxlength="3" value="250" class="wordcount"/></td>
            </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Full Description * </span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><textarea name="longDesc" class="textarea" rows="5" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Full Description');" id="longDesc" style="width:520px;" tabindex="9" ><logic:present name="airlineProductDetails"><logic:notEmpty name="airlineProductDetails"><bean:write  name="airlineProductDetails" property="longDesc"/></logic:notEmpty></logic:present></textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="longDesclen" size="3" maxlength="3" value="500" class="wordcount"/></td>
            </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" align="center" valign="top" nowrap="nowrap" class="lable">
			<table width="100%" align="center" cellpadding="0" cellspacing="0" class="broder_top0">
			 <tr align="center" class="tdbg">
            <td colspan="2" class="tdtop">&nbsp;</td>
            </tr>
			<tr><td rowspan="3" align="center"><c:if test="${airlineProductDetails.mainImgPath ne ''}"><div align="center"><img src="<c:out value='${airlineProductDetails.mainImgPath}' />" width="63" height="79" /></div><br/></c:if>
		   <div align="center"><bean:write  name="airlineProductDetails"  property="mainImgCap" /></div></td>
            <td width="85%" align="left" nowrap="nowrap" class="lable">Main Image </td>
            </tr>
			<tr>
			  <td width="85%" align="left" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadMainImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="10"/>
            </span></td>
			  </tr>
		   
          <tr class="tdbg">
            <td width="85%" align="left" nowrap="nowrap" class="lable">Caption </td>
            </tr>
          <tr class="tdbg">
            <td align="center">&nbsp;</td>
            <td width="85%" align="left" nowrap="nowrap" class="lable"><html:text property="uploadMainImgCap" styleClass="caption_input" tabindex="11"  maxlength="63"/></td>
            </tr>
		  <tr align="center" class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr align="center" class="tdbg">
            <td colspan="2" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
		  	<td rowspan="3" align="center"><c:if test="${airlineProductDetails.view1ImgPath ne ''}"><div align="center"><img src="<c:out value='${airlineProductDetails.view1ImgPath}' />" width="63" height="79" /></div><br/>
		  <div align="center"><bean:write  name="airlineProductDetails"  property="view1ImgCap" /></div></c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View1 </td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView1ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="12" />
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">Caption1</td>
            </tr>
          <tr class="tdbg">
            <td align="center" ><c:if test="${airlineProductDetails.view1ImgPath ne ''}">Remove <html:checkbox property="deleteView1Img" value="yes" styleClass="checkboxHTML"/></c:if></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView1ImgCap" styleClass="caption_input" tabindex="13"  maxlength="63"/></td>
            </tr>
          <tr align="center" class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3" align="center"><c:if test="${airlineProductDetails.view2ImgPath ne ''}"><div align="center"><img src="<c:out value='${airlineProductDetails.view2ImgPath}' />" width="63" height="79" /></div><br/> 
		    <div align="center"><bean:write  name="airlineProductDetails"  property="view2ImgCap" /></div>	  </c:if>             </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View2</td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView2ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="14"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">Caption2</td>
            </tr>
          <tr class="tdbg">
            <td align="center"> <c:if test="${airlineProductDetails.view2ImgPath ne ''}">Remove <html:checkbox property="deleteView2Img"  value="yes" styleClass="checkboxHTML"/>
              </c:if></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView2ImgCap" styleClass="caption_input" tabindex="15"  maxlength="63"/></td>
            </tr>
		  <tr align="center" class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3" align="center"><c:if test="${airlineProductDetails.view3ImgPath ne ''}"><img src="<c:out value='${airlineProductDetails.view3ImgPath}' />" width="63" height="79" /><br/> 
		    <div align="center"><bean:write  name="airlineProductDetails"  property="view3ImgCap" /></div>  </c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View3</td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView3ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="16"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">Caption3 </td>
            </tr>
          <tr class="tdbg">
            <td align="center"><c:if test="${airlineProductDetails.view3ImgPath ne ''}">Remove <html:checkbox property="deleteView3Img"  value="yes" styleClass="checkboxHTML" tabindex="15"/>
              </c:if></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView3ImgCap" styleClass="caption_input" tabindex="17"  maxlength="63"/></td>
            </tr>
		  <tr align="center" class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          </table>			</td>
            </tr>
			
			 <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments * </span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left">
			<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="sbhComment" class="textarea" rows="5"  tabindex="18" style="width:520px;" onKeyUp="textLimit(this.form.sbhComment,this.form.commentlen,250,'Comments');"></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/>			</td>
			 </tr>
			
			
      </table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"><html:button property="method" styleClass="button" 
		   onclick="javascript:fnCallSaveProduct()" tabindex="19">Save </html:button> 	
          <input type="button" class="button" 
		  onclick="javascript:fnCallSearchAdvertisement(<c:out value="${airlineProductDetails.prodId}"/>);" tabindex="20" value="Cancel" />
		</td>
       
      </tr>
	   <tr>
        <td height="50" colspan="3" align="center" class="help">
        	<table width="80%" border="0" cellpadding="2">
	            <tr>
	              <td valign="top"><strong>Note:</strong> </td>
	              <td align="left">The image uploaded should have a minimum resolution of 175 dpi 
	                and the minimum dimension of 920 x 1380 pixels.</td>
	            </tr>
          </table>
       </td>
      </tr>
    </table>
	<html:hidden property="prodId"/>	
<html:hidden property="ownerId"/>
<html:hidden property="cateId"/>	

<html:hidden property="imgType"/>
<html:hidden property="poPct"/>

</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>


