<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/main.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript" src="../js/menu.js"></script>

<script>
 function triggerEvent() {  
		if(event.keyCode==13) {
		  	fnCallLogin();
		}           
	 }
	
	function fnCallLogin(){
		if(isEligibleToLogin()) {
			document.forms[0].action="login.do?method=login";
			document.forms[0].submit();
		}
	}
	function isEligibleToLogin() {
		var password = document.forms[0].password.value;
		var username = document.forms[0].userId.value;
		
		if(username == "") {
			alert("User Name is required.");
			document.forms[0].userId.focus();
			return false;
		}
		if(password == "") {
			alert("Password is required.");
			document.forms[0].password.focus();
			return false;
		}
		return true;
	}

window.onload=function() {
	document.forms[0].userId.focus();
	document.forms[0].password.value="";
}
</script>

<div><html:form action="airline/login" method="post">
<tr><td>
<html:errors/>
<br /><br/>
<div id="page">
  <div id="logincontainer">
<h1>Login</h1>
<div class="loginbox">
<div class="error" align="center">
<html:messages id="loginErrorMsg" property="loginFailed" message="true">	
		<bean:write name="loginErrorMsg"/>
</html:messages></div>

<div class="lable">User Name :&nbsp;</div>
	<html:text property="userId" styleClass="input" tabindex="1" onkeydown="javascript:triggerEvent();"/>
<div class="lable">Password :&nbsp;</div>
	<html:password property="password" styleClass="input" tabindex="2"  onkeydown="javascript:triggerEvent();"/>

<div class="login"><a href="javascript:fnCallLogin()" >Login</a></div>
<html:hidden property="userType" value="airline"/>
</div>
</div>
</div>
</td></tr>
</html:form></div>
</div>
</div>