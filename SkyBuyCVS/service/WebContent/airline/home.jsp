<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/>    
<script type="text/javascript" src="swfobject.js"></script>
<script src="AC_RunActiveContent.js" language="javascript"></script>
<%
String path = request.getContextPath();
String basePath = "http://"+request.getServerName()+path+"/";

%>
<script>
function getCatalogue(link, windowname){
	var sURL = '<%=basePath%>';
	sURL += link
	window.open(sURL, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');	
}
</script>
 
  <tr>
<td> <logic:present name="airlineLoginInfo" scope="session">
	<div class="coverflowcontainer">
		<h1>Home Page of  <bean:write  name="airlineLoginInfo" property="userName" /></h1>
			
			 
		<div class="container">
		<div class="orderdetails">
		
			
				<h1>Order Details</h1>		
								<ul>
				<logic:present name="compltedOrders" scope="session">
					<logic:notEmpty name="compltedOrders" scope="session">
						<c:choose>
							<c:when test="${compltedOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=C"><c:out value="${compltedOrders}"/> 
						Successfully Completed Order(s)</a></li>
						 </c:when>
						 <c:otherwise>						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>
				
				<!--<logic:present name="pendingOrders" scope="session">
					<logic:notEmpty name="pendingOrders" scope="session">
						<c:choose>
							<c:when test="${pendingOrders ne '0'}">
						<li><a href="searchOrder.do?method=searchOrder&OrderStatus=P"><c:out value="${pendingOrders}"/> 
						Order(s) waiting for with Airline</a></li>
						</c:when>
						 <c:otherwise>
								<li>No Order(s) waiting for Airline approval </li>
						  </c:otherwise>
						</c:choose>	
					</logic:notEmpty>
				</logic:present>-->
				
				<logic:present name="ToBeChargedOrders" scope="session">
					<logic:notEmpty name="ToBeChargedOrders" scope="session">
						<c:choose>
							<c:when test="${ToBeChargedOrders ne '0'}">
								<li><a href="searchOrder.do?method=searchOrder&OrderStatus=Q"><c:out value="${ToBeChargedOrders}"/> Order(s) waiting for Air Charter approval</a></li>
							</c:when>
				  			<c:otherwise>
								 <li>No Order(s) waiting for Air Charter approval 
								   <!--<li><a href="#">Click here for Mange your Orders</a></li>-->
								</li>
				  			</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				<logic:present name="FailedOrders" scope="session">
					<logic:notEmpty name="FailedOrders" scope="session">
						<c:choose>
							<c:when test="${FailedOrders ne '0'}">
								<li><a href="searchOrder.do?method=searchOrder&OrderStatus=F"><c:out value="${FailedOrders}"/> Failed Order(s)</a></li>
							</c:when>
				  			<c:otherwise>
								 <li>No Failed Order
								   <!--<li><a href="#">Click here for Mange your Orders</a></li>-->
								</li>
				  			</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				<logic:present name="RejectedOrders" scope="session">
					<logic:notEmpty name="RejectedOrders" scope="session">
						<c:choose>
							<c:when test="${RejectedOrders ne '0'}">
								<li><a href="searchOrder.do?method=searchOrder&OrderStatus=R"><c:out value="${RejectedOrders}"/> Rejected Order(s)</a></li>
							</c:when>
				  			<c:otherwise>
								 <li>No Rejected Order
								   <!--<li><a href="#">Click here for Mange your Orders</a></li>-->
								</li>
				  			</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
					
				<!--<li><a href="#">Click here for Mange your Orders</a></li>-->
				</ul>
				<hr/>
				
				
				<h1>Catalogue Details (last 7 days)</h1>		
				<ul>
				<logic:present name="pendingCatalogue" scope="session">
					<logic:notEmpty name="pendingCatalogue" scope="session">
						<c:choose>
							<c:when test="${pendingCatalogue ne '0'}">								
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=N"><c:out value="${pendingCatalogue}"/>									
									Pending Item(s) </a></li>								
							</c:when>
							<c:otherwise>
								<li>No Pending Item </li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="acceptedCatalogue" scope="session">
					<logic:notEmpty name="acceptedCatalogue" scope="session">						
						<c:choose>
							<c:when test="${acceptedCatalogue ne '0'}">							
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=A"><c:out value="${acceptedCatalogue}"/> 
									Accepted Item(s) </a></li>
								
							</c:when>
							<c:otherwise>
								<!--<li>No Accepted Catalogue</li>-->
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>
				
				<logic:present name="rejectedCatalogue" scope="session">
					<logic:notEmpty name="rejectedCatalogue" scope="session">
						<c:choose>
							<c:when test="${rejectedCatalogue ne '0'}">
							
									<li><a href="searchAirlineCatalogue.do?method=searchAirlineCatalogue&ApprovalStatus=R"><c:out value="${rejectedCatalogue}"/> 
									Rejected Item(s) </a></li>
								
							</c:when>
							<c:otherwise>
								<li>No Rejected Item 
								  <hr/>
								</li>
							</c:otherwise>
						</c:choose>
					</logic:notEmpty>
				</logic:present>			
				</ul>
		
		
		<h1>Bottom Line </h1>
		<ul>
		<c:choose>
		<c:when test="${orderPlacedTotAmt ne 0}">
		<li>Total value of the orders placed since last statement  <fmt:formatNumber value="${orderPlacedTotAmt}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/></li>
		</c:when>
		<c:otherwise>
		<li>Total value of the orders placed since last statement  <fmt:formatNumber value="${orderPlacedTotAmt}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/></li>
		</c:otherwise>
		</c:choose>
		</ul>
		</div>
		<div class="divider"><img src="../images/h_line.gif" height="280" width="2" /></div>
		
		
		<div class="merchandise">
		<h2>By clicking the following links you can :</h2>
		<hr />		
		<!--For Airline-->
	
			<ul>			
			<li><a class="menuItem" href="initAddAirlineProduct.do?method=initAddAirlineProduct" title="Add Item">Add Item</a></li>
			<li><a class="menuItem" href="initSearchDevice.do?method=initSearchDevice" title="Search Device">Search Device</a> </li>
			<li><a class="menuItem" href="editAirlineReg.do?method=editAirlineDetails&airId=<bean:write name='airlineLoginInfo' property='refId' />" title="Edit My Info">Edit My Info</a> </li>
			</ul>
			<img src="../images/h_line.gif" height="100" width="1" class="divider"/>
			<ul>
				<li><a class="menuItem" href="initSearchAirlineCatalogue.do?method=initSearchAirlineCatalogue" title="Edit/Search Item">Edit/Search Item</a></li>	
				<c:if test="${productCount ne '0'}">
					<li><a class="menuItem"href="javascript:getCatalogue('previewCatalogue.jsp','catalogue');" title="View Catalogue">View Catalogue</a></li>		
				</c:if>			
				<li><a href="initSearchOrder.do?method=initSearchOrder" title="View Order">View Order</a></li>
			</ul>			
	
	
		
		</div>
		
		</div>
	
	
		</div>
				
		
		</div>
		</div></logic:present>
</td></tr>
<logic:empty name="airlineLoginInfo" scope="session">
  <jsp:include page="login.jsp" flush="true"/>
</logic:empty>