<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

window.onload = function(){
	var mon = document.forms[0].month.value;
	var fromDate = document.forms[0].orderFromDate.value;
	
	var my_date=new Date();

	if(mon=="" && fromDate==""){
		var year = my_date.getYear();
		var month =  my_date.getMonth()+1;
		if(month == 12){
			month = 1;
			year = year - 1;
		}else{
			month = month - 1;
		}
		
		document.forms[0].month.value = month;
		
		document.forms[0].year.value = year;
	}

}
function resetMonthAndYear(){
	document.forms[0].month.value = "";
	document.forms[0].year.value = "";
}
function resetFromAndToDate(){
	document.forms[0].orderFromDate.value = "";
	document.forms[0].orderToDate.value = "";
}
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
	}else {
		return datevar;
	}
}

function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
		fnCallSearch();
	}           
}/*
function displayPdf(){
	
	if(isEligibleToSubmit()) {
		document.forms[0].target = "_blank";
		document.forms[0].action="orderReport.do?method=exportOrderReportPdf";
		document.forms[0].submit();			
		document.forms[0].target = "";
	}
	
}*/

function fnCallSearch(){
	
	if(isEligibleForSubmit()) {
		document.forms[0].action="airlineCommReport.do?method=getAirlineCommReport";
		document.forms[0].submit();			
	}
	
}

	function isEligibleForSubmit() {
	
		//var airId = trim(document.forms[0].airId.value);
		var jsMonth = trim(document.forms[0].month.value);
		var jsYear = trim(document.forms[0].year.value);
		var fromDate = trim(document.forms[0].orderFromDate.value);
		var toDate = trim(document.forms[0].orderToDate.value);
		var nameFormat = new RegExp("^[A-Za-z ']*$"); 
		var OrderConfFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		var phoneFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		var orderItemIdFormat = new RegExp("^[0-9]*$");
		
		if(jsMonth == '' && jsYear == '' && fromDate == '' && toDate == ''){
			alert('Please select date range');
			return false;
		}else if(jsMonth != '' && jsYear == ''){
			alert('Please select year');
			return false;
		}else if(jsYear != '' && jsMonth == ''){
			alert('Please select month');
			return false;
		}else if(fromDate != '' && toDate == ''){
			alert('Please select order end date');
			return false;
		}else if(toDate != '' && fromDate == ''){
			alert('Please select order start date');
			return false;
		}else if(toDate != '' && fromDate != ''){
			var isFromDate=isDate(fromDate);
			if(!isFromDate) {
				alert("Enter the order start date in the correct and valid MM/DD/YYYY format");
				return false;
			}
			
			var isToDate=isDate(toDate);
			if(!isToDate) {
				alert("Enter the order end date in the correct and valid MM/DD/YYYY format");
				return false;
			}
		}
		
		/*var today = new Date();
		
		var month=today.getMonth()+1;
		var date=today.getDate();
		
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();

		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDate == ""){
			alert("Please enter valid Order To Date ");
			document.forms[0].orderToDate.focus();
			return false;
		}else {
			var isToDate=isDate(toDate);
			if(!isToDate) {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
				return false;
			}else if(isToDate=="Advance" || isToDate=="Past") {
				if(isToDate == "Past") {
					alert("Enter the To Date as earlier as you entered");
					return false;
				}else if(isToDate == "Advance") {
					alert("Enter the To Date as past as you entered");
					return false;
				}
			}else {
				document.forms[0].orderToDate.value = dateFormats(toDate);
			}
			if(fromDate == "" && !validateDate(dateFormats(toDate),today)) {
				alert("To Date should be earlier or equal to Current Date");
				return false;
			}
		}
		
		if(!((toDate.substring(2,3) == "/" && toDate.substring(5,6) == "/") 
		   || (toDate.substring(2,3) == "-" && toDate.substring(5,6) == "-"))) {
			alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && !((fromDate.substring(2,3) == "/" && fromDate.substring(5,6) == "/") 
		   || (fromDate.substring(2,3) == "-" && fromDate.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && toDate != "") {
			
			isCorrectFromDate = isDate(fromDate);
			isCorrectToDate = isDate(toDate);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].orderFromDate.value = dateFormats(fromDate);
				document.forms[0].orderToDate.value = dateFormats(toDate);
				if(!validateDate(dateFormats(fromDate),dateFormats(toDate))) {
					alert("From Date should be earlier than To Date");
					return false;
				}else if(!validateDate(dateFormats(toDate),today)) {
					alert("To Date should be earlier or equal to current Date");
					return false;
				}
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
			 	return false;
			}
		}*/
		/*if(airId!=""){
			return true;
		}else{
			alert('Select Air Charter');
			return false;
		}*/
		
		
		return true;
	}


	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	
	function fnHome(){
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();		
	}


	
</script>
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
  
  <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
		<ul>
		<li>You navigated from :</li>
		<li>Reports/Statements</li>
		<li>></li>
		<li>Commission Statement<br></li>
		</ul>
		</div>			
		</div>

	</td>
	
    
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td>
        <html:form action="admin/airlineCommReport" method="post">
        <table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td rowspan="2" align="left"><br></td><td class="lable">Month</td><td class="lable">:</td><td align="left"><html:select property="month" styleClass="textarea2" onchange="javascript:resetFromAndToDate();">	
							 <html:option value="">-- Select Month --</html:option>							  		
	 		  				<html:option value="1">January</html:option>
							<html:option value="2">February</html:option>
							<html:option value="3">March</html:option>	
							<html:option value="4">April</html:option>
							<html:option value="5">May</html:option>
							<html:option value="6">June</html:option>
							<html:option value="7">July</html:option>
							<html:option value="8">August</html:option>
							<html:option value="9">September</html:option>
							<html:option value="10">October</html:option>
							<html:option value="11">November</html:option>
							<html:option value="12">December</html:option>
						 </html:select></td><td rowspan="2" valign="middle" style="color:#0000CC">(OR)</td><td class="lable">Order Start Date</td><td class="lable">:</td><td nowrap="nowrap"><html:text property="orderFromDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();" onfocus="javascript:resetMonthAndYear();"/>
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('orderFromDate', false, 'mdy', '/');" /></td>
		  </tr>
		  <tr><td class="lable">Year</td><td class="lable">:</td><td align="left"><html:select property="year" styleClass="textarea2"  onchange="javascript:resetFromAndToDate();">	
						    <html:option value="">-- Select Year --</html:option>							  		
	 		  				<html:option value="2009">2009</html:option>
							<html:option value="2010">2010</html:option>
							<html:option value="2011">2011</html:option>	
							<html:option value="2012">2012</html:option>
							<html:option value="2013">2013</html:option>
							<html:option value="2014">2014</html:option>
							<html:option value="2015">2015</html:option>
							<html:option value="2016">2016</html:option>
							<html:option value="2017">2017</html:option>
							<html:option value="2018">2018</html:option>
							<html:option value="2019">2019</html:option>
							<html:option value="2020">2020</html:option>
						 </html:select></td><td class="lable">Order End Date</td><td class="lable">:</td><td nowrap="nowrap"><html:text property="orderToDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();"  onfocus="javascript:resetMonthAndYear();" />
                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onClick="displayDatePicker('orderToDate', false, 'mdy', '/');" /></td>
		  </tr>
		  
		  <tr><td colspan="10" align="center"><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
		  </tr>
        </table>
        </html:form>
        </td>
      </tr>
      
    </table>
	<logic:present name="noRecords">
	<table width="100%">
   <tr bgcolor="#FFFFFF">
    <td  class="error"  align="center" valign="top" >
     No Records Found</td>
   </tr>
   </table>
	</logic:present>
	
	 <logic:present name="AirlineCommReportDetails">
	 <center>
	 
	 <iframe src="airlineCommReport.do?method=getHTMLAirCommReport" style="height:500px; width:850px; overflow:auto;"/>
	  
	  </iframe>
			 
	</center>
	<table width="100%" align="center">
	<tr align="center">
	   <td align="center">
	   <a href="airlineCommReport.do?method=getPDFAirCommReport"><img src="../images/pdficon.jpg" height="40" width="40"/></a>
	   </td>
	   </tr>
   </table>
	</logic:present>
	
	
	</td>
  </tr>
 
	
	
	
  <tr>
    
<td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
	</td>
  </tr>
</table>

</div>

</td></tr>

