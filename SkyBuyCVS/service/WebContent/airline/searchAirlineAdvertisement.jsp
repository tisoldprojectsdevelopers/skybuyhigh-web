<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link media=screen href="../style/datepic.css" type=text/css rel=stylesheet>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">

function isEmpty(frm_fld){

		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function triggerEvent() {
	if(event.keyCode==13) {
		fnCallSearch();
	}           
}

function isEligibleForSubmit() {
	var searchby = document.forms[0].searchBy.value;
	var searchvalue = document.forms[0].searchValue.value;
	
	if(searchby != "ALL") {
		if(searchvalue == '') {
			alert("Search value is required");
			return false;
		}
	}
	
	return true;
}

function fnCallSearch(){
	if(document.forms[0].searchBy.value == 'ITEMCODE' && trim(document.forms[0].searchValue.value)==""){
		alert("Please enter Item Code");
		document.forms[0].searchValue.focus();	
		return;
	}
	else if(isEligibleForSubmit()) {
		document.forms[0].action="initSearchAdvertisement.do?method=searchAirlineAdvertisement";
		document.forms[0].submit();			
	}
}

	
function fnCallEdit(jsProdIdValue) {
	document.forms[0].action="editAirlineAdvertisement.do?method=editAirlineAdvertisementDetails&ProdId="+jsProdIdValue;
	document.forms[0].submit();

}
function fnCallDelete(jsProdIdValue) {
	var flag=false;
	flag=confirm('Are you sure you want to delete this Item');
	if(flag){
	document.forms[0].action="deleteAdvertisement.do?method=deleteAdvertisementDetails&ProdId="+jsProdIdValue;
	document.forms[0].submit();
	}

}
function fnCallSearchAirlinePackage() {
	document.forms[0].action="initSearchAdvertisement.do?method=initSearchAirlineAdvertisement";
	document.forms[0].submit();
}
function fnCallAccept(jsProdIdValue,jsSbhPrice) {
	document.forms[0].action="acceptAirlineAdvertisement.do?method=acceptAirlinePackage&prodId="+jsProdIdValue+"&sbhPrice="+jsSbhPrice;
	document.forms[0].submit();
}

function fnCallReject(jsProdIdValue) {
	document.forms[0].action="acceptAirlineAdvertisement.do?method=rejectAirlinePackage&prodId="+jsProdIdValue;
	document.forms[0].submit();
}
/********gtky search end *****/	
	
</script>

<html:form action="airline/searchAirlineCatalogue" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="../images/top_nav_middlebg.png">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Air Charter Package<br></li>
		</ul>
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td align="center" class="lable"><table border="0" cellpadding="2" cellspacing="0">
            <tr>
			<td nowrap="nowrap" class="lable">Search By  </td>
              <td class="lable">:</td>
              <td> <html:select property="searchBy" styleClass="textarea2">	
						<html:option value="ALL">All</html:option>
						<html:option value="ITEMCODE">Item Code</html:option>		  			
   					</html:select>
			 </td>	
			 <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input"/></td>
              <td nowrap="nowrap" class="lable">Show in Catalogue </td>
              <td class="lable">:</td>
              <td><html:select property="prodStatus" styleClass="textarea2">
						<html:option value="ALL">All</html:option>
						<html:option value="A">Yes</html:option>
	 		  			<html:option value="I">No</html:option>
    </html:select></td>
              <td nowrap="nowrap" class="lable">Approval Status</td>
              <td class="lable">:</td>
              <td><html:select property="sbhProdStatus" styleClass="textarea2">
						<html:option value="ALL">All</html:option>
						<html:option value="N">Pending</html:option>
	 		  			<html:option value="A">Accepted</html:option>
						<html:option value="R">Rejected</html:option>
    </html:select></td>
    			<td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td"  align="center">
	<logic:present name="catelogueInfo" scope="request"> 
  <logic:notEmpty name="catelogueInfo">	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td  colspan="3" class="tablehead" background="../images/header_bg.gif" align="center"><h2>Air Charter Package Information</h2> </td>
        </tr>
      <tr>
        <td><table width="750" border="0" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" class="broder_top0">
          <tr>
            <td height="32" class="table_header">Item</td>
            <td class="table_header">Description </td>
            <!-- <td class="table_header" nowrap="nowrap">Price</td> -->           
            <td class="table_header" nowrap="nowrap">Show in Catalogue</td>
			<td class="table_header" nowrap="nowrap">Approval Status</td>
			 <td nowrap="nowrap" class="table_header">Last Approval Date</td>
			<td class="table_header" align="center">Comments</td>
            <td class="table_header">Action</td>
          </tr>
		  <logic:iterate id="CatelogueInfo" name="catelogueInfo">
          <tr class="tdbg">
            <td align="left" valign="top" class="catagory"><div align="center"><img src='<bean:write name="CatelogueInfo" property="mainImgPath"/>' alt='<bean:write name="CatelogueInfo" property="prodCode"/>' width="79" height="79" /></div>
              <div class="lable"><bean:write name="CatelogueInfo" property="prodCode"/></div>			  </td>
            <td align="left">
				<div style="width:250px; height:60px; word-wrap: break-word; overflow:auto;" >
				<span class="lable"><bean:write name="CatelogueInfo" property="brandName"/></span><br/>
                <span class="lable"><bean:write name="CatelogueInfo" property="prodTitle"/></span><br/>
             	 <div class="desc">
              
						
						 <logic:present name="CatelogueInfo">
							<logic:notEmpty name="CatelogueInfo">
						 		 <c:out value="${CatelogueInfo.shortDesc}" escapeXml="false"/>
							 </logic:notEmpty>
					  	</logic:present>	
			</div>
              </div></td>
           <!--  <td align="right" valign="middle" nowrap="nowrap"><div class="lable"><fmt:setLocale value="en_US" /><fmt:formatNumber value="${CatelogueInfo.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>	</div>              </td>  -->          
            <td align="center"><div class="lable"><bean:write name="CatelogueInfo" property="inShopStatus"/></div></td>
			<td><div class="lable"><bean:write name="CatelogueInfo" property="sbhProdStatus"/></div></td>
			 <td align="center" valign="middle"><div class="lable"><bean:write name="CatelogueInfo" property="adminApprovalDt"/></div></td> 
			<td align="center"><div class="lable">
			<div style="width:150px; height: 60px; word-wrap: break-word; overflow:auto; " >
			<bean:write name="CatelogueInfo" property="sbhComment"/>
			</div>
			</div></td>
            <td nowrap="nowrap" align="center" class="action"><div class="lable">
	            <a href="viewAdvertisement.do?method=viewAdvertisementDetails&ProdId=<bean:write name='CatelogueInfo' property='prodId'/>" class="editdelete" title="View Item">View</a><br/>
	            <a href="javascript: void fnCallEdit('<bean:write name='CatelogueInfo' property='prodId'/>')" class="editdelete" title="Edit Item">Edit</a><br/>
	            <a href="javascript: void fnCallDelete('<bean:write name='CatelogueInfo' property='prodId'/>')" class="editdelete" title="Delete Item">Delete</a> </div>
	            <c:if test="${CatelogueInfo.sbhProdStatus eq 'R'}">
	       			<a href="javascript: void fnCallAccept('<bean:write name='MerchandizeInfo' property='prodId'/>','<bean:write name='MerchandizeInfo' property='sbhPrice'/>')" title="Accept Item">Accept</a>
	       		</c:if>
	       		<c:if test="${CatelogueInfo.sbhProdStatus eq 'A'}">
	       			<a href="javascript: void fnCallReject('<bean:write name='MerchandizeInfo' property='prodId'/>')" title="Reject Item">Reject</a>
	       		</c:if>
       		</td>
          </tr>
		   </logic:iterate>
		    
		
      </table></td>
      </tr>
      
    </table>
	<tr><td class="td" colspan="3">&nbsp;</td></tr>
  <tr><td class="td" colspan="3" align="center"><html:button property="method" onclick="javascritp:fnCallSearchAirlinePackage();" styleClass="button">Cancel</html:button></td></tr>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
	</td>
    </tr>
	 
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png" align="center"></td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>


</div>
</div>
</td></tr>
</html:form>