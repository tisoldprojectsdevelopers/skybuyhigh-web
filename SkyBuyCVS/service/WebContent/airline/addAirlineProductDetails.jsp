<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}

function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }
function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
	}else {
		return datevar;
	}
}
function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}	
function trimPrice1(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		while(startChar=="0" || startChar==".") {		
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);			
		}
		if(retVal>=1)		 
			return true;
	    else
	      	return false;
	      	
		
}
function validatePrice(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		if(startChar=="$") { 
			retVal=retVal.substring(1,retVal.length);
		} 
		var startChar=retVal.substring(0,1);
		while(startChar=="0") { 
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1); 
		} 
		var startChar=retVal.substring(0,1);
		if(startChar==".") { 
			retVal="0"+retVal; 
		} 
		if(retVal>=0.01)
		  return false;
		    else
		    return true;       

}
function fnCallAddProduct(){	
	if(document.forms[0].prodTitle.value==""){
		alert("Please enter Item Name");
		document.forms[0].prodTitle.focus();	
		return;
	}
	/*else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}*/
	else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Price");
		document.forms[0].vendPrice.focus();	
		return;
	}	
	else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Price");
		document.forms[0].vendPrice.focus();	
		return;
	}	
	/*else if(document.forms[0].validFromDate.value!="" && document.forms[0].validToDate.value==""){		
		alert("Please enter Valid To Date");
		document.forms[0].validToDate.focus();	
		return;	
	}
	else if(document.forms[0].validFromDate.value=="" && document.forms[0].validToDate.value!=""){		
		alert("Please enter Valid From Date");
		document.forms[0].validFromDate.focus();	
		return;	
	}*/	
	else if(document.forms[0].shortDesc.value==""){
		alert("Please enter Title");
		document.forms[0].shortDesc.focus();	
		return;
	}
	else if(document.forms[0].longDesc.value==""){
		alert("Please enter Full Description");
		document.forms[0].longDesc.focus();	
		return;
	}else{
	
	if(document.forms[0].vendPrice.value.charAt(0)=="$")
		document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
		
	/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
	
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
	
	document.forms[0].shortDescription.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDescription.value=document.forms[0].shortDescription.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
	
	document.forms[0].fullDescription.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].fullDescription.value=document.forms[0].fullDescription.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');
		
	/*if(!document.forms[0].validFromDate.value=="" && !document.forms[0].validToDate.value==""){		
		if(validateFromandToDate()){
			document.forms[0].action="addAirlineProduct.do?method=addAirlineProductDetails";
			document.forms[0].submit();
		}	
			
	}else{*/
		document.forms[0].action="addAirlineProduct.do?method=addAirlineProductDetails";
		document.forms[0].submit();
	/*}*/
  }
}

	/*function validateFromandToDate() {
	
	
		var fromDate = trim(document.forms[0].validFromDate.value);
		var toDate = trim(document.forms[0].validToDate.value);
		var today = new Date();
		
		var month=today.getMonth()+1;
		var date=today.getDate();
		
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();
		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDate == ""){
			alert("Please enter Valid To Date ");
			document.forms[0].validToDate.focus();
			return false;
		}		
		if(!((toDate.substring(2,3) == "/" && toDate.substring(5,6) == "/") 
		   || (toDate.substring(2,3) == "-" && toDate.substring(5,6) == "-"))) {
			alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && !((fromDate.substring(2,3) == "/" && fromDate.substring(5,6) == "/") 
		   || (fromDate.substring(2,3) == "-" && fromDate.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format");
			return false;
		}
		
		if(fromDate != "" && toDate != "") {
			
			isCorrectFromDate = isDate(fromDate);
			isCorrectToDate = isDate(toDate);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].validFromDate.value = dateFormats(fromDate);
				document.forms[0].validToDate.value = dateFormats(toDate);
				if(!validateDate(dateFormats(fromDate),dateFormats(toDate))) {
					alert("From Date should be earlier than To Date");
					return false;
				}else if(!validateDate(today, dateFormats(toDate))) {
					alert("To Date should be greater than or equal to current date");
					return false;
				}else {
					return true;
				}
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format");
			 	return false;
			}
		}	
	}

	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}*/

	function fnCallPreview(){
		document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
		document.forms[0].submit();
	}
	
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
</script>


<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="../images/top_nav_middlebg.png">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Add Item </li>
		</ul>
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<html:form action="airline/addAirlineProduct" method="post" enctype="multipart/form-data">
	<logic:present name="errorMsg">
	<table width="500">
	<tr>
    <td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
	  </tr>
	  </table>
  </logic:present>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td  colspan="3" align="center" class="tablehead"><h2>Item  Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="4" bgcolor="#FFFFFF" class="broder_top0">
<tr class="tdbg">
<td align="left" class="lable">Air Charter Name</td>
<td width="4%" class="lable">:</td>
<td align="left"><span class="catagory">
<logic:present name="airlineLoginInfo" scope="session">
<bean:write name="airlineLoginInfo" property="userName" /> 
<input type="hidden" name="ownerId" value="<c:out value='${airlineLoginInfo.refId}'/>"/> 
</logic:present> 
</span></td>
</tr>
<tr class="tdbg">
  <td align="left" class="lable"><span class="boldtext">Item Name * </span></td>
  <td class="lable">:</td>
  <td align="left"><span class="catagory">
    <html:text property="prodTitle" styleClass="input" tabindex="1" maxlength="30"/>
  </span></td>
  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code * </span></td>
  <td class="lable">:</td>
  <td align="left"><span class="catagory">
    <html:text styleId="productCode" property="prodCode" styleClass="input" tabindex="2" maxlength="15"/>
  </span></td>
</tr>
<tr class="tdbg">
  <td align="left" class="lable">Price (in US$) * </td>
  <td class="lable">:</td>
  <td align="left"><span class="catagory">
    <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" tabindex="3" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);"/>
  </span></td>
  <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status * </span></td>
  <td class="lable">:</td>
  <td align="left"><span class="catagory">
    <html:select property="inShopStatus" styleClass="textarea2"  tabindex="4">
      <html:option value="A">Active</html:option>
      <html:option value="I">Inactive</html:option>
    </html:select>
  </span></td>
</tr>
<tr class="tdbg">
 	<td align="left" nowrap="nowrap" class="lable">Pre-Authorize Credit Card</td>
 	<td align="center" class="lable">:</td>
	<td class="lable" align="left"><html:checkbox  property="preAuthCC" styleClass="checkboxHTML" value="Y"  tabindex="8"/> Yes</td>
 	<td align="left">&nbsp;</td>
	<td class="lable">&nbsp;</td>
	<td align="left" nowrap="nowrap">&nbsp;</td>
 </tr>
<tr class="tdbg">
  <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Special Instruction </span></td>
  <td valign="top" class="lable">:</td>
  <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><html:textarea property="instructions" styleClass="textarea" rows="5" onkeyup="textLimit(this.form.instructions,this.form.instructionslen,500,'Special Instruction');"  styleId="instructions" style="width:520px;" tabindex="8"></html:textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="instructionslen" size="3" maxlength="3" value="500" class="wordcount"/></td>
</tr>
<tr class="tdbg">
<td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Title * </span></td>
<td valign="top" class="lable">:</td>
<td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span><html:textarea property="shortDesc" styleClass="textarea" rows="5" onkeyup="textLimit(this.form.shortDesc,this.form.shortDesclen,250,'Title');"  styleId="shtDesc" style="width:520px;" tabindex="9"></html:textarea>
  <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="shortDesclen" size="3" maxlength="3" value="250" class="wordcount"/>
            <input type="hidden" name="shortDescription"/>
            </td></tr>
<tr class="tdbg">
<td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Full Description * </span></td>
<td valign="top" class="lable">:</td>
<td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><html:textarea property="longDesc" styleClass="textarea" rows="5" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Full Description');"  styleId="longDesc" style="width:520px;" tabindex="10"></html:textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="longDesclen" size="3" maxlength="3" value="500" class="wordcount"/>
            <input type="hidden" name="fullDescription" /> 
            </td>
</tr>
<!--<tr class="tdbg">
  <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
  <td valign="top" class="lable">:</td>
  <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="sbhComment" rows="5"  tabindex="14" style="width:520px;" onKeyUp="textLimit(this.form.sbhComment,this.form.commentlen,250,'Comments');"></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/></td></tr>-->

</table>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center">
	        <html:button property="method" styleClass="button" 
				onclick="javascript:fnCallAddProduct()" tabindex="11"> Add </html:button>
			<html:button property="method" styleClass="button" 
				onclick="javascript:fnCallHome();" tabindex="12"> Cancel </html:button>
		</td>
       
      </tr>
    </table>
    <html:hidden property="cateId" value="20"/>	
	</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>



