<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script src="../js/SpryMenuBar.js" type="text/javascript"></script>
<link href="../style/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />

<div id="top_menu">
	<logic:present name="airlineLoginInfo" scope="session">
	<c:if test="${airlineLoginInfo.userType eq 'airline'}">
		<ul id="MenuBar1" class="MenuBarHorizontal">
  			<li id="home"><a href="preEntry.do?method=preEntry" title="Back to home">Home</a></li>
			<li id="aircharter"><a href="#" title="Air Charter" class="MenuBarItemSubmenu">Air Charter</a>
				<ul id="aircharter-menu">
					<li><a href="editAirlineReg.do?method=editAirlineDetails&airId=<bean:write name='airlineLoginInfo' property='refId' />" title="Edit My Info">Edit My Info</a>					                        </li>
					<li><a href="initSearchDevice.do?method=initSearchDevice" title="Search Device">Search Device</a> </li>
				</ul>
		  	</li>
		
		    <li id="catalogue"><a href="#" title="Catalogue" class="MenuBarItemSubmenu">Catalogue</a>
				<ul id="catalogue-menu">					
					<li><a href="initAddAirlineProduct.do?method=initAddAirlineProduct" title="Add Item">Add Item</a></li>
					<li><a href="initSearchAirlineCatalogue.do?method=initSearchAirlineCatalogue" title="Edit/Search Item">Edit/Search Item</a>  </li>
					<li><a href="initAddAdvertisement.do?method=initAddAdvertisement" title="Add Air Charter Package">Add Air Charter Package</a>  </li>
					<li><a href="initSearchAdvertisement.do?method=initSearchAirlineAdvertisement" title="Edit/Search Air Charter Package">Edit/Search Air Charter Package</a>  </li>
				</ul>
		    </li>
			<li id="order"><a href="#" title="Order" class="MenuBarItemSubmenu">Order</a>
				<ul id="order-menu">	
					<li><a href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>
					<li class="last"><a href="initSearchOrderSummary.do?method=initSearchOrderSummary" title="Search Order Summary">Search Order Summary</a></li>
				</ul>
			</li>
			<li id="reports"><a href="#" title="Reports/Statements" class="MenuBarItemSubmenu">Reports/Statements</a>
				<ul id="reports-menu">	
					<li><a href="orderReport.do?method=initOrderReport" title="Order Report">Order Report</a></li>
					<li><a href="airlineCommReport.do?method=initAirlineCommReport" title="Commission Statement">Commission Statement</a></li>		
				</ul>
			</li>
		</ul>
	</c:if>	
	</logic:present>
</div>
<script type="text/javascript">
	<!--
	var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
	//-->
</script>
</div>
</div>
</td>
</tr>