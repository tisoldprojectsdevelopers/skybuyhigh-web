<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	function disablePaste(e) {
		if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	    {
	       window.clipboardData.clearData();
	    }
	    return true; 
	}

	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 function textLimit(field, countfield,maxlen,dispName) {		
			if (field.value.length > maxlen + 1){
			  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
			  countfield.value = 0;	
			 } 
			if (field.value.length > maxlen){
			   field.value = field.value.substring(0, maxlen);
			   countfield.value = 0;		
			}   
			else			
				countfield.value = maxlen - field.value.length;
	}
	
	function parseCurrency(field)
	{
		var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
		var testDollar=(field.value).charAt(0);
		var testData=(field.value).substring(1,(field.value).length);
		var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
		if( testDollar!="$"){
			if(!currency.test(field.value) )
				 field.value = field.value.replace(onlyCurrency, "$1");
		}else{
		  if(!currency.test(testData) )
				field.value = field.value.replace(onlyCurrency, "$1");
	      }
	 }
		
	function trimPrice1(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		while(startChar=="0" || startChar==".") {		
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);			
		}
		if(retVal>=1)		 
			return true;
	    else
	      	return false;
	}
	
	function validatePrice(jsPrice) {
			var retVal=jsPrice;
			var startChar=retVal.substring(0,1);
			if(startChar=="$") { 
				retVal=retVal.substring(1,retVal.length);
			} 
			var startChar=retVal.substring(0,1);
			while(startChar=="0") { 
				retVal=retVal.substring(1,retVal.length);
				startChar=retVal.substring(0,1); 
			} 
			var startChar=retVal.substring(0,1);
			if(startChar==".") { 
				retVal="0"+retVal; 
			} 
			if(retVal>=0.01)
			  return false;
			    else
			    return true;       
	
	}
	
	function fnCallAddProduct(){	
		if(document.forms[0].prodTitle.value=="") {
			alert("Please enter Item Name");
			document.forms[0].prodTitle.focus();	
			return;
		}else if(document.forms[0].prodCode.value=="") {
			alert("Please enter Item Code");
			document.forms[0].prodCode.focus();	
			return;
		}
	}
	
	function getFile(imagePath,jsField){
	if(imagePath=='' && jsField == 'Main Image'){
		alert('Please select main image');
		return false;
	}else if(imagePath == ''){
		return true;
	}
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
		return true;
	} else {
		alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
		return false;
	}
}
	function getSwfFile(imagePath,jsField){
	if(imagePath==''){
		alert('Please select advertisement file');
		return false;
	}
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".swf") || (fileType == ".SWF")) {
		return true;
	} else {
		alert("We supports .SWF formats. "+jsField+" file-type is " + fileType );
		return false;
	}
}
	function isEligibleForSubmit() {
	
		if(document.forms[0].prodTitle.value=="") {
			alert("Please enter Item Name");
			document.forms[0].prodTitle.focus();	
			return false;
		}else if(document.forms[0].prodCode.value=="") {
			alert("Please enter Item Code");
			document.forms[0].prodCode.focus();	
			return false;
		}else if(document.forms[0].longDesc.value==""){
			alert("Please enter Long Description");
			document.forms[0].longDesc.focus();	
			return false;
		}
		
		
		var swfPath = document.forms[0].advertisementPath.value;
		var imgPath = document.forms[0].advtMainImgPath.value;
		var view1Img = document.forms[0].uploadView1ImagePath.value;
		var view2Img = document.forms[0].uploadView2ImagePath.value;
		var view3Img = document.forms[0].uploadView3ImagePath.value;
		if(getFile(imgPath , "Main Image") && getFile(view1Img,'View1') && getFile(view2Img,'View2') && getFile(view3Img,'View3')) {
			if(getSwfFile(swfPath , "Advertisement ")){
				return true;
			}else{
				
				return false;
			}
		}else {
			
			return false;
		}
	}
	function fnCallAddAdvertisement() {
		if(isEligibleForSubmit()) {
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/(\r\n)|(\n)/g, "<br>"); 
			document.forms[0].action="viewAddAdvertisement.do?method=addAdvertisement";
			document.forms[0].submit();
		}
	}
	function fnCallPreview(){
		document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
		document.forms[0].submit();
	}
	function fnCallHome() {
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();
	}
</script>


<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		  <tr>
			    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
			    <td width="100%" align="left" background="../images/top_nav_middlebg.png">
					<ul>
						<li>You navigated from :</li>
						<li>Catalogue</li>
						<li>></li>
						<li>Add Air Charter Package<br></li>
					</ul>
				</td>
			    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
		  </tr>
				<tr>
					<td colspan="3" class="td">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="3" class="td">
						<html:form action="airline/initAddAdvertisement" method="post"
							enctype="multipart/form-data">
							<logic:present name="errorMsg">
	<table width="500">
	<tr>
    <td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
	  </tr>
	  </table>
  </logic:present>
							<table width="500" border="0" align="center" cellpadding="0"
								cellspacing="0" class="border">
								<tr>
									<td colspan="3" align="center" class="tablehead">
										<h2> 
											Package Information 
										</h2>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="4"
											bgcolor="#FFFFFF" class="broder_top0">
											<tr class="tdbg">
												<td align="left" class="lable" nowrap="nowrap">
													Air Charter Name
												</td>
												<td width="4%" class="lable">
													:
												</td>
												<td align="left">
													<span class="catagory"> <logic:present
															name="airlineLoginInfo" scope="session">
															<bean:write name="airlineLoginInfo" property="userName" />
															<input type="hidden" name="ownerId"
																value="<c:out value='${airlineLoginInfo.refId}'/>" />
														</logic:present> </span>
												</td>
												<td align="left" class="lable">
													<span class="boldtext">Item Name * </span>
												</td>
												<td class="lable">
													:
												</td>
												<td align="left">
													<span class="catagory"> <html:text
															property="prodTitle" styleClass="input" tabindex="1"
															maxlength="30" /> </span>
												</td>
											</tr>
											<tr class="tdbg">
												<td align="left" nowrap="nowrap" class="lable">
													<span class="boldtext">Item Code * </span>
												</td>
												<td class="lable">
													:
												</td>
												<td align="left">
													<span class="catagory"> <html:text
															styleId="productCode" property="prodCode"
															styleClass="input" tabindex="2" maxlength="15" /> </span>
												</td>
												<td align="left" nowrap="nowrap" class="lable">
													<span class="boldtext">Item Status * </span>
												</td>
												<td class="lable">
													:
												</td>
												<td align="left">
													<span class="catagory"> <html:select
															property="inShopStatus" styleClass="textarea2"
															tabindex="3">
															<html:option value="A">Active</html:option>
															<html:option value="I">Inactive</html:option>
														</html:select> </span>
												</td>
											</tr>
											<tr class="tdbg">
<td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Full Description * </span></td>
<td valign="top" class="lable">:</td>
<td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><html:textarea property="longDesc" styleClass="textarea" rows="5" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Full Description');"  styleId="longDesc" style="width:520px;" tabindex="4"></html:textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="longDesclen" size="3" maxlength="3" value="500" class="wordcount"/>
            <input type="hidden" name="fullDescription" /> 
            </td>
</tr>
											<tr class="tdbg">
												<td colspan="6">
													<hr />
												</td>
											</tr>
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">Main Image *</span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left">
													<html:file property="advtMainImgPath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="5"/>
												</td>
												 <tr class="tdbg">
										            <td align="left" nowrap="nowrap" class="lable">Main Caption </td>
										            <td align="left" nowrap="nowrap" class="lable">:</td>
										            <td align="left" colspan="4" nowrap="nowrap"><html:text property="uploadMainImgCap" styleClass="caption_input" tabindex="6" maxlength="63"/></td>
										          </tr>
											</tr>
											<tr align="right" class="tdbg">
            <td colspan="6" align="center" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"> View1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><span class="catagory">
            <html:file property="uploadView1ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="7"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Caption1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><html:text property="uploadView1ImgCap" styleClass="caption_input" tabindex="8" maxlength="63"/></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"> View2</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><span class="catagory">
            <html:file property="uploadView2ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="9"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Caption2 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><html:text property="uploadView2ImgCap" styleClass="caption_input" tabindex="10" maxlength="63"/></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"> View3</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><span class="catagory">
            <html:file property="uploadView3ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="11" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Caption3 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" colspan="4" nowrap="nowrap"><html:text property="uploadView3ImgCap" styleClass="caption_input" tabindex="12" maxlength="63"/></td>
          </tr>
											<tr class="tdbg">
												<td colspan="6">
													<hr />
												</td>
											</tr>
											<tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">Advertisement * </span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left">
													<html:file property="advertisementPath" accept="application/swf" styleClass="browse" tabindex="13"/><BR/>
													(Upload only the advertisement file with an extension .swf and dimension 508 x 1005 pixels.)
												</td>
											</tr>
											<!-- <tr class="tdbg">
												<td align="left" valign="top" nowrap="nowrap" class="lable">
													<span class="boldtext">Full Description * </span>
												</td>
												<td valign="top" class="lable">
													:
												</td>
												<td colspan="4" align="left">
													<span class="helptext" style="vertical-align: top">Max
														500 chars.<br /> </span>
													<textarea name="longDesc" class="textarea" rows="5"
														onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Long Description');"
														id="longDesc" style="width: 520px;" tabindex="10"></textarea>
													<br />

													<span class="normaltext">Remaining characters</span>
													<input readonly="readonly" type="text" name="longDesclen"
														size="3" maxlength="3" value="500" class="wordcount" />
												</td>
											</tr> -->

										</table>
								</tr>
								<tr>
									<td height="50" colspan="3" align="center">
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallAddAdvertisement()" tabindex="14"> Save </html:button>
										<html:button property="method" styleClass="button"
											onclick="javascript:fnCallHome();" tabindex="15"> Cancel </html:button>
									</td>

								</tr>
							</table>
							<html:hidden property="cateId" value="20" />
						</html:form>
					</td>
				</tr>

				<tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>
</div>
</div>
</td></tr>



