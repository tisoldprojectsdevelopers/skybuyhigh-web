<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link type="text/css" href="../style/registration.css" rel="stylesheet">
<script type="text/javascript">

	function fnCallViewOrderItemDetails(jsOrderItemId){
	
		document.forms[0].action = "editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	
</script>
    <html:form action="customer/customersOrderDisplay">
    	<tr><td>  
     	<div class="contentcontainer">     		
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table">
			  <tbody>
			  	<tr>
				    <td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
				    <td width="100%" background="../images/top_nav_middlebg.png" align="left"> 
						<ul>
						<li>You navigated from :</li>
						<li>Order</li>
						<li>></li>
						<li>Edit/Search Order</li>		
						</ul>
					</td>
				    <td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
			  	</tr>
			 	<tr>
			  		<td colspan="3" valign="top" align="center" class="td">
			  			<table border="0" cellspacing="0" cellpadding="0" class="returnpolicy">
			  				<tr>
			   					 <td colspan="3" nowrap="nowrap"><h2><c:if test="${OwnerType eq 'vendor'}">Return Policy </c:if>
									<c:if test="${OwnerType eq 'airline'}">Restrictions/Validity/Cancellation</c:if></h2></td>
			  				</tr>
			  				<tr>
				    			<td >
				  					<bean:write name="ReturnPolicy" property="returnPolicy"/> 
								</td>
			  				</tr>
			  				<tr>
			  					<td class="lable" width="28%" nowrap="nowrap"> &nbsp;Return Days </td>
			  					<td class="lable" width="10%">:</td>
				    			<td class="lable" nowrap="nowrap">
				  					<bean:write name="ReturnPolicy" property="returnDays"/>
								</td>
			  				</tr>
			  				<tr>
			    				<td>
			    					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="customerService">
			    						<tr>
			    							<td colspan="3" align="left"><h3>Customer Service</h3></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable" nowrap="nowrap">Phone No</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable" nowrap="nowrap"><bean:write name="ReturnPolicy" property="custServicePhone"/></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable" nowrap="nowrap">Extn No</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable" nowrap="nowrap"><bean:write name="ReturnPolicy" property="custServicePhoneExtnNo"/></td>
			    						</tr>
			    						<tr>
			    							<td align="left" class="lable" nowrap="nowrap">Email</td>
			    							<td align="left" class="lable"><b>:</b></td>
			    							<td align="left" class="lable" nowrap="nowrap"><bean:write name="ReturnPolicy" property="custServiceEmailId"/></td>
			    						</tr>
			    					</table>
			    				</td>
			  				</tr>
						</table>
					</td>
			    </tr>
			 	<tr>
			      <td height="50" align="center" class="td" colspan="3">
			      		<input type="button" class="button" onClick="javascript:fnCallViewOrderItemDetails('<c:out value="${OrderItemId}"/>');" value="OK" name="method"/>
			      </td>
			    </tr>
				<tr>
				    <td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
				    <td background="../images/top_nav_bmiddlebg.png"> </td>
				    <td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
			 	</tr>
			  </tbody>
		  </table>
     	</div>
		<!-- end contentcontainer -->
		</td></tr>
    </html:form>
  
