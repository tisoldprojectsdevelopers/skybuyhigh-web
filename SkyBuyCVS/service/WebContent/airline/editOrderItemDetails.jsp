<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/



function displayReturnPolicy(orderItemId,ownerType) {
	document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
	document.forms[0].submit();
}

function fnCallSaveOrder(){
	document.forms[0].action="updateOrder.do?method=updateOrderDetails";
	document.forms[0].submit();
}

function fnCallSearchOrder(){
	document.forms[0].action="searchOrder.do?method=searchOrder";
	document.forms[0].submit();
}
function fnGetCustTrackingDetails(orderItemId,jsItemStatus,jsViewOrder) {
	document.forms[0].action="orderTrackingDetails.do?method=getOrderTrackingDetails&OrderItemId="+orderItemId+"&ItemStatus="+jsItemStatus+"&ViewOrder="+jsViewOrder;
	document.forms[0].submit();
}

</script>




<html:form action="airline/editOrder" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">			
		<ul>
		<li>You navigated from :</li>
		<li>Order</li>		
		</ul>		
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="2" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
            <td align="right" colspan="4" class="lable">
           		
	           	<c:if test="${mode eq 'Edit' && (custOrderItemDetails.orderItemStatus eq 'C' || custOrderItemDetails.orderItemStatus eq 'Q')}">
	           		<a href="javascript:fnGetCustTrackingDetails('<bean:write  name="custOrderItemDetails"  property="orderItemId" />','<bean:write  name="custOrderItemDetails"  property="orderItemStatus" />','VIEW')">Order Tracking Details</a>  
	           	</c:if>
	           	
           	</td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="custOrderItemDetails" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable">Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails" property="custFirstName" />              
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
               <bean:write  name="custOrderItemDetails" property="custLastName" />           
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"> Address Line 1</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="custOrderItemDetails" property="custAddress1" />   
        	</span></td>
        	<c:if test="${custOrderItemDetails.custAddress2 ne null && custOrderItemDetails.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable"> Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="custOrderItemDetails" property="custAddress2" />   
	        	</span></td>
        	</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			 <bean:write  name="custOrderItemDetails" property="custCity" />		             
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<logic:iterate id="states" name="StateList" scope="application">
						<c:if test="${states.stateCode eq custOrderItemDetails.custState}">
							<bean:write name="states" property="stateName" />
						</c:if>
					</logic:iterate>		
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <bean:write  name="custOrderItemDetails" property="custCountry" />
			 </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
			<bean:write  name="custOrderItemDetails" property="custZip" />
			</span></td>
          </tr>
		   <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custPhone" />		
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
			 <bean:write  name="custOrderItemDetails" property="custEmail" />
            </span></td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="custOrderItemDetails" property="custShipFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custShipLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
       		   <bean:write  name="custOrderItemDetails" property="custShipAddress1" />     
            </span></td>
            <c:if test="${custOrderItemDetails.custShipAddress2 ne null && custOrderItemDetails.custShipAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	       		   <bean:write  name="custOrderItemDetails" property="custShipAddress2" />     
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="custOrderItemDetails" property="custShipCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq custOrderItemDetails.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>     
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="custOrderItemDetails" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <bean:write  name="custOrderItemDetails" property="custShipZip" />
            </span></td>
          </tr>
           <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custShipPhone" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="custOrderItemDetails" property="custShipEmail" />
            </span></td>
          </tr>
          
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="airName"/>
            </span></td>
            <c:if test="${custOrderItemDetails.flightNo ne null && custOrderItemDetails.flightNo ne ''}">
	            <td align="left" nowrap="nowrap" class="lable">Flight No </td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
	              <bean:write  name="custOrderItemDetails"  property="flightNo" />
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="custOrderItemDetails"  property="vendorName"/>
        </span></td>
            <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="custOrderItemDetails"  property="orderItemId"/>','<bean:write  name="custOrderItemDetails"  property="ownerType"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
          </tr>
          
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="10" align="left" class="lable"><div align="center">
			    <c:if test="${custOrderItemDetails.isSpecialProd ne 'Y' }">
			   <img src="editOrder.do?method=renderImage&orderItemId=<c:out value='${custOrderItemDetails.orderItemId}' />" width="200" height="200" />
			   </c:if>
			   </div></td>
			   </tr>
			   <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Order Item ID</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                 <bean:write  name="custOrderItemDetails"  property="orderItemId" />
	               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${custOrderItemDetails.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="custOrderItemDetails"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="itemName"/>
               </span></td>
			   </tr>
			 <c:if test="${custOrderItemDetails.travelDate ne null && custOrderItemDetails.travelDate ne ''}">
				   <tr class="tdbg">
				   <td align="left" class="lable"><span class="boldtext">Travel Date </span></td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                 <bean:write  name="custOrderItemDetails" property="travelDate" />
	               </span></td>
				   </tr>
				</c:if>	
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <!--<c:if test="${custOrderItemDetails.orderItemStatus eq 'P'}"> Order to be place with Airline </c:if>-->
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'C'}"> Completed </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'F'}"> Failed </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'R'}"> Rejected </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'Q'}"> Waiting for Air Charter approval</c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'A'}"> RMA Approved </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'E'}"> RMA Rejected </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'O'}"> Canceled </c:if>
               </span></td>
			   </tr>
			   <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Shipment Status</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${custOrderItemDetails.shipmentStatus eq 'N'}"> Not yet shipped</c:if>
                 <c:if test="${custOrderItemDetails.shipmentStatus eq 'Y'}"> Shipped </c:if>
                 <span></td>
			   </tr>
			   <c:if test="${custOrderItemDetails.orderItemStatus eq 'F' && custOrderItemDetails.reasonForFailure ne null && custOrderItemDetails.reasonForFailure ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Failure</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" valign="top"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="reasonForFailure" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${custOrderItemDetails.orderItemStatus eq 'R' && custOrderItemDetails.reasonForReject ne null && custOrderItemDetails.reasonForReject ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Rejected</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" valign="top"><span class="catagory">
					 <bean:write  name="custOrderItemDetails"  property="reasonForReject" />
				   </span></td>
				 </tr>
			 </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Quantity</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
               
                   <bean:write  name="custOrderItemDetails" property="qty" />
             
               </span></td>
			   </tr>		
			 
			 <tr class="tdbg">			
			   <td align="left" class="lable">		  	   
			   Price			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
			   <fmt:formatNumber value="${custOrderItemDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
               </span></td>			  
			   </tr>
			   
			    <c:if test="${custOrderItemDetails.rmaStatus ne null and custOrderItemDetails.rmaStatus ne ''}">
				    <tr class="tdbg">
				       <td align="left" nowrap="nowrap" class="lable">Return Status</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                   <c:if test="${custOrderItemDetails.rmaStatus eq 'RG'}">
		                   		RMA Generated
		                   </c:if>
		                   <c:if test="${custOrderItemDetails.rmaStatus eq 'WA'}">
		                   		Waiting for Admin to charge back
		                   </c:if>
		                   <c:if test="${custOrderItemDetails.rmaStatus eq 'WR'}">
		                   		Waiting for Admin to reject
		                   </c:if>
		                   <c:if test="${custOrderItemDetails.rmaStatus eq 'RR'}">
		                   		RMA Rejected
		                   </c:if>
		                   <c:if test="${custOrderItemDetails.rmaStatus eq 'RA'}">
		                   		RMA Approved
		                   </c:if>
		                   
		               </span></td>			
				   </tr>
			   </c:if>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 			
		  <html:button property="method" onclick="javascript:fnCallSearchOrder()" styleClass="button">OK</html:button>
		
		  </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</div>
</td></tr>
</html:form>
