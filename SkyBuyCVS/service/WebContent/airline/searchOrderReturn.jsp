<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<script src="../js/datepicker.js" type=text/javascript></script>
<script>
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	
	function fnHome(){
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();		
	}
	
	function triggerEvent() {
		if(event.keyCode==13) {
			fnCallSearch();
		}           
	}
	function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	var datevar = dateObj;
	datevar = dateFormats(datevar);
	if(datevar != "Past" && datevar != "Advance") {
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}
		}else {
			return datevar;
		}	
	}
	
	function dateFormats(sDate){
		var parseYr;
		var yl=1990;
		var ym=2200;
		if(sDate.length==8 || sDate.length==10){
			if(sDate.length==10){
				sDate=sDate.replace("-","/");
				sDate=sDate.replace("-","/");
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					sDate=sDate.replace("-","/");
					sDate=sDate.replace("-","/");
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
			if (y<yl) {
				parseYr = y+""+m+""+d;
				return "Past";
			}else if(y>ym) {
				parseYr = y+""+m+""+d;
				return "Advance";
			}
			else
				return sDate;
		}
		return sDate;
	}
	
	function checkdate(d,m,y)
	{
	//alert(m);
		var yl=1990; // least year to consider
		var ym=2200; // most year to consider
		if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
		if (m<1 || m>12) return(false);
		if (d<1 || d>31) return(false);
		if (y<yl || y>ym) return(false);
		if (m==4 || m==6 || m==9 || m==11)
		if (d==31) return(false);
		if (m==2)
		{
		var b=parseInt(y/4);
		if (isNaN(b)) return(false);
		if (d>29) return(false);
		if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
		}
		return(true);
	}
	function IsNumeric(sText)
	
	{
	   var ValidChars = "0123456789.";
	   var IsNumber=true;
	   var Char;
	
	 
	   for (i = 0; i < sText.length && IsNumber == true; i++) 
	      { 
	      Char = sText.charAt(i); 
	      if (ValidChars.indexOf(Char) == -1) 
	         {
	         IsNumber = false;
	         }
	      }
	   return IsNumber;
	}
	
	function isEligibleForSubmit() {
	
		var searchby = trim(document.forms[0].searchBy.value);
		var searchvalue = trim(document.forms[0].searchValue.value);
		var fromDateValue = trim(document.forms[0].orderFromDate.value);
		var toDateValue = trim(document.forms[0].orderToDate.value);
		var today = new Date();
		var month=today.getMonth()+1;
		var date=today.getDate();
		if(month<10) {
			month="0"+month;		
		}
		if(date<10) {
			date="0"+date;
		}
		today=month+"/"+date+"/"+today.getFullYear();
		var nameFormat = new RegExp("^[A-Za-z ']*$");
		var RMAFormat = new RegExp("^[A-Za-z0-9\-]*[A-Za-z0-9]$");
		var phoneFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		var OrderConfFormat = new RegExp("^[0-9][0-9\-]*[0-9]$");
		
		var isCorrectFromDate;
		var isCorrectToDate;
		if(toDateValue == "") {
			alert("Please enter valid Order To Date.");
			document.forms[0].orderToDate.focus();
			return false;
		}else {
			var isToDate=isDate(toDateValue);
			if(!isToDate) {
				alert("Enter the date in the correct and valid MM/DD/YYYY format.");
				return false;
			}else if(isToDate=="Advance" || isToDate=="Past") {
				if(isToDate == "Past") {
					alert("Enter the To Date as earlier as you entered.");
					return false;
				}else if(isToDate == "Advance") {
					alert("Enter the To Date as past as you entered.");
					return false;
				}
			}else {
				document.forms[0].orderToDate.value = dateFormats(toDateValue);
			}
			if(fromDateValue == "" && !validateDate(dateFormats(toDateValue),today)) {
				alert("To Date should be earlier or equal to Today's Date.");
				return false;
			}
		}
		if(!((toDateValue.substring(2,3) == "/" && toDateValue.substring(5,6) == "/") 
		   || (toDateValue.substring(2,3) == "-" && toDateValue.substring(5,6) == "-"))) {

		   	alert("Enter the date in the correct and valid MM/DD/YYYY format.");
			return false;
		}
		
		if(fromDateValue != "" && !((fromDateValue.substring(2,3) == "/" && fromDateValue.substring(5,6) == "/") 
		   || (fromDateValue.substring(2,3) == "-" && fromDateValue.substring(5,6) == "-"))) {
		   	alert("Enter the date in the correct and valid MM/DD/YYYY format.");
			return false;
		}
		
		if(fromDateValue != "" && toDateValue != "") {
			
			isCorrectFromDate = isDate(fromDateValue);
			isCorrectToDate = isDate(toDateValue);
			if(isCorrectFromDate == "Past") {
				alert("Enter the From Date as earlier as you entered.");
				return false;
			}else if(isCorrectToDate == "Past") {
				alert("Enter the To Date as earlier as you entered.");
				return false;
			}else if(isCorrectFromDate == "Advance") {
				alert("Enter the From Date as past as you entered.");
				return false;
			}else if(isCorrectToDate == "Advance") {
				alert("Enter the To Date as past as you entered");
				return false;
			}else if(isCorrectFromDate && isCorrectToDate) {
				document.forms[0].orderFromDate.value = dateFormats(fromDateValue);
				document.forms[0].orderToDate.value = dateFormats(toDateValue);
				if(!validateDate(dateFormats(fromDateValue),dateFormats(toDateValue))) {
					alert("From Date should be earlier than To Date.");
					return false;
				}else if(!validateDate(dateFormats(toDateValue),today)) {
					alert("To Date should be earlier or equal to Today's Date.");
					return false;
				}else {
					return true;
				}
			}else {
				alert("Enter the date in the correct and valid MM/DD/YYYY format.");
			 	return false;
			}
		}
		
		if(searchby != "ALL") {
			if(searchvalue != "") {
				document.forms[0].searchValue.value=trim(searchvalue);
				if(searchby == "CUSTTRNSID") {
					if(!OrderConfFormat.test(searchvalue)) {
						alert("You must enter a valid Order Confirmation No.");
					}else {
						return true;
					}
				}else if(searchby == "CUSTPHONE") {
					if(!phoneFormat.test(searchvalue)) {
						alert("You must enter a valid Phone Number.");
					}else {
						return true;
					}
				}else if(searchby == "CUSTNAME") {
					if(!nameFormat.test(searchvalue)) {
						alert("You must enter a valid customer name.");
					}else{
						return true;
					}
				}else if(searchby == "RMANUMBER") {
					if(!RMAFormat.test(searchvalue)) {
						alert('You must enter a valid RMA Number');
					}else {
						return true;
					}
				}else {
					return true;
				}
			}else {
				alert("You must provide the search value.");
			}
		}else {
			document.forms[0].searchValue.value="";
			return true;
		}
		return false;
	}
	
	function fnCallSearch() {
		if(isEligibleForSubmit()) {
			document.forms[0].action="OrderReturn.do?method=getOrderToReturn";
			document.forms[0].submit();
		}
	}
	
	function validateDate(fromDate,toDate) {
		var fromYear;
		var fromMonth;
		var fromDate;
		var toYear;
		var toMonth;
		var toDate;
		var index;
		var tempString;
		if(fromDate.length>0) {
			index = fromDate.indexOf("/");
			fromMonth = fromDate.substring(0,index);
			tempString = fromDate.substring(index+1);
			index = tempString.indexOf("/");
			fromDate = tempString.substring(0,index);
			fromYear = tempString.substring(index+1);
		}
		if(toDate.length>0) {
			index = toDate.indexOf("/");
			toMonth = toDate.substring(0,index);
			tempString = toDate.substring(index+1);
			index = tempString.indexOf("/");
			toDate = tempString.substring(0,index);
			toYear = tempString.substring(index+1);
		}
		
		if(fromYear > toYear) {
			return false;
		}else if(fromYear == toYear) {
			if(fromMonth > toMonth) {
				return false;
			}else if(fromMonth == toMonth) {
				if(fromDate > toDate) {
					return false;
				}else {
					return true;
				}
			}else {
				return true;
			}
		}else {
			return true;
		}
	}
	function fnCallView(jsReturnId) {
		document.forms[0].action="viewSearchRMADetails.do?method=viewReturnOrderDetails&ReturnId="+jsReturnId;
		document.forms[0].submit();
	}
	function fnCallViewCreditChargeBack(jsOrderItemId) {
		document.forms[0].action="viewCreditChargeBack.do?method=viewChargeBackOrderDetails&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
	function fnCallCreditReject(jsOrderItemId) {
		document.forms[0].action="viewCreditChargeBack.do?method=viewChargeBackOrderDetails&OrderItemId="+jsOrderItemId;
		document.forms[0].submit();
	}
</script>
<html:form action="airline/initSearchOrderReturn" method="post">
	<tr><td>
<div class="contentcontainer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  		<tr>
		    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
		    <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
				<ul>
					<li>You navigated from :</li>
					<li>Order</li>
					<li>></li>
					<li>Search Order Return</li>
				</ul>
			</td>
		    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
	  </tr>
	  <tr>
	    <td colspan="3" class="td">
			<table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
		      <tr>
		        <td>
					<table border="0" align="center" cellpadding="0" cellspacing="2" >
			          <tr>
			            <td nowrap="nowrap" class="lable">Search By : </td>
			            <td nowrap="nowrap">			  
				  			 <html:select property="searchBy" styleClass="textarea2">	
								<html:option value="ALL">All</html:option>
								<html:option value="AIRLINENAME">Air Charter Name</html:option>
								<!--<html:option value="AIRNAME">Air Name</html:option>-->
								<html:option value="CUSTNAME">Customer Name</html:option>	
								<html:option value="CUSTPHONE">Customer Phone No</html:option>
								<html:option value="CUSTTRNSID">Order Confirmation No</html:option>
								<html:option value="RMANUMBER">RMA Number</html:option>	
								<html:option value="VENDORNAME">Vendor Name</html:option>	
		               		 </html:select>
				        </td>
			            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search_input_rma" onkeydown="javascript:triggerEvent();" maxlength="64"/></td>
			            <td nowrap="nowrap" class="lable">Category :</td>
			            <td nowrap="nowrap">
			            	<html:select property="category" styleClass="textarea2">
				                <html:option value="ALL">All</html:option>				
				                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>			
								<html:option value="20">Private Jet Jaunts</html:option>	
			            	</html:select>
			            </td>
			            <td nowrap="nowrap" class="lable">Return Status :</td>
			             <td nowrap="nowrap">
			            	<html:select property="orderStatus" styleClass="textarea2">
				                 <html:option value="ALL">All</html:option>
				                 <html:option value="WA">Waiting For Admin to Charge back</html:option>
								 <html:option value="WR">Waiting For Admin to Reject</html:option>
								 <html:option value="RG">RMA Generated</html:option> 
								 <!--<html:option value="RR">RMA Rejected</html:option>-->
								 <html:option value="RA">RMA Approved</html:option>
			               </html:select>
			        	</td>
			          </tr>
			        </table>
			      </td>
			   </tr>
		       <tr>
	       		 <td>
	       			<table border="0" align="center" cellpadding="0" cellspacing="2">
	          			<tr>
			           	 <td nowrap="nowrap" class="lable">From Date: </td>
			             <td nowrap="nowrap"><html:text property="orderFromDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();"/>
			                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('orderFromDate', false, 'mdy', '/');" /></td>
						 <td nowrap="nowrap" class="lable">To Date: </td>
			             <td nowrap="nowrap"><html:text property="orderToDate"  styleClass="search-input" maxlength="10" onkeydown="javascript:triggerEvent();"/>
			                <img  src="../images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('orderToDate', false, 'mdy', '/');" /></td>	
			             <td nowrap="nowrap">&nbsp;</td>
			             <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
			          </tr>
	        		</table>
	        	 </td>
		      </tr>
		   </table>
		   <logic:present name="ReturnOrderItems">
								<logic:notEmpty name="ReturnOrderItems">
									<table width="800" border="0" align="center" cellpadding="0"
										cellspacing="0" class="border">
										<tr>
											<td height="30" colspan="3" class="tablehead" align="center">
												<h2>
													Order Information
												</h2>
											</td>
										</tr>
										<tr>
											<td valign="top">
												<table border="0" width="100%" cellpadding="1"
													cellspacing="1" bgcolor="#cccccc" class="broder_top0">
													<tr>
														<td height="32" align="center" nowrap="NOWRAP"
															class="table_header">
															Return Id
														</td>
														<td nowrap="NOWRAP" class="table_header">
															Order Confirmation No
														</td>
														<td nowrap="NOWRAP" class="table_header">
															Customer Name
														</td>
														<td nowrap="nowrap" class="table_header">
															Phone No
														</td>
														<td nowrap="nowrap" class="table_header">
															Item Code
														</td>
														<td nowrap="nowrap" class="table_header">
															Approved Quantity
														</td>
														<!-- <td nowrap="nowrap" class="table_header">
															Amount
														</td> -->
														<td nowrap="nowrap" class="table_header">
															Return Status
														</td>
														<td nowrap="nowrap" class="table_header">
															Order Date
														</td>
														<td nowrap="nowrap" class="table_header">
															Vendor Name
														</td>
														<td nowrap="nowrap" class="table_header">
															Action
														</td>
													</tr>
													<logic:iterate id="returnOrder" name="ReturnOrderItems">
														<tr class="tdbg">
															<td nowrap="nowrap" class="lable">
																<a href="javascript:fnCallView('<bean:write name="returnOrder" property="returnId" />')" title="View Order Details"><bean:write name="returnOrder" property="returnId" /></a>
															</td>
															<td nowrap="nowrap" class="lable">
																<bean:write name="returnOrder" property="custTransId" />
															</td>
															<td nowrap="nowrap" class="lable" align="left">
																<bean:write name="returnOrder" property="custFirstName" />
																&nbsp;
																<bean:write name="returnOrder" property="custLastName" />
															</td>
															<td nowrap="nowrap" class="lable">
																<bean:write name="returnOrder" property="custPhone" />
															</td>
															<td nowrap="nowrap" align="left" class="lable">
																<bean:write name="returnOrder" property="itemName" />
															</td>
															<td nowrap="nowrap" align="right" class="lable">
																<bean:write name="returnOrder" property="quantity" />
															</td>
															<!-- <td nowrap="nowrap" align="right" class="lable">
																<bean:write name="returnOrder" property="payAmount" />
															</td> -->
															<td nowrap="nowrap" class="lable" align="left">
																<c:if test="${returnOrder.status eq 'RG'}"> RMA Generated </c:if>
																<c:if test="${returnOrder.status eq 'WA'}"> Waiting for Admin to Charge Back </c:if>
																<c:if test="${returnOrder.status eq 'WR'}"> Waiting for Admin to Reject </c:if>
																<c:if test="${returnOrder.status eq 'RA'}"> RMA Approved </c:if>
																<c:if test="${returnOrder.status eq 'RR'}"> RMA Rejected </c:if>
															</td>
															<td nowrap="nowrap" class="lable">
																<bean:write name="returnOrder" property="orderCreatedDt" />
															</td>
															<td nowrap="nowrap" class="lable" align="left">
																<bean:write name="returnOrder" property="vendorName" />
															</td>
															<td nowrap="nowrap" class="lable">
																<div class="action">
																	<a href="javascript: void fnCallView('<bean:write name='returnOrder' property='returnId'/>')" class="editdelete" title="View Order details">View</a>
																</div>
															</td>
														</tr>
													</logic:iterate>
												</table>
											</td>
										</tr>
									</table>
								</logic:notEmpty>
							</logic:present>
							<logic:present name="NoRecords" scope="request">
								<div align="center">
									<font color="#FF0000" size="-2">No Records Found.</font>
								</div>
							</logic:present>
							<logic:present name="ReturnOrderItems">
								<logic:notEmpty name="ReturnOrderItems">
									<tr>
										<td height="50" colspan="3" align="center" class="td">
											<BR />
											<BR />
											<html:button property="method" styleClass="button"
												onclick="javascript:fnHome();" tabindex="10"> Cancel </html:button>
										</td>
									</tr>
								</logic:notEmpty>
							</logic:present>
						</td>
					</tr>
					<tr>
					  <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
					  <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
					  <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
					</tr>
				</table>
			</div>
		</div>
	</td></tr>
</html:form>
