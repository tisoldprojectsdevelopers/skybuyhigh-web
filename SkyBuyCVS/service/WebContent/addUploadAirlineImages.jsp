<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>



function fnCallUploadProductImg(){
	if(document.forms[0].mainImgPath.value==""){
		alert("Please select Main Image.");
		document.forms[0].mainImgPath.focus();	
		return;
	}else{
	document.forms[0].action="addUploadAirlineProdImg.do?method=addUploadAirlineProductImage";
	document.forms[0].submit();
	}
}
</script>

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    
    <td colspan="3">
		<div class="nav-header">
			<div class="nav-header-right"></div>
			<div class="nav-header-left"></div>
			<div class="nav-header-content">
				<ul>
					<li>You navigated from :</li>
					<li>Catalogue</li>
					<li>></li>
					<li>Add Item</li>
				</ul>
			</div>			
		</div>
		<!-- end nav-header -->
	</td>
    
    
    
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<html:form action="addUploadAirlineProdImg" method="post" enctype="multipart/form-data">
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Upload Image(s)</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="broder_top0">
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">Main Image </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="mainImgPath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> Caption </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="mainImgCap" styleClass="caption_input"/></td>
          </tr>
          <tr align="right" class="tdbg">
            <td colspan="3" align="center" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view1ImgPath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">Caption1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view1ImgCap" styleClass="caption_input"/></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View2</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view2ImgPath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">Caption2 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view2ImgCap" styleClass="caption_input"/></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View3</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view3ImgPath" accept="image/gif,image/jpeg" styleClass="browse"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">Caption3 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view3ImgCap" styleClass="caption_input"/></td>
          </tr>
      </table>      </tr>
      <tr>
        <td height="50" colspan="3" align="center"><html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallUploadProductImg()"> Upload </html:button>
          <html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="28"> Cancel </html:button></td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center" class="help"><table width="98%" border="0" cellpadding="2">
            <tr>
              <td valign="top"><strong>Note:</strong> </td>
              <td align="left">The image uploaded should have a minimum resolution of 175dpi 
                and the minimum dimension of 920 x 1380 pixels.</td>
            </tr>
          </table>
          </td>
      </tr>
    </table>
     <html:hidden property="cateId" value="20"/>	
	</html:form>
	</td>
    </tr>
  
  <tr>
   <td colspan="3">
		<div class="nav-footer">
			<div class="nav-footer-right"></div>
			<div class="nav-footer-left"></div>
			<div class="nav-footer-content"></div>			
		</div>
		<!-- end nav-footer -->
	</td>
   
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
