welcome.Calendar._DN = new Array
(
	 "Sunday",
	 "Monday",
	 "Tuesday",
	 "Wednesday",
	 "Thursday",
	 "Friday",
	 "Saturday",
	 "Sunday"
 );
// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   welcome.Calendar._SDN_len = N; // short day name length
//   welcome.Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.
// short day names
welcome.Calendar._SDN = new Array
(
	 "Sun",
	 "Mon",
	 "Tue",
	 "Wed",
	 "Thu",
	 "Fri",
	 "Sat",
	 "Sun"
);
// First day of the week. "0" means display Sunday first, "1" means display
// Monday first, etc.
welcome.Calendar._FD = 0;
// full month names
welcome.Calendar._MN = new Array
(
	 "January",
	 "February",
	 "March",
	 "April",
	 "May",
	 "June",
	 "July",
	 "August",
	 "September",
	 "October",
	 "November",
	 "December"
 );
// short month names
welcome.Calendar._SMN = new Array
(
	 "Jan",
	 "Feb",
	 "Mar",
	 "Apr",
	 "May",
	 "Jun",
	 "Jul",
	 "Aug",
	 "Sep",
	 "Oct",
	 "Nov",
	 "Dec"
 );
// tooltips
welcome.Calendar._TT_en = welcome.Calendar._TT = {};
welcome.Calendar._TT["INFO"] = "About the calendar";
welcome.Calendar._TT["ABOUT"] ="Thanks for using Welcome Wagon Calendar\n\n"+
"Date selection:\n" +
"- Use the \xab, \xbb buttons to select year\n" +
"- Use the " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " buttons to select month\n" +
"- Hold mouse button on any of the above buttons for faster selection.";
welcome.Calendar._TT["PREV_YEAR"] = "Prev. year (hold for menu)";
welcome.Calendar._TT["PREV_MONTH"] = "Prev. month (hold for menu)";
welcome.Calendar._TT["GO_TODAY"] = "Go Today (hold for history)";
welcome.Calendar._TT["NEXT_MONTH"] = "Next month (hold for menu)";
welcome.Calendar._TT["NEXT_YEAR"] = "Next year (hold for menu)";
welcome.Calendar._TT["SEL_DATE"] = "Select date";
welcome.Calendar._TT["DRAG_TO_MOVE"] = "Drag to move";
welcome.Calendar._TT["PART_TODAY"] = "(today)";
// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
welcome.Calendar._TT["DAY_FIRST"] = "Display %s first";
// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
welcome.Calendar._TT["WEEKEND"] = "0,6";
welcome.Calendar._TT["CLOSE"] = "Close";
welcome.Calendar._TT["TODAY"] = "Today";
welcome.Calendar._TT["TIME_PART"] = "(Shift-)Click or drag to change value";
// date formats
welcome.Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
welcome.Calendar._TT["TT_DATE_FORMAT"] = "%a, %b %e";

welcome.Calendar._TT["WK"] = "wk";
welcome.Calendar._TT["TIME"] = "Time:";

welcome.Calendar._TT["E_RANGE"] = "Outside the range";

/* Preserve data */
	if(welcome.Calendar._DN) welcome.Calendar._TT._DN = welcome.Calendar._DN;
	if(welcome.Calendar._SDN) welcome.Calendar._TT._SDN = welcome.Calendar._SDN;
	if(welcome.Calendar._SDN_len) welcome.Calendar._TT._SDN_len = welcome.Calendar._SDN_len;
	if(welcome.Calendar._MN) welcome.Calendar._TT._MN = welcome.Calendar._MN;
	if(welcome.Calendar._SMN) welcome.Calendar._TT._SMN = welcome.Calendar._SMN;
	if(welcome.Calendar._SMN_len) welcome.Calendar._TT._SMN_len = welcome.Calendar._SMN_len;
	welcome.Calendar._DN = welcome.Calendar._SDN = welcome.Calendar._SDN_len = welcome.Calendar._MN = welcome.Calendar._SMN = welcome.Calendar._SMN_len = null
