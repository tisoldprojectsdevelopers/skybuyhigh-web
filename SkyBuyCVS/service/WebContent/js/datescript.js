var showDateShort = 1;
var showDate= 2;
var showDateTime= 3;
var showTime= 4;

function ShowDateTime(dateStyle)
{
	var today = new Date();
	var dStr = "";
	
	switch (dateStyle)
	{
		case showDateShort:
			dStr = today.toDateString();
			break;
		case showDateTime:
			dStr = today.toLocaleString();
			break;
		case showTime:
			dStr = today.toLocaleTimeString();
			break;
		case showDate:
		default:
			dStr = today.toLocaleDateString();
			break;
	}
	document.write(dStr);
}