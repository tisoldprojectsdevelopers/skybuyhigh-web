function addLoadEvent(func) { 
	var oldonload = window.onload; 
	if (typeof window.onload != 'function') { 
		window.onload = func; 
	} else { 
		window.onload = function() { 
			if (oldonload) { 
				oldonload(); 
			} 
			func(); 
		} 
	} 
}

if(document.getElementById && document.createTextNode) {
	addLoadEvent(checkSignIn); 
	addLoadEvent(initPage); 
	addLoadEvent(changeMenu); 
	addLoadEvent(searchSubmit);
	addLoadEvent(checkOS); 
	addLoadEvent(checkref); 
	addLoadEvent(newWin); 
}