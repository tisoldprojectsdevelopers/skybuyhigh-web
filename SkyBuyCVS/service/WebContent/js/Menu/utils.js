// #############################################################################
// lets define the browser we have instead of multiple calls throughout the file
var userAgent = navigator.userAgent.toLowerCase();
var is_opera  = (userAgent.indexOf('opera') != -1);
var is_saf    = ((userAgent.indexOf('applewebkit') != -1) || (navigator.vendor == "Apple Computer, Inc."));
var is_webtv  = (userAgent.indexOf('webtv') != -1);
var is_ie     = ((userAgent.indexOf('msie') != -1) && (!is_opera) && (!is_saf) && (!is_webtv));
var is_ie4    = ((is_ie) && (userAgent.indexOf("msie 4.") != -1));
var is_moz    = ((navigator.product == 'Gecko') && (!is_saf));
var is_kon    = (userAgent.indexOf('konqueror') != -1);
var is_ns     = ((userAgent.indexOf('compatible') == -1) && (userAgent.indexOf('mozilla') != -1) && (!is_opera) && (!is_webtv) && (!is_saf));
var is_ns4    = ((is_ns) && (parseInt(navigator.appVersion) == 4));

// catch possible bugs with WebTV and other older browsers
var is_regexp = (window.RegExp) ? true : false;


// #############################################################################
// let's find out what DOM functions we can use
var vbDOMtype = '';
if (document.getElementById)
{
	vbDOMtype = "std";
}
else if (document.all)
{
	vbDOMtype = "ie4";
}
else if (document.layers)
{
	vbDOMtype = "ns4";
}

// #############################################################################
// simple function to toggle the 'display' attribute of an object
function toggle_display(idname)
{
	obj = fetch_object(idname);
	if (obj)
	{
		if (obj.style.display == "none")
		{
			obj.style.display = "";
		}
		else
		{
			obj.style.display = "none";
		}
	}
	return false;
}

// make an array to store cached locations of objects called by fetch_object
var vBobjects = new Array();

// #############################################################################
// function to emulate document.getElementById
function fetch_object(idname, forcefetch)
{
	if (forcefetch || typeof(vBobjects[idname]) == "undefined")
	{
		switch (vbDOMtype)
		{
			case "std":
			{
				vBobjects[idname] = document.getElementById(idname);
			}
			break;

			case "ie4":
			{
				vBobjects[idname] = document.all[idname];
			}
			break;

			case "ns4":
			{
				vBobjects[idname] = document.layers[idname];
			}
			break;
		}
	}
	return vBobjects[idname];
}


// #############################################################################
// ##################### Cookie Functions ############################
// #############################################################################

// #############################################################################
// function to set a cookie
function set_cookie(name, value, expires)
{
	if (!expires)
	{
		expires = new Date();
	}
	document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString() +  "; path=/";
}

// #############################################################################
// function to retrieve a cookie
function fetch_cookie(name)
{
	cookie_name = name + "=";
	cookie_length = document.cookie.length;
	cookie_begin = 0;
	while (cookie_begin < cookie_length)
	{
		value_begin = cookie_begin + cookie_name.length;
		if (document.cookie.substring(cookie_begin, value_begin) == cookie_name)
		{
			var value_end = document.cookie.indexOf (";", value_begin);
			if (value_end == -1)
			{
				value_end = cookie_length;
			}
			return unescape(document.cookie.substring(value_begin, value_end));
		}
		cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
		if (cookie_begin == 0)
		{
			break;
		}
	}
	return null;
}

// #############################################################################
// function to delete a cookie
function delete_cookie(name)
{
	var expireNow = new Date();
	document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
}




function isdefined(variable ) { 
    if(typeof(variable) !="undefined") return true; else return false; 
}









function getCheckedGroupValue( formObj, groupName ) {
    for (var i=0;i<formObj.elements.length;i++) {
        var e = formObj.elements[i];
        if (e.name==groupName && e.checked) {
            return e.value;
        }
    }
}



function setGroupEnabled( formObj, groupName, isEnabled ) {
    for (var i=0;i<formObj.elements.length;i++) {
        var e = formObj.elements[i];
        if (e.name==groupName) {
            e.disabled = !isEnabled;
        }
    }
}

function setFormEnabled( formObj, isEnabled ) {
    for (var i=0;i<formObj.elements.length;i++) {
        var e = formObj.elements[i];
        e.disabled = !isEnabled;
    }
}


function CheckAll( groupName, bState ) {
    for (var i=0;i<document.inputForm.elements.length;i++) {
        var e = document.inputForm.elements[i];
        if (e.name==groupName) {
            e.checked = bState;
        }
    }
}

function checkAll( formObj, groupName, bState ) {
    if (formObj == null)
        return;

    for (var i=0;i<formObj.elements.length;i++) {
        var e = formObj.elements[i];
        if (e.name==groupName) {
            e.checked = bState;
        }
    }
}


function addOption(name, val, selectedVal) {
    document.write('<option ');
    if (selectedVal!=null && selectedVal == val ) document.write(' selected ');
    document.write('value="');
    document.write(val);
    document.write('">');
    document.write(name);
}

function createSelect(name, option, arrayList, selectedVal) {
    document.write('<select name="');
    document.write(name);
    document.write('" ');
    document.write(option);
    document.write('> ');

    if (arrayList!=null) {
        for (var i=0; i < arrayList.length; i++) {
            addOption(arrayList[i], arrayList[i], selectedVal);
        }
    }
    document.write('</select>');
}


function getTodayDateString() {
	var today = new Date();
	today_s = today.getMonth()+1 + '/' + today.getDate() + '/' + today.getYear();
	return today_s;
}


function selectAll(object) {
	for (var i=0;i < object.options.length;i++) {
		object.options[i].selected = true;
	}
}

function unSelectAll(object) {
	for (var i=0;i < object.options.length;i++) {
		object.options[i].selected = false;
	}
}

function toBeDone() {
    alert('Sorry this feature is not yet available!');
}

function isOptionInSelect(selectObject, targetOption) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].value == targetOption.value)
            return true;
    }
    return false;
}

function isOptionTextInSelect(selectObject, targetOption) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].text == targetOption.text)
            return true;
    }
    return false;
}


function getOptionIndex(selectObject, targetOption) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].value == targetOption.value)
            return i;
    }
    return -1;
}

function selectOptionName(selectObject, selectName) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].name == selectName) {
            selectObject.options[i].selected=true;
            return true;
        }
    }
    return false;
}

function selectOptionValue(selectObject, selectName) {
    if ( isdefined(selectObject) ) {
        if (  isdefined(selectObject.options) ) {
            for (var i=0; i < selectObject.options.length; i++) {
                if (selectObject.options[i].value == selectName) {
                    selectObject.options[i].selected=true;
                    return true;
                }
            }
            return false;
        }
    }
    
    return false;
}

function selectOptionValueByOptionText(selectObject, selectName) {
    if ( isdefined(selectObject) ) {
        if (  isdefined(selectObject.options) ) {
            for (var i=0; i < selectObject.options.length; i++) {
                if (selectObject.options[i].text == selectName) {
                    selectObject.options[i].selected=true;
                    return true;
                }
            }
            return false;
        }
    }

    return false;
}

function moveTo(sObj, tObj, noSort) {
    moveOptions(sObj, tObj, false);
     /*to sort or not to sort...
       */
    if (typeof noSort == "undefined" || noSort == false) {
        sortOptions(tObj);
    }
    /* set on target's first item
     */
    tObj.focus();
    tObj.selectedIndex = 0;
}

function sortOptions(sObj) {
    if (sObj.type == "select-one" || sObj.type == "select-multiple") {
        var FIELD_DELIM = "::";
        var tArr = new Array;
        /* get the collection of text values
         */
        for (var i = 0; i < sObj.length; i++) {
            /* wouldn't it be great if the options array worked
               just like an array so you could use .sort() or even
               something like .join()... and worked like you expected...
             */
            tArr[i] = sObj[i].text + FIELD_DELIM + sObj[i].value;
        }
        /* sort on text content
         */
        tArr.sort();
        /* truncate list and rebuild in new order
         */
        sObj.length = 0;
        for (var i = 0; i < tArr.length; i++) {
            var tVal = tArr[i];
            /* slice the string into text and value and replace
               in new option -- note: using RE in split = JS 1.2
             */
            var s1 = tVal.split(FIELD_DELIM);
            tOpt = new Option
            tOpt.text = s1[0];
            tOpt.value = s1[1];
            sObj[sObj.length] = tOpt;
            tOpt = null;
        }
    }
}

function moveOptions(sObj, tObj, all) {
    if (sObj.type == "select-one" || sObj.type == "select-multiple") {
        if (all) {
            copyOptions(sObj, tObj, all);
            /* truncate source list
             */
            sObj.length = 1;
            sObj[0].value = "";
            sObj[0].text = "[None]";
        } else {
            /* move needs a WHILE loop since we're scanning for
               selected items and the list length will change
             */
            var safetyCnt = 0;
            /* so we need the original length...
             */
            var safetyMax = sObj.length;
            while (sObj.selectedIndex != "-1" && safetyCnt <= safetyMax) {
                copyOption(sObj, sObj.selectedIndex, tObj, true);
                safetyCnt++; // safety loop
            }
            /* if we've cleared last entry in source
             */
            if (sObj.length == 0) {
                 sObj.length = 1;
                 sObj[0].text = "[None]";
                 sObj[0].value = "";
            }
        }
    }
}
/*
function copyOption(sObj, currOpt, tObj, clearSrc) {
    // not src text? empty delim? skip...
    if (typeof sObj[currOpt].text != "undefined") {
        if (sObj[currOpt].text != "[None]") {
            // clone the option

            var tOpt = new Option;
            tOpt.text = sObj[currOpt].text;
            tOpt.value = sObj[currOpt].value;
            // first entry in an empty list?
            if (tObj.length == 1 && tObj[0].text == "[None]") {
                tObj[0] = tOpt;
            } else {
                //insert clone into target
                tObj[tObj.length] = tOpt;
            }
            tOpt = null;
            if (typeof clearSrc != "undefined" && clearSrc == true) {
                sObj[currOpt] = null;
            }
        }
    }
}
*/









function copyOption(sObj, currOpt, tObj, clearSrc) {
    /* not src text? empty delim? skip...*/
    if (typeof sObj[currOpt].text != "undefined") {
        if (sObj[currOpt].text.toUpperCase().indexOf("NONE") == -1 ) {
            /* clone the option*/
            var tOpt = new Option;
            tOpt.text = sObj[currOpt].text;
            tOpt.value = sObj[currOpt].value;
            /* first entry in an empty list?*/
            if (tObj.length == 1 && tObj[0].text.toUpperCase().indexOf("NONE") != -1) {
                tObj[0] = tOpt;
                tObj.selectedIndex = 0;
            } else {
                /* insert clone into target*/
                tObj[tObj.length] = tOpt;
                tObj.selectedIndex = tObj.length-1;
            }
            tOpt = null;
            if (typeof clearSrc != "undefined" && clearSrc == true) {
                sObj[currOpt] = null;
            }
        }
    }
}














function moveAllTo(sObj, tObj, noSort) {
    moveOptions(sObj, tObj, true);
    /* to sort or not to sort...
     */
    if (typeof noSort == "undefined" || noSort == false) {
        sortOptions(tObj);
    }
    /* set on target's first item
     */
    tObj.focus();
    tObj.selectedIndex = 0;
}


//function copyOptions(sObj, tObj, all) {
        //if (sObj.type == "select-one" || sObj.type == "select-multiple") {
            /* if we're not copying all, and there's no selections, skip it
             */
            //if (all || sObj.selectedIndex != "-1") {
                //for (var idx=0; idx < sObj.length; idx++) {
                    //if (all || sObj[idx].selected == true) {

                        //copyOption(sObj, idx, tObj, false);
                    //}
                //}
            //}
        //}
    //}





function copyOptions(sObj, tObj) {
    copyOptions(sObj, tObj, "all");
}

function copyOptions(sObj, tObj, all) {
    if (sObj.type == "select-one" || sObj.type == "select-multiple") {
        /* if we're not copying all, and there's no selections, skip it*/
        if (all || sObj.selectedIndex != "-1") {
            for (var idx=0; idx < sObj.length; idx++) {
                if (all || sObj[idx].selected == true) {
                    if (!isOptionValueInSelect(tObj, sObj[idx].value) ) {
                        copyOption(sObj, idx, tObj, false);
                    }
                }
            }
        }
    }
}



function transferSelectedOption(source, target) {
    var index = source.selectedIndex;
    if (index > -1) {
        var newoption = new Option(source.options[index].text, source.options[index].value, true, true);
        target.options[target.length] = newoption;

        source.options[index] = null;
        source.selectedIndex = 0;
    }
}




function transferAllOptions(source, target) {
    for (var i=0; i < source.options.length; i++) {
        var newoption = new Option(source.options[i].text, source.options[i].value, true, true);
        target.options[target.length] = newoption;
    }

    for (var i=source.options.length; i >-1 ; i--) {
        source.options[i] = null;
    }

    source.selectedIndex = 0;
}





























function removeOption(sObj) {
    removeOption(sObj, false);
}
function removeOption(sObj, all) {
    if (sObj.type == "select-one" || sObj.type == "select-multiple") {
        if (all) {
            sObj.length = 1;
            sObj[0].value = "";
            sObj[0].text = "[None]";
        } else {

            for (var i=0; i < sObj.options.length; i++) {
                if (sObj.options[i].selected == true) {
                    sObj[i] = null;
                }
            }

            if (sObj.length == 0) {
                 sObj.length = 1;
                 sObj[0].text = "[None]";
                 sObj[0].value = "";
            }
        }
    }
}

function removeOptionByValue(selectObject, value) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].value == value) {
            selectObject.options[i] = null;
            return;
        }
    }
}

function isOptionValueInSelect(selectObject, value) {
    for (var i=0; i < selectObject.options.length; i++) {
        if (selectObject.options[i].value == value)
            return true;
    }
    return false;
}




function isEmpty(s) {
    return ((s == null) || (s.length == 0))
}

function isWhitespace (s) {
    var i;
	var whitespace=" ";
    // Is s empty?
    if (isEmpty(s)) return true;

    // Search through string's characters one by one
    // until we find a non-whitespace character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++) {
        // Check that current character isn't whitespace.
        var c = s.charAt(i);

        if (whitespace.indexOf(c) == -1) return false;
    }

    // All characters are whitespace.
    return true;
}


function isDigit (c){
    return ((c >= "0") && (c <= "9"))
}

function isFloat (s) {
    var i;
    var seenDecimalPoint = false;
    var decimalPointDelimiter = "."

    if (s == decimalPointDelimiter) return false;

    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);

        if ((c == decimalPointDelimiter) && seenDecimalPoint) {
            return false;
        } else {
            if (c==decimalPointDelimiter) {
                seenDecimalPoint = true;
            } else {
                if (!isDigit(c)) {
                    return false;
                }
            }
        }
    }
    // All characters are numbers.
    return true;
}


//The window opener
var streamerWin = null;

function openWindowAutoSize(url, name) {
//Automatically determines the width and height of the window.

//make sure the window is closed
    if (!streamerWin || streamerWin.closed) {

        //Hook myself up with the size variables
        var w0, h0;


        var wint, winl;

        //These are the parameters - refer to the VAR "variables"
        var dirs = "no";
        var loc = "no";
        var menu = "no";
        var scrs = "yes";
        var stat = "no";
        var tool = "no";
        var resize ="yes";

        //this sets the width in pixels
        if (screen.width <= 800) {
            w0=parseInt(700);
            h0=parseInt(425);

        } else if (screen.width == 1024) {
            w0=parseInt(800);
            h0=parseInt(600);
        } else {
            w0=parseInt(815);
            h0=parseInt(820);
        }

        //if screen resolution is more than 800x600, shrink the window

        //make sure the height and width are integers
        //w0 = parseInt(w0);
        //h0 = parseInt(h0);

        winl = (screen.width-w0)/2;
        wint = (screen.height-h0)/2;

        //set up the window property variables for interpolation
        var features =
                        'width='        + w0 +
                        ',height='      + h0 +
                        ',top='         + wint +
                        ',left='         + winl +
                        ',directories=' + dirs +
                        ',location='    + loc +
                        ',menubar='     + menu +
                        ',scrollbars='  + scrs +
                        ',status='      + stat +
                        ',toolbar='     + tool +
                        ',resizable='  + resize;

        //open the window already
        streamerWin = window.open(url, name, features);

    } else {
        streamerWin.document.location = url;
        streamerWin.focus();
    }

    if (streamerWin.opener == null) streamerWin.opener = self;
}



function openWindow(url, name, w, h) {

//make sure the window is closed
    if (!streamerWin || streamerWin.closed) {

        //Hook myself up with the size variables
        var w0, h0;

        var wint, winl;

        //These are the parameters - refer to the VAR "variables"
        var dirs = "no";
        var loc = "no";
        var menu = "no";
        var scrs = "yes";
        var stat = "no";
        var tool = "no";
        var resize ="yes";

        //this sets the width in pixels
        w0 = w;
        h0 = h;

        //if screen resolution is more than 800x600, shrink the window

        //make sure the height and width are integers
        w0 = parseInt(w0);
        h0 = parseInt(h0);

        winl = (screen.width-w0)/2;
        wint = (screen.height-h0)/2;

        //set up the window property variables for interpolation
        var features =
                        'width='        + w0 +
                        ',height='      + h0 +
                        ',top='         + wint +
                        ',left='         + winl +
                        ',directories=' + dirs +
                        ',location='    + loc +
                        ',menubar='     + menu +
                        ',scrollbars='  + scrs +
                        ',status='      + stat +
                        ',toolbar='     + tool +
                        ',resizable='  + resize;

        //open the window already
        streamerWin = window.open(url, name, features);

    } else {
        streamerWin.document.location = url;
        streamerWin.focus();
    }

    if (streamerWin.opener == null) streamerWin.opener = self;
}


function openWindow(url, name, w, h, tool, menu, stat) {

//make sure the window is closed
    if (!streamerWin || streamerWin.closed) {

        //Hook myself up with the size variables
        var w0, h0;

        var wint, winl;

        //These are the parameters - refer to the VAR "variables"
        var dirs = "no";
        var loc = "no";

        var scrs = "yes";
        if (!menu)
            menu = "no";
        else
            menu = "yes";

        if (!stat)
            stat = "no";
        else
            stat = "yes";

        if (!tool)
            tool = "no";
        else
            tool = "yes";

        var resize ="yes";

        //this sets the width in pixels
        w0 = w;
        h0 = h;

        //if screen resolution is more than 800x600, shrink the window

        //make sure the height and width are integers
        w0 = parseInt(w0);
        h0 = parseInt(h0);

        winl = (screen.width-w0)/2;
        wint = (screen.height-h0)/2;

        //set up the window property variables for interpolation
        var features =
                        'width='        + w0 +
                        ',height='      + h0 +
                        ',top='         + wint +
                        ',left='         + winl +
                        ',directories=' + dirs +
                        ',location='    + loc +
                        ',menubar='     + menu +
                        ',scrollbars='  + scrs +
                        ',status='      + stat +
                        ',toolbar='     + tool +
                        ',resizable='  + resize;

        //open the window already
        streamerWin = window.open(url, name, features);

    } else {
        streamerWin.document.location = url;
        streamerWin.focus();
    }

    if (streamerWin.opener == null) streamerWin.opener = self;
}




function checkdate(objName) {
    var datefield = objName;
    if (chkdate(objName) == false) {
        //datefield.select();
        alert("That date is invalid.  Please try again.");
        //datefield.focus();
        return false;
        }
        else {
            return true;
        }
}

function chkdate(objName) {
    var strDatestyle = "US"; //United States date style
    //var strDatestyle = "EU";  //European date style
    var strDate;
    var strDateArray;
    var strDay;
    var strMonth;
    var strYear;
    var intday;
    var intMonth;
    var intYear;
    var booFound = false;
    var datefield = objName;
    var strSeparatorArray = new Array("-"," ","/",".");
    var intElementNr;
    var err = 0;
    var strMonthArray = new Array(12);
    strMonthArray[0] = "Jan";
    strMonthArray[1] = "Feb";
    strMonthArray[2] = "Mar";
    strMonthArray[3] = "Apr";
    strMonthArray[4] = "May";
    strMonthArray[5] = "Jun";
    strMonthArray[6] = "Jul";
    strMonthArray[7] = "Aug";
    strMonthArray[8] = "Sep";
    strMonthArray[9] = "Oct";
    strMonthArray[10] = "Nov";
    strMonthArray[11] = "Dec";
    strDate = datefield;

    if (strDate.length < 1) {
        return true;
    }

    for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
        if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
            strDateArray = strDate.split(strSeparatorArray[intElementNr]);
            if (strDateArray.length != 3) {
                err = 1;
                return false;
            }
            else {
                strDay = strDateArray[0];
                strMonth = strDateArray[1];
                strYear = strDateArray[2];
            }

            booFound = true;
        }
    }

    if (booFound == false) {
        if (strDate.length>5) {
            strDay = strDate.substr(0, 2);
            strMonth = strDate.substr(2, 2);
            strYear = strDate.substr(4);
        }
    }

    if (strYear.length == 2) {
        strYear = '20' + strYear;
    }
    // US style
    if (strDatestyle == "US") {
        strTemp = strDay;
        strDay = strMonth;
        strMonth = strTemp;
    }
    intday = parseInt(strDay, 10);
    if (isNaN(intday)) {
        err = 2;
        return false;
    }

    intMonth = parseInt(strMonth, 10);
    if (isNaN(intMonth)) {
        for (i = 0;i<12;i++) {
            if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
                intMonth = i+1;
                strMonth = strMonthArray[i];
                i = 12;
            }
        }
        if (isNaN(intMonth)) {
            err = 3;
            return false;
        }
    }

    intYear = parseInt(strYear, 10);
    if (isNaN(intYear)) {
        err = 4;
        return false;
    }
    if (intMonth>12 || intMonth<1) {
        err = 5;
        return false;
    }
    if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
        err = 6;
        return false;
    }
    if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
        err = 7;
        return false;
    }

    if (intMonth == 2) {
        if (intday < 1) {
            err = 8;
            return false;
        }
        if (LeapYear(intYear) == true) {
            if (intday > 29) {
                err = 9;
                return false;
            }
        }
        else {
            if (intday > 28) {
                err = 10;
                return false;
            }
        }
    }

    if (strDatestyle == "US") {
        datefield = strMonthArray[intMonth-1] + " " + intday+" " + strYear;
    }
    else {
        datefield = intday + " " + strMonthArray[intMonth-1] + " " + strYear;
    }

    return true;
}


function LeapYear(intYear) {
    if (intYear % 100 == 0) {
        if (intYear % 400 == 0) { return true; }
    }
    else {
        if ((intYear % 4) == 0) { return true; }
    }
    return false;
}


function doDateCheck(from, to) {
    if (Date.parse(from) < Date.parse(to)) {
        return true;
    }
    else {
        if (from.value == "" || to.value == "")
            alert("Both dates must be entered.");
        else
            alert("End date date must occur after the Start date.");
   }
   return false;
}


function currencyUnFormat(target) {
    var newVal = new String("");
    for (var i=0; target != null && i<target.length; i++) {
        if (target.charAt(i) != '$' && target.charAt(i) != ',')
            newVal = newVal + target.charAt(i);
    }
    return newVal;
}

function currencyFormat(target) {

    var blankString = new String ("");
    var newVal = currencyUnFormat(target);

    // convert blank entry to 0
    trimVal = trim(newVal);
    if (trimVal == blankString) {
        newVal = 0;
    }

    // check for mm
    if (isNaN(newVal)) {
        mm = newVal.substr(newVal.length-2);

        if (newVal == 'mm' || newVal =='MM') {
            newVal = '1000000';
        } else if (mm == 'mm' || mm == 'MM') {
            var n = Number(newVal.substr(0, newVal.length-2));
            if (isNaN(n))
                return null;

            newVal = Number(newVal.substr(0, newVal.length-2)) * 1000000;
        } else {
            return null;
        }
    }

    // clear to perform the currency format (newVal now 5000)
    //

    anynum=eval(newVal);
    workNum=Math.abs((Math.round(anynum*100)/100));

    workStr=""+workNum;
    if (workStr.indexOf(".")==-1){
        workStr+=".00";
    }

    dStr=workStr.substr(0,workStr.indexOf("."));
    dNum=dStr-0;
    pStr=workStr.substr(workStr.indexOf("."));

    while (pStr.length<3){
        pStr+="0";
    }

    if (dNum>=1000) {
        dLen=dStr.length;
        dStr=parseInt(""+(dNum/1000))+","+dStr.substring(dLen-3,dLen);
    }

    if (dNum>=1000000) {
        dLen=dStr.length;
        dStr=parseInt(""+(dNum/1000000))+","+dStr.substring(dLen-7,dLen);
    }

    if (dNum>=1000000000) {
        dLen=dStr.length;
        dStr=parseInt(""+(dNum/1000000000))+","+dStr.substring(dLen-11,dLen);
    }

    retval = dStr;// + pStr;

    if (anynum<0) {
    retval="("+retval+")";
    }

    return ("$"+retval);
}


function doAction(formObj, methodType, actionURL) {
    formObj.action=actionURL;
    formObj.method=methodType;
    formObj.submit();
}




function trim(strText) {

    // this will get rid of leading spaces

    while (strText.substring(0,1) == ' ' || strText.substring(0,1).charCodeAt(0) == 160)
        strText = strText.substring(1, strText.length);

    // this will get rid of trailing spaces
    while (strText.substring(strText.length-1,strText.length) == ' ' || strText.substring(strText.length-1,strText.length).charAt(0) == 160)
        strText = strText.substring(0, strText.length-1);

   return strText;

}

function indentedUnitSelectChange(obj) {

    if (obj==null || obj.options==null || obj.options.length<=1)
        return;


    if (obj.options[obj.selectedIndex].value == '' || (obj.options[obj.selectedIndex].value == '-1' && obj.options[obj.selectedIndex].text.toUpperCase() != 'ALL')) {
        obj.selectedIndex = 0;
        return;
    }

    var s = obj.options[obj.selectedIndex].text;
    obj.options[0] = new Option(trim(s), obj.options[obj.selectedIndex].value, true, false);
    obj.selectedIndex = 0;
}





var base64s =
 "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

function base64Encode(decStr){
  var bits, dual, i = 0, encOut = '';
  while(decStr.length >= i + 3){
    bits =
    (decStr.charCodeAt(i++) & 0xff) <<16 |
    (decStr.charCodeAt(i++) & 0xff) <<8  |
     decStr.charCodeAt(i++) & 0xff;
    encOut +=
     base64s.charAt((bits & 0x00fc0000) >>18) +
     base64s.charAt((bits & 0x0003f000) >>12) +
     base64s.charAt((bits & 0x00000fc0) >> 6) +
     base64s.charAt((bits & 0x0000003f));
    }
  if(decStr.length -i > 0 && decStr.length -i < 3){
    dual = Boolean(decStr.length -i -1);
    bits =
     ((decStr.charCodeAt(i++) & 0xff) <<16) |
     (dual ? (decStr.charCodeAt(i) & 0xff) <<8 : 0);
    encOut +=
      base64s.charAt((bits & 0x00fc0000) >>18) +
      base64s.charAt((bits & 0x0003f000) >>12) +
      (dual ? base64s.charAt((bits & 0x00000fc0) >>6) : '=') +
      '=';
    }
  return encOut
  }

function closeWindow(){
    if(streamerWin && !streamerWin.closed && streamerWin.open) {
        streamerWin.close();
        streamerWin=null;
    }
}





function Trim(TRIM_VALUE){
    if(TRIM_VALUE.length < 1){
        return"";
    }
    TRIM_VALUE = RTrim(TRIM_VALUE);
    TRIM_VALUE = LTrim(TRIM_VALUE);
    if(TRIM_VALUE==""){
        return "";
    } else{
        return TRIM_VALUE;
    }
} //End Function


function RTrim(VALUE){
    var w_space = String.fromCharCode(32);
    var v_length = VALUE.length;
    var strTemp = "";
    if(v_length < 0){
        return"";
    }
    var iTemp = v_length -1;

    while(iTemp > -1){
        if(VALUE.charAt(iTemp) == w_space){
        } else{
            strTemp = VALUE.substring(0,iTemp +1);
            break;
        }
        iTemp = iTemp-1;    
    } //End While
    return strTemp;
} //End Function



function LTrim(VALUE){
    var w_space = String.fromCharCode(32);
    if(v_length < 1){
        return"";
    }
    var v_length = VALUE.length;
    var strTemp = "";

    var iTemp = 0;

    while(iTemp < v_length){
        if(VALUE.charAt(iTemp) == w_space){
        }else{
            strTemp = VALUE.substring(iTemp,v_length);
            break;
        }
        iTemp = iTemp + 1;
    } //End While
    return strTemp;
} //End Function




function addEvent(obj, evType, fn)
{
	if (obj.addEventListener)
	{
		obj.addEventListener(evType, fn, true);
		return true;
	} 
	else if (obj.attachEvent)
	{
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	} 
	else 
	{
		return false;
	}
}


function setOpacity(node,val) {
    if (node.filters) {
        try {
            node.filters['alpha'].opacity = val*100;
        } catch (e) { }
    } else if (node.style.opacity) {
        node.style.opacity = val;
    }
}


function toggleBox(szDivID, iState) { // 1 visible, 0 hidden
    if(document.layers) {      //NN4+
       document.layers[szDivID].visibility = iState ? "show" : "hide";
        document.layers[szDivID].display = iState ? "inline" : "none";
    }
    else if(document.getElementById) {     //gecko(NN6) + IE 5+
        var obj = document.getElementById(szDivID);
        if ( obj == null ) {
            return;
        }

        obj.style.visibility = iState ? "visible" : "hidden";
        obj.style.display = iState ? "inline" : "none";
    } else if(document.all) {   // IE 4
        document.all[szDivID].style.visibility = iState ? "visible" : "hidden";
        document.all[szDivID].style.display = iState ? "inline" : "none";
    }
}

function modifydiv(divname,val) {
    oDiv = eval("document.all."+divname);
    if(val) {
         oDiv.style.visibility = "visible";
         oDiv.style.display = "inline";
    } else {
         oDiv.style.visibility = "hidden";
         oDiv.style.display = "none";
    }
}


/*
Submit Once form validation-
*/

function submitonce(theform){
    //if IE 4+ or NS 6+
    if (document.all||document.getElementById){
        //screen thru every element in the form, and hunt down "submit" and "reset"
        for (i=0;i<theform.length;i++){
            var tempobj=theform.elements[i]
            if(tempobj.type.toLowerCase()=="submit"||tempobj.type.toLowerCase()=="reset") {
                //disable em
                tempobj.disabled=true
            }
        }
    }
}

// function to check ie ver 7 and its lower version
function Is() {
agent = navigator.userAgent.toLowerCase();
this.major = parseInt(navigator.appVersion);
this.minor = parseFloat(navigator.appVersion);
this.ns = ((agent.indexOf('mozilla') != -1) &&
(agent.indexOf('spoofer') == -1) &&
(agent.indexOf('compatible') == -1) &&
(agent.indexOf('opera') == -1) &&
(agent.indexOf('webtv') == -1) &&
(agent.indexOf('hotjava') == -1));
this.ns2 = (this.ns && (this.major == 2));
this.ns3 = (this.ns && (this.major == 3));
this.ns4 = (this.ns && (this.major == 4));
this.ns6 = (this.ns && (this.major >= 5));
this.ie = ((agent.indexOf("msie") != -1) &&
(agent.indexOf("opera") == -1));
this.ie3 = (this.ie && (this.major < 4));
this.ie4 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 4") != -1));
this.ie5 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 5.") != -1) &&
(agent.indexOf("msie 5.5") == -1) &&
(agent.indexOf("mac") == -1));
this.iem5 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 5.") != -1) &&
(agent.indexOf("mac") != -1));
this.ie55 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 5.5") != -1));
this.ie6 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 6.") != -1));
this.ie7 = (this.ie && (this.major == 4) &&
(agent.indexOf("msie 7.0") != -1));
this.nsdom = (this.ns4 || this.ns6);
this.ie5dom = (this.ie5 || this.iem5 || this.ie55);
this.iedom = (this.ie4 || this.ie5dom || this.ie6);
this.w3dom = (this.ns6 || this.ie6 ||this.ie7);
}

