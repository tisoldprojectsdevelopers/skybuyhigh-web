function initPage()
{
	var navRoot = document.getElementById("log-block");
	if (navRoot) 
	{
		navRoot.onmouseover = function()
		{
			this.className = "hover";
                }
		navRoot.onmouseout = function()
		{
			this.className = "";
		}
	}
	var navRoot = document.getElementById("searchmenu");
	if (navRoot) 
	{
		navRoot.onmouseover = function()
		{
			this.className = "hover";
                }
		navRoot.onmouseout = function()
		{
			this.className = "";
		}
	}

}

function toggleVisibility(elem) {
	if (elem.className == "show") {
		elem.className = elem.className.replace ("show","hide");
	} else if (elem.className == "hide") {
		elem.className = elem.className.replace ("hide","show");
	}
}

function hideClass(elem) {
	if (elem.className == "show")
	elem.className = elem.className.replace ("show","hide");
} 
	
function showClass(elem) {
	if (elem.className == "hide")
	elem.className = elem.className.replace ("hide","show");
}

/* Checks if the user is logged in or not and displays logged in or not logged state in the header */
function checkSignIn() {
	var username =fetch_cookie('username');
	var thumbnail =fetch_cookie('thumbnail');

	var loggedInUser = document.getElementById("userInfoLoggedIn");
	var loggedOutUser = document.getElementById("userInfoLoggedOut");
	var loggedInNav = document.getElementById("navLoggedIn");
	var loggedOutNav = document.getElementById("navLoggedOut");
	var searchBox = document.getElementById("masthead-search");
	var formHide = document.getElementById("search-form");
	
	var usernick = document.getElementById("nickname");
	var thumb = document.getElementById("userpic");
	var logoutLink = document.getElementById("log-out");

	if ( username == null || username==''){
		toggleVisibility(loggedOutNav);
		toggleVisibility(loggedOutUser);
		toggleVisibility(formHide);
		return;
	} else {
		toggleVisibility(loggedInNav);
		toggleVisibility(loggedInUser);
		toggleVisibility(formHide);
		searchBox.className = searchBox.className.replace ("search-logout","search-login");
		nick = document.createTextNode(username);
		thumb.src = thumbnail;
		usernick.appendChild(nick);
		logoutLink.onclick = function() { document.logout.submit(); }
		return;
	}
}

/* Changes the search domain in the header's search */
function changeMenu(){
	domainSelect = document.getElementById('drop-arrow');
	chatSearch = document.getElementById('searchChat');
	memberSearch = document.getElementById('searchMembers');
	newsSearch = document.getElementById('searchNews');
	
	chatSearch.onclick = function () {
		document.frmMenu.searchTarget.value = 0;
		for ( var i=0; i<domainSelect.childNodes.length; i++) {
			if (domainSelect.childNodes[i].nodeType == 1 && domainSelect.childNodes[i].firstChild.nodeValue == 'Chat Rooms')
				showClass(domainSelect.childNodes[i]);
			else 
				hideClass(domainSelect.childNodes[i]);
		}
	}

	memberSearch.onclick = function () {
		document.frmMenu.searchTarget.value = 1;
		for ( var i=0; i<domainSelect.childNodes.length; i++) {
			if (domainSelect.childNodes[i].nodeType == 1 && domainSelect.childNodes[i].firstChild.nodeValue == 'Members' )
				showClass(domainSelect.childNodes[i]);
			else 
				hideClass(domainSelect.childNodes[i]);
		}
	}

	newsSearch.onclick = function () {
		document.frmMenu.searchTarget.value = 2;
		for ( var i=0; i<domainSelect.childNodes.length; i++) {
			if (domainSelect.childNodes[i].nodeType == 1 && domainSelect.childNodes[i].firstChild.nodeValue == 'News & Support' )
				showClass(domainSelect.childNodes[i]);
			else 
				hideClass(domainSelect.childNodes[i]);
		}
	}
}

/* Controls the search functionality in the header */
function searchSubmit(){
	var checkInput = document.getElementById('s');
	var clickDetect = document.getElementById('go');
	var submitDetect = document.frmMenu;
	checkInput.onfocus = function() { if(this.value=='Enter search term(s)') this.value=''; }
	clickDetect.onclick = function() { setSearchTarget(); }
	submitDetect.onsubmit = function() { setSearchTarget(); return false; }
}

function setSearchTarget(){
	var search_type = document.frmMenu.searchTarget.value;
	if(search_type==null || search_type==""){
		search_type=0;
	}
	var search_val = document.frmMenu.mainsearch.value;
		if(isWhitespace(search_val) || search_val=="Enter search term(s)"){
			Highlight("mainsearch");
			return;
		}else if(search_val!=null && search_val<3){
			alert("please enter minimum of 3 characters");
			return;
		}

	if ( search_type==0){					            
		document.groups_search.searchkey.value=search_val;
		document.groups_search.submit();

	}else if ( search_type==1){
		document.people_search.tag.value=search_val;
		document.people_search.submit();	
		
	}else if ( search_type==2){
		document.google_search.q.value=search_val;
		document.google_search.submit();

	}
}

// This is the amount of time (in milliseconds) that will lapse between each step in the fade
var FadeInterval = 600;

// This is where the fade will start, if you want it to be faster and start with a lighter color, make this number smaller
// It corresponds directly to the FadeSteps below
var StartFadeAt = 7;

// This is list of steps that will be used for the color to fade out
var FadeSteps = new Array();
	FadeSteps[1] = "ff";
	FadeSteps[2] = "ee";
	FadeSteps[3] = "dd";
	FadeSteps[4] = "cc";
	FadeSteps[5] = "bb";
	FadeSteps[6] = "aa";
	FadeSteps[7] = "99";

// This function is just a small wrapper to use for the oncick events of the anchors
function Highlight(target) {
	// Get the target ID from the string that was passed to the function    
	DoFade(StartFadeAt, target);
}

// This is the recursive function call that actually performs the fade
function DoFade(colorId, targetId) {
	 if (colorId >= 1) {
		document.getElementById(targetId).style.background = "#ffff" + FadeSteps[colorId];
		// If it's the last color, set it to transparent
        if (colorId==1) {
        	document.getElementById(targetId).style.background = "#fff";
		}
        colorId--;		
        // Wait a little bit and fade another shade
        setTimeout("DoFade("+colorId+",'"+targetId+"')", FadeInterval);
	}
}


/* checkOS() is a OS sniffer */
/* 
if a link's rel attribute is set to 'os-sniff' 
it sends windows users to the download page and 
mac users to the express page

if a link's rel attribute is set to 'os-sniff-auto' 
it sends windows users to the download page and initiates a download
and sends mac users to the express page 
*/
function download_sniff() {
	var jsurl="/en/start_chatting.shtml"
	if (navigator.appVersion.indexOf("Mac")!=-1) jsurl="/express/";
	window.location.href = jsurl;
}

function gotoChat(){
document.location="/en/download_auto.shtml";
}

function doChat(){
window.setTimeout('gotoChat()', 1000); 
ACTION='07';
frames['downloadframe'].location.href="http://www.paltalk.com/download/0.x/pal_install_r17711.exe";
}

function download_sniff_auto () {
	var jsurl="javascript:doChat();"
	if (navigator.appVersion.indexOf("Mac")!=-1) jsurl="/express/";
	window.location.href = jsurl;
}

function checkOS() {
	var sniffer = document.getElementsByTagName('a');
	for (var i=0; i<sniffer.length; i++ ) {
		if (sniffer[i].getAttribute("rel") == "os-sniff") {
			sniffer[i].onclick = function (){ download_sniff (); }
		} 
		
		else if (sniffer[i].getAttribute("rel") == "os-sniff-auto") {
			sniffer[i].onclick = function (){ download_sniff_auto (); }
		}
	}
}

/* 
checkref() checks for refc or advc params in the url string and appends them 
to the client link 
*/
function getUrlParam(name)
{
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var tmpURL = window.location.href;
  var results = regex.exec( tmpURL );
  if( results == null )
    return "";
  else
    return results[1];
}

function checkref() {
	var refc =getUrlParam('refc');
	var advc =getUrlParam('advc');
	var pmid =getUrlParam('pmid');
	if(refc!="" && isNaN(refc)) refc = "";
	if(advc!="" && isNaN(advc)) advc = "";
		
	if(refc == "" && advc == "" && pmid == "") return;
	// rename the download	
	var downurl = "http://www.paltalk.com/download/0.x/pal_install";
			
	if(pmid!=""){
		//alert(pmid);
		downurl = downurl + "_pmid" + pmid;	    
	} else {
		if(advc !="" ) downurl = downurl + "_a" + advc;
		if(refc !="" ) downurl = downurl + "_r" + refc;
	}
			
	downurl = downurl + ".exe";
			
	//alert(downurl);
	//update the href with correct link
	if (document.getElementById('dlu1')!= null)
	document.getElementById('dlu1').href=downurl;
	if (document.getElementById('dlu2')!= null)
	document.getElementById('dlu2').href=downurl;
	return;
}

/* checks rel link attribute, if it's set to external the link goes to a new window */
function newWin() {
   if (!document.getElementsByTagName) return false;
   var links = document.getElementsByTagName("a");
   for (var i=0; i<links.length; i++) {
      if (links[i].getAttribute("rel") == "external") {
         links[i].onclick = function() {
            return !window.open(this.href);
         }
      }
   }
}