<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>


<link type="text/css" href="../style/registration.css" rel="stylesheet">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html:html lang="true">
  <head>
    <title>Customer Transaction Details</title>
    <script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
    <script type="text/javascript">
    	function dispReturnPolicy(orderItemId,ownerType) {
		/*alert('orderItemId'+orderItemId);
		var jsOrderItemId;
			  <logic:iterate id="OrderItemsInfo" name="custOrderList">
			  		jsOrderItemId = <c:out value="${OrderItemsInfo.orderItemId}"/>
					alert('jsOrderItemId'+jsOrderItemId);
						
					if(jsOrderItemId == orderItemId){
					alert('inside if');
					alert('<c:out value="${OrderItemsInfo.returnPolicy}"/>');
							<c:set var="ReturnPolicy" scope="request" value="${OrderItemsInfo.returnPolicy}"></c:set>
					}
			   </logic:iterate>
    	
			
    		window.open("displayReturnPolicy.jsp",null,"height=400, width=400");*/
    	document.forms[0].target="";	
		document.forms[0].action="displayReturnDisplay.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
		document.forms[0].submit();	
    	}
    	function submitToLogin() {
    		document.forms[0].target="";
    		document.forms[0].action="customerLogins.do?method=displayLogin";
    		document.forms[0].submit();
    	}
    	window.onload=function() {
    		tablecollapse();
    	}  
    	function displayAndCollapse() {
    		var displayValue=document.getElementById("expandAndCollapse");
    		if(displayValue.style.display =="none") {
				displayValue.style.display="inline";
    		}
    	}
    	
    	function displayOrderItems(value) {
    		document.forms[0].target="";
    		document.forms[0].action="customersLogin.do?method=displayOrderConfirmationHistory&orderConfNo="+value;
    		document.forms[0].submit();
    	}
    	
    	function generatePDF() {
    		var htmlText = window.PDFText.innerHTML;
    		document.forms[0].pdfText.value = htmlText;
    		document.forms[0].target="_blank";
    		document.forms[0].action="customersLogin.do?method=generatePDF";
    		document.forms[0].submit();
    	}
    	
    	function dispTrackingDetail(jsOrderItemId) {
    		document.forms[0].target="";
    		document.forms[0].action="custOrderTrackingDetail.do?method=getCustOrderTrackingDetails&OrderItemId="+jsOrderItemId;
    		document.forms[0].submit();
    	}
    	
    	function dispInvoiceDetail(jsOrderItemId,jsInvoiceStatus) {
    		document.forms[0].target = "_blank";
    		document.forms[0].action="customersOrderDisplay.do?method=viewEmailPdf&OrderItemId="+jsOrderItemId+"&Status="+jsInvoiceStatus;
    		document.forms[0].submit();
    	}
    	
    	function displayOrderReturn(jsOrderItemId) {
    		document.forms[0].target="";
    		document.forms[0].action="displayOrderReturn.do?method=displayOrderReturn&OrderItemId="+jsOrderItemId;
    		document.forms[0].submit();
    	}
    	function fnCallViewRMADetails(jsOrderItemId) {
    		document.forms[0].target="";
    		document.forms[0].action="viewRMADetails.do?method=viewRMADetails&OrderItemId="+jsOrderItemId;
    		document.forms[0].submit();
    	}
    </script>
  </head>
  
  <body>
    <html:form action="customer/customersOrderDisplay" method="post">
    <tr><td>
    <input type="hidden" name="pdfText" value="" />  
     	<div class="contentcontainer container_margin" id="PDFText">
     	  <div class="ReplaceOrderDetailsStart"></div>     		
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table">
			  <tbody>
			  	<tr>
				    <td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
				    <td width="100%" background="../images/top_nav_middlebg.png" align="left" > 
						<ul>
							<li><font size="4">Order Details</font></li>
						</ul>
					</td>
				    <td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
			  </tr>
			 	 <div class="ReplaceOrderDetailsEnd"></div>
			  <tr>
				  <td colspan="3" valign="top" align="center" class="td">
				  <div class="cus_order_details">
		 		 <logic:present name="OtherDetails">
	     			<logic:iterate id="OtherDetail" name="OtherDetails">
	       			<table cellspacing="0" cellpadding="3" border="0" align="center" class="order-items">
	   				<tr> 
   						<td align="left" width="130" nowrap="nowrap" class="lable">Order Confirmation No</td>
   						<td width="10" class="lable">:</td>
   						<td align="left"><bean:write name="TransactionId"/></td>
   						<td width="20" align="left">&nbsp;</td>
   						<td width="90" align="left" nowrap="nowrap" class="lable">AirLine Name</td>
   						<td width="10" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="airName"/></td>
   					</tr>	
   					<tr>
   						<td width="100" align="left" class="lable">Order Date</td>
   						<td align="left" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="orderCreatedDt"/></td>
   						<td width="20" align="left">&nbsp;</td>
   						<c:if test="${OtherDetail.flightNo ne null && OtherDetail.flightNo ne ''}">
	   						<td width="90" align="left" class="lable">Tail No</td>
	   						<td align="left" class="lable">:</td>
	   						<td align="left"><bean:write name="OtherDetail" property="flightNo"/></td>
   						</c:if>
   					</tr>					
					<tr>
						<td align="left" colspan="7"> </td>
					</tr>
					<tr>
   						<td width="100" align="left" valign="top" class="lable"> Billing Address</td>
   						<td align="left" valign="top" class="lable">:</td>
   						<td align="left" valign="top">
						<bean:write name="OtherDetail" property="custFirstName" />
   							<bean:write name="OtherDetail" property="custLastName" />&nbsp;<br>
   							<bean:write name="OtherDetail" property="custAddress1" />&nbsp;<br>
   							<logic:present name="OtherDetail" property="custAddressSecond">
   								<logic:notEmpty name="OtherDetail" property="custAddressSecond">
   									<bean:write name="OtherDetail" property="custAddressSecond" />&nbsp;<br>
   								</logic:notEmpty> 
   							</logic:present>
   							<bean:write name="OtherDetail" property="custCity" />,&nbsp;
   							<logic:iterate id="states" name="StateList" scope="application">
								<c:if test="${states.stateCode eq OtherDetail.custState}">
									<bean:write name="states" property="stateName" />&nbsp;								</c:if>
							</logic:iterate>
   							<bean:write name="OtherDetail" property="custZip" /><br>USA						
   						</td>
   						<td width="20" align="left" valign="top">&nbsp;</td>
   						<td width="110" align="left" valign="top" nowrap class="lable"> Shipping Address</td>
   						<td align="left" valign="top" class="lable">:</td>
   						<td align="left" valign="top">
							<bean:write name="OtherDetail" property="custShipFirstName" />
   							<bean:write name="OtherDetail" property="custShipLastName" />&nbsp;<br>
   							<bean:write name="OtherDetail" property="custShipAddress1" />&nbsp;<br>
   							<logic:present name="OtherDetail" property="custShipAddressSecond">
   								<logic:notEmpty name="OtherDetail" property="custShipAddressSecond">
   									<bean:write name="OtherDetail" property="custShipAddressSecond" />&nbsp;<br>
   								</logic:notEmpty>
   							</logic:present>
   							<bean:write name="OtherDetail" property="custShipCity" />,&nbsp;
   							<logic:iterate id="states" name="StateList" scope="application">
								<c:if test="${states.stateCode eq OtherDetail.custShipState}">
									<bean:write name="states" property="stateName" />&nbsp;								</c:if>
							</logic:iterate>
   							<bean:write name="OtherDetail" property="custShipZip" /><br>USA						
   						</td>
   					</tr>	
					<tr>
   						<td width="100" align="left" class="lable">Email</td>
   						<td align="left" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="custEmail" /></td>
   						<td width="20" align="left">&nbsp;</td>
   						<td width="90" align="left" class="lable">Email</td>
   						<td align="left" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="custShipEmail" /></td>
   					</tr>
   					<tr>	
   						<td width="100" align="left" class="lable">Phone</td>
   						<td align="left" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="custPhone" /></td>
   					    <td width="20" align="left">&nbsp;</td>
   					    <td width="100" align="left" class="lable">Phone</td>
   						<td align="left" class="lable">:</td>
   						<td align="left"><bean:write name="OtherDetail" property="custShipPhone" /></td>
   					</tr>	
   				</table>
				<!-- end order-items -->
   				</logic:iterate>
     		</logic:present>
     		<logic:present name="custOrderList">
     			<logic:notEmpty name="custOrderList">
     			<div class="ReplaceOuterTableStart"></div>
     			<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" class="border">
					<tbody>
						<div align="right" class="lable"><a href="javascript:generatePDF()">Export To PDF</a></div>
	 			 		<tr>
       					 <td height="30" align="center" class="tablehead" colspan="3"><h2>Order Details</h2></td>
        				</tr>
        				<div class="ReplaceOuterTableEnd"></div>
						<tr>
       					 <td valign="top">
       						<div class="ReplaceTagStart"></div>
		  					<table width="100%" cellspacing="1" cellpadding="1" border="0" bgcolor="#cccccc" class="broder_top0">
          					<tbody>		     		  
				          <tr>
				          <div class="ReplaceTagEnd"></div>
					          <td class="table_header" nowrap="nowrap" height="32">Item</td>
							  <td class="table_header" nowrap="nowrap">Item Details</td>
							  <!-- <td class="table_header" nowrap="nowrap">Brand Name</td>
							  <td class="table_header" nowrap="nowrap">Item Name</td>       
					          <td class="table_header" nowrap="nowrap">Category</td>--> 
							  <td class="table_header" nowrap="nowrap">Quantity</td>
					          <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup><br>Unit Price</td>
					          <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup><br>Total Amount</td>
							  <td class="table_header" nowrap="nowrap">Status</td>
							  <td class="table_header">Shipment Status</td>
							  <!--<td class="table_header" nowrap="nowrap">Order Date</td>-->
					         
					          <div class="RemoveReturnsLabelStarts"></div>
					          	<td class="table_header" nowrap="nowrap">Return Policy</td>
					          	<td class="table_header" nowrap="nowrap">Tracking Details</td>
					          	 <td class="table_header" nowrap="nowrap">Invoice Details</td>	 
					          	<!-- <td class="table_header" nowrap="nowrap">Order Return</td> -->
					          <div class="RemoveReturnsLabelEnd"></div>				 
				          </tr>
						  <logic:iterate id="OrderItemsInfo" name="custOrderList">
					      <tr class="tdbg">
					      	 <td align="left" height="140" nowrap="nowrap" class="lable">
					      	 	<c:if test="${OrderItemsInfo.isSpecialProd ne 'Y'}">
					      	 		<DIV id="IMAGETAGSTART"></DIV>
					      	 		<div align="center"><img src="displayProdImage.do?method=renderImage&orderItemId=<c:out value='${OrderItemsInfo.orderItemId}' />" width="79" height="79" /></div>
					      	 		<DIV id="IMAGETAGEND"></DIV>
					      	 	</c:if>
					      	 	<div class="lable" align="center"><bean:write name="OrderItemsInfo" property="prodCode"/></div></td>
					      	 <td align="left" nowrap="nowrap" valign="middle" class="lable">
					      	 	<table>
					      	 	
					      	 		<tr>
					      	 			<c:if test="${!empty OrderItemsInfo.vendorName and OrderItemsInfo.vendorName ne ''}">
					      	 				<td nowrap="nowrap" valign="top"> Vendor Name </td>
					      	 				<td valign="top"> : </td>
					      	 				<td> <bean:write name="OrderItemsInfo" property="vendorName"/> </td>
					      	 			</c:if>
					      	 		</tr> 
					      	 		<tr>
					      	 			<c:if test="${OrderItemsInfo.brandName ne null and OrderItemsInfo.brandName ne ''}">
					      	 				<td nowrap="nowrap" valign="top"> Brand Name </td>
					      	 				<td valign="top"> : </td>
					      	 				<td> <bean:write name="OrderItemsInfo" property="brandName"/> </td>
					      	 			</c:if>
					      	 		</tr>
					      	 		<tr>
					      	 			<c:if test="${OrderItemsInfo.isSpecialProd ne 'Y'}">
					      	 				<td nowrap="nowrap" valign="top">Item Name </td>
					      	 				<td valign="top"> : </td>
					      	 				<td><bean:write name="OrderItemsInfo" property="itemName"/></td>
				      	 				</c:if>
				      	 			</tr>
				      	 			<tr>
				      	 				<c:if test="${OrderItemsInfo.size ne null and OrderItemsInfo.size ne ''}">
					      	 				<td nowrap="nowrap" valign="top">Size </td>
					      	 				<td valign="top"> : </td>
					      	 				<td><bean:write name="OrderItemsInfo" property="size"/></td>
				      	 				</c:if>
				      	 			</tr>
				      	 			<tr>
				      	 				<c:if test="${OrderItemsInfo.color ne null and OrderItemsInfo.color ne ''}">
					      	 				<td nowrap="nowrap" valign="top">Color </td>
					      	 				<td valign="top"> : </td>
					      	 				<td><bean:write name="OrderItemsInfo" property="color"/></td>
				      	 				</c:if>
				      	 			</tr>
				      	 			<tr>
				      	 				<c:if test="${OrderItemsInfo.travelDate ne null and OrderItemsInfo.travelDate ne ''}">
					      	 				<td nowrap="nowrap" valign="top"> Travel Date </td>
					      	 				<td valign="top"> : </td>
					      	 				<td> <bean:write name="OrderItemsInfo" property="travelDate"/> </td>
					      	 			</c:if>	
					      	 		</tr> 
					      	 	</table>
					      	 </td>	
				         	 <!--  <td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderItemsInfo" property="itemName"/></td>          	
							 <td align="left" nowrap="nowrap" class="lable">      
								 <logic:iterate id="category" name="Category"scope="application">
								 <c:if test="${category.cateId eq OrderItemsInfo.cateId}">
								  <bean:write name="category" property="cateName" />
								 </c:if>
								 </logic:iterate>
								 <c:if test="${OrderItemsInfo.cateId eq 20}">
								 	Private Jet Jaunts
								 </c:if>
						 	 </td>-->          			
							 <td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderItemsInfo" property="qty"/></td>
							 <td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderItemsInfo" property="sbhPrice"/></td>
							 <td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderItemsInfo" property="payAmount"/></td>   
							 <td align="left" nowrap="nowrap" class="lable">
							 	<!--<c:if test="${OrderItemsInfo.orderItemStatus eq 'A'}">RMA Approved</c:if>
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'E'}">RMA Rejected</c:if>-->
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'P' or OrderItemsInfo.orderItemStatus eq 'Q'}">Order in Process</c:if>
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'C'}">Completed</c:if>
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'R'}">Rejected</c:if>
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'F'}">Failed</c:if>
							 	<c:if test="${OrderItemsInfo.orderItemStatus eq 'O'}">Canceled</c:if>
							 </td>
							 <td align="left" nowrap="nowrap" class="lable">
							 	<c:if test="${OrderItemsInfo.shipmentStatus eq 'Y'}">Shipped</c:if>
							 	<c:if test="${OrderItemsInfo.shipmentStatus eq 'N'}">Not Shipped</c:if>
							 </td>    
							 <!--<td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderItemsInfo" property="orderCreatedDt"/></td> -->           	
							 <div class="RemoveReturnsStarts"></div>
							 	<td align="left" nowrap="nowrap" class="lable"><a href="javascript:dispReturnPolicy('<bean:write name="OrderItemsInfo" property="orderItemId"/>','<bean:write name="OrderItemsInfo" property="ownerType"/>')"><bean:write name="OrderItemsInfo" property="seturnPolicy"/></a></td>
							 	<td align="center" nowrap="nowrap" class="lable">
							 		<a href="javascript:dispTrackingDetail('<bean:write name="OrderItemsInfo" property="orderItemId"/>')">Tracking Details</a>
							 	</td>
							 	<td align="center" nowrap="nowrap" class="lable">
							 		<c:if test="${OrderItemsInfo.orderItemStatus eq 'C'}">
							 			<a href="javascript:dispInvoiceDetail('<bean:write name="OrderItemsInfo" property="orderItemId"/>','A')">Invoice Details</a>
							 		</c:if>
							 		<c:if test="${OrderItemsInfo.orderItemStatus eq 'O'}">
							 			<a href="javascript:dispInvoiceDetail('<bean:write name="OrderItemsInfo" property="orderItemId"/>','C')">Invoice Details</a>
							 		</c:if>
							 	</td>
							 	<!-- <td align="center" nowrap="nowrap" class="lable">
							 		<c:if test="${OrderItemsInfo.returnDateExpired eq 'N' && OrderItemsInfo.shipmentStatus eq 'Y' && OrderItemsInfo.orderItemStatus eq 'C' && OrderItemsInfo.isEligibleToReturn eq 'YES'}">
							 			<a href="javascript:displayOrderReturn('<bean:write name="OrderItemsInfo" property="orderItemId"/>')">Return &nbsp;&nbsp;</a>  
							 		</c:if>
							 		<c:if test="${OrderItemsInfo.returnDateExpired eq 'Y' && OrderItemsInfo.shipmentStatus eq 'Y' && OrderItemsInfo.orderItemStatus eq 'C'}">
							 			Expired &nbsp;&nbsp;
							 		</c:if>
							 		<c:if test="${OrderItemsInfo.shipmentStatus eq 'Y' && OrderItemsInfo.isRMAGenerated eq 'Y'}">
							 			  <a href="javascript:fnCallViewRMADetails('<bean:write name="OrderItemsInfo" property="orderItemId"/>')">View </a>
							 		</c:if>
							 	</td> -->
							 <div class="RemoveReturnsEnd"></div>
						 </tr>
					  </logic:iterate>
					  </tbody>
					  </table>
					  <!-- end broder_top0 -->
					  </td>
					  </tr>
					  </tbody>
					</table>
			     	</logic:notEmpty>
	     		</logic:present>
				<!-- end cus_order_details -->
				</td>
	   		 </tr>
	   		 <div class="RemoveButtonTagStarts"></div>
		 	<tr>
		 	 	<td colspan="3" align="center" class="td" height="50">
		 	 		<c:if test="${History eq 'Available'}">
			     		<div class="lable"><a href="javascript:displayAndCollapse()"> History </a></div>
			     		<div id="expandAndCollapse" style="display:none">
				     		<table width="60%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
				     			<thead class="tablehead">
				     				<tr>
				     					<td>
				     						Order Confirmation No
				     					</td>
				     					<td>
				     						Order Date
				     					</td>
				     					<td>
				     						Air Charter Name
				     					</td>
				     				</tr>
				     			</thead>
	
				     			<tfoot>
						          <tr>
						            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
						          </tr>
						        </tfoot>
	
				     			<tbody>
					     			<logic:present name="orderConfirmationList" scope="request">
						     			<logic:notEmpty name="orderConfirmationList" scope="request">
							     			<logic:iterate id="OrderConfList" name="orderConfirmationList">
						     					<tr>
							     					<td>
										     			<a href="javascript:displayOrderItems('<bean:write name="OrderConfList" property="orderConfirmationNo" /> ')"><bean:write name="OrderConfList" property="orderConfirmationNo" /></a>
													</td>
													<td>
														<bean:write name="OrderConfList" property="orderDate" />
													</td>
													<td>
														<bean:write name="OrderConfList" property="airlineName" />
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</logic:present>
								</tbody>
							</table>
						</div>
					</c:if>
				</td>			
			 </tr>
			 <tr>
			    <td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
			    <td background="../images/top_nav_bmiddlebg.png"> </td>
			    <td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
			  </tr>
			  <div class="RemoveButtonTagEnd"></div>
			  </tbody>
			</table>
     	</div>
		<!-- end contentcontainer -->
		</td></tr>
    </html:form>
  </body>
</html:html>
