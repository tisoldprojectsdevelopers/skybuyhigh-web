<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript">
	
	function fnCallSubmit(jsOrderConfNo) {
		if(isEligibleForSubmit()) {
			document.forms[0].action="displayGeneratedRMA.do?method=getRMAGeneratedValue&OCN="+jsOrderConfNo;
			document.forms[0].submit();
		}
	}
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function isEligibleForSubmit() {
		var reasonForReturn = document.forms[0].reasonForReturn.value;
		var customerReturnQuantity = trim(document.forms[0].returnQuantity.value);
		var returnQuantityFormat = new RegExp("^[0-9]*$");
		var returnQuantity = document.forms[0].EligibleToReturn.value;

		if(customerReturnQuantity == '') {
			alert("No. of return quantity is required");
			return false;
		}else {
			customerReturnQuantity = trimQuantity(customerReturnQuantity);
			if(!returnQuantityFormat.test(customerReturnQuantity)) {
				alert("Please enter valid return quantity");
				return false;
			} else if(customerReturnQuantity == 0) {
				alert("Please enter valid return quantity");
				return false;
			} else if(parseInt(customerReturnQuantity) > parseInt(returnQuantity)) {
				alert("Please enter the value less than or equal to the return quantity");
				return false;
			} else {
				document.forms[0].returnQuantity.value = customerReturnQuantity;
			}
		}
		if(reasonForReturn == '') {
			alert("Reason for return is required");
			return false;
		}
		return true;
	}
	function textLimit(field, countfield,maxlen,dispName) {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1) {
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		} 
		if (fieldvallength > maxlen) {
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
	}
	
	function fnCallViewCustOrderDetails() {
		document.forms[0].action = "customersLogin.do?method=getTransactionDetails";
		document.forms[0].submit();
	}
	
</script>
<html:form action="customer/displayOrderReturn" method="post">
    <tr><td>
    	<!-- START - DIV - contentcontainer -->
	    <div class="contentcontainer">
	    
	    	<!-- START - TABLE - Outer Table -->
	    	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table">
			  <tbody>
				<tr>
					<td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
					<td width="100%" background="../images/top_nav_middlebg.png" align="left"> 
						<ul>
							<li><font size="4">Display Order Return Details</font></li>
						</ul>
					</td>
					<td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
				</tr>
				<tr class="tdbg">
					<td colspan="3">
						&nbsp;
					</td>
				</tr>
				<tr class="tdbg">
     				<td class="td" align="center" colspan="3">
		 				<logic:present name="OrderReturnDetails">
		     				<logic:notEmpty name="OrderReturnDetails">
			     				<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" class="border">
									<tbody>
					 			 		<!--  <tr>
				       					 	<td height="30" align="center" class="tablehead" colspan="3"><h2>Order Details</h2></td>
				        				</tr>-->
										<tr bgcolor="#FFFFFF">
					       					<td valign="top">
					       					 <!-- START - TABLE - border_top0  -->
						       					 <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" >
							       					 <tr>
							       					 	<td height="30" align="left" class="tablehead" colspan="3"><h2>Return Policy</h2></td>
							       					 </tr>
							       					 <tr>
							       					 	<td>
								       					 	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="broder_top0">
								          						<tbody>	
								          							<tr>
								       					 				<td align="left" class="lable">
																		 	<div><bean:write name="OrderReturnDetails" property="returnPolicy"/></div>
																		 	<div style="margin:10px 0 0 0">Return Days : <bean:write name="OrderReturnDetails" property="returnDays"/></div>
																		</td>
								       					 			</tr>
								       					 		</tbody>
								       					 	</table>
							       					 	</td>
							       					 </tr>
						       					</table>
					       					</td>
				       					</tr>
				       					<tr bgcolor="#FFFFFF">
					       					<td valign="top">
						       					 <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="tablemargin">
							       					 <tr>
							       					 	<td height="30" align="left" class="tablehead" colspan="3"><h2>Item Details</h2></td>
							       					 </tr>
							       					 <tr>
							       					 	<td>	
										  					<table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#cccccc" class="broder_top0">
								          						<tbody>	
												         	 		<tr>
												         	 		  <td class="table_header" nowrap="nowrap" height="32">OrderItem Id</td>	
															          <td class="table_header" nowrap="nowrap">Item</td>
																	  <td class="table_header" nowrap="nowrap">Item Details</td> 
																	  <!--  <td class="table_header" nowrap="nowrap">Item Name</td>-->       
															         <!--  <td class="table_header" nowrap="nowrap">Category</td> -->
																	  <td class="table_header" nowrap="nowrap">Quantity</td>
																	  <c:if test="${OrderReturnDetails.returnQuantity ne null and OrderReturnDetails.returnQuantity ne '0'}">
																	  	<td class="table_header" nowrap="nowrap">Returned Quantity</td>
																	  </c:if>
															          <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup> Price</td>
																	  <!--  <td class="table_header" nowrap="nowrap">Status</td>-->
																	  <td class="table_header" nowrap="nowrap">Order Date</td>
															          <td class="table_header" nowrap="nowrap">Vendor Name</td>	
															          
														          </tr>
															      <tr class="tdbg">
															      	 <td align="center" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="orderItemId"/></td>
															      	 <td align="left" height="144" nowrap="nowrap" class="lable">
														      	     	<div align="center"><html-el:img src="${OrderReturnDetails.mainImgPath}" width="79" height="79" /></div><div class="lable" align="center"><bean:write name="OrderReturnDetails" property="prodCode"/></div></td>
															      	 <td align="left" nowrap="nowrap" valign="middle" class="lable">
															      	 	<table> 
															      	 		<tr>
															      	 			<c:if test="${OrderReturnDetails.brandName ne null and OrderReturnDetails.brandName ne ''}">
															      	 				<td nowrap="nowrap"> Brand Name </td>
															      	 				<td> : </td>
															      	 				<td> <bean:write name="OrderReturnDetails" property="brandName"/> </td> 
															      	 			</c:if>
															      	 		</tr>
															      	 		<tr>
														      	 				<td nowrap="nowrap">Item Name </td>
														      	 				<td> : </td>
														      	 				<td><bean:write name="OrderReturnDetails" property="itemName"/></td>
														      	 			</tr>
														      	 			<tr>
														      	 				<c:if test="${OrderReturnDetails.size ne null and OrderReturnDetails.size ne ''}">
															      	 				<td nowrap="nowrap">Size </td>
															      	 				<td> : </td>
															      	 				<td><bean:write name="OrderReturnDetails" property="size"/></td>
														      	 				</c:if>
														      	 			</tr>
														      	 			<tr>
														      	 				<c:if test="${OrderReturnDetails.color ne null and OrderReturnDetails.color ne ''}">
															      	 				<td nowrap="nowrap">Color </td>
															      	 				<td> : </td>
															      	 				<td><bean:write name="OrderReturnDetails" property="color"/></td>
														      	 				</c:if>
														      	 			</tr>
														      	 			<tr>
														      	 				<c:if test="${OrderReturnDetails.travelDate ne null and OrderReturnDetails.travelDate ne ''}">
															      	 				<td nowrap="nowrap"> Travel Date </td>
															      	 				<td> : </td>
															      	 				<td> <bean:write name="OrderReturnDetails" property="travelDate"/> </td>
															      	 			</c:if>	
															      	 		</tr> 
															      	 	</table>
															      	 </td>	
														         	 <!-- <td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="itemName"/></td> -->          	
																	 <!-- <td align="left" nowrap="nowrap" class="lable">      
																		 <logic:iterate id="category" name="Category"scope="application">
																		 <c:if test="${category.cateId eq OrderReturnDetails.cateId}">
																		  <bean:write name="category" property="cateName" />
																		 </c:if>
																		 </logic:iterate>
																		 <c:if test="${OrderReturnDetails.cateId eq 20}">
																		 	Private Jet Jaunts
																		 </c:if>
																 	 </td>-->          			
																	 <td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="qty"/></td>
																	 <c:if test="${OrderReturnDetails.returnQuantity ne null and OrderReturnDetails.returnQuantity ne '0'}">
																	 	<td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="returnQuantity"/></td>
																	 </c:if>
																	 <td align="right" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="sbhPrice"/></td>   
																	 <!--  <td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="orderItemStatus"/></td>-->    
																	 <td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="orderCreatedDt"/></td>            	
																	 <td align="left" nowrap="nowrap" class="lable"><bean:write name="OrderReturnDetails" property="vendorName"/></td>
																	 <input type="hidden" name="EligibleToReturn" value="${CustomerAbleToReturn}"/>
																 </tr>
												  			</tbody>
												  		</table>
												  	</td>
												 </tr>
											</table>
								  		<!-- END - TABLE border_top0 -->
								  	  	</td>
								  	</tr>
								  	<tr bgcolor="#FFFFFF">
					       				<td valign="top" colspan="3">
								  			<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="tablemargin">
						       					 <tr>
						       					 	<td height="30" align="left" class="tablehead" colspan="3"><h2>Return Quantity</h2></td>
						       					 </tr>
						       					 <tr>
						       					 	<td>
							       					 	<table cellspacing="0" cellpadding="0" border="0" class="broder_top0" align="left">
							          						<tbody>	
							          							<tr>
							          								<td align="left" class="lable">
							       					 					No. of Return Quantity																	
							       					 				</td>
																	<td align="center" class="lable">
							       					 					:																	
							       					 				</td>
							       					 				<td align="left" class="lable">
							       					 					<html:text property="returnQuantity" maxlength="4"/>
							       					 					<html:hidden property="totalQuantity" value="${OrderReturnDetails.custReturnQuantity}"/>
							       					 					<html:hidden property="vendorQuantity" value="${OrderReturnDetails.returnQuantity}"/></td>
							       					 			 </tr>
							       					 		</tbody>
						       					 	  </table>
						       					 	</td>
						       					 </tr>
					       					</table>
								  		</td>
								  	</tr>
								  	<tr bgcolor="#FFFFFF">
					       				<td valign="top" colspan="3">
								  			<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="tablemargin">
						       					 <tr>
						       					 	<td height="30" align="left" class="tablehead" colspan="3"><h2>Reason for return</h2></td>
						       					 </tr>
						       					 <tr>
						       					 	<td>
							       					 	<table cellspacing="0" cellpadding="0" border="0" class="broder_top0" align="left">
							          						<tbody>	
							          							<tr>
							          								<td align="left" class="lable" valign="top" nowrap="nowrap">
							       					 					Reason for the return																	
							       					 				</td>
																	<td align="center" class="lable" valign="top">
							       					 					:																	
							       					 				</td>
							       					 				<td align="left" class="lable" width="100%">
																	 	<html:textarea rows="5"  styleClass="textarea1" property="reasonForReturn" onkeyup="textLimit(this,this.form.policylen,1000,'Reason for return');"/>
															            <div align="left" style="margin:3px 0 3px 0;"><span class="normaltext">Remaining characters</span>
															            <input readonly="readonly" type="text" name="policylen" size="3" maxlength="3" value="1000" class="wordcount"/></div>																 
																	</td>
							       					 			</tr>
							       					 		</tbody>
							       					 	</table>
						       					 	</td>
						       					 </tr>
					       					</table>
								  		</td>
								  	</tr>
								  	<tr>
								  		<td height="32">
								  			<input type="button" class="bigbutton" onclick="javascript:fnCallSubmit('<bean:write name="OrderReturnDetails" property="orderItemId"/>')" value="Generate RMA" />
								  			<html:button property="method" onclick="javascript:fnCallViewCustOrderDetails();" styleClass="button">Cancel</html:button>							  		  
								  		</td>
								  	</tr>
								  </tbody>
								</table>
					     	</logic:notEmpty>
			     		</logic:present>
	     			</td>
				</tr>
				<tr>
					<td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
					<td background="../images/top_nav_bmiddlebg.png"> </td>
					<td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
				</tr>
			  
			  </tbody>
			</table>
			<!-- END - TABLE - Outer Table -->
			
	    </div>
	    <!-- END - DIV - contentcontainer -->
    </td></tr>
</html:form>