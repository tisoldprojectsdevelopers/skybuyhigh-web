<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/


	function fnCallViewCustOrderDetails(jsOrderItemId, jsCustTransId) {
		document.forms[0].action = "viewRMADetails.do?method=viewRMADetails&OrderItemId="+jsOrderItemId+"&CustTransId="+jsCustTransId;
		document.forms[0].submit();
	}

</script>




<html:form action="/customer/viewRMADetails" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			    <td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
			    <td width="100%" background="../images/top_nav_middlebg.png" align="left">
			   		<ul>
						<li><font size="4">Return Merchandise Authorization Details</font></li>
					</ul>				
				</td>
			    <td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
			</tr>
			<tr>
				<td colspan="3" class="td">

					<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border" style="margin:15px;">
						<tr>
							<td height="30" colspan="3" class="tablehead" align="center">
								<h2>
									RMA Information										</h2>									</td>
						</tr>
						<tr>

						  <td colspan="3"><table width="100%" border="0" cellpadding="2" cellspacing="2"
									class="broder_top0">
                                  <tr class="tdbg">
                                    <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4> Return Address Details : </h4></td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> Vendor contact name </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" class="catagory"><bean:write
													name="DisplayRMADetails" property="partnerContactName" />                                      </td>
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor name</span> </td>
                                    <td width="10" align="left" class="lable"> : </td>
                                    <td width="50%" align="left" class="catagory"><bean:write
													name="DisplayRMADetails" property="ownerName" />                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> Address line 1 </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" class="catagory"><bean:write
													name="DisplayRMADetails" property="partnerAddress1" />                                      </td>
                                    <c:if test="${DisplayRMADetails.partnerAddress2 ne null && DisplayRMADetails.partnerAddress2 ne ''}">
                                      <td align="left" nowrap="nowrap" class="lable"> Address line 2 </td>
                                      <td align="left" class="lable"> : </td>
                                      <td align="left" class="catagory"><bean:write
														name="DisplayRMADetails" property="partnerAddress2" />                                        </td>
                                    </c:if>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span> </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write
													name="DisplayRMADetails" property="partnerCity" />                                      </td>
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span> </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" class="catagory"><logic:iterate id="states"
													name="StateList" scope="application">
                                        <c:if
														test="${states.stateCode eq DisplayRMADetails.partnerState}">
                                          <bean:write name="states" property="stateName" />
                                        </c:if>
                                      </logic:iterate>                                      </td>
                                  </tr>
                                  
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Country</span> </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory">USA</td>
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span> </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" class="catagory"><bean:write
													name="DisplayRMADetails" property="partnerZip" />                                      </td>
                                  </tr>
                                  <!-- <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Phone</span> </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write name="DisplayRMADetails" property="partnerZip" /></td>
                                    <td align="left" nowrap="nowrap" class="lable">&nbsp; </td>
                                    <td align="left" class="lable"> &nbsp; </td>
                                    <td align="left" class="catagory">&nbsp; </td>
                                  </tr> -->
                                  <tr align="left" class="tdbg">
                                    <td colspan="6" valign="top" nowrap="nowrap" class="lable"><hr />                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td colspan="6" align="left" class="lable"><h4> RMA Details : </h4></td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> Order item ID </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write
													name="DisplayRMADetails" property="orderItemId" />                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> RMA number </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write
													name="DisplayRMADetails" property="RMANumber" />                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> RMA generated date </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write
													name="DisplayRMADetails" property="rmaGeneratedDate" />                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> No. of items returned </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><bean:write
													name="DisplayRMADetails" property="customerReturnQuantity" />                                      </td>
                                  </tr>
                                  <c:if test="${DisplayRMADetails.vendorReturnQuantity ne null and DisplayRMADetails.vendorReturnQuantity ne ''}">
                                    <tr class="tdbg">
                                      <td align="left" nowrap="nowrap" class="lable"> No. of items approved </td>
                                      <td align="left" class="lable"> : </td>
                                      <td align="left" class="catagory"><bean:write
														name="DisplayRMADetails" property="vendorReturnQuantity" />                                        </td>
                                    </tr>
                                  </c:if>
                                  <c:if test="${DisplayRMADetails.rmaAuthorizeSignatory ne null and DisplayRMADetails.rmaAuthorizeSignatory ne ''}">
                                    <tr class="tdbg">
                                      <td align="left" nowrap="nowrap" class="lable"> RMA Authorize Signatory </td>
                                      <td align="left" class="lable"> : </td>
                                      <td align="left" class="catagory"><bean:write
														name="DisplayRMADetails" property="rmaAuthorizeSignatory" />                                        </td>
                                    </tr>
                                  </c:if>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable"> Return status </td>
                                    <td align="left" class="lable"> : </td>
                                    <td align="left" nowrap="nowrap" class="catagory"><c:if test="${DisplayRMADetails.rmaStatus eq 'RG'}"> RMA Generated </c:if>
                                        <c:if test="${DisplayRMADetails.rmaStatus eq 'WA'}"> Waiting for Admin to charge back </c:if>
                                        <c:if test="${DisplayRMADetails.rmaStatus eq 'WR'}"> Waiting for Admin to reject </c:if>
                                        <c:if test="${DisplayRMADetails.rmaStatus eq 'RR'}"> RMA Rejected </c:if>
                                        <c:if test="${DisplayRMADetails.rmaStatus eq 'RA'}"> RMA Approved </c:if>                                      </td>
                                  </tr>
                                  <tr class="tdbg">
                                    <td align="left" nowrap="nowrap" class="lable" valign="top"> Return reason </td>
                                    <td align="left" class="lable" valign="top"> : </td>
                                    <td align="left" class="catagory" colspan="6"><bean:write name="DisplayRMADetails" property="customerReason" /> </td>
                                  </tr>
                                  <c:if test="${DisplayRMADetails.vendorComments ne null && DisplayRMADetails.vendorComments ne ''}">
                                    <tr class="tdbg">
                                      <td align="left" nowrap="nowrap" class="lable" valign="top"> Vendor comments </td>
                                      <td align="left" class="lable" valign="top"> : </td>
                                      <td align="left" class="catagory" colspan="6"><bean:write name="DisplayRMADetails" property="vendorComments" /> </td>
                                    </tr>
                                  </c:if>
                                  <c:if test="${DisplayRMADetails.adminComments ne null && DisplayRMADetails.adminComments ne ''}">
                                    <tr class="tdbg">
                                      <td align="left" nowrap="nowrap" class="lable" valign="top"> Admin comments </td>
                                      <td align="left" class="lable" valign="top"> : </td>
                                      <td align="left" class="catagory" colspan="6"><bean:write name="DisplayRMADetails" property="adminComments" /> </td>
                                    </tr>
                                  </c:if>
                                </table></td>
						</tr>
						<tr>
							<td height="50" colspan="3" align="center">
								<input type="button"
									onclick="javascript:fnCallViewCustOrderDetails('<bean:write name="DisplayRMADetails" property="orderItemId" />','<bean:write name="DisplayRMADetails" property="custTransId" />');" class="button" value="OK" />									</td>
						</tr>
					</table>

				</td>
			</tr>
			<tr>
			    <td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
			    <td background="../images/top_nav_bmiddlebg.png"> </td>
			    <td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
			</tr>
		</tbody>
	</table>
</div>
</td></tr>
</html:form>