<%@ page language="java" session="true"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/sslext.tld" prefix="sslext"%>

<script type="text/javascript">

		function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
		}
    	function displayEmailText(forgetTrId) {
    		if(forgetTrId=="ForgetTransactionId") {
    			document.forms[0].emailId.value="";
    			document.forms[0].transactionId.value="";
    			document.getElementById("displayIfForget").style.display="inline";
    			document.getElementById("NoDataMessage").style.visibility="hidden";
    			document.getElementById("ErrorMessage").style.visibility="hidden";
    		}else {
    			document.getElementById("displayIfForget").style.display="none";
    		} 
    	}
    	function formSubmitEmail(value) {
    		var regex= /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2,3})?)$/i
    		
    		if(value != "") {
    			if(regex.test(value)) {
    				document.forms[0].transactionId.value="";
		    		document.forms[0].action="customersLogin.do?method=sendTransactionDetailsToCustomer";
		    		document.forms[0].submit();
		    	}else {
		    		alert("Enter Valid Email Address.");
		    	}
    		}else {
    			alert("Email-Id is required.");
    		}
    	}
    	
    	function submitEmail() {
    		var value=trim(document.forms[0].emailId.value);
    		document.forms[0].emailId.value = value;
    		formSubmitEmail(value);
    		
    	}
    	function validateInput() {
    		var value=trim(document.forms[0].transactionId.value);
    		var regExp = new RegExp("^[0-9][0-9\-]*[0-9]$");
    		document.forms[0].transactionId.value = value;
    		
    		if(value=="") {
    			alert("Order Confirmation No. is required.");
    			return false;
    		}else {
    			if(!regExp.test(value)) {
    				alert("Enter Valid Order Confirmation No.");
    				return false;
    				
				}else {    				
    				return true;
    			}
    		}
    	}
    	function triggerEvent() {
			if(event.keyCode==13) {
				submitEmail();
			}           
		}
		function triggerEventSubmit() {
			if(event.keyCode==13) {
				fnCallSubmit();
			} 
		}
		function fnCallSubmit() {
			var jsOrderConfNo = document.forms[0].transactionId.value
			document.forms[0].OrderConfNo.value = document.forms[0].transactionId.value
			document.forms[0].CustEmailId.value = document.forms[0].emailId.value
			if(validateInput()) {
				document.forms[0].action="customersLogin.do?method=getTransactionDetails";/*&OCN="+jsOrderConfNo;*/
		   		document.forms[0].submit();
		   	}else {
		   		return false;
		   	}
		}
		window.onload=function() {
			document.forms[0].transactionId.focus();
		}
    </script>
	<sslext:form action="/customer/customersLogin" method="post" onsubmit="return false;">
	<tr><td>
	<div class="divfix"></div>
		 <html:hidden property="OrderConfNo" value=""/>
		 <html:hidden property="CustEmailId" value=""/>
	  	  <div class="customer-login">
		  	<h2>Login</h2>
	 	        <div class="loginbox" style="margin-top:25px;">
	 	        	<div id="NoDataMessage" style="visibility:visible;" class="error">
				  	  	 <html:messages id="loginerror" property="loginfailed" message="true">
			     			<bean:write name="loginerror" /> 
			     		 </html:messages>
	    			</div>
	 			<div id="ErrorMessage" style="visibility:visible;" class="error">
		  	  		<html:messages id="email" property="EmailSent" message="true">
						<b><bean:write name="email" /></b>
		 			</html:messages>
		 	 	</div>
		  	  	<div class="lable"><b>Enter Your Order Confirmation No:</b>
			  	  	 <html:text property="transactionId" styleClass="input" maxlength="64" onkeydown="javascript:triggerEventSubmit();"/>
					 <html:button styleClass="button"  property="SubmitId" value="Submit" onclick="fnCallSubmit()"/>
			  	</div>
				<div style=" margin:8px 0 -20px 100px;">
		  	  	 	<a href="javascript:displayEmailText('ForgetTransactionId')">Forgot Your Order Confirmation No?</a>
		  	  	</div>
	  	     </div>
	  	      <c:if test="${hasDisplay eq 'display'}">
				  <div id="displayIfForget" class="lable" style="display:inline; margin:0 0 0 155px; ">
			  </c:if>
			  <c:if test="${hasDisplay ne 'display'}">
	  	     	<div id="displayIfForget" class="lable" style="display:none; margin:0 0 0 155px; ">
	  	     </c:if>
	 	 		<b>Enter Your Email-Id :</b>  
	 	 		<html:text property="emailId" styleClass="input" maxlength="64" onkeydown="javascript:triggerEvent();"/>
				<html:button styleClass="button" property="SubmitEmail" value="Submit" onclick="submitEmail()"/>
	  	  	 </div>
		  </div>
		  </div>
		  <!-- end customer-login -->
		  </td></tr>
  	  </sslext:form>
