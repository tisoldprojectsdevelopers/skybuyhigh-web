<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/main.css" rel="stylesheet" type="text/css" />
<link href="../style/index.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript" src="../js/menu.js"></script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <title>CustomerLogin</title>
    <script type="text/javascript">
		function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
		}
    	function displayEmailText(forgetTrId) {
    		if(forgetTrId=="ForgetTransactionId") {
    			document.getElementById("emailId").value = "";
    			document.getElementById("transactionId").value="";
    			document.getElementById("displayIfForget").style.display="inline";
    			document.getElementById("NoDataMessage").style.display="none";
    			document.getElementById("ErrorMessage").style.display="none";
    		}else {
    			document.getElementById("displayIfForget").style.display="none";
    		} 
    	}
    	function formSubmitEmail() {
    		var value=trim(document.getElementById("emailId").value);
    		document.forms[0].emailId.value=value;
    		
    		var regex=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2,3})?)$/i
    		
    		if(value != "") {
    			if(regex.test(value)) {
    				document.getElementById("transactionId").value="";
		    		document.forms[0].action="customersLogin.do?method=sendTransactionDetailsToCustomer";
		    		document.forms[0].submit();
		    	}else {
		    		alert("Enter Valid Email Address");
		    	}
    		}else {
    			alert("Email-Id is required");
    		}
    	}
    	function validateInput() {
    		var value=trim(document.getElementById("transactionId").value);
			document.forms[0].transactionId.value=value;
			
    		if(value=="") {
    			alert("Order Confirmation No. is required");
    			return false;
    		}else {
    			if(isNaN(value)) {
    				alert("Enter Valid Order Confirmation No");
    				return false;
    				
				}else {    				
    				return true;
    			}
    		}
    	}
    	function triggerEvent() {
			if(event.keyCode==13) {
				formSubmitEmail();
			}           
		}
		function triggerEventSubmit() {
			if(event.keyCode==13) {
				fnCallSubmit();
			} 
		}
		function fnCallSubmit() {
			if(validateInput()) {
				document.forms[0].action="customersLogin.do?method=getTransactionDetails";
		   		document.forms[0].submit();
		   	}else {
		   		return false;
		   	}
		}
    </script>
  </head>
  
  <body>
  	  <html:form action="customersLogin" method="post">
	  	  <div id="page">
  			<div id="logincontainer">
		  	<h2>Login</h2>
  	        <div class="loginbox">		  	  	 
		  	  	 <div class="lable" ><b>Enter Your Order Confirmation No:</b>
		  	  	 <html:text property="transactionId" styleClass="input" maxlength="64" onkeydown="javascript:triggerEventSubmit();"/>
		  	  	 <html:button styleClass="button" property="Submit" value="Submit" onclick="fnCallSubmit()"/></div>
				  <div style=" margin-left:100px; margin-top:8px;">
		  	  	 <a href="javascript:displayEmailText('ForgetTransactionId')">Forget Your Order Confirmation No?</a></div>
	  	     </div>
	  	     <div id="displayIfForget" class="loginbox" style="display:none; margin:155px;">
	 	 		<b>Enter Your Email-Id :</b>  
	 	 		<html:text property="emailId" styleClass="input" maxlength="64" onkeydown="javascript:triggerEvent();"/>
	 	 		<html:button styleClass="button" property="Submit" value="Submit" onclick="formSubmitEmail()"/>
	  	  	 </div>
	  	  	 <div id="NoDataMessage" style="display:inline">
		  	  	 <logic:present name="NoData" scope="request">
	     			<logic:notEmpty name="NoData" scope="request">
	     				<font color="red"><div align="center"><bean:write name="NoData" scope="request"/></div></font> 
	     			</logic:notEmpty>
	     		 </logic:present>
     		 </div>
     		 <div id="ErrorMessage" style="display:inline">
		  	  	 <logic:present name="EmailSent" scope="request">
		 			<logic:notEmpty name="EmailSent" scope="request">
		 				<font color="red">
		 				<div align="center">
		 					<b><bean:write name="EmailSent" scope="request"/></b><br>
		 					<bean:write name="EmailSentFooter" scope="request"/>
		 				</div></font>
		 			</logic:notEmpty>
		  	  	 </logic:present>
	  	  	 </div>
	  	  </div>
	  	  </div>
		  <!-- end customer-login -->
  	  </html:form>
  </body>
  
  
