<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript">

	function fnCallHome() {
		document.forms[0].action="customersLogin.do?method=getTransactionDetails";
		document.forms[0].submit();
	}

</script>
<html:form action="customer/displayGeneratedRMA" method="post">
    <tr><td>
    	<!-- START - DIV - contentcontainer -->
	    <div class="contentcontainer">
	    
	    	<!-- START - TABLE - Outer Table -->
	    	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table">
			  <tbody>
				<tr>
					<td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
					<td width="100%" background="../images/top_nav_middlebg.png" align="left"> 
						<ul>
							<li><font size="4">Display Generated RMA</font></li>
						</ul>
					</td>
					<td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
				</tr>
				<tr class="tdbg">
     				<td class="td" align="center" colspan="3">
		 				<logic:present name="PartnerReturnDetails">
		     				<logic:notEmpty name="PartnerReturnDetails">
		     				<div style="margin:10px;"><font color="#FF0000"><B><c:if test="${RMAGeneratedFirst ne null && RMAGeneratedFirst ne ''}">
		     					<c:out value="${RMAGeneratedFirst}" />
		     				</c:if>
		     				<c:if test="${RMAGeneratedSecond ne null && RMAGeneratedSecond ne ''}">
		     					<c:out value="${RMAGeneratedSecond}" /> 
		     				</c:if></B></font></div>
			     				<table width="800" cellspacing="0" cellpadding="0" border="0" align="center" class="border">
									<tbody>
					 			 		<tr>
				       					 	<td height="30" align="center" class="tablehead" colspan="3"><h2>Order Details</h2></td>
				        				</tr>
										<tr bgcolor="#FFFFFF">
				       					 <td valign="top">
				       					 	<div style="margin:10px;"><font color="#FF0000"><B>Please enclose the following RMA form along with your return shipment</B></font></div>
				       					 <!-- START - TABLE - border_top0  -->
						  					<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#cccccc" class="broder_top0">
				          						<tbody>		     		  
								         	 		<tr class="tdbg">
								         	 			<td class="lable"  colspan="3">
								         	 				<table width="100%" cellspacing="0" cellpadding="0" border="0">
								         	 					<tr class="tdbg">
								         	 						<td align="left" nowrap="nowrap" class="lable">Vendor Name</td>
														        	<td class="lable">:</td>
														        	<td class="lable" align="left"><bean:write name="PartnerOrderDetails" property="vendorName"/> </td>
														        	<td align="left" nowrap="nowrap" class="lable">RMA Generated Date</td>
														        	<td class="lable">:</td>
														        	<td class="lable" align="left"><bean:write name="PartnerReturnDetails" property="rmaGeneratedDate"/> </td>
														        	
													        	</tr>
													        	<tr class="tdbg">
													        		<td align="left" nowrap="nowrap" class="lable">Order Confirmation No</td>
														        	<td class="lable">:</td>
														        	<td class="lable" align="left"><bean:write name="PartnerOrderDetails" property="custTransId"/></td>
														        	<td align="left" nowrap="nowrap" class="lable">RMA Generated No</td>
														        	<td class="lable">:</td>
														        	<td class="lable" align="left"><bean:write name="PartnerReturnDetails" property="generatedRMA"/> </td>
													        	</tr>
												        	</table>
											        	</td>
										          	</tr>
										          	<tr class="tdbg">
										          		<td class="lable"  colspan="3">
										          			<hr/>										          		</td>
										          	</tr>
										          	<tr class="tdbg">
										          		<td class="lable" colspan="3">
										          			<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" class="border">
											       					 <tr>
											       					 	<td height="30" align="center" class="tablehead" colspan="3"><h2>Item Details</h2></td>
											       					 </tr>
											       					 <tr>
											       					 	<td>	
														  					<table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#cccccc" class="broder_top0">
												          						<tbody>	
																         	 		<tr>
																         	 		  <!--  <td class="table_header" nowrap="nowrap" height="32">Order Confirmation No</td>-->
																			          <td class="table_header" nowrap="nowrap" height="32">Item</td>
																					  <td class="table_header" nowrap="nowrap">Item Details</td> 
																					  <!--  <td class="table_header" nowrap="nowrap">Item Name</td> -->       
																			          <!-- <td class="table_header" nowrap="nowrap">Category</td> -->
																					  <td class="table_header" nowrap="nowrap">Return Quantity</td>
																			          <td class="table_header" nowrap="nowrap">SkyBuy<sup>High</sup> Price</td>
																					  <td class="table_header" nowrap="nowrap">Return Status</td>
																					  <td class="table_header" nowrap="nowrap">Order Date</td>
																			          <!-- <td class="table_header" nowrap="nowrap">Vendor Name</td>	 -->
																		          </tr>
																			      <tr class="tdbg">
																			      	 <!--  <td align="left" nowrap="nowrap" class="lable"><bean:write name="PartnerOrderDetails" property="custTransId"/></td>-->	
																			      	 <td align="left" height="140" nowrap="nowrap" class="lable">
																			      	 <div align="center"><html-el:img src="${PartnerOrderDetails.mainImgPath}" width="79" height="79" /></div><div class="lable" align="center"><bean:write name="PartnerOrderDetails" property="prodCode"/></div></td>
																			      	 <!-- <td align="left" nowrap="nowrap" class="lable"><bean:write name="PartnerOrderDetails" property="brandName"/></td>	 -->
																		         	 <td align="left" nowrap="nowrap" valign="top" class="lable">
																		         	 	<table> 
																			      	 		<tr>
																			      	 			<c:if test="${OrderReturnDetails.brandName ne null and OrderReturnDetails.brandName ne ''}">
																			      	 				<td> Brand Name </td>
																			      	 				<td> : </td>
																			      	 				<td> <bean:write name="OrderReturnDetails" property="brandName"/> </td><br/> 
																			      	 			</c:if>
																			      	 		</tr>
																			      	 		<tr>
																		      	 				<td>Item Name </td>
																		      	 				<td> : </td>
																		      	 				<td><bean:write name="OrderReturnDetails" property="itemName"/></td><br/>
																		      	 			</tr>
																		      	 			<tr>
																		      	 				<c:if test="${OrderReturnDetails.size ne null and OrderReturnDetails.size ne ''}">
																			      	 				<td>Size </td>
																			      	 				<td> : </td>
																			      	 				<td><bean:write name="OrderReturnDetails" property="size"/></td><br/>
																		      	 				</c:if>
																		      	 			</tr>
																		      	 			<tr>
																		      	 				<c:if test="${OrderReturnDetails.color ne null and OrderReturnDetails.color ne ''}">
																			      	 				<td>Color </td>
																			      	 				<td> : </td>
																			      	 				<td><bean:write name="OrderReturnDetails" property="color"/></td><br/>
																		      	 				</c:if>
																		      	 			</tr>
																		      	 			<tr>
																		      	 				<c:if test="${OrderReturnDetails.travelDate ne null and OrderReturnDetails.travelDate ne ''}">
																			      	 				<td> Travel Date </td>
																			      	 				<td> : </td>
																			      	 				<td> <bean:write name="OrderReturnDetails" property="travelDate"/> </td><br/>
																			      	 			</c:if>	
																			      	 		</tr> 
																			      	 	</table>
																		         	 </td>          	
																					 <!-- <td align="left" nowrap="nowrap" class="lable">      
																						 <logic:iterate id="category" name="Category"scope="application">
																						 <c:if test="${category.cateId eq PartnerOrderDetails.cateId}">
																						  <bean:write name="category" property="cateName" />
																						 </c:if>
																						 </logic:iterate>
																						 <c:if test="${PartnerOrderDetails.cateId eq 20}">
																						 	Private Jet Jaunts																						 
																						</c:if>																				 	
																					 </td>  -->         			
																					 <td align="right" nowrap="nowrap" class="lable"><bean:write name="PartnerReturnDetails" property="returnQuantity"/></td>
																					 <td align="right" nowrap="nowrap" class="lable"><bean:write name="PartnerOrderDetails" property="sbhPrice"/></td>   
																					 <td align="left" nowrap="nowrap" class="lable">
																					 	<c:if test="${PartnerReturnDetails.rmaStatus eq 'RG'}">
																					 		RMA Generated
																					 	</c:if>
																					 	<c:if test="${PartnerReturnDetails.rmaStatus eq 'WA' || PartnerReturnDetails.rmaStatus eq 'WR'}">
																					 		Order in process
																					 	</c:if>
																					 	<c:if test="${PartnerReturnDetails.rmaStatus eq 'RA'}">
																					 		RMA Approved
																					 	</c:if>
																					 	<c:if test="${PartnerReturnDetails.rmaStatus eq 'RR'}">
																					 		RMA Rejected
																					 	</c:if>
																					 </td>    
																					 <td align="left" nowrap="nowrap" class="lable"><bean:write name="PartnerOrderDetails" property="orderCreatedDt"/></td>            	
																					 <!-- <td align="left" nowrap="nowrap" class="lable"><bean:write name="PartnerOrderDetails" property="vendorName"/></td> -->
																				 </tr>
																  			</tbody>
																  		</table>																  	
																  	</td>
																 </tr>
															</table>										          		
														</td>
										          	</tr>
										          	<tr class="tdbg">
										          		<td class="lable" colspan="3">
										          			<hr/>										          		
										          		</td>
										          	</tr>
										          	<tr class="tdbg">
										          		<td width="17%" align="left" nowrap="nowrap" class="lable">
							          						Reason for return							          					
							          					</td>
							          					<td width="2%" class="lable">
							          						:							          					
							          					</td>
							          					<td width="81%" align="left" class="lable">
						          							<bean:write name="PartnerReturnDetails" property="reasonForReturn"/>
					          						  	</td>
										          	</tr>
										          	<!--<c:if test="${PartnerReturnDetails.returnQuantity ne null and PartnerReturnDetails.returnQuantity ne ''}">
											          	<tr class="tdbg">
											          		<td align="left" nowrap="nowrap" class="lable">
								          						No: of quantity to return
								          					</td>
								          					<td class="lable">
								          						:							          					
								          					</td>
								          					<td width="100%" align="left" class="lable">
								          						<bean:write name="PartnerReturnDetails" property="returnQuantity"/>
							          						</td>
											          	</tr>
										          	</c:if>-->
										          	<tr class="tdbg">
										          		<td class="lable" colspan="3">
										          			<hr/>										          		</td>
										          	</tr>
										          	<tr class="tdbg">
										          	  <td  valign="top" align="center" colspan="3">
													  (Return the item to below mentioned shipping address)
													  <div class="tear">
													    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                          <tr>
                                                            <td align="left" valign="top" nowrap="nowrap">Return Address</td>
                                                            <td width="20" align="left" valign="top" nowrap="nowrap"><div align="center">:</div></td>
                                                            <td width="20" valign="top"><p>To,<br /></p></td>
                                                            <td width="85%" align="left" valign="top" nowrap="nowrap"><BR />
                                                              M/s. <bean:write name="PartnerReturnDetails" property="ownerName"/>
                                                              <br/>
											        		  <bean:write name="PartnerReturnDetails" property="partnerAddress1"/>
											        		  <br/>
											        		  <c:if test="${PartnerReturnDetails.partnerAddress2 ne null && PartnerReturnDetails.partnerAddress2 ne ''}">
											        			<bean:write name="PartnerReturnDetails" property="partnerAddress2"/><br/>
											        		  </c:if>
											        		  <bean:write name="PartnerReturnDetails" property="partnerCity"/>
											        		  ,&nbsp;<br />
											        		  <logic:iterate id="states" name="StateList" scope="application">
																<c:if test="${states.stateCode eq PartnerReturnDetails.partnerState}">
																	<bean:write name="states" property="stateName" />																</c:if>
															  </logic:iterate>
										        		    <bean:write name="PartnerReturnDetails" property="partnerZip"/>.</td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" align="left" valign="top" nowrap="nowrap"></td>
                                                          </tr>
                                                        </table>
													  </div>
													  </td>
									          	  </tr>
										          	<tr class="tdbg">
									          		  <td class="lable" valign="top" align="left" colspan="3">&nbsp;</td>
										          	</tr>
										          	<tr class="tdbg">
										          		<td class="lable" align="left">										          		</td>
											        	<td class="lable" align="left" colspan="2">
											        		</td>	
										          	</tr>
								  				</tbody>
								  			</table>
								  			<!-- END - TABLE border_top0 -->
								  	 	 </td>
								  	</tr>
								  	<tr class="tdbg">
						          		<td class="lable"  colspan="3">
						          			<hr/>
						          		</td>
						          	</tr>
								  	<tr>
								  		<td height="42">
								  			<input type="button" class="button" onclick="javascript:fnCallHome();" value="OK" />							  		  </td>
								  	</tr>
								  </tbody>
								</table>
					     	</logic:notEmpty>
			     		</logic:present>
	     			</td>
				</tr>
				<tr>
					<td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
					<td background="../images/top_nav_bmiddlebg.png"> </td>
					<td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
				</tr>
			  
			  </tbody>
			</table>
			<!-- END - TABLE - Outer Table -->
			
	    </div>
	    <!-- END - DIV - contentcontainer -->
    </td></tr>
</html:form>