<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">


	window.onload=function() {
		tablecollapse();
	}
	function fnCallSubmit(js_OrderItemId,IsUpdate) {
		var trackingDetails = document.forms[0].orderTrackingDetail.value;
		if(trackingDetails != "") {
			document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+js_OrderItemId+"&IsUpdate="+IsUpdate
			document.forms[0].submit();		
		}else {
			alert("Value must be Supplied");
		}
	}
	function fnCallViewRMAOrderDetails(jsReturnId) {
		document.forms[0].action = "displayRMADetails.do?method=displayRMADetails&ReturnId="+jsReturnId;
		document.forms[0].submit();
	}
	function fnCallViewCustOrderDetails() {
		document.forms[0].action = "customersLogin.do?method=getTransactionDetails";
		document.forms[0].submit();
	}

</script>
<html:form action="/customer/viewRMADetails" method="post">
	<tr><td>
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table">
		<tbody>
			<tr>
			    <td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
			    <td width="100%" background="../images/top_nav_middlebg.png" align="left">
			   		<ul>
						<li><font size="4">Return Merchandise Authorization History</font></li>
					</ul>				
				</td>
			    <td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
			</tr>
			<logic:present name="RMADetails">
				<logic:notEmpty name="RMADetails">
					<tr class="tdbg">
					  <td colspan="3" >&nbsp;</td>
					</tr>
					<tr class="tdbg">
					  <td class="td" colspan="3"><table border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td align="left"><table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td	align="left" nowrap="nowrap" height="30" class="lable"> Order Item ID </td>
                              <td class="lable">:</td>
                              <td align="left" nowrap="nowrap" class="lable"><c:out value="${OrderItemId}"/>
                              </td>
                            </tr>
                            <tr>
                              <td align="left" nowrap="nowrap" height="30" class="lable"> Order Confirmation No </td>
                              <td class="lable">:</td>
                              <td nowrap="nowrap" class="lable" align="left"><c:out value="${CustTransId}"/>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td colspan="3"><table border="0" cellpadding="0" cellspacing="0" class="border">
                            <tr>
                              <td height="30" colspan="3" class="tablehead" align="center"><h2>RMA Details</h2></td>
                            </tr>
                            <tr>
                              <td width="100%" colspan="3" bgcolor="#CCCCCC" class="td"><table border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
                                  <tr>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Return Id </td>
                                    <!-- <td	align="center" nowrap="nowrap" height="30" class="table_header"> Order Item Id </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Order Confirmation No. </td> -->
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> RMA Number </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> RMA Generated Date </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Status </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Return Quantity </td>
                                    <td	align="center" nowrap="nowrap" height="30" class="table_header"> Action </td>
                                  </tr>
                                  <logic:iterate id="rmaDetails" name="RMADetails">
                                    <tr bgcolor="#FFFFFF">
                                      <td nowrap="nowrap" class="lable"><bean:write name="rmaDetails" property="returnId"/></td>
                                      <!-- <td nowrap="nowrap" class="lable" align="left"><bean:write name="rmaDetails" property="orderItemId"/></td>
                                      <td nowrap="nowrap" class="lable"><bean:write name="rmaDetails" property="custTransId"/></td> -->
                                      <td nowrap="nowrap" align="left" class="lable"><bean:write name="rmaDetails" property="RMANumber"/></td>
                                      <td nowrap="nowrap" align="left" class="lable"><bean:write name="rmaDetails" property="rmaGeneratedDate"/></td>
                                      <td nowrap="nowrap" class="lable" align="left">
                                      	  <c:if test="${rmaDetails.rmaStatus eq 'RA'}"> RMA Approved </c:if>
										  <c:if test="${rmaDetails.rmaStatus eq 'RG'}"> RMA Generated </c:if>
                                          <c:if test="${rmaDetails.rmaStatus eq 'RR'}"> RMA Rejected </c:if>
										  <c:if test="${rmaDetails.rmaStatus eq 'WA'}"> Order in Process </c:if>
										  <c:if test="${rmaDetails.rmaStatus eq 'WR'}"> Order in Process </c:if>
                                      </td>
                                      <td class="lable" align="right"><bean:write name="rmaDetails" property="customerReturnQuantity"/></td>
                                      <td nowrap="nowrap" class="lable" align="center"><a href="javascript:fnCallViewRMAOrderDetails('<bean:write name="rmaDetails" property="returnId"/>')">View</a> </td>
                                    </tr>
                                  </logic:iterate>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
				      </td>
					</tr>
				</logic:notEmpty>
			</logic:present>
			<logic:empty name="RMADetails">
				<tr>
					<td colspan="3" class="td" align="center" style="margin:30px;">
						<font color="#FF0000"> Not yet RMA is generated for this Order Item.</font>
					</td>
				</tr>
			</logic:empty>
			<tr>
				<td colspan="3" class="td" align="center">
					<input type="button" style="margin:10px;" onclick="javascript:fnCallViewCustOrderDetails();" class="button" value="OK" />
				</td>
			</tr>
			<tr>
			    <td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
			    <td background="../images/top_nav_bmiddlebg.png"> </td>
			    <td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
			</tr>
		</tbody>
	</table>
</div>
</td></tr>
</html:form>