<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link type="text/css" href="../style/registration.css" rel="stylesheet">

<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script type="text/javascript">


	window.onload=function() {
		tablecollapse();
	}
	function fnCallSubmit(js_OrderItemId,IsUpdate) {
		var trackingDetails = document.forms[0].orderTrackingDetail.value;
		if(trackingDetails != "") {
			document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+js_OrderItemId+"&IsUpdate="+IsUpdate
			document.forms[0].submit();		
		}else {
			alert("Value must be Supplied");
		}
	}
	
	function fnCallViewCustOrderDetails() {
		document.forms[0].action = "customersLogin.do?method=getTransactionDetails";
		document.forms[0].submit();
	}
</script>
<html:form action="/customer/custOrderTrackingDetail" method="post">
	<tr><td>
	
	<div class="contentcontainer">
	<table border="0" cellpadding="0" cellspacing="0" class="table" align="center">
		<tbody>
			<tr>
			    <td class="leftcutver"><img height="44" width="26" src="../images/top_nav_leftcurve.png" style="width: 26px; height: 44px;"/></td>
			    <td width="100%" background="../images/top_nav_middlebg.png" align="left">
		   			<ul>
						<li><font size="4">Order Tracking History</font></li>
					</ul>
				</td>
			    <td align="right"><img height="44" width="26" src="../images/top_nav_rightcurve.png"/></td>
			</tr>
			<c:if test="${NoTrackingDetails eq 'Shipped'}">
				<tr class="tdbg">
					<td class="td" colspan="3" width="80%">
						<table width="70%" border="0" cellpadding="0" cellspacing="0" align="center" style="margin:30px" class="border">
							<div><B>Shipment Status : <c:if test="${ShipmentStatus ne 'Y'}"> Not yet shipped </c:if> <c:if test="${ShipmentStatus eq 'Y'}"> Order shipped </c:if></B> </div>
							<tr>
					        	<td height="30" colspan="3" class="tablehead" align="center"><h2>Order Tracking Details</h2></td>
					        </tr>
					        <tr>
					        	<td colspan="3" class="td" width="100%" bgcolor="#cccccc">
					        		<table align="center" border="0" cellpadding="0" cellspacing="1" width="100%" bgcolor="#cccccc">
										<tr>
											<td align="center" nowrap="nowrap" width="70%" height="30" class="table_header">
												Tracking Details
											</td>
											<td	align="center" nowrap="nowrap" height="30" class="table_header">
												Date
											</td>
										</tr>
										<logic:iterate id="trackingDetail" name="TrackingDetails">
											<tr bgcolor="#FFFFFF">
												<td class="lable"> 
														<bean:write name="trackingDetail" property="trackingDetails"/> 
												</td>
												<td class="lable">
														<bean:write name="trackingDetail" property="create_dt"/>
												</td>
											</tr>
										</logic:iterate>
									</table>
								</td>
							</tr>
							<!-- <tr>
								<td colspan="3" class="td">
									<div style="margin:15px;">
										<html:button property="method" onclick="history.back()" styleClass="button">OK</html:button>
									</div>
								</td>
							</tr> -->
						</table>
					</td>
				</tr>
				<!-- <tr class="tdbg">
					<td colspan="3" class="lable" style="margin:15px">
						<logic:present name="TrackingDetails" scope="request">
							<logic:notEmpty name="TrackingDetails" scope="request">
								<div id="TrackingDetails" style="margin:15px;">
									<table width="80%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
						     			<thead class="tablehead">
						     				<tr>
						     					<td width="70%">
						     						Tracking History
						     					</td>
						     					<td>
						     						Date
						     					</td>
						     					<td>
						     						Shipment Status
						     					</td>
						     				</tr>
						     			</thead>
				
						     			<tfoot>
								          <tr>
								            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
								          </tr>
								        </tfoot>
				
						     			<tbody>
							     			<logic:iterate id="trackingDetail" name="TrackingDetails">
						     					<tr>
													<td align="left">
														<bean:write name="trackingDetail" property="trackingDetails" />
													</td>
													<td align="left" nowrap="nowrap">
														<bean:write name="trackingDetail" property="create_dt" />
													</td>
													<td align="left" nowrap="nowrap">
														<c:if test="${trackingDetail.shipmentStatus ne 'Y'}">
															Not yet shipped
														</c:if>
														<c:if test="${trackingDetail.shipmentStatus eq 'Y'}">
															Order shipped
														</c:if>
													</td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
								</div>
							</logic:notEmpty>
						</logic:present>
					</td>
				</tr> -->
			</c:if>
			<c:if test="${NoTrackingDetails ne 'Shipped'}">
				<tr class="tdbg">
					<td class="td" colspan="3" width="80%" align="center">
						<table width="70%" border="0" cellpadding="0" cellspacing="0" style="margin:30px" class="border">
							<tr>
					        	<td height="30" colspan="3" class="tablehead" align="center"><h2>Order Tracking Details</h2></td>
					        </tr>
					        <tr>
					        	<td class="td">
					        		<div style="margin:15px">
										<font color="red"><c:out value="${NoTrackingDetails}" /></font>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>	
			</c:if>
			<tr class="tdbg">
				<td class="td" colspan="3" align="center">
					<html:button property="method" onclick="javascript:fnCallViewCustOrderDetails();" styleClass="button">OK</html:button>
				</td>
			</tr>
			<tr>
			    <td><img height="34" width="26" src="../images/top_nav_bleftcurve.png"/></td>
			    <td background="../images/top_nav_bmiddlebg.png"> </td>
			    <td align="right"><img height="34" width="26" src="../images/top_nav_brightcurve.png"/></td>
			</tr>
		</tbody>
	</table>
</div>
</div>
</td></tr>
</html:form>