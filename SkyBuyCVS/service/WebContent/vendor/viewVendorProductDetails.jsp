<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="JavaScript1.2" src="../js/col_exp_table.js"></script>
<script>
window.onload=function() {
		tablecollapse();
	}
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/




function fnCallAddProduct(){
	document.forms[0].action="addProduct.do?method=addProductDetails";
	document.forms[0].submit();
}
function fnCallSaveProduct(){
	document.forms[0].action="updateProduct.do?method=updateProductDetails";
	document.forms[0].submit();
}

function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}

function getCatalogue(link, windowname){
	window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');

}

function fnCallEditCatalogue() {
	document.forms[0].action="searchVendorCatalogue.do?method=searchVendorCatalogue";
	document.forms[0].submit();
}

function fnCallHome() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();
}

function fnCallEdit(jsProdIdValue) {
	document.forms[0].action="editVendorProduct.do?method=editVendorProductDetails&ProdId="+jsProdIdValue+"&SourceOfEdit=View";
	document.forms[0].submit();
}

</script>




<html:form action="vendor/addVendorProduct" method="post" enctype="multipart/form-data">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
	
		<c:if test="${mode eq 'editVendorProduct'}">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item</li>
		</ul>
		</c:if>
		
		<c:if test="${mode eq 'addVendorProduct'}">
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Add Item</li>
		</ul>
		</c:if>
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Item Summary Page</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table border="0" cellpadding="2" cellspacing="2" class="broder_top0">
			<tr class="tdbg">
        		<td height="15" colspan="6" align="right"><div align="right" class="editlink"><a href="javascript: void fnCallEdit('<bean:write name='vendorProductDetails' property='prodId'/>')" title="Edit Merchandise">Edit Merchandise</a></div></td>
       		</tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">Vendor Name</td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td width="153" align="left" valign="top" nowrap="nowrap"><span class="catagory">
         <logic:present name="vendorLoginInfo" scope="session">
			<bean:write  name="vendorLoginInfo" property="userName" />			
		</logic:present>
      </span></td>
            <td width="87" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Category</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td width="345" align="left" valign="top" class="catagory"><span class="catagory">
          <logic:iterate id="category" name="Category"scope="application">
            <c:if test="${category.cateId eq vendorProductDetails.cateId}">
              <bean:write name="category" property="cateName" />
            </c:if>
          </logic:iterate>
        </span></td>
          </tr>
        <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Item Code </span></td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="vendorProductDetails"  property="prodCode" />
        </span></td>
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Item Name</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="vendorProductDetails"  property="prodTitle" />
        </span></td>
          </tr>
        <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Item Status </span></td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <c:if test="${vendorProductDetails.inShopStatus eq 'A'}">Active</c:if>
          <c:if test="${vendorProductDetails.inShopStatus eq 'I'}">Inactive</c:if>
        </span></td>
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Approval Status</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <c:if test="${vendorProductDetails.sbhProdStatus eq 'N'}">Pending</c:if>
          <c:if test="${vendorProductDetails.sbhProdStatus eq 'A'}">Accepted</c:if>
          <c:if test="${vendorProductDetails.sbhProdStatus eq 'R'}">Rejected</c:if>
        </span></td>
          </tr>
           <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Color</span></td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="vendorProductDetails"  property="color" />
        </span></td>
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Size</span></td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="vendorProductDetails"  property="size" />
        </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" class="lable"><span class="boldtext">Brand Name</span></td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
          <bean:write  name="vendorProductDetails"  property="brandName"/>
        </span></td>
            <td align="left" valign="top" nowrap="nowrap" class="lable">Retail Price</td>
            <td width="10" align="left" valign="top" class="lable">:</td>
            <td align="left" valign="top" class="catagory"><span class="catagory">
       	 <fmt:setLocale value="en_US" /><fmt:formatNumber value="${vendorProductDetails.vendPrice}" type="currency" currencyCode="USD" pattern="$#,###,##0.00;-$#,###,##0.00"/>
        </span></td>
          </tr>
           <tr>
         	<td align="left" valign="top" nowrap="nowrap" class="lable">Pre-Authorize Credit Card </td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td  align="left" valign="top" class="catagory">
            <c:if test="${vendorProductDetails.preAuthCC eq 'Y'}">
            	Yes
            </c:if>
            <c:if test="${vendorProductDetails.preAuthCC eq 'N' || vendorProductDetails.preAuthCC eq null}">
            	No
            </c:if>
            </td>
            <td  align="left" valign="top" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" valign="top" class="lable">&nbsp;</td>
            <td align="left" valign="top" class="catagory">&nbsp;</td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap"  class="lable">Title</td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td colspan="4" align="left" valign="top" style="width:600px; word-wrap:break-word; "><c:out value="${vendorProductDetails.shortDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">Full Description </td>
            <td width="26" align="left" valign="top" class="lable">:</td>
            <td colspan="4" align="left" valign="top" class="catagory" style="width:600px; word-wrap:break-word; "><c:out value="${vendorProductDetails.longDesc}" escapeXml="false"/></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
        
          <tr class="tdbg">
            <td align="left" valign="top" class="lable">Item Image(s)</td>
            <td width="26" align="left" valign="top" class="lable"></td>
            <td colspan="4" align="left" class="catagory"></td>
          </tr>
		  
		  <tr align="left" class="tdbg" >
		  <td colspan="6" align="center">
		 
		  
		  <table border="0" cellspacing="2" cellpadding="2">
            <tr>
	
            <c:if test="${!empty vendorProductDetails.mainImgPath}">
              <td class="border" align="center"><img src="<c:out value='${vendorProductDetails.mainImgPath}' />" width="63" height="79" /></td>
			</c:if>
			  <c:if test="${!empty vendorProductDetails.view1ImgPath}">
              <td class="border" align="center"><img src="<c:out value='${vendorProductDetails.view1ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${!empty vendorProductDetails.view2ImgPath}">
              <td class="border" align="center"><img src="<c:out value='${vendorProductDetails.view2ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${!empty vendorProductDetails.view3ImgPath}">
              <td class="border" align="center"><img src="<c:out value='${vendorProductDetails.view3ImgPath}' />" width="63" height="79" /></td>
			  </c:if>
			   <c:if test="${empty vendorProductDetails.mainImgPath && empty vendorProductDetails.view1ImgPath && empty vendorProductDetails.view2ImgPath && empty vendorProductDetails.view3ImgPath}">
			   	<td class="border" align="center"><img src="<c:out value='${vendorProductDetails.noImgPath}' />" width="79" height="79" /></td>
			   </c:if>
            </tr>
            <tr>
             <c:if test="${!empty vendorProductDetails.mainImgPath}"><td bgcolor="#d9ecff" class="border"  align="center" style="width:100px;"><bean:write  name="vendorProductDetails"  property="mainImgCap" />&nbsp;</td></c:if>
              <c:if test="${!empty vendorProductDetails.view1ImgPath}"><td bgcolor="#d9ecff" class="border" align="center" style="width:100px;"><bean:write  name="vendorProductDetails"  property="view1ImgCap" />&nbsp;</td></c:if>
              <c:if test="${!empty vendorProductDetails.view2ImgPath}"><td bgcolor="#d9ecff" class="border" align="center" style="width:100px;"><bean:write  name="vendorProductDetails"  property="view2ImgCap" />&nbsp;</td></c:if>
              <c:if test="${!empty vendorProductDetails.view3ImgPath}"><td bgcolor="#d9ecff" class="border" align="center" style="width:100px;"><bean:write  name="vendorProductDetails"  property="view3ImgCap" />&nbsp;</td></c:if>
            </tr>
          </table>
		  
		 </td>
		  </tr>
		
		  <c:if test="${mode eq 'editVendorProduct'}">
		  <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable">Comments </td>
            <td width="26" valign="top" class="lable">&nbsp;</td>
            <td colspan="4" align="left" class="catagory"><!--<c:out value="${vendorProductDetails.sbhComment}" escapeXml="false"/>--></td>
          </tr>
          <tr class="tdbg">
					<td colspan="6" style="margin:5px 0 5px 0;">
						<logic:present name="productComments" scope="request">
							<logic:notEmpty name="productComments" scope="request">
								<div id="productComments" style="margin:5px 0 5px 0;">
									<table width="99%" align="center" border="1" cellpadding="0" cellspacing="0" class=footcollapse>
						     			<thead class="tablehead">
						     				<tr height="21">
						     					<td align="center">
						     						User Id
						     					</td>
						     					<td align="center">
						     						Date
						     					</td>
						     					<td width="70%" align="center">
						     						Comments
						     					</td>
						     				</tr>
						     			</thead>
				
						     			<tfoot>
								          <tr>
								            <td colspan=3 bgcolor="#E6EEFB" align="right" border="0px" class="normaltext"></td>
								          </tr>
								        </tfoot>
				
						     			<tbody>
							     			<logic:iterate id="prodComment" name="productComments">
						     					<tr height="20">
													<td align="left">
														<bean:write name="prodComment" property="userId" />
													</td>
													<td align="center" nowrap="nowrap">
														<bean:write name="prodComment" property="date" />
													</td>
													<td align="left">
														<bean:write name="prodComment" property="comments" />
													</td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
								</div>
							</logic:notEmpty>
						</logic:present>
					</td>
				</tr>
		  </c:if>
		 
	          <tr align="left" class="tdbg">
	            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
	          </tr>
            <c:if test="${!empty vendorProductDetails.mainImgPath}">
			    <c:if test="${vendorProductDetails.inShopStatus eq 'A'}">  
	          	<tr align="left" class="tdbg">
	           <td colspan="6" valign="top" class="clickhere"><a href="javascript:getCatalogue('../previewProduct.jsp','vendorproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue </td>
	            </tr>
				</c:if>
			</c:if>
      </table>
		</td>
      </tr>
      
      <tr>
        <c:if test="${mode eq 'editVendorProduct'}">
        	<td height="50" colspan="3" align="center"> 
		   		<!--<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none">&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</html:link>-->
			   <html:button property="method" onclick="javascript:fnCallEditCatalogue();" value="OK" styleClass="button"></html:button> 
		  	</td>
		</c:if>
		<c:if test="${mode eq 'addVendorProduct'}">
        	<td height="50" colspan="3" align="center"> 
		   		<!--<html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none">&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</html:link>-->
			   <html:button property="method" onclick="javascript:fnCallHome();" value="OK" styleClass="button"></html:button> 
		  	</td>
		</c:if>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>
</html:form>
