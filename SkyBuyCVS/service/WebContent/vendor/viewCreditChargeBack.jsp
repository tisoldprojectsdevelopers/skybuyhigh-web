<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="../WEB-INF/fmt.tld" prefix="fmt" %>

<script src="../js/datepicker.js" type=text/javascript></script>
<link href="../style/datepic.css" rel="stylesheet" type="text/css" />
<link href="../style/registration.css" rel="stylesheet" type="text/css" />
<link href="../style/Menu.css" rel="stylesheet" type="text/css" />

<script>

	
	function trim(inputString) {
			 var retValue = inputString;
			 var ch = retValue.substring(0, 1);
			 while (ch == " ") {
					retValue = retValue.substring(1, retValue.length);
					ch = retValue.substring(0, 1);
			 }
			 ch = retValue.substring(retValue.length-1, retValue.length);
			 while (ch == " ") {
					retValue = retValue.substring(0, retValue.length-1);
					ch = retValue.substring(retValue.length-1, retValue.length);
			 }
			 return retValue;
	}
	
	function trimQuantity(inputQuantity) {
		var retVal=inputQuantity;
		var startChar=retVal.substring(0,1);
		while(startChar=="0") {
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1);
		}
		return retVal;
	}
	
	function disablePaste(e)
		{
		  
		  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
	      {
	       window.clipboardData.clearData();
			
	     }
	   
	     
	    return true; 
		}
	
	function stripTags(txt) { 
		var str = new String(txt); 
		str = str.replace(/<br\/>/gi,"\n"); 
		str=str.replace(/<[^>]+>/g,"");
		str=str.replace(/&nbsp;/gi,"");
		return str;
	}
	 
	function textLimit(fieldLen,maxlen,dispName) {
		if (fieldLen > parseInt(maxlen) + 1){
			alert(dispName+" can have maximum of "+maxlen+" chars only."); 
			return false;
		}else
			return true;
	}
	
	
/****Offer Description B****/


	function displayReturnPolicy(orderItemId,ownerType) {
		document.forms[0].action="returnPolicy.do?method=displayReturnPolicy&OrderItemId="+orderItemId+"&OwnerType="+ownerType;
		document.forms[0].submit();
	}
	
	function fnCallProcessOrder(jsRMANumber) {
		document.forms[0].action="return.do?method=updateRMAStatus&RMANumber="+jsRMANumber;
		document.forms[0].submit();
	}
	
	function fnCallReturn(jsRMANumber) {
		document.forms[0].action="initOrderReturn.do?method=initOrderReturn";
		document.forms[0].submit();
	}
	
</script>

<html:form action="vendor/return" method="post">
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">			
		<ul>
		<li>You navigated from :</li>
		<li>Order</li>	
		<li>></li>
		<li>Accept/Reject Return</li>	
		</ul>		
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
			<c:if test="${updated ne null and updated ne ''}">
				<tr class="tdbg">
	            	<td colspan="6" align="center" nowrap="nowrap" class="lable"><font color="#FF0000"><c:out value="${updated}" /> </font></td>
	            </tr>
			</c:if>
            <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Order Confirmation No </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="ReturnOrderItemDetails" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable" >Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ReturnOrderItemDetails" property="custFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" >Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              	<bean:write  name="ReturnOrderItemDetails" property="custLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"  valign="top"> Address Line 1 </td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" class="catagory" valign="top"><span class="catagory">
				<bean:write  name="ReturnOrderItemDetails" property="custAddress1" />		
       		</span></td>
       		<c:if test="${ReturnOrderItemDetails.custAddress2 ne null && ReturnOrderItemDetails.custAddress2 ne ''}">
	            <td align="left" nowrap="nowrap" class="lable" > Address Line 2</td>
	            <td align="left" class="lable">:</td>
	            <td align="left" class="catagory"><span class="catagory">
					<bean:write  name="ReturnOrderItemDetails" property="custAddress2" />		
	       		</span></td>
       		</c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			   <bean:write  name="ReturnOrderItemDetails" property="custCity" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<logic:iterate id="states" name="StateList" scope="application">
					<c:if test="${states.stateCode eq ReturnOrderItemDetails.custState}">
						<bean:write name="states" property="stateName" />
					</c:if>
				</logic:iterate>
            </span></td>
          </tr>
          <tr class="tdbg">
             <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <bean:write  name="ReturnOrderItemDetails" property="custCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="ReturnOrderItemDetails" property="custZip" />
            </span></td>
          </tr>
		   <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             	<bean:write  name="ReturnOrderItemDetails" property="custPhone" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable" ><span class="boldtext">Email&nbsp;  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<bean:write  name="ReturnOrderItemDetails" property="custEmail" />
            </span></td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ReturnOrderItemDetails" property="custShipFirstName" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <bean:write  name="ReturnOrderItemDetails" property="custShipLastName" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable" valign="top"><span class="boldtext">Address Line 1</span></td>
            <td align="left" class="lable" valign="top">:</td>
            <td align="left" nowrap="nowrap" class="catagory" valign="top"><span class="catagory">
            <bean:write  name="ReturnOrderItemDetails" property="custShipAddress1" />
            </span></td>
            <c:if test="${(ReturnOrderItemDetails.custShipAddress2 ne null && ReturnOrderItemDetails.custShipAddress2 ne '')}">
	            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address Line 2</span></td>
	            <td align="left" class="lable">:</td>
	            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
	            <bean:write  name="ReturnOrderItemDetails" property="custShipAddress2" />
	            </span></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="ReturnOrderItemDetails" property="custShipCity" />
            </span></td>
            
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
            <logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq ReturnOrderItemDetails.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              <!--  <bean:write  name="ReturnOrderItemDetails" property="custShipState" />-->
            </span></td>
          </tr>
          <tr class="tdbg">
           <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
           <bean:write  name="ReturnOrderItemDetails" property="custShipCountry" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <bean:write  name="ReturnOrderItemDetails" property="custShipZip" />
             </span></td>
          </tr>
          <tr class="tdbg">
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Phone </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ReturnOrderItemDetails" property="custShipPhone" />
            </span></td>
          	<td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
                <bean:write  name="ReturnOrderItemDetails" property="custShipEmail" />
            </span></td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Air Charter/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable" > <span class="boldtext">Air Charter Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ReturnOrderItemDetails"  property="airName"/>
            </span></td>
            <c:if test="${ReturnOrderItemDetails.flightNo ne null}">
            	<td align="left" nowrap="nowrap" class="lable" >Tail No </td>
            	<td align="left" class="lable">:</td>
            </c:if>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="ReturnOrderItemDetails"  property="flightNo" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" ><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="ReturnOrderItemDetails"  property="vendorName"/>
        </span></td>
           <td colspan="3" align="left" nowrap="nowrap" class="lable"><div class='cus-service'><a href="javascript:displayReturnPolicy('<bean:write  name="ReturnOrderItemDetails"  property="orderItemId"/>','<bean:write  name="ReturnOrderItemDetails"  property="ownerType"/>')" title="Customer Service">Click here to view Customer Service</a></div></td>
          </tr>
          <tr align="left" class="tdbg">
            <td colspan="9" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ReturnOrderItemDetails"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="11" align="left" class="lable"><div align="center"><img src="<c:out value='${ReturnOrderItemDetails.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			    <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Item Id</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ReturnOrderItemDetails"  property="orderItemId" />
               </span></td>
			   </tr>
			   <c:if test="${ReturnOrderItemDetails.rmaGeneratedNo ne null and ReturnOrderItemDetails.rmaGeneratedNo ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">RMA Number</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                 <bean:write  name="ReturnOrderItemDetails"  property="rmaGeneratedNo" />
		               </span></td>
				   </tr>
			   </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ReturnOrderItemDetails"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq ReturnOrderItemDetails.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${ReturnOrderItemDetails.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="ReturnOrderItemDetails"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ReturnOrderItemDetails"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="ReturnOrderItemDetails"  property="itemName"/>
               </span></td>
			   </tr>
			   <c:if test="${ReturnOrderItemDetails.ownerType eq 'airline'}">
				  <tr class="tdbg">
				  <c:if test="${LoginType eq 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="YES"/>
				  </c:if>
				  <c:if test="${LoginType ne 'admin'}">
				  		<input type="hidden" name="TravelDatePresent" value="NO"/>
				  </c:if>
				  <c:if test="${ReturnOrderItemDetails.travelDate ne null || (LoginType eq 'admin' && (ReturnOrderItemDetails.orderItemStatus eq 'P'))}">
					   <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Travel Date </span></td>
					   <td align="left" class="lable">:</td>
				  </c:if>
				   <td align="left" class="catagory"><span class="catagory">
	                	<bean:write  name="ReturnOrderItemDetails" property="travelDate" />
                   </span></td>
				   </tr>
			   </c:if>

			  <c:if test="${ReturnOrderItemDetails.ownerType eq 'vendor'}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="ReturnOrderItemDetails"  property="brandName" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${ReturnOrderItemDetails.size ne null and ReturnOrderItemDetails.size ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Size </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="ReturnOrderItemDetails"  property="size" />
				   </span></td>
				 </tr>
			 </c:if>
			  <c:if test="${ReturnOrderItemDetails.color ne null and ReturnOrderItemDetails.color ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Color </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
					 <bean:write  name="ReturnOrderItemDetails"  property="color" />
				   </span></td>
				 </tr>
			 </c:if>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'P' && ReturnOrderItemDetails.ownerType eq 'vendor'}"> Order to be placed with Vendor</c:if>
                 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'P' && ReturnOrderItemDetails.ownerType eq 'airline'}"> Order to be placed with Air Charter</c:if>				                 
                 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'C'}"> Completed </c:if>
                 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'F'}"> Failed </c:if>
                 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'R'}"> Rejected </c:if>
                 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'Q' && ReturnOrderItemDetails.ownerType eq 'vendor'}"> To be charged after Vendor approval</c:if>
				 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'Q' && ReturnOrderItemDetails.ownerType eq 'airline'}"> To be charged after Air Charter approval</c:if>
				 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'A'}"> RMA Approved</c:if>
				 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'E'}"> RMA Rejected</c:if>
                 <!--    <bean:write  name="ReturnOrderItemDetails"  property="orderItemStatus" />-->
               </span></td>
			   </tr>
			   <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'F' && ReturnOrderItemDetails.reasonForFailure ne null && ReturnOrderItemDetails.reasonForFailure ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Failure</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="ReturnOrderItemDetails"  property="reasonForFailure" />
				   </span></td>
				 </tr>
			 </c:if>
			 <c:if test="${ReturnOrderItemDetails.orderItemStatus eq 'R' && ReturnOrderItemDetails.reasonForReject ne null && ReturnOrderItemDetails.reasonForReject ne ''}">		   
				 <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason For Rejected</td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
					 <bean:write  name="ReturnOrderItemDetails"  property="reasonForReject" />
				   </span></td>
				 </tr>
			 </c:if>
			 <!-- <tr class="tdbg">			
				   <td align="left" class="lable">
					   	In-Store Price
				   </td>
			   		<td align="left" class="lable">:</td>
			  		<td align="left" class="catagory">
		   				<span class="catagory">
		   					<bean:write name="ReturnOrderItemDetails"  property="poPrice"/>
              			</span>
              		</td>			  
			   </tr> -->
			 	<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Return Quantity</td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
	                   <bean:write  name="ReturnOrderItemDetails" property="custReturnQuantity" />
	                </span></td>
			   </tr>
			   <c:if test="${ReturnOrderItemDetails.returnQuantity ne null and ReturnOrderItemDetails.returnQuantity ne ''}">
				   <tr class="tdbg">
					   <td align="left" nowrap="nowrap" class="lable">Approved Quantity</td>
					   <td align="left" class="lable">:</td>
					   <td align="left" class="catagory"><span class="catagory">
		                   <bean:write  name="ReturnOrderItemDetails" property="returnQuantity" />
		                </span></td>
				   </tr>
			   </c:if>
			   <c:if test="${ReturnOrderItemDetails.rmaAuthorizeSignatory ne null and ReturnOrderItemDetails.rmaAuthorizeSignatory ne ''}">
                  <tr class="tdbg">
                    <td align="left" nowrap="nowrap" class="lable"> RMA Authorize Signatory </td>
                    <td align="left" class="lable"> : </td>
                    <td align="left" class="catagory"><bean:write name="ReturnOrderItemDetails" property="rmaAuthorizeSignatory" /></td>
                  </tr>
            	</c:if>
			   <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable">Return Status </td>
				   <td align="left" class="lable">:</td>
				   <td align="left" class="catagory"><span class="catagory">
				   	  <c:if test="${ReturnOrderItemDetails.rmaStatus eq 'WR'}"> Waiting for Admin to Reject </c:if>
	                  <c:if test="${ReturnOrderItemDetails.rmaStatus eq 'WA'}"> Waiting for Admin to Charge back </c:if>
	                  <c:if test="${ReturnOrderItemDetails.rmaStatus eq 'RG'}"> RMA Generated </c:if>
	                  <c:if test="${ReturnOrderItemDetails.rmaStatus eq 'RA'}"> RMA Approved </c:if>
	                  <c:if test="${ReturnOrderItemDetails.rmaStatus eq 'RR'}"> RMA Rejected </c:if>  
	               </span></td>
				</tr>
			      
			   <tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Reason for Return </td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
				   		<bean:write  name="ReturnOrderItemDetails" property="returnReason" />  
	               </span></td>
				</tr>
				<tr class="tdbg">
				   <td align="left" nowrap="nowrap" class="lable" valign="top">Vendor Comments </td>
				   <td align="left" class="lable" valign="top">:</td>
				   <td align="left" class="catagory" colspan="4" valign="top"><span class="catagory">
				   		<bean:write  name="ReturnOrderItemDetails" property="vendorComments" />  
	               </span></td>
				</tr>		
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 
			<html:button property="method" onclick="javascript:fnCallReturn();" styleClass="button" title="OK">OK</html:button> 
		 </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>
</div>
</td></tr>
</html:form>
