<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
function getFile(imagePath,jsField){
	if(imagePath=='')
		return true;
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
		return true;
	} else {
		alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
	}
}


function fnCallUploadProductImg(){
	if(document.forms[0].mainImgPath.value==""){
		alert("Please select Main Image.");
		document.forms[0].mainImgPath.focus();	
		return;
	}else{
		var mainImg = document.forms[0].mainImgPath.value;
		var view1Img = document.forms[0].view1ImgPath.value;
		var view2Img = document.forms[0].view2ImgPath.value;
		var view3Img = document.forms[0].view3ImgPath.value;
		if(getFile(mainImg, 'Main Image') && getFile(view1Img,'View1') && getFile(view2Img,'View2') && getFile(view3Img,'View3')){
			document.forms[0].action="addUploadVendorProdImg.do?method=addUploadVendorProductImage";
			document.forms[0].submit();
		}
	}
}

function fnCallSkip(){
	document.forms[0].action="skipUploadVendorProdImg.do?method=skipUploadVendorProductImage";
	document.forms[0].submit();
}

function fnCallInitAddVendorProduct() {
	document.forms[0].action="cancelAddVendorProduct.do?method=cancelAddProducts";
	document.forms[0].submit();
}

</script>
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
  	<td width="100%" background="../images/top_nav_middlebg.png" align="left">		
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Add Item</li>
		</ul>	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="vendor/addUploadVendorProdImg" method="post" enctype="multipart/form-data">
	<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Upload Image(s)</h2></td>
        </tr>
      <tr>
        <td align="left"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="broder_top0">
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">Main Image </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="mainImgPath" styleClass="browse" accept="image/gif,image/jpeg" tabindex="1" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> Caption </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="mainImgCap" styleClass="caption_input" tabindex="2" maxlength="63"/></td>
          </tr>
          <tr class="tdbg">
            <td colspan="3" align="center" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view1ImgPath" accept="image/gif,image/jpeg"  styleClass="browse" tabindex="3"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">  Caption1 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view1ImgCap" styleClass="caption_input" tabindex="4" maxlength="63"/></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View2</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view2ImgPath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="5"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">  Caption2 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view2ImgCap" styleClass="caption_input" tabindex="6" maxlength="63"/></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable"> View3</td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view3ImgPath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="7"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="right" nowrap="nowrap" class="lable">  Caption3 </td>
            <td align="left" nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view3ImgCap" styleClass="caption_input" tabindex="8" maxlength="63"/></td>
          </tr>
      </table>      </tr>
      <tr>
        <td height="50" colspan="3" align="center"><html:button property="method" onclick="javascript:fnCallUploadProductImg()"  styleClass="button" tabindex="9"> Upload </html:button>
          <html:button property="method" styleClass="button" onclick="javascript:fnCallSkip()" tabindex="10"> Skip </html:button>
          <html:button property="method" styleClass="button" onclick="javascript:fnCallInitAddVendorProduct();" tabindex="11"> Cancel </html:button></td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center" class="help"><table width="98%" border="0" cellpadding="2">
            <tr>
              <td valign="top"><strong>Note:</strong> </td>
              <td align="left">The image uploaded should have a minimum resolution of 175 dpi 
                and the minimum dimension of 970 x 906 pixels.</td>
            </tr>
        </table></td>
      </tr>
    </table>
	</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>

