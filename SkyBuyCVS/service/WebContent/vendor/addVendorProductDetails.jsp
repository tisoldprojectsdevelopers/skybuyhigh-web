<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>
<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}
function validatePrice(jsPrice) {
		var retVal=jsPrice;
		var startChar=retVal.substring(0,1);
		if(startChar=="$") { 
			retVal=retVal.substring(1,retVal.length);
		} 
		var startChar=retVal.substring(0,1);
		while(startChar=="0") { 
			retVal=retVal.substring(1,retVal.length);
			startChar=retVal.substring(0,1); 
		} 
		var startChar=retVal.substring(0,1);
		if(startChar==".") { 
			retVal="0"+retVal; 
		} 
		if(retVal>=0.01)
		  return false;
	    else
		    return true;       

}
function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }

function fnCallAddProduct(){
	/*var shortDescValue=tinyMCE.get('shtDesc').getContent();	
	shortDescValue=(stripTags(shortDescValue));
	
	var longDescValue=tinyMCE.get('longDesc').getContent();	
	longDescValue=(stripTags(longDescValue));  */  
	
	if(document.forms[0].cateId.value==""){
		alert("Please select Category");
		document.forms[0].cateId.focus();	
		return;
	}
	else if(document.forms[0].prodTitle.value==""){
		alert("Please enter Item Name");
		document.forms[0].prodTitle.focus();	
		return;
	}
	else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}
	else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}
	else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}	
	else if(document.forms[0].shortDesc.value==""){
		alert("Please enter Short Description");
		document.forms[0].shortDesc.focus();	
		return;
	}
	else if(document.forms[0].longDesc.value==""){
		alert("Please enter Long Description");
		document.forms[0].longDesc.focus();	
		return;
	}else{
	
	if(document.forms[0].vendPrice.value.charAt(0)=="$")
		document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
		
	/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/ 
	
	/*document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
	
	document.forms[0].shortDescription.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
	document.forms[0].shortDescription.value=document.forms[0].shortDescription.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');
	
	document.forms[0].fullDescription.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
	document.forms[0].fullDescription.value=document.forms[0].fullDescription.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');
	
	document.forms[0].action="addVendorProduct.do?method=addVendorProductDetails";
	document.forms[0].submit();
	}
}
function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}
function fnCallHome() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();
}
</script>
<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Add Item </li>
		</ul>
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="vendor/addVendorProduct" method="post" enctype="multipart/form-data">
	<logic:present name="errorMsg">
	<table width="500">
		<tr>
	    	<td colspan="3" class="error" align="center"><bean:message key="itemCodeErrorMsg"></bean:message></td>
		</tr>
	</table>
  </logic:present>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Item  Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="4" bgcolor="#FFFFFF" class="broder_top0">
          <tr class="tdbg">
            <td align="left" class="lable">Vendor Name</td>
            <td width="4%" class="lable">:</td>
            <td align="left"><span class="catagory">
            <logic:present name="vendorLoginInfo" scope="session">
			<bean:write  name="vendorLoginInfo" property="userName" />	
			<input type="hidden" name="ownerId" value="<c:out value='${vendorLoginInfo.refId}'/>" />		
			</logic:present>		
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Category *<br></span></td>
            <td width="5%" class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="cateId" styleClass="textarea2" tabindex="1">
                <html:option value="">--Select Category--</html:option>
                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Item Name *</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="prodTitle" styleClass="input" tabindex="2" maxlength="30" />
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Brand Name *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="brandName" styleClass="input" tabindex="3" maxlength="63"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code *<br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="prodCode" styleClass="input" tabindex="4" maxlength="15"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="inShopStatus" styleClass="textarea2" tabindex="5">
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Color<br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="color" styleClass="input" tabindex="4" maxlength="250"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Size<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="size" styleClass="input" tabindex="4" maxlength="250"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" nowrap="nowrap">Retail Price (in US$) *<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);" tabindex="6"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Pre-Authorize Credit Card</td>
          	<td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:checkbox  property="preAuthCC" styleClass="checkboxHTML" value="Y"  tabindex="7"/> Yes
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Title *<br></span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span><html:textarea property="shortDesc" styleClass="textarea" rows="5" onkeyup="textLimit(this.form.shortDesc,this.form.shortDesclen,250,'Short Description');"  styleId="shtDesc" style="width:520px;" tabindex="8" ></html:textarea>
		 <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="shortDesclen" size="3" maxlength="3" value="250" class="wordcount"/></td>
            <input type="hidden" name="shortDescription"/>
          </tr>
          <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Full Description *<br> </span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><html:textarea property="longDesc" styleClass="textarea"  rows="5" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Long Description');"  styleId="longDesc" style="width:520px;" tabindex="9" ></html:textarea>
		 <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="longDesclen" size="3" maxlength="3" value="500" class="wordcount"/>
			<input type="hidden" name="fullDescription" />            
            </td>
          </tr>
         <!-- <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext"><strong>Comments</strong></span></td>
            <td valign="top" class="lable"><strong>:</strong></td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.</span>
                <textarea name="sbhComment" rows="5"  tabindex="14" style="width:520px;" onkeyup="textLimit(this.form.sbhComment,this.form.commentlen,250,'Comments');"></textarea>
                Remaining characters
             
                <input readonly="readonly" type="text" name="commentlen" size="3" maxlength="3" value="250" class="wordcount"/>
              </td>
          </tr>-->
          
      </table>
      </tr>
	  
      <tr>
        <td height="50" colspan="3" align="center"><html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallAddProduct()" tabindex="10"> Add </html:button>
          <html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallHome();" tabindex="11"> Cancel </html:button></td>
      </tr>
    </table>
	</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>