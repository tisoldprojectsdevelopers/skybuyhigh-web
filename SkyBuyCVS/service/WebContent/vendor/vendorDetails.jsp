<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
function getAboutMeFile(filePath,jsField){
	if(filePath!='') {	
		var pathLength = filePath.length;
		var lastDot = filePath.lastIndexOf(".");
		var fileType = filePath.substring(lastDot,pathLength);
		if((fileType == ".swf") || (fileType == ".SWF")) {
			return true;
		} else {
			alert("We supports .SWF file format. "+jsField+" file-type is " + fileType );
			return false;
		}
	}else{
		return true;
	}
}
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}


function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;   
}	
function getValidTelephone(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp.length != 10){		
		return "";
	}
	if (phTemp=="0000000000"){		
		return "";
	}
	return phTemp;
}
function validateContactEmail(str,addStr){
	isValid=true;
	var regExp=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	if (!regExp.test(str)){
		isValid=false
		alert(addStr+" Email is an invalid e-mail address.");	
	}
	return (isValid)
}





function getValidZip(p_value){
	var phTemp="";
	var validchars = "0123456789";
	if (p_value == null)
		return "";
		
	var parm1 =trim(p_value);	
	for (var i=0; i<parm1.length; i++) {
		temp1 = "" + parm1.substring(i, i+1);		
			if(IsNumeric(temp1)){			
				phTemp=phTemp+temp1;				
			}		
	}
	if (phTemp=="00000"){		
		return "";
	}
	return phTemp;
}	
function fnCallAddVendor(){
	var phoneNo;
	phoneNo = getValidTelephone(document.forms[0].vendPhone1.value)
	if(phoneNo==""){
	alert("Phone Number is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].vendPhone1.focus();
		return false;
	} else {	
		document.forms[0].vendPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].vendPhoneExt1.value ;	
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension should be valid.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		}
		if(trim(phoneNoExt1).length>6){
			alert("Extension should not be more than 6 digits.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		} 
	}
	var Mobile1;
	if(document.forms[0].vendMobile1.value!=""){
		Mobile1 = getValidTelephone(document.forms[0].vendMobile1.value)
		if(Mobile1==""){
		alert("Mobile Number should be valid.");
			document.forms[0].vendMobile1.focus();
			return false;
		} else {	
			document.forms[0].vendMobile1.value = Mobile1;
		}	
	}
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].vendFax.value)	
	if (document.forms[0].vendFax.value != null && document.forms[0].vendFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendFax.focus();
			return false;
		} else {	
			document.forms[0].vendFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].vendZip.value)	
	if (document.forms[0].vendZip.value != null && document.forms[0].vendZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].vendZip.focus();
			return false;
		} else {	
			document.forms[0].vendZip.value = zipNo;
		}
	}	
	
	var phoneNo2;
	if((document.forms[0].vendPhone2.value)!=""){
		phoneNo2 = getValidTelephone(document.forms[0].vendPhone2.value)
		if(phoneNo2==""){
		alert("Second Contact Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendPhone2.focus();
			return false;
		} else {	
			document.forms[0].vendPhone2.value = (phoneNo2);
		}
	}
	
	var phoneNoExt2=document.forms[0].vendPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Second Contact Ext should be valid.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		}
		if(trim(phoneNoExt2).length>6){
			alert("Second Contact Ext should not be more than 6 digits.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		} 
	}
	var Mobile2;
	if(document.forms[0].vendMobile2.value!=""){
		Mobile2 = getValidTelephone(document.forms[0].vendMobile2.value)
		if(Mobile2==""){
		alert("Second Contact Mobile Number should be valid.");
			document.forms[0].vendMobile2.focus();
			return false;
		} else {	
			document.forms[0].vendMobile2.value = Mobile2;
		}	
	}
	var custPhoneNo;
	custPhoneNo = getValidTelephone(document.forms[0].custServicePhoneno.value)
	if(custPhoneNo==""){
	alert("Customer Service Phone Number is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].custServicePhoneno.focus();
		return false;
	} else {	
		document.forms[0].custServicePhoneno.value = custPhoneNo;
	}
	
	var custServicePhonenoExt=document.forms[0].custServicePhonenoExt.value ;
	if(trim(custServicePhonenoExt)!=""){	
		if(!IsNumeric(custServicePhonenoExt)){
			alert("Customer Service Ext should be valid.");
			document.forms[0].custServicePhonenoExt.focus();
			return false;
		}
		if(trim(custServicePhonenoExt).length>6){
			alert("Customer Service Ext should not be more than 6 digits.");
			document.forms[0].custServicePhonenoExt.focus();
			return false;
		} 
	}
	
	
	
	var sEmail1;
	sEmail1=validateContactEmail(document.forms[0].vendEmail.value); 
	var sEmail2;
	if(document.forms[0].vendEmail2.value!="")
		sEmail2=validateContactEmail(document.forms[0].vendEmail2.value); 
	var sCustEmail;
	sCustEmail=validateContactEmail(document.forms[0].custServiceEmail.value); 
	
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- starts**/
	if(trim(document.forms[0].vendEmail2.value)!=""){
		if(trim(document.forms[0].vendContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].vendContactName2.focus();
			return false;
		}
	}
	if(trim(document.forms[0].vendPhone2.value)!=""){
		if(trim(document.forms[0].vendContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].vendContactName2.focus();
			return false;
		}
	}	
	if(trim(document.forms[0].vendPhoneExt2.value)!=""){
		if(trim(document.forms[0].vendPhone2.value)!=""){
			if(trim(document.forms[0].vendContactName2.value)==""){
				alert("Enter Second Person Contact Name.");
				document.forms[0].vendContactName2.focus();
				return false;
			}
		}else {
			alert("Enter Second Person Phone.");
			document.forms[0].vendPhone2.focus();
			return false;
		}	
	}
	if(trim(document.forms[0].vendMobile2.value)!=""){
		if(trim(document.forms[0].vendContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].vendContactName2.focus();
			return false;
		}
	}
	
	var aboutMePath = trim(document.forms[0].aboutMe.value);
	if(!getAboutMeFile(aboutMePath, 'About Me')) {
		return false;
	}
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- ends**/
	
	if((sEmail1 == true && sEmail2 == true && sCustEmail == true) || (sEmail1 == true && sCustEmail == true && document.forms[0].vendEmail2.value==""))
		{
		document.forms[0].action="addVendor.do?method=addVendorDetails";
		document.forms[0].submit();
		}
	
}
function IsValidUserIdOrPassword(sText){
   var ValidChars = " ";
   var IsValid=true;
   var Char;
   for (i = 0; i < sText.length && IsValid == true; i++){ 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) != -1) 
         {
         	IsValid = false;
         }
      }
   return IsValid;   
}
function fnCallEditVendor(){

var userId = document.forms[0].vendUserId.value;
	if(userId.length<3){	
		alert("Please enter Vendor User Id more than 3 characters");
		document.forms[0].vendUserId.focus();
		return false;
	}
	
	if(!IsValidUserIdOrPassword(userId)){	
		alert("Please enter valid Vendor User Id");
		document.forms[0].vendUserId.focus();
		return false;
	}
	
	var password = document.forms[0].vendPassword.value;
	if(password.length<3){	
		alert("Please enter Vendor Password more than 3 characters");
		document.forms[0].vendPassword.focus();
		return false;
	}
	
	if(!IsValidUserIdOrPassword(password)){	
		alert("Please enter valid Vendor Password");
		document.forms[0].vendPassword.focus();
		return false;
	}
	
	var phoneNo;
	phoneNo = getValidTelephone(document.forms[0].vendPhone1.value)
	if(phoneNo==""){
	alert("Phone Number is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].vendPhone1.focus();
		return false;
	} else {	
		document.forms[0].vendPhone1.value = phoneNo;
	}
	
	var phoneNoExt1=document.forms[0].vendPhoneExt1.value ;
	if(trim(phoneNoExt1)!=""){	
		if(!IsNumeric(phoneNoExt1)){
			alert("Extension should be valid.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		}
		if(trim(phoneNoExt1).length>6){
			alert("Extension should not be more than 6 digits.");
			document.forms[0].vendPhoneExt1.focus();
			return false;
		} 
	}
	var Mobile1;
	if(document.forms[0].vendMobile1.value!=""){
		Mobile1 = getValidTelephone(document.forms[0].vendMobile1.value)
		if(Mobile1==""){
		alert("Mobile Number should be valid.");
			document.forms[0].vendMobile1.focus();
			return false;
		} else {	
			document.forms[0].vendMobile1.value = Mobile1;
		}	
	}
	
	var faxNo;
	faxNo = getValidTelephone(document.forms[0].vendFax.value)	
	if (document.forms[0].vendFax.value != null && document.forms[0].vendFax.value  != "") {
		if(faxNo==""){
			alert("Please enter valid Fax Number.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendFax.focus();
			return false;
		} else {	
			document.forms[0].vendFax.value = faxNo;
		}
	}
	var zipNo;
	zipNo = getValidZip(document.forms[0].vendZip.value)	
	if (document.forms[0].vendZip.value != null && document.forms[0].vendZip.value  != "") {
		if(zipNo==""){
			alert("Zip must follow valid 5 digit code");
			document.forms[0].vendZip.focus();
			return false;
		} else {	
			document.forms[0].vendZip.value = zipNo;
		}
	}	
	
	var phoneNo2;
	if(document.forms[0].vendPhone2.value!=""){
		phoneNo2 = getValidTelephone(document.forms[0].vendPhone2.value)
		if(phoneNo2==""){
		alert("Second Contact Phone Number should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
			document.forms[0].vendPhone2.focus();
			return false;
		} else {	
			document.forms[0].vendPhone2.value = phoneNo2;
		}
	}
	
	var phoneNoExt2=document.forms[0].vendPhoneExt2.value ;
	if(trim(phoneNoExt2)!=""){	
		if(!IsNumeric(phoneNoExt2)){
			alert("Second Contact Ext should be valid.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		}
		if(trim(phoneNoExt2).length>6){
			alert("Second Contact Ext should not be more than 6 digits.");
			document.forms[0].vendPhoneExt2.focus();
			return false;
		} 
	}
	
	var Mobile2;
	if(document.forms[0].vendMobile2.value!=""){
		Mobile2 = getValidTelephone(document.forms[0].vendMobile2.value)
		if(Mobile2==""){
		alert("Second Contact Mobile Number should be valid.");
			document.forms[0].vendMobile2.focus();
			return false;
		} else {	
			document.forms[0].vendMobile2.value = Mobile2;
		}	
	}
	
	var custPhoneNo;
	custPhoneNo = getValidTelephone(document.forms[0].custServicePhoneno.value)
	if(custPhoneNo==""){
	alert("Customer Service Phone Number is required and should be valid.\nValid Formats: xxxxxxxxxx and xxx-xxx-xxxx and (xxx)xxx-xxxx");
		document.forms[0].custServicePhoneno.focus();
		return false;
	} else {	
		document.forms[0].custServicePhoneno.value = custPhoneNo;
	}
	
	var custServicePhonenoExt=document.forms[0].custServicePhonenoExt.value ;
	if(trim(custServicePhonenoExt)!=""){	
		if(!IsNumeric(custServicePhonenoExt)){
			alert("Customer Service Ext should be valid.");
			document.forms[0].custServicePhonenoExt.focus();
			return false;
		}
		if(trim(custServicePhonenoExt).length>6){
			alert("Customer Service Ext should not be more than 6 digits.");
			document.forms[0].custServicePhonenoExt.focus();
			return false;
		} 
	}
	
	
	
	var sEmail1;
	sEmail1=validateContactEmail(document.forms[0].vendEmail.value,"");
	if(!sEmail1) {
		document.forms[0].vendEmail.focus();	
		return false;
	} 
	var sEmail2;
	if(document.forms[0].vendEmail2.value!="") {
		sEmail2=validateContactEmail(document.forms[0].vendEmail2.value, "Second Person");
		if(!sEmail2) {
			document.forms[0].vendEmail2.focus();
			return false;
		}  
	}
	var sCustEmail;
	sCustEmail=validateContactEmail(document.forms[0].custServiceEmail.value, "Customer Service"); 
	if(!sCustEmail) {
		document.forms[0].custServiceEmail.focus();
		return false;
	} 
	
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- starts**/
	if(trim(document.forms[0].vendEmail2.value)!=""){
		if(trim(document.forms[0].vendContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].vendContactName2.focus();
			return false;
		}
	}
	if(trim(document.forms[0].vendPhone2.value)!=""){
			if(trim(document.forms[0].vendContactName2.value)==""){
				alert("Enter Second Person Contact Name.");
				document.forms[0].vendContactName2.focus();
				return false;
			}
	}		
	if(trim(document.forms[0].vendPhoneExt2.value)!=""){
		if(trim(document.forms[0].vendPhone2.value)!=""){
			if(trim(document.forms[0].vendContactName2.value)==""){
				alert("Enter Second Person Contact Name.");
				document.forms[0].vendContactName2.focus();
				return false;
			}
		}else {
			alert("Enter Second Person Phone.");
			document.forms[0].vendPhone2.focus();
			return false;
		}	
	}
	if(trim(document.forms[0].vendMobile2.value)!=""){
		if(trim(document.forms[0].vendContactName2.value)==""){
			alert("Enter Second Person Contact Name.");
			document.forms[0].vendContactName2.focus();
			return false;
		}
	}
	
	var aboutMePath = trim(document.forms[0].aboutMe.value);
	if(!getAboutMeFile(aboutMePath, 'About Me')) {
		return false;
	}
	/**alert  second person name is reqired when  second person mail or mobile or phone ext is entered--- ends**/
	
	
	if((sEmail1 == true && sEmail2 == true && sCustEmail == true) || (sEmail1 == true && sCustEmail == true && document.forms[0].vendEmail2.value==""))
		{
		document.forms[0].action="updateVendor.do?method=UpdateVendorDetails";
		document.forms[0].submit();
 		
		}	
	
}

function textLimit(field, countfield,maxlen,dispName)
 {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (fieldvallength > maxlen){
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
}
function fnCallHome() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();
}

function fnCopyMainToCustomerService(){
	document.forms[0].custServicePhoneno.value = document.forms[0].vendPhone1.value;
	document.forms[0].custServicePhonenoExt.value = document.forms[0].vendPhoneExt1.value;
	document.forms[0].custServiceEmail.value = document.forms[0].vendEmail.value;
}
function fnClearCustomerService(){
	document.forms[0].custServicePhoneno.value = document.forms[0].custServicePhonenohidden.value;
	document.forms[0].custServicePhonenoExt.value = document.forms[0].custServicePhonenoexthidden.value;
	document.forms[0].custServiceEmail.value = document.forms[0].custServiceEmailhidden.value;
}

function fnCopyMainToReturnAddress(){
	document.forms[0].returnAddr1.value = document.forms[0].vendAddr1.value;
	document.forms[0].returnAddr2.value = document.forms[0].vendAddr2.value;
	document.forms[0].returnCity.value = document.forms[0].vendCity.value;
	document.forms[0].returnState.value = document.forms[0].vendState.value;
	document.forms[0].returnZip.value = document.forms[0].vendZip.value;
}

function fnClearReturnAddress(){
	if(document.forms[0].returnStatehidden.value == '') {
		document.forms[0].returnState.value = "";
	} else {
		document.forms[0].returnState.value = document.forms[0].returnStatehidden.value;
	}
	document.forms[0].returnAddr1.value = document.forms[0].returnAddr1hidden.value;
	document.forms[0].returnAddr2.value = document.forms[0].returnAddr2hidden.value;
	document.forms[0].returnCity.value = document.forms[0].returnCityhidden.value;
	document.forms[0].returnZip.value = document.forms[0].returnZiphidden.value;
}
function fnCallServiceSameAsMain(){
	
	if(document.forms[0].customerService.checked){
		fnCopyMainToCustomerService();
	}

}

function fnCallServiceSameAsMainOnClick(){
	
	if(document.forms[0].customerService.checked){
		fnCopyMainToCustomerService();
	}else{
		fnClearCustomerService();
	}

}

function fnCallReturnSameAsMain(){
	
	if(document.forms[0].returnDetails.checked){
		fnCopyMainToReturnAddress();
	}

}

function  fnCallReturnSameAsMainOnClick(){
	
	if(document.forms[0].returnDetails.checked){
		fnCopyMainToReturnAddress();
	}else{
		fnClearReturnAddress();
	}

}
function getCatalogue(link, windowname){
		window.open(link, windowname, 'width=1024,height=600,scrollbars=Yes,resizable=Yes');
}
</script>
<html:javascript formName="vendorDetailsForm"/>
<html:form action="vendor/addVendor" method="post" onsubmit="return validateVendorDetailsForm(this);"  enctype="multipart/form-data">

<tr><td>
<div class="contentcontainer">
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="../images/top_nav_middlebg.png">
	
	
	<logic:present name="vendorLoginInfo" scope="session">	
		<c:if test="${vendorLoginInfo.userType eq 'vendor' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from:</li>
		<li>Vendor</li>
		<li>></li>
		<li>Edit Vendor</li>
		</ul>
		</c:if>
	</logic:present>
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	
	<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center"><h2>Vendor Setup</h2></td>
        </tr>
      <tr>
        <td align="center"><table width="100%" border="0" cellpadding="6" cellspacing="0" >
	<c:if test="${!empty vendorNamevendorUserIdExist}">  
	<tr class="tdbg"   nowrap="nowrap">
		<td colspan="6" align="center" ><span class="error"><bean:message key="vendor.NameUserId"/>
		  </span></td>
	</tr>
	</c:if>
	<c:if test="${!empty vendNameExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error"> <bean:message key="vendor.Name"/>
		  </span></td>
	</tr>
	</c:if>
	<c:if test="${!empty vendorUserIdExist}">  
	<tr class="tdbg"  nowrap="nowrap">
		<td colspan="6" align="center"><span class="error"> <bean:message key="vendor.UserId"/>
		  </span></td>
	</tr>
	</c:if>
   
      
      <tr class="tdbg">
        <td align="left" class="lable">Vendor Name *</td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendName" styleClass="input" tabindex="1" maxlength="63"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendContactName" styleClass="input" tabindex="2" maxlength="63"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">Status </span></td>
        <td class="lable">:</td>
        <td align="left"><logic:equal name="vendorDetails" property="vendStatus" value="I"> Inactive </logic:equal>
            <logic:equal name="vendorDetails" property="vendStatus" value="A"> Active </logic:equal>
            <html:hidden property="vendStatus" />        </td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Charge Type  * </span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="chargeType" tabindex="4" styleClass="select">
            <html:option value="FA">Automated</html:option>
            <html:option value="SA">Semi Automated</html:option>
          </html:select>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor User Id *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
        <bean:write  name="vendorDetails" property="vendUserId"/>
        <html:hidden property="vendUserId"/>
          
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Vendor Password *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:password property="vendPassword" styleClass="input" tabindex="5"  maxlength="63"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Company Address*</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendAddr1" styleClass="input" tabindex="3" maxlength="127" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
        <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Additional Address</span></td>
        <td class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <html:text property="vendAddr2" styleClass="input" tabindex="4" maxlength="127" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
      </tr>
	   <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">City *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendCity" styleClass="input" tabindex="5" maxlength="63" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
        <td align="left" class="lable"><span class="boldtext">State *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
		  <html:select property="vendState" styleClass="input" styleId="state" tabindex="6" onchange="fnCallReturnSameAsMain();">
			 <html:option value="">------Select State------</html:option>
			 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
            </html:select>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td class="lable">:</td>
        <td align="left">
         
		  <html:select property="vendCountry" tabindex="7" styleClass="select">
			 <html:option value="US">US</html:option>
		  </html:select>       </td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendZip" styleClass="input" tabindex="8" maxlength="9" onchange="fnCallReturnSameAsMain();"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable">Phone *</td>
        <td class="lable">:</td>
        <td align="left">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="vendPhone1" maxlength="10" styleClass="input-phone" tabindex="10" onchange="fnCallServiceSameAsMain();"/>
        </span></td>
              <td class="lable">Ext :</td>
              <td><span class="catagory">
          <html:text property="vendPhoneExt1"  maxlength="6" styleClass="input-ext" tabindex="6" onchange="fnCallServiceSameAsMain();"/>
        </span></td>
            </tr>
          </table></td>
        <td align="left" class="lable">Mobile </td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
                <html:text property="vendMobile1" styleClass="input-phone" tabindex="11" maxlength="10"/>
              </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">Fax </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendFax" styleClass="input" tabindex="12" maxlength="10"/>
        </span></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Email *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendEmail" styleClass="input" tabindex="13" maxlength="63"  onchange="fnCallServiceSameAsMain();"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" bgcolor="#EFEFEF" class="lable">Upload Vendor Info<br> </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
       <tr class="tdbg">
      
	        <td align="left" nowrap="nowrap" class="lable" valign="top"><div style="margin:6px 0 0 0">About Me&nbsp;</div></td>
	        <td align="left" class="lable" valign="top"><div style="margin:6px 0 0 0">:</div></td>
	        <td align="left" colspan="5" valign="top"><span class="catagory">
	          <html:file property="aboutMe"  styleClass="browse" tabindex="19" />
	          <c:if test="${!empty vendorDetails.aboutMePath and vendorDetails.aboutMePath ne ''}">
		          <br/><a class="clickhere" href="javascript:getCatalogue('../previewAboutMe.jsp','airlineproduct');">Click here</a> to Preview this item in SkyBuy<sup>High</sup> Catalogue
	          </c:if>
	          <BR/>(Upload only file with and extension .swf) 
	        </span></td>
	        
	      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
        </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" bgcolor="#EFEFEF" class="lable">Second Person Contact Details </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr valign="middle" class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendContactName2" styleClass="input" tabindex="14" maxlength="63"/>
        </span></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Email </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendEmail2" styleClass="input" tabindex="15" maxlength="63"/>
        </span></td>
      </tr>
      <tr valign="middle" class="tdbg">
        <td align="left" nowrap="nowrap" class="lable">Phone </td>
        <td class="lable">:</td>
        <td align="left"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
                <html:text property="vendPhone2" styleClass="input-phone" tabindex="16" maxlength="10"/>
              </span></td>
              <td class="lable">Ext :</td>
              <td><span class="catagory">
                <html:text property="vendPhoneExt2" styleClass="input-ext" tabindex="17" maxlength="6"/>
              </span></td>
            </tr>
          </table></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Mobile </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="vendMobile2" styleClass="input" tabindex="18" maxlength="10"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
        <td height="23" colspan="6" align="left" nowrap="nowrap" bgcolor="#EFEFEF" class="lable">Customer Service &nbsp;
        	<input type="checkbox" class="checkboxHTML" name="customerService" onclick="fnCallServiceSameAsMainOnClick();"/> Same as main information 
        </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
        <td align="left" valign="middle" class="lable">Phone *</td>
        <td valign="middle" class="lable">:</td>
        <td align="left" valign="middle">
          <table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="catagory">
          <html:text property="custServicePhoneno" styleClass="input-phone" tabindex="19" maxlength="10"/>
          <html:hidden property="custServicePhonenohidden" value="${vendorDetailsForm.custServicePhoneno}"/>
        </span></td>
              <td class="lable">Ext :</td>
              <td><span class="catagory">
          <html:text property="custServicePhonenoExt" styleClass="input-ext" tabindex="20" maxlength="6"/>
          <html:hidden property="custServicePhonenoexthidden" value="${vendorDetailsForm.custServicePhonenoExt}"/>
        </span></td>
      </tr> 
          </table></td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Email*</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="custServiceEmail" styleClass="input" tabindex="21" maxlength="63"/>
          <html:hidden property="custServiceEmailhidden" value="${vendorDetailsForm.custServiceEmail}"/>
        </span></td>
		
		 <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
		<tr class="tdbg">
        <td height="23" colspan="6" align="left" bgcolor="#EFEFEF" class="lable">Return Details &nbsp;
        	<input type="checkbox" class="checkboxHTML" name="returnDetails" onclick="fnCallReturnSameAsMainOnClick();"/> Same as main information
        </td>
        </tr>
      <tr class="tdbg">
        <td colspan="6" align="left" bgcolor="#CCCCCC" class="lable" height="1"></td>
      </tr>
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Company Address * </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnAddr1" styleClass="input" tabindex="22" maxlength="127"/>
          <html:hidden property="returnAddr1hidden" value="${vendorDetailsForm.returnAddr1}"/>
        </span></td>
        <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Additional Address</span></td>
        <td class="lable">:</td>
        <td align="left" nowrap="nowrap"><span class="catagory">
          <html:text property="returnAddr2" styleClass="input" tabindex="23" maxlength="127"/>
          <html:hidden property="returnAddr2hidden" value="${vendorDetailsForm.returnAddr2}"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left" class="lable"><span class="boldtext">City *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnCity" styleClass="input" tabindex="24" maxlength="63"/>
          <html:hidden property="returnCityhidden" value="${vendorDetailsForm.returnCity}"/>
        </span></td>
        <td align="left" class="lable"><span class="boldtext">State *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:select property="returnState" styleClass="input" styleId="state" tabindex="25">
            <html:option value="">------Select State------</html:option>
            <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
          </html:select>
          <html:hidden property="returnStatehidden" value="${vendorDetailsForm.returnState}"/>
        </span></td>
      </tr>
      <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Country *</span></td>
        <td class="lable">:</td>
        <td align="left"><html:select property="returnCountry" tabindex="26" styleClass="select">
            <html:option value="US">US</html:option>
          </html:select>        </td>
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Zip *</span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnZip" styleClass="input" tabindex="27" maxlength="9"/>
          <html:hidden property="returnZiphidden" value="${vendorDetailsForm.returnZip}"/>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left"  nowrap="nowrap" class="lable"><span class="boldtext">Return Days * </span></td>
        <td class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="returnDays" styleClass="input" tabindex="28" maxlength="3"/>
        </span></td>
        <td align="left"  nowrap="nowrap" class="lable">&nbsp;</td>
        <td class="lable">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
		
		
        <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Return Policy * </span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span>
         <textarea name="vendReturnPolicy" class="textarea" cols="66" rows="6" tabindex="29" onkeyup="textLimit(this,this.form.policylen,1000,'Return Policy');"><logic:present name="vendorDetails"><logic:notEmpty name="vendorDetails"><bean:write  name="vendorDetails" property="vendReturnPolicy"/></logic:notEmpty></logic:present></textarea>
        
          
          <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="policylen" size="3" maxlength="3" value="1000" class="wordcount"/>        </td>
      </tr>
	  <tr class="tdbg">
        <td height="122" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span>
        <textarea name="vendComments" class="textarea" cols="66" rows="6" tabindex="30" onkeyup="textLimit(this,this.form.commentlen,250,'Comments');"></textarea>
        
          <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="commentlen" size="3" maxlength="3" value="250" class="wordcount"/>        </td>
      </tr>
      
    </table>      </tr>
     
      <tr>
         <td height="30" colspan="3" align="center">
		<logic:present name="mode">
			 <logic:equal name="mode"  value="Edit">
	  <html:button property="method" styleClass="button" tabindex="31"
		  onclick="javascript:if(document.forms[0].onsubmit())fnCallEditVendor(); void 0">Save </html:button> 
		      <html:button property="method" onclick="javascript:fnCallHome();" styleClass="button" tabindex="32">Cancel</html:button>
		   </logic:equal>
		</logic:present>   
		
        </td>
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>


<html:hidden property="vendId"/>
<logic:present name="mode">
	
	<c:if test="${mode eq 'Edit'}">
		
		<html:hidden property="vendType"/>
		<html:hidden property="poPct"/>
	</c:if>

</logic:present> 
</td></tr>
</html:form>
</div></div>