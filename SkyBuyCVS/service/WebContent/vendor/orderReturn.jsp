<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	
	function fnHome(){
		document.forms[0].action="preEntry.do?method=preEntry";
		document.forms[0].submit();		
	}
	
	function triggerEvent() {
		if(event.keyCode==13) {
			fnCallSearch();
		}           
	}
	
	function isEligibleForSubmit() {
		var jsRMANo = trim(document.forms[0].rmaNumber.value);
		var jsComments = trim(document.forms[0].returnComments.value);
		var jsReturnQuantity = trim(document.forms[0].returnQuantity.value);
		var jsAuthoriseSignatory = trim(document.forms[0].authoriseSignatory.value);
		
		document.forms[0].rmaNumber.value = jsRMANo;
		document.forms[0].returnComments.value = jsComments;
		var RMAFormat = new RegExp("^[A-Za-z0-9\-]*[A-Za-z0-9]$");
		var returnQuantityFormat = new RegExp("^[0-9,]*$");
		
		if(jsRMANo == '') {
			alert("RMA Number is required.");
			return false;
		}else{
			if(!RMAFormat.test(jsRMANo)) {
				alert("Enter valid RMA Number.");
				return false;
			}
		}
		if(jsReturnQuantity == '') {
			alert("Return Quantity is required.");
			return false;
		}else {
			if(!returnQuantityFormat.test(jsReturnQuantity)) {
				alert("Enter valid return quantity.");
				return false;
			}
		}
		if(jsComments == '') {
			alert("Vendor comments is required.");
			return false;
		}
		if(jsAuthoriseSignatory == '') {
			alert("RMA Authorise signatory name is required");
			return false;
		}
		return true;
	}
	
	function fnSubmitRMANumber() {
		if(isEligibleForSubmit()) {
			document.forms[0].action="return.do?method=updateRMAStatus";
			document.forms[0].submit();
		}
	}
	
	function textLimit(field, countfield,maxlen,dispName) {		
 		var fieldval=field.value;
 		var fieldvallength=fieldval.length;
		if (fieldvallength > maxlen + 1) {
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		} 
		if (fieldvallength > maxlen) {
		   field.value= fieldval.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - fieldval.length;
	}
	
</script>
<html:form action="vendor/initOrderReturn" method="post">
	<tr>
		<td>
			<div class="contentcontainer">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
			  		<tr>
					    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
					    <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
							<ul>
								<li>You navigated from :</li>
								<li>Order</li>
								<li>></li>
								<li>Accept/Reject Return</li>
							</ul>
						</td>
					    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
					</tr>
					<tr>
						<td colspan="3" class="td">
							<table border="0" cellpadding="0" cellspacing="0" style="margin:15px;" align="center">
								<tr class="tdbg">
									<td align="center">
										<logic:present name="updated">
					      					<logic:notEmpty name="updated">
					      						<div align="center" style="margin:10px;">
						      						<font color="#FF0000"><c:out value="${updated}"></c:out></font>
						      					</div>
					      					</logic:notEmpty>
					      				</logic:present>
										<table border="0" align="center" cellpadding="0" cellspacing="0" class="border">
										      <tr>
										        <td class="tablehead" height="25"><h2> Order Return </h2></td>
										      </tr>
										      <tr class="tdbg">
										      		<td>
										      			<table>
										      				<tr>
										      					<td class="lable" nowrap="nowrap" align="left">RMA Number</td>
										      					<td class="lable" align="left"> : </td>
										      					<td class="lable" align="left"> <html:text property="rmaNumber" styleClass="input" maxlength="64" tabindex="1"/></td>
										      					<td class="lable" nowrap="nowrap" align="left">No of items approved</td>
										      					<td class="lable" align="left">:</td>
										      					<td class="lable" align="right" align="left">
										      						<html:text maxlength="4" styleClass="input" property="returnQuantity" tabindex="2"/>
										      					</td>
										      				</tr>
										      				<tr>
										      					<td class="lable" valign="top" align="left"> Comments </td>
										      					<td class="lable" valign="top" align="left"> : </td>
										      					<td class="lable" colspan="4" align="left"> 
										      						<html:textarea rows="6"  styleClass="textarea1" property="returnComments" onkeyup="textLimit(this,this.form.policylen,1000,'Reason for return');" tabindex="3"/>
														            <div align="left" style="margin:10px 0 10px 0;"><span class="normaltext">Remaining characters</span>
														            <input readonly="readonly" type="text" name="policylen" size="3" maxlength="3" value="1000" class="wordcount"/></div>
										      					</td>
										      				</tr>
										      				<tr>
										      					<td class="lable" valign="top" nowrap="nowrap" align="left"> RMA authorise <br/>signatory name</td>
										      					<td class="lable" valign="top" align="left"> : </td>
										      					<td class="lable" colspan="4" align="left"> 
										      						<html:text property="authoriseSignatory" maxlength="128" />
										      					</td>
										      				</tr>
										      				<tr>
										      					<td class="lable" colspan=6>
										      						<input type="button" value="Process" class="bigbutton" onclick="fnSubmitRMANumber();" tabindex="4"/>
										      					</td>
										      				</tr>
										      			</table>
										      		</td>
										      </tr>
										  </table>
										</td>
									</tr>
							  </table>
						</td>
					</tr>
					<tr>
					  <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
					  <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
					  <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
					</tr>
				</table>
			</div>
		</div>
	</td></tr>
</html:form>
