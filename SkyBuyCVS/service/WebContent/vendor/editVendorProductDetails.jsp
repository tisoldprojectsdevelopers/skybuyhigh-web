<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<link href="../style/registration.css" rel="stylesheet" type="text/css" />

<script>

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	 if(e.ctrlKey && e.keyCode == '86') // CTRL-V
     {
       	window.clipboardData.clearData();
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

function parseCurrency(field)
{
	var currency = /^\d{0,8}(?:\.\d{0,2})?$/;
	var testDollar=(field.value).charAt(0);
	var testData=(field.value).substring(1,(field.value).length);
	var onlyCurrency = /^(\d{0,8}(?:\.\d{0,2})?)[\s\S]*$/;
	if( testDollar!="$"){
		if(!currency.test(field.value) )
			 field.value = field.value.replace(onlyCurrency, "$1");
	}else{
	  if(!currency.test(testData) )
			field.value = field.value.replace(onlyCurrency, "$1");
      }
 }
function validatePrice(jsPrice) {
			var retVal=jsPrice;
			var startChar=retVal.substring(0,1);
			if(startChar=="$") { 
				retVal=retVal.substring(1,retVal.length);
			} 
			var startChar=retVal.substring(0,1);
			while(startChar=="0") { 
				retVal=retVal.substring(1,retVal.length);
				startChar=retVal.substring(0,1); 
			} 
			var startChar=retVal.substring(0,1);
			if(startChar==".") { 
				retVal="0"+retVal; 
			} 
			if(retVal>=0.01)
			  return false;
			else
			  return true;       

}
function getFile(imagePath,jsField){
	if(imagePath=='')
		return true;
	var pathLength = imagePath.length;
	var lastDot = imagePath.lastIndexOf(".");
	var fileType = imagePath.substring(lastDot,pathLength);
	if((fileType == ".jpg") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".jpeg")) {
		return true;
	} else {
		alert("We supports .JPG and .JPEG image formats. "+jsField+" file-type is " + fileType );
	}
}


function fnCallSaveProduct(){
	/*var shortDescValue=tinyMCE.get('shtDesc').getContent();	
	shortDescValue=(stripTags(shortDescValue));
	
	var longDescValue=tinyMCE.get('longDesc').getContent();	
	longDescValue=(stripTags(longDescValue));  */  
	
	if(document.forms[0].cateId.value==""){
		alert("Please select Category");
		document.forms[0].cateId.focus();	
		return;
	}
	else if(document.forms[0].prodTitle.value==""){
		alert("Please enter Item Name");
		document.forms[0].prodTitle.focus();	
		return;
	}
	else if(document.forms[0].brandName.value==""){
		alert("Please enter Brand Name");
		document.forms[0].brandName.focus();	
		return;
	}
	else if(document.forms[0].prodCode.value==""){
		alert("Please enter Item Code");
		document.forms[0].prodCode.focus();	
		return;
	}else if(document.forms[0].vendPrice.value==""){
		alert("Please enter Vendor Price");
		document.forms[0].vendPrice.focus();	
		return;
	}else if(document.forms[0].vendPrice.value!="" && validatePrice(document.forms[0].vendPrice.value)){
		alert("Please enter valid Retail Price");
		document.forms[0].vendPrice.focus();	
		return;
	}	
	else if(document.forms[0].shortDesc.value==""){
		alert("Please enter Short Description");
		document.forms[0].shortDesc.focus();	
		return;
	}
	else if(document.forms[0].longDesc.value==""){
		alert("Please enter Long Description");
		document.forms[0].longDesc.focus();	
		return;
	}else if(trim(document.forms[0].sbhComment.value)==""){
		alert("Please enter Comments");
		document.forms[0].sbhComment.focus();	
		return;
	}else{
		if(document.forms[0].vendPrice.value.charAt(0)=="$")
			document.forms[0].vendPrice.value=(document.forms[0].vendPrice.value).substring(1,document.forms[0].vendPrice.value.length)
	
		/*document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\n/g,'<br/>');
		document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>'); 
		
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\n/g,'<br/>');                     
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/\s/g,' ').replace(/  ,/g,'</br>');*/
		
		document.forms[0].shortDesc.value=trim(document.forms[0].shortDesc.value);
		document.forms[0].longDesc.value=trim(document.forms[0].longDesc.value);
		
		document.forms[0].shortDesc.value=document.forms[0].shortDesc.value.replace(/(\r\n)|(\n)/g, "<br>");
		document.forms[0].longDesc.value=document.forms[0].longDesc.value.replace(/(\r\n)|(\n)/g, "<br>"); 
	
		var mainImg = document.forms[0].uploadMainImagePath.value;
		var view1Img = document.forms[0].uploadView1ImagePath.value;
		var view2Img = document.forms[0].uploadView2ImagePath.value;
		var view3Img = document.forms[0].uploadView3ImagePath.value;
		if(getFile(mainImg, 'Main Image') && getFile(view1Img,'View1') && getFile(view2Img,'View2') && getFile(view3Img,'View3')){
			document.forms[0].action="updateVendorProduct.do?method=updateVendorProductDetails";
			document.forms[0].submit();
		}
	}
	
}

function fnCallPreview(){
	document.forms[0].action="previewProduct.do?method=PreviewProductDetails";
	document.forms[0].submit();
}
function textLimit(field, countfield,maxlen,dispName) {		
		if (field.value.length > maxlen + 1){
		  alert(dispName+" can have maximum of "+maxlen+" chars only.");	
		  countfield.value = 0;	
		 } 
		if (field.value.length > maxlen){
		   field.value = field.value.substring(0, maxlen);
		   countfield.value = 0;		
		}   
		else			
			countfield.value = maxlen - field.value.length;
}

function fnCallSearchProduct(jsProdId) {
	<c:if test="${!empty SourceOfEdit and SourceOfEdit eq 'View'}">
		document.forms[0].action="viewVendorProduct.do?method=viewVendorProductDetails&ProdId="+jsProdId;
		document.forms[0].submit();
	</c:if>
	<c:if test="${empty SourceOfEdit or SourceOfEdit ne 'View'}">
		document.forms[0].action="searchVendorCatalogue.do?method=searchVendorCatalogue";
		document.forms[0].submit();
	</c:if>
}
</script>

<tr><td>
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="../images/top_nav_leftcurve.png" width="26" height="44" /></td>
   <td width="100%" background="../images/top_nav_middlebg.png" align="left">		
		<ul>
		<li>You navigated from :</li>
		<li>Catalogue</li>
		<li>></li>
		<li>Edit/Search Item </li>
		</ul>
	
	</td>
    <td align="right"><img src="../images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
    <logic:present name="ErrMsg">
    <div class="error"><c:out value="${ErrMsg}"/></div>
     </logic:present>
    &nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
<html:form action="vendor/updateVendorProduct" method="post" enctype="multipart/form-data">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Item  Information</h2></td>
        </tr>
      <tr>
        <td align="center"><table border="0" cellpadding="0" cellspacing="4" class="tablecontent">
          <tr class="tdbg">
            <td align="left" class="lable">Vendor Name</td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
            <logic:present name="vendorLoginInfo" scope="session">
			<bean:write  name="vendorLoginInfo" property="userName" />	
			<input type="hidden" name="ownerId" value="<c:out value='${vendorLoginInfo.refId}'/>"/>		
			</logic:present>		
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Category *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
            
		   <html:select property="cateId" styleClass="textarea2" tabindex="1">
                  <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Item Name *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="prodTitle" styleClass="input" tabindex="2" maxlength="30"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Brand Name *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text property="brandName" styleClass="input" tabindex="3"  maxlength="63"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Code <br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
            	 <html:text styleId="productCode" property="prodCode" styleClass="input" tabindex="4" maxlength="16"/>
               
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Item Status *<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:select property="inShopStatus" styleClass="textarea2" tabindex="4">
                <html:option value="A">Active</html:option>
                <html:option value="I">Inactive</html:option>
              </html:select>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Color<br> </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="color" styleClass="input" tabindex="4" maxlength="250"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Size<br></span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="productCode" property="size" styleClass="input" tabindex="4" maxlength="250"/>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" nowrap="nowrap">Retail Price (in US$) *<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <html:text styleId="vendorPrice" property="vendPrice" styleClass="input" onkeyup="javascript:parseCurrency(this);" onchange="javascript:parseCurrency(this);" tabindex="5"/>
            </span></td>
          
			 <td align="left" nowrap="nowrap" class="lable">Pre-Authorize Credit Card</td>
			 <td class="lable">:</td>
            <td align="left"><span class="catagory"><html:checkbox  property="preAuthCC" styleClass="checkboxHTML" value="Y" tabindex="8"/>Yes
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" nowrap="nowrap">Approval Status<br></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <c:if test="${vendorProductDetails.sbhProdStatus eq 'N'}">Pending</c:if>
	          <c:if test="${vendorProductDetails.sbhProdStatus eq 'A'}">Accepted</c:if>
	          <c:if test="${vendorProductDetails.sbhProdStatus eq 'R'}">Rejected</c:if>
            </span></td>
          
			 <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
			 <td class="lable">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr valign="top" class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Title *<br></span></td>
            <td class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 250 chars.<br />
        </span><textarea name="shortDesc" class="textarea" rows="4" tabindex="6" onkeyup="textLimit(this.form.shortDesc,this.form.shortDesclen,250,'Short Description');"  id="shtDesc" style="width:520px;" ><logic:present name="vendorProductDetails"><logic:notEmpty name="vendorProductDetails"><bean:write  name="vendorProductDetails" property="shortDesc"/></logic:notEmpty></logic:present></textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="shortDesclen" size="3" maxlength="3" value="250" class="wordcount"/></td>
            </tr>
          <tr valign="top" class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Full Description *<br></span></td>
            <td class="lable">:</td>
            <td colspan="4" align="left"><span class="helptext" style="vertical-align:top">Max 500 chars.<br />
        </span><textarea name="longDesc" class="textarea" rows="4"  tabindex="7" onkeyup="textLimit(this.form.longDesc,this.form.longDesclen,500,'Long Description');"  id="longDesc" style="width:520px;" ><logic:present name="vendorProductDetails"><logic:notEmpty name="vendorProductDetails"><bean:write  name="vendorProductDetails" property="longDesc"/></logic:notEmpty></logic:present></textarea> <br />
          
            <span class="normaltext">Remaining characters</span>
            <input readonly="readonly" type="text" name="longDesclen" size="3" maxlength="3" value="500" class="wordcount"/></td>
            </tr>
		 
        <tr align="left" >
        <td colspan="6" valign="top" class="lable">
		  <table width="100%" align="center" cellpadding="0" cellspacing="0"  class="broder_top0">
		   <tr class="tdbg">
            <td colspan="2" class="tdtop">&nbsp;</td>
            </tr>
			<tr>
		  		<td rowspan="3"><c:if test="${vendorProductDetails.mainImgPath ne ''}">
		<div align="center"><img src="<c:out value='${vendorProductDetails.mainImgPath}' />" width="63" height="79" />
		<br/>
		</div>
		</c:if>
			  <div align="center"><bean:write  name="vendorProductDetails"  property="mainImgCap" /></div></td>
            <td width="85%" align="left" nowrap="nowrap" class="lable">Main Image </td>
            </tr>
			<tr>
			  <td width="85%" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadMainImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="8"/>
            </span></td>
			  </tr>
		   
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"> Caption </td>
            </tr>
          <tr class="tdbg">
            <td>&nbsp;</td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadMainImgCap" styleClass="caption_input" tabindex="9"  maxlength="63"/></td>
            </tr>
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
            <td colspan="2" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
		  	<td rowspan="3"><c:if test="${vendorProductDetails.view1ImgPath ne ''}">
		  	  <div align="center"><img src="<c:out value='${vendorProductDetails.view1ImgPath}' />" width="63" height="79" /><br/>
		  	        </div>
		  	  <div align="center"><bean:write  name="vendorProductDetails"  property="view1ImgCap" /></div></c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"> View1 </td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView1ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="10"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable">Caption1</td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view1ImgPath ne ''}">Remove <html:checkbox property="deleteView1Img" value="yes" styleClass="checkboxHTML"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView1ImgCap" styleClass="caption_input" tabindex="11"  maxlength="63"/></td>
            </tr>
          <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3"><c:if test="${vendorProductDetails.view2ImgPath ne ''}">
		    <div align="center"><img src="<c:out value='${vendorProductDetails.view2ImgPath}' />" width="63" height="79" /><br/>
		        </div>
		   <div align="center"> <bean:write  name="vendorProductDetails"  property="view2ImgCap" />	</div>	  </c:if>              </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View2</td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView2ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="12"/>
            </span></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"> Caption2</td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view2ImgPath ne ''}">Remove <html:checkbox property="deleteView2Img"  value="yes" styleClass="checkboxHTML"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"><html:text property="uploadView2ImgCap" styleClass="caption_input" tabindex="13"  maxlength="63"/></td>
            </tr>
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          <tr class="tdbg">
		  <td rowspan="3"><c:if test="${vendorProductDetails.view3ImgPath ne ''}">
		    <div align="center"><img src="<c:out value='${vendorProductDetails.view3ImgPath}' />" width="63" height="79" /><br/>
		        </div>
		    <div align="center"><bean:write  name="vendorProductDetails"  property="view3ImgCap" /></div>
		  </c:if>             </td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable">View3<br/></td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable"><span class="catagory">
            <html:file property="uploadView3ImagePath" accept="image/gif,image/jpeg" styleClass="browse" tabindex="14"/>
            </span>             </td>
            </tr>
          <tr class="tdbg">
            <td width="85%" valign="top" nowrap="nowrap" class="lable">Caption3 </td>
            </tr>
          <tr class="tdbg">
            <td><div align="center"><c:if test="${vendorProductDetails.view3ImgPath ne ''}">Remove <html:checkbox property="deleteView3Img"  value="yes" styleClass="checkboxHTML"/>
              </c:if></div></td>
            <td width="85%" align="left" valign="top" nowrap="nowrap" class="lable"> <html:text property="uploadView3ImgCap" styleClass="caption_input" tabindex="15"  maxlength="63"/></td>
            </tr>
          
		  <tr class="tdbg">
            <td colspan="2" class="td">&nbsp;</td>
            </tr>
          </table>			
		<p>&nbsp;</p></td>
            </tr>
			 <tr class="tdbg">
            <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="4" align="left">
			<span class="helptext" style="vertical-align:top">Max 250 chars.</span><br />
			<textarea name="sbhComment"  class="textarea" rows="5"  tabindex="16" style="width:520px;" onKeyUp="textLimit(this.form.sbhComment,this.form.commentlen,250,'Comments');"></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="250" class="wordcount"/>	
			
			</td>
			 </tr>
			
			
			
      </table>
      </tr>
      <tr>
          <td height="50" colspan="3" align="center">
          		<html:button property="method" styleClass="button" 
			  	onclick="javascript:fnCallSaveProduct()" tabindex="17">Save </html:button> 	
	          	<input type="button" class="button" 
			  	onclick="javascript:fnCallSearchProduct(<c:out value="${vendorProductDetails.prodId}"/>);" tabindex="18" value="Cancel" />
		 </td>
      </tr>
	   <tr>
        	<td height="50" colspan="3" align="center" class="help"><table width="80%" border="0" cellpadding="2">
            <tr>
              <td valign="top"><strong>Note:</strong> </td>
              <td align="left">The image uploaded should have a minimum resolution of 175 dpi 
                and the minimum dimension of 970 x 906 pixels.</td>
            </tr>
          </table>
          </td>
      </tr>
    </table>
	<html:hidden property="prodId"/>	
<html:hidden property="ownerId"/>

<html:hidden property="imgType"/>
<html:hidden property="poPct"/>

</html:form>
	</td>
    </tr>
  
  <tr>
    <td><img src="../images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="../images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="../images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</td></tr>