<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page language="java"%>
<%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="../WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ taglib uri="../WEB-INF/c.tld" prefix="c"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/> 
<title>Welcome to SkyBuyHigh - Vendor</title>
<link rel="stylesheet" type="text/css" media="screen" href="../style/Menu/masthead.css" />
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" href="../style/Menu/ie6-mh.css" media="screen"/>
<![endif]--> 	
<!--[if  IE 7]>
<link rel="stylesheet" type="text/css" href="../style/Menu/ie7-mh.css" media="screen"/>
<![endif]--> 
<!-- <script type="text/javascript" src="../js/Menu/scripts.js"></script> -->
<script type="text/javascript" src="../js/Menu/utils.js"></script>
<script type="text/javascript" language="../javascript" src="../js/Menu/globalnew.js"></script>
<!-- <script type="text/javascript" language="../javascript" src="../js/Menu/add-event.js"></script> -->
<link rel="stylesheet" href="../style/index.css"  type="text/css" />
<!-- <script type="text/javascript" src="../js/coverflow.js"></script> -->
<link rel="stylesheet" type="text/css" media="screen" href="../style/home.css" />
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="../style/ie.css" media="screen"/>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="../style/ie7.css" media="screen"/>
<![endif]-->
<script type="text/javascript" src="../js/swfobject.js"></script>
<link rel="stylesheet" href="../spellify/images/spellify.css" type="text/css" media="screen" />
<!--[if IE]><link href="../spellify/images/ie.css" rel="stylesheet" type="text/css" media=screen><![endif]-->
<!--[if IE 6]><link href="../spellify/images/ie6.css" rel="stylesheet" type="text/css" media=screen><![endif]-->
<script src="../spellify/src/prototype.js" type="text/javascript"></script>
<script src="../spellify/src/scriptaculous.js" type="text/javascript"></script>
<script src="../spellify/src/spellify.js" type="text/javascript"></script>

</head>
<body>

<!-- Spellify BEGIN -->
<div id="spellify">
  <div id="correctionsContainer" style="position: absolute; z-index: 1000; display: none; background-color: transparent;">
    <ul>
      <li id="arrowItem"><span id="arrow"><img src="../spellify/images/arrw.png" width="12" height="6" alt="" /></span></li>
      <li>
        <div id="correctionsBody"></div>
      </li>
      <li>
        <div id="correctionsFooter"></div>
      </li>
    </ul>
  </div>
  <div id="loaderDiv" style="position: absolute; z-index: 1000; display: none;"><img src = "../spellify/images/ldr.gif" alt="" /></div>
</div>
<!-- Spellify END -->
<tiles:insert attribute="header"/>
<tiles:insert attribute="Menu"/>
<tiles:insert attribute="mainContent"/>
<tiles:insert attribute="footer"/>