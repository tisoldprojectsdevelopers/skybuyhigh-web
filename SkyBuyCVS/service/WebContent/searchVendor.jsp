<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>



<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
	
	function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
	function isEmpty(frm_fld){
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
	
	
	
	function isNumber(jsCustNo) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert("Vendor ID must be an numeric and should be valid.");
			document.forms[0].vendorSearchBy.focus();						     
			return false;
		}
	  }
	 return true;
	}
	
	function checkLength(jsText){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid Customer ID ");
			return false;
		}	
		return true;	
	}

function fnValidateId(jsIdValue) {		
		if(isEmpty(jsIdValue)){			
			alert("Please enter valid Vendor ID");			
			document.forms[0].vendorSearchBy.focus();			
			return;			
		}
		else{	
			if(isNumber(document.forms[0].vendorSearchValue)){
				if(checkLength(document.forms[0].vendorSearchValue)){
					document.forms[0].action="searchVendor.do?method=searchVendor";
					document.forms[0].submit();
				}
				else
					document.forms[0].vendorSearchBy.focus();
			}	
		}
	}
	
function fnValidateName(jsName,jsLabelName) {		
		if(isEmpty(jsName)){
			alert("Please enter "+jsLabelName);
			document.forms[0].vendorSearchValue.focus();
			return;
		}			
		else{					
			document.forms[0].action="searchVendor.do?method=searchVendor";
			document.forms[0].submit();	
		}
	}	

function fnCallSearch(){
	if(document.forms[0].vendorSearchBy.value=="VendorName"){			
		fnValidateName(document.forms[0].vendorSearchValue,"Vendor Name");
	}
	else if(document.forms[0].vendorSearchBy.value=="VendorId"){			
		fnValidateId(document.forms[0].vendorSearchValue,"Airline");
	}else{
		if(document.forms[0].vendorSearchBy.value=="All"){
			document.forms[0].vendorSearchValue.value="";
		}

		document.forms[0].action="searchVendor.do?method=searchVendor";
		document.forms[0].submit();			
	}
	
}
function fnCallEdit(jsVendIdValue) {
	document.forms[0].action="editVendor.do?method=EditVendorDetails&VendId="+jsVendIdValue;
	document.forms[0].submit();

}
function fnCancel() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();

}

/********gtky search end *****/	
	
</script>

<html:form action="searchVendor.do?method=searchVendor" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="images/top_nav_middlebg.png">
		<!--Admin -->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
		</logic:present>
		<!-- vendor-->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'vendor'}">
		<ul>
		<li>You navigated from :</li>
		<li>Vendor</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
		</logic:present>		
		

	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td><html:select property="vendorSearchBy" styleClass="textarea2">	
						 <html:option value="All">All</html:option>					  						  		
						<html:option value="VendorName">Vendor Name</html:option>		
												
	               		 </html:select>		</td>
        <td class="lable">Search Value : </td>
        <td><html:text property="vendorSearchValue" styleClass="input"/></td>
        <td><div class="catagory"><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center">
	 <logic:present name="productInfo" scope="request"> 
		 <logic:notEmpty name="productInfo">							
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td colspan="3" class="tablehead" align="center" ><h2>Vendor Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1"  bgcolor="#cccccc" class="broder_top0">
              <tr>               
                <td height="32" class="table_header" nowrap="nowrap">Vendor Name</td>
				 <td class="table_header" nowrap="nowrap">Contact Name</td>
                <td class="table_header" nowrap="nowrap" >Address</td>              
                <td class="table_header" nowrap="nowrap">City</td>
                <td class="table_header" nowrap="nowrap">State</td>
                <td class="table_header" nowrap="nowrap">Country</td>
                <td class="table_header" nowrap="nowrap">Status</td>
                <td class="table_header" nowrap="nowrap">Action</td>
              </tr>
              <logic:iterate id="ProductInfo" name="productInfo">
              <tr class="tdbg">             
                <td align="left" nowrap="nowrap" ><bean:write name="ProductInfo" property="vendName"/></td>
				<td align="left" nowrap="nowrap" width="150px"><bean:write name="ProductInfo" property="vendContactName"/></td>
                <td align="left" nowrap="nowrap" style="word-wrap:break-word;width:250px;"><bean:write name="ProductInfo" property="vendAddr1"/></td>             
                <td align="left" nowrap="nowrap" width="100px"><bean:write name="ProductInfo" property="vendCity"/></td>
                <td align="left" nowrap="nowrap">
				
				<logic:iterate id="states" name="StateList" scope="application">
					<c:if
						test="${states.stateCode eq ProductInfo.vendState}">
						<bean:write name="states" property="stateName" />
					</c:if>
			   </logic:iterate>	
				</td>
                 <td align="center" nowrap="nowrap" width="50px"><bean:write name="ProductInfo" property="vendCountry"/></td>
                <td align="left" nowrap="nowrap">
				<c:if test="${ProductInfo.vendStatus eq 'A'}">Active</c:if>
				<c:if test="${ProductInfo.vendStatus eq 'I'}">Inactive</c:if>
				</td>
                <td align="center" class="action"><a href="javascript: void fnCallEdit('<bean:write name='ProductInfo' property='vendId'/>')" title="Edit My Info">Edit</a></td>
              </tr>
             </logic:iterate>
            </table></td>
      </tr>
     
    </table>
	 </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
	</td>
    </tr>
	 <logic:present name="productInfo" scope="request"> 
		 <logic:notEmpty name="productInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnCancel();" styleClass="button">Cancel</html:button></td>
    </tr>
	 </logic:notEmpty>
	</logic:present>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</html:form>