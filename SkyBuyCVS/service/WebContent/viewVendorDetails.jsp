<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "shortDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
				var key= ed.selection.select(e.keyCode);
				if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
					var str=ed.getContent();
					str=(stripTags(str)); 
					str=trim(str); 
					var textLen = 128;
					if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
						textLen = 50;
					//alert(textLen);
					if(textLimit(str.length,textLen,"Short Description A"))
						return true; 
				}	
		  }); 
		return false;
	  } 
});

/****Offer Description B****/

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "longDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
			var key= ed.selection.select(e.keyCode);
			if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
				var str=ed.getContent();
				str = stripTags(str);
				str=trim(str); 
				var textLen = 128;
				if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
					textLen = 60;
				if(textLimit(str.length,textLen,"Long Description")) 
					return true; 
			}		
		  }); 
		return false;
	  } 
});


function fnCallSubmit(){
	document.forms[0].action="updateVendor.do?method=UpdateVendorDetails";
	document.forms[0].submit();
}

</script>




<html:form action="editVendor" method="post" >
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%"align="left" background="images/top_nav_middlebg.png">
	
	<!--Admin -->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
		</logic:present>
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Add'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Add Vendor</li>
		</ul>
		</c:if>
		</logic:present>	
		
			
		<!-- vendor-->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'vendor' && mode eq 'Edit'}">
		<ul>
		<li>You navigated from :</li>
		<li>Vendor</li>
		<li>></li>
		<li>Edit/Search Vendor</li>
		</ul>
		</c:if>
		</logic:present>	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">

	<table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Vendor Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr class="tdbg">
            <td height="4" colspan="7" class="lable"></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable">Vendor Name</td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails"  property="vendName"/>
            </span></td>
            <logic:present name="loginInfo" scope="session">
              <c:if test="${loginInfo.userType eq 'admin'}">
                <td width="4" rowspan="8" class="lable"></td>
                <td align="left" class="lable" nowrap="nowrap"><span class="boldtext">Vendor Type</span></td>
                <td class="lable">:</td>
                <td align="left"><span class="catagory">
                  <c:if test="${vendorDetails.vendType eq 'B'}"> Basic </c:if>
                  <c:if test="${vendorDetails.vendType eq 'P'}"> Premium </c:if>
                </span></td>
              </c:if>
            </logic:present>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Contact Name </span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendContactName" />
            </span></td>
            <td align="left" class="lable"><span class="boldtext">Status</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <logic:notEmpty name="vendorDetails">
                <logic:equal name="vendorDetails" property="vendStatus" value="I"> Inactive </logic:equal>
                <logic:equal name="vendorDetails" property="vendStatus" value="A"> Active </logic:equal>
              </logic:notEmpty>
            </span> </td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable" valign="top" nowrap="nowrap"><span class="boldtext">Company Address</span></td>
            <td class="lable" valign="top">:</td>
            <td align="left" style="word-wrap:break-word;width:150px;" valign="top" ><span class="catagory">
              <bean:write  name="vendorDetails" property="vendAddr1" />
            </span></td>
            <td align="left" class="lable" nowrap="nowrap" valign="top">Additional Address</td>
            <td class="lable" valign="top"  >:</td>
            <td align="left" style="word-wrap:break-word;width:200px;" valign="top" ><span class="catagory">
              <bean:write  name="vendorDetails" property="vendAddr2" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">City</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendCity" />
            </span></td>
            <td align="left" class="lable"><span class="boldtext">State</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <logic:iterate id="states" name="StateList" scope="application">
                <c:if
				test="${states.stateCode eq vendorDetails.vendState}">
                  <bean:write name="states" property="stateName" />
                </c:if>
              </logic:iterate>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Country</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendCountry" />
            </span></td>
            <td align="left" class="lable"><span class="boldtext">Zip</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendZip" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable">Phone1 <br></td>
            <td class="lable">:</td>
            <td align="left"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><span class="catagory">
                    <bean:write  name="vendorDetails" property="vendPhone1" />
                  </span></td>
                  <c:if test="${!empty vendorDetails.vendPhoneExt1 && vendorDetails.vendPhoneExt1 ne ''}">
                  <td class="lable">Ext1 :</td>
                  <td><span class="catagory">
                    <bean:write  name="vendorDetails" property="vendPhoneExt1" />
                  </span></td>
                  </c:if>
                </tr>
            </table></td>
             <c:if test="${!empty vendorDetails.vendPhone2 && vendorDetails.vendPhone2 ne ''}">
            <td align="left" class="lable">Phone2 </td>
            <td class="lable">:</td>
            <td align="left"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><span class="catagory">
                    <bean:write  name="vendorDetails" property="vendPhone2" />
                  </span></td>
				  <c:if test="${!empty vendorDetails.vendPhoneExt2 && vendorDetails.vendPhoneExt2 ne ''}">
                  <td class="lable">Ext2 :</td>
                  <td><span class="catagory">
                    <bean:write  name="vendorDetails" property="vendPhoneExt2" />
                  </span></td>
				  </c:if>
                </tr>
            </table></td>
            </c:if>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Fax</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendFax" />
            </span></td>
            <td align="left" class="lable"><span class="boldtext">Email</span></td>
            <td class="lable">:</td>
            <td align="left"><span class="catagory">
              <bean:write  name="vendorDetails" property="vendEmail" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Return Policy</span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="5" align="left" style="word-wrap:break-word;width:420px;" valign="top"><bean:write  name="vendorDetails" property="vendReturnPolicy"/></td>
          </tr>
          <tr class="tdbg">
            <td width="120" align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Comments</span></td>
            <td valign="top" class="lable">:</td>
            <td colspan="5" align="left" style="word-wrap:break-word; width:420px;" valign="top"><bean:write  name="vendorDetails" property="vendComments"/></td>
          </tr>
          <tr class="tdbg">
            <td height="4" colspan="7" class="lable" ></td>
          </tr>
        </table>
      </tr>
      <tr>
       <td height="50" colspan="3" align="center">
     <html:link href="preEntry.do?method=preEntry" styleClass="button" style="text-decoration:none"> &nbsp;&nbsp;&nbsp; OK&nbsp;&nbsp;&nbsp;   </html:link>	  </td>
      </tr>
    </table>

	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>


<html:hidden property="vendId" />

</html:form>
