<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div id="menuHead">
 
<logic:present name="loginInfo" scope="session">
	<ul  class="nav">
  <li><a href="preEntry.do?method=preEntry">Home</a></li>
      <c:if test="${loginInfo.userType eq 'vendor'}">
			   
				<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
				<li ><a href="#" >Vendor</a>
					<ul class="subMenu">
						<li class="last">
							<a class="menuItem" href="editVendor.do?method=EditVendorDetails&VendId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a>
						</li>
					</ul>
				</li>		
				<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
				<li><a href="#">Catalogue</a>
					<ul class="subMenu">					
					<li><a class="menuItem" href="initAddVendorProduct.do?method=initAddVendorProduct" title="Add Item">Add Item</a></li>
					<li><a class="menuItem" href="initSearchVendorCatalogue.do?method=initSearchVendorCatalogue" title="Edit/Search Item">Edit/Search Item</a> </li>
					<li class="last"><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Vendor" title="Generate Catalogue">Generate Catalogue</a> </li>
					</ul>
				</li>
				<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
				<li><a href="#">Order</a>
					<ul class="subMenu">	
							<li><a class="menuItem" href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>		
					</ul>
				</li>
				<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
				<li  class="selected"><a href="http://skybuyhigh.com/reports/sbhorder.php?ownerid=<c:out value='${loginInfo.refId}'/>&ownertype=<c:out value='${loginInfo.userType}'/>" title="Reports/Statements">Reports/Statements</a></li>

	</c:if>	
	<c:if test="${loginInfo.userType eq 'airline'}">
		  
			<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
			<li  ><a href="#" >Airline</a>
				<ul class="subMenu">
						<li>
							<a class="menuItem" href="editAirlineReg.do?method=editAirlineDetails&airId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a>					                        </li>
							
						
						<li class="last"><a class="menuItem" href="initSearchDevice.do?method=initSearchDevice" title="Search Device">Search Device</a> </li>
				</ul>
		  </li>
		
		 <li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	     <li ><a href="#">Catalogue</a>
			<ul class="subMenu">					
				<li><a class="menuItem" href="initAddAirlineProduct.do?method=initAddAirlineProduct" title="Add Item">Add Item</a></li>
				<li><a class="menuItem" href="initSearchAirlineCatalogue.do?method=initSearchAirlineCatalogue" title="Edit/Search Item">Edit/Search Item</a>  </li>
				
				<li class="last"><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Airline" title="Generate Catalogue">Generate Catalogue</a> </li>
				
			</ul>
	    </li>
		<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
		<li ><a href="#">Order</a>
			<ul class="subMenu">	
				<li><a class="menuItem" href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>		
			</ul>
		</li>
		<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
		<li class="selected" ><a href="http://skybuyhigh.com/reports/sbhorder.php?ownerid=<c:out value='${loginInfo.refId}'/>&ownertype=<c:out value='${loginInfo.userType}'/>" title="Reports/Statements">Reports/Statements</a></li>
	
	</c:if>	
	<c:if test="${loginInfo.userType eq 'admin'}">
		<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li ><a href="#" >Admin</a>
	<ul class="subMenu">
		<li><a class="menuItem" href="initAddVendor.do?method=initAddVendor" title="Add Vendor">Add Vendor</a> </li>
		<li><a class="menuItem" href="initSearchVendor.do?method=initSearchVendor" title="Edit/Search Vendor">Edit/Search Vendor</a> 	</li>
		<li><a class="menuItem" href="initAddAirlineReg.do?method=initAddAirlineReg" title="Add Airline">Add Airline</a>  </li>
		<li class="last"><a class="menuItem" href="initSearchAirline.do?method=initSearchAirline" title="Edit/Search Airline">Edit/Search Airline</a> 	</li>
   </ul>	
	</li>
		
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li><a href="#">Merchandise</a>
			<ul class="subMenu">					
				<li><a class="menuItem" href="initEditSearchMerchandize.do?method=initEditSearchMerchandize" title="Edit/Search Merchandise">Edit/Search Merchandise</a> </li>
					<li></li>
				<li class="last"><a class="menuItem" href="initSearchMerchandize.do?method=initSearchMerchandize&MerchandizeAction=Approve" title="Accept/Reject Merchandise">Accept/Reject Merchandise</a>   </li>
		</ul>
	</li>
		
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li  ><a href="#">Order</a>
	<ul class="subMenu">	
		<li><a class="menuItem" href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>
		<li><a class="menuItem" href="initPlaceOrder.do?method=initPlaceOrder" title="Add Item">Place Order</a></li>
	</ul>
	</li>
	
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li class="selected"><a href="http://skybuyhigh.com/reports/sbhorder.php?ownerid=<c:out value='${loginInfo.refId}'/>&ownertype=<c:out value='${loginInfo.userType}'/>" title="Reports/Statements">Reports/Statements</a></li>
		</c:if>
		</ul>
	</logic:present>
</div>
</div>