<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>



<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">

	 function triggerEvent() {
			if(event.keyCode==13) {
			  var val=document.forms[0].vendorSearchBy.value;
			  fnCallCustInfoByCustIdAndName();  
			}           
	 }
	
	
	
	
	
	function isNumber(jsCustNo) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert("Customer ID must be an numeric and should be valid.");
			document.forms[0].vendorSearchBy.focus();						     
			return false;
		}
	  }
	 return true;
	}
	
function checkLength(jsText){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid Customer ID ");
			return false;
		}	
		return true;	
}

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}
function isEmpty(frm_fld){
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
function fnValidateName(jsName,jsLabelName) {		
		if(isEmpty(jsName)){
			alert("Please enter "+jsLabelName);
			document.forms[0].airlineSearchValue.focus();
			return;
		}			
		else{					
			document.forms[0].action="searchAirline.do?method=searchAirline";
			document.forms[0].submit();	
		}
	}	
/*** gtky search start  ****/
function fnCallSearch(){
	
	if(document.forms[0].airlineSearchBy.value=="airContactName"){			
		fnValidateName(document.forms[0].airlineSearchValue,"Airline Contact Name");
	}
	else if(document.forms[0].airlineSearchBy.value=="airName"){			
		fnValidateName(document.forms[0].airlineSearchValue,"Airline");
	}else{
		if(document.forms[0].airlineSearchBy.value=="All"){
			document.forms[0].airlineSearchValue.value="";
		}
		document.forms[0].action="searchAirline.do?method=searchAirline";
		document.forms[0].submit();		
	}
}
function fnCallEdit(jsAirIdValue) {
	document.forms[0].action="editAirlineReg.do?method=editAirlineDetails&airId="+jsAirIdValue;
	document.forms[0].submit();

}
function fnCancel() {
	document.forms[0].action="preEntry.do?method=preEntry";
	document.forms[0].submit();

}

	
</script>

<html:form action="searchAirline.do?method=searchAirline" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" align="left" background="images/top_nav_middlebg.png">
	<!--Admin -->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'admin'}">
		<ul>
		<li>You navigated from :</li>
		<li>Admin</li>
		<li>></li>
		<li>Edit/Search Airline</li>
		</ul>
		</c:if>
		</logic:present>
		<!--Airline-->
		<logic:present name="loginInfo" scope="session">	
		<c:if test="${loginInfo.userType eq 'airline'}">
		<ul>
		<li>You navigated from :</li>
		<li>Airline</li>
		<li>></li>
		<li>Edit/Search Airline</li>
		</ul>
		</c:if>
		</logic:present>	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<table border="0" align="center" cellpadding="0" cellspacing="2" class="searchtable">
      <tr>
        <td class="lable">Search By : </td>
        <td><html:select property="airlineSearchBy" >					  						  						<html:option value="All">All</html:option>	
						<html:option value="airContactName">Airline Contact Name</html:option>	 		  			
						<html:option value="airName">Airline</html:option>
												
	               		 </html:select>	</td>
        <td class="lable">Search Value:</td>
        <td> <html:text property="airlineSearchValue" styleClass="input" onkeydown="javascript:triggerEvent();"/></td>
        <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
        </tr>
    </table></td>
  </tr>
  
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="airlineInfo" scope="request"> 
		 <logic:notEmpty name="airlineInfo">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Airline Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="3" cellspacing="1"  bgcolor="#cccccc" class="broder_top0">
              <tr>
               
                <td height="32" align="center" nowrap="nowrap" class="table_header" width="150px">Airline Name</td>
                <td class="table_header" nowrap="nowrap" width="150px">Contact Name</td>
                <td class="table_header" nowrap="nowrap" width="200px">Address</td>
                <td class="table_header" nowrap="nowrap"  width="100px">City</td>
                <td class="table_header" nowrap="nowrap"  width="50px">State</td>
                <td class="table_header" nowrap="nowrap"  width="50px">Country</td>
                <td class="table_header" nowrap="nowrap" width="60px">Zip</td>
                <td class="table_header" nowrap="nowrap">Action</td>
              </tr>
              <logic:iterate id="AirlineInfo" name="airlineInfo">
              <tr class="tdbg">
                <td align="left"><bean:write name="AirlineInfo" property="airName"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airContactName"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airAddr1"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airCity"/></td>
                 <td align="left" nowrap="nowrap" width="80px;">
				 	<logic:iterate id="states" name="StateList" scope="application">
					<c:if
						test="${states.stateCode eq AirlineInfo.airState}">
						<bean:write name="states" property="stateName" />
					</c:if>
			    </logic:iterate>	
				 
				</td>
                <td align="center"><bean:write name="AirlineInfo" property="airCountry"/></td>
                <td align="left"><bean:write name="AirlineInfo" property="airZip"/></td>
                <td align="center" class="action"><a href="javascript: void fnCallEdit('<bean:write name='AirlineInfo' property='airId'/>')" title="Edit My Info">Edit</a></td>
              </tr>
             </logic:iterate>
            </table></td>
      </tr>
      
    </table>
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>
	</td>
    </tr>
	<logic:present name="airlineInfo" scope="request"> 
		 <logic:notEmpty name="airlineInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="javascript:fnCancel();" styleClass="button">Cancel</html:button></td>
    </tr>
	 </logic:notEmpty>
	</logic:present>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>

</div>

</html:form>

