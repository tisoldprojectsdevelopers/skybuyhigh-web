<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="html-el"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="bean-el"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<script src="js/datepicker.js" type=text/javascript></script>
<link href="style/datepic.css" rel="stylesheet" type="text/css" />
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script language="JavaScript">
function textLimit(field,maxlen,dispName) {
   fieldLen=trim((field.value)).length;
  if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function isEmpty(frm_fld){
    
		if (frm_fld.value.length < 1){
			return true;
		}else {
			var strInput = new String(frm_fld.value);		
			if (trim(strInput)=="") {
				return true;
			}
			return false;
		}
		return false;
	}
		
function isNumber(jsCustNo,jsName) {
	  var str = jsCustNo.value;
	  var str1=trim(str);	  
	  if(str1.length > 0){ 
		var re = /^[-]?\d*\.?\d*$/;
		str1 = str1.toString();
		if (!str1.match(re)) {
			alert(jsName +" must be an numeric and should be valid.");
			document.forms[0].searchValue.focus();						     
			return false;
		}
	  }
	 return true;
	}	
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
	}	
	
	function checkLength(jsText,jsName){
		var text = jsText.value;
		text = trim(text);
		if((text < -2147483648) || (text > 2147483647)){
			alert("Please enter valid "+jsName);
			return false;
		}	
		return true;	
	}


function isDate(dateObj){	
	//var datevar = document.f1.t1.value;
	var y,m,d;
	
	 var datevar = dateObj;
	datevar =dateFormat(datevar);
	var dateTmp = datevar.replace("/","");
	dateTmp = dateTmp.replace("/","");
	if(dateTmp.length==8 || dateTmp.length==10  ){
			if(datevar.length==10){
				var ind = datevar.indexOf("/");
				if(ind==2){
					y = datevar.substring(6,10);
					m = datevar.substring(0,2);
					d = datevar.substring(3,5);
				}
				else{
					y = datevar.substring(0,4);
					m = datevar.substring(5,7);
					d = datevar.substring(8,10);
				}
				if(checkdate(d,m,y)){
					datevar = m+'/'+d+'/'+y; 
					dateObj.value= datevar;		
					return true;
				}
				else{
				//	dateObj.focus();
					return false;
				}
			}//endif(datevar)
			else if(datevar.length==8){
				if(datevar.indexOf("/")>=0 || datevar.indexOf("-")>=0){
					var yTemp1 = datevar.substring(6,8);
					y="20"+yTemp1;					
					m = datevar.substring(0,2);					
					d = datevar.substring(3,5);
					if(checkdate(d,m,y)){					
						dateObj.value	= datevar;		
						return true;
					}else{
					//	dateObj.focus();
						return false;
					}
					
				}else{
					y = datevar.substring(4,8);
					m = datevar.substring(0,2);
					d = datevar.substring(2,4);
					if(checkdate(d,m,y)){
						dateObj.value	= datevar;		
						return true;
					}
					else{
					//	dateObj.focus();
						return false;
					}
				}
			}
		}
		else{
			//dateObj.focus();
			return false;
		}	
		
}

function dateFormat(sDate){
	var parseYr;
	var yl=1990;
	var ym=2200;
		if(sDate.length==8 || sDate.length==10  ){
			if(sDate.length==10){
				y = sDate.substring(6,10);
				m = sDate.substring(0,2);
				d = sDate.substring(3,5);
			}else if(sDate.length==8){
				if(sDate.indexOf("/")>=0 || sDate.indexOf("-")>=0){
					var yTemp1 = sDate.substring(6,8);
					y="20"+yTemp1;					
					m = sDate.substring(0,2);					
					d = sDate.substring(3,5);
				}else{
					y = sDate.substring(4,8);
					m = sDate.substring(0,2);
					d = sDate.substring(2,4);
				}
			}
		   /*else if(sDate.length==6){
				var yTemp = sDate.substring(4,6);				
				y="20"+yTemp;
				m = sDate.substring(0,2);				
				d = sDate.substring(2,4);				
			}	*/
	if (y<yl || y>ym) {
		parseYr = y+""+m+""+d;
		return parseYr;
	}
	else
		return sDate;
	}
	return sDate;
}
function checkdate(d,m,y)
{
//alert(m);
	var yl=1990; // least year to consider
	var ym=2200; // most year to consider
	if(!IsNumeric(y)  || !IsNumeric(m) || !IsNumeric(d)) return(false);
	if (m<1 || m>12) return(false);
	if (d<1 || d>31) return(false);
	if (y<yl || y>ym) return(false);
	if (m==4 || m==6 || m==9 || m==11)
	if (d==31) return(false);
	if (m==2)
	{
	var b=parseInt(y/4);
	if (isNaN(b)) return(false);
	if (d>29) return(false);
	if (d==29 && ((y/4)!=parseInt(y/4))) return(false);
	}
	return(true);
}
function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function triggerEvent() {
	if(event.keyCode==13) {
	fnCallSubmit();
	}           
}

function fnCallSearch(){

	if(document.forms[0].orderToDate.value == ""){
			
		alert("Please enter valid Order To Date ");
		document.forms[0].orderDate.focus();			
		return false;
	}
	/*else if(!isDate(document.forms[0].catalogueDate.value)){	
		alert('d2');			
		alert("Please enter valid Catalogue Date ");
		document.forms[0].catalogueDate.focus();			
		return false;
	}*/
	else{	
		
		document.forms[0].action="searchOrder.do?method=searchOrder";
		document.forms[0].submit();			
	}
	
}
function fnCallEdit(jsOrderItemIdValue) {
	document.forms[0].action="editOrder.do?method=editOrderDetails&OrderItemId="+jsOrderItemIdValue;
	document.forms[0].submit();

}



/********gtky search end *****/	
	
</script>
<html:form action="/initSearchOrder" method="post">

<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver" ><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="images/top_nav_middlebg.png" align="left"> 
		<ul>
		<li>You navigated from :</li>
		<li>Order</li>
		</ul>
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td"><table border="0" align="center" cellpadding="0" cellspacing="0" class="searchtable">
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2" >
          <tr>
            <td nowrap="nowrap" class="lable">Search By : </td>
            <td nowrap="nowrap">			  
			  			 <html:select property="searchBy" styleClass="textarea2">	
							 <html:option value="ALL">All</html:option>							  		
	 		  				<html:option value="CUSTTRNSID">Order Confirmation No</html:option>
							<html:option value="CUSTNAME">Customer Name</html:option>	
							<html:option value="CUSTPHONE">Customer Phone No</html:option>	
							<html:option value="AIRNAME">Airline Name</html:option>	
							<html:option value="FLIGHTNO">Tail No</html:option>												
	               		 </html:select>
			          </td>
            <td nowrap="nowrap"><html:text property="searchValue" styleClass="search-input" onkeydown="javascript:triggerEvent();"/></td>
            <td nowrap="nowrap" class="lable">Category :</td>
            <td nowrap="nowrap"><html:select property="category" styleClass="textarea2">
                <html:option value="ALL">All</html:option>
				<c:if test="${loginInfo.userType eq 'admin' || loginInfo.userType eq 'vendor'}">
                <html:options collection="Category" property="cateId" labelProperty="cateName"></html:options>
				</c:if>
				<html:option value="20">Private Jet Jaunts</html:option>	
				
            </html:select></td>
			 <td nowrap="nowrap" class="lable">Status :</td>
            <td nowrap="nowrap"><html:select property="orderStatus" styleClass="textarea2">
                <html:option value="ALL">All</html:option>
                <html:option value="C">Completed</html:option>
				 <html:option value="F">Failed</html:option> 
				 <html:option value="P">Pending</html:option>
            </html:select></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
           	 <td nowrap="nowrap" class="lable">From Date: </td>
            <td nowrap="nowrap"><html:text property="orderFromDate"  styleClass="search-input" />
                <img  src="images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('orderFromDate', false, 'mdy', '/');" /></td>
			<td nowrap="nowrap" class="lable">To Date: </td>
            <td nowrap="nowrap"><html:text property="orderToDate"  styleClass="search-input" />
                <img  src="images/dateicon.gif" hspace="2" border="0" align="absmiddle" onclick="displayDatePicker('orderToDate', false, 'mdy', '/');" /></td>	
            <td nowrap="nowrap">&nbsp;</td>
            
            <td><html:button property="method" value="Search"  styleClass="button" onclick="fnCallSearch();"/></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" class="td" align="center" valign="top">
	<logic:present  name="orderItemsInfo"> 
 	 <logic:notEmpty name="orderItemsInfo">	
	<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
        <td valign="top">
		  <table border="0" width="100%" cellpadding="1" cellspacing="1" bgcolor="#cccccc" class="broder_top0">
          <tr>
          <td align="center" class="table_header" height="32">Order Confirmation No</td>
          <td class="table_header" nowrap="nowrap">Customer Name</td>  
		  <td class="table_header" nowrap="nowrap">Phone No</td>        
          <td class="table_header" nowrap="nowrap">Item Code</td>
		  <td class="table_header" nowrap="nowrap">Item Name </td>
          <td class="table_header" nowrap="nowrap">Brand Name</td>
		   <td class="table_header" nowrap="nowrap">Category</td>
          <td nowrap="nowrap" class="table_header">Status</td>
          <td class="table_header" nowrap="nowrap">Order Date</td>
		 <!-- <td  class="table_header" nowrap="nowrap">Airline Name</td>	-->
		   <td class="table_header" nowrap="nowrap">Action</td>			
          </tr>
		  <logic:iterate id="OrderItemsInfo" name="orderItemsInfo">
          <tr class="tdbg">
         	 <td align="left"><div class="lable"><bean:write name="OrderItemsInfo" property="custTransId"/></div></td>          	
			 <td align="left" nowrap="nowrap"><div class="lable"><bean:write name="OrderItemsInfo" property="custFirstName"/></div></td>      
			  <td align="left" nowrap="nowrap"><div class="lable"><bean:write name="OrderItemsInfo" property="custPhone"/></div></td>         			
			  <td align="left"  nowrap="nowrap"><div class="lable"><bean:write name="OrderItemsInfo" property="prodCode"/></div></td>   
			  <td align="left" nowrap="nowrap"><div class="lable"><bean:write name="OrderItemsInfo" property="itemName"/></div></td>    
			  <td align="left" nowrap="nowrap"><div class="lable"><bean:write name="OrderItemsInfo" property="brandName"/></div></td>   
			  <td align="left" nowrap="nowrap"><div class="lable">
			  <logic:iterate id="category" name="Category"scope="application">
					<c:if test="${category.cateId eq OrderItemsInfo.cateId}">
					  <bean:write name="category" property="cateName" />
					</c:if>				
          		</logic:iterate>
				<c:if test="${OrderItemsInfo.cateId eq '20'}">
					  Private Jet Jaunts
				</c:if>
			 </td>          	
			 <td align="left"><div class="lable">
			 <c:if test="${OrderItemsInfo.orderItemStatus eq 'P'}">
			 Pending
			 </c:if>
			  <c:if test="${OrderItemsInfo.orderItemStatus eq 'C'}">
			  Completed
			 </c:if>
			  <c:if test="${OrderItemsInfo.orderItemStatus eq 'F'}">
			 Failed
			 </c:if>
			</div></td>
			 <td align="left"><div class="lable"><bean:write name="OrderItemsInfo" property="orderCreatedDt"/></div></td>
			 <!--<td align="left"><div class="lable"><bean:write name="OrderItemsInfo" property="airName"/></div></td> --> 
			<c:if test="${loginInfo.userType eq 'admin' && OrderItemsInfo.orderItemStatus ne 'C'}">    
			<td nowrap="nowrap" class="action"><div class="lable"><span class="lable"><a href="javascript: void fnCallEdit('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title="Edit Order details">Edit</a></span></div></td>
			 </c:if>	
			 
			 <c:if test="${loginInfo.userType eq 'admin' && OrderItemsInfo.orderItemStatus eq 'C'}">    
		<td nowrap="nowrap" class="action"><div class="lable"><span class="lable"><a href="javascript: void fnCallEdit('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a></span></div></td>
			 </c:if>	
			 
			 
		 <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline'}">    
		<td nowrap="nowrap" class="action"><div class="lable"><span class="lable"><a href="javascript: void fnCallEdit('<bean:write name='OrderItemsInfo' property='orderItemId'/>')" class="editdelete" title=" View Order details">View</a></span></div></td>
			 </c:if>		       	
           </tr>
		  </logic:iterate>
      </table>
		  </td>
       
    </table>  </logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
			 <font color="#FF0000" size="-2">No Records Found.</font>		
        </logic:present>	</td>
    </tr>
	<logic:present  name="orderItemsInfo"> 
	 <logic:notEmpty name="orderItemsInfo">	
  <tr>
      <td height="50" colspan="3" align="center" class="td"><html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button></td>
    </tr>
	</logic:notEmpty>
	</logic:present>
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>
</html:form>
