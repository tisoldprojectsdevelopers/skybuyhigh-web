<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript" src="js/menu.js"></script>
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>



function fnCallUploadProductImg(){
	document.forms[0].action="editUploadAirlineProdImg.do?method=editUploadAirlineProductImage";
	document.forms[0].submit();
}
</script>




<html:form action="editUploadAirlineProdImg" method="post" enctype="multipart/form-data">

<div id="page">
  <table border="0" width="400" cellpadding="0" cellspacing="0" id="cataloginfo">
    <thead>
      <tr>
        <th width="8" align="left"><img src="images/table_topleftcurve.gif" width="8" height="35" alt="" /></th>
        <th width="350" align="left"><h1>Upload Image(s)</h1></th>
        <th align="right"><img src="images/table_toprightcurve.gif" alt="" width="8" height="35" align="right" /></th>
      </tr>
    </thead>
    <tr>
      <td colspan="3"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="broder_top0">
          <tr class="tdbg">
		 	 <td rowspan="2"> <img src="<c:out value='${airlineProductDetails.mainImgPath}' />" width="63" height="79" /><br/>
		   <bean:write  name="airlineProductDetails"  property="mainImgCap" /></td>
            <td nowrap="nowrap" class="lable">Main Image </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="mainImgPath" accept="image/gif,image/jpeg" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td nowrap="nowrap" class="lable">Main Image Caption </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="mainImgCap" styleClass="input"/></td>
          </tr>
          <tr class="tdbg">
            <td colspan="4" align="center" nowrap="nowrap" bgcolor="#cccccc" class="lable">Alternate Views </td>
          </tr>
          <tr class="tdbg">
		  	<td rowspan="2"><c:if test="${airlineProductDetails.view1ImgPath ne ''}"><img src="<c:out value='${airlineProductDetails.view1ImgPath}' />" width="63" height="79" /><br/>
		  <bean:write  name="airlineProductDetails"  property="view1ImgCap" /></c:if></td>
            <td nowrap="nowrap" class="lable">Alternate View1 </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view1ImgPath" accept="image/gif,image/jpeg" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td nowrap="nowrap" class="lable">Alternate View1 Caption </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view1ImgCap" styleClass="input"/></td>
          </tr>
          <tr class="tdbg">
		  <td rowspan="2"><c:if test="${airlineProductDetails.view2ImgPath ne ''}"><img src="<c:out value='${airlineProductDetails.view2ImgPath}' />" width="63" height="79" /><br/>
		  <bean:write  name="airlineProductDetails"  property="view2ImgCap" /></c:if></td>
            <td nowrap="nowrap" class="lable">Alternate View2</td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view2ImgPath" accept="image/gif,image/jpeg" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td nowrap="nowrap" class="lable">Alternate View2 Caption </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view2ImgCap" styleClass="input"/></td>
          </tr>
          <tr class="tdbg">
		  <td rowspan="2"><c:if test="${airlineProductDetails.view3ImgPath ne ''}"><img src="<c:out value='${airlineProductDetails.view3ImgPath}' />" width="63" height="79" /><br/>
		  <bean:write  name="airlineProductDetails"  property="view3ImgCap" /></c:if></td>
            <td nowrap="nowrap" class="lable">Alternate View3</td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><span class="catagory">
            <html:file property="view3ImgPath" accept="image/gif,image/jpeg" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td nowrap="nowrap" class="lable">Alternate View3 Caption </td>
            <td nowrap="nowrap" class="lable">:</td>
            <td align="left" nowrap="nowrap"><html:text property="view3ImgCap" styleClass="input"/></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td height="50" colspan="3" align="center"><html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallUploadProductImg()"> Upload </html:button>
          <html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="28"> Cancel </html:button></td>
    </tr>
  </table>
</div>
</html:form>
