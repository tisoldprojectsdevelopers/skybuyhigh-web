<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "Description",
theme_advanced_buttons1 : "",
	plugins : "noneditable",			
	theme_advanced_toolbar_location : "none",
	theme_advanced_toolbar_align : "none"
	
});

function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}



/****Offer Description B****/




function fnCallSaveOrder(){
	document.forms[0].action="updateOrder.do?method=updateOrderDetails";
	document.forms[0].submit();
}


</script>




<html:form action="editOrder" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="images/top_nav_middlebg.png" align="left">			
		<ul>
		<li>You navigated from :</li>
		<li>Order</li>		
		</ul>		
	
	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	
	<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Information</h2></td>
        </tr>
      <tr>
	  
        <td colspan="3">
		<table width="100%" border="0" cellpadding="2" cellspacing="2" class="broder_top0">
            <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Billing Details :</h4> </td>
            </tr>
		  <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable">Confirmation Number </td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" nowrap="nowrap"><span class="catagory">         
			<bean:write  name="custOrderItemDetails" property="custTransId" />			
		
      </span></td>
            <td  align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          <tr class="tdbg">
            <td  align="left" nowrap="nowrap" class="lable">Customer First Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">
                <html:text property="custFirstName" styleClass="input" />
              </c:if>
              <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}">
                <bean:write  name="custOrderItemDetails" property="custFirstName" />
              </c:if>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Customer Last Name</td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">
                <html:text property="custLastName" styleClass="input" />
              </c:if>
              <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}">
                <bean:write  name="custOrderItemDetails" property="custLastName" />
              </c:if>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"> Address </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:text property="custAddress" styleClass="input" />
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custAddress" />		
				</c:if>	
      
        </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			   <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:text property="custCity" styleClass="input" />
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custCity" />
				</c:if>	              
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> State </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
				<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:select property="custState" styleClass="input" styleId="state" tabindex="10">
						 <html:option value="">------Select State------</html:option>
						 <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
           			 </html:select>
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><logic:iterate id="states" name="StateList" scope="application">
						<c:if test="${states.stateCode eq custOrderItemDetails.custState}">
							<bean:write name="states" property="stateName" />
						</c:if>
					</logic:iterate>
				</c:if>	  
			
			
			
             <!-- <bean:write  name="custOrderItemDetails" property="custState" />-->
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
			  <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
				  <html:select property="custCountry" styleClass="select">
					 <html:option value="US">US</html:option>
				  </html:select>
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custCountry" />
				</c:if>	    
             
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
			<c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:text property="custZip" styleClass="input" />
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custZip" />
				</c:if>	    
        
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext"> Phone</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             
			   <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:text property="custPhone" styleClass="input" />
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custPhone" />
				</c:if>	   
            </span></td>
          </tr>
		   <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id  </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
			 <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
					 <html:text property="custEmail" styleClass="input" />
			    </c:if>
				  <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custEmail" />
				</c:if>	    
        
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td align="left" class="lable">&nbsp;</td>
            <td align="left" nowrap="nowrap" class="catagory">&nbsp;</td>
          </tr>
           <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" nowrap="nowrap" class="lable"><h4>Shipping Details :</h4> </td>
            </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer First Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:text property="custShipFirstName" styleClass="input" />
              </c:if>			  
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipFirstName" />
              </c:if>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Customer Last Name </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
             <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:text property="custShipLastName" styleClass="input" />
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipLastName" />
              </c:if>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Address</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:text property="custShipAddress" styleClass="input" />
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipAddress" />
              </c:if>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">City</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:text property="custShipCity" styleClass="input" />
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipCity" />
              </c:if>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">State</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
             <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:select property="custShipState" styleClass="input" styleId="state" tabindex="10">
                  <html:option value="">------Select State------</html:option>
                  <html:options collection="StateList" property="stateCode" labelProperty="stateName"></html:options>
                </html:select>
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><logic:iterate id="states" name="StateList" scope="application">
                  <c:if test="${states.stateCode eq custOrderItemDetails.custShipState}">
                    <bean:write name="states" property="stateName" />
                  </c:if>
                </logic:iterate>
              </c:if>
              <!--  <bean:write  name="custOrderItemDetails" property="custShipState" />-->
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Country</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
            <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:select property="custShipCountry" styleClass="select">
                  <html:option value="US">US</html:option>
                </html:select>
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipCountry" />
              </c:if>
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Zip</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" nowrap="nowrap" class="catagory"><span class="catagory">
              <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
                <html:text property="custShipZip" styleClass="input" />
              </c:if>
                <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}"><bean:write  name="custOrderItemDetails" property="custShipZip" />
              </c:if>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Email Id </span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">
                <html:text property="custShipEmail" styleClass="input" />
              </c:if>
              <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}">
                <bean:write  name="custOrderItemDetails" property="custShipEmail" />
              </c:if>
            </span></td>
          </tr>
		   <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
          <tr class="tdbg">
            <td colspan="6" align="left" class="lable"><h4>Airline/Vendor Details:</h4></td>
            </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Airline Name</span></td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="airName"/>
            </span></td>
            <td align="left" nowrap="nowrap" class="lable">Tail No </td>
            <td align="left" class="lable">:</td>
            <td align="left" class="catagory"><span class="catagory">
              <bean:write  name="custOrderItemDetails"  property="flightNo" />
            </span></td>
          </tr>
          <tr class="tdbg">
            <td align="left" class="lable"><span class="boldtext">Vendor Name</span></td>
            <td width="10" align="left" class="lable">:</td>
            <td width="50%" align="left" class="catagory"><span class="catagory">
          <bean:write  name="custOrderItemDetails"  property="vendorName"/>
        </span></td>
            <td align="left" nowrap="nowrap" class="lable">&nbsp;</td>
            <td width="10" align="left" class="lable">&nbsp;</td>
            <td width="50%" align="left" class="catagory">&nbsp;</td>
          </tr>
          
          <tr align="left" class="tdbg">
            <td colspan="6" valign="top" nowrap="nowrap"  class="lable"><hr /></td>
            </tr>
			 <tr class="tdbg">
			   <td colspan="6" align="left" class="lable"><h4>Order Details :</h4> </td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable">Order Id </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderId"/>
               </span></td>
			   <td colspan="3" rowspan="10" align="left" class="lable"><div align="center"><img src="<c:out value='${custOrderItemDetails.mainImgPath}' />" width="200" height="200" /></div></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Order Date </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="orderCreatedDt" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable">Category</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <logic:iterate id="category" name="Category"scope="application">
                   <c:if test="${category.cateId eq custOrderItemDetails.cateId}">
                     <bean:write name="category" property="cateName" />
                   </c:if>
                 </logic:iterate>
				 <c:if test="${custOrderItemDetails.cateId eq '20'}">
                    Private Jet Jaunts
                 </c:if>
                 <!--<bean:write  name="custOrderItemDetails"  property="cateId" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
               <td align="left" class="lable"><span class="boldtext">Item Code </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="prodCode"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" class="lable"><span class="boldtext">Item Name </span></td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="itemName"/>
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Brand Name </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="brandName" />
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Item Status </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'P'}"> Pending </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'C'}"> Completed </c:if>
                 <c:if test="${custOrderItemDetails.orderItemStatus eq 'F'}"> Failed </c:if>
                 <!--    <bean:write  name="custOrderItemDetails"  property="orderItemStatus" />-->
               </span></td>
			   </tr>
			 <tr class="tdbg">
			   <td align="left" nowrap="nowrap" class="lable">Qty</td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">
                   <html:text property="qty" style="width:60px;"/>
                 </c:if>
                 <c:if test="${loginInfo.userType eq 'vendor' || loginInfo.userType eq 'airline' || loginInfo.userType eq 'admin' && mode eq 'Update' || loginInfo.userType eq 'admin' && custOrderItemDetails.orderItemStatus eq 'C'}">
                   <bean:write  name="custOrderItemDetails" property="qty" />
                 </c:if>
               </span></td>
			   </tr>		
			 
			 <tr class="tdbg">			
			   <td align="left" class="lable">
			   <c:if test="${custOrderItemDetails.ownerType eq 'vendor'}">		   
			   Retail Price
			   </c:if>
			   <c:if test="${custOrderItemDetails.ownerType eq 'airline'}">		   
			   Price
			   </c:if>
			   </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="vendPrice"/>
               </span></td>			  
			   </tr>
			   
			    <tr class="tdbg">
			  <c:if test="${loginInfo.userType eq 'admin'}">
			   <td align="left" nowrap="nowrap" class="lable">SkyBuy<sup>High</sup> Price </td>
			   <td align="left" class="lable">:</td>
			   <td align="left" class="catagory"><span class="catagory">
                 <bean:write  name="custOrderItemDetails"  property="sbhPrice" />
               </span></td>
			    </c:if>
			   </tr>
      </table>
		</td>
      </tr>
      <tr>
        <td height="50" colspan="3" align="center"> 
			 <c:if test="${loginInfo.userType eq 'admin' && mode eq 'Edit' && custOrderItemDetails.orderItemStatus ne 'C'}">	
			 <html:button property="method" styleClass="button" onclick="javascript:fnCallSaveOrder()">Save </html:button> 	
		   </c:if>
		  <html:button property="method" onclick="history.back()" styleClass="button">Cancel</html:button>
		
		  </td>
       </tr>
    </table>
	
	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>
<html:hidden property="custId"/>
<html:hidden property="orderItemId"/>
</div>
</div>

</html:form>
