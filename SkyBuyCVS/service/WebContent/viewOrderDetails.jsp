<%@ page language="java" session="true"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>



<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">

function fnCallView(jsOrderIdValue) {
	document.forms[0].action="getOrderItemDetails.do?method=getOrderItemDetails&OrderId="+jsOrderIdValue;
	document.forms[0].submit();

}
	
</script>

<html:form action="searchAirline.do?method=searchAirline" method="post">
<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="images/top_nav_middlebg.png" align="left">
		<c:if test="${OrderStatus eq 'C'}">
		<ul>
		<li><!--<c:out value="${compltedOrders}"/> -->Successfully Completed Order Details</li>
		</ul>
		</c:if>
		
		<c:if test="${OrderStatus eq 'P'}">
		<ul>
		<li><!--<c:out value="${pendingOrders}"/>-->Pending Order Details</li>
		</ul>
		</c:if>
		
		
		<c:if test="${OrderStatus eq 'F'}">
		<ul>
		<li><!--<c:out value="${incompleteOrders}"/>--> Incomplete Order Details </li>
		</ul>
		</c:if>	</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="3" class="td" align="center">
	<logic:present name="orderDetails" scope="request"> 
		 <logic:notEmpty name="orderDetails">
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Order Details</h2></td>
        </tr>
      <tr>
        <td align="center"><table width="100%" border="0" cellpadding="1" cellspacing="1"  bgcolor="#cccccc" class="broder_top0">
              <tr>
               
                <td height="32" align="center" nowrap="nowrap" class="table_header">Confirmation Number</td>
                <td class="table_header" nowrap="nowrap" width="150px">Customer Name</td>
				<td class="table_header" nowrap="nowrap" width="150px">Customer Email</td>
                <td class="table_header" nowrap="nowrap" width="150px">Airline Name </td>
                <td class="table_header" nowrap="nowrap"  width="100px">Tail No</td>
                <td class="table_header" nowrap="nowrap">Order Status</td>
                <td class="table_header" nowrap="nowrap">Order Date</td>
				  <td class="table_header" nowrap="nowrap"  width="50px">Action</td>
              </tr>
              <logic:iterate id="OrderDetails" name="orderDetails">
              <tr class="tdbg">
                <td align="left" nowrap="nowrap">&nbsp;<bean:write name="OrderDetails" property="custTransId"/></td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;<bean:write name="OrderDetails" property="custName"/></td>
                <td align="left">&nbsp;&nbsp;<bean:write name="OrderDetails" property="custEmail"/></td>
				<td align="left">&nbsp;&nbsp;<bean:write name="OrderDetails" property="airName"/></td>
				<td align="left">&nbsp;&nbsp;<bean:write name="OrderDetails" property="flightNo"/></td>
                <td align="left">&nbsp;&nbsp;<bean:write name="OrderDetails" property="orderStatus"/></td>
                <td align="left">&nbsp;&nbsp;<bean:write name="OrderDetails" property="orderDt"/></td>
				<td align="center"><a href="javascript: void fnCallView('<bean:write name='OrderDetails' property='orderId'/>')" class="editdelete"><img src="images/viewicon.gif" alt="View Order Item" width="14" height="15" hspace="3" border="0" align="absmiddle" longdesc="#" /></a></td>
              </tr>
             </logic:iterate>
            </table>			</td>
      </tr>
    </table>
			</td>
    </tr>
  <tr>
    <td height="30" colspan="3" align="center" valign="bottom" class="td"><html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="18"> Cancel </html:button>			
	</logic:notEmpty>
	</logic:present>
		<logic:present name="NoRecords" scope="request">
	  <font color="#FF0000" size="-2">No Records Found.</font>        </logic:present></td>
  </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

</div>

</div>

</html:form>


