<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<script type="text/javascript">
function initPage()
{
	var navRoot1 = document.getElementById("log-block1");
	if (navRoot1) 
	{
		navRoot1.onmouseover = function()
		{
			this.className = "hover";
                }
		navRoot1.onmouseout = function()
		{
			this.className = "";
		}
	}
	var navRoot2 = document.getElementById("log-block2");
	if (navRoot2) 
	{
		navRoot2.onmouseover = function()
		{
			this.className = "hover";
                }
		navRoot2.onmouseout = function()
		{
			this.className = "";
		}
	}
	
	var navRoot3 = document.getElementById("log-block3");
	if (navRoot3) 
	{
		navRoot3.onmouseover = function()
		{
			this.className = "hover";
                }
		navRoot3.onmouseout = function()
		{
			this.className = "";
		}
	}
	

}
if (window.attachEvent && !window.opera)
	attachEvent("onload", initPage);
</script>

<div class="menu">
<ul>
	
    <li class="selected" ><a href="preEntry.do?method=preEntry">Home</a></li>
	<logic:present name="loginInfo" scope="session">
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li id="log-block1"><a href="#" >Vendor</a>
	<ul>
		<li>
		<a class="menuItem" href="editVendor.do?method=EditVendorDetails&VendId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a></li>
		</ul>
		
		
	</li>
		
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li id="log-block2"><a href="#">Catalogue</a>
			<ul>					
				<li><a class="menuItem" href="initAddVendorProduct.do?method=initAddVendorProduct" title="Add Item">Add Item &nbsp;&nbsp;&nbsp;</a></li>
				<li><a class="menuItem" href="initSearchVendorCatalogue.do?method=initSearchVendorCatalogue" title="Edit/Search Item">Edit/Search Item</a> </li>
				<li><img width="100%" height="1" alt="" src="images/header-log-divider.gif" /></li>
				<li><a class="menuItem" href="generateCatalogue.do?method=generateCatalogue&UserType=Vendor" title="Generate Catalogue">Generate Catalogue</a> </li>
				
			</ul>
	</li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li id="log-block3"><a href="#">Order</a>
	<ul class="subMenu">	
		<li><a class="menuItem" href="initSearchOrder.do?method=initSearchOrder" title="Edit/Search Order">Edit/Search Order</a></li>		
		</ul>
	</li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li><a href="http://skybuyhigh.com/reports/sbhorder.php?ownerid=<c:out value='${loginInfo.refId}'/>&ownertype=<c:out value='${loginInfo.userType}'/>" title="Reports/Statements">Reports/Statements</a></li>
	
	</logic:present>
	<logic:empty name="loginInfo" scope="session">
	<li><a href="preEntry.do?method=preEntry">Home</a></li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li class="selected"><a href="#">Vendor</a></li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li><a href="merchandise.html">Catalogue</a></li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li><a href="#">Order</a></li>
	<li class="divider"><img src="images/h_line.gif" height="20" width="2" /></li>
	<li><a href="#">Reports/Statements</a></li>
	</logic:empty>
</ul>
</div>
</div>
