<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link href="style/registration.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jscripts/tiny_mce/tiny_mce_dev.js"></script>
<script>
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}
function trim(inputString) {
		 var retValue = inputString;
		 var ch = retValue.substring(0, 1);
		 while (ch == " ") {
				retValue = retValue.substring(1, retValue.length);
				ch = retValue.substring(0, 1);
		 }
		 ch = retValue.substring(retValue.length-1, retValue.length);
		 while (ch == " ") {
				retValue = retValue.substring(0, retValue.length-1);
				ch = retValue.substring(retValue.length-1, retValue.length);
		 }
		 return retValue;
}
function disablePaste(e)
	{
	  
	  if(e.ctrlKey && e.keyCode == '86') // CTRL-V
      {
       window.clipboardData.clearData();
		
     }
   
     
    return true; 
	}

function stripTags(txt) { 
	var str = new String(txt); 
	str = str.replace(/<br\/>/gi,"\n"); 
	str=str.replace(/<[^>]+>/g,"");
	str=str.replace(/&nbsp;/gi,"");
	return str;
}
 
function textLimit(fieldLen,maxlen,dispName) {
	if (fieldLen > parseInt(maxlen) + 1){
		alert(dispName+" can have maximum of "+maxlen+" chars only."); 
		return false;
	}else
		return true;
}

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "shortDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
				var key= ed.selection.select(e.keyCode);
				if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
					var str=ed.getContent();
					str=(stripTags(str)); 
					str=trim(str); 
					var textLen = 128;
					if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
						textLen = 50;
					//alert(textLen);
					if(textLimit(str.length,textLen,"Short Description A"))
						return true; 
				}	
		  }); 
		return false;
	  } 
});

/****Offer Description B****/

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_selector : "longDescription",
  handle_event_callback : "disablePaste",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "customtag,bold,italic,underline,fontsizeselect,pasteword,bullist,numlist,spellchecker",
		theme_advanced_buttons2 : "",
	 font_size_style_values : "8pt,10pt,12pt,14pt,18pt,24pt,36pt",
	theme_advanced_toolbar_location : "external",
	theme_advanced_toolbar_align : "left",
	
	setup : function(ed) { 
		ed.onKeyUp.add(function(ed,e) { 
			var key= ed.selection.select(e.keyCode);
			if((key >=47 && key <= 126) || (key == 13) || (key >=32 && key <= 34) || (key >=41 && key <= 44)){
				var str=ed.getContent();
				str = stripTags(str);
				str=trim(str); 
				var textLen = 128;
				if((textLen == '') || (textLen == undefined) || (textLen == '0')) 
					textLen = 60;
				if(textLimit(str.length,textLen,"Long Description")) 
					return true; 
			}		
		  }); 
		return false;
	  } 
});


function fnCallAddDevice(){
	document.forms[0].action="addDeviceReg.do?method=addDeviceRegDetails";
	document.forms[0].submit();
}
function fnCallEditDevice(){
	document.forms[0].action="updateDeviceReg.do?method=updateDeviceDetails";
	document.forms[0].submit();
}

</script>


<div class="contentcontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
  <tr>
    <td class="leftcutver"><img src="images/top_nav_leftcurve.png" width="26" height="44" /></td>
    <td width="100%" background="images/top_nav_middlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_rightcurve.png" width="26" height="44" /></td>
  </tr>
  <tr>
    <td colspan="3" class="td">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="td">
	<html:form action="addDeviceReg" method="post" >
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
      <tr>
        <td height="30" colspan="3" class="tablehead" align="center"><h2>Device Information</h2></td>
        </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="2" cellspacing="0" class="broder_top0">

      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable">Air Code</td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
		
			<html:select property="airId" styleClass="textarea2" tabindex="1">
			 <html:option value="">--Select AirCode--</html:option>
			 <html:options collection="airCodeList" property="airId" labelProperty="airCode"></html:options>
            </html:select>		
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Type</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="deviceType" styleClass="input" tabindex="2"/>
        </span></td>
      </tr>
       <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Serial No</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="deviceSerialNo" styleClass="input" tabindex="3"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Device Model</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
         <html:text property="deviceModel" styleClass="input" tabindex="4"/>
        </span></td>
      </tr>
       
      <tr class="tdbg">
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Mac Address</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="macAddr" styleClass="input" tabindex="5"/>
        </span></td>
        <td align="left" nowrap="nowrap" class="lable"><span class="boldtext">Processor Id</span></td>
        <td align="left" class="lable">:</td>
        <td align="left"><span class="catagory">
          <html:text property="processorId" styleClass="input_180" tabindex="6"/>
        </span></td>
      </tr>
      
      <tr class="tdbg">
        <td align="left" valign="top" nowrap="nowrap" class="lable"><span class="boldtext">Mac Description</span></td>
        <td align="left" valign="top" class="lable">:</td>
        <td colspan="4" align="left"> 
			<textarea name="macDesc" rows="6" onKeyUp="textLimit(this.form.macDesc,this.form.commentlen,128,'Mac Description');" class="textarea1" tabindex="7"><logic:present name="deviceDetails"><logic:notEmpty name="deviceDetails"><bean:write  name="deviceDetails" property="macDesc"/></logic:notEmpty></logic:present></textarea><br/>
			<span class="normaltext">Remaining characters</span>
			 <input readonly type=text name=commentlen size=3 maxlength=3 value="128" class="wordcount"/></td>
        </tr>
      
    </table>
      </tr>
      <tr>
         <td height="50" colspan="3" align="center"> <logic:present name="mode">
			 <logic:equal name="mode"  value="deviceAdd">	 
	 		 <html:button property="method" styleClass="button" 
		  	onclick="javascript:fnCallAddDevice()" tabindex="8"> Add </html:button> 
		  </logic:equal>
		</logic:present>    
		
		<logic:present name="mode">
			 <logic:equal name="mode"  value="deviceEdit">
	  <html:button property="method" styleClass="button" 
		  onclick="javascript:fnCallEditDevice()" tabindex="9">Save </html:button> 
		   </logic:equal>
		</logic:present>   
		
        <html:button property="method" styleClass="button" 
		  onclick="history.back();" tabindex="10" > Cancel </html:button></td>
       
      </tr>
    </table>
	<html:hidden property="deviceId"/>
</html:form>

	</td>
    </tr>
  
  <tr>
    <td><img src="images/top_nav_bleftcurve.png" width="26" height="34" /></td>
    <td background="images/top_nav_bmiddlebg.png">&nbsp;</td>
    <td align="right"><img src="images/top_nav_brightcurve.png" width="26" height="34" /></td>
  </tr>
</table>

<!--<ul>
<li class="leftcutver"></li>
<li class="middlebg"><h1>asdasd</h1></li>
<li class="rightcutver"></li>
</ul>
<ul style=" background:#fff; list-style-type:none; width:100%;">
<li >asdfadsf</li>
</ul>
<ul>
<li class="bottomleftcutver"></li>
<li class="bottommiddlebg"></li>
<li class="bottomrightcutver"></li>
</ul>-->
</div>
</div>

