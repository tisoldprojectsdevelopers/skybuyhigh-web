/**
 * $Id: editor_plugin_src.js,v 1.1 2009/08/25 09:35:06 narayanan Exp $
 *
 * @author Moxiecode
 * @copyright Copyright � 2004-2008, Moxiecode Systems AB, All rights reserved.
 */

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('customTag');

	tinymce.create('tinymce.plugins.ExamplePlugin', {
    createControl: function(n, cm) {
        switch (n) {
            case 'customtag':
                var mlb = cm.createListBox('customtag', {
                     title : 'Custom Tag',
                     onselect : function(v) {
                         tinyMCE.activeEditor.execCommand('mceInsertContent', false,  v);
                     }
                });

                // Add some values to the list box
                mlb.add('Bussiness Name', '<strong>{{BUS_NAME}}</strong>');
                mlb.add('Address 1', '<strong>{{BUS_ADDR_1}}</strong>');
                mlb.add('Address 2', '<strong>{{BUS_ADDR_2}}</strong>');				  
				mlb.add('CITY', '<strong>{{BUS_CITY}}</strong>');
                mlb.add('STATE', '<strong>{{BUS_STATE}}</strong>');

                // Return the new listbox instance
                return mlb;

          
        }

        return null;
    }
});

// Register plugin with a short name
tinymce.PluginManager.add('example', tinymce.plugins.ExamplePlugin);

})();