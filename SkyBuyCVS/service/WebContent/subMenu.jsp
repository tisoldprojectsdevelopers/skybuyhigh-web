<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<ul id=vendorMenu>
	<logic:present name="loginInfo" scope="session">
		<li><a  href="editVendor.do?method=EditVendorDetails&VendId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a></li>
	</logic:present>
</ul>
<ul id=airlineRegMenu>
	<logic:present name="loginInfo" scope="session">	
		<li><a  href="editAirlineReg.do?method=editAirlineDetails&airId=<bean:write name='loginInfo' property='refId' />" title="Edit My Info">Edit My Info</a></li>
	</logic:present>
	<li><a  href="initSearchDevice.do?method=initSearchDevice" title="Edit Device">Search Device</a></li>
</ul>

<ul id=vendorCatalogueMenu>
	<li><a  href="initAddVendorProduct.do?method=initAddVendorProduct" title="Add Item">Add Item</a></li>
	<li><a  href="initSearchVendorCatalogue.do?method=initSearchVendorCatalogue" title="Edit/Search Item">Edit/Search Item</a></li>
	<li><a  href="generateCatalogue.do?method=generateCatalogue&UserType=Vendor" title="Generate Catalogue">Generate Catalogue</a> </li>
</ul>

<ul id=airlineCatalogueMenu>
	<li><a  href="initAddAirlineProduct.do?method=initAddAirlineProduct" title="Add Item">Add Item</a></li>
	<li><a  href="initSearchAirlineCatalogue.do?method=initSearchAirlineCatalogue" title="Edit/Search Item">Edit/Search Item</a></li>
	<li><a  href="generateCatalogue.do?method=generateCatalogue&UserType=Airline" title="Generate Catalogue">Generate Catalogue</a></li>
</ul>

<ul id=adminMenu>
	<li><a  href="#" title="Manage Airline">Manage Airline</a> 
		<ul>
		    <li><a  href="initAddAirlineReg.do?method=initAddAirlineReg" title="Manage Airline">Add Airline</a> 	</li>
			<li><a  href="initSearchAirline.do?method=initSearchAirline" title="Manage Airline">Edit/Search Airline</a> 	</li>
		</ul>
	</li>
	<li>
		<a  href="#" title="Manage Vendor">Manage Vendor</a> 
		<ul>
		    <li><a  href="initAddVendor.do?method=initAddVendor" title="Manage Airline">Add Vendor</a> 	</li>
			<li><a  href="initSearchVendor.do?method=initSearchVendor" title="Manage Airline">Edit/Search Vendor</a> 	</li>
		</ul>
	</li>
</ul>
<ul id=merchandizeMenu>
	<li><a  href="initEditSearchMerchandize.do?method=initEditSearchMerchandize" title="Edit/Search Merchandise">Edit/Search Merchandise</a> </li>
	<li><a  href="initSearchMerchandize.do?method=initSearchMerchandize" title="Accept/Reject Merchandise">Accept/Reject Merchandise</a> </li>
</ul>

	
